#pragma once
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "TextureManager.h"
#include "AnimDirectional.h"
#include "Utilities.h"
#include <unordered_map>
#include <map>



class TestEntity;
class Window;
/**
 * A simple Sprite class.
 */
class GameSprite
{
public:
	GameSprite(TextureManager* textureManager);
	~GameSprite();

	void CropSprite(const sf::IntRect& rect);
	bool LoadSheet(const string& filename);
	void ReleaseSheet();
	void Update(real32 dt);
	void Draw(Window* window, int32 zOrder = -1);
	Direction StringToDirection(const string& directionString);

	/********************************************************************************
	 * Getters&Setters.
	 ********************************************************************************/
	///@{
	sf::IntRect GetSpriteRect(const string& name, const Direction& direction);
	sf::IntRect GetDefaultRect(const Direction& direction);
	void SetOrigin(const Origin& origin);
	//void SetSpriteRect(sf::IntRect rect);
	const vec2u& GetSpriteSize() const;
	const vec2f& GetSpritePosition() const;
	void SetSpriteSize(const vec2u& size);
	void SetPosition(const vec2f& pos);
	void SetDirection(const Direction& direction);
	void Tint(const sf::Color& color);
	Direction GetDirection();
	AnimBase* GetCurrentAnim();
	bool SetAnimation(const string& animName, bool play = false, bool loop = false);
	///@}********************************************************************************/
	void StopAnimation();
public:
	TestEntity* owner_;
	//sf::Sprite sprite_;
	sf::Sprite* sprite_;
	void SetScale(const vec2f& scale);
private:
	Origin origin_;
	string textureName_;
	vec2u spriteSize_;
	vec2f spriteScale_;
	Direction direction_;
	sf::IntRect currentRect_;


	string animationType_;
	std::unordered_map<string, AnimBase*> animations_;
	AnimBase* animationCurrent_;
	TextureManager* textureManager_;
	std::map<std::pair<string, Direction>, sf::IntRect> spriteRects_;
	std::map<Direction, sf::IntRect> defaultSpriteRects_;
	//std::map<std::pair<string, Direction>, std::vector<sf::IntRect>> animationFrameRects_;
public:
	void SetTexture(const string& textureName);
};
