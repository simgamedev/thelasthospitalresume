#pragma once
#include "BaseState.h"
#include "EventManager.h"

class StatePaused : public BaseState
{
public:
	StatePaused(StateManager* stateManager);
	~StatePaused();

	void OnCreate();
	void OnDestroy();

	void Activate();
	void Deactivate();

	void Update(const sf::Time& time);
	void Draw();

	void Unpause(EventDetails* eventDetails);
private:

	sf::Font font_;
	sf::Text text_;
	sf::RectangleShape rectangle_;
};
