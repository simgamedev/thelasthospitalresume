#pragma once
#include "common.h"
#include "Directions.h"

class GameSprite;
/**
   A simple animation base class.
 */
class AnimBase
{
	friend class GameSprite;
public:
	AnimBase();
	virtual ~AnimBase();

	void Play();
	void Pause();
	void Stop();
	void Reset();
	virtual void Update(real32 dt);

	/** @name Getters&Setters
	*/
	///@{
	void SetSprite(GameSprite* sprite);
	bool SetCurrentFrame(uint32 frame);
	void SetStartFrame(uint32 frame);
	void SetEndFrame(uint32 frame);
	//void SetFrameRow(uint32 row);
	//void SetActionStart(uint32 frame);
	//void SetActionEnd(uint32 frame);
	void SetLooping(bool looping);
	void SetName(const string& name);
	void SetFrameTime(real32 time);
	GameSprite* GetSprite();
	uint32 GetCurrentFrame();
	uint32 GetStartFrame();
	uint32 GetEndFrame();
	//uint32 GetFrameRow();
	//int32 GetActionStart();
	//int32 GetActionEnd();
	real32 GetFrameTime();
	real32 GetElapsedTime();
	bool IsLooping();
	bool IsPlaying();
	//bool IsInAction();
	//bool CheckMoved();
	string GetName();
	///@}********************************************************************************/

	friend std::stringstream& operator>>(std::stringstream& stream, AnimBase& animBase)
	{
		animBase.ReadIn(stream);
		return stream;
	}
protected:
	virtual void FrameStep() = 0;
	virtual void CropSprite() = 0;
	virtual void ReadIn(std::stringstream& stream) = 0;

	uint32 frameCurrent_;
	uint32 frameStart_;
	uint32 frameEnd_;
	uint32 frameRow_;
	//int32 frameActionStart_;
	//int32 frameActionEnd_;
	real32 frameTime_;
	real32 elapsedTime_;
	bool looping_;
	bool playing_;
	//bool hasMoved_;
	bool frameJumped_;

	string name_;
	GameSprite* sprite_;
};
