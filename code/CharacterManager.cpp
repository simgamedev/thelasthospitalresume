#include "CharacterManager.h"
#include "StateGame.h"
#include "ECS/EntityManager.h"
#include <SFML/Graphics/Sprite.hpp>
//#include <SFML/Graphics/View.hpp>

int32 CharacterManager::nextID_ = 0;

// TODO: should be a singleton
CharacterManager::CharacterManager(StateGame* stateGame)
	: pStateGame_(stateGame)
{
	//CharMsgDispatcher::Instance()->Register(CharMsgType::GotHome, this);
	RegisterForMsgType(CharMsgType::GotHome);
}

void
CharacterManager::NewCharacter(Profession profession, const vec2f& pos)
{
	if (profession == Profession::Patient)
	{
		Patient* newChar = new Patient(pos, pStateGame_);
		newChar->SetID(nextID_);
		characters_.emplace(nextID_, newChar);
		nextID_++;
		pStateGame_->GetGameWorld()->TestEntityBorn(newChar);
	}
	else if (profession == Profession::Tester)
	{
		Tester* newChar = new Tester(pos, pStateGame_);
		newChar->SetID(nextID_);
		characters_.emplace(nextID_, newChar);
		nextID_++;
		pStateGame_->GetGameWorld()->TestEntityBorn(newChar);
	}
	else if (profession == Profession::Nurse)
	{
		Nurse* newChar = new Nurse(pos, pStateGame_);
		newChar->SetID(nextID_);
		characters_.emplace(nextID_, newChar);
		nextID_++;
		pStateGame_->GetGameWorld()->TestEntityBorn(newChar);
	}
	else
	{
		TestEntity* newChar = new TestEntity(profession, pos, pStateGame_);
		newChar->SetID(nextID_);
		characters_.emplace(nextID_, newChar);
		nextID_++;
		pStateGame_->GetGameWorld()->TestEntityBorn(newChar);
	}
}

std::vector<TestEntity*>
CharacterManager::GetAllCharactersOfProfession(Profession profession)
{
	// TODO: OPTIMIZATION
	std::vector<TestEntity*> result;
	for(auto itr : characters_)
	{
		TestEntity* character = itr.second;
		if(character->GetProfession() == profession)
		{
			result.push_back(character);
		}
	}
	return result;
}

TestEntity*
CharacterManager::GetCharacterFromID(const int32 id)
{
	auto itr = characters_.find(id);
	if(itr == characters_.end())
		return nullptr;
	return itr->second;
		
}

void
CharacterManager::Update(const sf::Time& time)
{
	for(auto& itr : characters_)
	{
		itr.second->Update(time);
	}
}

int32
CharacterManager::GetNumCharacters() const
{
	return characters_.size();
}

int32
CharacterManager::SetHoldingEntity(const int32& characterID, const string& entityName)
{
	TestEntity* character = GetCharacterFromID(characterID);
	EntityManager* entityManager = pStateGame_->GetEntityManager();
	int32 id = entityManager->AddEntity(entityName);
	return id;
}

void
CharacterManager::Draw(Window* window)
{
	for (auto itr : characters_)
	{
		TestEntity* character = itr.second;
		character->Draw(window);
	}
}

void
CharacterManager::OnBroadcast(Telegram telegram)
{
	switch (telegram.type)
	{
		case CharMsgType::GotHome:
		{
			// delete this 
			TestEntity* character = GetCharacterFromID(telegram.sender);
			characters_.erase(telegram.sender);
			// TODO: refactor this to pStateGame_->DeselectSelectedCharacter
			pStateGame_->pSelectedCharacter_ = nullptr;
			delete character;
		} break;
	}
}


/*
void
CharacterManager::OnMessage(Telegram telegram)
{
}
*/

void
CharacterManager::RegisterForMsgType(CharMsgType msgType)
{
	CharMsgDispatcher::Instance()->Register(msgType, this);
}

/******************************* GetCharacters **********************************
 *
 ********************************************************************************/
std::map<int32, TestEntity*>
CharacterManager::GetCharacters()
{
	return characters_;
}

std::vector<TestEntity*>
CharacterManager::GetVisibleCharacters(const sf::FloatRect& viewSpace)
{
	std::vector<TestEntity*> visibleCharacters;
	for (auto itr : characters_)
	{
		if (viewSpace.intersects(itr.second->GetGlobalBounds()))
		{
			visibleCharacters.push_back(itr.second);
		}
	}
	return visibleCharacters;
}
