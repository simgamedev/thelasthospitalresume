#include "StateMainMenu.h"
#include "StateManager.h"

#include "Window.h"


using namespace Noesis;
/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
StateMainMenu::StateMainMenu(StateManager* stateManager)
	: BaseState(stateManager)
    , bKey1Pressed_(false)
{
}


/********************************************************************************
 * Empty Destructor.
 ********************************************************************************/
StateMainMenu::~StateMainMenu(){}


void
StateMainMenu::OnCreate()
{
    // ---> Logo
    TextureManager* textureManager = stateManager_->GetContext()->textureManager;
    textureManager->RequireResource("MainMenuLogo");
    sf::Texture* logoTexture = textureManager->GetResource("MainMenuLogo");
    vec2f textureSize = vec2f(logoTexture->getSize());
    vec2f windowSize = vec2f(stateManager_->GetContext()->window->GetWindowSize());
    logo_ = new sf::Sprite();
    logo_->setTexture(*logoTexture);
    logo_->setOrigin(vec2f(textureSize.x / 2, textureSize.y));
    vec2f logoPosition(windowSize.x / 2.0f, windowSize.y * 0.4f);
    if (windowSize.y < 1050)
    {
        logo_->setScale(0.5f, 0.5f);
        logoPosition.y = windowSize.y * 0.3f;
    }
	logo_->setPosition(logoPosition);
    // ---< Logo


	// ---> Version Number
	FontManager* fontManager = stateManager_->GetContext()->fontManager;
	fontManager->RequireResource("04b_30");
	sf::Font* font = fontManager->GetResource("04b_30");
	versionNumber_.setFont(*font);
	versionNumber_.setString("Alpha 0");
    versionNumber_.setFillColor(sf::Color(255, 0, 0, 255));
    versionNumber_.setCharacterSize(28);
    versionNumber_.setPosition(vec2f(windowSize.x * 0.9, windowSize.y * 0.92));
    // ---< Version Number

    // ---> Musics
    AudioManager* audioManager = stateManager_->GetContext()->audioManager;
    audioManager->PlayMusic("MainMenuThemeMusic", 20.0f, true);
    // ---< Musics


	EventManager* eventManager = stateManager_->GetContext()->eventManager;
    eventManager->AddCallback(GameStateType::MainMenu, "Key_1_Pressed", &StateMainMenu::OnKey1Pressed, this);

    // ---> NoesisGUI
    Noesis::RegisterComponent<MYGUI::MainMenuGUI>();
    spMainMenuGUI_ = *new MYGUI::MainMenuGUI();
    spMainMenuGUI_->SetStateMainMenu(this);
    Window* window = stateManager_->GetContext()->window;
    // TODO: UI could be on a seperate thread
    spNoesisView_ = Noesis::GUI::CreateView(spMainMenuGUI_);
    window->SetNoesisView(spNoesisView_);
    // ---< NoesisGUI

    LOG_ENGINE_INFO("StateMainMenu Created");
}


void
StateMainMenu::OnKey1Pressed(EventDetails* eventDetails)
{
    //Window* window = stateManager_->GetContext()->window;
    //window->PauseNoesisGUI();
	stateManager_->SwitchTo(GameStateType::Game);
}

/********************************************************************************
 * NewGame
 ********************************************************************************/
void
StateMainMenu::NewGame()
{
    Window* window = stateManager_->GetContext()->window;
    window->PauseNoesisGUI();
	stateManager_->SwitchTo(GameStateType::Game);
}

/********************************************************************************
 * Credits
 ********************************************************************************/
void
StateMainMenu::Credits()
{
}

/********************************************************************************
 * Map Editor.
 ********************************************************************************/
void
StateMainMenu::MapEditor()
{
}

/********************************************************************************
 * Exit
 ********************************************************************************/
void
StateMainMenu::Exit()
{
    stateManager_->GetContext()->window->Close();
}

void
StateMainMenu::OnDestroy()
{
    LOG_ENGINE_INFO("StateMainMenu Destroyed");
}

/******************************* Activate ****************************************
 *
 ********************************************************************************/
void
StateMainMenu::Activate()
{
    LOG_ENGINE_INFO("StateMainMenu Activated");
}

/******************************* Deactivate *************************************
 *
 ********************************************************************************/
void StateMainMenu::Deactivate()
{
    LOG_ENGINE_INFO("StateMainMenu Deactivated");
    //Window* window = stateManager_->GetContext()->window;
}


/********************************************************************************
 * Gets called when Binding"Mouse_Left" gets triggered
 ********************************************************************************/
void
StateMainMenu::MouseClick(EventDetails* eventDetails)
{
}


void
StateMainMenu::Draw()
{
    Window* window = stateManager_->GetContext()->window;
	window->Draw(*logo_);
	window->Draw(versionNumber_);
}

void StateMainMenu::Update(const sf::Time& time){}
