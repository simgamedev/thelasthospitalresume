#pragma once
#include <string>
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <SFML/System/Mutex.hpp>
#include <SFML/System/Lock.hpp>
#include "Utilities.h"
/**
   A base class for all resource management classes.
 */

template<typename T>
using ResourceEntry = std::pair<std::unique_ptr<T>, uint32>;
template<typename Derived, typename T>
class ResourceManager
{
public:
	ResourceManager(const string& pathsFile)
	{
		LoadPathsFromFile(pathsFile);
	}

	virtual ~ResourceManager() { PurgeResources(); }

	/********************************************************************************
	 * Get a pointer to the resource.
	 ********************************************************************************/
	T* GetResource(const string& name)
	{
		/// @todo multithreading
		sf::Lock lock(mutex_);
		auto resourceEntry = Find(name);
		return(resourceEntry ? resourceEntry->first.get() : nullptr);
	}

	/********************************************************************************
	 * Get resource's file path.
	 ********************************************************************************/
	string GetPath(const string& name)
	{
		auto pathsItr = paths_.find(name);
		return(pathsItr != paths_.end() ? pathsItr->second : "");
	}


	/********************************************************************************
	 * Every time a resource is required, it's retain count increments by 1.
	 ********************************************************************************/
	bool RequireResource(const string& name)
	{
		// Case 1: Resource is already loaded.
		/// @todo multithreading
		sf::Lock lock(mutex_);
		auto resourceEntry = Find(name);
		if(resourceEntry)
		{
			// increment retain count.
			uint32& resourceCounter = resourceEntry->second;
			++resourceCounter;
			//++resourceEntry->second;
			return true;
		}

		// Case 2: Resource is not loaded yet.
		auto pathsItr = paths_.find(name);
		if(pathsItr == paths_.end()) { return false; }
		std::unique_ptr<T> resource = std::make_unique<T>();
		if(!Load(resource.get(), pathsItr->second)) { return false; }
		resources_.emplace(name, std::make_pair(std::move(resource), 1));
		return true;
	}

	/********************************************************************************
	 * Decrement the resource's retain counter, or unload it when it's retain counter is 0.
	 ********************************************************************************/
	bool ReleaseResource(const string& name)
	{
		auto resourceEntry = Find(name);
		if(!resourceEntry){ return false; } // Resource doens't exist.
		uint32 resourceCounter = resourceEntry->second;
		--resourceCounter;
		if(resourceCounter == 0)
		{
			Unload(name);
		}
		return true;
	}

	void PurgeResources()
	{
		std::cout << "Purging all resources:" << std::endl;
		while(resources_.begin() != resources_.end())
		{
			string resourceName = resources_.begin()->first;
			std::cout << "Removing: ";
			std::cout << resourceName << std::endl;
			resources_.erase(resources_.begin());
		}
		std::cout << "Purging finished." << std::endl;
	}

protected:
	bool Load(T* resource, const string& path)
	{
		return static_cast<Derived*>(this)->Load(resource, path);
	}
private:
	ResourceEntry<T>* Find(const string& name)
	{
		auto itr = resources_.find(name);
		if(itr != resources_.end())
		{
			ResourceEntry<T>* resourceEntry = &itr->second;
			return resourceEntry;
		}
		else
		{
			return nullptr;
		}
	}

	/********************************************************************************
	 * Unload resource from memory.
	 ********************************************************************************/
	bool Unload(const string& name)
	{
		auto itr = resources_.find(name);
		if(itr == resources_.end()){ return false; }
		resources_.erase(itr);
		return true;
	}


	/********************************************************************************
	 * Load resources' filepaths from a cfg file.
	 ********************************************************************************/
	void LoadPathsFromFile(const string& pathsFileName)
	{
		std::ifstream pathsFile;
		pathsFile.open(Utils::GetMediaDirectory() + pathsFileName);
		if(pathsFile.is_open())
		{
			string line;
			while(std::getline(pathsFile, line))
			{
				std::stringstream keystream(line);
				string name;
				string path;
				keystream >> name;
				keystream >> path;
				paths_.emplace(name, path);
			}
			pathsFile.close();
			return;
		}
		std::cout << "Failed to load path file: " << pathsFileName << std::endl;
	}

	std::unordered_map<string, ResourceEntry<T>> resources_;
	std::unordered_map<string, string> paths_;
	sf::Mutex mutex_;
};

