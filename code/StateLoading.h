#pragma once
#include "BaseState.h"
#include "EventManager.h"
#include "Threads/FileLoader.h"
#include "GameSprite.h"


class StateLoading : public BaseState
{
public:
	StateLoading(StateManager* stateManager);
	~StateLoading();

	void OnCreate();
	void OnDestroy();

	void Activate();
	void Deactivate();

	void AddFileLoader(FileLoader* fileLoader);
	bool HasWork() const;

	void Update(const sf::Time& time);
	void Draw();
	void Proceed(EventDetails* eventDetails);

	void SetManualContinue(bool manualContinue);
	template<class T>
	void SetFinishedCallback(void(T::* func)(), T* instance)
	{
		finishedCallback_ = std::bind(func, instance);
	}
private:
	void UpdateText(const string& text, real32 percentage);
	real32 CalculateProgress();
	std::vector<FileLoader*> fileLoaders_;
	sf::Text sfText_;
	sf::Sprite progressBarFrame_;
	uint32 percentage_;
	uint32 numProgressBarPieces_;
	sf::Texture* progressBarPieceTexture_;
	sf::Texture* progressBarFrameTexture_;
	std::vector<sf::Sprite> progressBarPieces_;
	GameSprite* runningDoctor_;
	/// @todo rename originalWork -> number of fileLoaders to process
	uint32 originalWork_;
	bool manualContinue_;
	void DrawProgressBarPieces();
	// finished callback
	std::function<void()> finishedCallback_;
};
