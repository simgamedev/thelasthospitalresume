#include "GITileSelector.h"

#include "../GUI/GUIManager.h"
#include "../GUI/GUISprite.h"
#include "../GUI/GUIDropDownMenu.h"
#include "../GUI/GUIScrollbar.h"
#include "../EventManager.h"
#include "../GameStateTypes.h"
#include "../TextureManager.h"
#include "../TileMap/TileMap.h"

// ---> TEMP
#include "MapEditorController.h"
// ---< TEMP


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
GITileSelector::GITileSelector(EventManager* eventManager, GUIManager* guiManager, TextureManager* textureManager)
	: eventManager_(eventManager)
	, guiManager_(guiManager)
	, textureManager_(textureManager)
{
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_MouseClickOnTileSet",
							  &GITileSelector::TileSelect, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_MouseReleaseOnTileSet",
							  &GITileSelector::TileSelect, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_TileSelector_Close",
							  &GITileSelector::BtnClose, this);


	selectionBox_.setFillColor({168, 222, 114, 150});
	selectionBox_.setSize({TILE_SIZE, TILE_SIZE});
	int32 a = TILE_SIZE;
	selectionBox_.setPosition(0.0f, 0.0f);


	CreateGUI();
}


/********************************************************************************
 * Destructor.
 ********************************************************************************/
GITileSelector::~GITileSelector()
{
	eventManager_->RemoveCallback(GameStateType::MapEditor, "MapEditor_TileSelector_Click");
	eventManager_->RemoveCallback(GameStateType::MapEditor, "MapEditor_TileSelector_Release");
	eventManager_->RemoveCallback(GameStateType::MapEditor, "MapEditor_TileSelector_BtnClose");
	guiManager_->RemoveInterface(GameStateType::MapEditor, "TileSelector");
	/// @todo think carefuly
	if(!currentSheetTextureName_.empty())
	{
		textureManager_->ReleaseResource(currentSheetTextureName_);
	}
}


/********************************************************************************
 * Create GUI Programatically.
 ********************************************************************************/
void
GITileSelector::CreateGUI()
{
	/// @todo change to GUIWindow guiWindow = guiManager_->AddWindow();
	guiManager_->AddInterfaceDefault(GameStateType::MapEditor,
									 "TestInterface",
									 "Tile Selector",
									 1337, 81, 560, 600,
									 true,
									 true);
	interface_ = guiManager_->GetInterface(GameStateType::MapEditor,
										   "TestInterface");
	// GUISprite, GUIScrollBar
	tileSetSubInterface_ = guiManager_->ProduceInterface("TileSetSubInterface",
		                                                 vec2f(512 + 16, 512 + 16),
														 false,
													     false);
	tileSetSubInterface_->SetContentRectSize(vec2i(512, 512));
	/*
	guiManager_->AddButtonDefault(interface_,
								  "CloseButton",
								  vec2f(260, 550),
								  vec2f(128, 32),
								  "Close",
								  "Content");
								  */
	GUISprite* tileSetSprite = guiManager_->AddSpriteDefault(tileSetSubInterface_,
								                             "TileSetSprite",
								       					     vec2f(0, 0),
															 vec2f(512, 512),
															 "GrassyRPG");

	GUIScrollbar* vScrollBar = guiManager_->AddScrollBarDefault(tileSetSubInterface_,
																"VScrollBar",
																vec2f(256, 0),
																vec2f(16, 256),
																SliderDirection::Vertical);

	GUIScrollbar* hScrollBar = guiManager_->AddScrollBarDefault(tileSetSubInterface_,
																"HScrollBar",
																vec2f(0, 256),
																vec2f(256, 16),
																SliderDirection::Horizontal);
	GUIDropDownMenu* tileSetDropdownMenu = guiManager_->AddDropDownMenuDefault(interface_,
																		       "TileSetDropDown",
																		       vec2f(16, 560),
																		   	   vec2f(256, 32),
																		       "TileSets...");
	tileSetDropdownMenu->Setup();
	guiManager_->AddButtonDefault(interface_,
								  "CopyToBrush",
								  vec2f(280, 560),
								  vec2f(256, 32),
								  "Copy To Brush",
								  "Content");
	GUIEvent guiEvent;
	guiEvent.interfaceName = "TestInterface";
	guiEvent.elementName = "CopyToBrush";
	//binding = std::make_unique<Binding>(entryCallbackNamePrefix_ + "Click");
	/// @todo make a SetButtonCallback(GUIElement* element, GUIEventType::Hover, std::function);
	/// @todo change to smart ptr or figure out where to delete/new
	Binding* binding = new Binding("CopyToBrushBtnRelease");
	guiEvent.type = GUIEventType::Release;
	EventInfo eventInfo(guiEvent);
	binding->BindEvent(EventType::GUIRelease, eventInfo);
	eventManager_->AddBinding(binding);
	eventManager_->AddCallback(GameStateType::MapEditor,
							   "CopyToBrushBtnRelease",
							  &GITileSelector::CopyToBrushButtonReleased, this);
	/*
	eventManager_->AddCallback(GameStateType::MapEditor,
							   "CopyToBrushBtnRelease",
							   &GITileSelector::CopySelectionToBrush,
							   this);
	*/


	GUIVerticalDropDown* dropDown = static_cast<GUIDropDownMenu*>(interface_->GetElement("TileSetDropDown"))->GetDropDown();
	dropDown->PurgeEntries();
	auto files = Utils::GetFileList(Utils::GetMediaDirectory() + "Tilesets/" + "*.tileset");
	for(auto& itr : files)
	{
		string filename = itr.first;
		string filenameNoExtension = filename.substr(0, filename.find(".tileset"));
		dropDown->AddEntry(filenameNoExtension);
	}
	dropDown->Redraw();
	dropDown->SetEntryClickCallback(&GITileSelector::ChangeTileSet, this);


 

	guiManager_->AddSubInterfaceDefault(interface_, tileSetSubInterface_);
	tileSetSubInterface_->SetPosition(vec2f(16, 16));

	//SetSheetTexture("GrassyRPG");
}

void
GITileSelector::ChangeTileSet(const string& tileSetName)
{
	std::cout << "Should Change TileSet to" << tileSetName << std::endl;

	// ---> CHANGE guiTileSetSprite texture.
	textureManager_->RequireResource(tileSetName); /// @todo don't require it everytime
	tileSetSprite_.setTexture(*textureManager_->GetResource(tileSetName));
	auto textureSize = tileSetSprite_.getTexture()->getSize();
	tileSetSprite_.setTextureRect(sf::IntRect(0, 0, textureSize.x, textureSize.y));
	tileSetRenderTexture_.create(textureSize.x, textureSize.y);
	tileSetRenderTexture_.clear({ 0, 0, 0, 0 });
	tileSetRenderTexture_.draw(tileSetSprite_);
	tileSetRenderTexture_.display();
	GUISprite* guiTileSetSprite = (GUISprite*)tileSetSubInterface_->GetElement("TileSetSprite");
	guiTileSetSprite->SetTexture(tileSetRenderTexture_);
	guiTileSetSprite->SetScale(vec2f(3.0f, 3.0f));
	tileSetSubInterface_->AdjustContentSize();
	// ---< CHANGE

	// ---> RESET ScrollBar.
	GUIScrollbar* vScrollBar = (GUIScrollbar*)tileSetSubInterface_->GetElement("VScrollBar");
	GUIScrollbar* hScrollBar = (GUIScrollbar*)tileSetSubInterface_->GetElement("HScrollBar");
	vScrollBar->Reset();
	hScrollBar->Reset();
	// ---< RESET

	selectedTileSetName_ = tileSetName;
}







void
GITileSelector::SetSheetTexture(const string& textureName)
{
	/*
	std::cout << "SetSheetTexture!!!!!!!" << std::endl;
	/// @todo naming is too complicated??
	if(!currentSheetTextureName_.empty())
	{
		textureManager_->ReleaseResource(currentSheetTextureName_);
	}
	currentSheetTextureName_ = textureName;
	textureManager_->RequireResource(currentSheetTextureName_);
	sheetDisplaySprite_.setTexture(*textureManager_->GetResource(currentSheetTextureName_));
	sheetDisplaySprite_.setPosition({0.0f, 0.0f});
	vec2u textureSize = sheetDisplaySprite_.getTexture()->getSize();

	sheetRenderTexture_.create(textureSize.x, textureSize.y);
	sheetRenderTexture_.clear({0, 0, 0, 0});
	sheetRenderTexture_.draw(sheetDisplaySprite_);
	sheetRenderTexture_.display();

	GUISprite* interfaceSprite = static_cast<GUISprite*>(interface_->GetElement("TileSetSprite"));
	interfaceSprite->SetTexture(sheetRenderTexture_);
	*/
}



void
GITileSelector::UpdateInterface()
{
	tileSetRenderTexture_.clear({0, 0, 0, 0});
	tileSetRenderTexture_.draw(tileSetSprite_);
	tileSetRenderTexture_.draw(selectionBox_);
	tileSetRenderTexture_.display();

	interface_->RequestContentRedraw();
}


/********************************************************************************
 * Copy Selection To Brush.
 ********************************************************************************/
void 
GITileSelector::CopyToBrushButtonReleased(EventDetails* eventDetails)
{
	CopySelectionToBrush();
}

bool
GITileSelector::CopySelectionToBrush()
{
	if(!hasSelection_) { return false; }
	TileMap* brush = mapEditorController_->GetBrush();
	brush->Purge();
	vec2u selectedTilesStartPos = vec2u(mouseStartPos_) / static_cast<uint32>(TILE_SIZE);
	vec2u selectedTilesEndPos = vec2u(mouseEndPos_) / static_cast<uint32>(TILE_SIZE);
	vec2u selectionSize = selectedTilesEndPos - selectedTilesStartPos;

	brush->SetSize(selectionSize + vec2u(1, 1));

	vec2u selectedTileSetSize = textureManager_->GetResource(selectedTileSetName_)->getSize();
	uint32 numTilesPerRow = selectedTileSetSize.x / TILE_SIZE;

	//mapEditorController_->GetCurrentMap()->AddTileSet(selectedTileSetName_);
	TileSet* tileSet = mapEditorController_->GetTileSet(selectedTileSetName_);
	//mapEditorController_->GetCurrentMap()->AddTileSet(tileSet);

	uint32 brushTileX = 0;
	uint32 brushTileY = 0;
	for(uint32 x = selectedTilesStartPos.x;
		x <= selectedTilesEndPos.x;
		++x)
	{
		for(uint32 y = selectedTilesStartPos.y;
			y <= selectedTilesEndPos.y;
			++y)
		{
			uint32 tileID = (y * numTilesPerRow) + x;
			//Tile* tile = brush->SetTile(x, y, 0, currentTileSetName_, tileID);
			Tile* tile = brush->SetTile(brushTileX, brushTileY, 0, tileSet, tileID);
			if(!tile)
			{
				std::cout << "Failed to create tile in TileSelector::CopySelectionToBrush" << std::endl;
				++brushTileY;
				continue;
			}
			tile->solid = false;
			++brushTileY;
		}
		brushTileY = 0;
		++brushTileX;
	}
	mapEditorController_->SetTool(MapEditorTool::Brush);
}

/********************************************************************************
 * Tile Select.
 ********************************************************************************/
void
GITileSelector::TileSelect(EventDetails* eventDetails)
{
	std::cout << "TileSelect" << std::endl;
	if(eventDetails->bindingName == "MapEditor_MouseClickOnTileSet")
	{
		mouseStartPos_ = vec2f(eventDetails->mousePos);
		mouseEndPos_ = vec2f(eventDetails->mousePos);
		mouseStartPos_.x = mouseStartPos_.x / 3.0f;
		mouseStartPos_.y = mouseStartPos_.y / 3.0f;
		mouseEndPos_.x = mouseEndPos_.x / 3.0f;
		mouseEndPos_.y = mouseEndPos_.y / 3.0f;
		/// @todo false??
		hasSelection_ = false;
	}
	else
	{
		if(eventDetails->mousePos.x < 0 || eventDetails->mousePos.y < 0)
		{
			mouseEndPos_ = vec2f(0, 0);
			return;
		}
		mouseEndPos_ = vec2f(eventDetails->mousePos);
		mouseEndPos_.x = mouseEndPos_.x / 3.0f;
		mouseEndPos_.y = mouseEndPos_.y / 3.0f;
		hasSelection_ = true;
	}

	if(mouseStartPos_.x > mouseEndPos_.x)
	{
		std::swap(mouseStartPos_.x, mouseEndPos_.x);
	}
	if(mouseStartPos_.y > mouseEndPos_.y)
	{
		std::swap(mouseStartPos_.y, mouseEndPos_.y);
	}

	vec2i tileStart = vec2i(mouseStartPos_.x / TILE_SIZE, mouseStartPos_.y / TILE_SIZE);
	tileStart *= TILE_SIZE;
	vec2i tileEnd = vec2i(mouseEndPos_.x / TILE_SIZE, mouseEndPos_.y / TILE_SIZE);
	tileEnd *= TILE_SIZE;

	selectionBox_.setPosition(vec2f(tileStart));
	selectionBox_.setSize(vec2f(tileEnd - tileStart) + vec2f(TILE_SIZE, TILE_SIZE));
	UpdateInterface();
}


/********************************************************************************
 * Btn Close.
 ********************************************************************************/
void
GITileSelector::BtnClose(EventDetails* eventDetails)
{
	std::cout << "GITileSelector Close Button!!!" << std::endl;
	Hide();
}


/********************************************************************************
 * Show.
 ********************************************************************************/
void
GITileSelector::Show()
{
	interface_->SetActive(true);
	interface_->Focus();
}

/********************************************************************************
 * Hide.
 ********************************************************************************/
void
GITileSelector::Hide()
{
	interface_->SetActive(false);
}


/********************************************************************************
 * Is Active.
 ********************************************************************************/
bool
GITileSelector::IsActive() const
{
	return interface_->IsActive();
}

void
GITileSelector::SetMapEditorController(MapEditorController* controller)
{
	mapEditorController_ = controller;
}





