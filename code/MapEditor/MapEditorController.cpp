#include "MapEditorController.h"
#include "../SharedContext.h"
#include "../GameWorld/GameWorld.h"
#include "../EventManager.h"
#include "../Window.h"
#include "../GUI/GUIManager.h"
#include "../StateManager.h"
#include "../StateLoading.h"
#include "../StateMapEditor.h"
//#include "../GUI/GUIVerticalDropDownMenu.h"

// ---> TEMP
#include "../GUI/GUIButton.h"
#include "../GUI/GUILabel.h"
#include "../GUI/GUIToggleButton.h"
// ---< TEMP

// ---> magic_enum
#include "../magic_enum.hpp"
// ---< 


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
MapEditorController::MapEditorController(Window* window, StateManager* stateManager,
										 GameWorld* gameMap, sf::View& view)
	: window_(window)
	, stateManager_(stateManager)
	, currentMap_(gameMap)
	, view_(view)
	, currentTool_(MapEditorTool::None)
	, isInAction_(false)
	, isInSecondaryAction_(false)
	, isRightClickPan_(false)
	, zoom_(1.0f)
	, brush_(vec2u(1, 1))
	, onlyDrawSelectedLayers_(false)
{
	tileSelector_ = new GITileSelector(stateManager_->GetContext()->eventManager,
									   stateManager_->GetContext()->guiManager,
									   stateManager_->GetContext()->textureManager);
	// ---> TEMP
	tileSelector_->SetMapEditorController(this);
	// ---< TEMP
	tileSelector_->Hide();

	CreateOptionsGUI();
	options_->SetActive(false);
	/*
	mapEditorOptions_ = new GIMapEditorOptions(stateManager_->GetContext()->eventManager,
					 						   stateManager_->GetContext()->guiManager,
					 						   this,
					 						   tileSelector_,
					 						   currentMap_,
					 						   &brush_);
											   */
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->AddCallback(GameStateType::MapEditor, "Mouse_Left",
							  &MapEditorController::OnMouseClick, this);
	eventManager->AddCallback(GameStateType::MapEditor, "Mouse_Left_Release",
							  &MapEditorController::OnMouseRelease, this);
	eventManager->AddCallback(GameStateType::MapEditor, "Mouse_Right",
							  &MapEditorController::OnMouseClick, this);
	eventManager->AddCallback(GameStateType::MapEditor, "Mouse_Right_Release",
							  &MapEditorController::OnMouseRelease, this);
	eventManager->AddCallback(GameStateType::MapEditor, "Mouse_Wheel",
							  &MapEditorController::OnMouseWheel, this);
	eventManager->AddCallback(GameStateType::MapEditor, "Mouse_Moved",
							  &MapEditorController::OnMouseMove, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_NewMapDialogue_Create",
							  &MapEditorController::NewMapDialogueBtnCreate, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_NewMapDialogue_Close",
							  &MapEditorController::NewMapDialogueBtnClose, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_SelectPanTool",
							  &MapEditorController::ToolSelect, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_SelectBrushTool",
							  &MapEditorController::ToolSelect, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_SelectBucketTool",
							  &MapEditorController::ToolSelect, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_SelectEraserTool",
							  &MapEditorController::ToolSelect, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_SelectSelectionTool",
							  &MapEditorController::ToolSelect, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_SelectPaintBucketTool",
							  &MapEditorController::ToolSelect, this);
	eventManager->AddCallback(GameStateType::MapEditor, "MapEditor_Delete",
							  &MapEditorController::DeleteTiles, this);
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_CopySelection",
							  &MapEditorController::CopySelection,
							  this);
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_RemoveSelection",
							  &MapEditorController::RemoveSelection,
							  this);
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_ToggleSolidity",
							  &MapEditorController::ToggleSolidity,
							  this);
	eventManager->AddCallback(GameStateType::MapEditor, "Key_Escape",
							  &MapEditorController::OnEscPressed, this);
	

	// ---> GUI Toobox.
	GUIManager* guiManager = stateManager_->GetContext()->guiManager;
	guiManager->LoadInterfaceNew("MapEditorTools.interface", "MapEditorTools");
	toolBox_ = guiManager->GetInterface("MapEditorTools");
	toolBox_->SetPosition(vec2f(10.0f, 348.0f));
	toolBox_->SetActive(false);
	// ---<

	// ---> GUI New Map Dialogue.
	guiManager->LoadInterfaceNew("NewMapDialogue.interface", "NewMapDialogue");
	newMapDialogue_ = guiManager->GetInterface("NewMapDialogue");
	newMapDialogue_->PlaceAtCenterWindow();
	newMapDialogue_->SetActive(false);
	// ---<


	//brush_.SetTile(0, 0, 0, 0);
	brushDrawable_.setFillColor({255, 255, 255, 200});
	brushDrawable_.setOutlineColor({ 255, 0, 0, 255} );
	brushDrawable_.setOutlineThickness(-1.0f);
	mapBoundingBox_.setPosition({0.0f, 0.0f});
	mapBoundingBox_.setFillColor({0, 0, 0, 0});
	mapBoundingBox_.setOutlineColor({255, 50, 50, 255});
	mapBoundingBox_.setOutlineThickness(4.0f);

}

MapEditorController::~MapEditorController()
{
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->RemoveCallback(GameStateType::MapEditor, "Mouse_Left");
	eventManager->RemoveCallback(GameStateType::MapEditor, "Mouse_Left_Release");
	eventManager->RemoveCallback(GameStateType::MapEditor, "Mouse_Right");
	eventManager->RemoveCallback(GameStateType::MapEditor, "Mouse_Right_Release");
	eventManager->RemoveCallback(GameStateType::MapEditor, "Mouse_Wheel");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_NewMapDialogue_BtnCreate");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_NewMapDialogue_BtnClose");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_SelectPanTool");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_SelectBrushTool");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_SelectBucketTool");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_SelectEraserTool");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_SelectSelectionTool");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_Delete");

	GUIManager* guiManager = stateManager_->GetContext()->guiManager;
	guiManager->RemoveInterface(GameStateType::MapEditor, "MapEditorTools");
	guiManager->RemoveInterface(GameStateType::MapEditor, "NewMapDialogue");
}


void
MapEditorController::Update(real32 dt)
{
	//mapBoundingBox_.setSize(vec2f(currentMap_->GetTileMap()->GetSize() * static_cast<uint32>(TILE_SIZE)));
	UpdateMouse();
	if(currentTool_ == MapEditorTool::Pan || isRightClickPan_)
	{
		ToolPanUpdate();
	}
	else if(currentTool_ == MapEditorTool::Brush)
	{
		ToolBrushUpdate();
	}
	else if(currentTool_ == MapEditorTool::Bucket)
	{
		ToolBucketUpdate();
	}
	else if(currentTool_ == MapEditorTool::Eraser)
	{
		ToolEraserUpdate();
	}
	else if(currentTool_ == MapEditorTool::Selection)
	{
		ToolSelectionUpdate();
	}
}


/********************************************************************************
 * Draw.
 ********************************************************************************/
void
MapEditorController::Draw(sf::RenderWindow* renderWindow)
{
	renderWindow->draw(mapBoundingBox_);
	if(currentTool_ == MapEditorTool::Brush)
	{
		renderWindow->draw(brushDrawable_);
	}
	if (currentTool_ == MapEditorTool::Selection)
	{
		renderWindow->draw(selectionDrawable_);
	}
	//mapEditorOptions_->Draw(renderWindow);
}


/********************************************************************************
 * Tools Update.
 ********************************************************************************/
void
MapEditorController::ToolPanUpdate()
{
	if(!isInAction_ && !isRightClickPan_)
	{
		return;
	}

	if(mouseDifference_ == vec2f(0.0f, 0.0f))
	{
		return;
	}

	view_.setCenter(view_.getCenter() + (vec2f(0.0f, 0.0f) - vec2f(mouseDifference_)));
}

void
MapEditorController::ToolBrushUpdate()
{
	vec2f tileWorldPos = vec2f(mouseTilePos_.x * TILE_SIZE,
							   mouseTilePos_.y * TILE_SIZE);
	brushDrawable_.setPosition(tileWorldPos);
	PlaceBrushTiles();
}

void
MapEditorController::ToolBucketUpdate()
{
	if(!isInAction_ || !isInSecondaryAction_) { return; }
	std::cout << "Should Paint!!!!" << std::endl;
	// ---> Find continous identicle tiles(empty tile == empty tile)
	// ---> Plot Single-Tiled brush to all the tiles
	vec2u mapSize = currentMap_->GetMapSize();
	for (int32 x = 0; x < mapSize.x; ++x)
	{
		for (int32 y = 0; y < mapSize.y; ++y)
		{
			currentMap_->GetTileMap()->PlotBrush(brush_,
												 vec2i(x, y),
												 GetLowestSelectedLayer());
		}
	}
	// ---> redraw map
	currentMap_->Redraw();
	isInSecondaryAction_ = false;
}

void
MapEditorController::ToolEraserUpdate()
{
	if(!isInAction_ || !isInSecondaryAction_) { return; }
	currentMap_->GetTileMap()->RemoveTile(mouseTilePos_.x,
										  mouseTilePos_.y,
										  GetLowestSelectedLayer());
	currentMap_->Redraw();
	isInSecondaryAction_ = false;
}

void
MapEditorController::ToolSelectionUpdate()
{
	if (!isInAction_) { return;  }
	//mapEditorOptions_->Update();
	// TODO: tilepos > mapSize
	if (mouseTileStartPos_.x < 0 ||
		mouseTileStartPos_.y < 0 ||
		mouseTilePos_.x < 0 ||
		mouseTilePos_.y < 0)
	{
		return;
	}
	vec2f selectionStartWorld = vec2f(
		(mouseTileStartPos_.x + (mouseTileStartPos_.x > mouseTilePos_.x ? 1 : 0)) * TILE_SIZE,
		(mouseTileStartPos_.y + (mouseTileStartPos_.y > mouseTilePos_.y ? 1 : 0)) * TILE_SIZE
	);
	vec2f selectionEndWorld = vec2f(
		(mouseTilePos_.x + (mouseTileStartPos_.x <= mouseTilePos_.x ? 1 : 0)) * TILE_SIZE,
		(mouseTilePos_.y + (mouseTileStartPos_.y <= mouseTilePos_.y ? 1 : 0)) * TILE_SIZE
	);
	selectionDrawable_.setPosition(
		(selectionStartWorld.x <= selectionEndWorld.x ? selectionStartWorld.x : selectionEndWorld.x),
		(selectionStartWorld.y <= selectionEndWorld.y ? selectionStartWorld.y : selectionEndWorld.y)
	);
	selectionDrawable_.setFillColor(sf::Color(255, 0, 0, 55));
	selectionDrawable_.setSize({std::abs(selectionEndWorld.x - selectionStartWorld.x),
								std::abs(selectionEndWorld.y - selectionStartWorld.y)});

	selectionRangeX_ = vec2i(
		std::min(mouseTileStartPos_.x, mouseTilePos_.x),
		std::max(mouseTileStartPos_.x, mouseTilePos_.x)
	);

	selectionRangeY_ = vec2i(
		std::min(mouseTileStartPos_.y, mouseTilePos_.y),
		std::max(mouseTileStartPos_.y, mouseTilePos_.y)
	);

}

/********************************************************************************
 * Redraw brush.
 ********************************************************************************/
void
MapEditorController::RedrawBrush()
{
	// ---> Prepare
	vec2u brushSize = brush_.GetSize();
	vec2u brushPixelSize = brushSize * static_cast<uint32>(TILE_SIZE);
	vec2u textureSize = brushRenderTexture_.getSize();
	if(brushPixelSize.x != textureSize.x || brushPixelSize.y != textureSize.y)
	{
		if(!brushRenderTexture_.create(brushPixelSize.x, brushPixelSize.y))
		{
			std::cout << "Failed to create brush render texture!" << std::endl;
		}
	}

	// ---> Draw
	brushRenderTexture_.clear({0, 0, 0, 0});

	for(uint32 x = 0; x < brushSize.x; ++x)
	{
		for(uint32 y = 0; y < brushSize.y; ++y)
		{
			for(uint32 layer = 0; layer < MAX_MAP_LAYERS; ++layer)
			{
				Tile* tile = brush_.GetTile(x, y, layer);
				if(!tile) { continue; }
				TileInfo* tileInfo = tile->tileInfo;
				if(!tileInfo) { continue; }
				tileInfo->GetSprite().setPosition(vec2f(x * TILE_SIZE, y * TILE_SIZE));
				brushRenderTexture_.draw(tileInfo->GetSprite());
			}
		}
	}

	// ---> Display.
	brushRenderTexture_.display();
	/// @todo so you mean that sf::RectangleShape could be used like a sprite??
	brushDrawable_.setTexture(&brushRenderTexture_.getTexture());
	brushDrawable_.setSize(vec2f(brushPixelSize));
	brushDrawable_.setTextureRect(sf::IntRect(vec2i(0, 0), vec2i(brushPixelSize)));
}


void
MapEditorController::OnMouseRelease(EventDetails* eventDetails)
{
	// ---> right button release
	if(eventDetails->keycode != sf::Mouse::Left)
	{
		isRightClickPan_ = false;
	}
	// ---> left button release
   	isInAction_ = false;
	isInSecondaryAction_ = false;
	//mapEditorOptions_->MouseRelease();

	// ---> SOLIDITY
	if (currentTool_ == MapEditorTool::Selection)
	{
		bool solid = false;
		bool mixed = false;
		uint32 numChanges = 0;
		for (auto x = selectionRangeX_.x; x <= selectionRangeX_.y; ++x)
		{
			for (auto y = selectionRangeY_.x; y <= selectionRangeY_.y; ++y)
			{
				for (auto layer = lowestSelectedLayer_; layer <= highestSelectedLayer_; ++layer)
				{
					Tile* tile = currentMap_->GetTile(x, y, layer);
					if (!tile) { continue; }
					if (tile->solid && !solid) { solid = true; ++numChanges; }
					else if (!tile->solid && solid) { solid = false; ++numChanges; }
					if (numChanges >= 2) { mixed = true; }
				}
			}
		}
		GUIToggleButton* solidityCheckBox = (GUIToggleButton*)options_->GetElement("SolidityCheckBox");
		if (mixed)
		{
			std::cout << "Options Update Mixed" << std::endl;
		}
		else if (solid)
		{
			std::cout << "Options Update All Solid" << std::endl;
			solidityCheckBox->Check();
			//options_->GetElement("")
		}
		else
		{
			std::cout << "Options Update All NOT!! Solid" << std::endl;
			solidityCheckBox->UnCheck();
		}
	}
	// ---< SOLIDITY
}


void
MapEditorController::PlaceBrushTiles()
{
	if(!isInAction_ || !isInSecondaryAction_) { return; }
	// ---> place brush
	currentMap_->GetTileMap()->PlotBrush(brush_,
										 mouseTilePos_,
										 GetLowestSelectedLayer());
	// ---<

	// ---> redraw map
	vec2u brushSize = brush_.GetSize();
	vec3i from = vec3i(mouseTilePos_.x,
					   mouseTilePos_.y,
					   GetLowestSelectedLayer());
	vec3i to = vec3i(mouseTilePos_.x + brushSize.x - 1,
					 mouseTilePos_.y + brushSize.y - 1,
					 GetHighestSelectedLayer());
	currentMap_->Redraw(from, to);
	// ---<
	/// @todo set it to false in order to avoid multiple placements???
	isInSecondaryAction_ = false;
}


void
MapEditorController::OnMouseClick(EventDetails* eventDetails)
{
	if(eventDetails->hasBeenProcessed) { return; }
	std::cout << "MapEditorController received mouse click!" << std::endl;
	// --> right click.
	if(eventDetails->keycode != sf::Mouse::Left)
	{
		isRightClickPan_ = true;
		return;
	}


	/// @todo make a final decision with EventManager::GetMousePos() OR  EventDetails->mousePos
	mousePos_ = eventDetails->mousePos;
	mouseStartPos_ = mousePos_;
	// TOODO: how come window_->GetRenderWindow()->getView() != view_????
	//auto view = window_->GetRenderWindow()->getView();
	auto viewPos = view_.getCenter() - (view_.getSize() * 0.5f);
	auto mouseWorld = viewPos + (vec2f(mousePos_) * zoom_);
	mouseTileStartPos_ = vec2i(floor(mouseWorld.x / TILE_SIZE),
							   floor(mouseWorld.y / TILE_SIZE));

	//if(!options_->OnMouseClick(mouseWorld)) { return; }
	isInAction_ = true;
	isInSecondaryAction_ = true;
}

void
MapEditorController::ResetTools()
{
	mouseTilePos_ = vec2i(-1, -1);
	mouseTileStartPos_ = vec2i(-1, -1);
	//mapEditorOptions_->Reset();
	//tileSelector_->Hide();
}


void
MapEditorController::OnMouseWheel(EventDetails* eventDetails)
{
	std::cout << "MapEditorController::OnMouseWheel" << std::endl;
	/// @todo what does zoom_'s value mean?
	if(eventDetails->hasBeenProcessed) { return; }
	real32 factor = 0.05f;
	factor *= eventDetails->mouseWheelDelta;
	factor = 1.0f - factor;
	view_.zoom(factor);
	zoom_ *= factor;
}


/********************************************************************************
 * Select Tool From Toolbox.
 ********************************************************************************/
void
MapEditorController::ToolSelect(EventDetails* eventDetails)
{
	MapEditorTool tool = MapEditorTool::None;
	if(eventDetails->bindingName == "MapEditor_SelectPanTool")
	{
		tool = MapEditorTool::Pan;
	}
	else if(eventDetails->bindingName == "MapEditor_SelectBrushTool")
	{
		tool = MapEditorTool::Brush;
	}
	else if(eventDetails->bindingName == "MapEditor_SelectPaintBucketTool")
	{
		tool = MapEditorTool::Bucket;
	}
	else if(eventDetails->bindingName == "MapEditor_SelectEraserTool")
	{
		tool = MapEditorTool::Eraser;
	}
	else if(eventDetails->bindingName == "MapEditor_SelectSelectionTool")
	{
		tool = MapEditorTool::Selection;
	}

	SetTool(tool);
}


void
MapEditorController::NewMapDialogueBtnCreate(EventDetails* eventDetails)
{
	string sizeXstr = newMapDialogue_->GetElement("Size_X")->GetText();
	string sizeYstr = newMapDialogue_->GetElement("Size_Y")->GetText();

	StateMapEditor* stateMapEditor = stateManager_->GetState<StateMapEditor>(GameStateType::MapEditor);
	if (currentMap_ != nullptr)
	{
		delete currentMap_;
	}
	currentMap_ = new GameWorld(stateManager_->GetContext()->window);
	stateMapEditor->currentMap_ = currentMap_;
	currentMap_->SetStateManager(stateManager_);
	currentMap_->GetTileMap()->SetSize(vec2u(std::stoi(sizeXstr),
											 std::stoi(sizeYstr)));
	currentMap_->GetDefaultTileInfo()->SetFriction(vec2f(1.0f, 1.0f));
	mapBoundingBox_.setSize(vec2f(currentMap_->GetTileMap()->GetSize() * static_cast<uint32>(TILE_SIZE)));

	newMapDialogue_->SetActive(false);
	// show tile selector
	tileSelector_->Show();
	// show tool box interface
	toolBox_->SetActive(true);
	// show options interface
	options_->SetActive(true);
}


void
MapEditorController::NewMapDialogueBtnClose(EventDetails* eventDetails)
{
	newMapDialogue_->SetActive(false);
}

/********************************************************************************
 * Set Tool.
 ********************************************************************************/
void
MapEditorController::SetTool(MapEditorTool tool)
{
	ResetTools();
	currentTool_ = tool;
	if(currentTool_ == MapEditorTool::Brush)
	{
		RedrawBrush();
	}
	LOG_ENGINE_ERROR(magic_enum::enum_name(currentTool_));
	//mapEditorOptions_->SetTool(currentTool_);
}

/*********************************************************************************
 * Update Mouse.
 *********************************************************************************/
void
MapEditorController::UpdateMouse()
{
	/// @todo fix eventManager::GetMousePos();
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	vec2i newMousePos = eventManager->GetMousePos();
	mouseDifference_ = vec2f(newMousePos - mousePos_);
	mouseDifference_ *= zoom_;
	mousePos_ = newMousePos;

	//sf::View view = window_->GetRenderWindow()->getView();
	vec2f viewPos = view_.getCenter() - (view_.getSize() * 0.5f);
	vec2f mouseWorldPos = viewPos + (vec2f(mousePos_) * zoom_);
	vec2i newMouseTilePosition = vec2i(floor(mouseWorldPos.x / TILE_SIZE),
									   floor(mouseWorldPos.y / TILE_SIZE));
	if(mouseTilePos_ != newMouseTilePosition && isInAction_)
	{
		isInSecondaryAction_ = true;
	}

	mouseTilePos_ = newMouseTilePosition;
}

/*********************************************************************************
 * On Mouse Move.
 *********************************************************************************/
void
MapEditorController::OnMouseMove(EventDetails* eventDetails)
{
	sf::View view = window_->GetRenderWindow()->getView();
	vec2f viewPos = view.getCenter() - (view.getSize() * 0.5f);

	vec2f tileWorldPos = vec2f(mouseTilePos_.x * TILE_SIZE,
							   mouseTilePos_.y * TILE_SIZE);
	//brushDrawable_.setPosition(tileWorldPos);
}


void
MapEditorController::ShowNewMapDialogue()
{
	newMapDialogue_->SetActive(true);
}

void
MapEditorController::SetTileSheetTexture(const string& textureName)
{
}

MapEditorTool
MapEditorController::GetCurrentTool() const
{
	return currentTool_;
}

bool
MapEditorController::IsInAction() const
{
	return isInAction_;
}

bool
MapEditorController::IsInSecondaryAction() const
{
	return isInSecondaryAction_;
}
	

/*
GIMapEditorOptions*
MapEditorController::GetMapEditorOptions()
{
	return mapEditorOptions_;
}
*/

vec2i
MapEditorController::GetMouseTileStartPos() const
{
	return mouseTileStartPos_;
}

vec2i
MapEditorController::GetMouseTilePos() const
{
	return mouseTilePos_;
}

vec2f
MapEditorController::GetMouseDifference() const
{
	return mouseDifference_;
}

bool
MapEditorController::IsOnlyDrawSelectedLayers() const
{
	return onlyDrawSelectedLayers_;
}

void
MapEditorController::ToggleOnlyDrawSelectedLayers(EventDetails* eventDetails)
{
	onlyDrawSelectedLayers_ = !onlyDrawSelectedLayers_;
}

/*********************************************************************************
 * Toggle Solidity.
 *********************************************************************************/
void
MapEditorController::ToggleSolidity(EventDetails* eventDetails)
{
	if (currentTool_ != MapEditorTool::Brush && 
		currentTool_ != MapEditorTool::Selection) { return; }
	std::cout << "solidity toggled!" << std::endl;
	// >>>>>>>>>> Toggle Brush Tiles Solidity
	if (currentTool_ == MapEditorTool::Brush)
	{
		vec2u start = vec2u(0, 0);
		vec2u end = brush_.GetSize() - vec2u(1, 1);
		for (uint32 x = start.x; x <= end.x; ++x)
		{
			for (uint32 y = start.y; y <= end.y; ++y)
			{
				// TODO: FIXME: toggle solidity not working
				for (auto layer = 0; layer <= MAX_MAP_LAYERS; ++layer)
				{
					Tile* tile = brush_.GetTile(x, y, layer);
					if (!tile) { continue; }
					tile->solid = true;
				}
			}
		}
	}
	// <<<<<<<<<<

	// >>>>>>>>>> Toggle Selected Tiles Solidity
	if (currentTool_ == MapEditorTool::Selection)
	{
		for (int32 x = selectionRangeX_.x; x <= selectionRangeX_.y; ++x)
		{
			for (int32 y = selectionRangeY_.x; y <= selectionRangeY_.y; ++y)
			{
				Tile* tile = currentMap_->GetTile(x, y, GetLowestSelectedLayer());
				if (!tile) continue;
				tile->solid = !tile->solid;
				LOG_ENGINE_ERROR("Should be working");
			}
		}
		GUIToggleButton* solidityCheckBox = (GUIToggleButton*)options_->GetElement("SolidityCheckBox");
		solidityCheckBox->Toggle();
	}
	// <<<<<<<<<<
}


/********************************************************************************
 * Delete Tiles.
 ********************************************************************************/
void
MapEditorController::DeleteTiles(EventDetails* eventDetails)
{
	if(currentTool_ != MapEditorTool::Selection)
	{
		//mapEditorOptions_->RemoveSelection(eventDetails);
	}
}

/*********************************************************************************
 * Create Options GUI.
 *********************************************************************************/
void
MapEditorController::CreateOptionsGUI()
{
	GUIManager* guiManager = stateManager_->GetContext()->guiManager;
	guiManager->LoadInterfaceNew("MapEditorOptions.interface", "MapEditorOptions");
	options_ = guiManager->GetInterface("MapEditorOptions");
	options_->SetPosition(vec2f(10.0f, 848.0f));
	options_->SetActive(false);

	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_LayerPlus",
							  &MapEditorController::OnLayerElevation,
							  this);
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_LayerMinus",
							  &MapEditorController::OnLayerElevation,
							  this);
	/*
	GUIManager* guiManager = stateManager_->GetContext()->guiManager;
	guiManager->AddInterfaceDefault(GameStateType::MapEditor,
									"MapEditorOptions",
									"Options",
									10, 32,
									200, 284,
									true,
									true);
	options_ = guiManager->GetInterface("MapEditorOptions");
	GUIButton* layerMinusBtn = guiManager->AddButtonDefault(options_,
															"LayerMinus",
															vec2f(72, 8),
														    vec2f(32, 32),
														    "M",
															"Content");
	layerMinusBtn->SetTextPadding(vec2f(16, 16));
	GUIButton* layerPlusBtn = guiManager->AddButtonDefault(options_,
								                           "LayerPlus",
								                           vec2f(166, 8),
								                           vec2f(32, 32),
								                           "P",
								                           "Content");
	layerPlusBtn->SetTextPadding(vec2f(16, 16));
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_LayerPlus",
							  &MapEditorController::OnLayerElevation,
							  this);
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_LayerMinus",
							  &MapEditorController::OnLayerElevation,
							  this);
	GUILabel* layerLabel = guiManager->AddLabelDefault(options_,
													   "LayerLabel",
													   vec2f(4, 12),
													   vec2f(32, 32),
													   "Layer:");

	GUILabel* layer = guiManager->AddLabelDefault(options_,
												  "Layer",
												  vec2f(112, 12),
												  vec2f(32, 32),
												  "0");
	GUICheckBox* solidityCheckBox = guiManager->AddCheckBoxDefault(options_,
																   "SolidityCheckBox",
																   vec2f(74, 56),
																   vec2f(32, 32));
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_ToggleSolidity",
							  &MapEditorController::ToggleSolidity,
							  this);
	GUILabel* solidityLabel = guiManager->AddLabelDefault(options_,
														  "SolidityLabel",
														  vec2f(4, 56),
														  // TODO: GUILabelSize should only be Font Size
														  vec2f(32, 32),
														  "Solid:");

	GUILabel* drawSelectedLayerLabel = guiManager->AddLabelDefault(options_,
																   "DrawSelectedLayerLabel",
																   vec2f(4, 100),
																   vec2f(32, 32),
																   "Draw Selected Layer:");
	GUICheckBox* drawSelectedLayerCheckBox = guiManager->AddCheckBoxDefault(options_,
																			"DrawSelectedLayerCheckBox",
																			vec2f(74, 128),
																			vec2f(32, 32));
	GUIButton* copySelectionBtn = guiManager->AddButtonDefault(options_,
															   "CopySelection",
															   vec2f(32, 168),
															   vec2f(128, 32),
															   "Copy",
															   "Content");
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_CopySelection",
							  &MapEditorController::CopySelection,
							  this);
	GUIButton* removeSelectionBtn = guiManager->AddButtonDefault(options_,
																 "RemoveSelection",
																 vec2f(32, 204),
																 vec2f(128, 32),
																 "Remove",
																 "Content");
	eventManager->AddCallback(GameStateType::MapEditor,
							  "MapEditor_RemoveSelection",
							  &MapEditorController::RemoveSelection,
							  this);

							  */
}

/*********************************************************************************
 * Get Brush.
 *********************************************************************************/
TileMap*
MapEditorController::GetBrush()
{
	return &brush_;
}

/*********************************************************************************
 * Get Current Map.
 *********************************************************************************/
GameWorld*
MapEditorController::GetCurrentMap() const
{
	return currentMap_;
}

/*********************************************************************************
 * Get TileSet.
 *********************************************************************************/
TileSet*
MapEditorController::GetTileSet(const string& tileSetName) const
{
	StateMapEditor* stateMapEditor = stateManager_->GetState<StateMapEditor>(GameStateType::MapEditor);
	TileSet* tileSet = stateMapEditor->GetTileSet(tileSetName);
	return tileSet;
}

/*********************************************************************************
 * On ESC Pressed.
 *********************************************************************************/
void
MapEditorController::OnEscPressed(EventDetails* eventDetails)
{
	if (currentTool_ != MapEditorTool::None)
	{
		SetTool(MapEditorTool::None);
	}
}

/*********************************************************************************
 * Get Lowest Selected Layer.
 *********************************************************************************/
int32
MapEditorController::GetLowestSelectedLayer() const
{
	return lowestSelectedLayer_;
}
/*********************************************************************************
 * Get Highest Selected Layer.
 *********************************************************************************/
int32
MapEditorController::GetHighestSelectedLayer() const
{
	return highestSelectedLayer_;
}

/*********************************************************************************
 * On Layer Elveation.
 *********************************************************************************/
void
MapEditorController::OnLayerElevation(EventDetails* eventDetails)
{
	std::cout << "On Layer Elevation" << std::endl;
	int32 lowDelta = 0;
	int32 highDelta  = 0;
	bool shiftKeyPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::LShift);
	if (eventDetails->bindingName == "MapEditor_LayerPlus")
	{
		if (shiftKeyPressed)
		{
			highDelta = 1;
		}
		else
		{
			lowDelta = 1;
		}
	}
	else if (eventDetails->bindingName == "MapEditor_LayerMinus")
	{
		if (shiftKeyPressed)
		{
			highDelta = -1;
		}
		else
		{
			lowDelta = -1;
		}
	}

	// ---> BRUSH TOOL
	if (currentTool_ == MapEditorTool::Brush ||
		currentTool_ == MapEditorTool::Eraser ||
		currentTool_ == MapEditorTool::Bucket)
	{
		if (highDelta != 0) { return; } // When using brush, we don't allow multiple layer selection.
		int32 newLowestSelectedLayer = lowestSelectedLayer_ + lowDelta;
		if (newLowestSelectedLayer < 0 ||
			newLowestSelectedLayer >= MAX_MAP_LAYERS)
		{
			return;
		}
		lowestSelectedLayer_ = newLowestSelectedLayer;
		highestSelectedLayer_ = newLowestSelectedLayer + brush_.GetHighestElevation();
		std::cout << "lowestSelectedLayer_ = " << lowestSelectedLayer_ << std::endl;
		UpdateOptionsGUI();
	}
	// ---< BRUSH TOOL

	// ---> SELECTION TOOL
	if (currentTool_ == MapEditorTool::Selection)
	{
		int32 newLowest = lowestSelectedLayer_ + lowDelta;
		int32 newHighest = highestSelectedLayer_ + highDelta;
		if (newLowest < 0 || newLowest >= MAX_MAP_LAYERS) { return; }
		if (newHighest < 0 || newHighest >= MAX_MAP_LAYERS) { return; }
		if (lowestSelectedLayer_ == highestSelectedLayer_ && !shiftKeyPressed)
		{
			lowestSelectedLayer_ += lowDelta;
			highestSelectedLayer_ += highDelta;
			highestSelectedLayer_ = lowestSelectedLayer_;
		}
		else
		{
			lowestSelectedLayer_ = newLowest;
			highestSelectedLayer_ = newHighest;
		}
		if (lowestSelectedLayer_ > highestSelectedLayer_)
		{
			std::swap(lowestSelectedLayer_, highestSelectedLayer_);
		}
		UpdateOptionsGUI();
	}
	// ---< SELECTION TOOL
}

/*********************************************************************************
 * Update Options
 *********************************************************************************/
void
MapEditorController::UpdateOptionsGUI()
{
	GUILabel* layer = (GUILabel*)options_->GetElement("Layer");
	layer->SetText(
		std::to_string(lowestSelectedLayer_) +
		(lowestSelectedLayer_ != highestSelectedLayer_ ? " - " + std::to_string(highestSelectedLayer_): "")
	);
	options_->RequestContentRedraw();
}

/*********************************************************************************
 * Copy Selection.
 *********************************************************************************/
void 
MapEditorController::CopySelection(EventDetails* eventDetails)
{
	if (selectionRangeX_.x == -1) { return;  } // No selection.
	std::cout << "Should Copy Selection!" << std::endl;
	vec2u selectionSize = vec2u(selectionRangeX_.y - selectionRangeX_.x,
							    selectionRangeY_.y - selectionRangeY_.x);
	selectionSize.x += 1;
	selectionSize.y += 1;
	brush_.Purge();
	brush_.SetSize(selectionSize);
	uint32 brushX = 0;
	uint32 brushY = 0;
	uint32 brushLayer = 0;
	bool solid = false; // previous to copy tile is solid
	bool mixedSolidity = false;
	uint32 solidityChanges = 0;
	for (auto x = selectionRangeX_.x; x <= selectionRangeX_.y; ++x)
	{
		for (auto y = selectionRangeY_.x; y <= selectionRangeY_.y; ++y)
		{
			for (auto layer = lowestSelectedLayer_; layer <= highestSelectedLayer_; ++layer)
			{
				Tile* tileToCopy = currentMap_->GetTile(x, y, layer);
				if (!tileToCopy) { ++brushLayer; continue; }
				Tile* brushTile = brush_.SetTile(brushX, brushY, brushLayer, tileToCopy);
				if (!brushTile)
				{ 
					std::cout << "Failed to copy selection to brush" << std::endl;
					++brushLayer;
					continue;
				}
				if (!mixedSolidity)
				{
					if (tileToCopy->solid && !solid) { solid = true; ++solidityChanges; }
					else if (solid) { solid = false; ++solidityChanges; }
					if (solidityChanges >= 2) { mixedSolidity = true;  }
				}
				*brushTile = *tileToCopy;
				++brushLayer;
			}
			brushLayer = 0;
			++brushY;
		}
		brushY = 0;
		++brushX;
	}
	highestSelectedLayer_ = lowestSelectedLayer_ + brush_.GetHighestElevation();
	if (highestSelectedLayer_ >= MAX_MAP_LAYERS)
	{
		int32 difference = (highestSelectedLayer_ - MAX_MAP_LAYERS) + 1;
		highestSelectedLayer_ = MAX_MAP_LAYERS - 1;
		lowestSelectedLayer_ -= difference;
	}
	UpdateOptionsGUI();
	SetTool(MapEditorTool::Brush);
	// TODO: set solidity label
}

/*********************************************************************************
 * Remove Selection.
 *********************************************************************************/
void
MapEditorController::RemoveSelection(EventDetails* eventDetails)
{
	if (selectionRangeX_.x == -1) { return; }
	std::cout << "Should Remove Selection!" << std::endl;
	bool shiftKeyPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::LShift);
	vec2u layerRange;
	if (shiftKeyPressed)
	{
		layerRange = vec2u(0, MAX_MAP_LAYERS - 1);
	}
	else
	{
		layerRange = vec2u(lowestSelectedLayer_, highestSelectedLayer_);
	}
	currentMap_->GetTileMap()->RemoveTiles(vec2u(selectionRangeX_),
										   vec2u(selectionRangeY_),
										   layerRange);
	currentMap_->ClearRenderTexture(vec3i(selectionRangeX_.x, selectionRangeY_.x, layerRange.x),
									vec3i(selectionRangeX_.y, selectionRangeY_.y, layerRange.y));
}

/*********************************************************************************
 * Set Current Map
 *********************************************************************************/
void
MapEditorController::SetCurrentMap(GameWorld* currentMap)
{
	currentMap_ = currentMap;
}


