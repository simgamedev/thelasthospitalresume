#pragma once
#include "../common.h"
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

class GUIManager;
class GUIInterface;
class EventManager;
struct EventDetails;
class TextureManager;
class TileMap;
class MapEditorController;

class GITileSelector
{
public:
	GITileSelector(EventManager* eventManager, GUIManager* guiManager, TextureManager* textureManager);
	~GITileSelector();

	void Show();
	void Hide();

	bool IsActive() const;
	void SetSheetTexture(const string& textureName);

	void UpdateInterface();
	bool CopySelectionToBrush();
	void CopyToBrushButtonReleased(EventDetails* eventDetails);

	void TileSelect(EventDetails* eventDetails);
	void BtnClose(EventDetails* eventDetails);
	void SheetSelectionDropDown();
	void SetMapEditorController(MapEditorController* controller);
	void ChangeTileSet(const string& tileSetName);
	// ---> TEMP
	void CreateGUI();
	// ---< TEMP
private:
	EventManager* eventManager_;
	GUIManager* guiManager_;
	TextureManager* textureManager_;

	//GUIInterface* interface_;
	sf::RenderTexture tileSetRenderTexture_;
	sf::Sprite tileSetSprite_;
	sf::RectangleShape selectionBox_;

	string currentSheetName_;
	string currentSheetTextureName_;

	vec2f mouseStartPos_;
	vec2f mouseEndPos_;

	bool hasSelection_;
	//string currentTileSetName_;
	// ---> TEMP
	GUIInterface* interface_;
	GUIInterface* tileSetSubInterface_;
	MapEditorController* mapEditorController_;
	string selectedTileSetName_;
	// ---< TEMP
};

