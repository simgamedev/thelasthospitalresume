#pragma once
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include "GITileSelector.h"
//#include "GIMapEditorOptions.h"
#include "../GameWorld/GameWorld.h"
//#include "../Map/TileMap.h"

class Window;
class EventManager;
struct EventDetails;
class GUIManager;
class GUIInterface;
class EntityManager;
class StateManager;


enum MapEditorTool { None, Pan, Brush, Bucket, Eraser, Selection };


class MapEditorController
{
public:
	MapEditorController(Window* window, StateManager* stateManager,
						GameWorld* gameMap, sf::View& view);
	~MapEditorController();

	void Update(real32 dt);
	void Draw(sf::RenderWindow* renderWindow);

	void ShowNewMapDialogue();
	void SetTileSheetTexture(const string& textureName);
	MapEditorTool GetCurrentTool() const;
	GameWorld* GetCurrentMap() const;
	TileSet* GetTileSet(const string& tileSetName) const;
	bool IsInAction() const;
	bool IsInSecondaryAction() const;
	//GIMapEditorOptions* GetMapEditorOptions();

	vec2i GetMouseTileStartPos() const;
	vec2i GetMouseTilePos() const;
	vec2f GetMouseDifference() const;

	bool IsOnlyDrawSelectedLayers() const;
	void ToggleOnlyDrawSelectedLayers(EventDetails* eventDetails);
	void ToggleSolidity(EventDetails* eventDetails);

	void OnEscPressed(EventDetails* eventDetails);
	void OnMouseClick(EventDetails* eventDetails);
	void OnMouseRelease(EventDetails* eventDetails);
	void OnMouseWheel(EventDetails* eventDetails);
	void OnMouseMove(EventDetails* eventDetails);
	void ToolSelect(EventDetails* eventDetails);
	void DeleteTiles(EventDetails* eventDetails);
	void NewMapDialogueBtnCreate(EventDetails* eventDetails);
	void NewMapDialogueBtnClose(EventDetails* eventDetails);
	// ---> TEMP
	void OnLayerElevation(EventDetails* eventDetails);
	void UpdateOptionsGUI();
	void SetCurrentMap(GameWorld* currentMap);
	// ---< TEMP

	void SetTool(MapEditorTool tool);
	void RedrawBrush();
	TileMap* GetBrush();
public:
	void CreateOptionsGUI();
	void UpdateMouse();
	void ResetTools();

	void ToolPanUpdate();
	void ToolBrushUpdate();
	void ToolBucketUpdate();
	void ToolEraserUpdate();
	void ToolSelectionUpdate();

	void PlaceBrushTiles();
	// ---> TEMP
	int32 GetLowestSelectedLayer() const;
	int32 GetHighestSelectedLayer() const;
	int32 lowestSelectedLayer_ = 0;
	int32 highestSelectedLayer_ = 0;
	vec2i selectionRangeX_;
	vec2i selectionRangeY_;
	void CopySelection(EventDetails* eventDetails);
	void RemoveSelection(EventDetails* eventDetails);
	// ---< TEMP
	void ResetZoom();

	MapEditorTool currentTool_;
	bool isInAction_;
	bool isInSecondaryAction_;
	bool isRightClickPan_;
	bool onlyDrawSelectedLayers_;

	vec2i mousePos_;
	vec2i mouseStartPos_;
	vec2i mouseTilePos_;
	vec2i mouseTileStartPos_;
	vec2f mouseDifference_;
	real32 zoom_;

	/// @attention brush is a tilemap.
	TileMap brush_;
	sf::RenderTexture brushRenderTexture_;
	sf::RectangleShape brushDrawable_;
	sf::RectangleShape selectionDrawable_;
	sf::RectangleShape mapBoundingBox_;

	GITileSelector* tileSelector_;
	//GIMapEditorOptions* mapEditorOptions_;
	GUIInterface* newMapDialogue_;
	GUIInterface* toolBox_;
	GUIInterface* options_;

	Window* window_;
	StateManager* stateManager_;
	GameWorld* currentMap_;

	sf::View& view_;
};



