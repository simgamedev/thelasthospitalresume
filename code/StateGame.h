#pragma once
#include "BaseState.h"
#include "EventManager.h"
//#include "Map.h"
#include "GameWorld/GameWorld.h"
#include <queue>
#include "TestEntity.h"
//#include "AI/Path.h" // To delete
//#include "AI/PathFinder.h" // To delete
#include "Graph/PathManager.h"

// ---> GOAP
//#include "AI/GOAP/Planner.h"
const int32 quadCount = 1 << 24;
// ---<

// ---> TBB
//class Image;
//using ImagePtr = std::shared_ptr<Image>;
// ---<


// ---> Modify
#include "Time/Clock.h"
#include "CharacterManager.h"
#include "FontManager.h"
#include "TextureManager.h"
#include "ECS/SystemManager.h"
// ---< 
#include "Audio/AudioManager.h"

// ---> Game Data
#include "GameStateData.h"
//#include "ItemFloor.h"
// ---< Game Data


enum class SubMode {
	BuildWall, BuyEntity, BuildPipeGround, BuildPipeUnderGround, Normal,
	LayFloor, AddDoor,
	SetGoal,
	Paused,
	BuildFoundation,
	AssignRoom,
	Bulldozing,
	ChangeWallpaper,
	MoveEntity,
	SellEntity,
	InspectEntity,
};

enum class MouseDraggingDirection
{
	None, Up, Down, Left, Right, Area
};

enum class MouseDraggingMode
{
	Disabled = 0, UnConstrained, Constrained,
};

enum class CursorType {
	Normal,
	Demolish,
	Sell,
	PickUp,
};

struct WallPlaceHolder {
	vec2i tilepos;
	sf::RectangleShape shape;
};

struct TilePlaceHolder {
	vec2i tilepos;
	sf::RectangleShape shape;
};


namespace MYGUI
{
	class GamePlayGUI;
	class StateGameViewModel;
}
class StateGame : public BaseState
{
public:
	StateGame(StateManager* stateManager);
	~StateGame();

	void OnCreate();
	void OnDestroy();

	void Activate();
	void Deactivate();

	void Update(const sf::Time& time);
	void Draw();

	void EscPressed(EventDetails* eventDetails);
	void OnMouseLeft(EventDetails* eventDetails);
	void OnMouseLeftRelease(EventDetails* eventDetails);
	void ResetMouseLeft();
	void OnMouseRight(EventDetails* eventDetails);
	void OnMouseRightRelease(EventDetails* eventDetails);
	void OnMouseMoved(EventDetails* eventDetails);
	void OnMouseWheel(EventDetails* eventDetails);
	void OnTabKeyPressed(EventDetails* eventDetails);
	void UpdateMouse();
	// ---> Save/Load
	void Save(const string& saveFileName = "");
	void LoadWalls();
	void LoadDoors();
	void LoadFloors();
	// ---<
	// ---> Coordinate Conversion
	vec2i WorldToScreenPos(const vec2f& worldPos);
	// ---<
	// ---> Getters/Setters
	vec2u GetWindowSize() const;
	SubMode GetSubMode() const;
	//GameWorld* GetGameWorld() const;
	// ---> TEMP
	void PlayerMove(EventDetails* eventDetails);
	vec2i mousePos_;
	vec2f mouseWorldPos_;
	vec2i mouseTilePos_;
	vec2f mouseDifference_;
	vec2i mouseFirstDownPos_;
	vec2f mouseFirstDownWorldPos_;
	vec2i mouseFirstDownTilePos_;
	void Pan();
	void ToggleDrawGrid();
	void ToggleDrawCollidable(EventDetails* eventDetails);
	bool IsTileWalkable(vec2i goalTilePos);

	// ---> GOAP
	//GOAP::WorldState* GetWorldState() const;
	// TODO: should refactor to Engine->GetEntityManager()->GetFreeEntityID(type/name?); (has no owner)
	// TODO: should refacotr to StateGame::GetFreeEntityID(type/name?) to decouple gameplay from engine!
	int32 GetEntityID(const string& name);
	// TODO: should refactor to Engine->GetEntityManager()->GetEntityPos(entityID);
	vec2f GetEntityPos(const int32& entityID);
	// ---< 


	// ---> GamePlay GUI
	void BackToMainMenu();
	void Pause();
	void UnPause();
	// ---<

	// ---> Building
	//void BuildWall(const string& wallEntityFileName);
	//void AddDoor(const string& doorEntityFileName);
	void LayFloor(const int32& x, const int32& y, const string& tileSetName, const int32& tileID);
	string floorTileSetName_;
	string tempEntityFilePath_;
	int32 floorTileID_;
	vec2i tempEntityTilePos_;
	string wallEntityFileName_; // TODO: remove this
	// ---< Building

private:
	GameWorld* pGameWorld_;
	// ---> TEMP
	int32 playerID_ = -1;
	real32 zoom_ = 1.0f;
	bool isRightClickPan_ = true;
	// void ResetZoom();
	bool isDrawGrid_ = false;
	std::vector<sf::RectangleShape> tileOutlines_;
	bool isDebugStuffInited_ = false;
	bool isDrawCollidable_ = false;
	// ---< TEMP

	// ---> Build Wall
	SubMode subMode_ = SubMode::Normal;
	bool isMouseLeftDown_ = false;
	//void DrawTempWallLayout(sf::RenderTarget* renderTarget);
	//void DrawTempFloorLayout(sf::RenderTarget* renderTarget);
	void DrawTempTileLayout(Window* window);
	//std::vector<WallPlaceHolder> tempWallPlaceHolders_;
	std::vector<TilePlaceHolder> tempTilePlaceHolders_;
	vec2u tempTileLayoutXRange_;
	vec2u tempTileLayoutYRange_;
	// ---<

	// ---> Assign Room
	std::vector<sf::RectangleShape> tempRoomTileOverlays_;
	Room* pTempRoom_;
	RoomType tempRoomType_;
	public:
	Room* GetClosestRoom(RoomType roomType) const;
	private:
	// ---< Assign Room

    // ---> TempEntity
	// ---> Buy Equipment
	int32 tempEntityID_ = -1;
	CPosition* cPosTempEntity_;
	void OnNewTempEntity(const int32 entityID);
	std::vector<sf::FloatRect> tempEntityCollisions_;
	// ---< Buy Equipment
	// ---< TempEntity


	// ---> DEBUG
	void DEBUGInitDebugStuff();
	void DrawDebugStuff();
	void DEBUGDrawPanel();
	void DEBUGDrawNavGraph();
	void DEBUGDrawCharacterInfo();
	bool m_bDEBUGDrawNavGraph;
	bool m_bDEBUGDrawGrid;
	// ---< DEBUG

	// ---> GOAP
	GOAP::WorldState* worldState_;
	void EntityAdded(int32 entityID);
	void EntityRemoved(int32 entityID);
	//void UpdateWorldState(EntityEventType entityEvent);
	// ---< GOAP

	// ---> GamePlay GUI
	sf::RectangleShape pausedOverlay_;
	// ---<
	// ---> Path Finder
	//Node** nodes_;
	//PathFinder* pPathFinder_;
	PathManager<PathPlanner>* pathManager_;

	sf::Time elapsedTime_;
	//---<
public:
	// ---> Modify
	void PutItemOnGround(const vec2f& pos, const ItemType& itemType);
	void ChangeSubMode(const SubMode& subMode, const char* parameter = nullptr);
	void AssignRoom(RoomType roomType);
	TestEntity* pSelectedCharacter_;
	CharacterManager* charMgr_;
	void PopThoughtBubble(int32 testEntityID);
	// FIXME: should only get entityname from entitymanager
	string GetEntityName(int32 entityID);
	vec2i GetEntityTilePos(const int32 entityID) const;
	Clock* clock_;
	// ---< Modify
	sf::Font& DEBUGGetFont();
	void InitTestEntities();
	TestEntity* GetTestEntityFromID(int32 testEntityID) const;
	PathManager<PathPlanner>* GetPathManager() { return pathManager_; }
	// ---<
	//--->Fix Game Map Crash
	void RedrawGameMap();
	//---<

	// ---> Modify
	Clock* GetClock() const;
	void SelectNextCharacter(EventDetails* eventDetails);
	void SelectPrevCharacter(EventDetails* eventDetails);
	TextureManager* GetTextureManager() const;
	FontManager* GetFontManager() const;
	sf::RenderWindow* GetRenderWindow() const;
	SystemManager* GetSystemManager() const;
	// FIXME: should we expose this?
	EntityManager* GetEntityManager() const;
	CharacterManager* GetCharacterManager() const;
	GameWorld* GetGameWorld() const;
	sf::Sprite* m_pCursorSprite;
	sf::View m_DefaultView;
	// ---< Modify
	AudioManager* pAudioManager_;

	// ---> Characters
	int32 selectedCharID_;
	int32 mouseOverCharID_;
	// ---< Characters

	// ---> NoesisGUI
	Noesis::Ptr<MYGUI::StateGameViewModel> spViewModel_;
	Noesis::Ptr<MYGUI::GamePlayGUI> spGamePlayGUI_;
	Noesis::Ptr<Noesis::IView> spNoesisView_;
	bool bKey1Pressed_;
	void Key1Pressed(EventDetails* eventDetails);
	void LoadingFinishedCallback();
	// ---< NoesisGUI

	// ---> Game Data
	GameStateData* pGameStateData_;
	real32 tempCost_;
	sf::Text* tempCostText_;
	//std::vector<ItemFloor*> itemFloors_;
	//std::vector<ItemFloor*> GetItemFloors() { return itemFloors_; }
	// ---< Game Data
private:
	EntityManager* pEntityManager_;
	TextureManager* pTextureManager_;
	SystemManager* pSystemManager_;
	SRenderer* pSRenderer_;
	int32 countGreen_;
	int32 countRed_;
	void SetCountRed(int32 countRed);
	bool bMLBFirstDownOnGUI_;
	void DrawTempGrid();
	//void DrawTempWallGrid();
	void DrawHorizontalGridLine(uint32 row, vec2u xRange, sf::Color color);
	void DrawVerticalGridLine(uint32 column, vec2u yRange, sf::Color color);
	sf::Sprite* tileTempSprite_;
	bool bMouseWasForcedToRelease_; // NOTE: when player was dragging mouse in game, but then hit GUI
	MouseDraggingDirection mouseDraggingDirection_;
	bool bIsMouseDragging_;
	void UpdateMouseDragging();
	vec2u mouseDraggingXRange_;
	vec2u mouseDraggingYRange_;
	SWall* pSWall_;
	SCollision* pSCollision_;
	std::vector<int32> wallsToChangeWallpaper_;
	std::vector<int32> wallsToBulldoze_;
	std::vector<int32> doorsToBulldoze_;
	Tile* tileUnderMouse_;
	Tile* oldTileUnderMouse_;
	int32 entityUnderMouse_;
	int32 oldEntityUnderMouse_;
	string entityUnderMouseName_;
	void UpdateTileUnderMouse(bool bForceUpdate = false);
	void UpdateEntityUnderMouse();
	void UpdateTempEntity();
	bool DEBUG_bTintEntityUnderMouse_;
	MouseDraggingMode mouseDraggingMode_;
public:
	std::vector<vec2i> GetTilesTakenByEntity(const int32 entityID) const;
private:
	void UpdateTempEntityGizmo();
	CCollidable* cColTempEntity_;
	sf::Sprite* gizmoFootstep_;
	sf::Sprite* gizmoHighlightedEntityUR_;
	sf::Sprite* gizmoHighlightedEntityUL_;
	sf::Sprite* gizmoHighlightedEntityBL_;
	sf::Sprite* gizmoHighlightedEntityBR_;
	void UpdateHighlightedEntityGizmo();
	int32 highlightedEntityID_;
	void ConfirmTempEntity();
	void MakeEntityTemp(int32 entityID);
};
