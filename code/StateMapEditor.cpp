#include "StateMapEditor.h"

#include "StateManager.h"
#include "Window.h"
#include "StateLoading.h"
#include "GUI/GUIManager.h"


/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
StateMapEditor::StateMapEditor(StateManager* stateManager)
	: BaseState(stateManager)
	/// @todo this should take a SharedContext
	, editorController_(stateManager_->GetContext()->window,
						stateManager_,
						stateManager_->GetContext()->gameMap,
						view_)
	, mapRedraw_(false)
	, currentMap_(nullptr)
{
	fileExplorer_ = new GIFileExplorer("FileExplorer",
									   stateManager_->GetContext()->guiManager,
									   stateManager_);
}

StateMapEditor::~StateMapEditor()
{
}

/********************************************************************************
 * On Create.
 ********************************************************************************/
void
StateMapEditor::OnCreate()
{
	/// ---> Bind key binding callbacks.
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->AddCallback("MapEditor_New", &StateMapEditor::BtnNew, this);
	eventManager->AddCallback("MapEditor_Load", &StateMapEditor::BtnLoad, this);
	eventManager->AddCallback("MapEditor_Save", &StateMapEditor::BtnSave, this);
	eventManager->AddCallback("MapEditor_SaveAs", &StateMapEditor::BtnSaveAs, this);
	eventManager->AddCallback("MapEditor_Exit", &StateMapEditor::BtnExit, this);

	// ---> GUI File Explorer.
	fileExplorer_->SetActionButtonCallback(&StateMapEditor::SaveOrLoad, this);
	fileExplorer_->SetDirectory(Utils::GetMediaDirectory() + "Maps/");
	fileExplorer_->Hide();
	// ---<


	// ---> GUI Top Bar.
	GUIManager* guiManager = stateManager_->GetContext()->guiManager;
	guiManager->LoadInterfaceNew("MapEditorTopBar.interface", "MapEditorTopBar");
	GUIInterface* topBar = guiManager->GetInterface("MapEditorTopBar");
	vec2f windowSize = vec2f(stateManager_->GetContext()->window->GetSize());
	topBar->SetPosition(vec2f(windowSize.x / 2.0f - topBar->GetSize().x / 2.0f, 0.0f));
	// ---<


	// ---> TEMP
	// ---> Preload every tileset.
	auto loadingState = stateManager_->GetState<StateLoading>(GameStateType::Loading);
	auto tileSetsFiles = Utils::GetFileList(Utils::GetMediaDirectory() + "Tilesets/" + "*.tileset");
	for(auto& itr : tileSetsFiles)
	{
		string tileSetFileName = itr.first;
		string tileSetName = tileSetFileName.substr(0, tileSetFileName.find(".tileset"));
		TileSet* tileSet = new TileSet(tileSetName, stateManager_->GetContext()->textureManager);
		if (!tileSets_.emplace(tileSetName, tileSet).second)
		{
			std::cout << "Failed to preload tileset: " << tileSetName << std::endl;
		}
		tileSet->AddFile(Utils::GetMediaDirectory() + "TileSets/"+ tileSetFileName);
		loadingState->AddFileLoader(tileSet);
	}
	//tileSet->ResetWorker();
	loadingState->SetManualContinue(true);
	// ---< TEMP
}


/********************************************************************************
 * On Destroy.
 ********************************************************************************/
void
StateMapEditor::OnDestroy()
{
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	TextureManager* textureManager = stateManager_->GetContext()->textureManager;

	eventManager->RemoveCallback(GameStateType::MapEditor, "Key_Escape");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_New");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_Load");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_Save");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_SaveAs");
	eventManager->RemoveCallback(GameStateType::MapEditor, "MapEditor_Exit");

	currentMap_->PurgeMap();
	/// @todo this should be in gameMap->PurgeMap()?
	currentMap_->GetTileMap()->SetMapSize({0, 0});	
}


/********************************************************************************
 * Draw.
 ********************************************************************************/
void
StateMapEditor::Draw()
{
	sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();
	// clear the background to a grayish color
	renderWindow->clear(sf::Color(61, 67, 65, 255));
	/*
	// ---> Darw All or Selected Layers of Map.
	uint32 fromLayer;
	uint32 toLayer;
	if(editorController_.OnlyDrawSelectedLayers())
	{
		fromLayer = editorController_.GetMapEditorOptions()->GetLowestSelectedLayer();
		toLayer = editorController_.GetMapEditorOptions()->GetHighestSelectedLayer();
	}
	else
	{
		fromLayer = 0;
		/// @todo deal with MAX_NUM_LAYERS
		toLayer = MAX_MAP_LAYERS;
	}
	for(uint32 i = fromLayer; i <= toLayer; ++i)
	{
		currentMap_->Draw(i);
	}
	*/
	if (currentMap_)
	{
		currentMap_->Draw(0);
		currentMap_->Draw(1);
	}

	/// ---> Draw editorController.
	editorController_.Draw(renderWindow);
}

void
StateMapEditor::ResetSavePath()
{
	mapSavePath_ = "";
}

void
StateMapEditor::SetMapRedraw(bool redraw)
{
	mapRedraw_ = redraw;
}

void
StateMapEditor::BackToMainMenu(EventDetails* eventDetails)
{
	stateManager_->SwitchTo(GameStateType::MainMenu);
}


/********************************************************************************
 * On Button New.
 ********************************************************************************/
void
StateMapEditor::BtnNew(EventDetails* eventDetails)
{
	std::cout << "Should show new map dialogue!" << std::endl;
	editorController_.ShowNewMapDialogue();
}


/********************************************************************************
 * On Button Load.
 ********************************************************************************/
void
StateMapEditor::BtnLoad(EventDetails* eventDetails)
{
	fileExplorer_->SetLoadMode();
	fileExplorer_->Show();
}


/********************************************************************************
 * On Btn Save.
 ********************************************************************************/
void
StateMapEditor::BtnSave(EventDetails* eventDetails)
{
	std::cout << "Should Save Map!" << std::endl;
	if (currentMap_)
	{
		currentMap_->SaveToFile("W:/thelasthospital/media/Maps/map4.map");
	}
}


/********************************************************************************
 * On Btn Save As.
 ********************************************************************************/
void
StateMapEditor::BtnSaveAs(EventDetails* eventDetails)
{
	fileExplorer_->SetSaveMode();
	fileExplorer_->Show();
}


/********************************************************************************
 * On Button Exit.
 ********************************************************************************/
void
StateMapEditor::BtnExit(EventDetails* eventDetails)
{
	stateManager_->SwitchTo(GameStateType::MainMenu);
	//stateManager_->Remove(GameStateType::MapEditor);
	stateManager_->RequestRemove(GameStateType::MapEditor);
}
/********************************************************************************
 * Save Map.
 ********************************************************************************/
void
StateMapEditor::SaveMap(const  string& filepath)
{
	currentMap_->SaveToFile(filepath);
	mapSavePath_ = filepath;
}

/********************************************************************************
 * Load Map.
 ********************************************************************************/
void
StateMapEditor::LoadMap(const string& filepath)
{
	StateLoading* stateLoading = stateManager_->GetState<StateLoading>(GameStateType::Loading);
	/*
	currentMap_->PurgeMap();
	currentMap_->ResetWorker();
	currentMap_->GetTileMap()->GetTileSet().ResetWorker();
	currentMap_->AddFile(filepath);
	stateLoading->AddFileLoader(currentMap_);
	stateLoading->SetManualContinue(false);
	mapRedraw_ = true;
	mapSavePath_ = filepath;
	*/
	if(currentMap_)
	{
		delete currentMap_;
	}
	currentMap_ = new GameWorld(stateManager_->GetContext()->window);
	editorController_.SetCurrentMap(currentMap_);
	/// @todo replace setStateManager with a proper process
	currentMap_->SetStateManager(stateManager_);
	currentMap_->AddFile(filepath);
	stateManager_->GetContext()->gameMap = currentMap_;

	stateLoading->AddFileLoader(currentMap_);
	stateLoading->SetManualContinue(true);

	editorController_.toolBox_->SetActive(true);
	editorController_.tileSelector_->Show();
	editorController_.options_->SetActive(true);
	stateManager_->SwitchTo(GameStateType::Loading);
}



/********************************************************************************
 * Save Or Load Callback from FileExplorer.
 ********************************************************************************/
void
StateMapEditor::SaveOrLoad(const string& filepath)
{
	if(fileExplorer_->IsSaveMode())
	{
		SaveMap(filepath);
	}
	else
	{
		LoadMap(filepath);
	}
	fileExplorer_->Hide();
}
/********************************************************************************
 * Update.
 ********************************************************************************/
void
StateMapEditor::Update(const sf::Time& time)
{
	editorController_.Update(time.asSeconds());
	currentMap_->Update(time.asSeconds());
}



/********************************************************************************
 * Activate/Deactivate
 ********************************************************************************/
void
StateMapEditor::Activate()
{
	std::cout << "StateMapEditor Activate!!!!" << std::endl;
	//if(!mapRedraw_) { return; }
	if(currentMap_)
	{
		currentMap_->Redraw();
	}
	//editorController_.SetTileSheetTexture(currentMap_->GetTileSet()->GetTextureName());
	mapRedraw_ = false;
}


void
StateMapEditor::Deactivate()
{
}

/*********************************************************************************
 * Get TileSet.
 *********************************************************************************/
TileSet*
StateMapEditor::GetTileSet(const string& tileSetName) const
{
	for (auto& itr : tileSets_)
	{
		string name = itr.first;
		if (name == tileSetName)
		{
			return itr.second;
		}
	}
	return nullptr;
}


