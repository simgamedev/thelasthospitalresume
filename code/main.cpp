#include "game.h"

int main()
{
	Game game;
	sf::Clock clock;
	real32 lastTime = 0;
	while(!game.GetWindow()->IsDone())
	{
		game.Update();
		real32 currentTime = clock.getElapsedTime().asSeconds();
		real32 ups = 1.0f / (currentTime - lastTime);
		game.ups_ = ups;

		game.Render();

		game.LateUpdate();
		currentTime = clock.restart().asSeconds();
		real32 fps = 1.0f / (currentTime - lastTime);
		game.fps_ = fps;
	}
	
	return 0;
}


