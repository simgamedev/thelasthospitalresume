#include "EventManager.h"
#include "Window.h"
#include "Utilities.h"
/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
EventManager::EventManager(Window* window)
	: hasFocus_(true)
	, window_(window)
{
	LoadBindings();
}

/********************************************************************************
 * Destructor
 ********************************************************************************/
EventManager::~EventManager()
{
	for(auto &itr : bindings_)
	{
		delete itr.second;
		itr.second = nullptr;
	}
}


/********************************************************************************
 * Change Game State.
 ********************************************************************************/
// todo: naming is too ambigious
void
EventManager::ChangeGameState(const GameStateType& gameState)
{
	SetState(gameState);
}


/********************************************************************************
 * Remove Game State.
 ********************************************************************************/
void
EventManager::RemoveGameState(const GameStateType& gameState)
{
	callbacks_.erase(currentGameState_);
}



/*
void
EventManager::SetCurrentGameState(GameStateType gameState)
{
	currentGameState_ = gameState;
}
*/

/********************************************************************************
 * Add a Binding.
 ********************************************************************************/
bool
EventManager::AddBinding(Binding* binding)
{
	// The EventManager already has a biniding with the same name
	if(bindings_.find(binding->name) != bindings_.end())
	{
		return false;
	}

	return bindings_.emplace(binding->name, binding).second;
}

/********************************************************************************
 * Removes a Binding
 ********************************************************************************/
bool
EventManager::RemoveBinding(const string& name)
{
	auto itr = bindings_.find(name);
	if(itr == bindings_.end()) { return false; }

	delete itr->second;
	bindings_.erase(itr);
	return true;
}

void EventManager::SetFocus(const bool& focus) { hasFocus_ = focus; }


/********************************************************************************
 * Handle sfml Event.
 ********************************************************************************/
void
EventManager::HandleEvent(sf::Event& event)
{
	for(auto &bindings_itr : bindings_)
	{
		Binding* binding = bindings_itr.second;
		// iterate through events that should happen to trigger this binding
		for(auto &events_itr : binding->eventsShouldHappen)
		{
			EventType eventHappeningType = (EventType)event.type;
			EventType eventShouldHappenType = events_itr.first;
			if(eventShouldHappenType != eventHappeningType) { continue; }
			// ---> Keydown/Keyup event
			if(eventHappeningType == EventType::KeyDown ||
			   eventHappeningType == EventType::KeyUp)
			{
				EventInfo eventShouldHappenInfo = events_itr.second;
				if(eventShouldHappenInfo.keycode == event.key.code)
				{
					if(binding->eventDetails.keycode == -1)
					{
						binding->eventDetails.keycode = events_itr.second.keycode;
					}
					if (eventHappeningType == EventType::KeyDown)
					{
						binding->eventDetails.controlPressed = event.key.control;
						binding->eventDetails.altPressed = event.key.alt;
						binding->eventDetails.shiftPressed = event.key.shift;
					}
					++(binding->numEventsHappening);
					break;
				}
			}
			// ---> Mouse Button Down/Up
			else if(eventHappeningType == EventType::MButtonDown ||
					eventHappeningType == EventType::MButtonUp)
			{
				EventInfo eventShouldHappenInfo = events_itr.second;
				if(eventShouldHappenInfo.keycode == event.mouseButton.button)
				{
					binding->eventDetails.mousePos.x = event.mouseButton.x;
					binding->eventDetails.mousePos.y = event.mouseButton.y;
					if(binding->eventDetails.keycode == -1)
					{
						binding->eventDetails.keycode = eventShouldHappenInfo.keycode;
					}
					++(binding->numEventsHappening);
					break;
				}
			}
			// ---> Mouse Moved
			else if (eventHappeningType == EventType::MouseMoved)
			{
				binding->eventDetails.mousePos = GetMousePos();
				++(binding->numEventsHappening);
			}
			// MouseWheel/Text Entered
			else
			{
				if(eventHappeningType == EventType::MouseWheel)
				{
					binding->eventDetails.mouseWheelDelta = event.mouseWheel.delta;
				}
				else if(eventHappeningType == EventType::TextEntered)
				{
					binding->eventDetails.characterEntered = event.text.unicode;
				}
				++(binding->numEventsHappening);
			}
		}
	}
}


/********************************************************************************
 * Handle GUIEvent.
 ********************************************************************************/
void
EventManager::HandleEvent(GUIEvent& event)
{
	if (event.elementName == "TileSetSprite" &&
		event.type == GUIEventType::Click)
	{
		int32 a = 100;
	}
	for(auto& bindingsItr : bindings_)
	{
		string bindingName = bindingsItr.first;
		Binding* binding = bindingsItr.second;
		for(auto& eventsShouldHappenItr : binding->eventsShouldHappen)
		{
			EventType eventShouldHappenType = eventsShouldHappenItr.first;
			EventInfo eventShouldHappenInfo = eventsShouldHappenItr.second;
			// ---> Event Should Happen is Not a GUIEvent, so skip it.
			if(eventShouldHappenType != EventType::GUIClick &&
			   eventShouldHappenType != EventType::GUIRelease &&
			   eventShouldHappenType != EventType::GUIHover &&
			   eventShouldHappenType != EventType::GUILeave &&
			   eventShouldHappenType != EventType::GUIFocus &&
			   eventShouldHappenType != EventType::GUIDefocus)
			{
				continue;
			}
			// ---> Event Should Happen Type != event happening type
			/// @todo why not get rid of GUIEventType && just use EventType::GUIXXX alone??
			if((eventShouldHappenType == EventType::GUIClick && event.type != GUIEventType::Click) ||
			   (eventShouldHappenType == EventType::GUIRelease && event.type != GUIEventType::Release) ||
			   (eventShouldHappenType == EventType::GUIHover && event.type != GUIEventType::Hover) ||
			   (eventShouldHappenType == EventType::GUILeave && event.type != GUIEventType::Leave) ||
			   (eventShouldHappenType == EventType::GUIFocus && event.type != GUIEventType::Focus) ||
			   (eventShouldHappenType == EventType::GUIDefocus && event.type != GUIEventType::Defocus))
			{
				continue;
			}

			GUIEvent& eventShouldHappen = eventShouldHappenInfo.guiEvent;
			if(eventShouldHappen.interfaceName != event.interfaceName)
			{
				continue;
			}
			if(eventShouldHappen.elementName == "*")
			{
				if(event.elementName.empty()) { continue; }
			}
			else if(eventShouldHappen.elementName != event.elementName)
			{
				continue;
			}
			binding->eventDetails.interfaceName = event.interfaceName;
			binding->eventDetails.elementName = event.elementName;
			binding->eventDetails.mousePos = vec2i(event.clickCoords.x, event.clickCoords.y);
			++(binding->numEventsHappening);
		}
	}
}

/********************************************************************************
 * Update
 ********************************************************************************/
void
EventManager::Update()
{
	if(!hasFocus_) { return; }
	for(auto &bindings_itr : bindings_)
	{
		Binding* binding = bindings_itr.second;
		for(auto& eventsShouldHappen_itr : binding->eventsShouldHappen)
		{
			EventType eventShouldHappenType = eventsShouldHappen_itr.first;
			switch(eventShouldHappenType)
			{
				case(EventType::Keyboard):
				{
					if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key(eventsShouldHappen_itr.second.keycode)))
					{
						/// @todo FIXME?
						if(binding->eventDetails.keycode != -1)
						{
							binding->eventDetails.keycode = eventsShouldHappen_itr.second.keycode;
						}
						++(binding->numEventsHappening);
					}
					   
				} break;

				case(EventType::Mouse):
				{
					if(sf::Mouse::isButtonPressed(sf::Mouse::Button(eventsShouldHappen_itr.second.keycode)))
					{
						/// @todo FIXME?
						if(binding->eventDetails.keycode != -1)
						{
							binding->eventDetails.keycode = eventsShouldHappen_itr.second.keycode;
						}
						++(binding->numEventsHappening);
					}
				} break;
				case(EventType::Joystick):
				{
				} break;
			}
		}

		// Binding is triggered
		if(binding->eventsShouldHappen.size() == binding->numEventsHappening)
		{
			//std::cout << binding->name << " Triggered!" << std::endl;
			// ---> global callbacks
			auto callbackContainersItr = callbacks_.find(GameStateType(0));
			if(callbackContainersItr != callbacks_.end())
			{
				auto globalCallbacks = callbackContainersItr->second;
				auto callbacksItr = globalCallbacks.find(binding->name);
				if(callbacksItr != globalCallbacks.end())
				{
					auto callbackFunc = callbacksItr->second;
					callbackFunc(&binding->eventDetails);
				}
			}
			// ---> game state specific callbacks
			callbackContainersItr = callbacks_.find(currentGameState_);
			if(callbackContainersItr != callbacks_.end())
			{
				auto currentStateCallbacks = callbackContainersItr->second;
				auto callbacksItr = currentStateCallbacks.find(binding->name);
				if(callbacksItr != currentStateCallbacks.end())
				{
					auto callbackFunc = callbacksItr->second;
					callbackFunc(&binding->eventDetails);
				}
			}

		}
		// Reset
		binding->numEventsHappening = 0;
		binding->eventDetails.Clear();
	}
}

/********************************************************************************
 * Load Bindings from keys.cfg
 ********************************************************************************/
void
EventManager::LoadBindings()
{
	string delimiter = ":";

	std::ifstream bindingsFile;
    bindingsFile.open(Utils::GetMediaDirectory() + "keys.cfg");
	if(!bindingsFile.is_open())
	{
		std::cout << "! Failed to load keys.cfg" << std::endl;
	}
	string line;
	while(std::getline(bindingsFile, line))
	{
		if(line[0] == '|') { continue; }
		std::stringstream keystream(line);
		string bindingName;
		keystream >> bindingName;
		Binding* binding = new Binding(bindingName);
		while(!keystream.eof())
		{
			string keyValue;
			keystream >> keyValue;
			int32 start = 0;
			int32 end = keyValue.find(delimiter);
			// binding key value pair invalid
			if(end == std::string::npos) { delete binding; binding = nullptr; break; }
			EventType eventType = EventType(stoi(keyValue.substr(start, end - start)));
			if(eventType == EventType::GUIClick ||
			   eventType == EventType::GUIRelease ||
			   eventType == EventType::GUIHover ||
			   eventType == EventType::GUILeave ||
			   eventType == EventType::GUIFocus ||
			   eventType == EventType::GUIDefocus)
			{
				start = end + delimiter.length();
				end = keyValue.find(delimiter, start);
				string interfaceName = keyValue.substr(start, end - start);
				string elementName;
				if(end != std::string::npos)
				{
					start = end + delimiter.length();
					end = keyValue.length();
					elementName = keyValue.substr(start, end);
				}
				GUIEvent guiEvent;
				guiEvent.interfaceName = interfaceName;
				guiEvent.elementName = elementName;
				EventInfo eventInfo(guiEvent);
				binding->BindEvent(eventType, std::move(eventInfo));
			}
			else
			{
				int32 keycode = stoi(keyValue.substr(end + delimiter.length(), keyValue.find(delimiter, end + delimiter.length())));
				EventInfo eventInfo(keycode);

				binding->BindEvent(eventType, eventInfo);
			}
		}

		if(!AddBinding(binding))
		{
			delete binding;
		}

		binding = nullptr;
	}

	bindingsFile.close();
}


vec2i
EventManager::GetMousePos()
{
	return sf::Mouse::getPosition(*window_->GetRenderWindow());
}
