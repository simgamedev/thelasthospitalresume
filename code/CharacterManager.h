#pragma once
#include "common.h"

#include "TestEntity.h"
#include "Characters/Patient.h"
#include "Characters/Tester.h"
#include "Characters/Nurse.h"
#include "Messaging/IMsgObserver.h"
class StateGame;
class CharacterManager : public IMsgObserver
{
private:
	static int32 nextID_;
	std::map<int32, TestEntity*> characters_;
	StateGame* pStateGame_;
public:
	CharacterManager(StateGame* stateGame);
	TestEntity* GetCharacterFromID(const int32 id);
	std::vector<TestEntity*> GetAllCharactersOfProfession(Profession profession);
	void NewCharacter(Profession profession, const vec2f& pos);
	int32 GetNumCharacters() const;
	int32 SetHoldingEntity(const int32& characterID, const string& entityName);

	void Draw(Window* window);
	void Update(const sf::Time& time);

	//void OnMessage(Telegram telegram);
	void OnBroadcast(Telegram telegram);
	void RegisterForMsgType(CharMsgType msgType);
	std::map<int32, TestEntity*> GetCharacters();
	std::vector<TestEntity*> GetVisibleCharacters(const sf::FloatRect& viewSpace);
};
