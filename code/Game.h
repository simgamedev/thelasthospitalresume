#pragma once
#include "Window.h"
#include "EventManager.h"
#include "StateManager.h"
#include "GUI/GUIManager.h"
#include "SharedContext.h"
#include "TextureManager.h"
#include "AudioResourceManager.h"
#include "Audio/AudioManager.h"
#include <iostream>

/**
   A Game Class.
 */
class Game
{
public:
	Game();
	~Game();

	void Update();
	void Render();
	Window* GetWindow();
	sf::Time GetElapsedTime();
	void LateUpdate();
	real32 fps_;
	real32 ups_;
private:
	Window window_;
	SharedContext context_;
	GUIManager* guiManager_;
	FontManager* fontManager_;
	TextureManager textureManager_;
	StateManager* stateManager_;
	AudioManager* audioManager_;
	AudioResourceManager* audioResourceManager_;
	// ---> ECS
	EntityManager* entityManager_;
	SystemManager* systemManager_;
	// ---< ECS
	sf::Clock clock_;
	sf::Time elapsedTime_;
	void RestartClock();
	void Init();
};
 
