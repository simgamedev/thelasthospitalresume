#pragma once
#include "common.h"
/**
   A simple Bitmask.
 */
class Bitmask
{
public:
	Bitmask()
		: bits_(0)
	{
	}
	Bitmask(const uint32& bits)
		: bits_(bits)
	{
	}

	uint32 GetBits() const { return bits_; }
	void SetBits(const uint32& bits) { bits_ = bits; }

	bool Matches(const Bitmask& bitmask)
	{
		//return bitmask.GetBits() == bits_;
		if (bitmask.GetBits() == 7)
		{
			int32 a = 100;
		}
		auto andResult = bits_ & bitmask.GetBits();
		if (andResult == bits_)
		{
			return true;
		}
		return false;
	}

	bool GetBit(const uint32& pos) const
	{
		return ((bits_&(1 << pos)) != 0);
	}
	void TurnOnBit(const uint32& pos)
	{
		bits_ |= (1 << pos);
	}
	void TurnOnBits(const uint32& bits)
	{
		bits_ |= bits;
	}
	void TurnOffBit(const uint32& pos)
	{
		bits_ &= ~(1 << pos);
	}
	void ToggleBit(const uint32& pos)
	{
		bits_ ^= (1 << pos);
	}

	void Clear()
	{
		bits_ = 0;;
	}
private:
	uint32 bits_;
};
