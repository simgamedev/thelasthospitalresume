#pragma once
#include "Communicator.h"
#include "EntityMessageTypes.h"
#include <unordered_map>
#include "../Time/Clock.h"
#include <set>

struct MessageCompare
{
	bool operator()(const Message& lhs, const Message& rhs) const
	{
		uint32 t1 = lhs.timeToDispatch.minutes_ + lhs.timeToDispatch.hours_ * 60 + lhs.timeToDispatch.days_ * 60 * 24;
		uint32 t2 = rhs.timeToDispatch.minutes_ + rhs.timeToDispatch.hours_ * 60 + rhs.timeToDispatch.days_ * 60 * 24;
		return t1 < t2;
	}
};

class EntityMessageHandler
{
private:
	std::unordered_map<EntityMessageType, Communicator> communicators_;
	static EntityMessageHandler* instance_;
	EntityMessageHandler();
public:
	static EntityMessageHandler* Instance();
	bool Subscribe(const EntityMessageType& type, Observer* observer)
	{
		return communicators_[type].AddObserver(observer);
	}

	bool Unsubscribe(const EntityMessageType& type, Observer* observer)
	{
		return communicators_[type].RemoveObserver(observer);
	}

	void Dispatch(const Message& msg);
	void DispatchLater(Message& msg, GameTime timeDelay);
	void DispatchDelayedMessages();
	void Update(real32 dt);
	GameTime lastGameTime_;
	//std::queue<std::pair<GameTime, Message>> delayedMessageQ_;
	std::set<Message, MessageCompare> delayedMessageQ_;
};
