#pragma once
#include "CBase.h"


class CItem : public CBase
{
public:
	CItem()
		: CBase(ComponentType::Item)
	{
	}

	void ReadIn(std::stringstream& stream)
	{
		uint32 state = 0;
		stream >> state;
		state_ = (EntityState)state;
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CState" << std::endl;
	}

	EntityState GetState()
	{
		return state_;
	}

	void SetState(const EntityState& state)
	{
		state_ = state;
	}
private:
	EntityState state_;
	int32 dayCreated_;
	real32 originalPrice_;
	real32 currentPrice_;
	int32 deco_;
};
