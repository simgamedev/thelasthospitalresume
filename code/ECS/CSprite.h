#pragma once
#include "CDrawable.h"
#include "../GameSprite.h"
#include "../magic_enum.hpp"

class CSprite : public CDrawable
{
public:
	CSprite()
		: CDrawable(ComponentType::Sprite)
		, sprite_(nullptr)
	{
	}
	~CSprite() { if(sprite_) { delete sprite_; } }

	void ReadIn(std::stringstream& stream)
	{
		stream >> sheetName_;
		if (sheetName_ == "WallPieces_01")
		{
			int32 a = 100;
		}
		string directionsAllowedString;
		stream >> directionsAllowedString;
		// TODO: uint8
		// TODO: CSprite Origin is not working yet
		// NOTE: if you dont specify origin in entity file, it's defaulted to be 1(BottomLeft)
		uint32 origin = 1;
		stream >> origin;
		origin_ = (Origin)origin;

		// ---> Split directionsAllowedString
		std::istringstream iss(directionsAllowedString);
		string item;
		std::vector<string> directionStrings;
		while (std::getline(iss, item, '/')) {
			directionStrings.push_back(item);
		}
		// ---<
		for (string directionString : directionStrings)
		{
			if(directionString == "Up")
			{
				directionsAllowed_.push_back(Direction::Up);
			}
			if(directionString == "Down")
			{
				directionsAllowed_.push_back(Direction::Down);
			}
			if(directionString == "Left")
			{
				directionsAllowed_.push_back(Direction::Left);
			}
			if(directionString == "Right")
			{
				directionsAllowed_.push_back(Direction::Right);
			}
		}
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CSprite" << std::endl;
		file << "|Component Sprite SheetName DirectionsAllowed|" << std::endl;
		file << "Component "
			<< (int32)ComponentType::Sprite << " "
			<< sheetName_ << " ";
		int32 counter = 0; // calculate number of slashes to write
		for (Direction& dir : directionsAllowed_)
		{
			file << magic_enum::enum_name(dir);
			counter++;
			if (counter == 1 && directionsAllowed_.size() == 1)
			{
				// no slash to write
			}
			else if (counter < directionsAllowed_.size())
			{
				file << "/";
			}
		}
		file << std::endl;
	}

	void Create(TextureManager* textureManager)
	{
		if(sprite_) { return; }
		/// @todo: new Sprite("PatientMonitor_01"), each Sprite has its own sheet?
		sprite_ = new GameSprite(textureManager);
		sprite_->LoadSheet(sheetName_);
	}

	GameSprite* GetSprite() { return sprite_; }

	void UpdatePosition(const vec2f& position)
	{
		sprite_->SetPosition(position);
	}

	const vec2u& GetSize()
	{
		return sprite_->GetSpriteSize();
	}

	Direction GetDirection()
	{
		return sprite_->GetDirection();
	}
	void SetDirection(Direction& direction)
	{
		for (auto& itr : directionsAllowed_)
		{
			if(itr == direction)
				sprite_->SetDirection(direction);
		}
	}

	void Draw(Window* window, int32 zOrder)
	{
		if(!sprite_) { return; }
		sprite_->Draw(window, zOrder);
	}

	void Tint(const sf::Color& color)
	{
		sprite_->Tint(color);
	}
	
	Origin GetOrigin() const
	{
		return origin_;
	}
private:
	GameSprite* sprite_;
	string sheetName_;
	std::vector<Direction> directionsAllowed_;
	Origin origin_;
};
