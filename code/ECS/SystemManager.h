#pragma once
#include <SFML/Graphics.hpp>
#include "../Window.h"
#include "SRenderer.h"
#include "SMovement.h"
#include "SMovementNew.h"
#include "SCollision.h"
#include "SControl.h"
#include "SState.h"
#include "SWall.h"
#include "SFloor.h"
#include "SSpriteAnimation.h"
#include "SStorage.h"
#include "EntityEventQueue.h"
#include "MessageHandler.h"
#include <unordered_map>
#include <map>

using SystemContainer = std::unordered_map<SystemType, SBase*>;
//using SystemContainer = std::map<SystemType, SBase*>;

class EntityManager;
/**
   Manages all the systems of ECS.
 */
class SystemManager
{
public:
	SystemManager();
	~SystemManager();

	/// @todo Engine->GetEntityManager()?
	void SetEntityManager(EntityManager* entityManager);
	EntityManager* GetEntityManager();

	EntityMessageHandler* GetMessageHandler();

	template<class T>
	T* GetSystem(const SystemType& systemType)
	{
		auto systemsItr = systems_.find(systemType);
		//if(!systemsItr == systems_.end())
        if(systemsItr != systems_.end())
		{
			return dynamic_cast<T*>(systemsItr->second);
		}
		else
		{
			return nullptr;
		}
	}

	void AddEvent(const EntityID& entityID, const uint32& eventType);
	void Update(real32 dt);
	void HandleEvents();
	void Draw(Window* window, uint32 elevation);

	void EntityModified(const EntityID& entityID, const Bitmask& bitmask);
	void RemoveEntity(const EntityID& entityID);

	void PurgeEntities();
	void PurgeSystems();
private:
	SystemContainer systems_;
	EntityManager* entityManager_;
 	std::unordered_map<EntityID, EntityEventQueue> eventQueues_;
	EntityMessageHandler* messageHandler_;
};
