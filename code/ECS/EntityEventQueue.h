#pragma once
#include "../common.h"
#include <queue>

/**
   A simple entity event queue.
 */
class EntityEventQueue
{
public:
	void AddEvent(const uint32& eventType)
	{
		queue_.push(eventType);
	}

	bool ProcessEvents(uint32& eventType)
	{
		if(queue_.empty()) { return false; }
		eventType = queue_.front();
		queue_.pop();
		return true;
	}

	void Clear()
	{
		while(!queue_.empty())
		{
			queue_.pop();
		}
	}
private:
	std::queue<uint32> queue_;
};

