#include "MessageHandler.h"

EntityMessageHandler* EntityMessageHandler::instance_ = nullptr;

// TODO: formal singleton
EntityMessageHandler*
EntityMessageHandler::Instance()
{
	if (instance_ == nullptr)
		instance_ = new EntityMessageHandler();
	return instance_;
}


EntityMessageHandler::EntityMessageHandler()
{
}

void 
EntityMessageHandler::Dispatch(const Message& msg)
{
	auto communicatorsItr = communicators_.find((EntityMessageType)msg.type);
	if(communicatorsItr == communicators_.end()) { return; }
	Communicator communicator = communicatorsItr->second;
	communicator.Broadcast(msg);
}

/******************************* DispatchDelayed ********************************
 *
 ********************************************************************************/
void
EntityMessageHandler::DispatchLater(Message& msg, GameTime timeDelay)
{
	LOG_ENGINE_ERROR("EntityMessageHandler::DispatchLater");
	GameTime curGameTime = Clock::Instance()->GetGameTime();
	GameTime timeToDispatch = curGameTime + timeDelay;
	msg.timeToDispatch = timeToDispatch;
	delayedMessageQ_.emplace(msg);
}

void
EntityMessageHandler::DispatchDelayedMessages()
{
	GameTime curGameTime = Clock::Instance()->GetGameTime();
	GameTime zeroGameTime(0, 0, 0);
	while(!delayedMessageQ_.empty() &&
		  (delayedMessageQ_.begin()->timeToDispatch < curGameTime))
	{
		LOG_ENGINE_ERROR("EntityMessageHandler::DispatchDelayedMesssagesInnerWhile");
		Message msg = *delayedMessageQ_.begin();
		Dispatch(msg);
		delayedMessageQ_.erase(delayedMessageQ_.begin());
	}
	/*
	if (!delayedMessageQ_.empty() &&
		(delayedMessageQ_.begin()->timeToDispatch >= curGameTime))
	{
		delayedMessageQ_.erase(delayedMessageQ_.begin());
	}
	*/
}

/******************************* Update	******************************
 *
 *********************************************************************/
void
EntityMessageHandler::Update(real32 dt)
{
	GameTime curGameTime = Clock::Instance()->GetGameTime();
	if (curGameTime.minutes_ - lastGameTime_.minutes_ < 1)
		return;
	DispatchDelayedMessages();
	lastGameTime_ = curGameTime;
}



