#pragma once

// TODO: CharMsgType & EntityMessageType
// TODO: CharMsgDispatcher & EntityMessageHandler
// TODO: DispatchMessage BroadcastMessage 
// TODO: Combine all of them 
enum class EntityMessageType
{
	StartMoving,
		IsMoving,
		StateChanged,
		SwitchState,
		DirectionChanged,
		AttackAction,
		Dead,
		ItemTook,
		NeedRefill,
		ReadyToOperate,
		GoodResult,
		BadResult,
};
