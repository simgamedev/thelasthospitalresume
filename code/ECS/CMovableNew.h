#pragma once
#include "CBase.h"
#include "../Directions.h"
#include <SFML/System/Vector2.hpp>


class CMovableNew : public CBase
{
public:
	CMovableNew()
		: CBase(ComponentType::MovableNew)
	{
	}

	void ReadIn(std::stringstream& stream)
	{
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CMovableNew" << std::endl;
	}

private:
	vec2f velocity_;
	vec2f heading_;
	// perpendicular to heading_
	vec2f side_;
	real32 mass_;
	real32 maxSpeed_;
	real32 maxForce_;
	real32 maxTurnRate_;
};
