#include "SRenderer.h"
#include "SystemManager.h"

/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
SRenderer::SRenderer(SystemManager* systemManager)
	: SBase(SystemType::Renderer, systemManager)
	, prevTopmostEntityUnderMouse_(-1)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Position);
	requirement.TurnOnBit((uint32)ComponentType::Sprite);
	requirements_.push_back(requirement);
	requirement.Clear();
	
	requirement.TurnOnBit((uint32)ComponentType::Position);
	requirement.TurnOnBit((uint32)ComponentType::SimpleSprite);
	requirements_.push_back(requirement);
	requirement.Clear();

	systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::DirectionChanged, this);
}

SRenderer::~SRenderer()
{
}


/********************************************************************************
 * Update each drawable's position.
 ********************************************************************************/
void
SRenderer::Update(real32 dt)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for(auto& entityID : entities_)
	{
		if (entityID == 0)
		{
		}
		CPosition* cPos = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
		CDrawable* cDrawable = nullptr;
		if(entityManager->HasComponent(entityID, ComponentType::Sprite))
		{
			cDrawable = entityManager->GetComponent<CDrawable>(entityID, ComponentType::Sprite);
		}
		else if(entityManager->HasComponent(entityID, ComponentType::SimpleSprite))
		{
			cDrawable = entityManager->GetComponent<CDrawable>(entityID, ComponentType::SimpleSprite);
		}
		else
		{
			continue;
		}
		cDrawable->UpdatePosition(cPos->GetPosition());
	}
}


/********************************************************************************
 * Resort the drawbles when certain events happen.
 ********************************************************************************/
void
SRenderer::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
	/*
	if(eventType == EntityEventType::MovingLeft ||
	   eventType == EntityEventType::MovingRight ||
	   eventType == EntityEventType::MovingUp ||
	   eventType == EntityEventType::MovingDown ||
	   eventType == EntityEventType::ElevationChanged ||
	   eventType == EntityEventType::Spawned)
	{
		SortDrawables();
	}
	*/
}


void
SRenderer::OnNotify(const Message& msg)
{
	if(HasEntity(msg.receiver))
	{
		EntityMessageType msgType = (EntityMessageType)msg.type;
		switch(msgType)
		{
			/// @todo why do we need process EntityMessageType::DirectionChanged??
			/// @todo shouldn't it be processed by Sxxx?
			case EntityMessageType::DirectionChanged:
			{
				SetSpriteDirection(msg.receiver, (Direction)msg.intValue);
			} break;
		}
	}
}


/********************************************************************************
 * Draw entity if it's in window's viewspace.
 ********************************************************************************/
void
SRenderer::Render(Window* window, uint32 elevation)
{
	//std::cout << "num entities: " << entities_.size() << std::endl;
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for(auto& entityID : entities_)
	{
		CPosition* cPos = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
		/// @todo why not just use cPos->GetElevation != elevation??
		if(cPos->GetElevation() < elevation) { continue; }
		if(cPos->GetElevation() > elevation) { continue; }
		CDrawable* cDrawable = nullptr;
		if(!entityManager->HasComponent(entityID, ComponentType::Sprite) &&
		   !entityManager->HasComponent(entityID, ComponentType::SimpleSprite))
		{ 
			continue; 
		}
		cDrawable = entityManager->GetComponent<CDrawable>(entityID, ComponentType::Sprite);
		if (!cDrawable)
			cDrawable = entityManager->GetComponent<CDrawable>(entityID, ComponentType::SimpleSprite);
		sf::FloatRect drawableBounds;
		/// @todo make sure CPosition->Pos is anchored at mid bottom
		switch (cDrawable->GetOrigin())
		{
			case Origin::BottomLeft:
			{
				drawableBounds.left = cPos->GetPosition().x;
				drawableBounds.top = cPos->GetPosition().y - cDrawable->GetSize().y;
				drawableBounds.width = cDrawable->GetSize().x;
				drawableBounds.height = cDrawable->GetSize().y;
			} break;

			case Origin::MidBottom:
			{
				drawableBounds.left = cPos->GetPosition().x - (cDrawable->GetSize(). x / 2);
				drawableBounds.top = cPos->GetPosition().y - cDrawable->GetSize().y;
				drawableBounds.width = cDrawable->GetSize().x;
				drawableBounds.height = cDrawable->GetSize().y;
			} break;
		}
		if(!window->GetViewSpace().intersects(drawableBounds)) { continue; }
		//cDrawable->Draw(window->GetRenderWindow());
		int32 zOrder = cPos->GetTilePos().y;
		cDrawable->Draw(window, zOrder);
	}
}

void
SRenderer::SetSpriteDirection(const EntityID& entityID, const Direction& direction)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	if(!entityManager->HasComponent(entityID, ComponentType::Sprite)) { return; }
	CSprite* cSprite = entityManager->GetComponent<CSprite>(entityID, ComponentType::Sprite);
	/// @todo: Refactor Sprite to Sprite
	cSprite->GetSprite()->SetDirection(direction);
}

/******************************* GetEntityUnderMouse ****************************
 *
 ********************************************************************************/
int32
SRenderer::GetEntityUnderMouse(vec2f mouseWorldPos)
{
	entitiesUnderMouse_.clear();

	EntityManager* entityManager = systemManager_->GetEntityManager();
	for(auto& entityID : entities_)
	{
		CPosition* cPos = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
		CDrawable* cDrawable = nullptr;
		if(!entityManager->HasComponent(entityID, ComponentType::Sprite) &&
		   !entityManager->HasComponent(entityID, ComponentType::SimpleSprite))
		{ 
			continue; 
		}
		cDrawable = entityManager->GetComponent<CDrawable>(entityID, ComponentType::Sprite);
		if (!cDrawable)
			cDrawable = entityManager->GetComponent<CDrawable>(entityID, ComponentType::SimpleSprite);
		sf::FloatRect drawableBounds;
		/// @todo make sure CPosition->Pos is anchored at mid bottom
		switch (cDrawable->GetOrigin())
		{
			case Origin::BottomLeft:
			{
				drawableBounds.left = cPos->GetPosition().x;
				drawableBounds.top = cPos->GetPosition().y - cDrawable->GetSize().y;
				drawableBounds.width = cDrawable->GetSize().x;
				drawableBounds.height = cDrawable->GetSize().y;
			} break;

			case Origin::MidBottom:
			{
				drawableBounds.left = cPos->GetPosition().x - (cDrawable->GetSize(). x / 2);
				drawableBounds.top = cPos->GetPosition().y - cDrawable->GetSize().y;
				drawableBounds.width = cDrawable->GetSize().x;
				drawableBounds.height = cDrawable->GetSize().y;
			} break;
		}
		int32 zOrder = cPos->GetTilePos().y;
		// ---> Entity Under Mouse
		if (drawableBounds.contains(mouseWorldPos))
		{
			entitiesUnderMouse_.emplace(zOrder, entityID);
			//LOG_ENGINE_ERROR("NumEntitiesUnderMouse:" + std::to_string(entitiesUnderMouse_.size()));
		}
		// ---< Entity Under Mouse
	}
	if (entitiesUnderMouse_.size() == 0)
	{
		//entityManager->UntintEntity(prevTopmostEntityUnderMouse_);
		return -1;
	}


	
	int32 topmostEntity = (entitiesUnderMouse_.rbegin())->second;
	if (topmostEntity != prevTopmostEntityUnderMouse_ &&
		prevTopmostEntityUnderMouse_ != -1)
	{
		//entityManager->UntintEntity(prevTopmostEntityUnderMouse_);
	}
	//entityManager->TintEntity(topmostEntity, sf::Color::Green);
	prevTopmostEntityUnderMouse_ = topmostEntity;
	return topmostEntity;
	//std::cout << "num entities: " << entities_.size() << std::endl;
}

// FIXME: this should be removed
void
SRenderer::SortDrawables()
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	std::sort(entities_.begin(),
			  entities_.end(),
			  [entityManager](uint32 entity1, uint32 entity2)
			  {
				  CPosition* cPos1 = entityManager->GetComponent<CPosition>(entity1, ComponentType::Position);
				  CPosition* cPos2 = entityManager->GetComponent<CPosition>(entity2, ComponentType::Position);
				  if(cPos1->GetElevation() == cPos1->GetElevation())
				  {
					  return cPos1->GetPosition().y < cPos2->GetPosition().y;
				  }
				  return cPos1->GetElevation() < cPos2->GetElevation();
			  });
}
