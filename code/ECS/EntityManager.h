#pragma once
#include "CPosition.h"
#include "CSprite.h"
#include "CState.h"
#include "CMovable.h"
#include "CController.h"
#include "CCollidable.h"
#include "CWall.h"
#include "CDoor.h"
#include "CMovableNew.h"
#include "CStorage.h"
#include "CSimpleSprite.h"
#include "../Bitmask.h"
#include "../TextureManager.h"
#include <unordered_map>
#include <vector>
#include <functional>

using EntityID = uint32;
using ComponentContainer = std::vector<CBase*>;
using EntityData = std::pair<Bitmask, ComponentContainer>;
using EntityContainer = std::unordered_map<EntityID, EntityData>;
using ComponentsFactories = std::unordered_map<ComponentType, std::function<CBase*(void)>>;


class SystemManager;
/**
   Manages all the entities of ECS.
 */
class EntityManager
{
public:
	EntityManager(SystemManager* systemManager, TextureManager* textureManager);
	~EntityManager();

	int32 AddEntity(const Bitmask& bitmask);
	int32 AddEntity(const string& entityFilePath);
	int32 AddEntity(const string& entityFileName, const string& directory);
	int32 LoadEntity(const string& entityFile);
	/// @todo AddEntity(tilepos, entityFilename);
	///int32 AddEntity(const vec2u& tilepos, const string& entityFilename);
	bool RemoveEntity(const EntityID& entityID);
	bool AddComponent(const EntityID& entityID, const ComponentType& componentType);
	int32 GetEntityID(const string& name);

	template<class T>
	T* GetComponent(const EntityID& entityID, const ComponentType& componentType)
	{
		auto entitiesItr = entities_.find(entityID);
		if(entitiesItr == entities_.end()) { return nullptr; }
		// Found the entity.
		Bitmask entityBitmask = entitiesItr->second.first;
		// Doesn't have the component
		if(!entityBitmask.GetBit((uint32)componentType))
		{
			return nullptr;
		}
		// Component exists.
		auto& componentsContainer = entitiesItr->second.second;
		auto componentsItr = std::find_if(componentsContainer.begin(),
										  componentsContainer.end(),
										  [componentType](CBase* component)
										  {
											  return component->GetType() == componentType;
										  });
		if(componentsItr == componentsContainer.end())
		{
			return nullptr;
		}
		else
		{
			return dynamic_cast<T*>(*componentsItr);
		}
	}

	bool RemoveComponent(const EntityID& entityID, const ComponentType& componentType);
	bool HasComponent(const EntityID& entityID, const ComponentType& componentType);

	void Purge();
	// ---> Modify
	void RotateEntityClockwise(int32 entityID);
	string GetEntityName(const EntityID& entityID) const;
	bool SaveEntitiesToFile();
	void SetEntityPos(const EntityID& entityID, const vec2f& pos);
	vec2f GetEntityPos(const EntityID& entityID);
	vec2i GetEntityTilePos(const EntityID& entityID);
	void TintEntity(const EntityID& entityID, const sf::Color& color);
	void UntintEntity(const EntityID& entityID);
	void GetEntityUnderMouse(vec2f mouseWorldPos);
	// ---< Modify
private:
	template<class T>
	void RegisterComponentType(const ComponentType& componentType)
	{
		componentsFactories_[componentType] = []()->CBase* { return new T(); };
	}

	uint32 IDCounter_;
	EntityContainer entities_;
	std::unordered_map<EntityID, string> entityNames_;
	ComponentsFactories componentsFactories_;

	SystemManager* systemManager_;
	TextureManager* textureManager_;
public:
	vec2i GetEntityUserTile(int32 entityID);
	vec2i GetEntityReceiverTile(int32 entityID);
};
