#include "SystemManager.h"
#include "EntityManager.h"

/********************************************************************************
 * Consturcter, in which we create all the systems.
 ********************************************************************************/
SystemManager::SystemManager()
	: entityManager_(nullptr)
{
	messageHandler_ = EntityMessageHandler::Instance();
	systems_[SystemType::State] = new SState(this);
	systems_[SystemType::Control] = new SControl(this);
	systems_[SystemType::Movement] = new SMovement(this);
	systems_[SystemType::Collision] = new SCollision(this);
	systems_[SystemType::SpriteAnimation] = new SSpriteAnimation(this);
	systems_[SystemType::Renderer] = new SRenderer(this);
	systems_[SystemType::Wall] = new SWall(this);
	systems_[SystemType::Floor] = new SFloor(this);
	systems_[SystemType::MovementNew] = new SMovementNew(this);
	systems_[SystemType::Storage] = new SStorage(this);
}


SystemManager::~SystemManager()
{
	/// @todo where do you purge entities
	PurgeSystems();
}


void
SystemManager::SetEntityManager(EntityManager* entityManager)
{
	if(!entityManager_)
	{
		entityManager_ = entityManager;
	}
}

EntityManager*
SystemManager::GetEntityManager()
{
	return entityManager_;
}

EntityMessageHandler*
SystemManager::GetMessageHandler()
{
	return messageHandler_;
}

/********************************************************************************
 * Add event to entity's event queue.
 ********************************************************************************/
void
SystemManager::AddEvent(const EntityID& entityID, const uint32& eventType)
{
	eventQueues_[entityID].AddEvent(eventType);
}

void
SystemManager::Update(real32 dt)
{
	for(auto& systemsItr : systems_)
	{
		SBase* system = systemsItr.second;
		system->Update(dt);
	}
	messageHandler_->Update(dt);
	HandleEvents();
}

/********************************************************************************
 * Handles each entity's EventQueue.
 ********************************************************************************/
void
SystemManager::HandleEvents()
{
	for(auto& eventQueuesItr : eventQueues_)
	{
		// TODO: Be aware of Ref/Copy!!!
		EntityEventQueue& eventQueue = eventQueuesItr.second;
		EntityID entityID = eventQueuesItr.first;
		uint32 eventType = 0;
		while(eventQueue.ProcessEvents(eventType))
		{
			for(auto& systemsItr : systems_)
			{
				SBase* system = systemsItr.second;
				if(system->HasEntity(entityID))
				{
					system->HandleEvent(entityID, (EntityEventType)eventType);
				}
			}
		}
	}
}

void
SystemManager::Draw(Window* window, uint32 elevation)
{
	//---> Find Renderer System.
	auto systemsItr = systems_.find(SystemType::Renderer);
	if(systemsItr == systems_.end()) { return; }
	SRenderer* sRenderer = (SRenderer*)systemsItr->second;

	// TODO: book 2 chapter 6(Unified Rendering System)
	sRenderer->Render(window, elevation);
}


void
SystemManager::EntityModified(const EntityID& entityID, const Bitmask& bitmask)
{
	for(auto& systemsItr : systems_)
	{
		SBase* system = systemsItr.second;
		if(system->FitsRequirements(bitmask))
		{
			if(!system->HasEntity(entityID))
			{
				system->AddEntity(entityID);
			}
		}
		else
		{
			if(system->HasEntity(entityID))
			{
				system->RemoveEntity(entityID);
			}
		}
	}
}


void
SystemManager::RemoveEntity(const EntityID& entityID)
{
	for(auto& systemsItr : systems_)
	{
		SBase* system = systemsItr.second;
		system->RemoveEntity(entityID);
	}
}

void
SystemManager::PurgeEntities()
{
	for(auto& systemsItr : systems_)
	{
		SBase* system = systemsItr.second;
		system->Purge();
	}
}

void
SystemManager::PurgeSystems()
{
	for(auto& systemsItr : systems_)
	{
		SBase* system = systemsItr.second;
		delete system;
	}
	systems_.clear();
}
