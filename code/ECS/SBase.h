#pragma once
#include <vector>
#include "../Bitmask.h"
#include "EntityManager.h"
#include "EntityEventQueue.h"
#include "Observer.h"
#include "ECSTypes.h"
#include "EntityEventTypes.h"

class SystemManager;
/**
   A base class for all the systems in ECS.
 */
class SBase : public Observer
{
public:
	SBase(const SystemType& type, SystemManager* systemManager);
	virtual ~SBase();

	virtual bool AddEntity(const EntityID& entityID);
	bool HasEntity(const EntityID& entityID);
	bool RemoveEntity(const EntityID& entityID);

	SystemType GetType();

	bool FitsRequirements(const Bitmask& bitmask);
	void Purge();

	virtual void Update(real32 dt) = 0;
	virtual void HandleEvent(const EntityID& entityID, const EntityEventType& eventType) = 0;
	int32 GetNumEntities() const;
protected:
	SystemType type_;
	std::vector<Bitmask> requirements_;
	std::vector<EntityID> entities_;

	SystemManager* systemManager_;
};
