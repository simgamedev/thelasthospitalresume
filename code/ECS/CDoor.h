#pragma once
#include <SFML/System/Vector2.hpp>
#include "CBase.h"


enum class DoorState {
	Closed,
	Open,
	InTransition,
};
/**
   Door Component of ECS.
 */
class CDoor : public CBase
{
public:
	CDoor()
		: CBase(ComponentType::Door)
		, doorState_(DoorState::Closed)
	{
	}
	~CDoor()
	{
	}

	void ReadIn(std::stringstream& stream)
	{
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CDoor" << std::endl;
	}


private:
	DoorState doorState_;
};
