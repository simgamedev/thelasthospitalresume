#pragma once
#include "SBase.h"
#include "CState.h"

class SState : public SBase
{
public:
	SState(SystemManager* systemManager);
	~SState();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);
private:
	void ChangeState(const EntityID& entityID, const EntityState& state, const bool& forceChange);
};
