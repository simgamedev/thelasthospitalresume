#pragma once
#include "CBase.h"
#include <SFML/Graphics.hpp>

#include "../Directions.h"
#include "../Window.h"
/**
   A base class for all drawable components of ECS.
 */
class CDrawable : public CBase
{
public:
	CDrawable(const ComponentType& type)
		: CBase(type)
	{
	}
	virtual ~CDrawable()
	{
	}

	virtual void UpdatePosition(const vec2f& position) = 0;
	virtual const vec2u& GetSize() = 0;
	virtual void Draw(Window* window, int32 zOrder) = 0;
	virtual Origin GetOrigin() const = 0;
private:
};
