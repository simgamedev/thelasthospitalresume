#pragma once
#include "CBase.h"
enum class FloorState { Normal = 0, Dirty, WornOut };


/**
   Floor Component of ECS.
 */
class CFloor : public CBase
{
public:
	CFloor()
		: CBase(ComponentType::Floor)
	{
	}
	~CFloor()
	{
	}

	void ReadIn(std::stringstream& stream)
	{
	}
private:
	FloorState floorState_;
};
