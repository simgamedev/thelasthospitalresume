#include "SSpriteAnimation.h"
#include "SystemManager.h"

/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
SSpriteAnimation::SSpriteAnimation(SystemManager* systemManager)
	: SBase(SystemType::SpriteAnimation, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Sprite);
	//requirement.TurnOnBit((uint32)ComponentType::State);
	requirements_.push_back(requirement);

	systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::StateChanged, this);
}

SSpriteAnimation::~SSpriteAnimation()
{
}

void
SSpriteAnimation::Update(real32 dt)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for(auto& entityID : entities_)
	{
		CSprite* cSprite = entityManager->GetComponent<CSprite>(entityID, ComponentType::Sprite);
		CState* cState = entityManager->GetComponent<CState>(entityID, ComponentType::State);

		// TOOD: if(!cSprite->IsAnimateable()) { continue; }
		cSprite->GetSprite()->Update(dt);

		/*
		const string& animName = cSprite->GetSprite()->GetCurrentAnim()->GetName();
		if(animName == "Attack")
		{
		}
		else if(animName == "Death" && !cSprite->GetSprite()->GetCurrentAnim()->IsPlaying())
		{
			/// @todo dying animation finished, so entity should be dead now.
		}
		else if (animName == "Walk")
		{
			std::cout << "SSpriteAnimation::Update::Walk" << std::endl;
		}
		*/
	}
}

void
SSpriteAnimation::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
}


/********************************************************************************
 * Our animation is entirely state based(each state corresponds to an animation).
 ********************************************************************************/
void
SSpriteAnimation::OnNotify(const Message& msg)
{
	if(HasEntity(msg.receiver))
	{
		EntityMessageType msgType = (EntityMessageType)msg.type;
		switch(msgType)
		{
			case EntityMessageType::StateChanged:
			{
				EntityState entityState = (EntityState)msg.intValue;
				switch(entityState)
				{
					case EntityState::Working:
					{
						string entityName = systemManager_->GetEntityManager()->GetEntityName(msg.receiver);
						if (entityName.find("XRayMachine") != std::string::npos)
						{
							ChangeAnimation(msg.receiver, "XRayMachine_00_Working", true, true);
						}
						if (entityName.find("PCRTestMachine") != std::string::npos)
						{
							ChangeAnimation(msg.receiver, "PCRTestMachine_00_Testing", true, true);
						}
					} break;

					case EntityState::GoodResult:
					{
						string entityName = systemManager_->GetEntityManager()->GetEntityName(msg.receiver);
						if (entityName.find("XRayMachine") != std::string::npos)
						{
							ChangeAnimation(msg.receiver, "XRayMachine_00_GoodResult", true, true);
						}
						if (entityName.find("PCRTestMachine") != std::string::npos)
						{
							ChangeAnimation(msg.receiver, "PCRTestMachine_00_Negative", true, true);
						}
					} break;

					case EntityState::BadResult:
					{
						string entityName = systemManager_->GetEntityManager()->GetEntityName(msg.receiver);
						if (entityName.find("PCRTestMachine") != std::string::npos)
						{
							ChangeAnimation(msg.receiver, "PCRTestMachine_00_Positive", true, true);
						}
						if (entityName.find("XRayMachine") != std::string::npos)
						{
							ChangeAnimation(msg.receiver, "XRayMachine_00_BadResult", true, true);
						}
					} break;

					case EntityState::Normal:
					{
						StopAnimation(msg.receiver);
					} break;

				}
			} break;
		}
	}
}

void
SSpriteAnimation::ChangeAnimation(const EntityID& entityID, const string& animName, bool play, bool loop)
{
	if (animName == "Idle")
	{
		//std::cout << "SSpriteAnimation::ChangeAnimation(IDLE)" << std::endl;
	}
	CSprite* cSprite = systemManager_->GetEntityManager()->GetComponent<CSprite>(entityID, ComponentType::Sprite);
	cSprite->GetSprite()->SetAnimation(animName, play, loop);
}

/******************************* StopAnimation **********************************
 *
 ********************************************************************************/
void
SSpriteAnimation::StopAnimation(const EntityID& entityID)
{
	CSprite* cSprite = systemManager_->GetEntityManager()->GetComponent<CSprite>(entityID, ComponentType::Sprite);
	cSprite->GetSprite()->StopAnimation();
}
