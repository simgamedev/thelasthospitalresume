#include "SCollision.h"
#include "SystemManager.h"
#include "SWall.h"
#include "../GameWorld/GameWorld.h"


/********************************************************************************
 * Constructor.
 ********************************************************************************/
SCollision::SCollision(SystemManager* systemManager)
	: SBase(SystemType::Collision, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Position);
	requirement.TurnOnBit((uint32)ComponentType::Collidable);
	requirements_.push_back(requirement);
	requirement.Clear();

	pGameWorld_ = nullptr;
}

SCollision::~SCollision()
{
}


void
SCollision::SetMap(GameWorld* map)
{
	pGameWorld_ = map;
}

void
SCollision::Update(real32 dt)
{
	if(!pGameWorld_) { return; }
	EntityManager* entityManager = systemManager_->GetEntityManager();
	/*
	for(auto& entityID : entities_)
	{
		CPosition* cPosition = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
		CCollidable* cCollidable = entityManager->GetComponent<CCollidable>(entityID, ComponentType::Collidable);

		cCollidable->SetPosition(cPosition->GetPosition());
		cCollidable->ResetCollidingFlags();
		//CheckOutOfBounds(cPosition, cCollidable);
		MapCollisions(entityID, cPosition, cCollidable);
	}
	EntityCollisions();
	*/
}

/********************************************************************************
 * Checks if entity goes out of game map.
 ********************************************************************************/
void
SCollision::CheckOutOfBounds(CPosition* cPos, CCollidable* cCol)
{
}


/********************************************************************************
 * Checks if entity collides with Tile.
 ********************************************************************************/
void
SCollision::EntityCollisions()
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for (auto itr = entities_.begin(); itr != entities_.end(); ++itr)
	{
		for (auto itr2 = std::next(itr); itr2 != entities_.end(); ++itr2)
		{
			CCollidable* cCol1 = entityManager->GetComponent<CCollidable>(*itr, ComponentType::Collidable);
			CSprite* cSprite1 = entityManager->GetComponent<CSprite>(*itr, ComponentType::Sprite);
			CPosition* cPos1 = entityManager->GetComponent<CPosition>(*itr, ComponentType::Position);
			CMovable* cMov1 = entityManager->GetComponent<CMovable>(*itr, ComponentType::Movable);
			CCollidable* cCol2 = entityManager->GetComponent<CCollidable>(*itr2, ComponentType::Collidable);
			CSprite* cSprite2 = entityManager->GetComponent<CSprite>(*itr2, ComponentType::Sprite);
			CMovable* cMov2 = entityManager->GetComponent<CMovable>(*itr2, ComponentType::Movable);
			sf::FloatRect aabb1 = cCol1->GetAABB(cSprite1->GetDirection());
			sf::FloatRect aabb2 = cCol2->GetAABB(cSprite2->GetDirection());
			if (aabb1.intersects(aabb2))
			{
				//std::cout << "The two entities should be colliding!" << std::endl;
				// ---> CASE1: only one entity is movable
				if (cMov1 == nullptr && cMov2 != nullptr ||
					cMov1 != nullptr && cMov2 == nullptr)
				{
					//std::cout << "A movable entity collding into a stationary entity." << std::endl;
					if (cMov1 != nullptr)
					{
						sf::FloatRect intersection;
						sf::FloatRect movingEntityAABB = cCol1->GetAABB(cSprite1->GetDirection());
						sf::FloatRect stationaryEntityAABB = cCol2->GetAABB(cSprite2->GetDirection());
						if (!movingEntityAABB.intersects(stationaryEntityAABB, intersection)) { continue;}
						// ---> Calculate Collision
						real32 xDiff = (movingEntityAABB.left + movingEntityAABB.width / 2) -
							(stationaryEntityAABB.left + stationaryEntityAABB.width / 2);
						real32 yDiff = (movingEntityAABB.top + movingEntityAABB.height / 2) -
							(stationaryEntityAABB.top + stationaryEntityAABB.height / 2);
						real32 resolve = 0.0f;
						if (std::abs(xDiff) > std::abs(yDiff))
						{
							std::cout << "Resolve XXXXXX" << std::endl;
							if (xDiff > 0.0f)
							{
								resolve = (stationaryEntityAABB.left + stationaryEntityAABB.width) - movingEntityAABB.left;
							}
							else
							{
								resolve = -((movingEntityAABB.left + movingEntityAABB.width) - stationaryEntityAABB.left);
							}
							cPos1->MoveBy(resolve, 0);
							cCol1->SetPosition(cPos1->GetPosition());
							systemManager_->AddEvent(*itr, (uint32)EntityEventType::CollidingX);
							cCol1->CollideOnX();
							std::cout << "Velocity on X: " << cMov1->GetVelocity().x << std::endl;
						}
						else
						{
							std::cout << "Resolve Y" << std::endl;
							if (yDiff > 0.0f)
							{
								resolve = stationaryEntityAABB.top + stationaryEntityAABB.height - movingEntityAABB.top;
							}
							else
							{
								resolve = -(movingEntityAABB.top + movingEntityAABB.height - stationaryEntityAABB.top);
							}
							cPos1->MoveBy(0, resolve);
							cCol1->SetPosition(cPos1->GetPosition());
							systemManager_->AddEvent(*itr, (uint32)EntityEventType::CollidingY);
							cCol1->CollideOnY();
							std::cout << "Velocity on Y: " << cMov1->GetVelocity().y << std::endl;
						}
						// ---<
					}
				}
				// ---<

				// ---> CASE2: two entities are all movable
				if (cMov1 && cMov2)
				{
					std::cout << "2 movable entities are colliding into each other" << std::endl;
				}
				// ---<
			}
		}
	}
}

/********************************************************************************
 * Checks if entity collides with Tile.
 ********************************************************************************/
void
SCollision::MapCollisions(const EntityID& entityID, CPosition* cPos, CCollidable* cCol)
{
	Collisions collisions;
	CheckCollisions(entityID, cPos, cCol, collisions);
	HandleColiisions(entityID, cPos, cCol, collisions);
}

void
SCollision::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
}


void
SCollision::OnNotify(const Message& msg)
{
}

/*********************************************************************************
 * Check Collisions.
 *********************************************************************************/
void
SCollision::CheckCollisions(const EntityID& entityID, CPosition* cPos, 
							CCollidable* cCollidable, Collisions& collisions)
{
	uint32 tileSize = pGameWorld_->GetTileSize();
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CSprite* cSprite = entityManager->GetComponent<CSprite>(entityID, ComponentType::Sprite);
	sf::FloatRect entityAABB = cCollidable->GetAABB(cSprite->GetDirection());
	int32 fromTileX = floor(entityAABB.left / tileSize);
	int32 toTileX = floor((entityAABB.left + entityAABB.width) / tileSize);
	int32 fromTileY = floor(entityAABB.top / tileSize);
	int32 toTileY = floor((entityAABB.top + entityAABB.height) / tileSize);

	for (int32 x = fromTileX; x <= toTileX; ++x)
	{
		for (int32 y = fromTileY; y <= toTileY; ++y)
		{
			for(int32 layer = cPos->GetElevation(); layer < cPos->GetElevation() + 1; ++layer)
			{
				Tile* tile = pGameWorld_->GetTile(x, y, layer);
				if (!tile) { continue;  }
				if (!tile->solid) { continue;  }
				sf::FloatRect tileAABB = sf::FloatRect(x * tileSize,
													   y * tileSize,
													   tileSize,
													   tileSize);
				sf::FloatRect intersection;
				entityAABB.intersects(tileAABB, intersection);
				real32 interArea = intersection.width * intersection.height;
				collisions.emplace_back(interArea, tile->tileInfo, tileAABB);
				break;
			}
		}
	}
}

/*********************************************************************************
 * Handle Collisions.
 * TODO: currently only works with sqaure collisions.
 *********************************************************************************/
void
SCollision::HandleColiisions(const EntityID& entityID, CPosition* cPos, CCollidable* cCollidable, Collisions& collisions)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CSprite* cSprite = entityManager->GetComponent<CSprite>(entityID, ComponentType::Sprite);
	sf::FloatRect entityAABB = cCollidable->GetAABB(cSprite->GetDirection());
	uint32 tileSize = pGameWorld_->GetTileSize();
	if (collisions.empty()) { return; }
	std::sort(collisions.begin(),
			  collisions.end(),
			  [](CollisionElement& element1, CollisionElement& element2)
			  {
				  return element1.area > element2.area;
			  });
	for (auto& collision : collisions)
	{
		sf::FloatRect myIntersection;
		if (!entityAABB.intersects(collision.tileBounds, myIntersection)) { continue; }
		real32 xDiff = (entityAABB.left + entityAABB.width / 2) -
			           (collision.tileBounds.left + (collision.tileBounds.width / 2));
		real32 yDiff = (entityAABB.top + (entityAABB.height / 2)) -
			           (collision.tileBounds.top + (collision.tileBounds.height / 2));
		real32 resolve = 0;
		if(std::abs(xDiff) > std::abs(yDiff))
		{
			std::cout << "Resolve On X Axis!!!" << std::endl;
			if (xDiff > 0)
			{
				resolve = (collision.tileBounds.left + tileSize) - entityAABB.left;
			}
			else
			{
				resolve = -((entityAABB.left + entityAABB.width) - collision.tileBounds.left);
			}
			cPos->MoveBy(resolve, 0);
			cCollidable->SetPosition(cPos->GetPosition());
			// TODO: but y axis should still has velocity!
			systemManager_->AddEvent(entityID, (uint32)EntityEventType::CollidingX);
			cCollidable->CollideOnX();
		}
		else
		{
			//std::cout << "Resolve on Y axis" << std::endl;
			if (yDiff > 0)
			{
				resolve = (collision.tileBounds.top + tileSize) - entityAABB.top;
			}
			else
			{
				resolve = -((entityAABB.top + entityAABB.height) - collision.tileBounds.top);
			}
			cPos->MoveBy(0, resolve);
			cCollidable->SetPosition(cPos->GetPosition());
			systemManager_->AddEvent(entityID, (uint32)EntityEventType::CollidingY);
			cCollidable->CollideOnY();
		}
	}
}

/*********************************************************************************
 * DEBUG Draw Collidables.
 *********************************************************************************/
void
SCollision::DEBUGDrawCollidables(sf::RenderTarget* renderTarget)
{
	/*
	// TODO: SharedContext singleton or not??
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for (int32 entityID : entities_)
	{
		CCollidable* cCollidable = entityManager->GetComponent<CCollidable>(entityID, ComponentType::Collidable);
		CSprite* cSprite = entityManager->GetComponent<CSprite>(entityID, ComponentType::Sprite);
		sf::RectangleShape collidableDrawable;
		sf::FloatRect aabb = cCollidable->GetAABB(cSprite->GetDirection());
		collidableDrawable.setPosition(aabb.left, aabb.top);
		collidableDrawable.setSize(vec2f(aabb.width, aabb.height));
		collidableDrawable.setFillColor(sf::Color(0, 255, 0, 100));
		renderTarget->draw(collidableDrawable);
	}
	*/
}

/*********************************************************************************
 * Entity Collides With Any Other Entity
 *********************************************************************************/
std::vector<sf::FloatRect>
SCollision::EntityCollidesWithAnyOtherEntity(const EntityID& tempEntityID)
{
	std::vector<sf::FloatRect> result;
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CCollidable* cTempCollidable = entityManager->GetComponent<CCollidable>(tempEntityID, ComponentType::Collidable);
	CSprite* cTempSprite = entityManager->GetComponent<CSprite>(tempEntityID, ComponentType::Sprite);
	sf::FloatRect tempEntityAABB = cTempCollidable->GetAABB(cTempSprite->GetDirection());
	for (auto& itr : entities_)
	{
		if (itr == tempEntityID) { continue; }
		CCollidable* cCollidable = entityManager->GetComponent<CCollidable>(itr, ComponentType::Collidable);
		CSprite* cSprite = entityManager->GetComponent<CSprite>(itr, ComponentType::Sprite);
		sf::FloatRect aabb = cCollidable->GetAABB(cSprite->GetDirection());
		sf::FloatRect intersection;
		if(aabb.intersects(tempEntityAABB, intersection))
		{
			result.push_back(intersection);
		}
	}
	return result;
}

/*********************************************************************************
 * Entity Collides With Wall
 *********************************************************************************/
std::vector<sf::FloatRect>
SCollision::EntityCollidesWithWalls(const EntityID& tempEntityID)
{
	std::vector<sf::FloatRect> result;
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CPosition* cPosTempEntity = entityManager->GetComponent<CPosition>(tempEntityID,
																	   ComponentType::Position);
	CCollidable* cColTempEntity = entityManager->GetComponent<CCollidable>(tempEntityID,
																		  ComponentType::Collidable);
	CSprite* cSpriteTempEntity = entityManager->GetComponent<CSprite>(tempEntityID,
																	  ComponentType::Sprite);
	sf::FloatRect aabbTempEntity = cColTempEntity->GetAABB(cSpriteTempEntity->GetDirection());
	//int8 numTilesSpanX = std::ceil(aabbTempEntity.width / TILE_SIZE);
	//int8 numTilesSpanY = std::ceil(aabbTempEntity.height / TILE_SIZE);
	int8 numTilesSpanX = 5;
	int8 numTilesSpanY = 5;
	SWall* sysWall = systemManager_->GetSystem<SWall>(SystemType::Wall);
	vec2u tilepos = vec2u(cPosTempEntity->GetTilePos());
	for(int8 x = -numTilesSpanX; x < numTilesSpanX; ++x)
	{
		for(int8 y = -numTilesSpanY; y < numTilesSpanY; ++y)
		{
			int32 wallID = sysWall->GetWallID(tilepos.x + x, tilepos.y - y);
			if(wallID != -1)
			{
				sf::FloatRect wallBaseRect((tilepos.x + x) * TILE_SIZE,
										   (tilepos.y - y) * TILE_SIZE,
										   TILE_SIZE,
										   TILE_SIZE);
				sf::FloatRect intersection;
				if (aabbTempEntity.intersects(wallBaseRect, intersection))
				{
					result.push_back(intersection);
				}
			}
		}
	}
	return result;
}

/******************************* CollidesWithStructures *************************
 * HasWall/Solid
 ********************************************************************************/
std::vector<vec2i>
SCollision::CollidesWithStructures(const EntityID& colliderID)
{
	std::vector<vec2i> collidingTiles;
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CCollidable* cCollidable = entityManager->GetComponent<CCollidable>(colliderID, ComponentType::Collidable);
	CPosition* cPos = entityManager->GetComponent<CPosition>(colliderID, ComponentType::Position);
	TileSpan tileSpan = cCollidable->GetTileSpan();

	// ---> Walls
	// ---> Entity itself collides with Walls
	SWall* sysWall = systemManager_->GetSystem<SWall>(SystemType::Wall);
	vec2i colliderTilePos = cPos->GetTilePos();
	for (int32 x = colliderTilePos.x;
		 x < colliderTilePos.x + tileSpan.x;
		 x++)
	{
		for (int32 y = colliderTilePos.y;
			 y > colliderTilePos.y - tileSpan.y;
			 y--)
		{
			if (sysWall->HasWall(x, y) ||
				pGameWorld_->GetTile(x, y)->solid)
				collidingTiles.push_back(vec2i(x, y));
		}
	}
	// ---< Entity itself collides with Walls
	// ---> Entity UserTile collides with Walls
	vec2i userTilePos = cPos->GetTilePos() + cCollidable->GetUserOffset();
	if (sysWall->HasWall(userTilePos.x, userTilePos.y))
		collidingTiles.push_back(userTilePos);
	// ---< Entity UserTile collides with Walls
	// ---< Walls
	return collidingTiles;
}

/******************************* CollidesWithEntities ***************************
 *
 ********************************************************************************/
std::vector<vec2i>
SCollision::CollidesWithEntities(const EntityID& colliderID)
{
	std::vector<vec2i> result;

	std::vector<vec2i> tilesTaken = GetTilesTakenByEntity(colliderID);

	for (vec2i tilePos : tilesTaken)
	{
		Tile* tile = pGameWorld_->GetTile(tilePos);
		if (tile->entityID_ != -1)
			result.push_back(tilePos);
	}


	return result;
}

/******************************* GetTilesTakenByEntity **************************
 *
 ********************************************************************************/
std::vector<vec2i>
SCollision::GetTilesTakenByEntity(const EntityID& entityID) const
{
	std::vector<vec2i> result;

	EntityManager* entityManager = systemManager_->GetEntityManager();
	CPosition* cPosition = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
	CCollidable* cCollidable = entityManager->GetComponent<CCollidable>(entityID, ComponentType::Collidable);

	vec2i originTilePos = cPosition->GetTilePos();
	vec2i tileSpan = cCollidable->GetTileSpan();
	for (int32 x = 0; x < tileSpan.x; x++)
	{
		for (int32 y = 0; y < tileSpan.y; y++)
		{
			vec2i tilePos = originTilePos + vec2i(x, -y);
			result.push_back(tilePos);
		}
	}
	// ---> Also add userTile and receiverTile 
	vec2i userTilePos = originTilePos + cCollidable->GetUserOffset();
	result.push_back(userTilePos);
	// TODO: Remove HOLES from the tiles list
	return result;
}



