#pragma once
#include "CBase.h"
#include "ItemTypes.h"
#include "../magic_enum.hpp"


/**
   Storage Component of ECS.
 */
class SStorage;
class CStorage : public CBase
{
	friend SStorage;
public:
	CStorage()
		: CBase(ComponentType::Storage)
	{
	}
	~CStorage()
	{
	}

	void ReadIn(std::stringstream& stream)
	{
		// item type
		string itemTypeStr;
		stream >> itemTypeStr;
		itemType_ = magic_enum::enum_cast<ItemType>(itemTypeStr).value();

		// num items
		stream >> numItems_;
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should Save CStorage" << std::endl;
		file << "|Component " << (int32)ComponentType::Storage << " ItemType|NumItems" << std::endl;
		file << "Component " << (int32)ComponentType::Storage << (int32)itemType_ << " " << numItems_ << std::endl;
	}
private:
	ItemType itemType_;
	uint32 numItems_;
};
