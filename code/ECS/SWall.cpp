#include "SWall.h"
#include "SystemManager.h"
#include "../GameWorld/GameWorld.h"

/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
SWall::SWall(SystemManager* systemManager)
	: SBase(SystemType::Wall, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Wall);
	requirements_.push_back(requirement);
	requirement.Clear();

	//systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::IsMoving, this);

	//gameMap_ = nullptr;
	for (int32 x = 0; x < 256; ++x)
	{
		for (int32 y = 0; y < 256; ++y)
		{
			wallGrid_[x][y] = -1;
		}
	}
}

SWall::~SWall(){}


/********************************************************************************
 * Update.
 ********************************************************************************/
void
SWall::Update(real32 dt)
{
}




void
SWall::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
	switch (eventType)
	{
	}
}



/********************************************************************************
 * Keeps checking if the entity just became idle.
 ********************************************************************************/
void
SWall::OnNotify(const Message& msg)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	EntityMessageType msgType = (EntityMessageType)msg.type;
	switch(msgType)
	{
		case EntityMessageType::IsMoving:
		{
		} break;
	}
}

/*********************************************************************************
 * Calculate Wall Shapes.
 *********************************************************************************/
void
SWall::CalculateWallShapes()
{
	std::cout << "Calculating Wall Shapes!" << std::endl;
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for (auto& itr : entities_)
	{
		CWall* cWall = entityManager->GetComponent<CWall>(itr, ComponentType::Wall);
		CPosition* cPos = entityManager->GetComponent<CPosition>(itr, ComponentType::Position);
		vec2u tilePos = vec2u(cPos->GetTilePos());
		// Calculate WallShape
		vec2u neighborTilePos = vec2u(tilePos.x - 1, tilePos.y - 1);
		bool neighborWall0 = HasWall(neighborTilePos);

		neighborTilePos = vec2u(tilePos.x, tilePos.y - 1);
		bool neighborWall1 = HasWall(neighborTilePos);

		neighborTilePos = vec2u(tilePos.x + 1, tilePos.y - 1);
		bool neighborWall2 = HasWall(neighborTilePos);

		neighborTilePos = vec2u(tilePos.x - 1, tilePos.y);
		bool neighborWall3 = HasWall(neighborTilePos);

		// NOTE: 4 is the wall we are checking
		neighborTilePos = vec2u(tilePos.x + 1, tilePos.y);
		bool neighborWall5 = HasWall(neighborTilePos);

		neighborTilePos = vec2u(tilePos.x - 1, tilePos.y + 1);
		bool neighborWall6 = HasWall(neighborTilePos);

		neighborTilePos = vec2u(tilePos.x, tilePos.y + 1);
		bool neighborWall7 = HasWall(neighborTilePos);

		neighborTilePos = vec2u(tilePos.x + 1, tilePos.y + 1);
		bool neighborWall8 = HasWall(neighborTilePos);

		WallShape wallShape = WallShape::H;
		// ---> 1 Upper Left Corner
		if (neighborWall5 && neighborWall7)
		{
			wallShape = WallShape::UL;
		}
		// ---> 2 Upper Right Corner
		if (neighborWall3 && neighborWall7)
		{
			wallShape = WallShape::UR;
		}
		// ---> 3 Bottom Left Corner
		if (neighborWall1 && neighborWall5)
		{
			wallShape = WallShape::BL;
		}
		// ---> 4 Bottom Right Corner
		if (neighborWall1 && neighborWall3)
		{
			wallShape = WallShape::BR;
		}
		// ---> 5 Horizontal
		if (neighborWall1 == false &&
			neighborWall7 == false &&
			(neighborWall3 || neighborWall5))
		{
			wallShape = WallShape::H;
			if (HasDoor(tilePos.x, tilePos.y))
			{
				wallShape = WallShape::H_Door;
			}
			else if (HasDoor(tilePos.x -1 , tilePos.y))
			{
				wallShape = WallShape::H_DoorRight;
			}
			else if (HasDoor(tilePos.x + 1, tilePos.y))
			{
				wallShape = WallShape::H_DoorLeft;
			}
		}
		// ---> 6 Vertical
		if (neighborWall3 == false &&
			neighborWall5 == false &&
			neighborWall1 &&
			neighborWall7)
		{
			if (HasDoor(tilePos.x, tilePos.y))
			{
				wallShape = WallShape::V_Door;
			}
			else
			{
				wallShape = WallShape::V;
			}
		}
		// ---> 7 T Shaped Up
		if (neighborWall0 == false &&
			neighborWall2 == false &&
			neighborWall7 == false &&
			neighborWall1 &&
			neighborWall3 &&
			neighborWall5)
		{
			wallShape = WallShape::TU;
		}
		// ---> 8 T Shaped Down
		if (neighborWall1 == false &&
			neighborWall6 == false &&
			neighborWall8 == false &&
			neighborWall3 &&
			neighborWall5 &&
			neighborWall7)
		{
			wallShape = WallShape::TD;
		}
		// ---> 9 T Shaped Left
		if (neighborWall0 == false &&
			neighborWall5 == false &&
			neighborWall6 == false &&
			neighborWall1 &&
			neighborWall3 &&
			neighborWall7)
		{
			wallShape = WallShape::TL;
		}
		// ---> 10 T Shaped Right
		if (neighborWall2 == false &&
			neighborWall3 == false &&
			neighborWall8 == false &&
			neighborWall1 &&
			neighborWall5 &&
			neighborWall7)
		{
			wallShape = WallShape::TR;
		}
		// ---> 11 Cross Shaped.
		if (neighborWall0 == false &&
			neighborWall2 == false &&
			neighborWall6 == false &&
			neighborWall8 == false &&
			neighborWall1 &&
			neighborWall3 &&
			neighborWall5 &&
			neighborWall7)
		{
			wallShape = WallShape::C;
		}
		// ---> 12 Duzi Up
		if (neighborWall1 == false &&
			neighborWall3 == false &&
			neighborWall5 == false &&
			neighborWall7)
		{
			wallShape = WallShape::DU;
		}
		// ---> 13 Duzi Down
		if (neighborWall3 == false &&
			neighborWall5 == false &&
			neighborWall7 == false &&
			neighborWall1)
		{
			wallShape = WallShape::DD;
		}
		// ---> 14 Duzi Left
		if (neighborWall1 == false &&
			neighborWall3 == false &&
			neighborWall7 == false &&
			neighborWall5)
		{
			wallShape = WallShape::DL;
		}
		// ---> 15 Duzi right
		if (neighborWall1 == false &&
			neighborWall5 == false &&
			neighborWall7 == false &&
			neighborWall3)
		{
			wallShape = WallShape::DR;
		}
		// ---> Island
		// FIXME: all these independent (if else)es are slowing the program down?
		if (neighborWall1 == false &&
			neighborWall3 == false &&
			neighborWall5 == false &&
			neighborWall7 == false)
		{
			wallShape = WallShape::Island;
		}
		// ---< Island

		SetWallShape(tilePos.x, tilePos.y, wallShape);
		/*
		// Set Wall Shape
		cWall->SetShape(wallShape);
		// Set SpriteRect
		CSprite* cSprite = entityManager->GetComponent<CSprite>(itr, ComponentType::Sprite);
		sf::IntRect rect = cSprite->GetSprite()->GetSpriteRect(WallShapeToString(wallShape), Direction::Down);
		cSprite->GetSprite()->CropSprite(rect);
		*/
	}
}

/*********************************************************************************
 * Has Wall or not at tilepos
 *********************************************************************************/
bool
SWall::HasWall(int32 tileX, int32 tileY) const
{
	return (wallGrid_[tileX][tileY] != -1);
}

bool
SWall::HasWall(vec2u tilePos) const
{
	return (wallGrid_[tilePos.x][tilePos.y] != -1);
}

/*********************************************************************************
 * Wall Shape To String.
 *********************************************************************************/
string
SWall::WallShapeToString(WallShape shape)
{
	switch (shape)
	{
		// 1
		case WallShape::UL:
		{
			return "WallPiece_UL";
		} break;
		// 2
		case WallShape::UR:
		{
			return "WallPiece_UR";
		} break;
		// 3
		case WallShape::BL:
		{
			return "WallPiece_BL";
		} break;
		// 4
		case WallShape::BR:
		{
			return "WallPiece_BR";
		} break;
		// 5
		case WallShape::H:
		{
			return "WallPiece_H";
		} break;
		// 6
		case WallShape::V:
		{
			return "WallPiece_V";
		} break;
		// 7
		case WallShape::TU:
		{
			return "WallPiece_TU";
		} break;
		// 8
		case WallShape::TD:
		{
			return "WallPiece_TD";
		} break;
		// 9
		case WallShape::TL:
		{
			return "WallPiece_TL";
		} break;
		// 10
		case WallShape::TR:
		{
			return "WallPiece_TR";
		} break;
		// 11
		case WallShape::C:
		{
			return "WallPiece_C";
		} break;
		// 12
		case WallShape::DU:
		{
			return "WallPiece_DU";
		} break;
		// 13
		case WallShape::DD:
		{
			return "WallPiece_DD";
		} break;
		// 14
		case WallShape::DL:
		{
			return "WallPiece_DL";
		} break;
		// 15
		case WallShape::DR:
		{
			return "WallPiece_DR";
		} break;
		// 16
		case WallShape::H_Door:
		{
			return "WallPiece_H_Door";
		} break;
		// 17
		case WallShape::H_DoorLeft:
		{
			return "WallPiece_H_DoorLeft";
		} break;
		// 18
		case WallShape::H_DoorRight:
		{
			return "WallPiece_H_DoorRight";
		} break;
		// 19
		case WallShape::V_Door:
		{
			return "WallPiece_V_Door";
		} break;
		// 20
		case WallShape::Island:
		{
			// FIXME: do it
			//return "WallPiece_Island";
			return "WallPiece_Island";
		} break;
	}
}

/*********************************************************************************
 * Update Wall Grid.
 *********************************************************************************/
void
SWall::UpdateWallGrid(const EntityID& entityID)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CPosition* cPos = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
	vec2u tilePos = vec2u(cPos->GetTilePos());
	wallGrid_[tilePos.x][tilePos.y] = entityID;
	int32 a = 100;
}
void
SWall::WallRemoved(int32 tileX, int32 tileY)
{
	wallGrid_[tileX][tileY] = -1;
}

/*********************************************************************************
 * Get Wall ID.
 *********************************************************************************/
int32
SWall::GetWallID(int32 tileX, int32 tileY) const
{
	return wallGrid_[tileX][tileY];
}

/*********************************************************************************
 * OK to add door.
 *********************************************************************************/
bool
SWall::OKToAddDoor(int32 tileX, int32 tileY)
{
	isDoorVertical_ = false;
	if (HasWall(tileX, tileY))
	{
		EntityManager* entityManager = systemManager_->GetEntityManager();
		int32 wallID = GetWallID(tileX, tileY);
		int32 leftWallID = GetWallID(tileX - 1, tileY);
		int32 rightWallID = GetWallID(tileX + 1, tileY);
		int32 leftLeftWallID = GetWallID(tileX - 2, tileY);
		int32 rightRightWallID = GetWallID(tileX + 2, tileY);
		CWall* cWall = entityManager->GetComponent<CWall>(wallID, ComponentType::Wall);
		if (wallID == -1 ||
			leftWallID == -1 ||
			leftLeftWallID == -1 ||
			rightWallID == -1 ||
			rightRightWallID == -1)
		{
	//		return false;
			if (cWall->GetShape() == WallShape::V)
			{
				isDoorVertical_ = true;
				// FIXME: temporarily disblae vertical door
				return false;
			}
			else
			{
				return false;
			}
		}
		CWall* cLeftWall = entityManager->GetComponent<CWall>(leftWallID, ComponentType::Wall);
		CWall* cLeftLeftWall = entityManager->GetComponent<CWall>(leftLeftWallID, ComponentType::Wall);
		CWall* cRightWall = entityManager->GetComponent<CWall>(rightWallID, ComponentType::Wall);
		CWall* cRightRightWall = entityManager->GetComponent<CWall>(rightRightWallID, ComponentType::Wall);


		if(cWall->GetShape() == WallShape::H &&
		   !cWall->HasDoor() &&
		   cLeftWall->GetShape() == WallShape::H &&
		   !cLeftWall->HasDoor() &&
		   !cLeftLeftWall->HasDoor() &&
		   cRightWall->GetShape() == WallShape::H &&
		   !cRightWall->HasDoor() &&
		   !cRightRightWall->HasDoor())
		{
			return true;
		}
		else
		{
			std::cout << "Can't add door here" << std::endl;
			return false;
		}
	}
	else
	{
		std::cout << "No wall to add door to!" << std::endl;
		return false;
	}
}

/*********************************************************************************
 * Add Door.
 *********************************************************************************/
bool
SWall::AddDoor(const string& doorEntityFileName, int32 tileX, int32 tileY)
{
	if (!OKToAddDoor(tileX, tileY))
	{
		return false;
	}

	EntityManager* entityManager = systemManager_->GetEntityManager();
	//int32 doorID = entityManager->AddEntity("Door_00");
	int32 doorID = -1;
	if (!isDoorVertical_)
		doorID = entityManager->AddEntity(doorEntityFileName);
	else
	{
		// FIXME: temporarily disable vertical doors
		return false;
		//doorID = entityManager->AddEntity(doorEntityFileName + "_Vertical");
	}

	CPosition* cPos = entityManager->GetComponent<CPosition>(doorID, ComponentType::Position);
	cPos->SetPosition(tileX * TILE_SIZE, (tileY + 1 ) * TILE_SIZE);
	int32 wallID = GetWallID(tileX, tileY);
	CWall* cWall = entityManager->GetComponent<CWall>(wallID, ComponentType::Wall);
	cWall->SetHasDoor(true);
	cWall->SetDoorID(doorID);

	// FIXME: should only recalculate nearby walls
	CalculateWallShapes();
}

bool
SWall::AddDoor(const string& doorEntityFileName,
			   const string& directory,
			   int32 tileX, int32 tileY)
{
	if (!OKToAddDoor(tileX, tileY))
	{
		return false;
	}

	EntityManager* entityManager = systemManager_->GetEntityManager();
	int32 doorID = entityManager->AddEntity(doorEntityFileName,
											directory);
	CSprite* cSprite = entityManager->GetComponent<CSprite>(doorID, ComponentType::Sprite);
	//auto rect = cSprite->GetSprite()->GetDefaultRect(Direction::Down);
	auto rect = cSprite->GetSprite()->GetSpriteRect("Frame_00", Direction::Down);
	cSprite->GetSprite()->CropSprite(rect);
	CPosition* cPos = entityManager->GetComponent<CPosition>(doorID, ComponentType::Position);
	cPos->SetPosition(tileX * TILE_SIZE, (tileY + 1 ) * TILE_SIZE);
	int32 wallID = GetWallID(tileX, tileY);
	CWall* cWall = entityManager->GetComponent<CWall>(wallID, ComponentType::Wall);
	cWall->SetHasDoor(true);
	cWall->SetDoorID(doorID);
}

/*********************************************************************************
 * Has Door.
 *********************************************************************************/
bool
SWall::HasDoor(int32 tileX, int32 tileY) const
{
	int32 wallID = GetWallID(tileX, tileY);
	if (wallID == -1) { return false; }
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CWall* cWall = entityManager->GetComponent<CWall>(wallID, ComponentType::Wall);
	return cWall->HasDoor();
}

/*********************************************************************************
 * Open Door.
 *********************************************************************************/
void
SWall::OpenDoor(int32 tileX, int32 tileY)
{
	if (!HasDoor(tileX, tileY)) { return; }
	EntityManager* entityManager = systemManager_->GetEntityManager();
	std::cout << "Should Play Door Open Animation!" << std::endl;
	int32 wallID = GetWallID(tileX, tileY);
	CWall* cWall = entityManager->GetComponent<CWall>(wallID, ComponentType::Wall);
	int32 doorID = cWall->GetDoorID();
	CSprite* cSprite = entityManager->GetComponent<CSprite>(doorID, ComponentType::Sprite);
	cSprite->GetSprite()->SetAnimation("Door_00", true, true);
}

/******************************* ChangeWallpaper ********************************
 *
 ********************************************************************************/
bool
SWall::ChangeWallpaper(int32 tileX, int32 tileY, GameWorld* gameWorld)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	int32 wallID = GetWallID(tileX, tileY);
	CSprite* cSprite = entityManager->GetComponent<CSprite>(wallID, ComponentType::Sprite);
	cSprite->GetSprite()->SetTexture("WallPieces_01");
	return true;
}

/******************************* BuildWall **************************************
 *
 ********************************************************************************/
bool
SWall::BuildWall(int32 tileX, int32 tileY,
				 GameWorld* gameWorld,
				 const string& wallEntityFilePath,
				 bool recalcWallShapes)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	int32 wallpieceID = entityManager->AddEntity(wallEntityFilePath);
	CPosition* cPos = entityManager->GetComponent<CPosition>(wallpieceID, ComponentType::Position);
	//cPos->SetPosition(vec2f(itr.shape.getPosition()));
	cPos->SetPosition(vec2f(tileX * TILE_SIZE, (tileY+1) * TILE_SIZE));
	UpdateWallGrid(wallpieceID);
	// update navgraph
	//gameWorld_->RemoveFromNavGraph(itr.tilepos);
	//!!!!!!!!!!!!!!!!!! what the 
	//Tile* tile = gameWorld_->GetTile(itr.tilepos.x, itr.tilepos.y, 0);
	//tile->tileType = TileType::Wall;
	//Room::FloodFill(itr.tilepos.x, itr.tilepos.y, gameWorld_);
	//tempWallPlaceHolders_.clear();
	gameWorld->RemoveFromNavGraph(vec2i(tileX, tileY));
	//!!!!!!!!!!!!!!!!!! what the 
	Tile* tile = gameWorld->GetTile(tileX, tileY, 0);
	tile->tileType = TileType::Wall;
	Room::FloodFill(tileX, tileY, gameWorld);
	if (recalcWallShapes)
		CalculateWallShapes();
	return true;
}

/******************************* BulldozeWall ***********************************
 *
 ********************************************************************************/
bool
SWall::BulldozeWall(int32 tileX, int32 tileY, GameWorld* gameWorld, bool recalcWallShapes)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	Tile* tile = gameWorld->GetTile(tileX, tileY, 0);
	// ---> Case: not wall
	if (tile->tileType != TileType::Wall)
		return false;
	// ---> Case: has door
	if (HasDoor(tileX, tileY))
		RemoveDoor(tileX, tileY, gameWorld);

	int32 wallpieceID = GetWallID(tileX, tileY);
	entityManager->RemoveEntity(wallpieceID);
	//sysWall->UpdateWallGrid(wallpieceID);
	WallRemoved(tileX, tileY);
	gameWorld->AddBackToNavGraph(vec2i(tileX, tileY));

	// ---> Set tiletype back to TileType::Foundation
	gameWorld->GetTileMap()->SetTile(tileX,
									 tileY,
									 0,
									 "PHC_Exterior2",
									 59);
	tile->tileType = TileType::Foundation;
	tile->roomID = 0;
	if (recalcWallShapes)
		CalculateWallShapes();
	gameWorld->Redraw(vec3i(tileX, tileY, 0),
					  vec3i(tileX, tileY, 0));
	return true;
}

/******************************* IsOKToPlaceWall ********************************
 *
 ********************************************************************************/
bool
SWall::IsOKToPlaceWall(int32 tileX, int32 tileY, GameWorld* gameWorld)
{
	Tile* tile = gameWorld->GetTile(tileX, tileY, 0);
	// ---> Case: no tile
	if (!tile) return false;
	// ---> Case: no foundation(/floor)
	if (tile->tileType != TileType::Foundation &&
		tile->tileType != TileType::Floor)
		return false;
	// ---> Case: Top Left Corner
	if (HasWall(tileX - 1, tileY) &&
		HasWall(tileX - 1, tileY - 1) &&
		HasWall(tileX, tileY - 1))
		return false;
	// ---> Case: Bottom Left Corner
	if (HasWall(tileX - 1, tileY) &&
		HasWall(tileX - 1, tileY + 1) &&
		HasWall(tileX, tileY + 1))
		return false;
	// ---> Case: Top Right Corner
	if (HasWall(tileX + 1, tileY) &&
		HasWall(tileX + 1, tileY - 1) &&
		HasWall(tileX, tileY - 1))
		return false;
	// ---> Case: Bottom Right Corner
	if(HasWall(tileX + 1, tileY) &&
	   HasWall(tileX + 1, tileY + 1) &&
	   HasWall(tileX, tileY + 1))
		return false;
	// ---> Case: Blocking Door
	if (HasDoor(tileX, tileY - 1) ||
		HasDoor(tileX, tileY + 1))
		return false;
	return true;
}


void
SWall::SetWallShape(const int32 tileX, const int32 tileY, const WallShape wallShape)
{
	// Set Wall Shape
	int32 wallID = GetWallID(tileX, tileY);
	if (wallID == -1) return;

	EntityManager* entityManager = systemManager_->GetEntityManager();
	CWall* cWall = entityManager->GetComponent<CWall>(wallID, ComponentType::Wall);
	cWall->SetShape(wallShape);
	// Set SpriteRect
	CSprite* cSprite = entityManager->GetComponent<CSprite>(wallID, ComponentType::Sprite);
	sf::IntRect rect = cSprite->GetSprite()->GetSpriteRect(WallShapeToString(wallShape), Direction::Down);
	cSprite->GetSprite()->CropSprite(rect);
}
/******************************* RemoveDoor *************************************
 *
 ********************************************************************************/
void
SWall::RemoveDoor(const int32 tileX, const int32 tileY, GameWorld* gameWorld)
{
	if (!HasDoor(tileX, tileY))
		return;

	// ---> Remove Door Entity
	int32 wallID = GetWallID(tileX, tileY);
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CWall* cWall = entityManager->GetComponent<CWall>(wallID, ComponentType::Wall);
	int32 doorID = cWall->GetDoorID();
	entityManager->RemoveEntity(doorID);
	// ---< Remove Door Entity
	cWall->SetHasDoor(false);
	cWall->SetDoorID(-1);
	gameWorld->RemoveFromNavGraph(vec2i(tileX, tileY));

	// ---> Set wallshape back to WallShape::H
	SetWallShape(tileX, tileY, WallShape::H);
	SetWallShape(tileX - 1, tileY, WallShape::H);
	SetWallShape(tileX + 1, tileY, WallShape::H);
	// ---< Set wallshape back to WallShape::H
}

void
SWall::RemoveDoor(const int32 wallID)
{
}

