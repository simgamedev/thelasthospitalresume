#include "SState.h"
#include "SystemManager.h"


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
SState::SState(SystemManager* systemManager)
	: SBase(SystemType::State, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::State);
	requirements_.push_back(requirement);

	systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::StartMoving, this);
	/// @todo what's the difference between StateChanged and SwitchState????
	systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::SwitchState, this);
}

SState::~SState()
{
}

void
SState::Update(real32 dt)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for(auto& entityID : entities_)
	{
		CState* cState = entityManager->GetComponent<CState>(entityID, ComponentType::State);
		/*
		if(cState->GetState() == EntityState::Walking)
		{
			Message msg((MessageType)EntityMessageType::IsMoving);
			msg.receiver = entityID;
			systemManager_->GetMessageHandler()->Dispatch(msg);
		}
		*/
	}
}

void
SState::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
	switch(eventType)
	{
		case EntityEventType::BecameIdle:
		{
			//ChangeState(entityID, EntityState::Idle, false);
			//std::cout << "SState: Get EntityEventType::BecameIdle" << std::endl;
		} break;
	}
}

void
SState::OnNotify(const Message& msg)
{
	if(!HasEntity(msg.receiver)) { return; }
	EntityMessageType msgType = (EntityMessageType)msg.type;
	switch(msgType)
	{
		/*
		case EntityMessageType::StartMoving:
		{
			CState* cState = systemManager_->GetEntityManager()->GetComponent<CState>(msg.receiver, ComponentType::State);

			if(cState->GetState() == EntityState::Dying) { return; }
			EntityEventType entityEventType;
			if(msg.intValue == (int32)Direction::Up)
			{
				entityEventType = EntityEventType::MovingUp;
			}
			else if(msg.intValue == (int32)Direction::Down)
			{
				entityEventType = EntityEventType::MovingDown;
			}
			else if(msg.intValue == (int32)Direction::Left)
			{
				entityEventType = EntityEventType::MovingLeft;
			}
			else if(msg.intValue == (int32)Direction::Right)
			{
				entityEventType = EntityEventType::MovingRight;
			}

			systemManager_->AddEvent(msg.receiver, (uint32)entityEventType);
			//ChangeState(msg.receiver, EntityState::Walking, false);
		} break;

		case EntityMessageType::SwitchState:
		{
			ChangeState(msg.receiver, (EntityState)msg.intValue, false);
			//std::cout << "SState: Get EntityMessageType::SwitchState" << std::endl;
		} break;
		*/
	}
}

void
SState::ChangeState(const EntityID& entityID, const EntityState& state, const bool& forceChange)
{
	/*
	if (state == EntityState::Idle)
	{
		//std::cout << "SState::ChangeState: Should change to Idle" << std::endl;
	}
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CState* cState = entityManager->GetComponent<CState>(entityID, ComponentType::State);
	if(!forceChange && cState->GetState() == EntityState::Dying) { return; }
	cState->SetState(state);
	Message msg((MessageType)EntityMessageType::StateChanged);
	msg.receiver = entityID;
	msg.intValue = (int32)state;
	systemManager_->GetMessageHandler()->Dispatch(msg);
	*/
}
