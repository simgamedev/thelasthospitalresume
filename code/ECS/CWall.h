#pragma once
#include "CBase.h"
enum class WallShape { UL, UR, BL, BR, H, V, C, TU, TD, TL, TR, DU, DD, DL, DR ,
	H_Door, H_DoorLeft, H_DoorRight, V_Door, Island,
};


/**
   Wall Component of ECS.
 */
class CWall : public CBase
{
public:
	CWall()
		: CBase(ComponentType::Wall)
	{
	}
	~CWall()
	{
	}

	void ReadIn(std::stringstream& stream)
	{
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should Save CWall" << std::endl;
		file << "|Component " << (int32)ComponentType::Wall << " Wall|" << std::endl;
		file << "Component " << (int32)ComponentType::Wall << std::endl;
	}

	void SetShape(WallShape shape)
	{
		shape_ = shape;
	}
	WallShape GetShape() const
	{
		return shape_;
	}
	bool HasDoor() const
	{
		return hasDoor_;
	}
	void SetHasDoor(bool hasDoor)
	{
		hasDoor_ = hasDoor;
	}
	void SetDoorID(int32 doorID)
	{
		doorID_ = doorID;
	}
	int32 GetDoorID() const
	{
		return doorID_;
	}
private:
	WallShape shape_;
	bool hasDoor_ = false;
	int32 doorID_ = -1;
};
