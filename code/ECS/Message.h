#pragma once
#include "../common.h"
#include "../Time/Clock.h"
using MessageType = uint32;
struct TwoReal32s{ real32 x; real32 y; };
/**
   Message.
 */
struct Message
{
	Message(const MessageType& msgType)
		: type(msgType)
		, timeToDispatch(GameTime(0, 0, 0))

	{
	}

	MessageType type;
	int32 sender;
	int32 receiver;
	GameTime timeToDispatch;

	union
	{
		/// @todo why can't we use vec2f here?
		TwoReal32s twoReal32;
		bool boolValue;
		int32 intValue;
	};
};
