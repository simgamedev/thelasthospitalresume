#pragma once
#include "Message.h"

class Observer
{
public:
	virtual ~Observer(){}
	virtual void OnNotify(const Message& msg) = 0;
};
