#include "EntityManager.h"
#include "SystemManager.h"


/********************************************************************************
 * Contrustor, register all the component types here.
 ********************************************************************************/
EntityManager::EntityManager(SystemManager* systemManager, TextureManager* textureManager)
	: IDCounter_(0)
	, systemManager_(systemManager)
	, textureManager_(textureManager)
{
	RegisterComponentType<CPosition>(ComponentType::Position);
	RegisterComponentType<CSprite>(ComponentType::Sprite);
	RegisterComponentType<CState>(ComponentType::State);
	RegisterComponentType<CMovable>(ComponentType::Movable);
	RegisterComponentType<CController>(ComponentType::Controller);
	RegisterComponentType<CCollidable>(ComponentType::Collidable);
	RegisterComponentType<CWall>(ComponentType::Wall);
	RegisterComponentType<CDoor>(ComponentType::Door);
	RegisterComponentType<CMovableNew>(ComponentType::MovableNew);
	RegisterComponentType<CStorage>(ComponentType::Storage);
	RegisterComponentType<CSimpleSprite>(ComponentType::SimpleSprite);
}

EntityManager::~EntityManager()
{
	Purge();
}

/********************************************************************************
 * Returns the ID of added entity, -1 when failed to add an entity.
 ********************************************************************************/
int32 EntityManager::AddEntity(const Bitmask& bitmask)
{
	uint32 entityID = IDCounter_;
	if(!entities_.emplace(entityID, EntityData(0, ComponentContainer())).second)
	{
		return -1;
	}
	++IDCounter_;
	for(uint32 i = 0; i < NUM_COMPONENT_TYPES; ++i)
	{
		if(bitmask.GetBit(i))
		{
			AddComponent(entityID, (ComponentType)i);
		}
	}

	systemManager_->EntityModified(entityID, bitmask);
	systemManager_->AddEvent(entityID, (uint32)EntityEventType::Spawned);
	return entityID;
}


/*********************************************************************************
 * Add Entity with file from specified directory
 *********************************************************************************/
int32 
EntityManager::AddEntity(const string& entityFileName, const string& directory)
{
	int32 entityID = -1;

	std::ifstream entityFile;
	entityFile.open(directory + entityFileName);
	if(!entityFile.is_open())
	{
		std::cout << "Failed to load entity from file: " << entityFileName << std::endl;
		return -1;
	}
	string line;
	string entityName;
	while(std::getline(entityFile, line))
	{
		if(line[0] == '|')
		{
			continue;
		}
		std::stringstream keystream(line);
		string type;
		keystream >> type;
		if(type == "Name")
		{
			keystream >> entityName;
		}
		else if(type == "Attributes")
		{
			if(entityID != -1) { continue; }
			uint32 bits = 0;
			Bitmask bitmask;
			keystream >> bits;
			bitmask.SetBits(bits);
			entityID = AddEntity(bitmask);
			// Failed to add entity
			if(entityID == -1)
			{
				return -1;
			}
		}
		else if(type == "Component")
		{
			if(entityID == -1) { continue; }
			uint32 componentType = 0;
			keystream >> componentType;
			CBase* component = GetComponent<CBase>(entityID, (ComponentType)componentType);
			if(!component) { continue; }
			// Read component data in
			keystream >> *component;
			/// @todo Is there is better way to do below?
			// TODO: Is there a better way to do below?
			if(component->GetType() == ComponentType::Sprite)
			{
				CSprite* cSprite = (CSprite*)component;
				cSprite->Create(textureManager_);
				//Sprite_->SetSpriteRect(rect);
				GameSprite* sprite = cSprite->GetSprite();
				sprite->CropSprite(sprite->GetDefaultRect(Direction::Down));
			}
			if (component->GetType() == ComponentType::SimpleSprite)
			{
				CSimpleSprite* cSimpleSprite = (CSimpleSprite*)component;
				cSimpleSprite->Create(textureManager_);
			}
		}
	}
	entityNames_.emplace(entityID, entityName);
	entityFile.close();
	return entityID;
}
/********************************************************************************
 * Returns the ID of added entity, -1 when failed to add an entity.
 @todo doxygen test
 ********************************************************************************/
int32 EntityManager::AddEntity(const string& entityFilePath)
{
	int32 entityID = -1;

	std::ifstream entityFile;
	//entityFile.open(Utils::GetMediaDirectory() + "Entities/" + entityFilename + ".entity");
	entityFile.open(Utils::GetMediaDirectory() + "Entities/" + entityFilePath);
	if(!entityFile.is_open())
	{
		LOG_ENGINE_ERROR("Failed to load entity from file: " + entityFilePath);
		return -1;
	}
	string line;
	string entityName;
	while(std::getline(entityFile, line))
	{
		if(line[0] == '|')
		{
			continue;
		}
		std::stringstream keystream(line);
		string type;
		keystream >> type;
		if(type == "Name")
		{
			keystream >> entityName;
		}
		else if(type == "Attributes")
		{
			if(entityID != -1) { continue; }
			uint32 bits = 0;
			Bitmask bitmask;
			keystream >> bits;
			bitmask.SetBits(bits);
			entityID = AddEntity(bitmask);
			// Failed to add entity
			if(entityID == -1)
			{
				return -1;
			}
		}
		else if(type == "Component")
		{
			if(entityID == -1) { continue; }
			uint32 componentType = 0;
			keystream >> componentType;
			CBase* component = GetComponent<CBase>(entityID, (ComponentType)componentType);
			if(!component) { continue; }
			// Read component data in
			keystream >> *component;
			/// @todo Is there is better way to do below?
			if(component->GetType() == ComponentType::Sprite)
			{
				CSprite* cSprite = (CSprite*)component;
				cSprite->Create(textureManager_);
				//Sprite_->SetSpriteRect(rect);
				GameSprite* sprite = cSprite->GetSprite();
				sprite->CropSprite(sprite->GetDefaultRect(Direction::Down));
			}
			// TODO: why are there 2 different AddEntity???
			if (component->GetType() == ComponentType::SimpleSprite)
			{
				CSimpleSprite* cSimpleSprite = (CSimpleSprite*)component;
				cSimpleSprite->Create(textureManager_);
			}
		}
	}
	entityNames_.emplace(entityID, entityName);
	entityFile.close();
	return entityID;
}

/********************************************************************************
 * Load Entity From Saved Game.
 * TODO: it's the same with AddEntity(), combine them.
 ********************************************************************************/
int32 EntityManager::LoadEntity(const string& entityFilename)
{
	int32 entityID = -1;

	std::ifstream entityFile;
	entityFile.open(Utils::GetMediaDirectory() + "/SavedEntities/" + entityFilename);
	if(!entityFile.is_open())
	{
		std::cout << "Failed to load entity from file: " << entityFilename << std::endl;
		return -1;
	}
	string line;
	string entityName;
	while(std::getline(entityFile, line))
	{
		if(line[0] == '|')
		{
			continue;
		}
		std::stringstream keystream(line);
		string type;
		keystream >> type;
		if(type == "Name")
		{
			keystream >> entityName;
		}
		else if(type == "Attributes")
		{
			if(entityID != -1) { continue; }
			uint32 bits = 0;
			Bitmask bitmask;
			keystream >> bits;
			bitmask.SetBits(bits);
			entityID = AddEntity(bitmask);
			// Failed to add entity
			if(entityID == -1)
			{
				return -1;
			}
		}
		else if(type == "Component")
		{
			if(entityID == -1) { continue; }
			uint32 componentType = 0;
			keystream >> componentType;
			CBase* component = GetComponent<CBase>(entityID, (ComponentType)componentType);
			if(!component) { continue; }
			// Read component data in
			keystream >> *component;
			/// @todo Is there is better way to do below?
			if(component->GetType() == ComponentType::Sprite)
			{
				CSprite* cSprite = (CSprite*)component;
				cSprite->Create(textureManager_);
				//Sprite_->SetSpriteRect(rect);
				GameSprite* sprite = cSprite->GetSprite();
				sprite->CropSprite(sprite->GetDefaultRect(Direction::Down));
			}
		}
	}
	entityNames_.emplace(entityID, entityName);
	entityFile.close();
	return entityID;
}
/********************************************************************************
 * Add Entity at specific location.
 ********************************************************************************/
/*
int32
EntityManager::AddEntity(const vec2u& tilepos, const string& entityFilename)
{
}
*/

/********************************************************************************
 * Remove an entity and all its data.
 ********************************************************************************/
bool
EntityManager::RemoveEntity(const EntityID& entityID)
{
	auto entitiesItr = entities_.find(entityID);
	if(entitiesItr == entities_.end()) { return false; }
	// delete all it's components
	auto components = entitiesItr->second.second;
	while(components.begin() != components.end())
	{
		delete components.back();
		components.pop_back();
	}
	entities_.erase(entitiesItr);
	systemManager_->RemoveEntity(entityID);
	return true;
}


bool
EntityManager::AddComponent(const EntityID& entityID, const ComponentType& componentType)
{
	auto entitiesItr = entities_.find(entityID);
	if(entitiesItr == entities_.end()) { return false; }
	Bitmask& entityBitmask = entitiesItr->second.first;
	auto& entityComponents = entitiesItr->second.second;
	// component already exists.
	if(entityBitmask.GetBit((uint32)componentType)) { return false; }
	auto factoriesItr = componentsFactories_.find(componentType);
	if(factoriesItr == componentsFactories_.end()) { return false; }
	// create component from factory.
	CBase* component = factoriesItr->second();
	entityComponents.emplace_back(component);
	entityBitmask.TurnOnBit((uint32)componentType);

	systemManager_->EntityModified(entityID, entityBitmask);
	return true;
}

bool
EntityManager::RemoveComponent(const EntityID& entityID, const ComponentType& componentType)
{
	auto entitiesItr = entities_.find(entityID);
	if(entitiesItr == entities_.end()) { return false; } // Entity doesn't exist.
	Bitmask entityBitmask = entitiesItr->second.first;
	if(!entityBitmask.GetBit((uint32)componentType)) { return false; } // Component doesn't exist.
	auto entityComponents = entitiesItr->second.second;
	auto componentsItr = std::find_if(entityComponents.begin(), entityComponents.end(),
									  [componentType](CBase* component)
									  {
										  return component->GetType() == componentType;
									  });
	if(componentsItr == entityComponents.end()) { return false; }
	delete (*componentsItr);
	entityComponents.erase(componentsItr);
	entityBitmask.TurnOffBit((uint32)componentType);

	systemManager_->EntityModified(entityID, entityBitmask);
	return true;
}


/********************************************************************************
 * Query if entity has component.
 ********************************************************************************/
bool
EntityManager::HasComponent(const EntityID& entityID, const ComponentType& componentType)
{
	auto entitiesItr = entities_.find(entityID);
	if(entitiesItr == entities_.end()) { return false; }
	Bitmask entityBitmask = entitiesItr->second.first;
	return entityBitmask.GetBit((uint32)componentType);
}


/********************************************************************************
 * Purge everything.
 ********************************************************************************/
void
EntityManager::Purge()
{
	systemManager_->PurgeEntities();

	for(auto& entitiesItr : entities_)
	{
		auto entityComponents = entitiesItr.second.second;
		Bitmask entityBitmask = entitiesItr.second.first;
		for(auto& component : entityComponents)
		{
			delete component;
		}
		entityComponents.clear();
		entityBitmask.Clear();
	}
	entities_.clear();
	IDCounter_ = 0;
}

/*********************************************************************************
 * Rotate Entity Clockwise.
 *********************************************************************************/
void
EntityManager::RotateEntityClockwise(int32 entityID)
{
	// sprite
	CSprite* cSprite = GetComponent<CSprite>(entityID, ComponentType::Sprite);
	Direction direction = cSprite->GetDirection();
	switch (direction)
	{
		case Direction::Up:
		{
			direction = Direction::Right;
		} break;
		case Direction::Down:
		{
			direction = Direction::Left;
		} break;
		case Direction::Left:
		{
			direction = Direction::Up;
		} break;
		case Direction::Right:
		{
				direction = Direction::Down;
		} break;
	}
	cSprite->SetDirection(direction);
	std::cout << "Direction should be changed" << std::endl;
	// collidable
	// position??
}

/*********************************************************************************
 * Get Entity Name.
 *********************************************************************************/
string
EntityManager::GetEntityName(const EntityID& entityID) const
{
	auto itr = entityNames_.find(entityID);
	if (itr == entityNames_.end())
	{
		return "InvalidEntityID";
	}
	else
	{
		return itr->second;
	}
}

/*********************************************************************************
 * Get Entity ID by name
 *********************************************************************************/
int32
EntityManager::GetEntityID(const string& name)
{
	for (auto& itr : entityNames_)
	{
		string entityName = itr.second;
		if (entityName.find(name, 0) != std::string::npos)
		{
			int32 entityID = itr.first;
			return entityID;
		}
	}
	return -1;
}

/*********************************************************************************
 * Save Entities To File.
 *********************************************************************************/
bool
EntityManager::SaveEntitiesToFile()
{
	for (auto& itr : entities_)
	{
		EntityID entityID = itr.first;
		if (entityID == 0) continue;
		ComponentContainer allComponents = itr.second.second;
		string saveFilePath = Utils::GetMediaDirectory() + "SavedEntities\\" + std::to_string(entityID) + ".entity";
		// ---> Walls Folder
		if (GetEntityName(entityID).find("WallPiece") != string::npos)
		{
			saveFilePath = Utils::GetMediaDirectory() + "SavedEntities\\Walls\\" + std::to_string(entityID) + ".entity";
		}
		// ---> Floors Folder
		else if (GetEntityName(entityID).find("Floor") != string::npos)
		{
			saveFilePath = Utils::GetMediaDirectory() + "SavedEntities\\Floors\\" + std::to_string(entityID) + ".entity";
		}
		// ---> Doors Folder
		else if (GetEntityName(entityID).find("Door") != string::npos)
		{
			saveFilePath = Utils::GetMediaDirectory() + "SavedEntities\\Doors\\" + std::to_string(entityID) + ".entity";
		}
		std::ofstream file(saveFilePath, std::ios::out);

		// write header
		file << "Name " << GetEntityName(entityID) << std::endl;
		Bitmask entityBitmask = itr.second.first;
		file << "Attributes " << entityBitmask.GetBits() << std::endl;
		// write components
		for (CBase* c : allComponents)
		{
			if(!file.is_open())
			{
				std::cout << "Failed to open/create file: " << saveFilePath <<
					" to save game entity!" << std::endl;
			} 
			c->WriteOut(file);
		}
		file.close();
	}
	return true;
}

/******************************* SetEntityPos ***********************************
 *
 ********************************************************************************/
void
EntityManager::SetEntityPos(const EntityID& entityID, const vec2f& pos)
{
	CPosition* cPos = GetComponent<CPosition>(entityID, ComponentType::Position);
	cPos->SetPosition(pos);
}

/******************************* SetEntityPos ***********************************
 *
 ********************************************************************************/
vec2f
EntityManager::GetEntityPos(const EntityID& entityID)
{
	CPosition* cPos = GetComponent<CPosition>(entityID, ComponentType::Position);
	return cPos->GetPosition();
}

/******************************* GetEntityTilePos *******************************
 *
 ********************************************************************************/
vec2i
EntityManager::GetEntityTilePos(const EntityID& entityID)
{
	CPosition* cPos = GetComponent<CPosition>(entityID, ComponentType::Position);
	return cPos->GetTilePos();
}

/******************************* TintEntity *************************************
 *
 ********************************************************************************/
void
EntityManager::TintEntity(const EntityID& entityID, const sf::Color& color)
{
	CSprite* cSprite = GetComponent<CSprite>(entityID, ComponentType::Sprite);
	if (!cSprite) return;
	cSprite->Tint(color);
}

void
EntityManager::UntintEntity(const EntityID& entityID)
{
	CSprite* cSprite = GetComponent<CSprite>(entityID, ComponentType::Sprite);
	if (!cSprite) return;
	cSprite->Tint(sf::Color::White);
}

/******************************* GetEntityUnderMouse ****************************
 *
 * TODO: entities should be put into SubRegions
 ********************************************************************************/
void
EntityManager::GetEntityUnderMouse(vec2f mouseWorldPos)
{
}

vec2i
EntityManager::GetEntityUserTile(int32 entityID)
{
	CPosition* cPos = GetComponent<CPosition>(entityID, ComponentType::Position);
	CCollidable* cCol = GetComponent<CCollidable>(entityID, ComponentType::Collidable);

	vec2i originTilePos = cPos->GetTilePos();
	vec2i userTilePos = originTilePos + cCol->GetUserOffset();

	return userTilePos;
}

vec2i
EntityManager::GetEntityReceiverTile(const int32 entityID)
{
	CPosition* cPos = GetComponent<CPosition>(entityID, ComponentType::Position);
	CCollidable* cCol = GetComponent<CCollidable>(entityID, ComponentType::Collidable);

	vec2i originTilePos = cPos->GetTilePos();
	vec2i userTilePos = originTilePos + cCol->GetUserOffset();

	return userTilePos;
}




