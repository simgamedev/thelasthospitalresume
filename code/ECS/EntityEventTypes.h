#pragma once

enum class EntityEventType
{
	Spawned,
		Despawned,
		CollidingX,
		CollidingY,
		MovingLeft,
		MovingRight,
		MovingUp,
		MovingDown,
		ElevationChanged,
		BecameIdle,
		BeganMoving,
		BeganAction,
};
