#pragma once
#include "CBase.h"
#include "../Directions.h"
#include <SFML/System/Vector2.hpp>


//FIXME:
/**
 * Refactor This whole class using proper physics.
 @todo use proper physics for easy understanding.
 */
class CMovable : public CBase
{
public:
	CMovable()
		: CBase(ComponentType::Movable)
		, velocityMax_(0.0f)
		, direction_(Direction::Down)
	{
	}

	void ReadIn(std::stringstream& stream)
	{
		stream >> velocityMax_ >> speed_.x >> speed_.y;

		uint32 direction = 0;
		stream >> direction;
		direction_ = (Direction)direction;
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CMovable" << std::endl;
	}

	// FIXME: clarify DIFFERENCE between speed_ and velocity???????
	const vec2f& GetVelocity() { return velocity_; }
	real32 GetMaxVelocity(){ return velocityMax_; }
	// FIXME: speed should be a scalar quantity
	const vec2f& GetSpeed() { return speed_; }
	const vec2f& GetAcceleration() { return acceleration_; }
	Direction GetDirection() { return direction_; }

	void SetVelocity(const vec2f& velocity) { velocity_ = velocity; }
	void SetMaxVelocity(real32 maxVelocity) { velocityMax_ = maxVelocity; }
	void SetSpeed(const vec2f& speed) { speed_ = speed; }
	void SetAcceleration(const vec2f& acceleration) { acceleration_ = acceleration; }
	void SetDirection(const Direction& direction) { direction_ = direction; }
	void AddVelocity(const vec2f& increment)
	{
		// NOTE: speed x and y should be the same
		velocity_ += increment;
		if(std::abs(velocity_.x) > velocityMax_)
		{
			velocity_.x = velocityMax_ * (velocity_.x / std::abs(velocity_.x));
		}

		if(std::abs(velocity_.y) > velocityMax_)
		{
			velocity_.y = velocityMax_ * (velocity_.y / std::abs(velocity_.y));
		}
	}

	void ApplyFriction(const vec2f& friction)
	{
		// ---> X Axis
		if(velocity_.x != 0 && friction.x != 0)
		{
			if(std::abs(velocity_.x) - std::abs(friction.x)  < 0)
			{
				velocity_.x = 0;
			}
			else
			{
				velocity_.x += (velocity_.x > 0 ? -1 * friction.x : friction.x);
			}
		}
		// ---<

		// ---> Y Axis
		if(velocity_.y != 0 && friction.y != 0)
		{
			if(std::abs(velocity_.y) - std::abs(friction.y) < 0)
			{
				velocity_.y = 0;
			}
			else
			{
				velocity_.y += (velocity_.y > 0 ? -1 * friction.y : friction.y);
			}
		}
		// ---<
	}

	/*
	void Accelerate(const vec2f& increment)
	{
		acceleration_ += increment;
	}
	*/

	//void Accelerate(real32 x, real32 y) { acceleration_ += vec2f(x, y); }

	void Move(const Direction& direction)
	{
		if (direction == Direction::Up)
		{
			acceleration_.y -= speed_.y;
		}
		else if (direction == Direction::Down)
		{
			acceleration_.y += speed_.y;
		}
		else if (direction == Direction::Left)
		{
			acceleration_.x -= speed_.x;
		}
		else if(direction == Direction::Right)
		{
			acceleration_.x += speed_.x;
		}
	}
private:
	vec2f velocity_;
	vec2f speed_; // NOTE: speed_ is the inital acceleration kickstart
	vec2f acceleration_;
	real32 velocityMax_;
	// TOOD: cMovable should not have a direction_.
	Direction direction_;
};
