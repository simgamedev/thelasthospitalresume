#pragma once
#include "CBase.h"
#include <SFML/Graphics/Rect.hpp>


using TileSpan = sf::Vector2i;
/**
   Component Collidable.
 */
class CCollidable : public CBase
{
public:
	CCollidable()
		: CBase(ComponentType::Collidable)
		, origin_(Origin::BottomLeft)
		, collidingOnX_(false)
		, collidingOnY_(false)
		, offset_(0.0f, 0.0f)
	{
	}

	void ReadIn(std::stringstream& stream)
	{
		uint32 origin = 0;
		string directionsAllowedString;
		stream >> directionsAllowedString; 
		// ---> Split directionsAllowedString
		std::istringstream iss(directionsAllowedString);
		string item;
		std::vector<string> directionStrings;
		while (std::getline(iss, item, '/')) {
			directionStrings.push_back(item);
		}
		for (auto& itr : directionStrings)
		{
			if (itr == "Up")
			{
				sf::FloatRect aabb;
				stream >> aabb.width >> aabb.height >> origin;
				origin_ = (Origin)origin;
				AABBs_.emplace(Direction::Up, aabb);
			}
			if (itr == "Down")
			{
				int32 userOffsetX, userOffsetY;
				stream >> tileSpan_.x >> tileSpan_.y >> origin >> userOffsetX >> userOffsetY;
				origin_ = (Origin)origin;

				userOffset_ = vec2i(userOffsetX, userOffsetY);
				/*
				sf::FloatRect aabb;
				stream >> aabb.width >> aabb.height >> origin;
				origin_ = (Origin)origin;
				AABBs_.emplace(Direction::Down, aabb);
				*/
			}
			if (itr == "Left")
			{
				sf::FloatRect aabb;
				stream >> aabb.width >> aabb.height >> origin;
				origin_ = (Origin)origin;
				AABBs_.emplace(Direction::Left, aabb);
			}
			if (itr == "Right")
			{
				sf::FloatRect aabb;
				stream >> aabb.width >> aabb.height >> origin;
				origin_ = (Origin)origin;
				AABBs_.emplace(Direction::Right, aabb);
			}
		}
		// ---<
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CCollidable" << std::endl;
		file << "|Component Collidable Up/Down/Left/Right width/height/origin|" << std::endl;
		file << "Component "
			<< (int32)ComponentType::Collidable << " ";
		int32 counter = 0; // calculate number of slashes to write
		for (auto& itr : AABBs_)
		{
			Direction dir = itr.first;
			file << magic_enum::enum_name(dir);
			counter++;
			if (counter == 1 && AABBs_.size() == 1)
			{
				// No need to write a slash
			}
			else if (counter < AABBs_.size())
			{
				file << "/";
			}
		}
		file << " ";
		for (auto& itr : AABBs_)
		{
			sf::FloatRect aabb = itr.second;
			file << aabb.width << " " << aabb.height << " " << (int32)origin_ << " ";
		}
	}

	const sf::FloatRect& GetAABB() { return AABB_; }
	// ---> TEMP
	const sf::FloatRect& GetAABB(const Direction& direction)
	{
		auto itr = AABBs_.find(direction);
		if(itr != AABBs_.end())
		{
			sf::FloatRect& aabb = itr->second;
			return aabb;
		}
	}
	// ---< TEMP
	bool IsCollidingOnX() { return collidingOnX_; }
	bool IsCollidingOnY() { return collidingOnY_; }

	void CollideOnX() { collidingOnX_ = true; }
	void CollideOnY() { collidingOnY_ = true; }

	void ResetCollidingFlags()
	{
		collidingOnX_ = false;
		collidingOnY_ = false;
	}

	void SetAAABB(const sf::FloatRect& AABB) { AABB_ = AABB; }
	void SetOrigin(const Origin& origin) { origin_ = origin; }
	void SetSize(const vec2f& size)
	{
		AABB_.width = size.x;
		AABB_.height = size.y;
	}

	void SetPosition(const vec2f& position)
	{
		switch(origin_)
		{
			case(Origin::TopLeft):
			{
				//AABB_.left = position.x + offset_.x;
				//AABB_.top = position.y + offset_.y;
				for (auto& itr : AABBs_)
				{
					sf::FloatRect& aabb = itr.second;
					aabb.left = position.x - (aabb.width / 2) + offset_.x;
					aabb.top = position.y - aabb.height + offset_.y;
				}
			} break;

			case(Origin::AbsCenter):
			{
				//AABB_.left = position.x - (AABB_.width / 2) + offset_.x;
				//AABB_.top = position.y - (AABB_.height / 2) + offset_.y;
				for (auto& itr : AABBs_)
				{
					sf::FloatRect& aabb = itr.second;
					aabb.left = position.x - (aabb.width / 2) + offset_.x;
					aabb.top = position.y - aabb.height + offset_.y;
				}
			} break;

			case(Origin::MidBottom):
			{
				for (auto& itr : AABBs_)
				{
					sf::FloatRect& aabb = itr.second;
					aabb.left = position.x - (aabb.width / 2) + offset_.x;
					aabb.top = position.y - aabb.height + offset_.y;
				}
			} break;

			case(Origin::BottomLeft):
			{
				for (auto& itr : AABBs_)
				{
					sf::FloatRect& aabb = itr.second;
					aabb.left = position.x;
					aabb.top = position.y - aabb.height;
				}
			} break;
		}

	}

	// get tilespan
	// NOTE: we currently only use Direction::Down
	TileSpan GetTileSpan(const Direction& direction = Direction::Down) const
	{
		return tileSpan_;
	}

	vec2i GetUserOffset(const Direction& direction = Direction::Down) const
	{
		return userOffset_;
	}
private:
	// NOTE: we currently use tileSpan_ instead of AABB_.
	sf::FloatRect AABB_;
	std::unordered_map<Direction, sf::FloatRect> AABBs_;
	vec2f offset_;
	Origin origin_;
	// [DOCUMEN] https://simgengames.com/docs/tlh/TileSpan
	TileSpan tileSpan_;
	// NOTE: userOffset 
	// [DOCUMEN] https://simgengames.com/docs/tlh/UserOffset
	vec2i userOffset_; 

	bool collidingOnX_;
	bool collidingOnY_;
};
