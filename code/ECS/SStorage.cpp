#include "SStorage.h"
#include "SystemManager.h"
#include "CStorage.h"
#include "../Messaging/CharMsgDispatcher.h"

/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
SStorage::SStorage(SystemManager* systemManager)
	: SBase(SystemType::SpriteAnimation, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Storage);
	//requirement.TurnOnBit((uint32)ComponentType::State);
	requirements_.push_back(requirement);

	systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::ItemTook, this);
	systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::ReadyToOperate, this);
}

SStorage::~SStorage()
{
}

void
SStorage::Update(real32 dt)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for(auto& entityID : entities_)
	{
	}
}

void
SStorage::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
}


/********************************************************************************
 * Our animation is entirely state based(each state corresponds to an animation).
 ********************************************************************************/
void
SStorage::OnNotify(const Message& msg)
{
	if(HasEntity(msg.receiver))
	{
		EntityMessageType msgType = (EntityMessageType)msg.type;
		switch(msgType)
		{
			case EntityMessageType::ItemTook:
			{
				ItemType itemType = (ItemType)msg.intValue;
				DecreaseItem(msg.receiver, itemType);
			} break;

		}
	}
}


/******************************* DecreaseItem ***********************************
 *
 ********************************************************************************/
void
SStorage::DecreaseItem(const EntityID& entityID, const ItemType& itemType)
{
	//TODO: #define GetComponent(entityID, ComponentType)
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CStorage* cStorage = entityManager->GetComponent<CStorage>(entityID, ComponentType::Storage);
	if(cStorage->numItems_ > 0)
		cStorage->numItems_--;
	// Need to refill this storage unit
	if (cStorage->numItems_ == 0)
	{
		vec2f entityPos = entityManager->GetEntityPos(entityID);
		Message msg((uint32)EntityMessageType::NeedRefill);
		msg.sender = entityID;
		msg.twoReal32.x = entityPos.x;
		msg.twoReal32.y = entityPos.y;
		EntityMessageHandler::Instance()->Dispatch(msg);
	}
}

/******************************* GetNumItem *************************************
 *
 ********************************************************************************/
int32
SStorage::GetNumItem(const EntityID& entityID, const ItemType& itemType)
{
	CStorage* cStorage = systemManager_->GetEntityManager()->GetComponent<CStorage>(entityID, ComponentType::Storage);
	return cStorage->numItems_;
}

/******************************* SetNumItem *************************************
 ********************************************************************************/
void
SStorage::SetNumItem(const EntityID& entityID, const ItemType& itemType, int32 quantity)
{
	CStorage* cStorage = systemManager_->GetEntityManager()->GetComponent<CStorage>(entityID, ComponentType::Storage);
	cStorage->numItems_ = quantity;
}
