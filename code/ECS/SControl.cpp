#include "SControl.h"
#include "SystemManager.h"


SControl::SControl(SystemManager* systemManager)
	: SBase(SystemType::Control, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Position);
	requirement.TurnOnBit((uint32)ComponentType::Movable);
	requirement.TurnOnBit((uint32)ComponentType::Controller);
	requirements_.push_back(requirement);
	requirement.Clear();
}

SControl::~SControl(){}


void
SControl::Update(real32 dt)
{
}

void
SControl::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
	switch(eventType)
	{
		case EntityEventType::MovingLeft:
		{
			MoveEntity(entityID, Direction::Left);
		} break;

		case EntityEventType::MovingRight:
		{
			MoveEntity(entityID, Direction::Right);
		} break;

		case EntityEventType::MovingUp:
		{
			MoveEntity(entityID, Direction::Up);
		} break;

		case EntityEventType::MovingDown:
		{
			MoveEntity(entityID, Direction::Down);
		} break;
	}
}

void
SControl::OnNotify(const Message& msg)
{
}

void
SControl::MoveEntity(const EntityID& entityID, const Direction& direction)
{
	CMovable* cMovable = systemManager_->GetEntityManager()->GetComponent<CMovable>(entityID, ComponentType::Movable);
	cMovable->Move(direction);
}
