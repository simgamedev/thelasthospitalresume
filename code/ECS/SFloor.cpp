#include "SFloor.h"
#include "SystemManager.h"
#include "../GameWorld/GameWorld.h"


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
SFloor::SFloor(SystemManager* systemManager)
	: SBase(SystemType::Floor, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Floor);
	requirements_.push_back(requirement);
	requirement.Clear();

	//systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::IsMoving, this);

	//gameMap_ = nullptr;
	for (int32 x = 0; x < 256; ++x)
	{
		for (int32 y = 0; y < 256; ++y)
		{
			floorGrid_[x][y] = -1;
		}
	}
}

SFloor::~SFloor(){}


/********************************************************************************
 * Update.
 ********************************************************************************/
void
SFloor::Update(real32 dt)
{
}




void
SFloor::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
	switch (eventType)
	{
	}
}



/********************************************************************************
 * Keeps checking if the entity just became idle.
 ********************************************************************************/
void
SFloor::OnNotify(const Message& msg)
{
}


/*********************************************************************************
 * Update Floor Grid.
 *********************************************************************************/
void
SFloor::UpdateFloorGrid(const EntityID& entityID)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	CPosition* cPos = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
	vec2u tilePos = vec2u(cPos->GetTilePos());
	floorGrid_[tilePos.x][tilePos.y] = entityID;

	// TODO: formal select floor style.
	CSprite* cSprite = entityManager->GetComponent<CSprite>(entityID, ComponentType::Sprite);
	CSimpleSprite* cSimpleSprite = entityManager->GetComponent<CSimpleSprite>(entityID, ComponentType::SimpleSprite);
	if (cSprite)
	{
		sf::IntRect rect = cSprite->GetSprite()->GetSpriteRect("Normal", Direction::Down);
		cSprite->GetSprite()->CropSprite(rect);
		cSprite->GetSprite()->SetOrigin(Origin::BottomLeft);
	}
	if (cSimpleSprite)
	{
		int32 a = 100;
		sf::Sprite* sprite = cSimpleSprite->GetSprite();
		int32 b = 100;
	}
}
void
SFloor::FloorRemoved(int32 tileX, int32 tileY)
{
	floorGrid_[tileX][tileY] = -1;
}


/*********************************************************************************
 * Has Floor.
 *********************************************************************************/
bool 
SFloor::HasFloor(int32 tileX, int32 tileY) const
{
	return (floorGrid_[tileX][tileY] != -1);
}
bool
SFloor::HasFloor(vec2u tilePos) const
{
	return (floorGrid_[tilePos.x][tilePos.y] != -1);
}

/*********************************************************************************
 * Floor State To String.
 *********************************************************************************/
string 
SFloor::FloorStateToString(FloorState floorState)
{
	switch (floorState)
	{
		case FloorState::Normal:
		{
			return "";
		} break;

		case FloorState::WornOut:
		{
			return "WornOut";
		} break;
	}
}

/*********************************************************************************
 * Get Floor ID;
 *********************************************************************************/
int32
SFloor::GetFloorID(int32 tileX, int32 tileY) const
{
	return floorGrid_[tileX][tileY];
}
