#pragma once
#include "CBase.h"

class CController : public CBase
{
public:
	CController()
		: CBase(ComponentType::Controller)
	{
	}
	void ReadIn(std::stringstream& stream){}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CController!" << std::endl;
	}
private:
};
