#pragma once
//#include <SFML/System/Vector2.hpp>
#include "CBase.h"
#include "../TileMap/TileInfo.h"


// TODO: proper place to define TILE_SIZE
/**
   Position Component of ECS.
 */
class CPosition : public CBase
{
public:
	CPosition()
		: CBase(ComponentType::Position)
		, elevation_(1)
	{
	}
	~CPosition()
	{
	}

	void ReadIn(std::stringstream& stream)
	{
		stream >> position_.x >> position_.y >> elevation_;
		// TODO: tile pos y should change to origin
		tilePos_ = vec2i(position_.x / TILE_SIZE, position_.y / TILE_SIZE);
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CPosition" << std::endl;
		// component 0 200 200 1
		file << "|Component Position x y elevation|" << std::endl;
		file << "Component "
			<< (int32)ComponentType::Position << " "
			<< position_.x << " "
			<< position_.y << " "
			<< elevation_ << std::endl;
	} 

	const vec2f& GetPosition() { return position_; }
	const vec2f& GetOldPosition() { return positionOld_; }
	uint32 GetElevation() { return elevation_; }

	void SetPosition(real32 x, real32 y)
	{
		positionOld_ = position_;
		position_ = vec2f(x, y);
		// TODO: tile pos y should change to origin
		tilePos_ = vec2i(position_.x / TILE_SIZE, position_.y / TILE_SIZE - 1);
	}

	void SetPosition(const vec2f& pos)
	{
		positionOld_ = position_;
		position_ = pos;
		tilePos_ = vec2i(position_.x / TILE_SIZE, position_.y / TILE_SIZE - 1);
	}

	void SetElevation(uint32 elevation)
	{
		elevation_ = elevation;
	}

	void MoveBy(real32 x, real32 y)
	{
		positionOld_ = position_;
		position_ += vec2f(x, y);
		tilePos_ = vec2i(position_.x / TILE_SIZE, position_.y / TILE_SIZE - 1);
	}

	void MoveBy(const vec2f& displacement)
	{
		positionOld_ = position_;
		position_ += displacement;
		tilePos_ = vec2i(position_.x / TILE_SIZE, position_.y / TILE_SIZE - 1);
	}
	vec2i GetTilePos() const
	{
		return tilePos_;
	}
private:
	vec2f position_;
	vec2f positionOld_;
	vec2i tilePos_;
	uint32 elevation_;
};
