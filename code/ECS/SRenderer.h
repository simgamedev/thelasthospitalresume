#pragma once
#include "SBase.h"
#include <SFML/Graphics.hpp>
#include <algorithm>
#include "../Window.h"


/**
   A Temp Renderer, will be replaced with a better/formal one.
 */
class SRenderer : public SBase
{
public:
	SRenderer(SystemManager* systemManager);
	~SRenderer();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);
	void Render(Window* window, uint32 elevation);
	int32 GetEntityUnderMouse(vec2f mouseWorldPos);
private:
	/// @todo what do we need SetSpriteDirection for??
	void SetSpriteDirection(const EntityID& entityID, const Direction& direction);
	void SortDrawables();
	//std::multimap<ZOrder, EntityID>
	std::multimap<int32, int32> entitiesUnderMouse_;
	int32 prevTopmostEntityUnderMouse_;
};
