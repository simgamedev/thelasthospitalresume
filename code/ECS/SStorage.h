#pragma once

#include "SBase.h"
#include "ItemTypes.h"

class SStorage : public SBase
{
public:
	SStorage(SystemManager* systemManager);
	~SStorage();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);
	int32 GetNumItem(const EntityID& entityID, const ItemType& itemType);
	void SetNumItem(const EntityID& entityID, const ItemType& itemType, int32 quantity);
private:
	void DecreaseItem(const EntityID& entityID, const ItemType& itemType);
};
