#pragma once
#include "SBase.h"

class GameWorld;
/**
   The system responsible for all the movables.
 */
class SMovementNew : public SBase
{
public:
	SMovementNew(SystemManager* systemManager);
	~SMovementNew();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);

	void SetMap(GameWorld* map);
private:

	GameWorld* gameMap_;
};
