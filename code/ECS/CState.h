#pragma once
#include "CBase.h"

//enum class EntityState{ Idle, Walking, Attacking, Hurt, Dying };
enum class EntityState { Normal, GoodResult, BadResult, OnGood, OnBad, Working };
enum class CharacterGlobalState{ Sitting };
enum class CharacterState{ Working, Sleeping, DroppingShit, EatingFood, EatingDrug, TakingInjection };


class CState : public CBase
{
public:
	CState()
		: CBase(ComponentType::State)
	{
	}

	void ReadIn(std::stringstream& stream)
	{
		uint32 state = 0;
		stream >> state;
		state_ = (EntityState)state;
	}

	void WriteOut(std::ofstream& file)
	{
		std::cout << "Should save CState" << std::endl;
	}

	EntityState GetState()
	{
		return state_;
	}

	void SetState(const EntityState& state)
	{
		state_ = state;
	}
private:
	EntityState state_;
};
