#pragma once
#include "SBase.h"

class SSpriteAnimation : public SBase
{
public:
	SSpriteAnimation(SystemManager* systemManager);
	~SSpriteAnimation();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);
private:
	void ChangeAnimation(const EntityID& entityID, const string& animName, bool play, bool loop);
	void StopAnimation(const EntityID& entityID);
};
