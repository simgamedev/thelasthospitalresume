#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include "ECSTypes.h"


/**
   A base class for all types of components of ECS.
 */
class CBase
{
public:
	CBase(const ComponentType& type)
		: type_(type)
	{
	}

	virtual ~CBase()
	{
	}

	ComponentType GetType() const { return type_;}

	friend std::stringstream& operator >> (std::stringstream& stream, CBase& cbase)
	{
		cbase.ReadIn(stream);
		return stream;
	}

	virtual void ReadIn(std::stringstream& stream) = 0;
	virtual void WriteOut(std::ofstream& file) = 0;
protected:
	ComponentType type_;
	// TODO: shoud we have a GetOwnerID()??
};
