#pragma once
#include "../common.h"
//using NUM_COMPONENT_TYPES = 32;
static const int32 NUM_COMPONENT_TYPES = 32;

enum class ComponentType
{
	    Position = 0,
		Sprite, // 1
		State, // 2
		Movable, // 3 xx
		Controller, // 4 xx
		Collidable, // 5
		Wall, // 6
		Door, // 7 
		Floor, // 8 xx
		MovableNew, // 9 xx
		Ownable, // 10 xx?
		Storage, // 11
		SimpleSprite, // 12
		Item, // 13
};

enum class SystemType
{
		Renderer = 0,
		Movement,
		Collision,
		Control,
		State,
		SpriteAnimation,
		Wall,
		Floor,
		MovementNew,
		Storage,
};
