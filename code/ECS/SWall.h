#pragma once
#include "SBase.h"
#include "CWall.h"

/**
   The system responsible for all the wallpieces.
 */
class GameWorld;
class SWall : public SBase
{
public:
	SWall(SystemManager* systemManager);
	~SWall();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);
	void CalculateWallShapes();
	void UpdateWallGrid(const EntityID& entityID);
	void WallRemoved(int32 tileX, int32 tileY);
	bool HasWall(int32 tileX, int32 tileY) const;
	bool HasWall(vec2u tilePos) const;
	bool HasDoor(int32 tileX, int32 tileY) const;
	int32 GetWallID(int32 tileX, int32 tileY) const;
	//int32 GetWallID(vec2u tilePos) const;
	bool AddDoor(const string& doorEntityFileName, int32 tileX, int32 tileY);
	bool AddDoor(const string& doorEntityFileName,
				 const string& directory,
				 int32 tileX, int32 tileY);
	void RemoveDoor(const int32 tileX, const int32 tileY, GameWorld* gameWorld);
	void RemoveDoor(const int32 wallID);
	void SetWallShape(const int32 tileX, const int32 tileY, const WallShape wallShape);
	void OpenDoor(int32 tileX, int32 tileY);
	void CloseDoor(int32 tileX, int32 tileY);
	bool OKToAddDoor(int32 tileX, int32 tileY);
	bool IsOKToPlaceWall(int32 tileX, int32 tileY, GameWorld* gameWorld);
	bool ChangeWallpaper(int32 tileX, int32 tileY, GameWorld* gameWorld);


	bool BuildWall(int32 tileX, int32 tileY,
				   GameWorld* gameWorld,
				   const string& wallEntityFilePath = "Walls/Base.entity",
				   bool recalcWallShapes = false);
	bool BulldozeWall(int32 tileX, int32 tileY, GameWorld* gameWorld, bool recalcWallShapes = false);
private:
	string WallShapeToString(WallShape shape);
	int32 wallGrid_[256][256];
	bool isDoorVertical_;
};
