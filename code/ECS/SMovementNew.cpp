#include "SMovementNew.h"
#include "SystemManager.h"
#include "../GameWorld/GameWorld.h"


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
SMovementNew::SMovementNew(SystemManager* systemManager)
	: SBase(SystemType::MovementNew, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Position);
	requirement.TurnOnBit((uint32)ComponentType::MovableNew);
	requirements_.push_back(requirement);
	requirement.Clear();

	gameMap_ = nullptr;
}

SMovementNew::~SMovementNew(){}


/********************************************************************************
 * Moves each entity by it's velocity * dt.
 ********************************************************************************/
void
SMovementNew::Update(real32 dt)
{
	if(!gameMap_) { return; }
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for(auto& entityID : entities_)
	{
	}
}




void
SMovementNew::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
}


/********************************************************************************
 * Keeps checking if the entity just became idle.
 ********************************************************************************/
void
SMovementNew::OnNotify(const Message& msg)
{
}


void
SMovementNew::SetMap(GameWorld* map)
{
	gameMap_ = map;
}
