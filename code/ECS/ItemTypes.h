#pragma once

enum class ItemType {
	LitterSnack = 0,
	LitterVomit,
	SnackSandwich,
	SnackCola,
	MedBloodPack,
	MedPill,
	MedPCRTestSwab,
};