#pragma once
#include "SBase.h"

enum class Axis{ X, Y };

class GameWorld;

/**
   The system responsible for all the movables.
 */
class SMovement : public SBase
{
public:
	SMovement(SystemManager* systemManager);
	~SMovement();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);

	void SetMap(GameWorld* map);
private:
	void StopEntity(const EntityID& entityID, const Axis& axis);
	void SetDirection(const EntityID& entityID, const Direction& direction);
	const vec2f& GetTileFriction(uint32 x, uint32 y, uint32 elevation);
	void MovementStep(real32 dt, CMovable* cMov, CPosition* cPos);

	GameWorld* gameMap_;
};
