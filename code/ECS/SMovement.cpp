#include "SMovement.h"
#include "SystemManager.h"
#include "../GameWorld/GameWorld.h"


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
SMovement::SMovement(SystemManager* systemManager)
	: SBase(SystemType::Movement, systemManager)
{
	Bitmask requirement;
	requirement.TurnOnBit((uint32)ComponentType::Position);
	requirement.TurnOnBit((uint32)ComponentType::Movable);
	requirements_.push_back(requirement);
	requirement.Clear();

	systemManager_->GetMessageHandler()->Subscribe(EntityMessageType::IsMoving, this);

	gameMap_ = nullptr;
}

SMovement::~SMovement(){}


/********************************************************************************
 * Moves each entity by it's velocity * dt.
 ********************************************************************************/
void
SMovement::Update(real32 dt)
{
	if(!gameMap_) { return; }
	EntityManager* entityManager = systemManager_->GetEntityManager();
	for(auto& entityID : entities_)
	{
		CPosition* cPos = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
		CMovable* cMov = entityManager->GetComponent<CMovable>(entityID, ComponentType::Movable);
		MovementStep(dt, cMov, cPos);
		cPos->MoveBy(cMov->GetVelocity() * dt);
	}
}


/********************************************************************************
 * Stop Entity On X/Y Axis.
 ********************************************************************************/
void
SMovement::StopEntity(const EntityID& entityID, const Axis& axis)
{
	CMovable* cMov = systemManager_->GetEntityManager()->GetComponent<CMovable>(entityID, ComponentType::Movable);
	if (axis == Axis::X)
	{
		cMov->SetVelocity(vec2f(0.0f, cMov->GetVelocity().y));
	}
	else if (axis == Axis::Y)
	{
		cMov->SetVelocity(vec2f(cMov->GetVelocity().x, 0.0f));
	}
}


void
SMovement::HandleEvent(const EntityID& entityID, const EntityEventType& eventType)
{
	switch(eventType)
	{
		case EntityEventType::CollidingX:
		{
			StopEntity(entityID, Axis::X);
		} break;

		case EntityEventType::CollidingY:
		{
			StopEntity(entityID, Axis::Y);
		} break;

		case EntityEventType::MovingUp:
		{
			CMovable* cMov = systemManager_->GetEntityManager()->GetComponent<CMovable>(entityID, ComponentType::Movable);
			if(cMov->GetVelocity().x == 0)
			{
				SetDirection(entityID, Direction::Up);
			}
		} break;

		case EntityEventType::MovingDown:
		{
			CMovable* cMov = systemManager_->GetEntityManager()->GetComponent<CMovable>(entityID, ComponentType::Movable);
			if(cMov->GetVelocity().y == 0)
			{
				SetDirection(entityID, Direction::Down);
			}
		} break;

		case EntityEventType::MovingLeft:
		{
			SetDirection(entityID, Direction::Left);
		} break;

		case EntityEventType::MovingRight:
		{
			SetDirection(entityID, Direction::Right);
		} break;
	}
}

const vec2f&
SMovement::GetTileFriction(uint32 x, uint32 y, uint32 elevation)
{
	Tile* tile = nullptr;

	/// @todo should only get the tile 1 layer below?
	/*
	while(!tile && elevation >= 0)
	{
		tile = gameMap_->GetTile(x, y, elevation);
		--elevation;
	}
	*/
	tile = gameMap_->GetTile(x, y, elevation);

	if(tile)
	{
		return tile->tileInfo->GetFriction();
	}
	else
	{
		return gameMap_->GetDefaultTileInfo()->GetFriction();
	}
}


/********************************************************************************
 * Keeps checking if the entity just became idle.
 ********************************************************************************/
void
SMovement::OnNotify(const Message& msg)
{
	EntityManager* entityManager = systemManager_->GetEntityManager();
	EntityMessageType msgType = (EntityMessageType)msg.type;
	switch(msgType)
	{
		/// @todo Figure out EXACTLY how the  movement/collsion works!!!!
		case EntityMessageType::IsMoving:
		{
			if(!HasEntity(msg.receiver)) { return; }
			CMovable* cMov = entityManager->GetComponent<CMovable>(msg.receiver, ComponentType::Movable);
			/// @todo maybe error prone? like a really small number?
			if(cMov->GetVelocity() != vec2f(0.0f, 0.0f)) { return; }
			systemManager_->AddEvent(msg.receiver, (uint32)EntityEventType::BecameIdle);
			//std::cout << "Entity Should Stop!" << std::endl;
		} break;
	}
}


void
SMovement::SetMap(GameWorld* map)
{
	gameMap_ = map;
}


void
SMovement::MovementStep(real32 dt, CMovable* cMov, CPosition* cPos)
{
	/*
	vec2u tilepos = vec2u(floor(cPos->GetPosition().x / gameMap_->GetTileSize()),
						  floor(cPos->GetPosition().y / gameMap_->GetTileSize()));
						  */
	vec2u tilepos = vec2u(floor(cPos->GetPosition().x / TILE_SIZE),
						  floor(cPos->GetPosition().y / TILE_SIZE));
	vec2f frictionCoefficient = GetTileFriction(tilepos.x, tilepos.y, cPos->GetElevation());

	/// @todo figure out HOW THE friction/movement WORKs!!!!!
	vec2f friction(cMov->GetSpeed().x * frictionCoefficient.x,
				   cMov->GetSpeed().y * frictionCoefficient.y);

	cMov->AddVelocity(cMov->GetAcceleration() * dt);
	cMov->SetAcceleration(vec2f(0.0f, 0.0f));
	cMov->ApplyFriction(friction * dt);

	/// @todo make diagnol movement magnitude same as x or y.
	//real32 magnitude = sqrt((cMov->GetVelocity().x * cMov->GetVelocity().x) +
	//					(cMov->GetVelocity().y * cMove->GetVelocity().y));
	//if(magnitude <= cMove->GetMaxSpeed()) { return; } // NOTE:
	real32 magnitude = sqrt(
		(cMov->GetVelocity().x * cMov->GetVelocity().x) +
		(cMov->GetVelocity().y * cMov->GetVelocity().y));

	if (magnitude <= cMov->GetMaxVelocity()) { return; }
	float max_V = cMov->GetMaxVelocity();
	cMov->SetVelocity(sf::Vector2f(
		(cMov->GetVelocity().x / magnitude) * max_V,
		(cMov->GetVelocity().y / magnitude) * max_V));
}


void
SMovement::SetDirection(const EntityID& entityID, const Direction& direction)
{
	CMovable* cMov = systemManager_->GetEntityManager()->GetComponent<CMovable>(entityID, ComponentType::Movable);
	cMov->SetDirection(direction);

	Message msg((uint32)EntityMessageType::DirectionChanged);
	msg.receiver = entityID;
	msg.intValue = (int32)direction;
	systemManager_->GetMessageHandler()->Dispatch(msg);
}
