#pragma once
#include "SBase.h"
#include "CFloor.h"

/**
   The system responsible for all the floor pieces.
 */
class SFloor : public SBase
{
public:
	SFloor(SystemManager* systemManager);
	~SFloor();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);
	void UpdateFloorGrid(const EntityID& entityID);
	void FloorRemoved(int32 tileX, int32 tileY);
	bool HasFloor(int32 tileX, int32 tileY) const;
	bool HasFloor(vec2u tilePos) const;
	int32 GetFloorID(int32 tileX, int32 tileY) const;
private:
	string FloorStateToString(FloorState floorState);
	int32 floorGrid_[256][256];
};
