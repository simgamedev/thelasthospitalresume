#pragma once
#include "CDrawable.h"
#include "../TextureManager.h"
#include <SFML/Graphics/Sprite.hpp>

class CSimpleSprite : public CDrawable
{
public:
	CSimpleSprite()
		: CDrawable(ComponentType::SimpleSprite)
		, sprite_(nullptr)
	{
	}
	~CSimpleSprite() { if(sprite_) { delete sprite_; } }

	void ReadIn(std::stringstream& stream)
	{
		stream >> textureName_;
		int32 originInt;
		//stream >> originInt;
		//origin_ = (Origin)originInt;
		origin_ = Origin::BottomLeft;
	}

	void WriteOut(std::ofstream& file)
	{
		file << "|Component SimpleSprite TextureName |" << std::endl;
		file << "Component " << (int32)ComponentType::SimpleSprite << " " << textureName_;
		file << std::endl;
	}

	void Create(TextureManager* textureManager)
	{
		if(sprite_) { return; }
		sprite_ = new sf::Sprite();
		sprite_->setTexture(*textureManager->GetResource(textureName_));
		sprite_->setOrigin(vec2f(0, 16));
	}

	sf::Sprite* GetSprite() { return sprite_; }

	void UpdatePosition(const vec2f& position)
	{
		sprite_->setPosition(position);
	}


	const vec2u& GetSize()
	{
		return vec2u(16, 16);
	}


	void Draw(Window* window, int32 zOrder)
	{
		if(!sprite_) { return; }
		window->Draw(*sprite_, zOrder);
	}

	Origin GetOrigin() const
	{
		return origin_;
	}

private:
	Origin origin_;
	sf::Sprite* sprite_;
	string textureName_;
};
