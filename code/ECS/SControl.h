#pragma once
#include "SBase.h"

/**
   The system managing all ComponentControllers.
 */
class SControl : public SBase
{
public:
	SControl(SystemManager* systemManager);
	~SControl();

	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);
private:
	/**
	 */
	void MoveEntity(const EntityID& entityID, const Direction& direction);
};

