#pragma once
#include "SBase.h"

struct TileInfo;
class GameWorld;

struct CollisionElement
{
	CollisionElement(real32 area, TileInfo* tileInfo, const sf::FloatRect& tileBounds)
	{
		/// @todo let all structs take this form.
		// TODO: let all structs take this form.
		this->area = area;
		this->tileInfo = tileInfo;
		this->tileBounds = tileBounds;
	}

	real32 area;
	TileInfo* tileInfo;
	sf::FloatRect tileBounds;
};

using Collisions = std::vector<CollisionElement>;

/**
   A System that handles entity<->entity/entity<->map collision.
 */
class SCollision : public SBase
{
public:
	SCollision(SystemManager* systemManager);
	~SCollision();

	void SetMap(GameWorld* map);


	void Update(real32 dt);
	void HandleEvent(const EntityID& entityID, const EntityEventType& eventType);
	void OnNotify(const Message& msg);
	// ---> DEBUG
	void DEBUGDrawCollidables(sf::RenderTarget* renderTarget);
	std::vector<sf::FloatRect> EntityCollidesWithAnyOtherEntity(const EntityID& tempEntityID);
	std::vector<sf::FloatRect> EntityCollidesWithWalls(const EntityID& tempEntityID);
	// TODO: using TilePos = vec2i
	std::vector<vec2i> CollidesWithStructures(const EntityID& colliderID);
	std::vector<vec2i> CollidesWithEntities(const EntityID& colliderID);
	// ---< 
	std::vector<vec2i> GetTilesTakenByEntity(const EntityID& entityID) const;
private:
	void EntityCollisions();
	void MapCollisions(const EntityID& entityID, CPosition* cPos, CCollidable* cCol);
	void CheckOutOfBounds(CPosition* cPos, CCollidable* cCollidable);
	void CheckCollisions(const EntityID& entityID, CPosition* cPos, CCollidable* cCollidable, Collisions& collisions);
	void HandleColiisions(const EntityID& entityID, CPosition* cPos, CCollidable* cCollidable, Collisions& collisions);

	GameWorld* pGameWorld_;
};
