#pragma once
#include <vector>
#include "Observer.h"


/**
   Each Message type has exactly 1 Communicator.
 */
class Communicator
{
public:
	~Communicator()
	{
		observers_.clear();
	}
	bool AddObserver(Observer* observer)
	{
		if(HasObserver(observer))
		{
			return false;
		}
		observers_.emplace_back(observer);
		return true;
	}

	bool RemoveObserver(Observer* observer)
	{
		auto observersItr = std::find_if(observers_.begin(), observers_.end(),
										 [observer](Observer* element)
										 {
											 return element == observer;
										 });
		if(observersItr == observers_.end())
		{
			return false;
		}
		observers_.erase(observersItr);
		return true;
	}

	bool HasObserver(const Observer* observer)
	{
		return(std::find_if(observers_.begin(),
							observers_.end(),
							[&observer](Observer* element)
							{
								return element == observer;
							}) != observers_.end());
	}

	void Broadcast(const Message& msg)
	{
		for(auto observer : observers_)
		{
			observer->OnNotify(msg);
		}
	};
private:
	std::vector<Observer*> observers_;
};
