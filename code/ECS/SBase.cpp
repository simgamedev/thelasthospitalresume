#include "SBase.h"
#include "SystemManager.h"


SBase::SBase(const SystemType& type, SystemManager* systemManager)
	: type_(type)
	, systemManager_(systemManager)
{
}


SBase::~SBase()
{
	/// @todo didn't you just called Purge() in SystemManager::PurgeEntities??
	Purge();
}

bool
SBase::HasEntity(const EntityID& entityID)
{
	return std::find(entities_.begin(), entities_.end(), entityID) != entities_.end();
}


/*********************************************************************************
 * Add Entity.
 *********************************************************************************/
bool
SBase::AddEntity(const EntityID& entityID)
{
	if (HasEntity(entityID)) { return false; }
	entities_.emplace_back(entityID);
	return true;
}


bool
SBase::RemoveEntity(const EntityID& entityID)
{
	auto entitiesItr = std::find_if(entities_.begin(), entities_.end(),
									[entityID](EntityID id)
									{
										return id == entityID;
									});
	if(entitiesItr == entities_.end()) { return false; }
	entities_.erase(entitiesItr);
	return true;
}

SystemType
SBase::GetType()
{
	return type_;
}

/********************************************************************************
 * Check if entity belongs to this system using it's bitmask.
 ********************************************************************************/
bool
SBase::FitsRequirements(const Bitmask& bitmask)
{
	return std::find_if(requirements_.begin(), requirements_.end(),
						[&bitmask](Bitmask& requirement)
						{
							return requirement.Matches(bitmask);
						}) != requirements_.end();
}


void
SBase::Purge()
{
	entities_.clear();
}

/*********************************************************************************
 * Get Num Entities.
 *********************************************************************************/
int32
SBase::GetNumEntities() const
{
	return entities_.size();
}
