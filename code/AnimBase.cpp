#include "AnimBase.h"
#include "GameSprite.h"

/********************************************************************************
 * Simple Constructor.
 ********************************************************************************/
AnimBase::AnimBase()
	: frameCurrent_(0)
	, frameStart_(0)
	, frameEnd_(0)
	, frameTime_(0)
	, elapsedTime_(0)
	, looping_(true)
	, playing_(false)
{
}


/********************************************************************************
 * Empty Destructor.
 ********************************************************************************/
AnimBase::~AnimBase(){}



/********************************************************************************
 * Update.
 ********************************************************************************/
void
AnimBase::Update(real32 dt)
{
	if(playing_)
	{
		elapsedTime_ += dt;
		if(elapsedTime_ >= frameTime_)
		{
			FrameStep();
			CropSprite();
			elapsedTime_ = 0.0f;
		}
	}
}

 /** @name Getters&Setters
 */
///@{
void
AnimBase::SetSprite(GameSprite* sprite)
{
	sprite_ = sprite;
}

bool
AnimBase::SetCurrentFrame(uint32 frame)
{
	if(frame >= frameStart_ && frame <= frameEnd_)
	{
		frameCurrent_ = frame;
		frameJumped_ = true;
		return true;
	}
	return false;
}

void
AnimBase::SetStartFrame(uint32 frame)
{
	frameStart_ = frame;
}

void
AnimBase::SetEndFrame(uint32 frame)
{
	frameEnd_ = frame;
}

void
AnimBase::SetFrameTime(real32 time)
{
	frameTime_ = time;
}

void
AnimBase::SetLooping(bool looping)
{
	looping_ = looping;
}

void
AnimBase::SetName(const string& name)
{
	name_ = name;
}

GameSprite* AnimBase::GetSprite(){ return sprite_; }
uint32 AnimBase::GetCurrentFrame(){ return frameCurrent_; }
uint32 AnimBase::GetStartFrame(){ return frameStart_; }
uint32 AnimBase::GetEndFrame(){ return frameEnd_; }
real32 AnimBase::GetFrameTime(){ return frameTime_; }
real32 AnimBase::GetElapsedTime(){ return elapsedTime_; }
string AnimBase::GetName(){ return name_; }
bool AnimBase::IsLooping(){ return looping_; }
bool AnimBase::IsPlaying(){ return playing_; }
///@}********************************************************************************/

void AnimBase::Play() { playing_ = true; }
void AnimBase::Pause() { playing_ = false; }
void AnimBase::Stop() { playing_ = false; Reset(); }
void
AnimBase::Reset()
{
	frameCurrent_ = frameStart_;
	elapsedTime_ = 0.0f;
	CropSprite();
}
