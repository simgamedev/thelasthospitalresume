#pragma once
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>
#include <cstdint>
#include <string>
#include <assert.h>
#include "Log/Logger.h"
#define LOG_ENGINE_ERROR(...) ::Logger::Instance()->EngineLogger()->error(__VA_ARGS__)
#define LOG_ENGINE_INFO(...) ::Logger::Instance()->EngineLogger()->info(__VA_ARGS__)
#define LOG_ENGINE_WARN(...) ::Logger::Instance()->EngineLogger()->warn(__VA_ARGS__)
#define LOG_ENGINE_CRITICAL(...) ::Logger::Instance()->EngineLogger()->critical(__VA_ARGS__)
#define LOG_GAME_INFO(...) ::Logger::Instance()->GamePlayLogger()->info(__VA_ARGS__)
#define LOG_GAME_ERROR(...) ::Logger::Instance()->GamePlayLogger()->error(__VA_ARGS__)
#define LOG_GAME_WARN(...) ::Logger::Instance()->GamePlayLogger()->warn(__VA_ARGS__)
#define LOG_GAME_CRITICAL(...) ::Logger::Instance()->GamePlayLogger()->critical(__VA_ARGS__)
#define STR(...) ::std::to_string(__VA_ARGS__)
using vec2u = sf::Vector2u;
using vec2i = sf::Vector2i;
using vec3i = sf::Vector3i;
using vec3f = sf::Vector3f;
using vec2f = sf::Vector2f;
using string = std::string;

using int32 = int32_t;
using uint32 = uint32_t;
using int8 = int8_t;
using uint8 = uint8_t;
using int64 = int64_t;
using uint64 = uint64_t;
using real32 = float;
using real64 = double;



