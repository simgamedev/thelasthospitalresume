#pragma once
#include "../common.h"

class GameWorld;

class HospitalFinance
{
public:
	HospitalFinance(GameWorld* gameWorld);
	~HospitalFinance();
	void PayMonthlyBill();
	void PayForEntity(const string& entityName);
	void PayCash(real32 amount);
private:
	GameWorld* pGameWorld_;
	real32 cash_;
};