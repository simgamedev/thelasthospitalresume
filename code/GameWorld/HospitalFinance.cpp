#include "HospitalFinance.h"
#include "GameWorld.h"


HospitalFinance::HospitalFinance(GameWorld* gameWorld)
	: pGameWorld_(gameWorld)
{
	cash_ = 1000000.0f; // 1 million dollars
}

HospitalFinance::~HospitalFinance()
{
}


/******************************* PayMonthlyBill *********************************
 * 
 ********************************************************************************/
void
HospitalFinance::PayMonthlyBill()
{
	// ---> pay interest if has loan
	// ---<

	// ---> pay salaries
	// ---<

	// ---> pay for electricity
	// ---<

	// ---> pay for water
	// ---<

	// ---> pay for sewage
	// ---<

	// ---> pay for garbage collection
	// ---<
}


/******************************* PayForEntity ***********************************
 * 
 ********************************************************************************/
void
HospitalFinance::PayForEntity(const string& entityName)
{
}

void
HospitalFinance::PayCash(real32 amount)
{
	cash_ -= amount;
}