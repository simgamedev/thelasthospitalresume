#include "GameWorld.h"
#include "../StateManager.h"
#include "../ECS/CPosition.h"
#include "../Window.h"
#include "../StateLoading.h"
#include "../StateGame.h"

// ---> Graph
#include "../Graph/HandyGraphFunctions.h"
// ---<

// ---> Modify
#include "../AI/GOAP/ActionTakeSnack.h"
#include "../AI/GOAP/ActionUseToilet.h"
#include "../AI/GOAP/ActionTestSwab.h"
#include "../AI/GOAP/ActionSwabMe.h"
#include "../AI/GOAP/ActionTakeSwab.h"
#include "../AI/GOAP/ActionDrugMe.h"
#include "../AI/GOAP/ActionLieInMe.h"
#include "../AI/GOAP/ActionTakeDrug.h"
#include "../AI/GOAP/ActionSweepMe.h"
#include "../AI/GOAP/ActionTakeSupplyBoxSnack.h"
#include "../AI/GOAP/ActionRefillMe.h"
#include "../AI/GOAP/ActionGetRegistered.h"
#include "../AI/GOAP/ActionGotoRoom.h"
#include "../AI/GOAP/ActionXRayScan.h"
#include "../AI/GOAP/ActionGotoEntity.h"
#include "../AI/GOAP/ActionGiveMeDrug.h"
#include "../AI/GOAP/ActionTurnOnVent.h"
#include "../AI/GOAP/ActionTurnOnXRay.h"
#include "../AI/GOAP/ActionHookMeToVent.h"
// TODO: one or two messaging system??
#include "../Messaging/CharMsgDispatcher.h"
#include "../ECS/MessageHandler.h"
// ---< Modify

/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
GameWorld::GameWorld(Window* window)
	: window_(window)
	, navGraph_(nullptr)
	//, defaultTileInfo_(nullptr)
	, tileMap_({0, 0})
	, playerID_(-1)
	, inGameTime_(0.0f)
	, dayLength_(30.0f)
{
	displaySprite_.setPosition({0, 0});
	RegisterForMsgType(CharMsgType::NeedDrug);
	EntityMessageHandler::Instance()->Subscribe(EntityMessageType::NeedRefill, this);
	EntityMessageHandler::Instance()->Subscribe(EntityMessageType::ReadyToOperate, this);


	// ---> Default outside room.
	Room* outsideRoom = new Room(RoomType::Outside, this);
	rooms_.emplace(outsideRoom->GetID(), outsideRoom);
	// TODO: do we need to add all tiles to this outside room
	// or just everying tile.roomID = 0 is enough?
	// ---< Default outside room.

}


/********************************************************************************
 * Default Destructor.
 ********************************************************************************/
GameWorld::~GameWorld()
{
	PurgeMap();
	for (auto& itr : m_DEBUGNavGraphNodeDrawables)
	{
		delete itr.second;
	}
	for (sf::Text* nodeIdxText : m_DEBUGNavGraphNodeIdxTexts)
	{
		delete nodeIdxText;
	}
	for (sf::RectangleShape* grid : m_DEBUGGrid)
	{
		delete grid;
	}
}

void
GameWorld::SetStateManager(StateManager* stateManager)
{
	stateManager_ = stateManager;

	textureManager_ = stateManager_->GetContext()->textureManager;
	stateGame_ = stateManager_->GetState<StateGame>(GameStateType::Game);
	pEntityManager_ = stateManager_->GetContext()->entityManager;
}

/********************************************************************************
 * Get Tile.
 ********************************************************************************/
Tile*
GameWorld::GetTile(uint32 x, uint32 y, uint32 elevation)
{
	return tileMap_.GetTile(x, y, elevation);
}

Tile*
GameWorld::GetTile(vec2i tilePos)
{
	return tileMap_.GetTile(tilePos.x, tilePos.y, 0);
}

Tile*
GameWorld::GetTile(vec3i tilePos)
{
	return tileMap_.GetTile(tilePos.x, tilePos.y, tilePos.z);
}

TileInfo*
GameWorld::GetDefaultTileInfo()
{
	return &defaultTileInfo_;
}

TileSet*
GameWorld::GetTileSet(const string& tileSetName)
{
	return nullptr;
}

TileMap*
GameWorld::GetTileMap()
{
	return &tileMap_;
}

uint32
GameWorld::GetTileSize() const
{
	return TILE_SIZE;
}

/********************************************************************************
 * Map Size is (num x tiles, num y tiles, num layers).
 ********************************************************************************/
vec2u
GameWorld::GetMapSize() const
{
	return tileMap_.GetSize();
}

void
GameWorld::SaveToFile(const string& filepath)
{
	std::ofstream file(filepath, std::ios::out);
	if(!file.is_open())
	{
		std::cout << "Failed to open/create file: " << filepath << " to save game map." << std::endl;
	}
	vec2u mapSize = tileMap_.GetSize();
	// todo: properly save map
	file << "SIZE " << mapSize.x << " " << mapSize.y << std::endl;
	file << "TILE_SET " << "PHC_Exterior2" << std::endl;
	file << "TILE_SET " << "PHC_Exterior" << std::endl;
	file << "TILE_SET " << "PHC_Store" << std::endl;
	file << "TILE_SET " << "HC_Factory" << std::endl;
	file << "TILE_SET " << "HC_Car" << std::endl;
	file << "TILE_SET " << "ExteriorWall1" << std::endl;
	file << "TILE_SET " << "ExteriorWall2" << std::endl;
	file << "TILE_SET " << "RoadAndSidewalk" << std::endl;
	tileMap_.SaveToFile(file);
	file.close();
}

void
GameWorld::Redraw(vec3i from, vec3i to)
{
	//sf::RenderWindow* renderWindow = window_->GetRenderWindow();
	//renderWindow->resetGLStates();

	vec2u mapSize = tileMap_.GetSize();
	if(mapSize.x == 0 || mapSize.y == 0) { return; }
	if(from.x < 0 || from.y < 0 || from.z < 0) { return; }
	if(from.x >= mapSize.x || from.y >= mapSize.y) { return; }

	vec3i originalTo = to;
	if(to.x < 0 || to.x >= mapSize.x) { to.x = mapSize.x - 1; }
	if(to.y < 0 || to.y >= mapSize.y) { to.y = mapSize.y - 1; }
	if(to.z < 0 || to.z >= MAX_MAP_LAYERS) { to.z = MAX_MAP_LAYERS - 1; }

	// ---> Prepare RenderTexture
	vec2u mapSizeInPixels = vec2u(mapSize.x * TILE_SIZE, mapSize.y * TILE_SIZE);
	for(uint32 elevation = from.z;
		elevation <= to.z;
		++elevation)
	{
		if(renderTextures_[elevation].getSize() == mapSizeInPixels) { continue; }
		if(!renderTextures_[elevation].create(mapSizeInPixels.x, mapSizeInPixels.y))
		{
			std::cout << "Failed to create renderTexture for redrawing game map" << std::endl;
		}
	}

	// ---> Draw each tile
	ClearRenderTexture(from, originalTo);
	for(int32 x = from.x; x <= to.x; ++x)
	{
		for(int32 y = from.y; y <= to.y; ++y)
		{
			for(int32 elevation = from.z; elevation <= to.z; ++ elevation)
			{
				if (elevation == 1)
				{
					int32 a = 100;
				}
				Tile* tile = tileMap_.GetTile(x, y, elevation);
				if(!tile) { continue; }
				auto& sprite = tile->tileInfo->GetSprite();
				sprite.setPosition(static_cast<real32>(x * TILE_SIZE),
								   static_cast<real32>(y * TILE_SIZE));
				renderTextures_[elevation].draw(sprite);
			}
		}
	}

	// ---> Display
	for(int32 elevation = from.z; elevation <= to.z; ++elevation)
	{
		renderTextures_[elevation].display();
	}
	// TODO: you are not using it
	bDoneRedraw_ = true;

	LOG_ENGINE_INFO("[GameWorld] GameMap is being redrawn!");
}

/********************************************************************************
 * Clears portion of rednertexture, it's for the MapEditor.
 * @param to == (-1, -1, *) means clear entire texutre, not portion of it.
 ********************************************************************************/
void
GameWorld::ClearRenderTexture(vec3i from, vec3i to)
{
	vec2u mapSize = tileMap_.GetSize();
	// ---> Parameter Error Checking
	if(from.x < 0 || from.y < 0 || from.z < 0) { return; }
	if(from.x >= mapSize.x || from.y >= mapSize.y) { return; }
	/// @todo what about from.z >= NUM_LAYERS
	uint32 toLayer;
	if(to.z < 0 || to.z >= MAX_MAP_LAYERS)
	{
		toLayer = MAX_MAP_LAYERS - 1;
	}
	else
	{
		toLayer = to.z;
	}

	// ---> Case: Clears Entire Texture
	if(to.x == -1 && to.y == -1)
	{
		for(int32 elevation = from.z; elevation <= toLayer; ++elevation)
		{
			renderTextures_[elevation].clear({0, 0, 0, 0});
		}
		return;
	}
	// ---> Case: Clears Portion of Texture
	vec2i position = vec2i(from.x, from.y) * TILE_SIZE;
	if(to.x < 0)
	{
		to.x = mapSize.x - 1;
	} 
	if(to.y < 0)
	{
		to.y = mapSize.y - 1;
	}
  	vec2i size = vec2i((to.x - from.x) + 1,
					   (to.y - from.y) + 1) * TILE_SIZE;
	sf::RectangleShape rectangleShape;
	rectangleShape.setPosition(vec2f(position));
	rectangleShape.setSize(vec2f(size));
	rectangleShape.setFillColor(sf::Color(0, 0, 0, -255));
	for(int32 elevation = from.z;
		elevation <= toLayer;
		++elevation)
	{
		renderTextures_[elevation].draw(rectangleShape, sf::BlendMultiply);
		renderTextures_[elevation].display();
	}
}


/********************************************************************************
 * Process line method from super class FileLoaer.
 ********************************************************************************/
bool
GameWorld::ProcessLine(std::stringstream& stream)
{
	string key;
	if(!(stream >> key)) { return false; }
	if(key == "TILE")
	{
		tileMap_.ReadInTile(stream);
	}
	else if(key == "TILE_SET")
	{
	 	string tileSetName;
		stream >> tileSetName;
		TileSet* tileSet = new TileSet(tileSetName, textureManager_);
		if(!tileSets_.emplace(tileSetName, tileSet).second)
		{
			delete tileSet;
		}
		/// @todo where should we add tileset to TileMap?
		tileMap_.AddTileSet(tileSet);
		tileSet->ResetWorker();
		tileSet->AddFile(Utils::GetMediaDirectory() + "TileSets/"+ tileSetName + ".tileset");
		auto loadingState = stateManager_->GetState<StateLoading>(GameStateType::Loading);
		loadingState->AddFileLoader(tileSet);
		//loadingState->SetFinishedCallback(&GameWorld::UpdateNavGraph, this);
		while(!tileSet->IsDone())
		{
			//std::cout << "Waiting for tileSet to load..." << std::endl;
		}
	}
	else if(key == "DEFAULT_FRICTION")
	{
		real32 frictionX;
		real32 frictionY;
		vec2f friction(frictionX, frictionY);
		defaultTileInfo_.SetFriction(friction);
		//stream >> defaultTileInfo_.friction.x >> defaultTileInfo_.friction.y;
	}
	else if(key == "ENTITY")
	{
	}
	else if(key == "NUM_LAYERS")
	{
		/// @todo numLayers_?? or MAX_MAP_LAYERS
		//stream >> numLayers_;
	}
	else if(key == "SIZE")
	{
		vec2u mapSize;
		stream >> mapSize.x >> mapSize.y;
		tileMap_.SetMapSize(mapSize);
	}
	else if(key == "TILE_SIZE")
	{
		/// @todo tileSize_ or TILE_SIZE??
		//stream >> tileSize_;
	}
	else
	{
		std::cout << "Unknow map key word so passing to loadees!" << std::endl;
	}
	return true;
}


/********************************************************************************
 * In Update what do we do?
 ********************************************************************************/
void
GameWorld::Update(real32 dt)
{
}


/********************************************************************************
 * Draw the map to window.
 ********************************************************************************/
void
GameWorld::Draw(uint32 elevation)
{
	if (elevation >= MAX_MAP_LAYERS) { return; }
	displaySprite_.setTexture(renderTextures_[elevation].getTexture());
	/// @todo if you don't set texture rect, by default doesn't it just use the whole?
	displaySprite_.setTextureRect(sf::IntRect(vec2i(0, 0), vec2i(renderTextures_[elevation].getSize())));

	// NOTE: gamemap is the only thing not drawn by the unified renderer
	window_->GetRenderWindow()->draw(displaySprite_);
}

/********************************************************************************
 * Purge TileSets, TileMap etc.
 ********************************************************************************/
void
GameWorld::PurgeMap()
{
	tileMap_.Purge();
	for (auto& tileSetsItr : tileSets_)
	{
		TileSet* tileSet = tileSetsItr.second;
		delete tileSet;
	}
	tileSets_.clear();
	Redraw();
}


/*********************************************************************************
 * Add TileSet.
 *********************************************************************************/
bool
GameWorld::AddTileSet(string& tileSetName)
{
	/*
		TileSet* tileSet = new TileSet(tileSetName, textureManager_);
		if(!tileSets_.emplace(tileSetName, tileSet).second)
		{
			delete tileSet;
			return false;
		}
		/// @todo where should we add tileset to TileMap?
		tileSet->ResetWorker();
		tileSet->AddFile(Utils::GetMediaDirectory() + "TileSets/"+ tileSetName + ".tileset");
		auto loadingState = stateManager_->GetState<StateLoading>(GameStateType::Loading);
		loadingState->AddFileLoader(tileSet);
		while(!tileSet->IsDone())
		{
			std::cout << "Waiting for tileSet to load..." << std::endl;
		}
		return true;
		*/
	return true;
}


bool
GameWorld::AddTileSet(TileSet* tileSet)
{
	if (!tileSets_.emplace(tileSet->GetName(), tileSet).second)
	{
		std::cout << "Failed to add tileSet to gamemap" << tileSet->GetName() << std::endl;
		return false;
	}
	return true;
}

/*********************************************************************************
 * Update NavGraph.
 *********************************************************************************/
bool
GameWorld::ValidNeighbour(int32 x, int32 y)
{
	return !((x < 0) || (x >= tileMap_.GetSize().x) ||
			 (y < 0) || (y >= tileMap_.GetSize().y));
}

bool
GameWorld::ValidNeighbourNew(int32 x, int32 y)
{
	int32 nodeIdx = m_DEBUGNavGraphNodeIndices.at(vec2i(x, y));
    return (navGraph_->GetNode(nodeIdx).Index() != invalid_node_index);
}
void
GameWorld::DEBUGInitNavGraphDrawable()
{
	stateGame_->RedrawGameMap();
	NavGraph::ConstNodeIterator nodeItr(*navGraph_);
	for (const NavGraph::NodeType* pN = nodeItr.begin();
		 !nodeItr.end();
		 pN = nodeItr.next())
	{
		//---> node
		sf::CircleShape* nodeDrawable = new sf::CircleShape(1);
		nodeDrawable->setOrigin(vec2f(1, 1));
		if (pN->Index() != invalid_node_index)
		{
			nodeDrawable->setFillColor(sf::Color::Red);
		}
		else
		{
			nodeDrawable->setFillColor(sf::Color::Green);
		}
		nodeDrawable->setPosition(pN->Pos());
		//m_DEBUGNavGraphNodeDrawables.push_back(nodeDrawable);
		m_DEBUGNavGraphNodeDrawables.emplace(pN->Index(), nodeDrawable);

		//---> edges
		NavGraph::ConstEdgeIterator edgeItr(*navGraph_, pN->Index());
		for (const NavGraph::EdgeType* pE = edgeItr.begin();
			 !edgeItr.end();
			 pE = edgeItr.next())
		{
			vec2f from(pN->Pos().x, pN->Pos().y);
			vec2f to(navGraph_->GetNode(pE->To()).Pos().x,
					 navGraph_->GetNode(pE->To()).Pos().y);
			sf::Vertex f(from);
			f.color = sf::Color(0, 0, 255, 100);
			sf::Vertex t(to);
			t.color = sf::Color(0, 0, 255, 100);
			//m_DEBUGNavGraphEdgeDrawables.push_back(f);
			//m_DEBUGNavGraphEdgeDrawables.push_back(t);
			std::vector<sf::Vertex> line;
			line.push_back(f);
			line.push_back(t);
			m_DEBUGNavGraphEdgeDrawables.emplace(vec2i(pE->From(), pE->To()), line);
		}
		//---<
	}
}

/******************************* AddBackToNavGraph ********************
 * Add node back to navgraph
 **********************************************************************/
void 
GameWorld::AddBackToNavGraph(vec2i tilepos)
{
	int32 navGraphNodeIdx = m_DEBUGNavGraphNodeIndices.at(tilepos);
	real32 midX = TILE_SIZE / 2;
	real32 midY = TILE_SIZE / 2;
	vec2f nodePos = vec2f(midX + (tilepos.x * TILE_SIZE),
						  midY + (tilepos.y * TILE_SIZE));
	// ---> add back debug node drawable
	sf::CircleShape* nodeDrawable = new sf::CircleShape(1);
	nodeDrawable->setOrigin(vec2f(1, 1));
	nodeDrawable->setPosition(nodePos);
	nodeDrawable->setFillColor(sf::Color::Red);
	m_DEBUGNavGraphNodeDrawables.emplace(navGraphNodeIdx, nodeDrawable);
	// ---<

	navGraph_->AddNode(NavGraphNode<>(navGraphNodeIdx,
									  nodePos));
	for (int i = -1; i < 2; ++i)
	{
		for (int j = -1; j < 2; ++j)
		{
			int nodeX = tilepos.x + j;
			int nodeY = tilepos.y + i;
			// skip if equal to this node
			if ((i == 0) && (j == 0)) continue;
			// skip if diagonal
			if ((i == -1) && (j == -1)) continue;
			if ((i == -1) && (j == 1)) continue;
			if ((i == 1) && (j == -1)) continue;
			if ((i == 1) && (j == 1)) continue;
			// check to see if this a a valid node
			if (ValidNeighbourNew(nodeX, nodeY))
			{
				vec2f toNodePos = vec2f(midX + (nodeX * TILE_SIZE),
										midY + (nodeY * TILE_SIZE));
				NavGraph::EdgeType newEdge(tilepos.y * tileMap_.GetSize().x + tilepos.x,
										   nodeY * tileMap_.GetSize().x + nodeX,
										   1.0);
				navGraph_->AddEdge(newEdge);
				// ---> Add back NavGraphEdgeDrawable
				sf::Vertex f(nodePos);
				f.color = sf::Color(0, 0, 255, 100);
				sf::Vertex t(toNodePos);
				t.color = sf::Color(0, 0, 255, 100);
				//m_DEBUGNavGraphEdgeDrawables.push_back(f);
				//m_DEBUGNavGraphEdgeDrawables.push_back(t);
				std::vector<sf::Vertex> line;
				line.push_back(f);
				line.push_back(t);
				m_DEBUGNavGraphEdgeDrawables.emplace(vec2i(newEdge.From(), newEdge.To()), line);
				// ---< 

				// if graph is not a digraph then an edge needs to be added going
				// in the other direction
				if (!navGraph_->isDigraph())
				{
					NavGraph::EdgeType newEdge(nodeY * tileMap_.GetSize().x + nodeX,
											   tilepos.y * tileMap_.GetSize().x + tilepos.x,
											   1.0);
					navGraph_->AddEdge(newEdge);
					// ---> Add back NavGraphEdgeDrawable
					sf::Vertex f(nodePos);
					f.color = sf::Color(0, 0, 255, 100);
					sf::Vertex t(toNodePos);
					t.color = sf::Color(0, 0, 255, 100);
					//m_DEBUGNavGraphEdgeDrawables.push_back(f);
					//m_DEBUGNavGraphEdgeDrawables.push_back(t);
					std::vector<sf::Vertex> line;
					line.push_back(f);
					line.push_back(t);
					m_DEBUGNavGraphEdgeDrawables.emplace(vec2i(newEdge.To(), newEdge.From()), line);
					// ---< 
				}
			}

		}
	}
}
/******************************* DEBUGUpdateNavGraphDrawable ********************
 *
 ********************************************************************************/
void
GameWorld::DEBUGRemoveFromNavGraphDrawable(vec2i tilepos)
{
	int32 navGraphNodeIdx = m_DEBUGNavGraphNodeIndices.at(tilepos);
	//---> edges
	//m_DEBUGNavGraphEdgeDrawables.erase(navGraphNodeIdx);
	// ---> nodes
	m_DEBUGNavGraphNodeDrawables.erase(navGraphNodeIdx);
	//---<
}

/******************************* UpdateNavGraph *********************************
 *
 ********************************************************************************/
void
GameWorld::RemoveFromNavGraph(vec2i tilepos)
{
	int32 navGraphNodeIdx = m_DEBUGNavGraphNodeIndices.at(tilepos);
	// ---< First Time Running
	if (tilepos.x != -1)
	{
		//---> edges
		NavGraph::ConstEdgeIterator edgeItr(*navGraph_, navGraphNodeIdx);
		for (const NavGraph::EdgeType* pE = edgeItr.begin();
			 !edgeItr.end();
			 pE = edgeItr.next())
		{
			m_DEBUGNavGraphEdgeDrawables.erase(vec2i(pE->From(), pE->To()));
			m_DEBUGNavGraphEdgeDrawables.erase(vec2i(pE->To(), pE->From()));
		}
		//---<

		navGraph_->RemoveNode(tilepos.y * tileMap_.GetSize().x + tilepos.x);
	}
	DEBUGRemoveFromNavGraphDrawable(tilepos);
}

/******************************* InitNavGraph ***********************************
 *
 ********************************************************************************/
void
GameWorld::InitNavGraph()
{
	navGraph_ = new NavGraph(false);

	real32 midX = TILE_SIZE / 2.0f;
	real32 midY = TILE_SIZE / 2.0f;

	//---> first create all the nodes
	for (int32 row = 0; row < tileMap_.GetSize().y; ++row)
	{
		for (int32 col = 0; col < tileMap_.GetSize().x; ++col)
		{
			vec2f nodePos = vec2f(midX + (col * TILE_SIZE),
								  midY + (row * TILE_SIZE));
			int32 nextFreeNodeIdx = navGraph_->GetNextFreeNodeIndex();
			navGraph_->AddNode(NavGraphNode<>(nextFreeNodeIdx,
											  nodePos));
			m_DEBUGNavGraphNodeIndices.emplace(vec2i(col, row), nextFreeNodeIdx);
		}
	}
	//---<


	//---> then add all the edges
	for (int row = 0; row < tileMap_.GetSize().y; ++row)
	{
		for (int col = 0; col < tileMap_.GetSize().x; ++col)
		{
			for (int i = -1; i < 2; ++i)
			{
				for (int j = -1; j < 2; ++j)
				{
					int nodeX = col + j;
					int nodeY = row + i;
					// skip if equal to this node
					if ((i == 0) && (j == 0)) continue;
					// skip if diagonal
					if ((i == -1) && (j == -1)) continue;
					if ((i == -1) && (j == 1)) continue;
					if ((i == 1) && (j == -1)) continue;
					if ((i == 1) && (j == 1)) continue;
					// check to see if this a a valid node
					if (ValidNeighbour(nodeX, nodeY))
					{
						NavGraph::EdgeType newEdge(row * tileMap_.GetSize().x + col,
												   nodeY * tileMap_.GetSize().x + nodeX,
												   1.0);
						navGraph_->AddEdge(newEdge);
						// if graph is not a digraph then an edge needs to be added going
						// in the other direction
						if (!navGraph_->isDigraph())
						{
							NavGraph::EdgeType newEdge(nodeY * tileMap_.GetSize().x + nodeX,
													   row * tileMap_.GetSize().x + col,
													   1.0);
						}
					}

				}
			}
		}
	}
	//---> then remove node on solid tile/wall tile
	for (int32 row = 0; row < tileMap_.GetSize().y; ++row)
	{
		for (int32 col = 0; col < tileMap_.GetSize().x; ++col)
		{
			if (!stateGame_->IsTileWalkable(vec2i(col, row)))
			{
				navGraph_->RemoveNode(row * tileMap_.GetSize().x + col);
			}
		}
	}
	//---<
	int32 a = 100;

	//DEBUGInitGrid();
	DEBUGInitNavGraphDrawable();
	// TODO: should be here
	stateGame_->InitTestEntities();
	// ---< 
}

/******************************* DEBUGDrawNavGraph ******************************
 *
 ********************************************************************************/
void 
GameWorld::DEBUGDrawNavGraph(sf::RenderWindow* renderWindow)
{
	// draw nodes
	for (auto& itr : m_DEBUGNavGraphNodeDrawables)
	{
		renderWindow->draw(*itr.second);
	}

	// draw edges
	for (auto& itr : m_DEBUGNavGraphEdgeDrawables)
	{
		std::vector<sf::Vertex> vertices = itr.second;
		renderWindow->draw(&vertices[0],
						   vertices.size(),
						   sf::Lines);
	}
}

/******************************* DEBUGInitGrid **********************************
 *
 ********************************************************************************/
void
GameWorld::DEBUGInitGrid()
{
	/*
	for (int32 row = 0; row < tileMap_.GetSize().y; ++row)
	{
		for (int32 col = 0; col < tileMap_.GetSize().x; ++col)
		{
			sf::RectangleShape* grid = new sf::RectangleShape(vec2f(TILE_SIZE, TILE_SIZE));
			grid->setOutlineThickness(0.5f);
			grid->setFillColor(sf::Color(0, 0, 0, 0));
			grid->setOutlineColor(sf::Color(0, 100, 0, 100));
			grid->setOrigin(vec2f(0, 0));
			grid->setPosition(vec2f(col * TILE_SIZE, row * TILE_SIZE));
			m_DEBUGGrid.push_back(grid);
		}
	}
	*/
}

/******************************* DEBUGDrawGrid **********************************
 *
 ********************************************************************************/
void
GameWorld::DEBUGDrawGrid()
{
	sf::RenderWindow* renderWindow = window_->GetRenderWindow();
	/*
	sf::RenderWindow* renderWindow = window_->GetRenderWindow();
	for (sf::RectangleShape* grid : m_DEBUGGrid)
	{
		renderWindow->draw(*grid);
	}
	*/
	// ---> Horizontal Lines
	for (int32 y = 0; y < GetMapSize().y; y++)
	{
		sf::Vertex line[] = {
			sf::Vertex(vec2f(0, y * TILE_SIZE), sf::Color::Green),
			sf::Vertex(vec2f(GetMapSize().x * TILE_SIZE, y * TILE_SIZE), sf::Color::Green)
		};
		renderWindow->draw(line, 2, sf::Lines);
	}
	// ---< Horizontal Lines

	// ---> Vertical Lines
	for (int32 x = 0; x < GetMapSize().x; x++)
	{
		sf::Vertex line[] = {
			sf::Vertex(vec2f(x * TILE_SIZE, 0), sf::Color(0, 255, 0, 180)),
			sf::Vertex(vec2f(x * TILE_SIZE, GetMapSize().y * TILE_SIZE), sf::Color(0, 255, 0, 180)),
		};
		renderWindow->draw(line, 2, sf::Lines);
	}
	// ---< Vertical Lines
}

/******************************* GetAvailableActions ****************************
 *
 ********************************************************************************/
// ---> Modify
std::vector<GOAP::Action>
GameWorld::GetAvailableActions(TestEntity* testEntity)
{
	// TODO: give back action types depending on the agent type
	// ---> Only return actions that are not taken
	std::vector<GOAP::Action> result;
	for(auto itr = actions_.begin();
		itr != actions_.end();
		itr++)
	{
		if(!itr->isTaken_)
		{
			result.push_back(*itr);
		}
	}
	// ---<

	//return actions_;
	return result;
}

void
GameWorld::EntityAdded(const int32 entityID)
{
	// ---> Remove Tiles taken by added Entity from NavGraph
	// ---> RemoveFromNavGraph
	vec2i entityTilePos = stateGame_->GetEntityTilePos(entityID);
	std::vector<vec2i> tilesTaken = stateGame_->GetTilesTakenByEntity(entityID);
	vec2i userTile = pEntityManager_->GetEntityUserTile(entityID);
	vec2i receiverTile = pEntityManager_->GetEntityReceiverTile(entityID);
	for (auto itr : tilesTaken)
	{
		if(itr != userTile &&
		   itr != receiverTile)
			RemoveFromNavGraph(itr);
		// mark tiles taken by entityID
		Tile* tile = GetTile(itr);
		tile->entityID_ = entityID;
	}
	// ---< RemoveFromNavGraph
	// ---< Remove Tiles taken by added Entity from NavGraph
	string entityName = stateGame_->GetEntityName(entityID);
	vec2f entityPos = stateGame_->GetEntityPos(entityID);
	// ---> Fridge
	if (entityName.find("Fridge") != std::string::npos)
	{
		GOAP::ActionTakeSnack takeSnack(nullptr, entityPos);
		takeSnack.SetProviderEntityID(entityID);
		actions_.push_back(takeSnack);
	}
	// ---< Fridge

	// ---> Toilet
	if (entityName.find("Toilet") != std::string::npos)
	{
		GOAP::ActionUseToilet useToilet(nullptr, entityPos);
		useToilet.SetProviderEntityID(entityID);
		actions_.push_back(useToilet);
	}
	// ---< Toilet

	// ---> PCRTestMachine
	if (entityName.find("PCRTestMachine") != std::string::npos)
	{
		GOAP::ActionTestSwab testSwab(nullptr, entityPos - vec2f(8.0f, 8.0f));
		testSwab.SetProviderEntityID(entityID);
		actions_.push_back(testSwab);
	}
	// ---< PCRTestMachine

	// ---> SwabBox
	if (entityName.find("SwabBox") != std::string::npos)
	{
		GOAP::ActionTakeSwab takeSwab(nullptr, entityPos);
		takeSwab.SetProviderEntityID(entityID);
		actions_.push_back(takeSwab);
	}
	// ---< SwabBox

	// ---> Med Bed
	if (entityName.find("Bed") != std::string::npos)
	{
		GOAP::ActionLieInMe lieInMe(nullptr, entityPos);
		lieInMe.SetProviderEntityID(entityID);
		actions_.push_back(lieInMe);
	}
	// ---< Med Bed

	// ---> Drug Cabinet
	if (entityName.find("DrugCabinet") != std::string::npos)
	{
		GOAP::ActionTakeDrug takeDrug(nullptr, entityPos);
		takeDrug.SetProviderEntityID(entityID);
		actions_.push_back(takeDrug);
	}
	// ---< Drug Cabinet

	// ---> SupplyBox
	if (entityName.find("SupplyBox") != std::string::npos)
	{
		GOAP::ActionTakeSupplyBoxSnack takeSupplyBox(nullptr, entityPos);
		takeSupplyBox.SetProviderEntityID(entityID);
		actions_.push_back(takeSupplyBox);
	}
	// ---< SupplyBox

	// ---> ReceptionDesk
	if (entityName.find("ReceptionDesk") != std::string::npos)
	{
		GOAP::ActionGetRegistered getRegistered(nullptr, entityPos);
		getRegistered.SetProviderEntityID(entityID);
		actions_.push_back(getRegistered);
	}
	// ---< ReceptionDesk

	// ---> XRayMachine
	if (entityName.find("XRayMachine") != std::string::npos)
	{
		vec2f targetPos = vec2f(entityPos.x + 55, entityPos.y);
		GOAP::ActionGotoEntity actGotoEntity(nullptr, entityID, entityName, targetPos);
		actGotoEntity.SetProviderEntityID(entityID);
		actGotoEntity.providerEntityName_ = entityName;
		actions_.push_back(actGotoEntity);

		GOAP::ActionTurnOnXRay actTurnOnXRay(nullptr, entityPos);
		actTurnOnXRay.SetProviderEntityID(entityID);
		actTurnOnXRay.providerEntityName_ = entityName;
		actions_.push_back(actTurnOnXRay);
	}
	// ---< XRayMachine

	// ---< Ventilator
	if (entityName.find("Ventilator") != std::string::npos)
	{
		GOAP::ActionTurnOnVent actTurnOnVent(nullptr, entityPos);
		actTurnOnVent.SetProviderEntityID(entityID);
		actTurnOnVent.providerEntityName_ = entityName;
		actions_.push_back(actTurnOnVent);
	}
	// ---< Ventilator


	// ------> For All Entities
	// ---> Check If Entity Lands In Any Room
	vec2i entityTilepos = vec2i(entityPos.x / TILE_SIZE,
								entityPos.y / TILE_SIZE);
	for (auto& itr : rooms_)
	{
		Room* room = itr.second;
		for (Tile* tile : room->GetTiles())
		{
			if (entityTilepos == vec2i(tile->pos.x, tile->pos.y))
			{
				room->AddEntityToRoom(entityID);
				break;
			}
		}
	}
	// ---< Check If Entity Lands In Any Room
	// ------< For All Entities
}

void
GameWorld::TestEntityBorn(TestEntity* testEntity)
{
	if (testEntity->GetProfession() == Profession::Patient)
	{
		/*
		// TODO: should have a target and an owner
		GOAP::ActionSwabMe swabMe(nullptr, testEntity->GetPosition());
		swabMe.SetProviderTestEntity(testEntity);
		CharMsgDispatcher::Instance()->BroadcastMessage(SENDER_ID_IRRELEVANT,
														CharMsgType::NewPatient,
														testEntity);
		actions_.push_back(swabMe);
		*/
	}
}

void
GameWorld::EntityRemoved(const int32 entityID)
{
	// ---> Remve the action the to be removed entity provides
	/*
	for(auto itr = actions_.begin();
		itr != actions_.end();
		itr++)
	{
		if(itr->providerEntityID_ == entityID)
		{
			actions_.erase(itr);
		}
	}
	*/

	// ---> Remove Tiles taken by added Entity from NavGraph
	// ---> RemoveFromNavGraph
	vec2i entityTilePos = stateGame_->GetEntityTilePos(entityID);
	std::vector<vec2i> tilesTaken = stateGame_->GetTilesTakenByEntity(entityID);
	vec2i userTile = pEntityManager_->GetEntityUserTile(entityID);
	vec2i receiverTile = pEntityManager_->GetEntityReceiverTile(entityID);
	for (auto itr : tilesTaken)
	{
		if (itr != userTile &&
			itr != receiverTile)
			AddBackToNavGraph(itr);
		// mark tiles taken by entityID
		Tile* tile = GetTile(itr);
		tile->entityID_ = -1;
	}
	// ---< RemoveFromNavGraph
	// ---< Remove Tiles taken by added Entity from NavGraph
}


void
GameWorld::NewPositivePatient(TestEntity* testEntity)
{
}

void 
GameWorld::OnBroadcast(Telegram telegram)
{
	switch (telegram.type)
	{
		case CharMsgType::Hospitalized:
		{
		} break;

		case CharMsgType::NeedDrug:
		{
			TestEntity* sender = stateGame_->GetCharacterManager()->GetCharacterFromID(telegram.sender);
			GOAP::ActionDrugMe actionDrugMe(nullptr, sender->GetPosition());
			actionDrugMe.SetProviderTestEntity(sender);
			actions_.push_back(actionDrugMe);
		} break;


		default:
		{
		} break;
	}
}

void
GameWorld::OnMessage(Telegram telegram)
{
}

void
GameWorld::RegisterForMsgType(CharMsgType msgType)
{
	CharMsgDispatcher::Instance()->Register(msgType, this);
}

void
GameWorld::DeleteActionsProvidedBy(TestEntity* character)
{
	//swabMe.SetProviderTestEntity(testEntity);
	for (auto actsItr = actions_.begin();
		 actsItr != actions_.end();)
	{
		if (actsItr->providerTestEntity_ != nullptr &&
			actsItr->providerTestEntity_ == character)
		{
			actsItr = actions_.erase(actsItr);
		}
		else
		{
			actsItr++;
		}
	}
}

void
GameWorld::DeleteActionsProvidedBy(int32 entityID)
{
	// TODO:
}
// ---< Modify


/******************************* OnNotify ***************************************
 * OnNotify is for Entities's Messages
 * OnBroadcast/OnMessage is for Characters's Messages
 * TODO: or make the two system into one?
 ********************************************************************************/
void
GameWorld::OnNotify(const Message& msg)
{
	EntityMessageType msgType = (EntityMessageType)msg.type;
	switch(msgType)
	{
		case EntityMessageType::NeedRefill:
		{
			/**
			int32 a = 100;
			vec2f targetPos(msg.twoReal32.x, msg.twoReal32.y);
			//ItemType itemType = (ItemType)msg.intValue;
			//DecreaseItem(msg.receiver, itemType);
			GOAP::ActionRefillMe refillMe(nullptr, targetPos);
			refillMe.SetProviderEntityID(msg.sender);
			actions_.push_back(refillMe);
			*/
		} break;

		case EntityMessageType::ReadyToOperate:
		{
			/*
			string entityName = GetEntityName(msg.intValue);
			if (entityName.find("XRayMachine") != std::string::npos)
			{
				AddAction("ActionXRayScan", msg.intValue);
			}
			*/
		} break;
	}
}
/******************************* ReleaseAction **********************************
 * so other characters will be able to use this action
 ********************************************************************************/
void
GameWorld::ReleaseAction(const GOAP::Action& action)
{
	GOAP::Action* a = GetAction(action.GetID());
	if (a == nullptr)
		return;
	a->isTaken_ = false;
	int32 b = 100;
}

/******************************* GrabAction *************************************
 * so other characters won't use this action 
 ********************************************************************************/
void
GameWorld::GrabAction(const GOAP::Action& action)
{
	GOAP::Action* a = GetAction(action.GetID());
	if (a == nullptr)
		return;
	a->isTaken_ = true;
}

GOAP::Action*
GameWorld::GetAction(int32 actionID)
{
	for (auto& itr : actions_)
	{
		if (itr.GetID() == actionID)
			return &itr;
	}
	return nullptr;
}

/******************************* DeleteAction ***********************************
 *
 ********************************************************************************/
void
GameWorld::DeleteAction(int32 actionID)
{
	// ---> Remve the action the to be removed entity provides
	for (auto actsItr = actions_.begin();
		 actsItr != actions_.end();)
	{
		if (actsItr->GetID() == actionID)
		{
			int32 a = 100;
			string actName = actsItr->GetName();
			int32 b = 100;
			actsItr = actions_.erase(actsItr);
		}
		else
		{
			actsItr++;
		}
	}
}

// ---> Delete Room
void
GameWorld::DeleteRoom(Room* room)
{
	// NOTE: Don't ever delete the "outside room"
	if (room->GetID() == 0) // outside room
	{
		LOG_GAME_ERROR("Tried to delte the outside room, which is bad");
		return;
	}
	
	room->RemoveAllTiles();
	rooms_.erase(room->GetID());
	delete room;
}

// ---<

// ---> AddRoom
void
GameWorld::AddRoom(Room* room)
{
	rooms_.emplace(room->GetID(), room);
}
// ---< AddRoom

// ---> GetRoom
Room*
GameWorld::GetRoom(RoomID roomID)
{
	auto& roomsItr = rooms_.find(roomID);
	if (roomsItr == rooms_.end())
		return nullptr;
	return roomsItr->second;
}
// ---< GetRoom


/******************************* GetTileNeighbours ******************************
 *
 ********************************************************************************/
std::vector<Tile*>
GameWorld::GetTileNeighbours(Tile* tile)
{
	std::vector<Tile*> neighbours;
	// north
	Tile* north = GetTile(tile->pos.x, tile->pos.y - 1, tile->pos.z);
	if (north)
		neighbours.push_back(north);
	// east
	Tile* east = GetTile(tile->pos.x + 1, tile->pos.y, tile->pos.z);
	if (east)
		neighbours.push_back(east);
	// south
	Tile* south = GetTile(tile->pos.x, tile->pos.y + 1, tile->pos.z);
	if (south)
		neighbours.push_back(south);
	// west
	Tile* west = GetTile(tile->pos.x - 1, tile->pos.y, tile->pos.z);
	if (west)
		neighbours.push_back(west);

	// ---> Diagnoal Neighbours
	/*
	Tile* northWest = GetTile(tile->pos.x - 1, tile->pos.y - 1, tile->pos.z);
	if (northWest)
		neighbours.push_back(northWest);

	Tile* northEast = GetTile(tile->pos.x + 1, tile->pos.y - 1, tile->pos.z);
	if (northEast)
		neighbours.push_back(northEast);

	Tile* southWest = GetTile(tile->pos.x - 1, tile->pos.y + 1, tile->pos.z);
	if (southWest)
		neighbours.push_back(southWest);

	Tile* southEast = GetTile(tile->pos.x + 1, tile->pos.y + 1, tile->pos.z);
	if (southEast)
		neighbours.push_back(southEast);
	// ---< Diagnoal Neighbours
	*/

	return neighbours;
}


/******************************* GetRooms ***************************************
 *
 ********************************************************************************/
std::vector<Room*> 
GameWorld::GetRooms() const
{
	std::vector<Room*> result;
	for (auto& itr : rooms_)
	{
		result.push_back(itr.second);
	}
	return result;
}

/******************************* RegisterPatient ********************************
 *
 ********************************************************************************/
void
GameWorld::RegisterPatient(TestEntity* testEntity)
{
	Patient* patient = static_cast<Patient*>(testEntity);
	registeredPatients_.push(patient);
	int32 a = 100;

	// CharMsgType::PatientRegistered
	CharMsgDispatcher::Instance()->BroadcastMessage(testEntity->GetID(),
													CharMsgType::PatientRegistered,
													testEntity);

}

/******************************* GetEntityName **********************************
 *
 ********************************************************************************/
string
GameWorld::GetEntityName(int32 entityID)
{
	return stateGame_->GetEntityName(entityID);
}

/******************************* GetEntityPos **********************************
 *
 ********************************************************************************/
vec2f
GameWorld::GetEntityPos(int32 entityID)
{
	return stateGame_->GetEntityPos(entityID);
}




/******************************* GetRegisteredPatientFromQueue ******************
 *
 ********************************************************************************/
Patient*
GameWorld::GetRegisteredPatientFromQueue()
{
	Patient* patient = registeredPatients_.front();
	registeredPatients_.pop();
	return patient;
}

/******************************* AddAction **************************************
 *
 ********************************************************************************/
bool
GameWorld::AddAction(const string& actionName, TestEntity* provider)
{
	if (actionName == "SwabMe")
	{
		// TODO: should have a target and an owner
		GOAP::ActionSwabMe swabMe(nullptr, provider->GetPosition());
		swabMe.SetProviderTestEntity(provider);
		/*
		CharMsgDispatcher::Instance()->BroadcastMessage(SENDER_ID_IRRELEVANT,
														CharMsgType::NewPatient,
														testEntity);
														*/
		actions_.push_back(swabMe);
		return true;
	}

	// TODO: Rename to XRayScanMe
	if (actionName == "XRayScan")
	{
		GOAP::ActionXRayScan xRayScan(nullptr, provider->GetPosition());
		xRayScan.SetProviderTestEntity(provider);
		actions_.push_back(xRayScan);
		return true;
	}

	if (actionName == "GiveMeDrug")
	{
		GOAP::ActionGiveMeDrug giveMeDrug(nullptr, provider->GetPosition());
		giveMeDrug.SetProviderTestEntity(provider);
		actions_.push_back(giveMeDrug);
		return true;
	}

	if (actionName == "HookMeToVent")
	{
		GOAP::ActionHookMeToVent actHookMeToVent(nullptr, provider->GetPosition());
		actHookMeToVent.SetProviderTestEntity(provider);
		actions_.push_back(actHookMeToVent);
		return true;
	}
	return false;
}

bool
GameWorld::AddAction(const string& actionName, Room* room)
{
	GOAP::ActionGotoRoom actGotoRoom(nullptr, room);
	actions_.push_back(actGotoRoom);
	return true;
}

bool
GameWorld::AddAction(const string& actionName, int32 providerEntityID)
{
	int32 a = 100;
	//
	GOAP::ActionXRayScan actXRayScan(nullptr, GetEntityPos(providerEntityID));
	actXRayScan.SetProviderEntityID(providerEntityID);
	actions_.push_back(actXRayScan);
	return true;
}

TestEntity*
GameWorld::GetTestEntity(int32 testEntityID)
{
	return stateGame_->GetTestEntityFromID(testEntityID);
}


	
	


