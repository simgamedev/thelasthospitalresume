#pragma once
#include "../Common.h"
#include <map>
#include <fstream>
#include <sstream>
#include <memory>
#include <array>
#include <SFML/Graphics.hpp>
//#include "MapDefinitions.h"
#include "../TileMap/TileInfo.h"
#include "../TileMap/TileMap.h"
#include "../TileMap/TileSet.h"
#include "../Threads/FileLoader.h"
#include "../Utilities.h"
#include "../Window.h"
#include "../TextureManager.h"
#include "../ECS/EntityManager.h"

// ---> Graph
#include "../Graph/SparseGraph.h"
#include "../Graph/GraphEdgeTypes.h"
#include "../Graph/GraphNodeTypes.h"
#include "../Graph/GraphAlgorithms.h"
// ---<

// ---> Modify
//#include "../TestEntity.h"
#include "../Messaging/IMsgObserver.h"
#include "../ECS/Observer.h"
class TestEntity;
class Patient;
#include "../AI/GOAP/Action.h"
// ---< Modify


// ---> Rooms
#include "../Room/Room.h"
// ---< Rooms
class StateGame;
struct vec2i_comparator {
	bool operator()(const vec2i& a, const vec2i& b) const
	{
		if (a.x == b.x)
		{
			return (a.y < b.y);
		}
		else
			return (a.x < b.x);
	}
};
// TODO: should change name to GameWorld
/**
   A GameMap Class.
 */
class GameWorld : public FileLoader, public IMsgObserver, public Observer
{
public:
	typedef SparseGraph<NavGraphNode<void*>, NavGraphEdge> NavGraph;
public:
	/// @todo change this
	GameWorld(Window* window);
	~GameWorld();

	/// @todo change this maybe a singleton to get state manager
	/// @todo or just give gameMap direct access to pointer stateManager_
	void SetStateManager(StateManager* stateManager);

	Tile* GetTile(uint32 x, uint32 y, uint32 elevation = 0);
	Tile* GetTile(vec2i tilePos);
	Tile* GetTile(vec3i tilePos);
	// @todo Get rid fo this
	TileInfo* GetDefaultTileInfo();

	TileSet* GetTileSet(const string& tilesetName);
	TileMap* GetTileMap();

	uint32 GetTileSize() const;
	vec2u GetMapSize() const;
	//vec2f GetPlayerStartPos() const;
	//int32 GetPlayerID() const;

	void PurgeMap();


	void SaveToFile(const string& filepath);

	void Update(real32 dt);
	void ClearRenderTexture(vec3i from= vec3i(0, 0, 0), vec3i to = vec3i(-1, -1, -1));
	void Redraw(vec3i from = vec3i(0, 0, 0),
				vec3i to = vec3i(-1, -1, -1));
	void Draw(uint32 elevation);
	// ---> TEMP
	bool AddTileSet(string& tileSetName);
	bool AddTileSet(TileSet* tileSet);
	// ---< TEMP
	void DEBUGDrawNavGraph(sf::RenderWindow* renderWindow);
	void DEBUGRemoveFromNavGraphDrawable(vec2i tilepos);
	std::map<vec2i, int32, vec2i_comparator> m_DEBUGNavGraphNodeIndices;
	void DEBUGInitNavGraphDrawable();
	void DEBUGInitGrid();
	void DEBUGDrawGrid();
	std::map<int32, sf::CircleShape*> m_DEBUGNavGraphNodeDrawables;
	//std::vector<sf::Vertex> m_DEBUGNavGraphEdgeDrawables;
	std::map<vec2i, std::vector<sf::Vertex>, vec2i_comparator> m_DEBUGNavGraphEdgeDrawables;
	std::vector<sf::RectangleShape*> m_DEBUGGrid;
	std::vector<sf::Text*> m_DEBUGNavGraphNodeIdxTexts;
	bool DoneRedraw() { return bDoneRedraw_; }
protected:
	bool ProcessLine(std::stringstream& stream);
	/// @todo rename it to game window
	Window* window_;
	TextureManager* textureManager_;
	EntityManager* pEntityManager_;
	StateManager* stateManager_;
  
	std::unordered_map<string, TileSet*> tileSets_;
	TileMap tileMap_;

	std::array<sf::RenderTexture, MAX_MAP_LAYERS> renderTextures_;
	sf::Sprite displaySprite_;
	vec2u sheetSize_;

	TileInfo defaultTileInfo_;
	//vec2f playerStartPos_;

	int32 playerID_;
	/// @todo a GameTime struct
	real32 inGameTime_;
	real32 dayLength_;
	bool bDoneRedraw_ = false;

	// ---> Pathfinding
public:
	bool ValidNeighbour(int32 x, int32 y);
	bool ValidNeighbourNew(int32 x, int32 y);
	void InitNavGraph();
	void RemoveFromNavGraph(vec2i tilepos);
	void AddBackToNavGraph(vec2i tilepos);
	NavGraph* GetNavGraph() { return navGraph_; }
protected:
	NavGraph* navGraph_;
	StateGame* stateGame_;
	// ---<

public:
	// ---> Modify
	sf::RectangleShape testShape;
	void DeleteAction(int32 actionID);
	void ReleaseAction(const GOAP::Action& action);
	void GrabAction(const GOAP::Action& action);
	GOAP::Action* GetAction(int32 actionID); // TODO: should be private
	void DeleteActionsProvidedBy(TestEntity* character);
	void DeleteActionsProvidedBy(int32 entityID);
	void OnBroadcast(Telegram telegram);
	void OnMessage(Telegram telegram);
	void OnNotify(const Message& msg);
	void RegisterForMsgType(CharMsgType msgType);
	std::vector<GOAP::Action> GetAvailableActions(TestEntity* testEntity);
	std::queue<int32> newPatients_;
	//void EntityAdded(const string& entityName);
	void EntityRemoved(const int32 entityID);
	void EntityAdded(const int32 entityID);
	void TestEntityBorn(TestEntity* testEntity);
	void NewPositivePatient(TestEntity* testEntity);
	//void EntityRemoved(int32 entityID);
	std::vector<GOAP::Action> actions_;
	// ---< Modify

	// ---> Rooms
	void DeleteRoom(Room* room);
	std::map<RoomID, Room*> rooms_;
	void AddRoom(Room* room);
	std::vector<Room*> GetRooms() const;
	Room* GetRoom(RoomID roomID);
	std::vector<Tile*> GetTileNeighbours(Tile* tile);
	// ---< Rooms

	// ---> Patients
	void RegisterPatient(TestEntity* testEntity);
	std::queue<Patient*> registeredPatients_;
	Patient* GetRegisteredPatientFromQueue();
	// ---> Patients

	// ---> Entities
	string GetEntityName(int32 entityID);
	vec2f GetEntityPos(int32 entityID);
	// ---< Entities

	// ---> Actions
	bool AddAction(const string& actionName, TestEntity* provider);
	bool AddAction(const string& actionName, Room* room);
	bool AddAction(const string& actionName, int32 providerEntityID);
	// ---< Actions

	TestEntity* GetTestEntity(int32 testEntityID);
};

