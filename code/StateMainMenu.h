#pragma once
#include "BaseState.h"
#include "EventManager.h"

#include "MyGUI/MainMenuGUI.xaml.h"


class StateMainMenu : public BaseState
{
public:
	StateMainMenu(StateManager* stateManager);
	~StateMainMenu();

	void OnCreate();
	void OnDestroy();

	void Activate();
	void Deactivate();

	void Update(const sf::Time& time);
	void Draw();

	void MouseClick(EventDetails* eventDetails);
private:
	friend MYGUI::MainMenuGUI;
	void NewGame();
	void LoadGame();
	void Credits();
	void MapEditor();
	void Exit();
	void OnKey1Pressed(EventDetails* eventDetails = nullptr);
	//void Continue(EventDetails* eventDetails = nullptr);
	sf::Sprite* logo_;
	sf::Text versionNumber_;

	// ---> NoesisGUI
	Noesis::Ptr<MYGUI::MainMenuGUI> spMainMenuGUI_;
	Noesis::Ptr<Noesis::IView> spNoesisView_;
	bool bKey1Pressed_;
	// ---< NoesisGUI
};
