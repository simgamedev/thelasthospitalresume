#pragma once

#include <NsCore/Noesis.h>
#include <NsApp/DelegateCommand.h>
#include <NsApp/NotifyPropertyChangedBase.h>
#include <NsApp/DelegateCommand.h>
#include <NsCore/ReflectionImplement.h>
#include "common.h"

// ---> Testing Lua
extern "C"
{
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}
// ---< Testing Lua

#ifdef _WIN32
#pragma comment(lib, "liblua54.a")
#endif

class BasicInfo : public NoesisApp::NotifyPropertyChangedBase
{
public:
	BasicInfo() {}
	~BasicInfo() {}
	// ---> Cash
	real32 GetCash() const { return cash_;  }
	void SetCash(const real32 cash) { cash_ = cash; OnPropertyChanged("Cash"); }
	// ---< Cash
	// ---> ResearchPoints
	int32 GetResearchPoints() const { return researchPoints_; }
	void SetResearchPoints(const int32 rp) { researchPoints_ = rp; OnPropertyChanged("ResearchPoints"); }
	// ---< ResearchPoints

	// ---> NumStaff
	int32 GetNumStaff() const { return numStaff_; }
	void SetNumStaff(const int32 numStaff) { numStaff_ = numStaff; OnPropertyChanged("NumStaff"); }
	// ---< NumStaff
	// ---> StaffCapacity
	int32 GetStaffCapacity() const { return staffCapacity_; }
	void SetStaffCapacity(const int32 staffCapacity) { staffCapacity_ = staffCapacity; OnPropertyChanged("StaffCapacity"); }
	// ---< StaffCapacity
private:
	real32 cash_;
	int32 researchPoints_;
	int32 staffCapacity_;
	int32 numStaff_;
private:
	NS_IMPLEMENT_INLINE_REFLECTION(BasicInfo, NoesisApp::NotifyPropertyChangedBase)
	{
		NsProp("Cash", &BasicInfo::GetCash, &BasicInfo::SetCash);
		NsProp("ResearchPoints", &BasicInfo::GetResearchPoints, &BasicInfo::SetResearchPoints);
		NsProp("NumStaff", &BasicInfo::GetNumStaff, &BasicInfo::SetNumStaff);
		NsProp("StaffCapacity", &BasicInfo::GetStaffCapacity, &BasicInfo::SetStaffCapacity);
	}
};

class GameStateData : public NoesisApp::NotifyPropertyChangedBase
{
public:
	GameStateData();
	~GameStateData();

	void SetDrawTileGrid(bool value) { bDrawTileGrid_ = value; }
	void SetDrawNavGraph(bool value) { bDrawNavGraph_ = value; }
	void SetMouseTilePos(vec2i value) { mouseTilePos_ = value; }
	vec2i GetMouseTilePos() const { return mouseTilePos_; }
	//GameWorld* GetGameWorld() const;
	// ---> Testing Lua
	void CreateLevel(int32 w, int32 h);
	void SetTile(int32 x, int32 y);
	// ---< Testing Lua

	// ---> BasisInfo
	BasicInfo* GetBasicInfo() const { return basicInfo_; }
	void SetBasicInfo(BasicInfo* basicInfo) { basicInfo_ = basicInfo; OnPropertyChanged("BasicInfo"); }
	// ---< BasicInfo
private:
	// ---> Testing Lua
	bool CheckLua(lua_State* L, int r);
	bool ReadIn(const char* filename);
	bool WriteOut(const char* filename);
	// ---< Testing Lua
private:
	bool bDrawTileGrid_ = false;;
	bool bDrawNavGraph_ = false;
	vec2i mouseTilePos_;
	BasicInfo* basicInfo_;
	//GameWorld* pGameWorld_;
private:
	NS_DECLARE_REFLECTION(GameStateData, NotifyPropertyChangedBase);
};
