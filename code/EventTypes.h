#pragma once
#include <SFML/Window/Event.hpp>
/********************************************************************************
 * A Wrapper to sf::Event + Our custom events.
 * EventType::Keyboard, EventType::Mouse, EventType::Joystick is for real-time query
 ****************************** **************************************************/
enum class EventType
{
	KeyDown = sf::Event::KeyPressed,
	KeyUp = sf::Event::KeyReleased,
	MButtonDown = sf::Event::MouseButtonPressed,
	MButtonUp = sf::Event::MouseButtonReleased,
	MouseWheel = sf::Event::MouseWheelMoved,
	GainedFocus = sf::Event::GainedFocus,
	LostFocus = sf::Event::LostFocus,
	MouseEntered = sf::Event::MouseEntered,
	MouseLeft = sf::Event::MouseLeft,
	MouseMoved = sf::Event::MouseMoved,
	Closed = sf::Event::Closed,
	TextEntered = sf::Event::TextEntered,
	Keyboard = sf::Event::Count + 1, Mouse, Joystick,
	GUIClick,
	GUIRelease,
	GUIHover,
	GUILeave,
	GUIFocus,
	GUIDefocus
};

enum class EventInfoType { SFML, GUI = -1 };
