#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "BaseState.h"
#include "EventManager.h"
#include "GUI/GIFileExplorer.h"
#include "GameWorld/GameWorld.h"
#include "MapEditor/MapEditorController.h"

/**
   A State for editing game maps.
 */
class StateMapEditor : public BaseState
{
public:
	StateMapEditor(StateManager* stateManager);
	~StateMapEditor();

	void OnCreate();
	void OnDestroy();

	void Activate();
	void Deactivate();

	void Update(const sf::Time& time);
	void Draw();

	void ResetSavePath();
	void SetMapRedraw(bool redraw);


	void BackToMainMenu(EventDetails* eventDetails);
	void BtnNew(EventDetails* eventDetails);
	void BtnLoad(EventDetails* eventDetails);
	void BtnSave(EventDetails* eventDetails);
	void BtnSaveAs(EventDetails* eventDetails);
 	void BtnExit(EventDetails* eventDetails);
	void SaveOrLoad(const string& filepath);

	// ---> TEMP
	TileSet* GetTileSet(const string& tileSetName) const;
	GameWorld* currentMap_;
	// ---< TEMP
private:
	void SaveMap(const string& filepath);
	void LoadMap(const string& filepath);
	GIFileExplorer* fileExplorer_;
	MapEditorController editorController_;
	string mapSavePath_;
	bool mapRedraw_;
	std::unordered_map<string, TileSet*> tileSets_;
};
