#include "Utilities.h"
#include "StateGame.h"

#include <iostream>
#include "Messaging/CharMsgTypes.h"
#include "GameWorld/GameWorld.h"

// ---> Modify
#include "AI/GOAP/ActionEatSnack.h"
#include "AI/GOAP/ActionTakeSnack.h"
#include "AI/GOAP/ActionUseToilet.h"
#include "AI/GOAP/ActionSwabMe.h"
#include "AI/GOAP/ActionTakeSwab.h"
#include "AI/GOAP/ActionTestSwab.h"
#include "AI/GOAP/ActionGoHome.h"
#include "AI/GOAP/ActionLieInMe.h"
#include "AI/GOAP/ActionTakeDrug.h"
#include "AI/GOAP/ActionDrugMe.h"
#include "AI/GOAP/ActionRefillMe.h"
#include "AI/GOAP/ActionTakeSupplyBoxSnack.h"
#include "AI/GOAP/ActionGotoPos.h"
#include "AI/GOAP/ActionGetRegistered.h"
#include "AI/GOAP/ActionGotoRoom.h"
#include "AI/GOAP/ActionGotoEntity.h"
#include "AI/GOAP/ActionXRayScan.h"
#include "AI/GOAP/ActionGiveMeDrug.h"
#include "AI/GOAP/ActionHookMeToVent.h"
#include "AI/GOAP/ActionTurnOnVent.h"
#include "AI/GOAP/ActionTurnOnXRay.h"
// ---< Modify

#include "Characters/Patient.h"
/*
TestEntity::TestEntity(const string& spriteSheetName, const vec2f& pos, StateGame* stateGame, TextureManager* textureManager,
					   GameWorld* gameWorld, Profession profession)
	*/
TestEntity::TestEntity(Profession profession, const vec2f& pos, StateGame* stateGame)
	: stateGame_(stateGame)
	, pos_(pos)
	, timeElapsed_(0.0f)
	, currentGoal_(nullptr)
	, currentAction_(nullptr)
	, pSteering_(nullptr)
	, m_bLastEdgeInPath(false)
	, currentEdge_(nullptr)
	, DEBUGDrawPath_(false)
	, profession_(profession)
	, currentPatient_(nullptr)
	, holdingItemID_(-1)
	, lastGameTime_(GameTime(0, 0, 0))
{
	mass_ = 1.0f;
	velocity_ = vec2f(0.0f, 0.0f);
	maxSpeed_ = 30.0f;
	maxForce_ = 1000.0f;
	gameWorld_ = stateGame_->GetGameWorld();
	// ---> Setup Sprite
	gameSprite_ = new GameSprite(stateGame_->GetTextureManager());
	gameSprite_->owner_ = this;
	switch (profession)
	{
		case Profession::Tester:
		{
			//gameSprite_->LoadSheet("Characters/Bodies/Tester");
			gameSprite_->LoadSheet("Characters/Bodies/Tester_00");
		} break;

		case Profession::Nurse:
		{
			gameSprite_->LoadSheet("Characters/Bodies/Nurse_00");
		} break;

		case Profession::Janitor:
		{
			gameSprite_->LoadSheet("Characters/Bodies/janitor_body_male_00");
		} break;

		case Profession::Doctor:
		{
			gameSprite_->LoadSheet("Characters/Bodies/Doctor");
		} break;

		case Profession::Worker:
		{
			gameSprite_->LoadSheet("Characters/Bodies/worker_body_female_00");
		} break;

		case Profession::Patient:
		{
			gameSprite_->LoadSheet("Characters/Bodies/Patient");
		} break;

		default: 
		{
		} break;
	}
	gameSprite_->SetOrigin(Origin::MidBottom);
	gameSprite_->SetDirection(Direction::Down);
	gameSprite_->SetPosition(pos_);
	gameSprite_->SetAnimation("Idle", true, true);
	// ---<
	
	//pSteering_ = new SteeringBehaviors(this);
	pSteering_ = new Steering(this);

	heading_ = vec2f(0.0f, -1.0f);
	side_ = Utils::Perpendicular(heading_);

	//pSteering_->WanderOn();


	// ---> GOAP
	// ---> innate actions
	// ActionEatSnack
	GOAP::ActionEatSnack eatSnack(this);
	eatSnack.isInnate_ = true;
	innateActions_.push_back(eatSnack);

	// ActionGoHome
	switch (profession_)
	{
		case Profession::Patient:
		{
			/*
			*/
		} break;
	}
	// ---< innate actions

	// ---< GOAP

	//---> Path Finding
	pathPlanner_ = new PathPlanner(this);
	//---<

	// ---> Modify
	goapPlanner_ = new GOAP::Planner();
	stateMachine_ = new FSM(this);
	stateMachine_->PushState(AgentStateType::Idle);
	goalArbitrator_ = new GOAP::Arbitrator(this);
	// ---> innate goals
	// ---< innate goals
	// ---< Modify


	SetPosition(pos_);
	Init();

	// messaing
	if (profession_ == Profession::Janitor)
		EntityMessageHandler::Instance()->Subscribe(EntityMessageType::NeedRefill, this);

	/*
	GOAP::ActionGotoPos* actGotoPos = new GOAP::ActionGotoPos(this, vec2f(500, 500));
	currentPlan_.push_back(actGotoPos);
	stateMachine_->PushState(AgentStateType::Perform);
	*/
}
/*********************************************************************************
 * Init.
 *********************************************************************************/
void
TestEntity::Init()
{
	switch (profession_)
	{
		case Profession::Patient:
		{
			name_ = "Patient John";
			numIVsTook_ = 0;
			numDrugsTook_ = 0;
		} break;

		case Profession::Nurse:
		{
			name_ = "Nurse John";
		} break;
	}
	//bloodType_ = BloodType::NegativeO;
	temperature_ = 36.5f;
	// TODO: ??
	// FIXME:?
	//state_ = TestEntityState::Idle;
	hunger_ = 100;
	toilet_ = 100;
	switch(profession_)
	{
		case Profession::Nurse:
		{
			infected_ = false;
		} break;

		case Profession::Patient:
		{
			infected_ = true;
			pcrTested_ = false;
			infectionStage_ = 0;
		} break;
	}
	//agentBeliefs_.SetVariable(std::make_pair("kIsHungry", GetID()), 0);
	agentBeliefs_.SetVariable("kIsHungry", 0);
	agentBeliefs_.SetVariable("kIsInNeedOfToilet", 0);
	agentBeliefs_.SetVariable("kIsPCRTested", 0);
}



/******************************* Destructor *************************************
 *
 ********************************************************************************/
TestEntity::~TestEntity()
{
	// TODO: delete the actions that your provided to the GameWorld
	gameWorld_->DeleteActionsProvidedBy(this);
	int32 a = 100;
	if (pSteering_)
	{
		delete pSteering_;
	}
}


/*
void
TestEntity::SetSeekOn(const bool& seekON)
{
	seekON_ = seekON;
}
*/

/*
void
TestEntity::SetTargetPos(const vec2f& targetPos)
{
	targetPos_ = targetPos;
}
*/

/******************************* Update ****************************************
 *******************************************************************************/
void
TestEntity::Update(const sf::Time& time)
{
	if (profession_ == Profession::Patient)
	{
		int32 a = 100;
	}
	real32 dt = time.asSeconds();
	// ---> Modify
	if (currentAction_)
	{
		currentAction_->Update(time);
	}
	timeElapsed_ += dt;
	GameTime currentGameTime = Clock::Instance()->GetGameTime();
	UpdateAI(time);
	stateMachine_->Update(time);
	if(stateMachine_->GetCurrentState()->GetStateType() == AgentStateType::Idle &&
	   timeElapsed_ > 2.0f)
	{
		timeElapsed_ = 0.0f;
		//UpdateMovement(dt);
		UpdateWander(dt);
		// if you are in idle state, that means you don't have an active plan
		//GOAP::WorldState* currentState = GetAgentBeliefs();
		/*
		GameTime gameTimeDiff = currentGameTime - lastGameTime_;
		if (gameTimeDiff.minutes_ < 2)
			return;
		lastGameTime_ = currentGameTime;
		*/
		GOAP::Goal* goal = goalArbitrator_->GetGoal();
		currentGoal_ = goal;
		if (goal)
		{
			if (goal->GetName() == "GotoEntityXRayMachine")
			{
				int32 a = 100;
				int32 b = 200;
			}
			std::vector<GOAP::Action> availableActions = gameWorld_->GetAvailableActions(this);
			for (GOAP::Action action : innateActions_)
			{
				availableActions.push_back(action);
			}
			std::vector<GOAP::Action> plan;
			plan = goapPlanner_->Plan(agentBeliefs_,
									  goal->GetDesiredState(),
									  availableActions);
			if (plan.size() > 0)
			{
				if (goal->GetName() == "GiveXRayScan")
				{
					int32 a = 100;
					int32 b = 200;
				}
				// we have a plan, hooray!
				CopyPlan(plan);
				stateMachine_->PopState();
				stateMachine_->PushState(AgentStateType::Perform);
			}
			else
			{
				goalArbitrator_->OnGoalNotAchievable(goal);
				// ugh, we couldn't get a plan
				//Logger::Instance()->EngineLogger()->info("Has Goal but no plan, so Idling!!!");
			}
		}
		else
		{
			// ugh, we don't have any goal, keep being idle.
			//Logger::Instance()->EngineLogger()->info("Doesn't has any goal so Idling!!!");
		}
	}

	// ----------> AgentStateType::MoveTo
	if(stateMachine_->GetCurrentState()->GetStateType() == AgentStateType::MoveTo)
	{
		if (id_ == 0)
		{
			LOG_ENGINE_ERROR("MoveTo Updating!");
		}
		UpdateMovement(dt);

		// arrived at target pos
		//if(IsAtTargetPos())
		if(currentAction_->IsInRange())
		{
			//pSteering_->ArriveOff();
			stateMachine_->PopState();
		}
	}
	// ----------< AgentStateType::MoveTo


	// ----------> AgentStateType::Perform
	if(stateMachine_->GetCurrentState()->GetStateType() == AgentStateType::Perform)
	{
		// ---> Goal/Plan Finished
		if(!HasActivePlan())
		{
			// Done actions!
			stateMachine_->PopState();
			stateMachine_->PushState(AgentStateType::Idle);
			OnPlanFinished();
			return;
		}
		// ---< Goal/Plan Finished


		// ---> Current Action Done or Failed!
		currentAction_ = currentPlan_.front();
		if(currentAction_->IsDone())
		{
			// ---> Action failed!
			if (currentAction_->EndPerform() == false)
			{
				OnPlanAborted(*currentAction_);
				delete currentAction_;
				currentAction_ = nullptr;
			}
			// ---<
			// ---> Action succeeded
			else
			{
				if (currentAction_->isOneTime_)
				{
					gameWorld_->DeleteAction(currentAction_->GetID());
				}
				if (currentAction_->IsOnePerson())
				{
					if (currentAction_->GetName() == "GotoEntity")
					{
						int32 a = 100;
					}

					gameWorld_->ReleaseAction(*currentAction_);
				}
				delete currentAction_;
				currentAction_ = nullptr;
				currentPlan_.pop_front();
			}
			// ---<
			return;
		}
		// ---<

		// ---> Action start to perform
		if(HasActivePlan() &&
		   !currentAction_->IsStarted())
		{
			// ---> Check action procedural preconditions
			if (!currentAction_->CheckProceduralPreconditions())
			{
				OnPlanAborted(*currentAction_);
				currentAction_ = nullptr;
				return;
			}
			// ---<

			// ---> Check if action owner in in range to perform this action
			currentAction_ = currentPlan_.front();
			bool inRange = currentAction_->RequiresInRange() ? currentAction_->IsInRange() : true;
			if(inRange)
			{
				bool success = currentAction_->StartPerform();
				if (currentAction_->GetAnimName() == "Fiddle")
				{
					PlayAnimation("Fiddle", true, true);
				}
				if(!success)
				{
					// action failed, we need to plan again
					//stateMachine_->PopState();
					//stateMachine_->PushState(AgentStateType::Idle);
					OnPlanAborted(*currentAction_);
				}
			}
			// ---> Action owner should move to action range first
			else
			{
				GotoPosition(currentAction_->GetTargetPos());
				stateMachine_->PushState(AgentStateType::MoveTo);
			}
			// ---<
		}
	}
	if(stateMachine_->GetCurrentState()->GetStateType() == AgentStateType::Dead)
	{
		// just be a nice corpse
	}
	if(stateMachine_->GetCurrentState()->GetStateType() == AgentStateType::Crazy)
	{
		// attack every non crazy person in sight
	}
	// ---< Modify 
	gameSprite_->Update(dt);
	// ---> AI
	//UpdateAI(dt);
	// ---< 
}
void
TestEntity::SetPosition(const vec2f& position)
{
	pos_ = position;
	//sprite_.setPosition(position);
	gameSprite_->SetPosition(position);
	tilepos_.x = pos_.x / TILE_SIZE;
	tilepos_.y = pos_.y / TILE_SIZE;

	if (holdingItemID_ != -1)
		stateGame_->GetEntityManager()->SetEntityPos(holdingItemID_, pos_);
}


void
TestEntity::Draw(Window* window)
{
	gameSprite_->Draw(window);
	//DEBUGDraw(renderWindow);
	// draw path
	if (DEBUGDrawPath_)
	{
		// draw edges
		/*
		if (DEBUGPathDrawable_.size() > 0)
		{
			renderWindow->draw(&DEBUGPathDrawable_[0],
							   DEBUGPathDrawable_.size(),
							   sf::Lines);
		}
		*/
	}
}

/******************************* DEBUGUpdatePathDrawable ************************
 *
 ********************************************************************************/
void
TestEntity::DEBUGUpdatePathDrawable()
{
	DEBUGPathDrawable_.clear();
	if (m_PathAsEdges.size() > 0)
	{
		for (auto& edge : m_PathAsEdges)
		{
			vec2f from(edge.Source());
			vec2f to(edge.Destination());
			sf::Vertex f(from);
			f.color = sf::Color(255, 0, 0, 255);
			sf::Vertex t(to);
			t.color = sf::Color(255, 0, 0, 255);
			DEBUGPathDrawable_.push_back(f);
			DEBUGPathDrawable_.push_back(t);
		}
	}
}

/*
void
TestEntity::DEBUGDraw(sf::RenderWindow* renderWindow)
{
	sf::RectangleShape headingVector;
	headingVector.setSize(vec2f(10.0f, 1.0f));
	headingVector.setOrigin(vec2f(0.0f, 0.5f));
	headingVector.setPosition(pos_);
	headingVector.setFillColor(sf::Color(0, 255, 0, 255));
	real32 headingAngle = Utils::HeadingToAngle(heading_);
	headingVector.setRotation(headingAngle);


	sf::CircleShape headingVectorArrow(3, 3);

	headingVectorArrow.setOrigin(vec2f(3, 6));
	headingVectorArrow.setPosition(pos_ + heading_ * 8.0f);
	headingVectorArrow.setFillColor(sf::Color(255, 0, 0, 255));
	headingVectorArrow.setRotation(headingAngle + 90);
	//std::cout << "angle:" << headingAngle << std::endl;
	//std::cout << "heading:" << heading_.x << ", " << heading_.y << std::endl;
	renderWindow->draw(headingVector);
	renderWindow->draw(headingVectorArrow);
	//pSteering_->DEBUGDraw(renderWindow);


}
*/
/*********************************************************************************
 * Getters.
 *********************************************************************************/
vec2i
TestEntity::GetTilepos() const
{
	return tilepos_;
}
vec2f
TestEntity::GetPosition() const
{
	return pos_;
}
real32
TestEntity::GetMaxSpeed() const
{
	return maxSpeed_;
}
vec2f
TestEntity::GetVelocity() const
{
	return velocity_;
}

/*
vec2f
TestEntity::GetTargetPos() const
{
	return targetPos_;
}
*/


real32
TestEntity::GetTimeElapsed() const
{
	return timeElapsed_;
}

vec2f 
TestEntity::GetHeading() const
{
	return heading_;
}
// side is  perpendicular to heading
vec2f 
TestEntity::GetSide() const
{
	return side_;
}

// Get ID
int32 
TestEntity::GetID() const
{
	return id_;
}



/*********************************************************************************
 * Update AI.
 *********************************************************************************/
// TODO: should be renamed to UpdateAgentBeliefs??
void
TestEntity::UpdateAI(const sf::Time& time)
{
	// Update AI 1 time per  real world second
	if (timeElapsed_ < 3.0f) return;

	// ---> hunger
	if(profession_ != Profession::Janitor)
		hunger_ -= 1;
	if (hunger_ <= 80)
	{
		agentBeliefs_.SetVariable("kIsHungry", 1);
	}
	else
	{
		agentBeliefs_.SetVariable("kIsHungry", 0);
	}
	// ---< hunger

	// ---> toilet
	toilet_ -= 1;
	if (toilet_ <= 60)
	{
		agentBeliefs_.SetVariable("kIsInNeedOfToilet", 1);
	}
	else
	{
		agentBeliefs_.SetVariable("kIsInNeedOfToilet", 0);
	}
	// ---<

	// ---> Treatment
	if (numDrugsTook_ >= 1)
	{
		agentBeliefs_.SetVariable("kIsCured", 1);
	}
	// ---< Treatment
	goalArbitrator_->Update();
}


/*********************************************************************************
 * Play Animation.
 *********************************************************************************/
void
TestEntity::PlayAnimation(const string& name, bool play, bool loop)
{
	std::cout << "Should Play Animation: " << name << std::endl;
	gameSprite_->SetAnimation(name, play, loop);
}

/*********************************************************************************
 *
 *********************************************************************************/
int32
TestEntity::GetAvailableEntity(const string& name)
{
	return stateGame_->GetEntityID(name);
}

vec2f
TestEntity::GetTargetEntityPos(const int32& entityID)
{
	return stateGame_->GetEntityPos(entityID);
}

real32
TestEntity::GetMaxForce() const
{
	return maxForce_;
}


//---> Real Path Finding
void
TestEntity::GotoPosition(vec2f pos)
{
	pathPlanner_->RequestPathToPosition(pos);
}
//---<

// CanWalkTo
bool
TestEntity::CanWalkTo(const vec2f& pos)
{
	return false;
}

// Get Game World
GameWorld*
TestEntity::GetGameWorld()
{
	return stateGame_->GetGameWorld();
}

//
void
TestEntity::FollowPath()
{
	currentEdge_ = nullptr;
	if (m_PathAsEdges.size() > 0)
	{
		//std::cout << "Num Edges Left: " << m_PathAsEdges.size() << std::endl;
		PathEdge edge = m_PathAsEdges.front();
		currentEdge_ = new PathEdge(edge);

		m_PathAsEdges.pop_front();

		pSteering_->SetTarget(edge.Destination());

		if (m_PathAsEdges.size() == 0)
		{
			pSteering_->ArriveOn();
		}
		else
		{
			pSteering_->SeekOn();
		}
	}
}

//---> TEMP
void
TestEntity::ArriveAt(vec2f pos)
{
	pSteering_->SetTarget(pos);
	pSteering_->ArriveOn();
}

void
TestEntity::SeekTo(vec2f pos)
{
	pSteering_->SetTarget(pos);
	pSteering_->SeekOn();
}

void
TestEntity::Flee(const vec2f& target)
{
	pSteering_->SetTarget(target);
	pSteering_->FleeOn();
}
//---<

// Can walk between
bool 
TestEntity::CanWalkBetween(const vec2f& start, const vec2f& end)
{
	return true;
}

// ---> Modify
GOAP::WorldState*
TestEntity::GetAgentBeliefs()
{
	GOAP::WorldState* result = nullptr;
	if (hunger_ < 50)
	{
		result = new GOAP::WorldState("Hungry");
		result->SetVariable("kIsHungry", 1);
	}
	else
	{
		result = new GOAP::WorldState("Hungry");
		result->SetVariable("kIsHungry", 0);
	}
	return result;
}

/*
bool
TestEntity::IsAtTargetPos()
{
	if (std::abs(GetPosition().x - targetPos_.x) < 0.1f &&
		std::abs(GetPosition().y - targetPos_.y) < 0.1f)
		return true;
	return false;
}
*/

/*
bool
TestEntity::Perform(GOAP::Action* action)
{
	if (!action->CheckProceduralPreconditions())
	{
		return false;
	}
	LOG_GAME_ERROR("Should play anim" + action->GetAnimName());
	//gameSprite_->SetAnimation(action->GetAnimName(), true, false);
	action->StartPerform();
	return true;
}
*/

bool
TestEntity::HasActivePlan()
{
	return (currentPlan_.size() > 0);
}


/******************************* OnPlanFinished *********************************
 * TODO: proper way to remove finished goals.
 ********************************************************************************/
void 
TestEntity::OnPlanFinished()
{
	// ---> For all types of TestEntity
	// should play idle animation
	gameSprite_->SetAnimation("Idle", true, true);
	ClearCurrentPlan();
	// ---<

	// ---> Specific stuff for each profession
	switch (profession_)
	{
		case Profession::Patient:
		{
			Patient* patient = static_cast<Patient*>(this);
			if (patient)
			{
				patient->OnPlanFinished();
			}
		} break;

		case Profession::Tester:
		{
			Tester* tester = static_cast<Tester*>(this);
			if (tester)
			{
				tester->OnPlanFinished();
			}
		} break;
	}
	// ---<
}

void 
TestEntity::OnPlanAborted(GOAP::Action& failedAction)
{
	LOG_GAME_ERROR("PlanAborted!!!!");
	if (failedAction.IsOnePerson())
	{
		gameWorld_->ReleaseAction(failedAction);
	}
	//gameSprite_->SetAnimation("Idle", true, true);
	ClearCurrentPlan();
	currentGoal_ = nullptr;
	// FIXME: if you uncomment it it will crash
	stateMachine_->PopState();
	stateMachine_->PushState(AgentStateType::Idle);
	//LOG_GAME_INFO("Sad! Plan Aborted due to a failed action!");
	//LOG_GAME_INFO(failedAction.GetName());
}

void
TestEntity::DecreaseHunger(int32 amount)
{
	hunger_ += amount;
}

void
TestEntity::IncreaseToiletSatisfaction(int32 amount)
{
	toilet_ += amount;
}

void
TestEntity::DecreaseToiletSatisfaction(int32 amount)
{
	toilet_ -= amount;
}

void
// TODO: CopyPlan also means PlanStarted
// TODO: Properly refactor CopyPlan
TestEntity::CopyPlan(std::vector<GOAP::Action> plan)
{
	pSteering_->WanderOff();
	//std::reverse(std::begin(plan), std::end(plan));
	for (GOAP::Action& action : plan)
	{
		// ---> Grab the action so others can't use it
		if (!action.isInnate_)
		{
			if (action.GetName() == "LieInMe")
			{
				int32 a = 100;
			}
			if (action.isOnePerson_)
			{
				gameWorld_->GrabAction(action);
			}
		}
		// ---<
		// ---> TakeSnack
		if (action.GetName() == "TakeSnack")
		{
			GOAP::ActionTakeSnack* actionTakeSnack = new GOAP::ActionTakeSnack(this, action.GetTargetPos());
			actionTakeSnack->SetOwner(this);
			actionTakeSnack->SetProviderEntityID(action.providerEntityID_);
			// TODO: action id should be generated when it's put in the game world.
			actionTakeSnack->id_ = action.id_;
			currentPlan_.push_front(actionTakeSnack);
		}
		// ---< TakeSnack

		// ---> EatSnack
		if (action.GetName() == "EatSnack")
		{
			currentPlan_.push_front(new GOAP::ActionEatSnack(this));
		}
		// ---< EatSnack
		
		// ---> UseToilet
		if (action.GetName() == "UseToilet")
		{
			currentPlan_.push_front(new GOAP::ActionUseToilet(this, action.GetTargetPos()));
		}
		// ---< UseToilet

		// ---> SwabMe
		if (action.GetName() == "SwabMe")
		{
			GOAP::ActionSwabMe* actionSwabMe = new GOAP::ActionSwabMe(this, action.GetTargetPos());
			// TODO: need a better way to copy actions, yeah!! just copy constructor right?
			actionSwabMe->id_ = action.id_;
			actionSwabMe->SetProviderTestEntity(action.GetProviderTestEntity());
			currentGoal_->SetTargetTestEntity(action.GetProviderTestEntity());
			currentPlan_.push_front(actionSwabMe);
		}
		// ---< SwabMe

		// ---> TakeSwab
		if (action.GetName() == "TakeSwab")
		{
			GOAP::ActionTakeSwab* actionTakeSwab = new GOAP::ActionTakeSwab(this, action.GetTargetPos());
			actionTakeSwab->id_ = action.id_;
			currentPlan_.push_front(actionTakeSwab);
		}
		// ---< TakeSwab

		// ---> TestSwab
		if (action.GetName() == "TestSwab")
		{
			GOAP::ActionTestSwab* actionTestSwab = new GOAP::ActionTestSwab(this, action.GetTargetPos());
			//actionTestSwab->SetTargetCharacter(currentPatient_);
			actionTestSwab->id_ = action.id_;
			actionTestSwab->SetOwnerGoal(currentGoal_);
			actionTestSwab->providerEntityID_ = action.providerEntityID_;
			currentPlan_.push_front(actionTestSwab);
		}
		// ---< TestSwab

		// ---> LieInMe
		if (action.GetName() == "LieInMe")
		{
			GOAP::ActionLieInMe* actionLieInMe = new GOAP::ActionLieInMe(this, action.GetTargetPos());
			//actionTestSwab->SetTargetCharacter(currentPatient_);
			actionLieInMe->id_ = action.id_;
			actionLieInMe->SetOwnerGoal(currentGoal_);
			currentPlan_.push_front(actionLieInMe);
		}
		// ---< LieInMe

		// ---> TakeDrug
		if (action.GetName() == "TakeDrug")
		{
			GOAP::ActionTakeDrug* actionTakeDrug = new GOAP::ActionTakeDrug(this, action.GetTargetPos());
			actionTakeDrug->id_ = action.id_;
			actionTakeDrug->SetOwnerGoal(currentGoal_);
			currentPlan_.push_front(actionTakeDrug);
		}
		// ---< TakeDrug

		// ---> DrugMe
		if (action.GetName() == "DrugMe")
		{
			GOAP::ActionDrugMe* actionDrugMe = new GOAP::ActionDrugMe(this, action.GetTargetPos());
			//actionTestSwab->SetTargetCharacter(currentPatient_);
			actionDrugMe->id_ = action.id_;
			actionDrugMe->SetProviderTestEntity(action.GetProviderTestEntity());
			currentGoal_->SetTargetTestEntity(action.GetProviderTestEntity());
			actionDrugMe->SetOwnerGoal(currentGoal_);
			currentPlan_.push_front(actionDrugMe);
		}
		// ---< DrugMe

		// ---> GoHome
		if (action.GetName() == "GoHome")
		{
			GOAP::ActionGoHome* actionGoHome = new GOAP::ActionGoHome(this, action.GetTargetPos());
			//actionTestSwab->SetTargetCharacter(currentPatient_);
			//actionDrugMe->SetProviderTestEntity(action.GetProviderTestEntity());
			//currentGoal_->SetTargetTestEntity(action.GetProviderTestEntity());
			//actionDrugMe->SetOwnerGoal(currentGoal_);
			currentPlan_.push_front(actionGoHome);
		}
		// ---< GoHome

		// ---> ActionGotoRoom
		if (action.GetName() == "GotoRoom")
		{
			//GOAP::ActionGotoRoom* actionGotoRoom = new GOAP::ActionGotoRoom(thi
			GOAP::ActionGotoRoom* actGotoRoom = new GOAP::ActionGotoRoom(this, action.pRoom_);
			// FIXME: id_ should mean type or not??
			actGotoRoom->id_ = action.id_;
			currentPlan_.push_front(actGotoRoom);
		}
		// ---<

		// ---> Action GotoEntity
		if (action.GetName() == "GotoEntity")
		{
			//GOAP::ActionGotoRoom* actionGotoRoom = new GOAP::ActionGotoRoom(thi
			GOAP::ActionGotoEntity* actGotoEntity = new GOAP::ActionGotoEntity(this,
																			   action.providerEntityID_,
																			   action.providerEntityName_,
																			   action.targetPos_);
			actGotoEntity->id_ = action.id_;
			actGotoEntity->SetProviderTestEntity(action.GetProviderTestEntity());
			currentPlan_.push_front(actGotoEntity);
		}
		// ---<

		// ---> RefillMe
		if (action.GetName() == "RefillMe")
		{
			GOAP::ActionRefillMe* actionRefillMe = new GOAP::ActionRefillMe(this, action.GetTargetPos());
			actionRefillMe->id_ = action.id_;
			actionRefillMe->SetProviderEntityID(action.providerEntityID_);
			actionRefillMe->SetOwnerGoal(currentGoal_);
			currentPlan_.push_front(actionRefillMe);
		}
		// ---< RefillMe

		// ---> TakeSupplyBoxSnack
		if (action.GetName() == "TakeSupplyBoxSnack")
		{
			GOAP::ActionTakeSupplyBoxSnack* actionTakeSupplyBoxSnack = new GOAP::ActionTakeSupplyBoxSnack(this, action.GetTargetPos());
			//actionTestSwab->SetTargetCharacter(currentPatient_);
			actionTakeSupplyBoxSnack->id_ = action.id_;
			actionTakeSupplyBoxSnack->SetProviderEntityID(action.providerEntityID_);
			actionTakeSupplyBoxSnack->SetOwnerGoal(currentGoal_);
			currentPlan_.push_front(actionTakeSupplyBoxSnack);
		}
		// ---< TakeSupplyBoxSnack

		// ---> GetRegistered
		if (action.GetName() == "GetRegistered")
		{
			GOAP::ActionGetRegistered* actionGetRegistered = new GOAP::ActionGetRegistered(this, action.GetTargetPos());
			actionGetRegistered->id_ = action.id_;
			actionGetRegistered->SetProviderEntityID(action.providerEntityID_);
			actionGetRegistered->SetOwnerGoal(currentGoal_);
			currentPlan_.push_front(actionGetRegistered);
		}
		// ---< GetRegistered

		// ---> XRayScan
		if (action.GetName() == "XRayScan")
		{
			GOAP::ActionXRayScan* actXRayScan = new GOAP::ActionXRayScan(this,
																		 action.GetTargetPos());
			//actionTestSwab->SetTargetCharacter(currentPatient_);
			actXRayScan->id_ = action.id_;
			actXRayScan->SetIsOneTime(true);
			actXRayScan->SetOwnerGoal(currentGoal_);
			actXRayScan->SetProviderTestEntity(action.GetProviderTestEntity());
			currentGoal_->SetTargetTestEntity(action.GetProviderTestEntity());
			currentPlan_.push_front(actXRayScan);
		}
		// ---< XRayScan

		// ---> XRayScan
		if (action.GetName() == "TurnOnXRay")
		{
			GOAP::ActionTurnOnXRay* actTurnOnXRay = new GOAP::ActionTurnOnXRay(this,
																		       action.GetTargetPos());
			//actionTestSwab->SetTargetCharacter(currentPatient_);
			actTurnOnXRay->id_ = action.id_;
			//actTurnOnXRay->SetIsOneTime(true);
			actTurnOnXRay->SetOwnerGoal(currentGoal_);
			actTurnOnXRay->SetProviderEntityID(action.GetProviderEntityID());
			//actTurnOnXRay->SetProviderTestEntity(action.GetProviderTestEntity());
			currentGoal_->SetTargetTestEntity(action.GetProviderTestEntity());
			currentPlan_.push_front(actTurnOnXRay);
		}
		// ---< XRayScan

		// ---> GiveMeDrug
		if (action.GetName() == "GiveMeDrug")
		{
			GOAP::ActionGiveMeDrug* actGiveMeDrug = new GOAP::ActionGiveMeDrug(this,
																			  action.GetTargetPos());

			actGiveMeDrug->id_ = action.id_;
			actGiveMeDrug->SetIsOneTime(true);
			actGiveMeDrug->SetOwnerGoal(currentGoal_);
			actGiveMeDrug->SetProviderTestEntity(action.GetProviderTestEntity());
			currentGoal_->SetTargetTestEntity(action.GetProviderTestEntity());
			currentPlan_.push_front(actGiveMeDrug);
		}
		// ---< GiveMeDrug
	}
}

void
TestEntity::ClearCurrentPlan()
{
	// you are responsible for
	// delete delete delete
	for (auto itr : currentPlan_)
	{
		delete itr;
	}
	currentPlan_.clear();
}

int32
TestEntity::GetToiletSatisfaction() const
{
	return toilet_;
}

std::deque<GOAP::Action*> 
TestEntity::GetCurrentPlan() const
{
	return currentPlan_;
}

AgentStateType
TestEntity::GetCurrentState() const
{
	return stateMachine_->GetCurrentState()->GetStateType();
}

Profession
TestEntity::GetProfession() const
{
	return profession_;
}

void
TestEntity::OnMessage(Telegram telegram)
{
	// ----------> For All types of TestEntity
	// ---> Path ready
	if (telegram.type == CharMsgType::PathReady)
	{
		m_PathAsEdges = pathPlanner_->GetPath();
		DEBUGUpdatePathDrawable();
		DEBUGDrawPath_ = true;
		FollowPath();
	}
	// ---<

	// ---> No Path Available
	if (telegram.type == CharMsgType::NoPathAvailable)
	{
		int32 a = 100;
		OnPlanAborted(*currentAction_);
	}
	// ---<
	// ----------< For All types of TestEntity

	switch (profession_)
	{
		case Profession::Patient:
		{
			Patient* patient = static_cast<Patient*>(this);
			patient->OnMessage(telegram);
		} break;

		case Profession::Tester:
		{
		} break;
	}
	/*
	if (profession_ == Profession::Patient)
	{
		int32 myID = id_;
		int32 a = 100;
		switch (telegram.type)
		{
			case CharMsgType::TestResultReady:
			{
				if (*static_cast<bool*>(telegram.extraInfo) == true)
				{
					LOG_GAME_INFO("Oh!!!!God!!! I'm infected!!!");
					// goal
					GOAP::WorldState desiredState;
					desiredState.SetVariable("kIsInBed", 1);
					GOAP::Goal* goal = new GOAP::Goal("LieInBed", desiredState, 3.0f);
					//goal->SetTargetTestEntity(broadcast.senderTestEntity);
					goalArbitrator_->AddGoal(goal);
					// actions
					CharMsgDispatcher::Instance()->BroadcastMessage(id_,
																	CharMsgType::NeedDrug,
																	nullptr);
					//GameTimeDuration duration(1, 0, 0);
					GameTime curGameTime = Clock::Instance()->GetGameTime();
					GameTime futureGameTime(curGameTime.days_,
											curGameTime.hours_,
											curGameTime.minutes_ + 20);
					CharMsgDispatcher::Instance()->BroadcastDelayedMessage(futureGameTime,
																		   id_,
																	       CharMsgType::NeedDrug,
																	       nullptr);
					futureGameTime.hours_ += 2;
					CharMsgDispatcher::Instance()->BroadcastDelayedMessage(futureGameTime,
																		   id_,
																	       CharMsgType::NeedDrug,
																	       nullptr);
					futureGameTime.hours_ += 2;
					CharMsgDispatcher::Instance()->BroadcastDelayedMessage(futureGameTime,
																		   id_,
																	       CharMsgType::NeedDrug,
																	       nullptr);
				}
				else
				{
					LOG_GAME_INFO("I'm not infected!! Going home to mah wife!");
					GOAP::WorldState desiredState;
					desiredState.SetVariable("kIsHome", 1);
					GOAP::Goal* goal = new GOAP::Goal("GoHome", desiredState, 3.0f);
					//goal->SetTargetTestEntity(broadcast.senderTestEntity);
					goalArbitrator_->AddGoal(goal);
				}
			} break;
			default:
			{
			} break;
		}
	}
	*/
}

void
TestEntity::OnBroadcast(Telegram telegram)
{
	// TODO: Patient Class, Janitor Class, Security Class, Doctor Class
	switch (telegram.type)
	{
		case CharMsgType::NewLitter:
		{
			if (profession_ == Profession::Janitor)
			{
				// beliefs
				agentBeliefs_.SetVariable("kIsFloorClean", 0);
				// goal
				GOAP::WorldState desiredState;
				desiredState.SetVariable("kIsFloorClean", 1);
				GOAP::Goal* goal = new GOAP::Goal("SweepLitter", desiredState, 1000.0f);
				goalArbitrator_->AddGoal(goal);

			}
		} break;

		case CharMsgType::NewPatient:
		{
			/*
			if (profession_ == Profession::Tester)
			{
				GOAP::WorldState desiredState;
				desiredState.SetVariable("kIsPCRTested", 1);
				GOAP::Goal* goal = new GOAP::Goal("GivePCRTest", desiredState, 300.0f);
				goalArbitrator_->AddGoal(goal);
			}
			*/
		} break;

		case CharMsgType::NeedDrug:
		{
			LOG_GAME_ERROR("A Nurse should have a new GiveDrug Goal");
			GOAP::WorldState desiredState;
			desiredState.SetVariable("kIsDrugged", 1);
			GOAP::Goal* goal = new GOAP::Goal("GiveDrug", desiredState, 300.0f);
			//goal->SetTargetTestEntity(broadcast.senderTestEntity);
			goalArbitrator_->AddGoal(goal);

		} break;

		case CharMsgType::NeedChangeIV:
		{
		} break;
	}
}

void 
TestEntity::SetID(int32 id)
{
	id_ = id;
}

/******************************* UpdateWander ***********************************
 *
 ********************************************************************************/
void
TestEntity::UpdateWander(const real32& dt)
{
	//SetPosition(vec2f(100.0f, 100.0f));
	real32 randFloat = Math::RandFloat();
	//std::cout << "rand:" << randFloat << std::endl;
}

void
TestEntity::UpdateMovement(const real32& dt)
{
		// ---> MOVEMENT
		// calculate the combined force from each steering behaviors
		vec2f steeringForce = pSteering_->Calculate();

		// acceleration = force/mass
		vec2f acceleration = steeringForce / mass_;

		// update velocity
		//velocity_ += acceleration * dt;
		velocity_ = steeringForce;

		Utils::Truncate(velocity_, maxSpeed_);

		// update position
		SetPosition(vec2f(pos_ + velocity_ * dt));

		if (Utils::LengthSq(velocity_) > 0.00000001)
		{
			heading_ = Utils::Normalize(velocity_);

			side_ = Utils::Perpendicular(heading_);
		}
		// ---< MOVEMENT


		// ---> Walking Animation
		if (std::abs(heading_.x) > std::abs(heading_.y))
		{
			if (heading_.x > 0)
			{
				gameSprite_->SetDirection(Direction::Right);
			}
			else if (heading_.x < 0)
			{
				gameSprite_->SetDirection(Direction::Left);
			}
		}
		else
		{
			if (heading_.y < 0)
			{
				gameSprite_->SetDirection(Direction::Up);
			}
			else if (heading_.y > 0)
			{
				gameSprite_->SetDirection(Direction::Down);
			}
		}
		// ---<
		if (currentEdge_ != nullptr)
		{
			//std::cout << "current pos: " << GetPosition().x << "," << GetPosition().y << std::endl;
			//std::cout << "edge dest:" << currentEdge_->Destination().x << "," << currentEdge_->Destination().y << std::endl;
			if (Math::Distance(GetPosition(), currentEdge_->Destination()) < 4)
			{
				//std::cout << "Next Edge!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
				pSteering_->ArriveOff();
				pSteering_->SeekOff();
				DEBUGUpdatePathDrawable();
				FollowPath();
			}
		}

		if (Math::LengthSq(velocity_) == 0.0f)
		{
			if(gameSprite_->GetCurrentAnim()->GetName() != "Idle")
				gameSprite_->SetAnimation("Idle", true, true);
		}
		else
		{
			if (gameSprite_->GetCurrentAnim()->GetName() != "Walk")
			{
				gameSprite_->SetAnimation("Walk", true, true);
			}
		}
}


/******************************* SetHoldingItem *********************************
 *
 ********************************************************************************/
void
TestEntity::SetHoldingItem(const ItemType& itemType)
{
	if (itemType == ItemType::SnackSandwich)
	{
		holdingItemID_ = stateGame_->GetEntityManager()->AddEntity("/Items/ItemSnackSandwich");
		stateGame_->GetEntityManager()->SetEntityPos(holdingItemID_, pos_);
	}
}

/******************************* GetSysStorage *********************************
 * Get System Storage
 ********************************************************************************/
SStorage*
TestEntity::GetSysStorage()
{
	return stateGame_->GetSystemManager()->GetSystem<SStorage>(SystemType::Storage);
}

// ---< Modify

// ---> TEMP
StateGame*
TestEntity::GetStateGame() const
{
	return stateGame_;
}
// ---< TEMP

/******************************* ConsumeHoldingItem *****************************
 *
 ********************************************************************************/
void
TestEntity::ConsumeHoldingItem()
{
	if (holdingItemID_ == -1) return;

	stateGame_->GetEntityManager()->RemoveEntity(holdingItemID_);
	holdingItemID_ = -1;
}

/******************************* SetHoldingItem *********************************
 *
 ********************************************************************************/
void
TestEntity::SetHoldingItem(const EntityID& entityID)
{
	holdingItemID_ = entityID;
}

/******************************* OnNotify ***************************************
 * OnBroadcast/OnMessage is for Messaging with 
 * On Notify Is For Messaging with Entities
 ********************************************************************************/
void
TestEntity::OnNotify(const Message& msg)
{
	EntityMessageType msgType = (EntityMessageType)msg.type;
	switch(msgType)
	{
		case EntityMessageType::NeedRefill:
		{
			if (profession_ == Profession::Janitor)
			{
				// goal
				GOAP::WorldState desiredState;
				desiredState.SetVariable("kIsFridgeFull", 1);
				GOAP::Goal* goal = new GOAP::Goal("RefillFridge", desiredState, 3000.0f);
				//goal->SetTargetTestEntity(broadcast.senderTestEntity);
				goalArbitrator_->AddGoal(goal);
			}
		}
	}
}

/******************************* IntersectsWithMouse ****************************
 *
 ********************************************************************************/
bool
TestEntity::IntersectsMouse(const vec2f& mouseWorldPos)
{
	//return gameSprite_->sprite_->getGlobalBounds().contains(mouseWorldPos);
	sf::FloatRect characterBound = gameSprite_->sprite_->getGlobalBounds();
	if(characterBound.left < mouseWorldPos.x &&
	   characterBound.left + characterBound.width > mouseWorldPos.x &&
	   characterBound.top < mouseWorldPos.y &&
	   characterBound.top + characterBound.height > mouseWorldPos.y)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/******************************* Tint ****************************************
 *
 *****************************************************************************/
void
TestEntity::Tint(sf::Color& tintColor)
{
	gameSprite_->sprite_->setColor(tintColor);
}


void
TestEntity::UnTint()
{
	gameSprite_->sprite_->setColor(sf::Color(255, 255, 255, 255));
}


/******************************* GetGlobalBounds ********************************
 *
 ********************************************************************************/
sf::FloatRect
TestEntity::GetGlobalBounds()
{
	return gameSprite_->sprite_->getGlobalBounds();
}
