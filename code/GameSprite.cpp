#include "GameSprite.h"
#include "TestEntity.h"
#include "TileMap/TileInfo.h"

#include "Window.h"

/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
GameSprite::GameSprite(TextureManager* textureManager)
	: textureManager_(textureManager)
	, animationCurrent_(nullptr)
	, spriteScale_(1.0f, 1.0f)
	, direction_(Direction::Down)
	, owner_(nullptr)
	, origin_(Origin::BottomLeft)
{
}


/********************************************************************************
 * Empty Destructor.
 ********************************************************************************/
GameSprite::~GameSprite()
{
	delete sprite_;
	ReleaseSheet();
}


/********************************************************************************
 * Release Sheet.
 ********************************************************************************/
void
GameSprite::ReleaseSheet()
{
	textureManager_->ReleaseResource(textureName_);
	animationCurrent_ = nullptr;
	while(animations_.begin() != animations_.end())
	{
		AnimBase* animation = animations_.begin()->second;
		delete animation;
		animations_.erase(animations_.begin());
	}
}
/********************************************************************************
 * Load Sheet New.
 ********************************************************************************/
bool
GameSprite::LoadSheet(const string& filename)
{
	sprite_ = new sf::Sprite();
	std::ifstream sheetFile;
	sheetFile.open(Utils::GetMediaDirectory() + "SpriteSheets/" + filename + ".sheet");
	if(!sheetFile.is_open())
	{
		std::cout << "Failed to load SpriteSheet: " << filename << std::endl;
		return false;
	}
	ReleaseSheet();
	string line;
	while(std::getline(sheetFile, line))
	{
		std::stringstream keystream(line);
		string type;
		keystream >> type;
		// Case1: TextureName
		if(type == "TextureName")
		{
			keystream >> textureName_;
			if(!textureManager_->RequireResource(textureName_))
			{
				std::cout << "!Failed to require resource: " << textureName_ << std::endl;
				continue;
			}
			sprite_->setTexture(*textureManager_->GetResource(textureName_));
		}
		// Case 2: Scale
		else if(type == "Scale")
		{
			keystream >> spriteScale_.x >> spriteScale_.y;
			sprite_->setScale(spriteScale_);
		}
		// Case3: Rect
		else if(type == "Rect")
		{
			string name;
			string directionString;
			int32 x, y, w, h;
			keystream >> name >> directionString >> x >> y >> w >> h;
			sf::IntRect rect(x, y, w, h);
			spriteRects_.emplace(std::make_pair(name, StringToDirection(directionString)), rect);
		}
		// case4: Default Rects
		else if( type == "DefaultRect")
		{
			string directionString;
			string spriteRectName;
			keystream >> directionString;
			keystream >> spriteRectName;
			Direction direction = StringToDirection(directionString);
			sf::IntRect defaultRect = GetSpriteRect(spriteRectName, direction);
			defaultSpriteRects_.emplace(direction, defaultRect);

			// ---> MODIFY
			SetDirection(direction);
			// ---> MODIFY
		}
		// Case5: Animation
		else if(type == "Animation")
		{
			string animationName;
			string directionString;
			int32 numFrames;
			keystream >> animationName;
			keystream >> directionString;
			keystream >> numFrames;
			Direction direction = StringToDirection(directionString);
			if(animations_.find(animationName) != animations_.end())
			{
				std::cout << "Error: Duplicate animation(" << animationName << ") in: " << filename << std::endl;
				continue;
			}
			AnimBase* animation = nullptr;
			animation = new AnimDirectional();

			keystream >> *animation;
			animation->SetSprite(this);
			animation->SetName(animationName);
			animation->Reset();
			animations_.emplace(animationName, animation);
		}
	}
	sheetFile.close();
	return true;
}
/********************************************************************************
 * Get Default Sprite Rect.
 ********************************************************************************/
sf::IntRect
GameSprite::GetDefaultRect(const Direction& direction)
{
	auto defaultRectsItr = defaultSpriteRects_.find(direction);
	if(defaultRectsItr == defaultSpriteRects_.end())
	{
		return sf::IntRect(0, 0, 0, 0);
	}
	sf::IntRect defaultSpriteRect = defaultRectsItr->second;
	return defaultSpriteRect;
}


/********************************************************************************
 * Direction string to Direction.
 ********************************************************************************/
Direction
GameSprite::StringToDirection(const string& directionString)
{
	Direction result = Direction::Down;
	if (directionString == "Up")
	{
		result = Direction::Up;
	}
	else if (directionString == "Down")
	{
		result = Direction::Down;
	}
	else if (directionString == "Left")
	{
		result = Direction::Left;
	}
	else if (directionString == "Right")
	{
		result = Direction::Right;
	}
	return result;
}


/********************************************************************************
 * Set Animation.
 ********************************************************************************/
bool
GameSprite::SetAnimation(const string& name, bool play, bool loop)
{
	if (owner_)
	{
		LOG_ENGINE_INFO(owner_->GetName() + " set anim: " + name);
	}
	auto animsItr = animations_.find(name);
	if(animsItr == animations_.end()){ return false; }
	AnimBase* animation = animsItr->second;
	if(animation == animationCurrent_){ return false; }
	if(animationCurrent_)
	{
		animationCurrent_->Stop();
	}
	animationCurrent_ = animation;
	animationCurrent_->SetLooping(loop);
	if(play)
	{
		animationCurrent_->Play();
	}
	animationCurrent_->CropSprite();
	return true;
}
/********************************************************************************
* Crop Sprite.
********************************************************************************/
void
GameSprite::CropSprite(const sf::IntRect& rect)
{
	currentRect_ = rect;
	sprite_->setTextureRect(rect);
	// TODO: find proper place to set spriteSize
	SetSpriteSize(vec2u(rect.width, rect.height));
}


/********************************************************************************
 * Update.
 ********************************************************************************/
void
GameSprite::Update(real32 dt)
{
	// FIXME: if(animationCurrent_)
	if (animationCurrent_)
	{
		animationCurrent_->Update(dt);
	}
}


/********************************************************************************
 * Draw.
 ********************************************************************************/
void
GameSprite::Draw(Window* window, int32 zOrder)
{
	if (textureName_.find("Door") != std::string::npos)
	{
		int32 a = 100;
	}
	//renderWindow->draw(*sprite_);
	window->Draw(*sprite_, zOrder);
}


/********************************************************************************
* Getters&Setters.
********************************************************************************/
///@{
const vec2u& GameSprite::GetSpriteSize() const{ return spriteSize_; }
const vec2f& GameSprite::GetSpritePosition() const
{
	return sprite_->getPosition();
}
void GameSprite::SetSpriteSize(const vec2u& size)
{
	spriteSize_ = size;
	// TODO: find a proper place to set sf::sprite origin
	//sprite_.setOrigin(0, spriteSize_.y);
	// TODO: this should only be set once.
	//std::cout << "Sprite Size is : " << spriteSize_.x << "," << spriteSize_.y << std::endl;
	//std::cout << "This should only be set once!!!!!!" << std::endl;
	switch (origin_)
	{
		case Origin::BottomLeft:
		{
			sprite_->setOrigin(0, spriteSize_.y);
		} break;

		case Origin::MidBottom:
		{
			sprite_->setOrigin(spriteSize_.x / 2, spriteSize_.y);
		} break;

		case Origin::AbsCenter:
		{
			sprite_->setOrigin(spriteSize_.x / 2, spriteSize_.y / 2);
		} break;

		case Origin::TopLeft:
		{
			sprite_->setOrigin(0, 0);
		} break;
	}
}
void GameSprite::SetPosition(const vec2f& pos)
{
	sprite_->setPosition(pos);
}
void GameSprite::SetDirection(const Direction& direction)
{
	direction_ = direction;
	if (animationCurrent_)
	{
		//std::cout << "Animation Should Change Direction!" << std::endl;
	}
	else
	{
		sf::IntRect defaultRect = GetDefaultRect(direction_);
		//sprite_.setTextureRect(defaultRect);
		if (owner_)
		{
			string logString = owner_->GetName();
			LOG_ENGINE_INFO(logString);
		}
		CropSprite(defaultRect);
	}
	// TODO: set direction should cropsrite
	//std::cout << "Set Direction Should CropSprite" << std::endl;
//	animationCurrent_->CropSprite();
}
Direction GameSprite::GetDirection()
{
	return direction_;
}

AnimBase* GameSprite::GetCurrentAnim()
{
	return animationCurrent_;
}
sf::IntRect
GameSprite::GetSpriteRect(const string& name, const Direction& direction)
{
	sf::IntRect result(0, 0, 0, 0);
	auto spriteRectsItr = spriteRects_.find(std::make_pair(name, direction));
	if(spriteRectsItr != spriteRects_.end())
	{
		result = spriteRectsItr->second;
	}
	return result;
}
///@}********************************************************************************/

/*********************************************************************************
 * Set Origin.
 *********************************************************************************/
// FIXME: SetSpriteSize and SetOrigin are doing some of the same things, SORT IT out!!
void
GameSprite::SetOrigin(const Origin& origin)
{
	//sprite_->setOrigin(vec2f(0.0f, TILE_SIZE));
	origin_ = origin;
}

/******************************* SetScale ***************************************
 *
 ********************************************************************************/
void
GameSprite::SetScale(const vec2f& scale)
{
	spriteScale_ = scale;
	sprite_->setScale(spriteScale_);
}

/******************************* StopAnimation **********************************
 *
 ********************************************************************************/
void
GameSprite::StopAnimation()
{
	if (animationCurrent_)
	{
		animationCurrent_->Stop();
		animationCurrent_ = nullptr;
	}
	CropSprite(GetDefaultRect(Direction::Down));
}


void
GameSprite::Tint(const sf::Color& color)
{
	sprite_->setColor(color);
}

/******************************* SetTexture *************************************
 *
 ********************************************************************************/
void
GameSprite::SetTexture(const string& textureName)
{
	//sf::Texture* newTexture = textureManager_->GetResource(textureName);
	textureManager_->RequireResource(textureName);
	sf::Texture* newTexture = textureManager_->GetResource(textureName);
	sprite_->setTexture(*newTexture);
	//sprite_->setTextureRect(currentRect_);
	CropSprite(currentRect_);
//	CropSprite(GetDefaultRect(Direction::Down));
}

