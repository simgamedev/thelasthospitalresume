#pragma once

enum class GameStateType;
class StateManager;

class StateDependent
{
public:
	StateDependent()
		: currentGameState_((GameStateType)0)
	{
	}
	virtual ~StateDependent() {}

	/// @todo mabybe change creategamestate to addgamestate??
	virtual void CreateGameState(const GameStateType& gameState) {}
	virtual void ChangeGameState(const GameStateType& gameState) = 0;
	virtual void RemoveGameState(const GameStateType& gameState) = 0;

	GameStateType GetCurrentGameState() const { return currentGameState_; }
protected:
	void SetState(const GameStateType& gameState)
	{
		currentGameState_ = gameState;
	}
	GameStateType currentGameState_;
};
