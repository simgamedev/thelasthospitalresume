#ifndef PATHEDGE_H
#define PATHEDGE_H
//--------------------------------------------------------------------------------
// Name: PathEdge.h
//
// Author: Mat Buckland (ai-junkie.com)
//
// Modified By: Larry Yu(www.simgendog.com)
//
// Desc: class to represent a path edge. This path can be used by a path
//	     planner in the creation of paths.(instead of using waypoints as path)
//--------------------------------------------------------------------------------
#include "../common.h"

class PathEdge
{
private:
	// positions of the source and destination nodes this edge connects
	vec2f m_vSource;
	vec2f m_vDestination;

	// the behavior associated with traversing this edge
	int32 m_iBehavior;
	int32 m_iDoorID;

public:
	PathEdge(vec2f source,
			 vec2f destination,
			 int32 behavior,
			 int32 doorID = -1)
		: m_vSource(source)
		, m_vDestination(destination)
		, m_iBehavior(behavior)
		, m_iDoorID(doorID)
	{}

	vec2f Destination() const { return m_vDestination; }
	void SetDestination(vec2f newDest) { m_vDestination = newDest; }  

	vec2f Source() const { return m_vSource; }
	void SetSource(vec2f newSource) { m_vSource = newSource; }

	int32 DoorID() const { return m_iDoorID; }
	int32 Behavior() const { return m_iBehavior; }
};

#endif
