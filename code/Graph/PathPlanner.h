#ifndef PATHPLANNER_H
#define PATHPLANNER_H
#pragma warning (disable:4786)
//--------------------------------------------------------------------------------
// Name: PathPlanner.h
// Larry Yu(www.simgendog.com)
//--------------------------------------------------------------------------------
#include <list>
//#include "GraphAlgorithms.h"
#include "TimeSlicedGraphAlgorithms.h"
#include "GraphAlgorithms.h"
#include "AStarHeuristicPolicies.h"
#include "SparseGraph.h"
#include "PathEdge.h"
#include "../GameWorld/GameWorld.h"

#include "../common.h"

class TestEntity;
class PathPlanner
{
private:
	// for legibility
	enum { no_closest_node_found = -1 };

public:
	// for ease of use typedef the graph edge/node typs used by the navgraph
	typedef GameWorld::NavGraph::EdgeType EdgeType;
	typedef GameWorld::NavGraph::NodeType NodeType;
	typedef std::list<PathEdge> Path;

private:
	// A pointer to the owner of this class
	// or a ID(ECS)
	//int32 m_iOwnerID;
	TestEntity* m_pOwner;

	// a reference to the navgraph
	const GameWorld::NavGraph& m_rNavGraph;

	// a pointer to an instance of the current graph search algorithm
	Graph_SearchTimeSliced<EdgeType>* m_pCurrentSearch;

	// this is the position the agent wishes to plan a path to reach
	vec2f m_vDestinationPos;


	// returns the index of the closest visible and unobstructed graph node to
	// the given position
	int32 GetClosestNodeToPosition(vec2f pos) const;

	// smooths a path by removing extraneous edges. (may not remove all extraneous edges)
	void SmoothPathEdgesQuick(Path& path);

	// called at the commencement of a new search request. It clears up the
	// appropriate lists and memory in preperation for a new search request
	void GetReadyForNewSearch();

public:
	~PathPlanner();

	PathPlanner(TestEntity* owner);

	// creates an instance of the Dijkstra's time-sliced search and registers it with
	// the path manager
	bool RequestPathToEntity(uint32 itemType);

	// creates an instance of the A* time-sliced search and registers
	// it with the path manager
	bool RequestPathToPosition(vec2f targetPos);

	// called by an angent after it has been notified that a search has terminated
	// successfully. The method extracts the path from m_pCurrentSearch, adds
	// additional edges appropirate to the search type and returns it as a list of
	// PathEdges.
	Path GetPath();

	// returns the cost to travel from the angent's current position to a specific
	// graph node. This method makes use of the precalculated lookup table
	// created by ???
	// real32 GetCostToNode(uint32 nodeIdx) const;

	// returns the cost to the closest instance of the GiverType. This method
	// also makes use of the pre-calculated lookup table. Returns -1 if no active
	// trigger found
	// real32 GetCostToClosestEntity(uint32 giverType) const;


	// the path manager calls this to iterate once through the serach cycle
	// of the currently assigned search algorithm. When a search is terminated
	// the method messages the owner with either the msg_NoPathAvailable or
	// msg_PathReady messages
	int32 CycleOnce() const;

	vec2f GetDestination() const { return m_vDestinationPos; }
	void SetDestination(vec2f newPos) { m_vDestinationPos = newPos; }

	// used to retrieve the position of a graph node from its index. (takes
	// into account the enumerations 'non_graph_source_node' and
	// 'non_graph_target_node'
	vec2f GetNodePosition(int32 idx) const;
};

#endif
