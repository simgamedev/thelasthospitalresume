#ifndef TIME_SLICED_GRAPHALGORITHMS_H
#define TIME_SLICED_GRAPHALGORITHMS_H
#pragma warning (disable:4786)
/********************************************************************************
 * Name: TimeSlicedGraphAlgorithms.h
 *
 ********************************************************************************/

#include <vector>
#include <list>
#include <queue>
#include <stack>


#include "SparseGraph.h"
#include "AStarHeuristicPolicies.h"
//#include "SearchTerminationPolicies.h"
#include "../common.h"
#include "PriorityQueue.h"
#include "PathEdge.h"

// these enums are used as return values from each search update method
enum { target_found, target_not_found, search_incomplete };



//------------------------------ Graph_SearchTimeSliced --------------------------
//
// base class to define a common interface for graph search algorithims
//
//--------------------------------------------------------------------------------
template <class edge_type>
class Graph_SearchTimeSliced
{
public:
	enum SearchType { AStar, Dijkstra };
private:
	SearchType m_SearchType;

public:
	Graph_SearchTimeSliced(SearchType type)
		: m_SearchType(type)
	{}

	virtual ~Graph_SearchTimeSliced() {}

	// when called, this method runs the algorithm through one search cycle. The
	// method returns an enumerated value(target_found, target_nod_found,
	// search_incomplete) indicating the status of the search
	virtual int32 CycleOnce() = 0;

	// returns the vector of edges that the algorithm has examined
	virtual std::vector<const edge_type*> GetSPT() const = 0;

	// returns the total cost to the target
	virtual real32 GetCostToTarget() const = 0;

	// returns a list of node indices that comprise the shrotest path
	// form the source to the target
	virtual std::list<int32> GetPathToTarget() const = 0;

	// returns the path as a list of PathEdges
	virtual std::list<PathEdge> GetPathAsPathEdges() const = 0;

	SearchType GetType() const { return m_SearchType; }
};



//------------------------------ Graph_SearchAStar_TS ------------------------------
//
//  an A* class that enables a search to be completed over multiple update-steps
//
//--------------------------------------------------------------------------------
template <class graph_type, class heuristic>
class Graph_SearchAStar_TS : public Graph_SearchTimeSliced<typename graph_type::EdgeType>
{
private:
	// create typedefs for the node and edge types used by the graph
	typedef typename graph_type::EdgeType Edge;
	typedef typename graph_type::NodeType Node;

private:
	const graph_type& m_rGraph;

	std::vector<real32> m_GCosts;
	std::vector<real32> m_FCosts;

	std::vector<const Edge*> m_ShortestPathTree;
	std::vector<const Edge*> m_SearchFrontier;

	int32 m_iSource;
	int32 m_iTarget;

	// create an indexed priority queue of nodes. The nodes with the
	// lowest overall F cost(G+H) are positioned at the front.
	IndexedPriorityQLow<real32>* m_pPQ;


public:
	Graph_SearchAStar_TS(const graph_type& G,
						 int32 source,
						 int32 target)
		: Graph_SearchTimeSliced<Edge>(AStar)
		, m_rGraph(G)
		, m_ShortestPathTree(G.NumNodes())
		, m_SearchFrontier(G.NumNodes())
		, m_GCosts(G.NumNodes(), 0.0)
		, m_FCosts(G.NumNodes(), 0.0)
		, m_iSource(source)
		, m_iTarget(target)
	{
		// create the PQ
		m_pPQ = new IndexedPriorityQLow<real32>(m_FCosts, m_rGraph.NumNodes());

		// put the source node on the queue
		m_pPQ->insert(m_iSource);
	}

	~Graph_SearchAStar_TS() { delete m_pPQ; }

	// when called, this method pops the next node off the PQ and examines all
	// its edges. The method returns an enumerated value(target_found,
	// (target_not_found, search_incomplete) indicating the status of the search
	int32 CycleOnce();

	// returns the vector of edges that the algorithm has examined
	std::vector<const Edge*> GetSPT() const { return m_ShortestPathTree; }

	// returns a vector of node indices that comprise the shrotest path
	// from the source to the target
	std::list<int32> GetPathToTarget() const;

	// returns the path as a list of PathEdges
	std::list<PathEdge> GetPathAsPathEdges() const;

	// returns the total cost to the target
	real32 GetCostToTarget() const { return m_GCosts[m_iTarget]; }
};


/******************************* CycleOnce **************************************
 *
 ********************************************************************************/
template <class graph_type, class heuristic>
int32 Graph_SearchAStar_TS<graph_type, heuristic>::CycleOnce()
{
	// if the PQ is empty the target has not been found
	if(m_pPQ->empty())
	{
		return target_not_found;
	}

	// get lowest cost node from the queue
	int32 nextClosestNode = m_pPQ->Pop();

	// put the node on the SPT
	m_ShortestPathTree[nextClosestNode] = m_SearchFrontier[nextClosestNode];

	// if the target has been found exit
	if(nextClosestNode == m_iTarget)
	{
		return target_found;
	}

	// now to test all the edges attached to this node
	graph_type::ConstEdgeIterator ConstEdgeItr(m_rGraph, nextClosestNode);
	for(const Edge* pE= ConstEdgeItr.begin();
		!ConstEdgeItr.end();
		pE = ConstEdgeItr.next())
	{
		// calculate the heuristic cost from this node to the target (H)
		real32 hCost = heuristic::Calculate(m_rGraph, m_iTarget, pE->To());

		// calculate the 'real' cost to this node from the source (G)
		real32 gCost = m_GCosts[nextClosestNode] + pE->Cost();

		// if the node has not been added to the frontier, add it and update
		// the G and F costs
		if(m_SearchFrontier[pE->To()] == NULL)
		{
			m_FCosts[pE->To()] = gCost + hCost;
			m_GCosts[pE->To()] = gCost;

			m_pPQ->insert(pE->To());

			m_SearchFrontier[pE->To()] = pE;
		}
		else if(gCost < m_GCosts[pE->To()] && (m_ShortestPathTree[pE->To()] == nullptr))
		{
			m_FCosts[pE->To()] = gCost + hCost;
			m_GCosts[pE->To()] = gCost;

			m_pPQ->ChangePriority(pE->To());

			m_SearchFrontier[pE->To()] = pE;
		}
	}
	return search_incomplete;
}

//********************************************************************************
template<class graph_type, class heuristic>
std::list<int32>
Graph_SearchAStar_TS<graph_type, heuristic>::GetPathToTarget() const
{
	std::list<int32> path;

	if(m_iTarget < 0) return path;

	int32 nd = m_iTarget;

	path.push_back(nd);

	while((nd != m_iSource) && (m_ShortestPathTree[nd] != nullptr))
	{
		nd = m_ShortestPathTree[nd]->From();

		path.push_front(nd);
	}

	return path;
}

//********************************************************************************
template<class graph_type, class heuristic>
std::list<PathEdge>
Graph_SearchAStar_TS<graph_type, heuristic>::GetPathAsPathEdges() const
{
	std::list<PathEdge> path;

	// just return an empty path if no target or no path found
	if(m_iTarget < 0) return path;

	int32 nd = m_iTarget;

	while((nd != m_iSource) && (m_ShortestPathTree[nd] != 0))
	{
		path.push_front(PathEdge(m_rGraph.GetNode(m_ShortestPathTree[nd]->From()).Pos(),
								 m_rGraph.GetNode(m_ShortestPathTree[nd]->To()).Pos(),
								 m_ShortestPathTree[nd]->Flags(),
								 m_ShortestPathTree[nd]->IDofIntersectingEntity()));

		nd = m_ShortestPathTree[nd]->From();
	}

	return path;
}

#endif

