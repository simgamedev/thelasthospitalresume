#include "PathPlanner.h"
#include "GraphAlgorithms.h"
#include "../TestEntity.h"
#include "PathManager.h"
#include "../StateGame.h"
//#include "SearchTerminationPolicies.h"
//#include "../Lua/TestEntityScriptor.h"
#include "../Messaging/CharMsgTypes.h"
#include "../Messaging/CharMsgDispatcher.h"
#include "NodeTypeEnumerations.h"

#include <cassert>
#include "../Time/Time.h"

/********************************************************************************
 * Constructor.
 ********************************************************************************/
PathPlanner::PathPlanner(TestEntity* owner)
	: m_pOwner(owner)
	, m_rNavGraph(*m_pOwner->GetGameWorld()->GetNavGraph())
	, m_pCurrentSearch(NULL)
{
}

/********************************************************************************
 * Destructor.
 ********************************************************************************/
PathPlanner::~PathPlanner()
{
	GetReadyForNewSearch();
}


/****************************** GetReadyForNewSearch ****************************
 * called by the search manager when a search has been terminated to free
 * up the memory used when an instance of the search was created
 ********************************************************************************/
void
PathPlanner::GetReadyForNewSearch()
{
	// unreigster any existing search with the path manager
	m_pOwner->GetGame()->GetPathManager()->UnRegister(this);

	// clean up memory used by any existing search
	delete m_pCurrentSearch;
	m_pCurrentSearch = nullptr;
}


/****************************** GetPath ****************************************
 * called by an agent after it has been notified that a search has terminated
 * successfully. The method extracts the path from m_pCurrentSearch, adds
 * additional edges appropritate to the search type and returns it as a list of
 * PathEdges.
 *******************************************************************************/
PathPlanner::Path
PathPlanner::GetPath()
{
	assert((m_pCurrentSearch != nullptr) &&
		   "<PathPlanner::m_pCurrentSearch> is nullptr");

	Path path = m_pCurrentSearch->GetPathAsPathEdges();

	int closest = GetClosestNodeToPosition(m_pOwner->GetPosition());

	path.push_front(PathEdge(m_pOwner->GetPosition(),
							 GetNodePosition(closest),
							 NavGraphEdge::normal));
	// if the bot requested a path to a location then an edge leading to the
	// destination must be added
	if(m_pCurrentSearch->GetType() == Graph_SearchTimeSliced<EdgeType>::AStar)
	{
		path.push_back(PathEdge(path.back().Destination(),
								m_vDestinationPos,
								NavGraphEdge::normal));
	}

	// smooth paths if required
	//SmoothPathEdgesQuick(path);
	/*
	if(UserOptions->m_bSmoothPathsQuick)
	{
		SmoothPathEdgesQuick(path);
	}

	if(UserOptions->m_bSmoothPathsPrecise)
	{
		SmoothPathEdgesPrecise(path);
	}
	*/

	return path;
}

/****************************** CycleOnce ****************************************
 * the path manager calls this to iterate once through the search cycle
 * of the currently assigned search algorithm.
 *********************************************************************************/
int32
PathPlanner::CycleOnce() const
{
	assert((m_pCurrentSearch != nullptr) &&
		   "<PathPlanner::CycleOnce>: m_pCurrentSearch is nullptr");

	int32 result = m_pCurrentSearch->CycleOnce();

	// let the agent know of the failure to find a path
	if(result == target_not_found)
	{
		GameTime zeroGameTime(0, 0, 0);
		CharMsgDispatcher::Instance()->DispatchMessage(zeroGameTime,
												       SENDER_ID_IRRELEVANT,
								                       m_pOwner->GetID(),
								                       CharMsgType::NoPathAvailable,
								                       nullptr);
	}

	// let the agent know a path has been found
	else if(result == target_found)
	{
		// if the search was for an entity type then the final node in the path will
		// represent a giver trigger? Consequently, it's worth passing the pointer
		// to the trigger in the extra info field of the message.(The pointer
		// will just be nullptr if no trigger)
		void* pTrigger = m_rNavGraph.GetNode(m_pCurrentSearch->GetPathToTarget().back()).ExtraInfo();

		GameTime zeroGameTime(0, 0, 0);
		CharMsgDispatcher::Instance()->DispatchMessage(zeroGameTime,
													   SENDER_ID_IRRELEVANT,
													   m_pOwner->GetID(),
													   CharMsgType::PathReady,
													   pTrigger);
	}

	return result;
}

/****************************** GetClosestNodeToPosition **************************
 * returns the index of the closest visible graph node to the given position
 **********************************************************************************/
int32
PathPlanner::GetClosestNodeToPosition(vec2f pos) const
{
	int32 closestNode = no_closest_node_found;

	int32 tileX = pos.x / TILE_SIZE;
	int32 tileY = pos.y / TILE_SIZE;
	int32 idx = tileY * m_pOwner->GetGameWorld()->GetMapSize().x + tileX;

	if (m_pOwner->GetGameWorld()->GetNavGraph()->GetNode(idx).Index() != invalid_node_index)
	{
		closestNode = idx;
	}
	/*
	real32 closestSoFar = MaxReal32;

	// when the cell space is queried this
	const real32 range = m_pOwner->GetGameWorld()->GetCellSpaceNeighborhoodRange();

	// calculate the graph nodes that are neighbours this position
	m_pOwner->GetGameWorld()->GetCellSpace()->CalculateNeighbors(pos, range);

	// iterate through the neighbors and sum up all the position vectors
	for(NodeType* pN = m_pOwner->GetGameWorld()->GetCellSpace()->begin();
		!m_pOwner->GetGameWorld()->GetCellSpace()->end();
		pN = m_pOwner->GetWorld()->GetCellSpace()->next())
	{
			// if the path between this node and pos is unobstructed calculate the
		    // distance
		if(m_pOwner->CanWalkBetween(pos, pN->Pos()))
		{
			real32 dist = Math::DistanceSq(pos, pN->Pos());

			// keep a record of the closest so far
			if(dist < closestSoFar)
			{
				closestSoFar = dist;
				closestNode = pN->Index();
			}
		}
	}
	*/

	return closestNode;
}

/****************************** RequestPathToPosition *****************************
 * Given a target, this method frist determines if nodes can be reached from
 * the angent's current position and the target position. If either end point is
 * unreachable the method returns false.
 * 
 * If nodes are reachable from both positions then an instance of the time-sliced
 * A* search is created and registered with the search manager.
 * the method then returns true.
 **********************************************************************************/
bool
PathPlanner::RequestPathToPosition(vec2f targetPos)
{
	#ifdef SHOW_NAVINFO
	Logger;
	#endif
	GetReadyForNewSearch();


	// make a note of the target position.
	m_vDestinationPos = targetPos;

	// if the target is walkable from the angent's position a path does not need to
	// be calculated, the angent can go straight to the position by ARRIVING at
	// the current waypoint
	if(m_pOwner->CanWalkTo(targetPos))
	{
		return false;
	}

	// find the closest visible node to the agent's position
	int32 closestNodeToAgent = GetClosestNodeToPosition(m_pOwner->GetPosition());

	// remove the destination node from the list and return false if no visible
	// node found. This will occur if the navgraph is badly designed or if the angent
	// has managed to get itself *inside* the geometry(surrounded by walls)
	if(closestNodeToAgent == no_closest_node_found)
	{
		#ifdef SHOW_NAVINFO
		Logger;
		#endif
		return false;
	}

	#ifdef SHOW_NAVINFO
	Logger;
	#endif

	// find the closest visible node to the target position
	int32 closestNodeToTarget = GetClosestNodeToPosition(targetPos);

	// return false if there is a problem locating a visible node from the target.
	// This sort of thing occurs much more frequently than the ablove. For
	// example, if the user clicks inside an area bounded by walls or inside an
	// object.
	if(closestNodeToTarget == no_closest_node_found)
	{
		#ifdef SHOW_NAVINFO
		Logger;
		#endif

		return false;
	}

	// create an instance of the distributed A* search class
	typedef Graph_SearchAStar_TS<GameWorld::NavGraph, Heuristic_Euclid> AStar;
	m_pCurrentSearch = new AStar(m_rNavGraph,
								 closestNodeToAgent,
								 closestNodeToTarget);

	// and register the search with the path manager
	m_pOwner->GetGame()->GetPathManager()->Register(this);

	return true;
}

/****************************** RequestPathToEntity *******************************
 * Given an item type, this method determines the closest reachable graph node
 * to the agent's positin and then creates an instance of the time-sliced
 * Dijkstra's algorithm, which it registers with the search manager
 ********************************************************************************/
bool
PathPlanner::RequestPathToEntity(uint32 entityType)
{
	/*
	// clear the waypoint list and delete any active search
	GetReadyForNewSearch();

	// find the closest visible node to the agent's position
	int32 closestNodeToAgent = GetClosestNodeToPosition(m_pOwner->WorldPos());

	// remove the destination node from the list and return false if no visible
	// node found. This will occur if the navgraph is badly designed or if the angent
	// has managed to get itself *inside* the geometry(surrounded by walls),
	// or an obstacle
	if(closestNodeToAgent == no_closest_node_found)
	{
		#ifdef SHOW_NAVINFO
		Logger::Engine::Warn("Shit!");
		#endif

		return false;
	}

	// create an instance of the search algorithm
	typedef FindActiveTrigger<Trigger<TestEntity>> t_con;
	typedef Graph_SearchDijkstra_TS<GameWorld::NavGraph, t_con> DjkSearch;

	m_pCurrentSearch = new DjkSearch(m_rNavGraph, ClosestNodeToAgent, entityType);
	*/
	return false;
}

/****************************** GetNodePosition *********************************
 * used to retrieve the position of a graph node from its index. (takes
 * into account the enumerations 'non_graph_srouce_node' and
 * 'non_graph_target_node'
/********************************************************************************/
vec2f
PathPlanner::GetNodePosition(int32 idx) const
{
	return m_rNavGraph.GetNode(idx).Pos();
}


/******************************* SmoothPathEdgesQuick ***************************
 * Smooths a path by removing extraneous edges.
 ********************************************************************************/
void
PathPlanner::SmoothPathEdgesQuick(Path& path)
{
	// create a couple of iterators and point them at the front of the path
	Path::iterator e1(path.begin());
	Path::iterator e2(path.begin());

	// increment e2 so it points to the edge following e1.
	++e2;

	// while e2 is not the last edge in the path, step through the edges
	// checking to see if the agent can move without obstruction from the source node
	// of e1 to the destination node of e2. If the agent can move between those 
	// positions then the two edges are replaced with a single edge.
	while (e2 != path.end())
	{
		// check for obstruction.
		if (e2->Behavior() == EdgeType::normal &&
			m_pOwner->CanWalkBetween(e1->Source(), e2->Destination()))
		{
			e1->SetDestination(e2->Destination());
			e2 = path.erase(e2);
		}
		else
		{
			e1 = e2;
			++e2;
		}
	}
}

