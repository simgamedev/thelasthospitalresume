#ifndef PATH_MANAGER_H
#define PATH_MANAGER_H
#pragma warning (disable:4786)

/********************************************************************************
 * Name: PathManager.h
 * Larry Yu(www.simgendog.com)
 ********************************************************************************/
#include <list>
#include <cassert>


#include "../common.h"
template <class path_planner>
class PathManager
{
private:
	// a container of all the active search requests
	std::list<path_planner*> m_SearchRequests;

	// this is the total number of search cycles allocated to the manager
	// Each update-step these are divieded equally amongst all registered path
	// requests
	uint32 m_iNumSearchCyclesPerUpdate;

public:

	PathManager(uint32 numCyclesPerUpdate) :
		m_iNumSearchCyclesPerUpdate(numCyclesPerUpdate) {}

	// every time this is called the total amount of search cycles available will
	// be shared out equally between all the active path requests. If a search
	// completes successfully or fails the method will notify the relevant angent
	void UpdateSearches();

	// a path planner should call this method to register a search with the
	// manager (The method checks to ensure the path planner is only registered
	// once)
	void Register(path_planner* pathPlanner);
	void UnRegister(path_planner* pathPlanner);


	// returns the amount of path requests currently active.
	int32 GetNumActiveSearches() const { return m_SearchRequests.size(); }
};

/****************************** UpdateSearches **********************************
 * This method iterates through all the active path planning requests
 * updating their searches until the user specified total number of serach
 * cycles has been satisfied.
 *
 * If a path is found or the search is unsuccessful the relevant agent is
 * notified accordingly by Telegram
 ********************************************************************************/
template <class path_planner>
inline void
PathManager<path_planner>::UpdateSearches()
{
	int numCyclesRemaining = m_iNumSearchCyclesPerUpdate;

	// iterate through the search requests until either all requests have been
	// fullfilled or there are no search cycles remaining for this update-step.
	std::list<path_planner*>::iterator pathPlannerItr = m_SearchRequests.begin();
	while(numCyclesRemaining-- && !m_SearchRequests.empty())
	{
		// make on search cycle of this path request
		int32 result = (*pathPlannerItr)->CycleOnce();

		// if the search has terminated remove from the list
		if((result == target_found) || (result == target_not_found))
		{
			// remove this path planner from the list
			pathPlannerItr = m_SearchRequests.erase(pathPlannerItr);
		}
		// move to the next
		else
		{
			++pathPlannerItr;
		}

		// the iterator may now be pointing to the end of the list. If this is so,
		// it must be reset to the beginning
		if(pathPlannerItr == m_SearchRequests.end())
		{
			pathPlannerItr = m_SearchRequests.begin();
		}
	}
}


/****************************** Register ****************************************
 * this is called to register a search with the manager.
 ********************************************************************************/
template <class path_planner>
inline void PathManager<path_planner>::Register(path_planner* pathPlanner)
{
	// make sure the angent does not already have a current search in the queue
	if(std::find(m_SearchRequests.begin(),
				 m_SearchRequests.end(),
				 pathPlanner) == m_SearchRequests.end())
	{
		// add to the list
		m_SearchRequests.push_back(pathPlanner);
	}
}


/****************************** UnRegister **************************************
 *
 ********************************************************************************/
template <class path_planner>
inline void PathManager<path_planner>::UnRegister(path_planner* pathPlanner)
{
	m_SearchRequests.remove(pathPlanner);
}
#endif
