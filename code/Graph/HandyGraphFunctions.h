#ifndef GRAPH_FUNCS
#define GRAPH_FUNCS
//-----------------------------------------------------------------------------
//
//  Name:   HandyGraphFunctions.h
//
//  Author: Mat Buckland (www.ai-junkie.com)
//
//  Desc:   As the name implies, some useful functions you can use with your
//          graphs. 

//          For the function templates, make sure your graph interface complies
//          with the SparseGraph class
//-----------------------------------------------------------------------------
#include <iostream>

//#include "misc/Stream_Utility_Functions.h"
#include "GraphAlgorithms.h"
//#include "Graph/AStarHeuristicPolicies.h"

//--------------------------- ValidNeighbour -----------------------------
//
//  returns true if x,y is a valid position in the map
//------------------------------------------------------------------------
bool ValidNeighbour(int x, int y, int NumCellsX, int NumCellsY)
{
	return !((x < 0) || (x >= NumCellsX) || (y < 0) || (y >= NumCellsY));
}

//------------ GraphHelper_AddAllNeighboursToGridNode ------------------
//
//  use to add he eight neighboring edges of a graph node that 
//  is positioned in a grid layout
//------------------------------------------------------------------------
template <class graph_type>
void GraphHelper_AddAllNeighboursToGridNode(graph_type& graph,
											int row,
											int col,
											int NumCellsX,
											int NumCellsY)
{
	for(int i = -1; i < 2; ++i)
	{
		for(int j = -1; j < 2; ++j)
		{
			int nodeX = col + j;
			int nodeY = row + i;

			// skip if equal to this node
			if( (i == 0) && (j == 0) ) continue;

			// check to see if this is a valid neighbour
			if(ValidNeighbour(nodeX, nodeY, NumCellsX, NumCellsY))
			{
				// calculate the distance to this node
				vec2f PosNode = graph.GetNode(row*NumCellsX+col).Pos();
				vec2f PosNeighbour = graph.GetNode(nodeY*NumCellsX+nodeX).Pos();

				double dist = PosNode.Distance(PosNeighbour);


				// this neighbour is okay so it can be added
				graph_type::EdgeType NewEdge(row*NumCellsX+col,
											 nodeY*NumCellsX+nodeX,
											 dist);
				graph.AddEdge(NewEdge);

				// if graph is not a diagraph then an edge need to be added going
				// in the other direction.
				if(!graph.isDigraph())
				{
					graph_type::EdgeType NewEdge(nodeY*NumCellsX+nodeX,
												 row*NumCellsX+col,
												 dist);
				}
			}
		}
	}
}

//--------------------------- GraphHelper_CreateGrid --------------------------
//
//  creates a graph based on a grid layout. This function requires the 
//  dimensions of the environment and the number of cells required horizontally
//  and vertically 
//-----------------------------------------------------------------------------
template <class graph_type>
void GraphHelper_CreateGrid(graph_type& graph,
							int cySize,
							int cxSize,
							int NumCellsY,
							int NumCellsX)
{
	// need some temporaries to help calculate each node center
	double CellWidth = (double)cySize / (double)NumCellsX;
	double CellHeight = (double)cxSize / (double)NumCellsY;

	double midX = CellWidth/2;
	double midY = CellHeight/2;

	// first create all the nodes
	for (int row = 0; row < NumCellsY; ++row)
	{
		for(int col = 0; col < NumCellsX; ++col)
		{
			graph.AddNode(NavGraphNode<>(graph.GetNextFreeNodeIndex(),
										 Vector2D(midX + (col * CellWidth),
												  midY + (row * CellHeight))));
		}
	}

	// now to calculate the edges. (A position in a 2d array [x][y] is the
	// same as [y*NumCellsX +x] in a 1d array). Each cell has up to eight
	// neighbours
	for(int row = 0; row < NumCellsY; ++row)
	{
		for (int col = 0; col < NumCellsX; ++col)
		{
			GraphHelper_AddAllNeighboursToGridNode(graph, row, col, NumCellsX, NumCellsY);
		}
	}
}

//--------------------------- WeightNavGraphNodeEdges -------------------------
//
//  Given a cost value and an index to a valid node this function examines 
//  all a node's edges, calculates their length, and multiplies
//  the value with the weight. Useful for setting terrain costs.
//------------------------------------------------------------------------
template <class graph_type>
void WeightNavGraphNodeEdges(graph_type& graph, int node, double weight)
{
	// make sure the node is present
	assert(node < graph.NumNodes());

	// set the cost for each edge
	graph_type::ConstEdgeIterator ConstEdgeItr(graph, node);
	for (const graph_type::EdgeType* pE = ConstEdgeItr.begin();
		 !ConstEdgeItr.end();
		 pE = ConstEdgeItr.next())
	{
		// calculate the distance between nodes
		double dist = Vec2DDistance(graph.GetNode(pE->From()).Pos(),
									graph.GetNode(pE->To()).Pos());

		// set the cost of this edge
		graph.SetEdgeCost(pE->From(), pE->To(), dist * weight);

		// if not a digraph, set the cost of the parallel edge to be the same
		if (!graph.isDigraph())
		{
			graph.SetEdgeCost(pE->To(), pE->From(), dist * weight);
		}

	}
}

#endif
