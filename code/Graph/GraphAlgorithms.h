#ifndef GRAPHALGORITHMS_H
#define GRAPHALGORITHMS_H
#pragma warning (disable:4786)

//------------------------------------------------------------------------
//
//  Name:   GraphSearches.h
//
//  Desc:   classes to implement graph algorithms, including DFS, BFS,
//          Dijkstra's, A*, Prims etc. (based upon the code created
//          by Robert Sedgewick in his book "Algorithms in C++")
//
//          Any graphs passed to these functions must conform to the
//          same interface used by the SparseGraph
//          
//  Author: Mat Buckland (fup@ai-junkie.com)
//  Modified By: Larry Yu (simgamedev.com)
//
//------------------------------------------------------------------------
#include <vector>
#include <list>
#include <queue>
#include <stack>

#include "SparseGraph.h"
#include "PriorityQueue.h"

struct SnapShot
{
	std::deque<std::pair<int, int>> edgeStack;
	std::vector<int> visited;
	std::vector<int> route; // route[3] = 2 means node 3's parent is node 2/
	std::pair<int, int> currentEdge;
	std::vector<std::string> descriptionStrs;
};
//----------------------------- Graph_SearchDFS -------------------------------
//
//  class to implement a depth first search. 
//-----------------------------------------------------------------------------
template<class graph_type>
class Graph_SearchDFS
{
private:
	// to aid legibility
	enum {visited, unvisited, no_parent_assigned};

	// create a typedef for the edge and node types used by the graph
	typedef typename graph_type::EdgeType Edge;
	typedef typename graph_type::NodeType Node;

private:

	// a referende to th graph to be searched
	const graph_type& m_Graph;

	// this records the indexes of all the nodes that are visited as the
	// serach progresses
	std::vector<int> m_Visited;

	// this holds the route taken to the target. Given a node index, the value
	// at that index is the node's parent. ie if the path to the target is
	// 3-8-27, then m_Route[8] will hold 3 and m_Route[27] will hold 8.
	std::vector<int> m_Route;

	// As the search progresses, this will hold all the edges the algorithm has
	// examined. THIS IS NOT NECESSARY FOR THE SEARCH, IT IS HERE PURELY
	// TO PROVIDE THE USER WITH SOME VISUAL FEEDBACK
	std::vector<const Edge*> m_SpanningTree;

	// the source and target node indices
	int m_iSource, m_iTarget;

	// true if a path to the target has been found
	bool m_bFound;


	// this method performs the DFS search
	bool Search();

public:

	Graph_SearchDFS(const graph_type& graph,
					int source,
					int target = -1)
		: m_Graph(graph)
		, m_iSource(source)
		, m_iTarget(target)
		, m_bFound(false)
		, m_Visited(m_Graph.NumNodes(), unvisited)
		//, m_Route(m_Graph.NumNodes(), no_parent_assigned)
		, m_Route(m_Graph.NumNodes(), -1)
	{
		m_bFound = Search();
	}


	// returns a vector containing pointers to all the edges the search has examined
	std::vector<const Edge*> GetSearchTree() const { return m_SpanningTree; }


	// returns true if the target node has been located
	bool Found() const { return m_bFound; }

	// returns a vector of node indexes that comprise the shortest path
	// from the source to the target
	std::list<int> GetPathToTarget() const;
public:		
	/*********************************************************************************
	 * Take snapshot for illustartion purposes.
	 *********************************************************************************/
	std::list<SnapShot> snapShots_;
	void
	TakeSnapShot(std::stack<const Edge*> stack, std::pair<int, int> currentEdge,
				 std::vector<std::string> descriptionStrs)
	{
		std::deque<std::pair<int, int>> edgeStack;
		while (stack.size() > 0)
		{
			 auto edge = stack.top();
			 int iFrom = edge->From();
			 int iTo = edge->To();
			 //edgeStackReversed.push(std::make_pair(iFrom, iTo));
			 edgeStack.push_front(std::make_pair(iFrom, iTo));
			 stack.pop();
		}
		 SnapShot snapShot;
		 snapShot.edgeStack = edgeStack;
		 snapShot.visited = m_Visited;
		 snapShot.route = m_Route;
		 snapShot.currentEdge = currentEdge;
		 snapShot.descriptionStrs = descriptionStrs;
		 snapShots_.push_back(snapShot);
	}
	std::list<SnapShot>
	GetSnapShots()
	{
		return snapShots_;
	}
};





//-----------------------------------------------------------------------------
template <class graph_type>
bool Graph_SearchDFS<graph_type>::Search()
{
	srand(time(NULL));
	// create a std stack of edges
	std::stack<const Edge*> stack;

	// create a dummy edge and put on the stack
	Edge Dummy(m_iSource, m_iSource, 0);

	stack.push(&Dummy);
	std::vector<std::string> descriptionStrs;
	descriptionStrs.push_back("Search begins by creating a dummy edge ---");
	descriptionStrs.push_back("one leading from the source node back to");
	descriptionStrs.push_back("the source node --- and putting it on the stack.");
	TakeSnapShot(stack, std::make_pair(-1, -1),
				 descriptionStrs);

	// while there are edges in the stack keep searching
	while(!stack.empty())
	{
		//TakeSnapShot(stack, std::make_pair(-1, -1));
		// grab the next edge
		const Edge* Next = stack.top();

		// remove the edge from the stack
		stack.pop();
		std::vector<std::string> descriptionStrs;
		descriptionStrs.push_back(std::string("Remove the topmost edge from the stack"));
		TakeSnapShot(stack, std::make_pair(Next->From(), Next->To()),
					 descriptionStrs);

		// make a note of the parent of the node this edge points to
		m_Route[Next->To()] = Next->From();
		descriptionStrs.clear();
		descriptionStrs.push_back("Note the parent of the edge's destination node");
		descriptionStrs.push_back("by inserting the parent's index in the vector");
		descriptionStrs.push_back("m_Routes, at the element refered to by the");
		descriptionStrs.push_back("destination node's index.");
		TakeSnapShot(stack, std::make_pair(Next->From(), Next->To()),
					 descriptionStrs);

		// put it on the tree. (making sure the dummy edge is not palced on the tree)
		if(Next != &Dummy)
		{
			m_SpanningTree.push_back(Next);
		}

		// and mark it visited
		m_Visited[Next->To()] = visited;
		descriptionStrs.clear();
		descriptionStrs.push_back("Mark the edge's destination node visited");
		descriptionStrs.push_back("by assigning the enumeration visited to");
		descriptionStrs.push_back("the relevant index in the m_Visited vector");
		TakeSnapShot(stack, std::make_pair(Next->From(), Next->To()),
					 descriptionStrs);

		// if the target has been found the method can return success
		if(Next->To() == m_iTarget)
		{
			descriptionStrs.clear();
			std::string line1 = "Test for termination. If the edge's destination node is";
			std::string line2 = "the target node, then the search can return success. ";
			std::string line3 = "CurrentEdge->To() : " + std::to_string(Next->To() + 1) + " == " + "m_iTarget: " + std::to_string(m_iTarget + 1);
			descriptionStrs.push_back(line1);
			descriptionStrs.push_back(line2);
			descriptionStrs.push_back(line3);
			TakeSnapShot(stack, std::make_pair(Next->From(), Next->To()),
						 descriptionStrs);
			return true;
		}
		else
		{
			descriptionStrs.clear();
			std::string line1 = "Test for termination. If the edge's destination node is";
			std::string line2 = "the target node, then the search can return success.";
			std::string line3 = "CurrentEdge->To() : " + std::to_string(Next->To() + 1) + " == " + "m_iTarget: " + std::to_string(m_iTarget + 1);
			descriptionStrs.push_back(line1);
			descriptionStrs.push_back(line2);
			descriptionStrs.push_back(line3);
			TakeSnapShot(stack, std::make_pair(Next->From(), Next->To()),
						 descriptionStrs);
		}

		// push the edges leading from the node this edge points to onto
		// the stack (provided the edge does not point to a previously
		// visited node) (RANDOMLY)
		// --->
		graph_type::ConstEdgeIterator ConstEdgeItr(m_Graph, Next->To());

		std::vector<const Edge*> tempStack;
		for(const Edge* pE = ConstEdgeItr.begin();
			!ConstEdgeItr.end();
			pE = ConstEdgeItr.next())
		{
			if(m_Visited[pE->To()] == unvisited)
			{
				//stack.push(pE);
				tempStack.push_back(pE);
			}
		}
		if (tempStack.size() > 0)
		{
			while (tempStack.size() > 0)
			{
				int randPos = rand() % tempStack.size();
				std::cout << "RandPos: " << randPos << std::endl;
				stack.push(tempStack[randPos]);
				tempStack.erase(tempStack.begin() + randPos);
			}
		}
		// ---<
		descriptionStrs.clear();
		descriptionStrs.push_back("If the edge's(the edges leading from current edge's");
		descriptionStrs.push_back("destination node) destination node is not the target,");
		descriptionStrs.push_back("then provided the node the edge points to has not"); 
		descriptionStrs.push_back("already been visited, all its(current edge's destination node)");
		descriptionStrs.push_back("adjacent edges are pushed onto the stack");
		TakeSnapShot(stack, std::make_pair(Next->From(), Next->To()),
					 descriptionStrs);
	}

	// no path to target
	return false;
}

//-----------------------------------------------------------------------------
template <class graph_type>
std::list<int> Graph_SearchDFS<graph_type>::GetPathToTarget() const
{
	std::list<int> path;

	// just return an empty path if no path to target found or if
	// no target has been specified
	if(!m_bFound || m_iTarget<0) return path;

	int nd = m_iTarget;

	path.push_front(nd);

	while(nd != m_iSource)
	{
		nd = m_Route[nd];

		path.push_front(nd);
	}

	return path;
}


//----------------------------- Graph_SearchBFS -------------------------------
//
//-----------------------------------------------------------------------------
template<class graph_type>
class Graph_SearchBFS
{
private:
	// to aid legibility
	enum {visited, unvisited, no_parent_assigned};

	// create a typedef for the edge type used by the graph
	typedef typename graph_type::EdgeType Edge;

private:

	// a reference to the graph to be searched
	const graph_type& m_Graph;

	// this records the indexes of all the nodes that are visited as the
	// search progresses
	std::vector<int> m_Visited;

	// this holds the route taken to the target. Given a node index, the value
	// at that index is the node's parent. ie if the path to the target is
	// 3-8-27, then m_Route[8] will hold 3 and m_Route[27] will hold 8.
	std::vector<int> m_Route;

	// the source and target node indices
	int m_iSource, m_iTarget;

	// true if a path to the target has been found
	bool m_bFound;

	// As the search progresses, this will hold all the edges the algorithm has
	// examined. THIS IS NOT NECESSARY FOR THE SEARCH, IT IS HERE PURELY
	// TO PROVIDE THE USER WITH SOME VISUAL FEEDBACK
	std::vector<const Edge*> m_SpanningTree;

	// the BFS algorithm is very similar to the DFS except that it uses a
	// FIFO queue instead of a stack.
	bool Search();

public:

	Graph_SearchBFS(const graph_type& graph,
					int source,
					int target = -1)
		: m_Graph(graph)
		, m_iSource(source)
		, m_iTarget(target)
		, m_bFound(false)
		, m_Visited(m_Graph.NumNodes(), unvisited)
		//, m_Route(m_Graph.NumNodes(), no_parent_assigned)
		, m_Route(m_Graph.NumNodes(), -1)
	{
		m_bFound = Search();
	}

	bool Found() const { return m_bFound; }

	// returns a vector containing pointers to all the edges the serach has examined.
	std::vector<const Edge*> GetSearchTree() const { return m_SpanningTree; }

	// returns a vector of node indexes that comprise the shrotest path
	// from the source to the target.
	std::list<int> GetPathToTarget() const;
public:
	/*********************************************************************************
	 * Take snapshot for illustartion purposes.
	 *********************************************************************************/
	std::list<SnapShot> snapShots_;
	void
	TakeSnapShot(std::queue<const Edge*> Q, std::pair<int, int> currentEdge,
				 std::vector<std::string> descriptionStrs)
	{
		std::deque<std::pair<int, int>> edgeStack;
		while (Q.size() > 0)
		{
			 //auto edge = stack.top();
			 auto edge = Q.front();
			 int iFrom = edge->From();
			 int iTo = edge->To();
			 //edgeStackReversed.push(std::make_pair(iFrom, iTo));
			 edgeStack.push_front(std::make_pair(iFrom, iTo));
			 Q.pop();
		}
		 SnapShot snapShot;
		 snapShot.edgeStack = edgeStack;
		 snapShot.visited = m_Visited;
		 snapShot.route = m_Route;
		 snapShot.currentEdge = currentEdge;
		 snapShot.descriptionStrs = descriptionStrs;
		 snapShots_.push_back(snapShot);
	}
	std::list<SnapShot>
	GetSnapShots()
	{
		return snapShots_;
	}
};


//-----------------------------------------------------------------------------
template<class graph_type>
bool Graph_SearchBFS<graph_type>::Search()
{
	// create a std queue of pointer's edges
	std::queue<const Edge*> Q;

	// create a dummy edge and put on the queue
	const Edge Dummy(m_iSource, m_iSource, 0);
	Q.push(&Dummy);

	// mark the source node as visited
	m_Visited[m_iSource] = visited;
	std::vector<std::string> descriptionStrs;
	descriptionStrs.push_back("The BFS commences like the DFS. First a");
	descriptionStrs.push_back("dummy edge is created and pushed onto the");
	descriptionStrs.push_back("queue. Then the source node is marked as");
	descriptionStrs.push_back("visited.");
	TakeSnapShot(Q, std::make_pair(-1, -1),
				 descriptionStrs);

	// while there are edges in the queu keep searching
	while(!Q.empty())
	{
		// grab the next edge
		const Edge* Next = Q.front();

		Q.pop();
		std::vector<std::string> descriptionStrs;
		descriptionStrs.push_back(std::string("Remove the oldest edge from the stack"));
		TakeSnapShot(Q, std::make_pair(Next->From(), Next->To()),
					 descriptionStrs);

		// mark the parent of this node
		m_Route[Next->To()] = Next->From();
		descriptionStrs.clear();
		descriptionStrs.push_back("Note the parent of the edge's destination node.");
		TakeSnapShot(Q, std::make_pair(Next->From(), Next->To()),
					 descriptionStrs);

		// exit if the target has been found
		if(Next->To() == m_iTarget)
		{
			descriptionStrs.clear();
			std::string line1 = "Test for termination. If the edge's destination node is";
			std::string line2 = "the target node, then the search can return success. ";
			std::string line3 = "CurrentEdge->To() : " + std::to_string(Next->To() + 1) + " == " + "m_iTarget: " + std::to_string(m_iTarget + 1);
			descriptionStrs.push_back(line1);
			descriptionStrs.push_back(line2);
			descriptionStrs.push_back(line3);
			TakeSnapShot(Q, std::make_pair(Next->From(), Next->To()),
						 descriptionStrs);
			return true;
		}
		else
		{
			descriptionStrs.clear();
			std::string line1 = "Test for termination. If the edge's destination node is";
			std::string line2 = "the target node, then the search can return success.";
			std::string line3 = "CurrentEdge->To() : " + std::to_string(Next->To() + 1) + " == " + "m_iTarget: " + std::to_string(m_iTarget + 1);
			descriptionStrs.push_back(line1);
			descriptionStrs.push_back(line2);
			descriptionStrs.push_back(line3);
			TakeSnapShot(Q, std::make_pair(Next->From(), Next->To()),
						 descriptionStrs);
		}

		// push the edges leading from the node at the end of the edge
		// onto the queue
		graph_type::ConstEdgeIterator ConstEdgeItr(m_Graph, Next->To());
		for(const Edge* pE=ConstEdgeItr.begin();
			!ConstEdgeItr.end();
			pE=ConstEdgeItr.next())
		{
			// if the node hasn't already been visited we can push the
			// edge on the queue
			if(m_Visited[pE->To()] == unvisited)
			{
				Q.push(pE);

				// what the ?
				m_Visited[pE->To()] = visited;
			}
		}
		descriptionStrs.clear();
		descriptionStrs.push_back("If the edge's(the edges leading from current edge's");
		descriptionStrs.push_back("destination node) destination node is not the target,");
		descriptionStrs.push_back("then provided the node the edge points to has not"); 
		descriptionStrs.push_back("already been visited, all its(current edge's destination node)");
		descriptionStrs.push_back("adjacent edges are pushed onto the queue");
		TakeSnapShot(Q, std::make_pair(Next->From(), Next->To()),
					 descriptionStrs);
	}

	return false;
}

//-----------------------------------------------------------------------------
template <class graph_type>
std::list<int> Graph_SearchBFS<graph_type>::GetPathToTarget()const
{
	std::list<int> path;

	// just return an empty path if no path to target found or if
	// no target has been specified
	if (!m_bFound || m_iTarget < 0) return path;

	int nd = m_iTarget;

	path.push_front(nd);

	while (nd != m_iSource)
	{
		nd = m_Route[nd];

		path.push_front(nd);
	}

	return path;
}

//----------------------------- Graph_SearchDijkstra -------------------------------
//
//-----------------------------------------------------------------------------
template <class graph_type>
class Graph_SearchDijkstra
{
private:

	// create typedefs for the node and edge types used by the graph
	typedef typename graph_type::EdgeType Edge;
	typedef typename graph_type::NodeType Node;
public:
	struct SnapShotDijkstra
	{
		std::vector<const Edge*> ShortestPathTree;
		std::vector<const Edge*> SearchFrontier;
		int CurrentNode;
		std::vector<std::string> descriptionStrs;
		std::vector<double> costToThisNode;
	};
	std::list<SnapShotDijkstra> snapShots_;
private:
	void TakeSnapShot(int CurrentNode, std::vector<std::string> descriptionStrs)
	{
		SnapShotDijkstra snapShot;
		snapShot.ShortestPathTree = m_ShortestPathTree;
		snapShot.SearchFrontier = m_SearchFrontier;
		snapShot.CurrentNode = CurrentNode;
		snapShot.descriptionStrs = descriptionStrs;
		snapShot.costToThisNode = m_CostToThisNode;
		snapShots_.push_back(snapShot);
	}
public:
	std::list<SnapShotDijkstra> GetSnapShots()
	{
		return snapShots_;
	}

private:

	const graph_type& m_Graph;

	// this vector contains the edges that comprise the shortest path tree -
	// a directed sub-tree of the graph that encapsulates the best paths from
	// every node on the SPT to the source node.
	std::vector<const Edge*> m_ShortestPathTree;

	// this is indexed into by node index and holds the total cost of the best
	// path found so far to the given node. For example, m_CostToThisNode[5]
	// will hold the total cost of all the edges that comprise the best path
	// to node 5 found so far in the search (if node 5 is present and has
	// been visited of course).
	std::vector<double> m_CostToThisNode;

	// this is an indexed (by node) vector of "parent" edges leading to nodes
	// connected to the SPT but that have not been added to the SPT yet.
	std::vector<const Edge*> m_SearchFrontier;

    int m_iSource;
	int m_iTarget;
	
	void Search();

public:

	Graph_SearchDijkstra(const graph_type& graph,
						 int source,
						 int target)
		: m_Graph(graph)
		, m_ShortestPathTree(graph.NumNodes())
		, m_SearchFrontier(graph.NumNodes())
		, m_CostToThisNode(graph.NumNodes())
		, m_iSource(source)
		, m_iTarget(target)
	{
		Search();
	}

	// returns the vector of edges defining the SPT. If a target is given
	// in the constructor, then this will be the SPT comprising all the nodes
	// examined before the target is found, else it will contain all the nodes
	// in the graph.
	std::vector<const Edge*> GetAllPaths() const;

	// returns a vector of node indexes comprising the shrotest path
	// from the source to the target. It calculates the path by working
	// backward through the SPT from the target node.
	std::list<int> GetPathToTarget() const;

	// returns the total cost to the target
	double GetCostToTarget(unsigned int nd) const { return m_CostToThisNode[nd]; }
};


template<class graph_type>
void Graph_SearchDijkstra<graph_type>::Search()
{
	// create an indexed priority queue that sorts smallest to largest
	// (front to back). Note that the maximum number of elements the iPQ
	// may contain is NumNodes(). This is because no node can be represented
	// on the queue more than once.
	IndexedPriorityQLow<double> pq(m_CostToThisNode, m_Graph.NumNodes());

	// put the source node on the queue
	pq.insert(m_iSource);
	std::vector<std::string> descriptionStrs;

	int counter = 0;
	// while the queue is not empty
	while(!pq.empty())
	{
		// get the lowest cost node from the queue. Don't froget, the return value
		// is a *node index*, not the node itself. This node is the node not already
		// on the SPT that is closest to the source node
		int NextClosestNode = pq.Pop();

		if (NextClosestNode == 2)
		{
			int a = 100;
		}
		// move this edge from the search frontier to the shortest path tree
		m_ShortestPathTree[NextClosestNode] = m_SearchFrontier[NextClosestNode];
		if (counter != 0)
		{
			descriptionStrs.clear();
			descriptionStrs.push_back("The algorithm then examines the destination");
			descriptionStrs.push_back("nodes of the edges on the frontier and add");
			descriptionStrs.push_back("the one closest to the source to the SPT.");
			TakeSnapShot(NextClosestNode, descriptionStrs);
		}


		// if the target has been found exit
		if (NextClosestNode == m_iTarget)
		{
			if (counter != 0)
			{
				descriptionStrs.clear();
				descriptionStrs.push_back("Test for target");
				TakeSnapShot(NextClosestNode, descriptionStrs);
			}
			return;
		}
		else
		{
			if (counter != 0)
			{
				descriptionStrs.clear();
				descriptionStrs.push_back("Test for target");
				TakeSnapShot(NextClosestNode, descriptionStrs);
			}
		}

		// now to relax the edges. For each edge connected to the next closest node
		graph_type::ConstEdgeIterator ConstEdgeItr(m_Graph, NextClosestNode);
		for(const Edge* pE=ConstEdgeItr.begin();
			!ConstEdgeItr.end();
			pE=ConstEdgeItr.next())
		{
			// the total cost of the node this edge points to is the cost to the
			// current node plus the cost of the edge connecting them.
			double NewCost = m_CostToThisNode[NextClosestNode] + pE->Cost();

			// if this edge has never been on the frontier make a note of the cost
			// to reach the node it points to, then add the edge to the frontier
			// and the destination node to the PQ.
			if(m_SearchFrontier[pE->To()] == 0)
			{
				m_CostToThisNode[pE->To()] = NewCost;

				pq.insert(pE->To());

				m_SearchFrontier[pE->To()] = pE;

				if (counter != 0)
				{
					descriptionStrs.clear();
					descriptionStrs.push_back("Current edge: " +
											  std::to_string(pE->From()) + "," +
											  std::to_string(pE->To()));
					descriptionStrs.push_back("If this edge has never been on the frontier");
					descriptionStrs.push_back("make a note of the cost to reach the node");
					descriptionStrs.push_back("it points to, then add the edge to the frontier.");
					TakeSnapShot(NextClosestNode, descriptionStrs);
				}
			}

			// else test to see if the cost to reach the destination node via the
			// current node is cheaper than the cheapest cost found so far. If
			// this path is cheaper we assign the new cost to the destination
			// node, update its entry in the PQ to reflect the change, and add the
			// edge to the frontier
			else if((NewCost < m_CostToThisNode[pE->To()]) &&
					(m_ShortestPathTree[pE->To()] == 0))
			{
				m_CostToThisNode[pE->To()] = NewCost;

				// because the cost is less than it was previoulsy, the PQ must be
				// resorted to account for this.
				pq.ChangePriority(pE->To());

				m_SearchFrontier[pE->To()] = pE;

				if (counter != 0)
				{
					descriptionStrs.clear();
					descriptionStrs.push_back("Current edge: " +
											  std::to_string(pE->From()) + "," +
											  std::to_string(pE->To()));
					descriptionStrs.push_back("The cost to reach the destination node(pointed");
					descriptionStrs.push_back("by the current examining edge) via the current");
					descriptionStrs.push_back("node is cheaper than the cheapest");
					descriptionStrs.push_back("cost found so far. ");
					TakeSnapShot(NextClosestNode, descriptionStrs);
				}
			}
		}
		if (counter == 0)
		{
			descriptionStrs.clear();
			descriptionStrs.push_back("First, the source node is added to the");
			descriptionStrs.push_back("SPT and the edges leaving it are placed");
			descriptionStrs.push_back("on the search frontier.");
			TakeSnapShot(m_iSource, descriptionStrs);
		}
		counter++;
	}
}

//-----------------------------------------------------------------------------
template <class graph_type>
std::list<int> Graph_SearchDijkstra<graph_type>::GetPathToTarget() const
{
	std::list<int> path;

	// just return an empty path if no target or no path found
	if(m_iTarget < 0) return path;

	int nd = m_iTarget;

	path.push_front(nd);

	while((nd != m_iSource) && (m_ShortestPathTree[nd] != 0))
	{
		nd = m_ShortestPathTree[nd]->From();

		path.push_front(nd);
	}

	return path;
}


//----------------------------- Graph_SearchAStar -------------------------------
//
//-----------------------------------------------------------------------------
template<class graph_type, class heuristic>
class Graph_SearchAStar
{
private:

	// create a typedef for the edge type used by the graph
	typedef typename graph_type::EdgeType Edge;

private:
	const graph_type& m_Graph;

	// indexed into by node. Contains the "real" cumulative cost to that node
	std::vector<double> m_GCosts;

	// indexed into by node. Contains the cost from adding m_GCosts[n] to
	// the heuristic cost from n to the target node. This is the vector the
	// iPQ indexes into.
	std::vector<double> m_FCosts;

	std::vector<const Edge*> m_ShortestPathTree;
	std::vector<const Edge*> m_SearchFrontier;

	int m_iSource;
	int m_iTarget;


	// the A* search algorithm
	void Search();

public:

	Graph_SearchAStar(const graph_type& graph,
					  int source,
					  int target)
		: m_Graph(graph)
		, m_ShortestPathTree(graph.NumNodes())
		, m_SearchFrontier(graph.NumNodes())
		, m_GCosts(graph.NumNodes(), 0.0)
		, m_FCosts(graph.NumNodes(), 0.0)
		, m_iSource(source)
		, m_iTarget(target)
	{
		Search();
	}


	// returns the vector of edges that the algorithm has examined
	std::vector<const Edge*> GetSPT() const;

	// returns a vector of node indexes that comprise the shrotest path
	// from the source to the target
	std::list<int> GetPathToTarget() const;

	// returns the total cost to the target
	double GetCostToTarget() const { return m_GCosts[m_iTarget]; }
};


//-----------------------------------------------------------------------------
template<class graph_type, class heuristic>
void Graph_SearchAStar<graph_type, heuristic>::Search()
{
	// create an indexed priority queue of nodes. The queue will give priority
	// to nodes with low F costs. (F=G+H)
	IndexedPriorityQLow<double> pq(m_FCosts, m_Graph.NumNodes());

	// put the source node on the queue
	pq.insert(m_iSource);

	// while the queue is not empty
	while(!pq.empty())
	{
		// get lowest cost node from the queue
		int NextClosestNode = pq.Pop();

		// move this node from the frontier to the spanning tree
		m_ShortestPathTree[NextClosestNode] = m_SearchFrontier[NextClosestNode];

		// if the target has been found exit
		if(NextClosestNode == m_iTarget) return;


		// now to test all the edges attached to this node
		graph_type::ConstEdgeIterator ConstEdgeItr(m_Graph, NextClosestNode);

		for(const Edge* pE=ConstEdgeItr.begin();
			!ConstEdgeItr.end();
			pE=ConstEdgeItr.next())
		{
			// calculate the heuristic cost from this node to the target (H)
			double HCost = heuristic::Calculate(m_Graph, m_iTarget, pE->To());

			// calculate the "real" cost to this node from the source (G)
			double GCost = m_GCosts[NextClosestNode] + pE->Cost();

			// if the node has not been added to the frontier, add it and update
			// the G and F costs
			if(m_SearchFrontier[pE->To()] == NULL)
			{
				m_FCosts[pE->To()] = GCost + HCost;
				m_GCosts[pE->To()] = GCost;

				pq.insert(pE->To());

				m_SearchFrontier[pE->To()] = pE;
			}

			// if this node is already on the frontier but the cost to get here this
			// way is cheaper than has been found previously, update the node costs
			// and frontier accordingly.
			else if((GCost < m_GCosts[pE->To()] &&
					 (m_ShortestPathTree[pE->To()] == NULL)))
			{
				m_FCosts[pE->To()] = GCost + HCost;
				m_GCosts[pE->To()] = GCost;

				pq.ChangePriority(pE->To());

				m_SearchFrontier[pE->To()] = pE;
			}
		}
	}
}

//-----------------------------------------------------------------------------
template <class graph_type, class heuristic>
std::list<int> Graph_SearchAStar<graph_type, heuristic>::GetPathToTarget() const
{
	std::list<int> path;

	// just return an empty path if no target or no path found
	if (m_iTarget < 0) return path;

	int nd = m_iTarget;

	path.push_front(nd);

	while ((nd != m_iSource) && (m_ShortestPathTree[nd] != 0))
	{
		nd = m_ShortestPathTree[nd]->From();

		path.push_front(nd);
	}

	return path;
}


#endif
