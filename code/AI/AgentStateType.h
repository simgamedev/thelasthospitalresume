#pragma once
enum class AgentStateType
{
	Idle,
		MoveTo,
		Perform,
		Crazy,
		Dead,
};
