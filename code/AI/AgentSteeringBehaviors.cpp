#include "AgentSteeringBehaviors.h"
#include "../TestEntity.h"


#include <cassert>

/****************************** Ctor ****************************************
 *
 ****************************************************************************/
Steering::Steering(TestEntity* owner)
	: m_pOwner(owner)
	, m_iFlags(0)
	, m_fWeightSeparation(10.0f)
	, m_fWeightWallAvoidance(10.0f)
	, m_fWeightWander(1.0f)
	, m_fWeightSeek(1.0f)
	, m_fWeightFlee(1.0f)
	, m_fWeightArrive(1.0f)
	, m_fViewDistance(15.0)
	, m_fWallDetectionFeelerLength(25.0*2)
	, m_Feelers(3)
	, m_Deceleration(fast)
	, m_pTargetAgent1(nullptr)
	, m_pTargetAgent2(nullptr)
	, m_fWanderDistance(WanderDist)
	, m_fWanderJitter(WanderJitterPerSec)
	, m_fWanderRadius(WanderRad)
	, m_bCellSpaceOn(false)
	, m_SummingMethod(prioritized)
{
	// stuff for the wander behavior
	real32 theta = Math::RandFloat() * Math::TwoPi;

	// create a vector to a target position on the wander circle
	m_vWanderTarget = vec2f(m_fWanderRadius * Math::Cos(theta),
							m_fWanderRadius * Math::Sin(theta));
}

/****************************** Destructor ******************************
 *
 ************************************************************************/
Steering::~Steering() {}


/****************************** Calculate ***************************************
 *
 * calculates the accumulated steering force according to the method set
 * in m_SummingMethod
 ********************************************************************************/
vec2f
Steering::Calculate()
{
	// reset the steering force
	Math::Zero(m_vSteeringForce);

	// tag neighbors if any of the following 3 group behaviors are switched on
	if(On(separation))
	{
		//m_pOwner->GetGameWorld()->TagTestEntitiesWithinViewRange(m_pOwner, m_fViewDistance);
	}

	m_vSteeringForce = CalculatePrioritized();

	return m_vSteeringForce;
}


/****************************** ForwardComponent ********************************
 *
 * returns the forward component of the steering force
 ********************************************************************************/
real32
Steering::ForwardComponent()
{
	return Math::Dot(m_pOwner->GetHeading(), m_vSteeringForce);
}

/****************************** SideComponent ***********************************
 *
 * returns the side component of the steering force
 ********************************************************************************/
real32
Steering::SideComponent()
{
	return Math::Dot(m_pOwner->GetSide(), m_vSteeringForce);
}

/****************************** AccumulateForce *********************************
 *
 ********************************************************************************/
bool
Steering::AccumulateForce(vec2f &runningTot, vec2f forceToAdd)
{
	// calculate how much steering force the agent has used so far
	real32 magnitudeSoFar = Math::Length(runningTot);

	// calculate how much steering force remains to be used by this vehicle
	real32 magnitudeRemaining = m_pOwner->GetMaxForce() - magnitudeSoFar;

	// return false if there is no more force left to use
	if(magnitudeRemaining <= 0.0f) return false;

	// calculate the magnitude of the force we want to add
	real32 magnitudeToAdd = Math::Length(forceToAdd);

	// if the magnitude of the sum  of the forceToAdd and the running total
	// does not exceed the maximum force available to this agent, just
	// add together. Otherwise add as much of the ForceToAdd vector is
	// possible without going over the max.
	if(magnitudeToAdd < magnitudeRemaining)
	{
		runningTot += forceToAdd;
	}
	else
	{
		magnitudeToAdd = magnitudeRemaining;

		runningTot += (Math::Normalize(forceToAdd) * magnitudeToAdd);
	}

	return true;
}

/****************************** CalculatePrioritized ****************************
 *
 * this method calls each active steering behavior in order of priority
 * and accumulates their forces until the max steering force magnitude
 * is reached, at which time the function returns the steering force
 * accumulated to that point
 ********************************************************************************/
vec2f
Steering::CalculatePrioritized()
{
	vec2f force;
	if(On(wall_avoidance))
	{
		/*
		force = WallAvoidance(m_pOwner->GetWorld()->GetWalls()) * m_dWeightWallAvoidance;

		if(!AccumulateForce(m_vSteeringForce, force)) return m_vSteeringForce;
		*/
	}

	if(On(separation))
	{
		/*
		force = Separation(m_pOwner->GetWorld->GetAllTestEntities()) * m_dWeightSeparation;

		if(!AccumulateForce(m_vSteeringForce, force)) return m_vSteeringForce;
		*/
	}

	if(On(seek))
	{
		force = Seek(m_vTarget) * m_fWeightSeek;

		if(!AccumulateForce(m_vSteeringForce, force)) return m_vSteeringForce;
	}

	if (On(flee))
	{
		force = Flee(m_vTarget) * m_fWeightFlee;
		if (!AccumulateForce(m_vSteeringForce, force)) return m_vSteeringForce;
	}

	if(On(arrive))
	{
		force = Arrive(m_vTarget, m_Deceleration) * m_fWeightArrive;

		if(!AccumulateForce(m_vSteeringForce, force)) return m_vSteeringForce;
	}

	if (On(wander))
	{
		force = Wander() * m_fWeightWander;
		if (!AccumulateForce(m_vSteeringForce, force)) return m_vSteeringForce;
	}

	return m_vSteeringForce;
}

/****************************** Seek ****************************************
 *
 ****************************************************************************/
vec2f
Steering::Seek(const vec2f& target)
{
	vec2f desiredVelocity = Math::Normalize(target - m_pOwner->GetPosition()) * m_pOwner->GetMaxSpeed();

	//return (desiredVelocity - m_pOwner->GetVelocity());
	return desiredVelocity;
}

/****************************** Seek ****************************************
 *
 ****************************************************************************/
vec2f
Steering::Flee(const vec2f& target)
{
	// only flee if the target is within 'panic distance'. Work in distance
	// squared space.
	const real32 PanicDistanceSq = 100.0 * 100.0f;
	if (Math::DistanceSq(m_pOwner->GetPosition(), target) > PanicDistanceSq)
	{
		return vec2f(0.0f, 0.0f);
	}

	vec2f desiredVelocity = Math::Normalize(m_pOwner->GetPosition() - target) * m_pOwner->GetMaxSpeed();

	return (desiredVelocity - m_pOwner->GetVelocity());
}


/****************************** Arrive **************************************
 * 
 ****************************************************************************/
vec2f
Steering::Arrive(const vec2f& target,
				 const Deceleration deceleration)
{
	const real32 slowingRaidus = 100.0f;
	vec2f toTarget = target - m_pOwner->GetPosition();

	//calculate the distance to the target
	real32 dist = Math::Length(toTarget);

	std::cout << "dist: " << dist << std::endl;
	std::cout << "currentVelocity: " << m_pOwner->GetVelocity().x << "," << m_pOwner->GetVelocity().y << std::endl;
	// TODO: margin
	//std::cout << "Dist: " << dist << std::endl;
	if(dist > 0)
	{
		// because Deceleration is enumerated as an int, this value is required
		// to provide find tweaking of the deceleration..
		const real32 DecelerationTweaker = 0.3;

		// calculate the speed required to reach the target given the desired
		// deceleration
		real32 speed = dist / ((real32)deceleration * DecelerationTweaker);

		std::cout << "speed:" << speed << std::endl;
		std::cout << "toTarget: " << toTarget.x << "," << toTarget.y << std::endl;
		// make sure the velocity does not exceed the max
		speed = std::min(speed, m_pOwner->GetMaxSpeed());

		// from here proceed just like Seek except we don't need to normalize
		// the toTarget vector because we have already gone to the trouble
		// of calculating its length: dist.
		vec2f desiredVelocity = toTarget * speed / dist;

		//return (desiredVelocity - m_pOwner->GetVelocity());
		return desiredVelocity;
	}

	return vec2f(0, 0);
}

/****************************** Wander ****************************************
 ******************************************************************************/
vec2f
Steering::Wander()
{
	/*
	// first, add a small random vector to the target's position
	m_vWanderTarget += vec2f(Math::RandomClamped() * m_fWanderJitter,
							 Math::RandomClamped() * m_fWanderJitter);

	// reproject this new vector back on the a unit circle
	Math::Normalize(m_vWanderTarget);

	// increase the length of the vector to the same as the raidus
	// of the wander circle
	m_vWanderTarget *= m_fWanderRadius;

	// move the target into a position WanderDist in front of the agent
	vec2f target = m_vWanderTarget + vec2f(m_fWanderDistance, 0);

	// project the target into world space
	target = Math::PointToWorldSpace(target,
									 m_pOwner->GetHeading(),
									 m_pOwner->GetSide(),
									 m_pOwner->GetPosition());

	// and steer towards it
	return target - m_pOwner->GetPosition();
	*/
	return vec2f(0.0f, 0.0f);
}

/****************************** WallAvoidance ******************************
 *
 ***************************************************************************/
vec2f
Steering::WallAvoidance(const std::vector<Wall2D*> &walls)
{
	/*
	// the feelers are contained in std::vector, m_Feelers
	CreateFeelers();

	real32 DistToThisIP = 0.0;
	real32 DistToClosestIP = Math::MaxReal32;

	// this will hold an index into the std::vector of walls
	int32 closestWall = -1;

	vec2f steeringForce;
	vec2f point; // used for stroing temporary info;
	vec2f closestPoint; // holds the closest intersection point

	// examine each feeler in turn
	// TODO: is unsigned int uint32?
	for(unsigned int flr = 0; flr < m_Feelers.size(); ++flr)
	{
		// run through eacl wall checking for any interseciton points
		for(unsigned int w=0; w<walls.size(); ++w)
		{
			if(Math::LineIntersection2D(m_pOwner->Pos(),
										m_Feelers[flr],
										walls[w]->From(),
										walls[w]->To(),
										DistToThisIP,
										point))
			{
				// is this the closest found so far? If so keep a record
				if(distToThisIP < distToClosestIP)
				{
					distToClosestIP = distToThisIP;

					closestWall = w;

					closestPoint = point;
				}
			}
		}  // next wall

		// if an intersection point has been detected, calculate a force that will
		// direct the agent away
		if(closestWall >= 0)
		{
			// calculate by what distance the projected position of the agent
			// will overshoot the wall
			vec2f overShoot = m_Feelers[flr] - closetPoint;

			// create a force in the direction of the wall normal, with
			// a magnitude of the overshoot
			steeringForce = walls[closestWall]->Normal() * Math::Length(overShoot);
		}
	} // next feeler
	return steeringForce;
	*/
	return vec2f(0, 0);
}


/****************************** CreateFeelers ***********************************
 *
 * Creates the antenna utilized by WallAvoidance
 ********************************************************************************/
void
Steering::CreateFeelers()
{
	/*
	// feeler pointing straight in front
	m_Feelers[0] = m_pOwner->Pos() + m_dWallDetectionFeelerLength * m_pOwner->Heading() * m_pOwner->Speed();

	// feeler to left
	vec2f temp = m_pOwner->Heading();
	Math::RotateAroundOrigin(temp, Math::HalfPi * 3.5);
	m_Feelers[1] = m_pOwner->Pos() + m_dWallDetectionFeelerLength / 2.0 * temp;

	// feeler to right
	temp = m_pOwner->Heading();
	Math::RotateAroundOrigin(temp, Math::HalfPPi * 0.5);
	m_Feelers[2] = m_pOwner->Pos() + m_dWallDetectionFeelerLength / 2.0 * temp;
	*/
}


/****************************** Separation **************************************
 *
 ********************************************************************************/
vec2f
Steering::Separation(const std::list<TestEntity*>& neighbors)
{
	vec2f steeringForce;
	/*
	std::list<TestEntity*>::const_iterator it = neighbors.begin();
	for(it; it != neighbors.end(); ++it)
	{
		// make sure this agent isn't included in the calculations and that
		// the agent being examined is close enough. ***also make sure it doesn't
		// include the evade target ***
		if((*it != m_pOwner) && (*it)->IsTagged() &&
		   (*it != m_pTargetAgent1))
		{
			vec2f toItself = m_pOwner->Pos() - (*it)->Pos();

			// scale the force inversely proportinal to the agents distance
			// from its neighbor.
			steeringForce += Math::Normalize(toItself)/Math::Length(toItself);
		}
	}
	return steeringForce;
	*/
	return vec2f(0, 0);
}
