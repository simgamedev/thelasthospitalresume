#include "FSM.h"
#include "FSMState.h"
#include "../TestEntity.h"
#include "AgentStateIdle.h"
#include "AgentStateMoveTo.h"
#include "AgentStatePerform.h"


/****************************** Constructor *************************************
 *
 ********************************************************************************/
FSM::FSM(TestEntity* testEntity)
    : owner_(testEntity)
{
}


/****************************** Destructor *************************************
 *
 *******************************************************************************/
FSM::~FSM()
{
	// clean
}

/****************************** Update ****************************************
 *
 ******************************************************************************/
void FSM::Update(const sf::Time& time)
{
	if(states_.size() > 0)
	{
        states_.top()->Update(time);
	}
}

/****************************** PushState ****************************************
 *
 *********************************************************************************/
void FSM::PushState(AgentStateType stateType)
{
    switch (stateType)
    {
        case AgentStateType::Idle:
        {
            states_.push(new AgentStateIdle(owner_));
        } break;

        case AgentStateType::Perform:
        {
            states_.push(new AgentStatePerform(owner_));
        } break;

        case AgentStateType::MoveTo:
        {
            states_.push(new AgentStateMoveTo(owner_));
        } break;
    }
}

/****************************** PopState ****************************************
 *
 ********************************************************************************/
void FSM::PopState()
{
    delete states_.top();
	states_.pop();
}

FSMState*
FSM::GetCurrentState()
{
    return states_.top();
}

