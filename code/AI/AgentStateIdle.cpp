#include "AgentStateIdle.h"
#include "../TestEntity.h"


AgentStateIdle::AgentStateIdle(TestEntity* owner)
	: FSMState(owner, AgentStateType::Idle)
{
}

void AgentStateIdle::Update(const sf::Time& time)
{
}
