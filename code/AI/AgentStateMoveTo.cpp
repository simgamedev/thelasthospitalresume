#include "AgentStateMoveTo.h"
#include "../TestEntity.h"


AgentStateMoveTo::AgentStateMoveTo(TestEntity* owner)
	: FSMState(owner, AgentStateType::MoveTo)
{
}

void
AgentStateMoveTo::Update(const sf::Time& time)
{
}
