#pragma once
#include "../common.h"
#include <stack>
#include "AgentStateType.h"
#include <SFML/System/Time.hpp>

class TestEntity;
class FSMState;

class FSM
{
public:
	FSM(TestEntity* testEntity);
	~FSM();
	void Update(const sf::Time& time);
	void PushState(AgentStateType stateType);
	void PopState();
	FSMState* GetCurrentState();
private:
	std::stack<FSMState*> states_;
	TestEntity* owner_;
};

