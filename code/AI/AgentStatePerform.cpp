#include "AgentStatePerform.h"
#include "../TestEntity.h"

AgentStatePerform::AgentStatePerform(TestEntity* owner)
	: FSMState(owner, AgentStateType::Perform)
{
}

void
AgentStatePerform::Update(const sf::Time& time)
{
}
