#ifndef AGENT_STEERINGBEHAVIORS_H
#define AGENT_STEERINGBEHAVIORS_H
#pragma warning (disable:4786)
//--------------------------------------------------------------------------------
//
// Name: AgentSteeringBehavior.h
//--------------------------------------------------------------------------------

#include <vector>
#include "../common.h"
#include "../Graph/PathEdge.h"


class TestEntity;


class Wall2D;
//------------------------------ Constants ------------------------------
// the radius of the constraining circle for the wander behavior
const real32 WanderRad = 1.2f;
// distance the wander circle is projected in front of the agent
const real32 WanderDist = 2.0f;
// the maximum amount of displacement along the circle each frame
const real32 WanderJitterPerSec = 40.0;



//--------------------------------------------------------------------------------
//
// Class: AgentSteeringBehavior
//--------------------------------------------------------------------------------
class Steering
{
public:
	enum summing_method { weighted_average, prioritized, dithered };
private:
	enum behavior_type
	{
		none = 0x00000,
			seek = 0x00002,
			flee = 0x00004,
			arrive = 0x00008,
			wander = 0x00010,
		separation = 0x00040,
		follow_path = 0x00400,
		wall_avoidance = 0x00200,
	};

private:
	// a pointer to the owner of this instance
	TestEntity* m_pOwner;

	// a pointer to the world
	//GameWorld* m_pGameWorld;

	// the steering force created by the combined effect of all
	// the selected behaviors
	vec2f m_vSteeringForce;

	// these can be used to keep track of friends, pursuers, or prey
	TestEntity* m_pTargetAgent1;
	TestEntity* m_pTargetAgent2;

	// the current target pos
	vec2f m_vTarget;

	// a vertex buffer to contain the fellers rqd for wall avoidance
	std::vector<vec2f> m_Feelers;

	//  the length of the "feeler's" used to in wall detection
	real32 m_fWallDetectionFeelerLength;

	// the current position on the wnader circle the agent is
	// attempting to steer towards
	vec2f m_vWanderTarget;

	// explained above
	real32 m_fWanderJitter;
	real32 m_fWanderRadius;
	real32 m_fWanderDistance;

	// multipliers. These can be adjusted to effect strength of the
	// appropriate behavior.
	real32 m_fWeightSeparation;
	real32 m_fWeightWander;
	real32 m_fWeightWallAvoidance;
	real32 m_fWeightSeek;
	real32 m_fWeightFlee;
	real32 m_fWeightArrive;


	// how far the agent can 'see'
	real32 m_fViewDistance;

	// binary flags to indicate whether or not a behavior should be active
	int32 m_iFlags;


	// Arrive makes use of these to determine how quickly an Agent
	// should decelerate to its target
	enum Deceleration{ slow = 3, normal = 1, fast = 1};

	// default
	Deceleration m_Deceleration;

	// is cell space partitioning to be used or not?
	bool m_bCellSpaceOn;

	// what type of method is used to sum any active behavior
	summing_method m_SummingMethod;

	// this function tests if a specific bit of m_iFlags is set
	bool On(behavior_type bt) { return (m_iFlags & bt) == bt; }

	bool AccumulateForce(vec2f& sf, vec2f forceToAdd);

	// creates the antenna utilized by the wall avoidance behavior
	void CreateFeelers();


	//---> Behaviors

	// this behavior moves the agent towards a target position
	vec2f Seek(const vec2f& target);
	vec2f Flee(const vec2f& target);
	// this behavior is similar to seek but it attempts to arrive
	// at the target with a zero velocity
	vec2f Arrive(const vec2f& target,
				 const Deceleration deceleration);
	// this behavior makes the agent wander about randomly
	vec2f Wander();

	vec2f WallAvoidance(const std::vector<Wall2D*> &walls);

	vec2f Separation(const std::list<TestEntity*> &agents);

	//---<
	// calculates and sums the steering forces from	any active behaviors
	vec2f CalculatePrioritized();

public:
	Steering(TestEntity* owner);

	virtual ~Steering();

	// calculates
	vec2f Calculate();

	// forward component
	real32 ForwardComponent();
	real32 SideComponent();

	void SetTarget(vec2f target) { m_vTarget = target; }
	vec2f Target() const { return m_vTarget; }

	void SetTargetAgent1(TestEntity* agent) { m_pTargetAgent1 = agent; }
	void SetTargetAgent2(TestEntity* agent) { m_pTargetAgent2 = agent; }

	vec2f Force() const { return m_vSteeringForce; }

	void SetSummingMethod(summing_method sm) { m_SummingMethod = sm; }

	void SeekOn() { m_iFlags |= seek; }
	void FleeOn() { m_iFlags |= flee; }
	void ArriveOn() { m_iFlags |= arrive; }
	void WanderOn() { m_iFlags |= wander; }
	void SeparationOn() { m_iFlags = separation; }
	void WallAvoidanceOn() { m_iFlags = wall_avoidance; }

	void SeekOff() { if(On(seek)) m_iFlags ^= seek;	}
	void FleeOff() { if (On(flee)) m_iFlags ^= flee; }
	void ArriveOff() { if(On(arrive)) m_iFlags ^= arrive; }
	void WanderOff() { if(On(wander)) m_iFlags ^= wander; }
	void SeparationOff() { if(On(separation)) m_iFlags ^= separation; }
	void WallAvoidanceOff() {if(On(wall_avoidance)) m_iFlags ^= wall_avoidance; }

	bool IsSeekOn() { return On(seek); }
	bool IsArriveOn() { return On(arrive); }
	bool IsWanderOn() { return On(wander); }
	bool IsSeparationOn() { return On(separation); }
	bool IsWallAvoidanceOn() { return On(wall_avoidance); }


	const std::vector<vec2f>& GetFeelers() const { return m_Feelers; }

	real32 WanderJitter() const { return m_fWanderJitter; }
	real32 WanderDistance() const { return m_fWanderDistance; }
	real32 WanderRadius() const { return m_fWanderRadius; }
	real32 SeparationWeight() const { return m_fWeightSeparation; }

};

#endif
