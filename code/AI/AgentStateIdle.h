#pragma once
#include "FSMState.h"

class AgentStateIdle : public FSMState
{
public:
	AgentStateIdle(TestEntity* owner);
	void Update(const sf::Time& time);
};
