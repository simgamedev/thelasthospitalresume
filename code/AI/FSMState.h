#pragma once
#include <SFML/System/Time.hpp>
#include "AgentStateType.h"
class TestEntity;
class FSMState
{
public:
	FSMState(TestEntity* owner, AgentStateType stateType)
	{
		owner_ = owner;
		stateType_ = stateType;
	}
	AgentStateType GetStateType()
	{
		return stateType_;
	}
	virtual void Update(const sf::Time& time) = 0;
private:
	AgentStateType stateType_;
	TestEntity* owner_;
};
