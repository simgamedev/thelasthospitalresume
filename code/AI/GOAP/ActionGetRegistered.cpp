#include "ActionGetRegistered.h"
#include "../../TestEntity.h"

GOAP::ActionGetRegistered::ActionGetRegistered(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "GetRegistered", 1, 5.0f, targetPos)
{
	//SetPrecondition("kIsHungry", 1);
	SetEffect("kIsRegistered", 1);
	SetAnimName("Idle");
	isOnePerson_ = true;
}

GOAP::ActionGetRegistered::~ActionGetRegistered()
{
}


bool
GOAP::ActionGetRegistered::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionGetRegistered::CheckProceduralPreconditions()
{
	return true;
}


bool
GOAP::ActionGetRegistered::EndPerform()
{
	pOwner_->agentBeliefs_.SetVariable("kIsRegistered", 1);
	pOwner_->GetGameWorld()->ReleaseAction(*this);
	pOwner_->GetGameWorld()->RegisterPatient(pOwner_);

	return true;
}
