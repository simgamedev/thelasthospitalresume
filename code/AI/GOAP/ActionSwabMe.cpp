#include "ActionSwabMe.h"
#include "../../TestEntity.h"

GOAP::ActionSwabMe::ActionSwabMe(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "SwabMe", 1, 5.0f, targetPos)
{
	SetPrecondition("kHasSwab", 1);
	SetEffect("kIsSwabbed", 1);
	SetAnimName("Fiddleing");

	// it's a one time action, once it's been executed, it should no longer exist.
	SetIsOneTime(true);
}

GOAP::ActionSwabMe::~ActionSwabMe()
{
}


bool
GOAP::ActionSwabMe::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionSwabMe::CheckProceduralPreconditions()
{
	/*
	if (providerTestEntity_ == nullptr)
		return false;
		*/
	return true;
}


bool
GOAP::ActionSwabMe::EndPerform()
{
	//pOwner_->agentBeliefs_.SetVariable("kIsSwabbed", 1);
	providerTestEntity_->agentBeliefs_.SetVariable("kIsSwabbed", 1);
	pOwner_->agentBeliefs_.SetVariable("kHasSawb", 0);
	// TODO:
	//pOwner_->ConsumeHoldingItem();
	string logString("I'm " + GetName() + " and my provider should be swabbed");
	LOG_GAME_ERROR(logString.c_str());
	return true;
}
