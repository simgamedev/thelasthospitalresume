#include "ActionSweepMe.h"
#include "../../TestEntity.h"
#include "../../Messaging/CharMsgDispatcher.h"
// TODO: rename to EntityMsgDispatcher.h
//#include "../../ECS/MessageHandler.h"
#include "../../ECS/SystemManager.h"

GOAP::ActionSweepMe::ActionSweepMe(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "SweepMe", 1, 5.0f, targetPos)
{
	//SetPrecondition("kIsHungry", 1);
	SetEffect("kIsFloorClean", 1);
	SetAnimName("Sweeping");
}

GOAP::ActionSweepMe::~ActionSweepMe()
{
}


bool
GOAP::ActionSweepMe::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionSweepMe::CheckProceduralPreconditions()
{
	SStorage* sysStorage = pOwner_->GetSysStorage();
	if (sysStorage->GetNumItem(providerEntityID_, ItemType::SnackSandwich) > 0)
	{
		return true;
	}
	else
	{
		// 
		// TODO: should change dispatchmesage name to a difference one
		GameTime zeroTime(0, 0, 0);
		CharMsgDispatcher::Instance()->DispatchMessage(zeroTime,
													   SENDER_ID_IRRELEVANT,
													   pOwner_->GetID(),
													   CharMsgType::NoFoodForYou,
													   nullptr);
		// then the character's mood should be down a bit
		return false;
	}
}


bool
GOAP::ActionSweepMe::EndPerform()
{
	// TODO: refactor this to pOwner_->SetAgentBeliefs("kHasSnack", 1);
	pOwner_->agentBeliefs_.SetVariable("kIsFloorClean", 1);
	return true;
}
