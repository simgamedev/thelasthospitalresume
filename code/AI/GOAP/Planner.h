#pragma once

#include "Action.h"
#include "Node.h"
#include "WorldState.h"

#include <ostream>
#include <unordered_map>
#include <vector>

namespace GOAP
{
	class Planner
	{
	private:
		std::vector<Node> openList_; // A* open list
		std::vector<Node> closedList_; // A* closed list
		int32 CalculateHeuristic(const WorldState& start, const WorldState& goal) const;
		Node& PopAndClose();
		// TODO: change the functio to take a node for easy understanding??
		bool MemberOfClosedList(const WorldState& ws) const;
		std::vector<Node>::iterator MemberOfOpenList(const WorldState& ws);
		void AddToOpenList(Node&& n);
	public:
		Planner();
		std::vector<Action> Plan(const WorldState& start,
								 const WorldState& goal,
								 const std::vector<Action>& actions);
	};
}