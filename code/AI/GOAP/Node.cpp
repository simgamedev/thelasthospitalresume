#include "Node.h"

int32 GOAP::Node::idCounter_ = 0;

GOAP::Node::Node()
	: g_(0)
	, h_(0)
{
	id_ = ++idCounter_;
}

GOAP::Node::Node(const WorldState state, int32 g, int32 h, int32 parentID, const Action* action)
	: ws_(state)
	, g_(g)
	, h_(h)
	, parentID_(parentID)
	, action_(action)
{
	id_ = ++idCounter_;
}

bool
GOAP::operator<(const Node& lhs, const Node& rhs)
{
	return lhs.F() < rhs.F();
}
