#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionLieInMe : public Action
	{
	public:
		ActionLieInMe(TestEntity* owner, vec2f targetPos);
		~ActionLieInMe();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
