#include "ActionLieInMe.h"
#include "../../TestEntity.h"

GOAP::ActionLieInMe::ActionLieInMe(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "LieInMe", 1, 1500.0f, targetPos)
{
	//SetPrecondition("kIsHungry", 1);
	SetEffect("kIsInBed", 1);
	SetAnimName("LieInBed");
}

GOAP::ActionLieInMe::~ActionLieInMe()
{
}


bool
GOAP::ActionLieInMe::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionLieInMe::CheckProceduralPreconditions()
{
	return true;
}


bool
GOAP::ActionLieInMe::EndPerform()
{
	int32 a = 100;
	int32 b = 100;
	//pOwner_->agentBeliefs_.SetVariable("kHasSnack", 1);
	//string logString("I'm " + GetName() + " and my owner should have a snack");
	//LOG_GAME_ERROR(logString.c_str());
	return true;
}
