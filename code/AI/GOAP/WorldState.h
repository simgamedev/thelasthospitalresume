#pragma once

#include "../../common.h"
#include <map>
#include <ostream>

namespace GOAP
{
	class WorldState
	{
	// TODO: change to private:
	public:
		real32 priority_;
		string name_;
		std::map<string, int32> vars_;
		//std::map<std::pair<string, int32>, int32> vars_;
		//std::multimap<std::pair<string, int32>, int32> vars_;
		//std::map<std::pair<string, string>, int32> vars_;
	public:
		WorldState(const string& name = "");
		//void SetVariable(const std::pair<string, int32> key, int32 value);
		//int32 GetVariable(const std::pair<string, int32> key);
		void SetVariable(const string key, int32 value);
		int32 GetVariable(const string key);
		bool MeetsGoal(const WorldState& goalState) const;
		int32 DistanceTo(const WorldState& goalState) const;
		string GetName() const;
		bool operator==(const WorldState& other) const;
		friend std::ostream& operator<<(std::ostream& out, const WorldState& ws);
		
		void SetPriority(real32 priority);
	};

	inline std::ostream& operator<<(std::ostream& out, const WorldState& ws)
	{
		out << "WorldState { ";
		for (const auto& kv : ws.vars_)
		{
			/*
			out << kv.first.first << ","
				<< kv.first.second << ": "
				<< kv.second << std::endl;
				*/
			out << kv.first << ","
				<< kv.second << std::endl;
		}
		out << "}" << std::endl;
		return out;
	}
}
