#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionSwabMe : public Action
	{
	public:
		ActionSwabMe(TestEntity* owner, vec2f targetPos);
		~ActionSwabMe();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
