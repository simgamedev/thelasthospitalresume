#pragma once
#include "Action.h"

namespace GOAP
{
	class ActionGoHome : public Action
	{
	public:
		ActionGoHome(TestEntity* owner, vec2f targetPos);
		~ActionGoHome();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
