#pragma once

#include "../../common.h"
#include "WorldState.h"
class TestEntity;
namespace GOAP
{
	class Goal
	{
public:
		Goal(string name)
			: name_(name)
		{
		}

		Goal(string name, const WorldState& desiredState, real32 insistence = 1.0f)
			: name_(name)
			, insistence_(insistence)
			, targetTestEntity_(nullptr)
		{
			// copy
			desiredState_ = desiredState;
		}

		WorldState GetDesiredState()
		{
			return desiredState_;
		}
		
		string GetName() const
		{
			return name_;
		}

		real32 GetInsistence() const
		{
			return insistence_;
		}

		void SetTargetTestEntity(TestEntity* testEntity)
		{
			targetTestEntity_ = testEntity;
		}

		TestEntity* GetTargetTestEntity() const
		{
			return targetTestEntity_;
		}
private:
		string name_;
		real32 insistence_;
		WorldState desiredState_;
		TestEntity* targetTestEntity_;
	};
}
