#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionTurnOnVent : public Action
	{
	public:
		ActionTurnOnVent(TestEntity* owner, vec2f targetPos);
		~ActionTurnOnVent();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
