#pragma once

#include "Action.h"

namespace GOAP
{
	class Goal;
	class ActionXRayScan : public Action
	{
	public:
		// TODO: reanme owner -> Character* executer
		ActionXRayScan(TestEntity* owner, vec2f targetPos);
		~ActionXRayScan();

		bool StartPerform() override;
		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
