#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionTakeSupplyBoxSnack : public Action
	{
	public:
		ActionTakeSupplyBoxSnack(TestEntity* owner, vec2f targetPos);
		~ActionTakeSupplyBoxSnack();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
