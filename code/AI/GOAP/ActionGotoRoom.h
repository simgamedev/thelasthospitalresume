#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionGotoRoom : public Action
	{
	public:
		ActionGotoRoom(TestEntity* owner, Room* room);
		~ActionGotoRoom();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	public:
	};
}
