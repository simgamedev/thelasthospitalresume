#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionDrugMe : public Action
	{
	public:
		ActionDrugMe(TestEntity* owner, vec2f targetPos);
		~ActionDrugMe();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
