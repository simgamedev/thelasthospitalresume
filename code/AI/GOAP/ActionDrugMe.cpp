#include "ActionDrugMe.h"
#include "../../TestEntity.h"

GOAP::ActionDrugMe::ActionDrugMe(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "DrugMe", 1, 2.0f, targetPos)
{
	SetPrecondition("kHasDrug", 1);
	SetEffect("kIsDrugged", 1);
	SetAnimName("EatingLying");

	SetIsOneTime(true);
}

GOAP::ActionDrugMe::~ActionDrugMe()
{
}


bool
GOAP::ActionDrugMe::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionDrugMe::CheckProceduralPreconditions()
{
	/*
	if (providerTestEntity_ == nullptr)
		return false;
		*/
	return true;
}


bool
GOAP::ActionDrugMe::EndPerform()
{
	pOwner_->agentBeliefs_.SetVariable("kHasDrug", 0);
	//pOwner_->agentBeliefs_.SetVariable("kIsSwabbed", 1);
	//providerTestEntity_->agentBeliefs_.SetVariable("kIsSwabbed", 1);
	providerTestEntity_->numDrugsTook_++;
	string logString("I'm " + GetName() + " and my provider should be swabbed");
	LOG_GAME_ERROR(logString.c_str());
	return true;
}
