#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionRefillMe : public Action
	{
	public:
		ActionRefillMe(TestEntity* owner, vec2f targetPos);
		~ActionRefillMe();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
