#include "ActionGotoPos.h"
#include "../../TestEntity.h"

GOAP::ActionGotoPos::ActionGotoPos(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "GotoPos", 1, 1.0f, targetPos)
{
	//SetPrecondition("kIsHungry", 1);
	//SetEffect("kIsInBed", 1);
	//SetAnimName("LieInBed");
}

GOAP::ActionGotoPos::~ActionGotoPos()
{
}


bool
GOAP::ActionGotoPos::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionGotoPos::CheckProceduralPreconditions()
{
	return true;
}


bool
GOAP::ActionGotoPos::EndPerform()
{
	int32 a = 100;
	int32 b = 100;
	//pOwner_->agentBeliefs_.SetVariable("kHasSnack", 1);
	//string logString("I'm " + GetName() + " and my owner should have a snack");
	//LOG_GAME_ERROR(logString.c_str());
	return true;
}
