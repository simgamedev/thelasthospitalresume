#include "ActionTakeSnack.h"
#include "../../TestEntity.h"
#include "../../Messaging/CharMsgDispatcher.h"
// TODO: rename to EntityMsgDispatcher.h
//#include "../../ECS/MessageHandler.h"
#include "../../ECS/SystemManager.h"

GOAP::ActionTakeSnack::ActionTakeSnack(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "TakeSnack", 1, 5.0f, targetPos)
{
	SetPrecondition("kIsHungry", 1);
	SetEffect("kHasSnack", 1);
	SetAnimName("TakeSnack");
}

GOAP::ActionTakeSnack::~ActionTakeSnack()
{
}


bool
GOAP::ActionTakeSnack::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionTakeSnack::CheckProceduralPreconditions()
{
	// TODO: figure out a proper way to get ecs system storage
	SStorage* sysStorage = pOwner_->GetSysStorage();
	if (sysStorage->GetNumItem(providerEntityID_, ItemType::SnackSandwich) > 0)
	{
		return true;
	}
	else
	{
		// 
		// TODO: should change dispatchmesage name to a difference one
		GameTime zeroTime(0, 0, 0);
		CharMsgDispatcher::Instance()->DispatchMessage(zeroTime,
													   SENDER_ID_IRRELEVANT,
													   pOwner_->GetID(),
													   CharMsgType::NoFoodForYou,
													   nullptr);
		// then the character's mood should be down a bit
		return false;
	}
}


bool
GOAP::ActionTakeSnack::EndPerform()
{
	if(!CheckProceduralPreconditions())
	{
		return false;
	}	
	// TODO: refactor this to pOwner_->SetAgentBeliefs("kHasSnack", 1);
	pOwner_->agentBeliefs_.SetVariable("kHasSnack", 1);
	//providerEntityID_
	pOwner_->SetHoldingItem(ItemType::SnackSandwich);

	Message msg((MessageType)EntityMessageType::ItemTook);
	msg.receiver = providerEntityID_;
	EntityMessageHandler::Instance()->Dispatch(msg);

	return true;
}
