#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionTakeSnack : public Action
	{
	public:
		ActionTakeSnack(TestEntity* owner, vec2f targetPos);
		~ActionTakeSnack();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
