#include "ActionTurnOnXRay.h"
#include "../../TestEntity.h"
#include "../../ECS/SystemManager.h"


GOAP::ActionTurnOnXRay::ActionTurnOnXRay(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "TurnOnXRay", 1, 5.0f, targetPos)
{
	//SetPrecondition("kHasSupplyBoxSnack", 1);
	SetPrecondition("kIsReadyForXRayScan", 1);
	SetEffect("kIsXRayScanned", 1);
	//SetEffect("kIsVentOn", 1);
	SetAnimName("Fiddleing");
}

GOAP::ActionTurnOnXRay::~ActionTurnOnXRay()
{
}


bool
GOAP::ActionTurnOnXRay::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionTurnOnXRay::CheckProceduralPreconditions()
{
	/*
	if (providerTestEntity_ == nullptr)
		return false;
		*/
	return true;
}

bool 
GOAP::ActionTurnOnXRay::StartPerform()
{
	GOAP::Action::StartPerform();
	// TODO:
	//ECS->SetAnimation(providerEntityID_, "Testing", true, true);
	Message msg((MessageType)EntityMessageType::StateChanged);
	msg.intValue = (int32)EntityState::Working;
	msg.receiver = providerEntityID_;
	EntityMessageHandler::Instance()->Dispatch(msg);
	return true;
}


bool
GOAP::ActionTurnOnXRay::EndPerform()
{
	/*
	SStorage* sysStorage = pOwner_->GetSysStorage();
	//pOwner_->agentBeliefs_.SetVariable("kIsSwabbed", 1);
	//providerTestEntity_->agentBeliefs_.SetVariable("kIsSwabbed", 1);
	//string logString("I'm " + GetName() + " and my provider should be swabbed");
	//LOG_GAME_ERROR(logString.c_str());
	pOwner_->ConsumeHoldingItem();
	//ItemType itemType = sysStorage->GetItemType(providerEntityID_);
	sysStorage->SetNumItem(providerEntityID_, ItemType::SnackSandwich, 10);
	*/
	bool xRayResult = false;
	if (Math::RandInRange(0, 10) < 5)
	{
		xRayResult = true;
	}
	xRayResult = false;
	// ---<
	GameTime zeroGameTime(0, 0, 0);
	TestEntity* targetTestEntity = ownerGoal_->GetTargetTestEntity();
	int32 targetTestEntityID = targetTestEntity->GetID();
	CharMsgDispatcher::Instance()->DispatchMessage(zeroGameTime,
												   SENDER_ID_IRRELEVANT,
												   ownerGoal_->GetTargetTestEntity()->GetID(),
												   CharMsgType::XRayResultReady,
												   &xRayResult);

	if (xRayResult == true)
	{
		Message msg((MessageType)EntityMessageType::StateChanged);
		msg.intValue = (int32)EntityState::BadResult;
		msg.receiver = providerEntityID_;
		EntityMessageHandler::Instance()->Dispatch(msg);
	}
	else
	{
		Message msg((MessageType)EntityMessageType::StateChanged);
		msg.intValue = (int32)EntityState::GoodResult;
		msg.receiver = providerEntityID_;
		EntityMessageHandler::Instance()->Dispatch(msg);
	}

	Message msg((MessageType)EntityMessageType::StateChanged);
	msg.intValue = (int32)EntityState::Normal;
	msg.receiver = providerEntityID_;
	GameTime delayTime = GameTime(0, 0, 10);
	EntityMessageHandler::Instance()->DispatchLater(msg, delayTime);
	return true;
}
