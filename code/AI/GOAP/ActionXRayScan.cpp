#include "ActionXRayScan.h"
#include "../../TestEntity.h"
#include "Goal.h"

GOAP::ActionXRayScan::ActionXRayScan(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "XRayScan", 1, 5.0f, targetPos)
{
	//SetPrecondition("kIsSwabbed", 1);
	SetEffect("kIsReadyForXRayScan", 1);
	SetAnimName("Fiddling");
	SetIsOneTime(true);
}

GOAP::ActionXRayScan::~ActionXRayScan()
{
}


bool
GOAP::ActionXRayScan::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionXRayScan::CheckProceduralPreconditions()
{
	// TODO: action provider entity is broken
	/*
	if(!ECS->EntityExists(providerEntityID_) ||
	   ECS->EntityBroken(providerEntityID_)
	{
		return false;
	}
	*/
	return true;
}

bool 
GOAP::ActionXRayScan::StartPerform()
{
	GOAP::Action::StartPerform();
	// TODO:
	//ECS->SetAnimation(providerEntityID_, "Testing", true, true);
	return true;
}

bool
GOAP::ActionXRayScan::EndPerform()
{
	/*
	// TODO:
	//ECS->SetDefaultAnimation(providerEntityID_);
	//pOwner_->agentBeliefs_.SetVariable("kIsPCRTested", 1);
	ownerGoal_->GetTargetTestEntity()->agentBeliefs_.SetVariable("kIsPCRTested", 1);
	bool testResult = true;
	GameTime zeroGameTime(0, 0, 0);
	TestEntity* targetTestEntity = ownerGoal_->GetTargetTestEntity();
	int32 targetTestEntityID = targetTestEntity->GetID();
	CharMsgDispatcher::Instance()->DispatchMessage(zeroGameTime,
												   SENDER_ID_IRRELEVANT,
												   ownerGoal_->GetTargetTestEntity()->GetID(),
												   CharMsgType::TestResultReady,
												   &testResult);
	// TODO:
	string logString("I'm " + GetName() + " and my owner should have a Covid test result");
	LOG_GAME_ERROR(logString.c_str());
	// TODO
	//ECS->IncreaseEntityMaintnainceNeed();
	*/
	int32 a = 100;
	//ownerGoal_->GetTargetTestEntity()->agentBeliefs_.SetVariable("kIsXRayScanned", 1);
	// ---> NOTE: lung damaged possibility
	return true;
}


