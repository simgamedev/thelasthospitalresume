#include "Planner.h"
#include <algorithm>

/*********************************************************************************
 * Default Constructor.
 *********************************************************************************/
GOAP::Planner::Planner()
{
}

/*********************************************************************************
 * Calculate Heuristic.
 *********************************************************************************/
int32
GOAP::Planner::CalculateHeuristic(const GOAP::WorldState& start, const GOAP::WorldState& goal) const
{
    return 100;
}

std::vector<GOAP::Action>
GOAP::Planner::Plan(const WorldState& start,
                    const WorldState& goal,
                    const std::vector<Action>& actions)
{
    if (start.MeetsGoal(goal))
    {
        // return an empy list of actions.
        return std::vector<GOAP::Action>();
    }

    openList_.clear();
    closedList_.clear();

    Node startingNode(start, 0, CalculateHeuristic(start, goal), 0, nullptr);
    openList_.push_back(startingNode);

    while (openList_.size() > 0)
    {
        // get the lowest F node from openList
        Node& currentNode(PopAndClose());

        // ---> Is our current state the goal state? If so, we've found a path. yay!
        if (currentNode.ws_.MeetsGoal(goal))
        {
            std::vector<Action> thePlan;
            do
            {
                thePlan.push_back(*currentNode.action_);
                //  Get Parent Node
                auto itr = std::find_if(begin(openList_),
                                        end(openList_),
                                        [&](const Node& n)
                                        {
                                            return n.id_ == currentNode.parentID_;
                                        });
                if (itr == end(openList_))
                {
                    itr = std::find_if(begin(closedList_),
                                       end(closedList_),
                                       [&](const Node& n)
                                       {
                                           return n.id_ == currentNode.parentID_;
                                       });
                }
                currentNode = *itr;
            } while (currentNode.parentID_ != 0);
            return thePlan;
        }
        // ---<

        // ---> Check each NEIGHBOR node(i.e. nodes that are reachable from current node)
        for (const auto& potentialAction : actions)
        {
            if (potentialAction.AchievableGiven(currentNode.ws_))
            {
                WorldState outcome = potentialAction.ActOn(currentNode.ws_);
                // ---> Skip if already on closed list.(or is not walkable in case of normal pathfining)
                if (MemberOfClosedList(outcome))
                {
                    continue;
                }
                // ---<
                // ---> If not on open list, put it on open list
                auto itr = MemberOfOpenList(outcome);
                if (itr == end(openList_))
                {
                    Node found(outcome,
                               currentNode.g_ + potentialAction.Cost(),
                               CalculateHeuristic(outcome, goal),
                               currentNode.id_,
                               &potentialAction);
                    AddToOpenList(std::move(found));
                }
                // ---<
                // ---> Already on open list.
                else
                {
                    // Check if the current G is better than recored G
                    if (currentNode.g_ + potentialAction.Cost() < itr->g_)
                    {
                        itr->parentID_ = currentNode.id_;
                        itr->g_ = currentNode.g_ + potentialAction.Cost();
                        itr->h_ = CalculateHeuristic(outcome, goal);
                        itr->action_ = &potentialAction;

                        // resort
                        std::sort(begin(openList_), end(openList_));
                    }
                }
            }
        }
    }

    // ---> No Path Found, Return an empty list.
    std::vector<Action> empty;
    return empty;
}

/*********************************************************************************
 * Pop and Close.
 *********************************************************************************/
GOAP::Node&
GOAP::Planner::PopAndClose()
{
    assert(!openList_.empty());
    closedList_.push_back(std::move(openList_.front()));
    openList_.erase(openList_.begin());

    return closedList_.back();
}


/*********************************************************************************
 * Member of Closed List.
 *********************************************************************************/
bool
GOAP::Planner::MemberOfClosedList(const WorldState& ws) const
{
    if (std::find_if(begin(closedList_), end(closedList_),
                     [&](const Node& n)
                     {
                         return n.ws_ == ws;
                     }) == end(closedList_)) 
    {
        return false;
    }

	return true;
}

/*********************************************************************************
 * Member of Open List.
 *********************************************************************************/
std::vector<GOAP::Node>::iterator
GOAP::Planner::MemberOfOpenList(const WorldState& ws)
{
    return std::find_if(begin(openList_), end(openList_),
                        [&](const Node& n)
                        {
                            return n.ws_ == ws;
                        });
}

/*********************************************************************************
 * Add to Open List.
 *********************************************************************************/
void
GOAP::Planner::AddToOpenList(Node&& n)
{
    // insert mainting sort order
    auto itr = std::lower_bound(begin(openList_),
                                end(openList_),
                                n);
    openList_.emplace(itr, std::move(n));
}
