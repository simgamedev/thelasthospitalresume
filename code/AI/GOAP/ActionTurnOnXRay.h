#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionTurnOnXRay : public Action
	{
	public:
		ActionTurnOnXRay(TestEntity* owner, vec2f targetPos);
		~ActionTurnOnXRay();

		bool StartPerform() override;
		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
