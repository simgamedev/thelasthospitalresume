#pragma once

#include "Action.h"

namespace GOAP
{
	class Goal;
	class ActionGiveMeDrug : public Action
	{
	public:
		// TODO: reanme owner -> Character* executer
		ActionGiveMeDrug(TestEntity* owner, vec2f targetPos);
		~ActionGiveMeDrug();

		bool StartPerform() override;
		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
