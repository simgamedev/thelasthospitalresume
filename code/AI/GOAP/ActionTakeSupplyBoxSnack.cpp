#include "ActionTakeSupplyBoxSnack.h"
#include "../../TestEntity.h"

GOAP::ActionTakeSupplyBoxSnack::ActionTakeSupplyBoxSnack(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "TakeSupplyBoxSnack", 1, 5.0f, targetPos)
{
	//SetPrecondition("kIsHungry", 1);
	SetEffect("kHasSupplyBoxSnack", 1);
	SetAnimName("TakeStuffFromGiver");

	SetIsOneTime(true);
}

GOAP::ActionTakeSupplyBoxSnack::~ActionTakeSupplyBoxSnack()
{
}


bool
GOAP::ActionTakeSupplyBoxSnack::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionTakeSupplyBoxSnack::CheckProceduralPreconditions()
{
	return true;
}


bool
GOAP::ActionTakeSupplyBoxSnack::EndPerform()
{
	//pOwner_->agentBeliefs_.SetVariable("kHasDrug", 1);
	//string logString("I'm " + GetName() + " and my owner should have a drug");
	//LOG_GAME_ERROR(logString.c_str());
	pOwner_->SetHoldingItem(providerEntityID_);
	return true;
}
