#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionGotoEntity : public Action
	{
	public:
		ActionGotoEntity(TestEntity* owner, int32 entityID,
						 const string& entityName,
						 const vec2f& targetPos);
		~ActionGotoEntity();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	public:
		int32 entityID_;
		string entityName_;
	};
}
