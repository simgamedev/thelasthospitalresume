#include "ActionTakeDrug.h"
#include "../../TestEntity.h"

GOAP::ActionTakeDrug::ActionTakeDrug(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "TakeDrug", 1, 5.0f, targetPos)
{
	//SetPrecondition("kIsHungry", 1);
	SetEffect("kHasDrug", 1);
	SetAnimName("TakeStuffFromGiver");
}

GOAP::ActionTakeDrug::~ActionTakeDrug()
{
}


bool
GOAP::ActionTakeDrug::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionTakeDrug::CheckProceduralPreconditions()
{
	return true;
}


bool
GOAP::ActionTakeDrug::EndPerform()
{
	pOwner_->agentBeliefs_.SetVariable("kHasDrug", 1);
	string logString("I'm " + GetName() + " and my owner should have a drug");
	LOG_GAME_ERROR(logString.c_str());
	return true;
}
