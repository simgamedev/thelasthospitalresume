#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionSweepMe : public Action
	{
	public:
		ActionSweepMe(TestEntity* owner, vec2f targetPos);
		~ActionSweepMe();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
