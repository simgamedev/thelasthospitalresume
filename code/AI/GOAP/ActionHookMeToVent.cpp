#include "ActionHookMeToVent.h"
#include "../../TestEntity.h"
#include "../../ECS/SystemManager.h"


GOAP::ActionHookMeToVent::ActionHookMeToVent(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "RefillMe", 1, 5.0f, targetPos)
{
	SetPrecondition("kIsVentOn", 1);
	SetEffect("kIsPatientOnVent", 1);
	SetAnimName("Fiddleing");

	SetIsOneTime(true);
}

GOAP::ActionHookMeToVent::~ActionHookMeToVent()
{
}


bool
GOAP::ActionHookMeToVent::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionHookMeToVent::CheckProceduralPreconditions()
{
	/*
	if (providerTestEntity_ == nullptr)
		return false;
		*/
	return true;
}


bool
GOAP::ActionHookMeToVent::EndPerform()
{
	/*
	SStorage* sysStorage = pOwner_->GetSysStorage();
	//pOwner_->agentBeliefs_.SetVariable("kIsSwabbed", 1);
	//providerTestEntity_->agentBeliefs_.SetVariable("kIsSwabbed", 1);
	//string logString("I'm " + GetName() + " and my provider should be swabbed");
	//LOG_GAME_ERROR(logString.c_str());
	pOwner_->ConsumeHoldingItem();
	//ItemType itemType = sysStorage->GetItemType(providerEntityID_);
	sysStorage->SetNumItem(providerEntityID_, ItemType::SnackSandwich, 10);
	*/

	return true;
}
