#include "ActionGotoRoom.h"
#include "../../TestEntity.h"

GOAP::ActionGotoRoom::ActionGotoRoom(TestEntity* owner, Room* room)
	: Action(owner, "GotoRoom", 1, 1.0f, room->GetRandomEmptyPos())
{
	pRoom_ = room;
	string effectString = "kIsInRoom";
	effectString.append(magic_enum::enum_name(room->GetType()));
	SetEffect(effectString, 1);
	//SetAnimName("LieInBed");
	if (room->GetType() == RoomType::PCRTestRoom)
	{
		isOnePerson_ = true;
	}
}

GOAP::ActionGotoRoom::~ActionGotoRoom()
{
}


bool
GOAP::ActionGotoRoom::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionGotoRoom::CheckProceduralPreconditions()
{
	if (pRoom_->bOccupied_)
		return false;
	return true;
}


bool
GOAP::ActionGotoRoom::EndPerform()
{
	// ---> Room Is Occupied Then
	if (pRoom_->GetType() == RoomType::PCRTestRoom)
	{
		pRoom_->SetOccupied(true);
	}
	// ---< Room Is Occupied Then
	string effectString = "kIsInRoom";
	effectString.append(magic_enum::enum_name(pRoom_->GetType()));
	pOwner_->agentBeliefs_.SetVariable(effectString, 1);
	switch(pRoom_->GetType())
	{
		case RoomType::PCRTestRoom:
		{
			pOwner_->GetGameWorld()->AddAction("ActionSwabMe", pOwner_);
		} break;

		case RoomType::XRayRoom:
		{
			//pOwner_->AddGoal("GoalGotoEntity", room->GetEntity("XRayMachine"));
		} break;

		case RoomType::Pharmacy:
		{
			// Goal DispenseDrugToPatient
			// ActionTakeDrug effect: (kHasDrug,1)
			// ActionGiveMeDrug pre: (kHasDrug, 1), after: (kHasDrug, 1) / contrary to ActionFeedMeDrug
			// OnMessage(CharMsgType::ItemReceived)
			// Goal::PayMoney
			// Goal::GoHome
		} break;

		case RoomType::Ward:
		{
			//pOwner_->AddGoal("GoalLayInBedFor3Days");
		} break;

		default:
		{
		} break;
	}
	return  true;
}
