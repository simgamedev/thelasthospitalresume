#pragma once
#include "Action.h"

namespace GOAP
{
	class ActionEatSnack : public Action
	{
	public:
		ActionEatSnack(TestEntity* owner);
		~ActionEatSnack();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
