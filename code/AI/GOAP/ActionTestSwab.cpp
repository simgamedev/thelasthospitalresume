#include "ActionTestSwab.h"
#include "../../TestEntity.h"
#include "Goal.h"

GOAP::ActionTestSwab::ActionTestSwab(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "TestSwab", 1, 5.0f, targetPos)
{
	SetPrecondition("kIsSwabbed", 1);
	SetEffect("kIsPCRTested", 1);
	SetAnimName("Fiddle");
}

GOAP::ActionTestSwab::~ActionTestSwab()
{
}


bool
GOAP::ActionTestSwab::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionTestSwab::CheckProceduralPreconditions()
{
	// TODO: action provider entity is broken
	/*
	if(!ECS->EntityExists(providerEntityID_) ||
	   ECS->EntityBroken(providerEntityID_)
	{
		return false;
	}
	*/
	return true;
}

bool 
GOAP::ActionTestSwab::StartPerform()
{
	GOAP::Action::StartPerform();
	// TODO:
	//ECS->SetAnimation(providerEntityID_, "Testing", true, true);
	Message msg((MessageType)EntityMessageType::StateChanged);
	msg.intValue = (int32)EntityState::Working;
	msg.receiver = providerEntityID_;
	EntityMessageHandler::Instance()->Dispatch(msg);
	return true;
}

bool
GOAP::ActionTestSwab::EndPerform()
{
	// TODO:
	//ECS->SetDefaultAnimation(providerEntityID_);
	//pOwner_->agentBeliefs_.SetVariable("kIsPCRTested", 1);
	ownerGoal_->GetTargetTestEntity()->agentBeliefs_.SetVariable("kIsPCRTested", 1);

	// ---> NOTE: covid-90 positive possibility
	bool testResult = false;
	if (Math::RandInRange(0, 10) < 5)
	{
		testResult = true;
	}
	testResult = true;
	// ---<
	GameTime zeroGameTime(0, 0, 0);
	TestEntity* targetTestEntity = ownerGoal_->GetTargetTestEntity();
	int32 targetTestEntityID = targetTestEntity->GetID();
	CharMsgDispatcher::Instance()->DispatchMessage(zeroGameTime,
												   SENDER_ID_IRRELEVANT,
												   ownerGoal_->GetTargetTestEntity()->GetID(),
												   CharMsgType::TestResultReady,
												   &testResult);
	// TODO:
	string logString("I'm " + GetName() + " and my owner should have a Covid test result");
	LOG_GAME_ERROR(logString.c_str());
	// TODO
	//ECS->IncreaseEntityMaintnainceNeed();
	if (testResult == true)
	{
		Message msg((MessageType)EntityMessageType::StateChanged);
		msg.intValue = (int32)EntityState::BadResult;
		msg.receiver = providerEntityID_;
		EntityMessageHandler::Instance()->Dispatch(msg);
	}
	else
	{
		Message msg((MessageType)EntityMessageType::StateChanged);
		msg.intValue = (int32)EntityState::GoodResult;
		msg.receiver = providerEntityID_;
		EntityMessageHandler::Instance()->Dispatch(msg);
	}

	Message msg((MessageType)EntityMessageType::StateChanged);
	msg.intValue = (int32)EntityState::Normal;
	msg.receiver = providerEntityID_;
	GameTime delayTime = GameTime(0, 0, 10);
	EntityMessageHandler::Instance()->DispatchLater(msg, delayTime);
	return true;
}


