#include "ActionTakeSwab.h"
#include "../../TestEntity.h"

GOAP::ActionTakeSwab::ActionTakeSwab(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "TakeSwab", 1, 5.0f, targetPos)
{
	//SetPrecondition("kIsHungry", 1);
	SetEffect("kHasSwab", 1);
	SetAnimName("Fiddle");
}

GOAP::ActionTakeSwab::~ActionTakeSwab()
{
}


bool
GOAP::ActionTakeSwab::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionTakeSwab::CheckProceduralPreconditions()
{
	// TODO:
	/*
	if (!ECS->EntityExists(providerID_) ||
		ECS->GetProviderStorageNum() <= 0)
		return false;
		*/
	return true;
}


bool
GOAP::ActionTakeSwab::EndPerform()
{
	string logString("I'm " + GetName() + " and my owner should have a swab");
	LOG_GAME_ERROR(logString.c_str());

	// TODO: provider's swab quantity should decrease by one
	//ECS->DecreaseStorageByOne(providerID_);
	//pOwner_->SetHoldingEntity("pcrtestswab_00");
	// TODO: should put these 2 together
	pOwner_->agentBeliefs_.SetVariable("kHasSwab", 1);
	pOwner_->SetHoldingItem(ItemType::MedPCRTestSwab);

	return true;
}
