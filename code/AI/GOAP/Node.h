#pragma once


#include "Action.h"
#include "WorldState.h"

namespace GOAP
{
	struct Node
	{
		static int32 idCounter_;
		WorldState ws_; // the state of the world at this point
		int32 id_; // the unique id of the node
		int32 parentID_;
		int32 g_; // the A* cost from 'start' to 'here'
		int32 h_;
		const Action* action_;

		Node();
		Node(const WorldState state, int32 g, int32 h, int32 parentID, const Action* action);
		// F -- which is simply G+H
		int32 F() const
		{
			return g_ + h_;
		}

		friend std::ostream& operator<<(std::ostream& out, const Node& n);
	};

	bool operator<(const Node& lhs, const Node& rhs);

	inline std::ostream& operator<<(std::ostream& out, const Node& n)
	{
		out << "Node { id:" << n.id_ << " parent:" << n.parentID_
			<< " F:" << n.F() << " G:" << n.g_ << " H:" << n.h_;
		out << ", " << n.ws_ << "}";
		return out;
	}
}