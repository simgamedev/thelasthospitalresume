#pragma once

#include "../../common.h"
#include <map>
#include <SFML/System/Time.hpp>
#include "../../Messaging/CharMsgDispatcher.h"
#include "../../Messaging/CharMsgTypes.h"
#include "../../Time/Time.h"
#include "../../Room/Room.h"

class TestEntity;
namespace GOAP
{
	class Goal;
	struct WorldState;
	class Action
	{
	public:
		Action();
		Action(TestEntity* owner, string name, int32 cost, real32 duration = 0.0f,
			   vec2f targetPos = vec2f(-1.0f, -1.0f));
		bool AchievableGiven(const WorldState& ws) const;
		WorldState ActOn(const WorldState& ws) const;
		void SetTargetEntityName(const string& name);



		// ---> Getters&Setters
		//void SetPrecondition(const string stateName, int32 entityID, int32 stateValue) { preconditions_[std::make_pair(stateName, entityID)] = stateValue; }
		void SetPrecondition(const string stateName, int32 stateValue) { preconditions_[stateName] = stateValue; }
		//void SetEffect(const string stateName, int32 entityID, int32 stateValue) { effects_[std::make_pair(stateName, entityID)] = stateValue; }
		void SetEffect(const string stateName, int32 stateValue) { effects_[stateName] = stateValue;  }
		int32 Cost() const { return cost_; }
		real32 GetDuration() const { return duration_; };
		string GetName() const { return name_; }
		void SetTargetPos(const vec2f& targetPos) { targetPos_ = targetPos; }
		vec2f GetTargetPos() const { return targetPos_; }
		void SetAnimName(const string& name) { animName_ = name; }
		string GetAnimName() const { return animName_; }
		// ---> Modify
		int32 GetID() const;
		void SetProviderEntityID(const int32 entityID);
		int32 GetProviderEntityID() const;
		void SetProviderTestEntity(TestEntity* testEntity);
		TestEntity* GetProviderTestEntity() const;
		int32 providerEntityID_;
		string providerEntityName_;
		TestEntity* providerTestEntity_;
		void SetOwner(TestEntity* owner);
		void Update(const sf::Time& time);
		bool IsDone();
		bool IsStarted();
		virtual bool StartPerform();
		virtual bool EndPerform();
		virtual bool RequiresInRange();
		virtual bool IsInRange();
		virtual bool CheckProceduralPreconditions();
		bool started_;
		bool finished_;
		vec2f targetPos_ = vec2f(-1.0f, -1.0f);
		void SetOwnerGoal(Goal* ownerGoal);
		// ---<
		void SetIsOneTime(bool value);
		void SetIsOnePerson(bool value);
		bool IsOnePerson() const;
	public:
		Goal* ownerGoal_;
		string name_;
		int32 cost_;
		real32 duration_;
		real32 elapsedTime_;
		string animName_;
		string targetEntityName_;
		int32 targetEntityID_ = -1;
		//std::map<std::pair<string, int32>, int32> preconditions_;
		std::map<string, int32> preconditions_;
		//std::map<std::pair<string, int32>, int32> effects_;
		std::map<string, int32> effects_;
		TestEntity* pOwner_ = nullptr;
		Room* pRoom_ = nullptr;

		bool isTaken_;
		bool isInnate_;
		bool isOneTime_;
		bool isOnePerson_;
		uint32 id_;
	private:
		static int32 nextID_;
	};
}
