#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionTakeDrug : public Action
	{
	public:
		ActionTakeDrug(TestEntity* owner, vec2f targetPos);
		~ActionTakeDrug();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
