#include "WorldState.h"


/*********************************************************************************
 * Default Constructor.
 *********************************************************************************/
GOAP::WorldState::WorldState(const string& name)
	: name_(name)
    , priority_(0)
{
    // nop
}

void
GOAP::WorldState::SetVariable(const string key, int32 value)
{
    vars_[key] = value;
}

int32
GOAP::WorldState::GetVariable(const string key)
{
    if (vars_.find(key) == vars_.end())
    {
        return -1; // -1 means no key found
    }
    else
    {
        return vars_[key];
    }
}

bool
GOAP::WorldState::operator==(const WorldState& other) const
{
    return (vars_ == other.vars_);
}

bool
GOAP::WorldState::MeetsGoal(const WorldState& goalState) const
{
    for (const auto& kv : goalState.vars_)
    {
        if (vars_.find(kv.first) == vars_.end())
            return false;
        if (vars_.at(kv.first) != kv.second)
        {
            return false;
        }
    }
    return true;
}

int32
GOAP::WorldState::DistanceTo(const WorldState& goalState) const
{
    int32 result = 0;
    for (const auto& kv : goalState.vars_)
    {
        auto itr = vars_.find(kv.first);
        if (itr == end(vars_) || itr->second != kv.second)
        {
            ++result;
        }
    }

    return result;
}

/*********************************************************************************
 * Getters&Setters.
 *********************************************************************************/
void
GOAP::WorldState::SetPriority(real32 priority)
{
    priority_ = priority;
}
string
GOAP::WorldState::GetName() const
{
    return name_;
}
