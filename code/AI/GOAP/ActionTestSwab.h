#pragma once

#include "Action.h"

namespace GOAP
{
	class Goal;
	class ActionTestSwab : public Action
	{
	public:
		ActionTestSwab(TestEntity* owner, vec2f targetPos);
		~ActionTestSwab();

		bool StartPerform() override;
		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
