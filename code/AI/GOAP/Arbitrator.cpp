#include "Arbitrator.h"
#include "../../TestEntity.h"

GOAP::Arbitrator::Arbitrator(TestEntity* owner)
	: owner_(owner)
{

	// ---> tester goals
	/*
	if (owner_->GetProfession() == Profession::Tester)
	{
		GOAP::WorldState desiredState;
		desiredState.SetVariable("kIsPCRTested", 1);
		GOAP::Goal* goal = new GOAP::Goal("GivePCRTest", desiredState, 3.0f);
		goals_.push_back(goal);
	}
	*/
	// ---< tester goals

	// ---> nurse goals
	if (owner_->GetProfession() == Profession::Nurse)
	{
		/**
		GOAP::WorldState desiredState;
		desiredState.SetVariable("kIsPCRTested", 1);
		GOAP::Goal* goal = new GOAP::Goal("GivePCRTest", desiredState, 3.0f);
		goals_.push_back(goal);
		*/
	}
	// ---< nurse

	// ---> patient goals
	// ---< patient goals
}

void
GOAP::Arbitrator::AddGoal(Goal* goal)
{
	if (owner_->profession_ == Profession::Nurse)
	{
		int32 a = 100;
		int32 c = 200;
	}
	goals_.push_back(goal);
	int32 a = 100;
	int32 c = 200;
}

GOAP::Arbitrator::~Arbitrator()
{
	// clean
}

/******************************* Update ****************************************
 *
 *******************************************************************************/
void
GOAP::Arbitrator::Update()
{
	// TODO:
	// we shouldn't have the testEntityID in worldState right?
	// or we should use
	/*
	struct SWorldProperty
	{
		GAME_OBJECT_ID hSubjectID;
		WORLD_PROP_KEY eKey;

		union value
		{
			bool bVluae;
			real32 fValue;
			int32 nValue;
		};
	};
	*/
	// ---> Goal Eat
	if(owner_->agentBeliefs_.GetVariable("kIsHungry")== 1)
	{
		if (!HasGoal("Eat"))
		{
			if (owner_->GetProfession() == Profession::Patient &&
				owner_->pcrTested_ == false)
			{
			}
			else
			{
				GOAP::WorldState desiredState;
				desiredState.SetVariable("kIsHungry", 0);
				GOAP::Goal* goal = new GOAP::Goal("Eat", desiredState);
				goals_.push_back(goal);
			}
		}
	}
	// ---< Goal Eat

	// ---> Goal Shit
	if (owner_->agentBeliefs_.GetVariable("kIsInNeedOfToilet") == 1)
	{
		if (!HasGoal("Shit"))
		{
			GOAP::WorldState desiredState;
			desiredState.SetVariable("kIsInNeedOfToilet", 0);
			GOAP::Goal* goal = new GOAP::Goal("Shit", desiredState, 2.0f);
			goals_.push_back(goal);
		}
	}
	// ---< Goal Shit

	// ---> Goal Go Home
	if (owner_->agentBeliefs_.GetVariable("kIsCured") == 1)
	{
		if (!HasGoal("GoHome"))
		{
			GOAP::WorldState desiredState;
			desiredState.SetVariable("kIsAtHome", 1);
			GOAP::Goal* goal = new GOAP::Goal("GoHome", desiredState, 1000.0f);
			goals_.push_back(goal);
		}
	}
	// ---<
}

/******************************* HasGoal ***************************************
 *
 *******************************************************************************/
bool
GOAP::Arbitrator::HasGoal(const string& goalName)
{
	for (GOAP::Goal* goal : goals_)
	{
		if (goal->GetName().find(goalName) != std::string::npos)
		{
			return true;
		}
	}
	return false;
}

GOAP::Goal*
GOAP::Arbitrator::GetGoal()
{
	// TODO:
	// it has a 70% chance to pick up a TaskGoal
	// a 30% chance to pick up a BasicGoal
	if (goals_.size() > 0)
	{
		/*
		real32 highestInsistence = 0.0f;
		int32 idx = 0;
		int32 counter = 0;
		for (GOAP::Goal* goal : goals_)
		{
			if (goal->GetInsistence() > highestInsistence)
			{
				idx = counter;
				highestInsistence = goal->GetInsistence();
			}
			counter++;
		}
		*/

		return goals_.at(0);
	}
	else
	{
		return nullptr;
	}
}


void
GOAP::Arbitrator::RemoveGoal(const string& goalName)
{
	for (auto itr = goals_.begin();
		 itr != goals_.end();
		 itr++)
	{
		Goal* goal = *itr;
		if (goal->GetName() == goalName)
		{
			delete goal;
			goals_.erase(itr);
			break;
		}
	}
}

void
GOAP::Arbitrator::RemoveGoal(Goal* goal)
{
	for (auto itr = goals_.begin();
		 itr != goals_.end();
		 itr++)
	{
		if (goal == *itr)
		{
			delete goal;
			goals_.erase(itr);
			break;
		}
	}
}

/******************************* OnGoalNotAchievable ****************************
 ********************************************************************************/
void
GOAP::Arbitrator::OnGoalNotAchievable(Goal* goal)
{
	if (goals_.size() > 1)
	{
		auto itr = goals_.begin();
		Goal* goal = goals_.at(0);
		goals_.erase(itr);
		goals_.push_back(goal);
	}
	else
	{
		// just do nothing
	}
}
