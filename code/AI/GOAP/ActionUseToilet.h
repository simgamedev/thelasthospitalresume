#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionUseToilet : public Action
	{
	public:
		ActionUseToilet(TestEntity* owner, vec2f targetPos);
		~ActionUseToilet();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
