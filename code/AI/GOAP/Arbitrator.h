#pragma once
#include "Goal.h"
class TestEntity;
namespace GOAP
{

class Arbitrator
{
public:
	Arbitrator(TestEntity* owner);
	~Arbitrator();

	void AddGoal(Goal* goal);
	void RemoveGoal(const string& goalName);
	void RemoveGoal(Goal* goal);
	Goal* GetGoal();
	void OnGoalNotAchievable(Goal* goal);
	void Update();
private:
	TestEntity* owner_;
	std::vector<Goal*> goals_;
	bool HasGoal(const string& goalName);
	std::vector<Goal*> basicGoals_;
	std::vector<Goal*> taskGoals_;
};

}
