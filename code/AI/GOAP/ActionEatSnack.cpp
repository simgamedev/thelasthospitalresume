#include "ActionEatSnack.h"
#include "../../TestEntity.h"
#include "../../StateGame.h"


GOAP::ActionEatSnack::ActionEatSnack(TestEntity* owner)
	: GOAP::Action(owner, "EatSnack", 1, 10.0f)
{
	SetPrecondition("kHasSnack", 1);
	SetEffect("kIsHungry", 0);
	// TODO: Set Side Effect(eating will increase toilet)
	// Set
	SetAnimName("EatSnack");
}

GOAP::ActionEatSnack::~ActionEatSnack()
{
}

bool
GOAP::ActionEatSnack::RequiresInRange()
{
	return false;
}

bool
GOAP::ActionEatSnack::CheckProceduralPreconditions()
{
	return true;
}

bool
GOAP::ActionEatSnack::EndPerform()
{
	pOwner_->agentBeliefs_.SetVariable("kHasSnack", 0);
	pOwner_->GetStateGame()->PutItemOnGround(pOwner_->GetPosition(), ItemType::LitterSnack);
	// TODO: should rename to increase hunger(satisfaction)
	//pOwner_->DecreaseHunger(50);
	pOwner_->DecreaseHunger(Math::RandInRange(200, 300));
	pOwner_->ConsumeHoldingItem();
	pOwner_->DecreaseToiletSatisfaction(50);

	return true;
}
