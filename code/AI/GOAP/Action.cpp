#include "Action.h"
#include "WorldState.h"
#include "../../TestEntity.h"
#include "Goal.h"


int32 GOAP::Action::nextID_ = 0;
/*********************************************************************************
 * Default Constructor.
 *********************************************************************************/
GOAP::Action::Action()
    : cost_(0)
    , name_("")
    , started_(false)
    , finished_(false)
{
}

GOAP::Action::Action(TestEntity* owner, string name, int32 cost, real32 duration, vec2f targetPos)
    : cost_(cost)
    , name_(name)
    , duration_(duration)
    , pOwner_(owner)
    , elapsedTime_(0.0f)
    , targetPos_(targetPos)
    , started_(false)
    , finished_(false)
    , providerEntityID_(-1)
    , providerTestEntity_(nullptr)
    , isTaken_(false)
    , isInnate_(false)
    , isOneTime_(false)
    , isOnePerson_(false)
{
    id_ = nextID_;
    nextID_++;
}

/*********************************************************************************
 * Achievable Given that
 *********************************************************************************/
bool GOAP::Action::AchievableGiven(const WorldState& ws) const
{
    for (const auto& precond : preconditions_)
    {
        if (ws.vars_.find(precond.first) == ws.vars_.end())
            return false;
        if (ws.vars_.at(precond.first) != precond.second)
        {
            return false;
        }
    }
    return true;
}

/*********************************************************************************
 * Act On
 *********************************************************************************/
GOAP::WorldState
GOAP::Action::ActOn(const WorldState& ws) const
{
    GOAP::WorldState tmp(ws);
    for (const auto& effect : effects_)
    {
        tmp.SetVariable(effect.first, effect.second);
    }
    return tmp;
}

/*********************************************************************************
 * Set Target Name.
 *********************************************************************************/
void
GOAP::Action::SetTargetEntityName(const string& name)
{
    targetEntityName_ = name;
}

/*********************************************************************************
 * Set Target Entity.
 *********************************************************************************/

// ---> Modify
bool
GOAP::Action::IsStarted()
{
    return started_;
}
void
GOAP::Action::SetOwner(TestEntity* owner)
{
	pOwner_ = owner;
}
void
GOAP::Action::Update(const sf::Time& time)
{
    if (started_)
    {
        elapsedTime_ += time.asSeconds();
        if (elapsedTime_ >= duration_)
        {
			string logString("I'm " + GetName() + " and I'm updating!");
			LOG_GAME_INFO(logString);
            finished_ = true;
        }
    }
}

bool
GOAP::Action::StartPerform()
{
    string logString("I'm " + GetName() + " and I started to perform!");
    LOG_GAME_ERROR(logString.c_str());
    started_ = true;
    //return CheckProceduralPreconditions();
    return true;
}

bool
GOAP::Action::EndPerform()
{
    return true;
}

bool
GOAP::Action::CheckProceduralPreconditions()
{
    return true;
}


bool
GOAP::Action::IsDone()
{
    return finished_;
}


bool
GOAP::Action::RequiresInRange()
{
    return false;
}

bool
GOAP::Action::IsInRange()
{
    if (std::abs(pOwner_->GetPosition().x - targetPos_.x) < 4 &&
        std::abs(pOwner_->GetPosition().y - targetPos_.y) < 4)
        return true;
    return false;
}

void
GOAP::Action::SetProviderEntityID(const int32 entityID)
{
    providerEntityID_ = entityID;
}

void
GOAP::Action::SetProviderTestEntity(TestEntity* testEntity)
{
    providerTestEntity_ = testEntity;
}

int32 
GOAP::Action::GetProviderEntityID() const
{
    return providerEntityID_;
}


TestEntity*
GOAP::Action::GetProviderTestEntity() const
{
    return providerTestEntity_;
}

void
GOAP::Action::SetOwnerGoal(GOAP::Goal* ownerGoal)
{
    ownerGoal_ = ownerGoal;
}

void
GOAP::Action::SetIsOneTime(bool value)
{
    isOneTime_ = value;
}

void
GOAP::Action::SetIsOnePerson(bool value)
{
    isOnePerson_ = value;
}

bool
GOAP::Action::IsOnePerson() const
{
    return isOnePerson_;
}

/******************************* GetID ****************************************
 *
 ********************************************************************************/
int32
GOAP::Action::GetID() const
{
    return id_;
}
// ---< Modify
