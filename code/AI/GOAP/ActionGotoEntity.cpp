#include "ActionGotoEntity.h"
#include "../../TestEntity.h"

GOAP::ActionGotoEntity::ActionGotoEntity(TestEntity* owner, int32 entityID, 
										 const string& entityName,
										 const vec2f& targetPos)
	: Action(owner, "GotoEntity", 1, 1.0f, targetPos)
	, entityID_(entityID)
	, entityName_(entityName)
{
	string name = entityName.substr(0, entityName_.find('_'));
	providerEntityID_ = entityID;
	string effectString = "kIsAtEntity";
	effectString.append(name);
	SetEffect(effectString, 1);
	//SetAnimName("LieInBed");
	//isOnePerson_ = true;
	SetIsOnePerson(true);
}

GOAP::ActionGotoEntity::~ActionGotoEntity()
{
}


bool
GOAP::ActionGotoEntity::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionGotoEntity::CheckProceduralPreconditions()
{
	return true;
}


bool
GOAP::ActionGotoEntity::EndPerform()
{
	string effectString = "kIsAtEntity";
	effectString.append(entityName_);

	if (entityName_.find("XRayMachine") != string::npos)
	{
		/*
		Message msg((uint32)EntityMessageType::ReadyToOperate);
		msg.intValue = providerEntityID_;
		EntityMessageHandler::Instance()->Dispatch(msg);
		*/
	}
	return  true;
}
