#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionHookMeToVent : public Action
	{
	public:
		ActionHookMeToVent(TestEntity* owner, vec2f targetPos);
		~ActionHookMeToVent();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
