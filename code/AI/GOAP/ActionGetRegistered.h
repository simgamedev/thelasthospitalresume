#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionGetRegistered : public Action
	{
	public:
		ActionGetRegistered(TestEntity* owner, vec2f targetPos);
		~ActionGetRegistered();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
