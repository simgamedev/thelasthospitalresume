#include "ActionGoHome.h"
#include "../../TestEntity.h"


GOAP::ActionGoHome::ActionGoHome(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "GoHome", 1, 1.0f, targetPos)
{
	//SetPrecondition("kIsCured", 1);
	SetEffect("kIsAtHome", 1);
	// TODO: Set Side Effect(eating will increase toilet)
	// Set
	SetAnimName("GoHome");
}

GOAP::ActionGoHome::~ActionGoHome()
{
}

bool
GOAP::ActionGoHome::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionGoHome::CheckProceduralPreconditions()
{
	return true;
}

bool
GOAP::ActionGoHome::EndPerform()
{
	string logString("I'm " + GetName() + "and I finished executing");
	LOG_GAME_INFO(logString);
	GameTime curTime = Clock::Instance()->GetGameTime();
	GameTime futTime = curTime;
	futTime.minutes_ += 10;
	CharMsgDispatcher::Instance()->BroadcastDelayedMessage(futTime,
														   pOwner_->GetID(),
														   CharMsgType::GotHome,
														   nullptr);
	return true;
}

