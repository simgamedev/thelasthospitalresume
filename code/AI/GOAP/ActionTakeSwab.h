#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionTakeSwab : public Action
	{
	public:
		ActionTakeSwab(TestEntity* owner, vec2f targetPos);
		~ActionTakeSwab();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
