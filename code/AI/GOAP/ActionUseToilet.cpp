#include "ActionUseToilet.h"
#include "../../TestEntity.h"

GOAP::ActionUseToilet::ActionUseToilet(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "UseToilet", 1, 5.0f, targetPos)
{
	SetPrecondition("kIsInNeedOfToilet", 1);
	SetEffect("kIsInNeedOfToilet", 0);
	SetAnimName("UseToilet");
}

GOAP::ActionUseToilet::~ActionUseToilet()
{
}


bool
GOAP::ActionUseToilet::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionUseToilet::CheckProceduralPreconditions()
{
	return true;
}


bool
GOAP::ActionUseToilet::EndPerform()
{
	pOwner_->IncreaseToiletSatisfaction(80);
	string logString("I'm " + GetName() + " and my owner should use toilet");
	LOG_GAME_ERROR(logString.c_str());

	return true;
}
