#pragma once

#include "Action.h"

namespace GOAP
{
	class ActionGotoPos : public Action
	{
	public:
		ActionGotoPos(TestEntity* owner, vec2f targetPos);
		~ActionGotoPos();

		bool RequiresInRange() override;
		bool CheckProceduralPreconditions() override;
		bool EndPerform() override;
	private:
	};
}
