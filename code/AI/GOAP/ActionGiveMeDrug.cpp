#include "ActionGiveMeDrug.h"
#include "../../TestEntity.h"
#include "Goal.h"

GOAP::ActionGiveMeDrug::ActionGiveMeDrug(TestEntity* owner, vec2f targetPos)
	: GOAP::Action(owner, "GiveMeDrug", 1, 5.0f, targetPos)
{
	SetPrecondition("kHasDrug", 1);
	SetEffect("kPatientHasDrug", 1);
	SetAnimName("Fiddling");
}

GOAP::ActionGiveMeDrug::~ActionGiveMeDrug()
{
}


bool
GOAP::ActionGiveMeDrug::RequiresInRange()
{
	return true;
}

bool
GOAP::ActionGiveMeDrug::CheckProceduralPreconditions()
{
	// TODO: action provider entity is broken
	/*
	if(!ECS->EntityExists(providerEntityID_) ||
	   ECS->EntityBroken(providerEntityID_)
	{
		return false;
	}
	*/
	return true;
}

bool 
GOAP::ActionGiveMeDrug::StartPerform()
{
	GOAP::Action::StartPerform();
	// TODO:
	//ECS->SetAnimation(providerEntityID_, "Testing", true, true);
	return true;
}

bool
GOAP::ActionGiveMeDrug::EndPerform()
{
	/*
	// TODO:
	//ECS->SetDefaultAnimation(providerEntityID_);
	//pOwner_->agentBeliefs_.SetVariable("kIsPCRTested", 1);
	ownerGoal_->GetTargetTestEntity()->agentBeliefs_.SetVariable("kIsPCRTested", 1);
	bool testResult = true;
	GameTime zeroGameTime(0, 0, 0);
	TestEntity* targetTestEntity = ownerGoal_->GetTargetTestEntity();
	int32 targetTestEntityID = targetTestEntity->GetID();
	CharMsgDispatcher::Instance()->DispatchMessage(zeroGameTime,
												   SENDER_ID_IRRELEVANT,
												   ownerGoal_->GetTargetTestEntity()->GetID(),
												   CharMsgType::TestResultReady,
												   &testResult);
	// TODO:
	string logString("I'm " + GetName() + " and my owner should have a Covid test result");
	LOG_GAME_ERROR(logString.c_str());
	// TODO
	//ECS->IncreaseEntityMaintnainceNeed();
	*/
	int32 a = 100;
	//ownerGoal_->GetTargetTestEntity()->agentBeliefs_.SetVariable("kIsXRayScanned", 1);
	//bool xRayResult = false;
	GameTime zeroGameTime(0, 0, 0);
	TestEntity* targetTestEntity = ownerGoal_->GetTargetTestEntity();
	int32 targetTestEntityID = targetTestEntity->GetID();
	CharMsgDispatcher::Instance()->DispatchMessage(zeroGameTime,
												   SENDER_ID_IRRELEVANT,
												   ownerGoal_->GetTargetTestEntity()->GetID(),
												   CharMsgType::ReceivedDrug,
												   nullptr);
	return true;
}


