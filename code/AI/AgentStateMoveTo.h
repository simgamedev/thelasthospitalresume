#pragma once
#include "FSMState.h"

class AgentStateMoveTo : public FSMState
{
public:
	AgentStateMoveTo(TestEntity* owner);
	void Update(const sf::Time& time);
};
