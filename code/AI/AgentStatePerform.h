#pragma once
#include "FSMState.h"

class AgentStatePerform : public FSMState
{
public:
	AgentStatePerform(TestEntity* owner);
	void Update(const sf::Time& time);
};
