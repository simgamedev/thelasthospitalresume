#pragma once
#include "../common.h"
#include "../TileMap/Tile.h"
#include <queue>

#include "../Messaging/CharMsgDispatcher.h"

enum class RoomType
{
	Outside,
	None,
	Pharmacy,
		Ward,
		Washroom,
		WaitingRoom,
		PCRTestRoom,
		XRayRoom,
};



using RoomID = int32;
class GameWorld;
class TestEntity;
class Room : public IMsgObserver
{
public:
	Room(RoomType roomType, GameWorld* gameWorld);
	void AddTile(Tile* tile);
	void RemoveAllTiles();
	void RemoveTile(Tile* tile);
	static void FloodFill(int32 tileX, int32 tileY, GameWorld* gameWorld);


	std::vector<Tile*> GetTiles();
	int32 GetID();
	void SetType(RoomType roomType);
	RoomType GetType() const;

	// ---> GamePlay
	void Update(real32 dt);
	// ---< GamePlay
private:
	RoomType roomType_;
	bool bValidRoom_;
	bool bValidSpeicalRoom_;
	std::vector<Tile*> tiles_;
	int32 roomID_;
	GameWorld* gameWorld_;

	static void ActualFloodFill(Tile* tile, Room* oldRoom, GameWorld* gameWorld);
private:
	static int32 nextID_;
public:
	// ---> GamePlay
	bool bEquipped_;
	bool bOccupied_;
	TestEntity* pOwner_;
	bool bReadyToWork_;
	std::queue<TestEntity*> queue_;
	// ---< GamePlay
public:
	// ---> GamePlay
	void SetOwner(TestEntity* testEntity);
	void AddToQueue(TestEntity* testEntity);
	void PopQueue();
	bool IsInRoom(TestEntity* testEntity);
	vec2f GetRandomEmptyPos();
	// ---< GamePlay


	// ---> Messaging
	void OnBroadcast(Telegram telegram);
	void RegisterForMsgType(CharMsgType msgType);
	// ---<

	// ---> Entities
	int32 NumEntities(const string& entityName);
	void AddEntityToRoom(int32 entityID);
	std::vector<int32> entities_;

	bool IsValidRoomOfType(RoomType roomType);
	void SetOccupied(bool value);
	// ---< Entities
};
