#include "Room.h"
#include "../TestEntity.h"
#include "../Characters/Patient.h"
#include "../Characters/Tester.h"
#include "../GameWorld/GameWorld.h"
#include "../Messaging/CharMsgDispatcher.h"





int32 Room::nextID_ = 0;

Room::Room(RoomType roomType, GameWorld* gameWorld)
	: roomType_(roomType)
	, roomID_(nextID_)
	, bValidRoom_(true)
	, bOccupied_(false)
	, bValidSpeicalRoom_(false)
	, gameWorld_(gameWorld)
{
	nextID_++;
	//RegisterForMsgType(CharMsgType::PatientRegistered);
}



void
Room::AddTile(Tile* tile)
{
	// ---> Room already has this tile in it.
	if(std::find(tiles_.begin(), tiles_.end(), tile) != tiles_.end())
	{
		int32 a = 100;
		return;
	}
	// ---<

	if (tile->roomID != -1)
	{
		// Remove it from the old room
		gameWorld_->GetRoom(tile->roomID)->RemoveTile(tile);
	}
	tile->roomID = roomID_;
	tiles_.push_back(tile);
}

void
Room::RemoveAllTiles()
{
	for (Tile* tile : tiles_)
	{
		tile->roomID = 0; // room 0 is always the "outside room"
	}
	tiles_.clear();
}

// static
void 
Room::FloodFill(int32 tileX, int32 tileY, GameWorld* gameWorld)
{
	Tile* tile = gameWorld->GetTile(tileX, tileY, 0);
	Room* oldRoom = gameWorld->GetRoom(tile->roomID);

	// ???????
	tile->roomID = -1; // NOTE: a wall doesn't belong to any room
	oldRoom->RemoveTile(tile);

	// Try building new rooms for eachof our NESW directions
	for(Tile* t : gameWorld->GetTileNeighbours(tile))
	{
		ActualFloodFill(t, oldRoom, gameWorld);
	}


	// If this piece of wall was added to an existing room,
	// (which should always be true assuming with consider "outside" to be a big room)
	// delete that room and assign all tiles in that room to be in "outside room"


	if (tile->roomID != 0) // NOTE: Don't ever delete the outside room;
	{
		if (oldRoom->GetTiles().size() > 0)
		{
			LOG_GAME_ERROR("Old Room still has tiles in it, this is clearly wrong");
		}
		gameWorld->DeleteRoom(oldRoom);
	}
}

void
Room::ActualFloodFill(Tile* tile, Room* oldRoom, GameWorld* gameWorld)
{
	// ---> Skip
	if (tile == nullptr)
	{
		LOG_GAME_WARN("Trying to build a wall off the map");
		return;
	}

	if (tile->roomID != oldRoom->GetID())
	{
		// This tile was already assigned to another "new" room, which means
		// that the direction picked isn't isolated. So we can just return
		// without creating a new room??????????
		return;
	}

	if (tile->tileType == TileType::Wall)
	{
		// This tile has a wall
		return;
	}

	if (tile->tileType == TileType::Empty)
	{
		// This tile is empty(ground/land)
		return;
	}

	// ---< Skip


	// potential new room
	Room* newRoom = new Room(RoomType::None, gameWorld);
	std::queue<Tile*> tilesToCheck;
	tilesToCheck.push(tile);
	while (tilesToCheck.size() > 0)
	{
		Tile* t = tilesToCheck.front();
		tilesToCheck.pop();
		if (t->roomID == oldRoom->GetID())
		{
			newRoom->AddTile(t);

			// Add 4/8 neighbours, NO!, YOUDONT!!
			std::vector<Tile*> neighbours = gameWorld->GetTileNeighbours(t);
			for(Tile* t2 : neighbours)
			{
				if (t2 == nullptr || (t2->tileType == TileType::Empty))
				{
					// we have hit empty space(land/ground)
					// so this "room" we're buidling is actually part of the outside room
					// Therefore, we can immediately end the flood fill(which otherwise would
					// take ages), and more importantly, we need to delete this "newRoom"
					// and re-assign all the tiles to outside
					newRoom->RemoveAllTiles();
					delete newRoom;
					nextID_--;
					return;
				}
				if (t2->roomID == oldRoom->GetID() && (t2->tileType != TileType::Wall))
				{
					tilesToCheck.push(t2);
				}
			}
		}
	}

	// ---> Tell the world the new room has been formed
	// TODO: maybe tile->gameWorld?
	gameWorld->AddRoom(newRoom);
}

int32
Room::GetID()
{
	return roomID_;
}

std::vector<Tile*>
Room::GetTiles()
{
	return tiles_;
}


void
Room::RemoveTile(Tile* tile)
{
	for (auto itr = tiles_.begin();
		 itr != tiles_.end();
		 )
	{
		if (*itr == tile)
		{
			itr = tiles_.erase(itr);
			continue;
		}
		itr++;
	}
}

/******************************* RoomType ***************************************
 *
 ********************************************************************************/
void
Room::SetType(RoomType roomType)
{
	roomType_ = roomType;
	if (roomType_ == RoomType::PCRTestRoom)
	{
		/*
		CharMsgDispatcher::Instance()->Register(CharMsgType::PatientRegistered, this);
		// ---> Tell everyone a pcrTestRoom has been built
		CharMsgDispatcher::Instance()->BroadcastMessage(SENDER_ID_IRRELEVANT,
														CharMsgType::PCRTestRoomBuilt,
														this);
														*/
		gameWorld_->AddAction("ActionGotoRoom", this);
		Tester* tester = static_cast<Tester*>(gameWorld_->GetTestEntity(0));
		tester->pOwnedRoom_ = this;
	}

	if(roomType_ == RoomType::WaitingRoom)
	{
		//CharMsgDispatcher::Instance()->Register(CharMsgType::PCRTestRoomBuilt, this);
		gameWorld_->AddAction("ActionGotoRoom", this);
	}

	if (roomType_ == RoomType::XRayRoom)
	{
		gameWorld_->AddAction("ActionGotoRoom", this);
	}

	// TODO: stop using if else since all the rooms' ActionGotoRoom are the same
	if (roomType_ == RoomType::Pharmacy)
	{
		gameWorld_->AddAction("ActionGotoRoom", this);
	}

	if (roomType_ == RoomType::Ward)
	{
		gameWorld_->AddAction("ActionGotoRoom", this);
	}
}

/******************************* GetType ****************************************
 *
 ********************************************************************************/
RoomType
Room::GetType() const
{
	return roomType_;
}

/******************************* Update ****************************************
 *
 ********************************************************************************/
void
Room::Update(real32 dt)
{
	if (roomType_ == RoomType::PCRTestRoom)
	{
		/*
		if (gameWorld_->GetRegisteredPatients().size() > 0)
		{
		}
		*/
	}
}

/******************************* SetOwner ****************************************
 *
 ********************************************************************************/
void
Room::SetOwner(TestEntity* testEntity)
{
	pOwner_ = testEntity;
}

/******************************* AddToQueue *************************************
 *
 ********************************************************************************/
void
Room::AddToQueue(TestEntity* testEntity)
{
	queue_.push(testEntity);
	switch (roomType_)
	{
		case RoomType::PCRTestRoom:
		{
			/*
			if (pOwner_ &&
				IsInRoom(pOwner_))
			{
			}
			*/

			if (!bOccupied_)
			{
				// notify first patient in queue to come in
				//CharMsgType::RoomReady
				GameTime zeroGameTime(0, 0, 0);
				CharMsgDispatcher::Instance()->DispatchMessage(zeroGameTime,
															   roomID_,
															   testEntity->GetID(),
															   CharMsgType::RoomReady,
															   nullptr);

			}
		} break;
	}
}

/******************************* IsInRoom ***************************************
 *
 ********************************************************************************/
bool
Room::IsInRoom(TestEntity* testEntity)
{
	for(Tile* tile : tiles_)
	{ 
		if (testEntity->GetTilepos() == vec2i(tile->pos.x, tile->pos.y))
		{
			return true;
		}
	}
	return false;
}


/******************************* GetRandomEmptyPos ******************************
 ********************************************************************************/
vec2f
Room::GetRandomEmptyPos()
{
	int32 randTileIdx = Math::RandInRange(0, tiles_.size() - 1);
	Tile* randTile = tiles_.at(randTileIdx);
	while (randTile->tileType != TileType::Foundation &&
		   randTile->tileType != TileType::Floor)
	{
		randTileIdx = Math::RandInRange(0, tiles_.size() - 1);
		randTile = tiles_.at(randTileIdx);
	}
	vec2f posInRoom = vec2f(randTile->pos.x * TILE_SIZE, randTile->pos.y * TILE_SIZE);
	return posInRoom;
}

/******************************* Messaging **************************************
 *
 ********************************************************************************/
void
Room::OnBroadcast(Telegram telegram)
{
	// ---> PCRTestRoom
	if (roomType_ == RoomType::PCRTestRoom)
	{
		if (telegram.type == CharMsgType::PatientRegistered)
		{
			TestEntity* testEntity = static_cast<TestEntity*>(telegram.extraInfo);
			AddToQueue(testEntity);
		}
	}
	// ---< PCRTestRoom

	// ---> Waiting Room
	if (roomType_ == RoomType::WaitingRoom)
	{
		if (telegram.type == CharMsgType::PCRTestRoomBuilt)
		{
			Room* pcrTestRoom = static_cast<Room*>(telegram.extraInfo);
			TestEntity* patient = static_cast<TestEntity*>(gameWorld_->GetRegisteredPatientFromQueue());
			pcrTestRoom->AddToQueue(patient);
		}
	}
	// ---< Waiting Room
}

void
Room::RegisterForMsgType(CharMsgType type)
{
	CharMsgDispatcher::Instance()->Register(type, this);
}

/******************************* NumEntities ************************************
 *
 ********************************************************************************/
int32
Room::NumEntities(const string& entityName)
{
	int32 count = 0;

	if (entities_.size() == 0)
		return count;

	for (EntityID entityID : entities_)
	{
		string name = gameWorld_->GetEntityName(entityID);
		if (name.find(entityName) != std::string::npos)
			count++;
	}
	return count;
}

/******************************* AddEntityToRoom ********************************
 *
 ********************************************************************************/
void
Room::AddEntityToRoom(int32 entityID)
{
	entities_.push_back(entityID);
	int32 a = 100;
}

/******************************* IsValidOfRoomType ******************************
 *
 ********************************************************************************/
bool
Room::IsValidRoomOfType(RoomType roomType)
{
	// a room must have a door
	/*
	if (!HasDoor())
		return false;
		*/
	switch (roomType)
	{
		case RoomType::WaitingRoom:
		{
			if (NumEntities("ReceptionDesk") > 0)
				return true;
		} break;

		case RoomType::PCRTestRoom:
		{
			if (NumEntities("PCRTestMachine") > 0 &&
				NumEntities("SwabBox") > 0)
				return true;
		} break;

		case RoomType::XRayRoom:
		{
			if (NumEntities("XRayMachine") > 0)
				return true;
		} break;

		case RoomType::Pharmacy:
		{
			if (NumEntities("DrugCabinet") > 0)
				return true;
		} break;

		case RoomType::Ward:
		{
			if (NumEntities("Bed") > 0)
				return true;
		} break;

		default:
		{
		} break;
	}

	return false;
}

/******************************* PopQueue ****************************************
 *
 ********************************************************************************/
void
Room::PopQueue()
{
}

/******************************* SetOccupied ************************************
 *
 ********************************************************************************/
void
Room::SetOccupied(bool value)
{
	bOccupied_ = value;
}
