#pragma once

enum class Direction{ Up = 0, Down, Left, Right };
enum class Origin{ TopLeft = 0, BottomLeft, MidBottom, AbsCenter, };
