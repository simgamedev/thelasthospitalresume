#include "StateGame.h"
#include "StateManager.h"
#include "TextureManager.h"
#include "StateLoading.h"

#include <sstream>
#include <iomanip>


// TODO: important to include windows.h before include tbb.h
//#include <tbb/tbb.h> // TBB
//#include <tbb/tick_count.h>
//#include <tbb/parallel_for_each.h>
//#include <intrin.h> // SIMD
#include "Messaging/CharMsgDispatcher.h"


#include "MyGUI/GameTimeUserControl.h"
#include "MyGUI/ButtonsBarUserControl.h"
#include "MyGUI/WindowBuild.xaml.h"
#include "MyGUI/WindowDebug.xaml.h"
#include "MyGUI/WindowItem.xaml.h"
#include "MyGUI/StateGameViewModel.h"
#include "MyGUI/HUDResourcesBar.xaml.h"
#include "MyGUI/Tile.h"
#include "MyGUI/MultiTextConverter.h"

// ---> NoesisGUI
#include "MyGUI/GamePlayGUI.xaml.h"
//#include "MyGUI/StateGameViewModel.h"
// ---< NoesisGUI



/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
StateGame::StateGame(StateManager* stateManager)
	: BaseState(stateManager)
	, m_bDEBUGDrawNavGraph(false)
	, m_bDEBUGDrawGrid(false)
	, pathManager_(nullptr)
	, pSelectedCharacter_(nullptr)
	, selectedCharID_(0)
	, mouseOverCharID_(-1)
	, subMode_(SubMode::Normal)
	, pTempRoom_(nullptr)
	, bKey1Pressed_(false)
	, pGameStateData_(nullptr)
	, entityUnderMouse_(-1)
	, countGreen_(0)
	, countRed_(0)
	, bMLBFirstDownOnGUI_(false)
	, bMouseWasForcedToRelease_(false)
	, mouseDraggingDirection_(MouseDraggingDirection::None)
	, bIsMouseDragging_(false)
	// FIXME: find a proper place to init all game data
	, tileUnderMouse_(nullptr)
	, oldTileUnderMouse_(nullptr)
	, oldEntityUnderMouse_(-1)
	// FIXME: find a proper place to init all DEBUG game data/state
	, DEBUG_bTintEntityUnderMouse_(false)
	, mouseDraggingMode_(MouseDraggingMode::Disabled)
	, gizmoFootstep_(nullptr)
	, highlightedEntityID_(-1)
{
	// TODO: FIXME
	//CharMsgDispatcher::Instance()->SetStateGame(this);
	CharMsgDispatcher::Instance()->SetStateGame(this);

	tempEntityTilePos_ = vec2i(-1, -1);
}


/********************************************************************************
 * Empty Destructor.
 ********************************************************************************/
StateGame::~StateGame() {}


/********************************************************************************
 * Called after the state gets created.
 ********************************************************************************/
void
StateGame::OnCreate()
{
	// ---> Init Pointers
	pTextureManager_ = stateManager_->GetContext()->textureManager;
	pEntityManager_ = stateManager_->GetContext()->entityManager;
	pSystemManager_ = stateManager_->GetContext()->systemManager;
	pSRenderer_ = pSystemManager_->GetSystem<SRenderer>(SystemType::Renderer);
	pSWall_ = pSystemManager_->GetSystem<SWall>(SystemType::Wall);
	// ---< Init Pointers
	
	// ---> TEMP
	stateManager_->GetContext()->fontManager->RequireResource("Main");
	// ---< TEMP
	// TODO: BUG: FIXME: "Waiting for tileset to load!"
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->AddCallback(GameStateType::Game, "Key_Escape", &StateGame::EscPressed, this);
	eventManager->AddCallback(GameStateType::Game, "Mouse_Left", &StateGame::OnMouseLeft, this);
	eventManager->AddCallback(GameStateType::Game, "Mouse_Left_Release", &StateGame::OnMouseLeftRelease, this);
	eventManager->AddCallback(GameStateType::Game, "Mouse_Right", &StateGame::OnMouseRight, this);
	eventManager->AddCallback(GameStateType::Game, "Mouse_Right_Release", &StateGame::OnMouseRightRelease, this);
	eventManager->AddCallback(GameStateType::Game, "Mouse_Moved", &StateGame::OnMouseMoved, this);
	eventManager->AddCallback(GameStateType::Game, "Mouse_Wheel", &StateGame::OnMouseWheel, this);
	eventManager->AddCallback(GameStateType::Game, "Player_MoveUp", &StateGame::PlayerMove, this);
	eventManager->AddCallback(GameStateType::Game, "Player_MoveDown", &StateGame::PlayerMove, this);
	eventManager->AddCallback(GameStateType::Game, "Player_MoveLeft", &StateGame::PlayerMove, this);
	eventManager->AddCallback(GameStateType::Game, "Player_MoveRight", &StateGame::PlayerMove, this);
	eventManager->AddCallback(GameStateType::Game, "Key_Tab_Pressed", &StateGame::OnTabKeyPressed, this);
	eventManager->AddCallback(GameStateType::Game, "Key_1_Pressed", &StateGame::Key1Pressed, this);

	TextureManager* textureManager = stateManager_->GetContext()->textureManager;



	// ---> LOAD GameWorld
	pGameWorld_ = new GameWorld(stateManager_->GetContext()->window);
	/// @todo replace setStateManager with a proper process
	pGameWorld_->SetStateManager(stateManager_);
	pGameWorld_->AddFile(Utils::GetMediaDirectory() + "Maps/Map4.map");
	// TODO: buggy?
	SMovement* sysMove = stateManager_->GetContext()->systemManager->GetSystem<SMovement>(SystemType::Movement);
	SCollision* sysCollision = stateManager_->GetContext()->systemManager->GetSystem<SCollision>(SystemType::Collision);
	sysMove->SetMap(pGameWorld_);
	sysCollision->SetMap(pGameWorld_);
	pSCollision_ = sysCollision;
	// ---< LOAD
	// ---> GOAP
	worldState_ = new GOAP::WorldState("InitialWorldState");
	pGameStateData_ = new GameStateData();
	// ---< GOAP

	// ---> ECS add real entity
	//playerID_ = stateManager_->GetContext()->entityManager->AddEntity("Base");
	//playerID_ = stateManager_->GetContext()->entityManager->AddEntity("Doctor");
	/*
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	CSprite* doctorCSprite = entityManager->GetComponent<CSprite>(playerID_, ComponentType::Sprite);
	doctorCSprite->GetSprite()->SetAnimation("Walk", true, true);
	*/
	// ---< ECS

	
	// ---> Init Debug Stuff.
	DEBUGInitDebugStuff();
	// ---< Init Debug Stuff

	// ---> Steering Behaviors
	FontManager* fontManager = stateManager_->GetContext()->fontManager;
	// ---< 


	// ---> GamePlay GUI
	/*
	gameplayGUI_ = new GamePlayGUI(stateManager_->GetContext()->guiManager,
								   stateManager_->GetContext()->eventManager,
								   this);
								   */
	vec2u windowSize = stateManager_->GetContext()->window->GetSize();
	pausedOverlay_.setSize(vec2f(windowSize.x, windowSize.y));
	pausedOverlay_.setFillColor(sf::Color(0, 0, 0, 150));
	pausedOverlay_.setPosition(vec2f(0.0f, 0.0f));
	// ---<


	// ---> Load Saved Game(Continue)
	/*
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	auto savedEntities = Utils::GetFileList(Utils::GetMediaDirectory() + 
											"SavedEntities/" ,"*.entity");
	for (auto& itr : savedEntities)
	{
		entityManager->LoadEntity(itr.first);
	}
	LoadDoors();
	*/
	//LoadWalls();
	//LoadFloors();
	// ---<

	// ---> Path Finder
	//InitializeNodesForPathFinder();
	//pPathFinder_ = new PathFinder(this);
	// ---<

	//---> Real Path Finding
	pathManager_ = new PathManager<PathPlanner>(10000);
	//---<

	// ---> Init IMGUI
	// ---<


	// ---> Modify
	clock_ = Clock::Instance();
	// character manager
	charMgr_ = new CharacterManager(this);
	// ---< Modify


	// ---> Cursor/gizmos
	m_pCursorSprite = new sf::Sprite();
	textureManager->RequireResource("cursor_hammer");
	textureManager->RequireResource("cursor_pointhand");
	textureManager->RequireResource("cursor_normal");
	textureManager->RequireResource("cursor_foundation");
	m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_normal"));
	m_pCursorSprite->setScale(vec2f(2.0f, 2.0f));
	m_DefaultView = view_;

	pTextureManager_->RequireResource("gizmo_highlighted_entity_ul");
	pTextureManager_->RequireResource("gizmo_highlighted_entity_ur");
	pTextureManager_->RequireResource("gizmo_highlighted_entity_bl");
	pTextureManager_->RequireResource("gizmo_highlighted_entity_br");
	gizmoHighlightedEntityUL_ = new sf::Sprite();
	gizmoHighlightedEntityUR_ = new sf::Sprite();
	gizmoHighlightedEntityBL_ = new sf::Sprite();
	gizmoHighlightedEntityBR_ = new sf::Sprite();
	gizmoHighlightedEntityUL_->setOrigin(vec2f(8.0f, 9.0f));
	gizmoHighlightedEntityUR_->setOrigin(vec2f(0.0f, 9.0f));
	gizmoHighlightedEntityBL_->setOrigin(vec2f(8.0f, 0.0f));
	gizmoHighlightedEntityBR_->setOrigin(vec2f(0.0f, 0.0f));
	gizmoHighlightedEntityUL_->setTexture(*textureManager->GetResource("gizmo_highlighted_entity_ul"));;
	gizmoHighlightedEntityUR_->setTexture(*textureManager->GetResource("gizmo_highlighted_entity_ur"));;
	gizmoHighlightedEntityBL_->setTexture(*textureManager->GetResource("gizmo_highlighted_entity_bl"));;
	gizmoHighlightedEntityBR_->setTexture(*textureManager->GetResource("gizmo_highlighted_entity_br"));;
	// ---<

	// ---> Music
	pAudioManager_ = stateManager_->GetContext()->audioManager;
	pAudioManager_->PlayMusic("GamePlayBgMusic1", 20.0f, true);
	// ---< Music

	// ---> temp
	textureManager->RequireResource("FloorPiece_04");
	// ---< temp

	LOG_ENGINE_INFO("StateGame Created!");

	// ---> LOADING State
	auto loadingState = stateManager_->GetState<StateLoading>(GameStateType::Loading);
	loadingState->AddFileLoader(pGameWorld_);
	//loadingState->SetFinishedCallback(&GameWorld::InitNavGraph, gameWorld_);
	loadingState->SetFinishedCallback(&StateGame::LoadingFinishedCallback, this);
	loadingState->SetManualContinue(true);
	// ---< LOADING

	// TODO: Conver between meters/pixels
	view_.setSize(960, 540);
	zoom_ = 0.5;
	view_.setCenter(vec2f(500.0f, 280.0f));

	// ---> entities under mouse
	// ---< entities under mouse
}



/******************************* OnMouseLeft ************************************
 * left moust button down
 ********************************************************************************/
void
StateGame::OnMouseLeft(EventDetails* eventDetails)
{
	if (spGamePlayGUI_->IsMouseOverAnyControl())
	{
		bMLBFirstDownOnGUI_ = true;
		return;
	}
	bMLBFirstDownOnGUI_ = false;

	isMouseLeftDown_ = true;
	switch (subMode_)
	{
		case SubMode::BuyEntity:
		{
			if (tempEntityID_ != -1)
			{
				if (countRed_ == 0)
				{
					//UpdateWorldState(tempEntityID_);
					//tempEntityID_ = -1;
				}
				else
				{
					//pAudioManager_->Play("Error");
				}
				/*
				EntityManager* entityManager = stateManager_->GetContext()->entityManager;
				SCollision* sysCollision = stateManager_->GetContext()->systemManager->GetSystem<SCollision>(SystemType::Collision);
				if (sysCollision->EntityCollidesWithAnyOtherEntity(tempEntityID_).size() == 0 &&
					sysCollision->EntityCollidesWithWalls(tempEntityID_).size() == 0)
				{
					UpdateWorldState(tempEntityID_);
					tempEntityID_ = -1;
					std::cout << "Temp Entity Placed" << std::endl;
				}
				*/
			}
		} break;

		case SubMode::BuildWall:
		{
		} break;
	}
	mouseFirstDownPos_ = mousePos_;
	mouseFirstDownWorldPos_ = mouseWorldPos_;
	mouseFirstDownTilePos_ = mouseTilePos_;

	//WorldToScreenPos(mouseWorldPos_);
}
void
StateGame::OnMouseLeftRelease(EventDetails* eventDetails)
{
	// ---> Handling Mouse State Edge Cases
	// TODO: Abstract mouse to a singleton class?
    if(bMLBFirstDownOnGUI_)
	{
		ResetMouseLeft();
		return;
	}

	if (bMouseWasForcedToRelease_)
	{
		bMouseWasForcedToRelease_ = false;
		ResetMouseLeft();
		return;
	}
	// ---< Handling Mouse State Edge Cases


	switch(subMode_)
	{
		// ---> Buy Entity
		case SubMode::BuyEntity:
		{
			ConfirmTempEntity();
		} break;
		// ---< Buy Entity
		// ---> Move Entity
		case SubMode::MoveEntity:
		{
			// ---> Select an entity to move
			if (tempEntityID_ == -1 &&
				highlightedEntityID_ != -1)
			{
				MakeEntityTemp(highlightedEntityID_);
			}
			// ---< Select an entity to move
			// ---> Place moved entity 
			else
			{
				ConfirmTempEntity();
			}
			// ---< Place moved entity
		} break;
		// ---< Move Entity
		// ---> Build Wall
		case SubMode::BuildWall:
		{
			// TODO: think of a better name than countRed_
			if (countRed_ > 0)
			{
				LOG_ENGINE_ERROR("countRed_:" + STR(countRed_));
				pAudioManager_->Play("Error",
									vec3f(0.0f, 0.0f, 0.0f),
									false,
									false);
				ResetMouseLeft();
				return;
			}
			for (auto& itr : tempTilePlaceHolders_)
			{
				pSWall_->BuildWall(itr.tilepos.x, itr.tilepos.y,
								   pGameWorld_);
				tempTilePlaceHolders_.clear();
			}
			pAudioManager_->Play("WallBuilt",
								vec3f(0.0f, 0.0f, 0.0f),
								false,
								false);
			pSWall_->CalculateWallShapes();
			//std::cout << "Num Walls: " << pSWall_->GetNumEntities() << std::endl;
		} break;
		// ---<

		// ---> Lay Floor
		case SubMode::LayFloor:
		{
			if (countRed_ > 0)
			{
				LOG_ENGINE_ERROR("countRed_:" + STR(countRed_));
				pAudioManager_->Play("Error",
									vec3f(0.0f, 0.0f, 0.0f),
									false,
									false);
				ResetMouseLeft();
				return;
			}
			vec3i from(tempTilePlaceHolders_[0].tilepos.x,
					   tempTilePlaceHolders_[0].tilepos.y,
					   0);
			vec3i to(tempTilePlaceHolders_[tempTilePlaceHolders_.size() - 1].tilepos.x,
					 tempTilePlaceHolders_[tempTilePlaceHolders_.size() - 1].tilepos.y,
					   0);
			for (auto& itr : tempTilePlaceHolders_)
			{
				std::cout << "Should do it only once" << std::endl;
				LayFloor(itr.tilepos.x, itr.tilepos.y,
						 floorTileSetName_,
						 floorTileID_);
			}
			pAudioManager_->Play("FloorLaid",
								vec3f(0.0f, 0.0f, 0.0f),
								false,
								false);
			pGameWorld_->Redraw(from, to);
			tempTilePlaceHolders_.clear();
		} break;
		// ---<
		// ---> Build Foundation
		case SubMode::BuildFoundation:
		{
			// ---> Failed to build foundation
			if (countRed_ > 0)
			{
				pAudioManager_->Play("Error",
									vec3f(mouseWorldPos_.x, mouseWorldPos_.y, 0.0f));
				tempTilePlaceHolders_.clear();
				ResetMouseLeft();
				return;
			}
			// ---< Failed to build foundation
			vec3i from(tempTilePlaceHolders_[0].tilepos.x,
					   tempTilePlaceHolders_[0].tilepos.y,
					   0);
			vec3i to(tempTilePlaceHolders_[tempTilePlaceHolders_.size() - 1].tilepos.x,
					 tempTilePlaceHolders_[tempTilePlaceHolders_.size() - 1].tilepos.y,
					   0);
			for (auto& itr : tempTilePlaceHolders_)
			{

				// Set tile to TileType::Foundation
				pGameWorld_->GetTileMap()->SetTile(itr.tilepos.x,
												  itr.tilepos.y,
												  0,
												  "Building_00",
												  0);
				Tile* tile = pGameWorld_->GetTile(itr.tilepos.x, itr.tilepos.y, 0);
				tile->tileType = TileType::Foundation;
			}
			pAudioManager_->Play("FoundationPlaced",
								vec3f(mouseWorldPos_.x, mouseWorldPos_.y, 0.0f));
			// ---> Cost
			real32 currentCash = pGameStateData_->GetBasicInfo()->GetCash();
			pGameStateData_->GetBasicInfo()->SetCash(currentCash - tempCost_);
			// ---< Cost
			pGameWorld_->Redraw(from, to);
			tempTilePlaceHolders_.clear();
		} break;
		// ---<
		// ---> Bulldozing
		case SubMode::Bulldozing:
		{
			// ---> Mouse is not dragging
			if (!bIsMouseDragging_)
			{
				if (countRed_ > 0)
				{
					pAudioManager_->Play("Error",
										 vec3f(0.0f, 0.0f, 0.0f),
										 false,
										 false);
					ResetMouseLeft();
					return;
				}
				// ---> First bulldoze tiles(floors/foundations)
				for (auto itr : tempTilePlaceHolders_)
				{
					Tile* tile = pGameWorld_->GetTile(itr.tilepos);
					if (tile->tileType == TileType::Wall ||
						entityUnderMouseName_.find("Wall") != std::string::npos)
						continue;
					if (tile->tileType == TileType::Foundation)
					{
						// set tile to TileType::Empty
						pGameWorld_->GetTileMap()->SetTile(tile->pos.x,
														   tile->pos.y,
														   0,
														   "Exterior_00",
														   6);
						tile->tileType = TileType::Empty;
						tile->roomID = 0;
						pGameWorld_->Redraw(tile->pos,
											tile->pos);
					}
					else if (tile->tileType == TileType::Floor)
					{
						// set tile back to TileType::Foundation
						pGameWorld_->GetTileMap()->SetTile(tile->pos.x,
														   tile->pos.y,
														   0,
														   "Building_00",
														   0);
						tile->tileType = TileType::Foundation;
						pGameWorld_->Redraw(tile->pos,
											tile->pos);
					}
				}
				tempTilePlaceHolders_.clear();
				// ---< First bulldoze tiles(floors/foundations)
				// ---> Then bulldoze doors
				for (int32 doorID : doorsToBulldoze_)
				{
					vec2i doorTilePos = pEntityManager_->GetEntityTilePos(doorID);
					pSWall_->RemoveDoor(doorTilePos.x, doorTilePos.y, pGameWorld_);
				}
				doorsToBulldoze_.clear();
				// ---< Then bulldoze doors
				// ---> Then bulldoze walls
				for (int32 wallID : wallsToBulldoze_)
				{
					vec2i wallTilePos = pEntityManager_->GetEntityTilePos(wallID);
					pSWall_->BulldozeWall(wallTilePos.x, wallTilePos.y,
										  pGameWorld_, true);
				}
				wallsToBulldoze_.clear();
				// ---< Then bulldoze walls
				pAudioManager_->Play("Bulldozed",
									 vec3f(mouseWorldPos_.x, mouseWorldPos_.y, 0.0f));
			}
			// ---< Mouse is not dragging
			// ---> Mouse is dragging
			else
			{
				for (auto itr : tempTilePlaceHolders_)
				{
					Tile* tile = pGameWorld_->GetTile(itr.tilepos);
					if (tile->tileType == TileType::Empty)
						continue;
					if (tile->tileType == TileType::Foundation ||
						tile->tileType == TileType::Floor)
					{
						// set tile to TileType::Empty
						pGameWorld_->GetTileMap()->SetTile(tile->pos.x,
														   tile->pos.y,
														   0,
														   "Exterior_00",
														   6);
						tile->tileType = TileType::Empty;
						tile->roomID = 0;
					}
					else if (tile->tileType == TileType::Wall)
					{
						// first bulldoze wall
						pSWall_->BulldozeWall(tile->pos.x, tile->pos.y, pGameWorld_);
						// then bulldoze foundation/floor
						pGameWorld_->GetTileMap()->SetTile(tile->pos.x,
														   tile->pos.y,
														   0,
														   "Exterior_00",
														   6);
						tile->tileType = TileType::Empty;
						tile->roomID = 0;
					}
				}
				wallsToBulldoze_.clear();
				vec3i from = vec3i(mouseDraggingXRange_.x,
								   mouseDraggingYRange_.x,
								   0);
				vec3i to = vec3i(mouseDraggingXRange_.y,
								 mouseDraggingYRange_.y,
								 0);
				pGameWorld_->Redraw(from, to);
				pSWall_->CalculateWallShapes();
				std::cout << "tempTilePlaceHolders_.size() = " << tempTilePlaceHolders_.size() << std::endl;
				// ---< Then bulldoze walls
				pAudioManager_->Play("Bulldozed");
			}
			// ---< Mouse is dragging
		} break;
		// ---< Bulldozing

		// ---> Add Door
		case SubMode::AddDoor:
		{
			SWall* sysWall = stateManager_->GetContext()->systemManager->GetSystem<SWall>(SystemType::Wall);
			if (sysWall->AddDoor(tempEntityFilePath_, mouseTilePos_.x, mouseTilePos_.y))
			{
				std::cout << "Door Added" << std::endl;
				sysWall->CalculateWallShapes();
				pGameWorld_->AddBackToNavGraph(vec2i(mouseTilePos_.x, mouseTilePos_.y));
				pAudioManager_->Play("DoorPlaced",
									vec3f(0.0f, 0.0f, 0.0f),
									false,
									false);
				ChangeSubMode(SubMode::Normal);
			}
			else
			{
				std::cout << "Can't Add Door Here" << std::endl;
				pAudioManager_->Play("Error",
									vec3f(0.0f, 0.0f, 0.0f),
									false,
									false);
			}
		} break;
		// ---<

		// ---> Assign Room
		case SubMode::AssignRoom:
		{
			if (pTempRoom_)
			{
				pTempRoom_->SetType(tempRoomType_);
			}
			tempRoomTileOverlays_.clear();
			ChangeSubMode(SubMode::Normal);
		} break;
		// ---< Assign Room

		// ---> Normal
		case SubMode::Normal:
		{
			if (mouseOverCharID_ != -1)
			{
				selectedCharID_ = mouseOverCharID_;
				pSelectedCharacter_ = charMgr_->GetCharacterFromID(selectedCharID_);
			}
		} break;
		// ---< Normal

		// ---> ChangeWallpaper
		case SubMode::ChangeWallpaper:
		{
			if (wallsToChangeWallpaper_.size() == 0)
			{
				pAudioManager_->Play("Error",
									 vec3f(0.0f, 0.0f, 0.0f),
									 false,
									 false);
				break;
			}
			for (int32 wallID : wallsToChangeWallpaper_)
			{
				pEntityManager_->UntintEntity(wallID);
				vec2i wallTilePos = pEntityManager_->GetEntityTilePos(wallID);
				pSWall_->ChangeWallpaper(wallTilePos.x, wallTilePos.y, pGameWorld_);
			}
			wallsToChangeWallpaper_.clear();
			pAudioManager_->Play("WallpaperChanged",
								 vec3f(0.0f, 0.0f, 0.0f),
								 false,
								 false);
		} break;
		// ---< ChangeWallpaper
		default:
		{
		} break;
		// ---<
	}


	tempCostText_->setString("");
	tempTilePlaceHolders_.clear();

	ResetMouseLeft();
}

/********************************************************************************
 * Called after the state gets destroy.
 ********************************************************************************/
void
StateGame::OnDestroy()
{
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->RemoveCallback(GameStateType::Game, "Key_Escape");
	eventManager->RemoveCallback(GameStateType::Game, "Key_P");
}

/********************************************************************************
 * Update.
 ********************************************************************************/
void
StateGame::Update(const sf::Time& time)
{
	// ---> Modify
	clock_->Update(time);
	//gameplayGUI_->Update(time);
	CharMsgDispatcher::Instance()->Update(time);
	// ---< Modify

	if(subMode_ != SubMode::Paused)
	{
		UpdateMouse();

		if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
		{
			Pan();
		}

		// ---> Update ECS
		stateManager_->GetContext()->systemManager->Update(time.asSeconds());
		// ---<


		// ---> Update Characters
		charMgr_->Update(time);
		// ---< Update Characters
	}

	// ---> IMGUI
	// ---<

	//---> Pathfinding
	if (pathManager_ != nullptr)
	{
		pathManager_->UpdateSearches();
	}
	//---<

	// ---> gizmoFootstep
	//UpdateTempEntityGizmo();
	// ---< gizmoFootstep
}


/********************************************************************************
 * Draw.
 ********************************************************************************/
void
StateGame::Draw()
{
	if (bKey1Pressed_)
	{
		Window* window = stateManager_->GetContext()->window;
		sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();
		// <--- Draw Map
		if (pGameWorld_->DoneRedraw())
		{
			pGameWorld_->Draw(0);
			pGameWorld_->Draw(1);
		}
		stateManager_->GetContext()->systemManager->Draw(stateManager_->GetContext()->window, 0);
		stateManager_->GetContext()->systemManager->Draw(stateManager_->GetContext()->window, 1);
		stateManager_->GetContext()->systemManager->Draw(stateManager_->GetContext()->window, 2);
		//gameMap_->Draw(2);
		//gameMap_->Draw(3);
		// --->

		// ---> Draw characters
		charMgr_->Draw(window);
		// ---< Draw Characters


		// ---> Temp Wall/Temp Foundation/Temp Bulldozing Layout
		switch (subMode_)
		{
			case SubMode::BuildFoundation:
			case SubMode::LayFloor:
			case SubMode::BuildWall:
			case SubMode::Bulldozing:
			{
				DrawTempTileLayout(window);
			} break;
		}
		// ---< Temp Wall/Temp Foundation/Temp Bulldozing Layout

		DrawDebugStuff();

		// ---> Paused
		if (subMode_ == SubMode::Paused)
		{
			renderWindow->draw(pausedOverlay_);
		}
		// ---<
		// ---> draw temp room tiles overlay
		if (subMode_ == SubMode::AssignRoom)
		{
			for (auto& overlay : tempRoomTileOverlays_)
			{
				renderWindow->draw(overlay);
			}
		}
		// ---< draw temp room tiles overlay

		// ---> Draw highlighted entity gizmo
		if (subMode_ == SubMode::MoveEntity ||
			subMode_ == SubMode::SellEntity ||
			subMode_ == SubMode::InspectEntity)
		{
			if (entityUnderMouse_ != -1 &&
				entityUnderMouseName_.find("Wall") == std::string::npos &&
				entityUnderMouseName_.find("Door") == std::string::npos &&
				tempEntityID_ == -1)
			{
				window->DrawGizmo(gizmoHighlightedEntityUL_);
				window->DrawGizmo(gizmoHighlightedEntityUR_);
				window->DrawGizmo(gizmoHighlightedEntityBL_);
				window->DrawGizmo(gizmoHighlightedEntityBR_);
			}
		}
		// ---< Draw highlighted entity gizmo

		// ---> Draw Temp Entity Collision
		if (tempEntityID_ != -1)
		{
			// ---> draw gizmoFootstep_
			if (gizmoFootstep_)
				window->DrawGizmo(gizmoFootstep_);
			// ---< draw gizmoFootstep_
			/*
			SCollision* sysCollision = stateManager_->GetContext()->systemManager->GetSystem<SCollision>(SystemType::Collision);
			std::vector<sf::FloatRect> tempEntityCollisions = sysCollision->EntityCollidesWithAnyOtherEntity(tempEntityID_);
			for (sf::FloatRect collision : tempEntityCollisions)
			{
				sf::RectangleShape collisionDrawable;
				collisionDrawable.setPosition(collision.left, collision.top);
				collisionDrawable.setSize(vec2f(collision.width, collision.height));
				collisionDrawable.setFillColor(sf::Color(255, 0, 0, 100));
				renderWindow->draw(collisionDrawable);
			}
			std::vector<sf::FloatRect> tempEntityWallCollisions = sysCollision->EntityCollidesWithWalls(tempEntityID_);
			for (sf::FloatRect collision : tempEntityWallCollisions)
			{
				sf::RectangleShape collisionDrawable;
				collisionDrawable.setPosition(collision.left, collision.top);
				collisionDrawable.setSize(vec2f(collision.width, collision.height));
				collisionDrawable.setFillColor(sf::Color(255, 0, 0, 100));
				renderWindow->draw(collisionDrawable);
			}
			*/
		}

		// ---> draw imgui
		// ---< draw imgui

		// ---> draw cursor
		// TODO: fix changing sf:View, somehow characters are drawn with cursor view
		//renderWindow->setView(m_DefaultView);
		window->DrawCursor(m_pCursorSprite);
		//renderWindow->draw(*m_pCursorSprite);
		// ---< draw cursor
		// ---> draw tempCostText_
		if (tempCostText_)
		{
			tempCostText_->setPosition(vec2f(mousePos_.x + 35, mousePos_.y - 10));
			window->DrawCursor(tempCostText_);
		}
		// ---< draw tempCostText_

		// ---> Draw Temp Grid
		if (subMode_ == SubMode::BuildFoundation ||
			subMode_ == SubMode::LayFloor ||
			subMode_ == SubMode::Bulldozing ||
			subMode_ == SubMode::BuildWall ||
			subMode_ == SubMode::BuyEntity ||
			(subMode_ == SubMode::MoveEntity && tempEntityID_ != -1))
			DrawTempGrid();
		// ---< Draw Temp Grid

		// ---> Draw Temp Wall Grid
		//if (subMode_ == SubMode::BuildWall)
		//	DrawTempWallGrid();
		// ---< Draw Temp Wall Grid


		// ---> Change Wallpaper
		// ---< Change Wallpaper


		// ---> Bulldozing
		if (subMode_ == SubMode::Bulldozing)
		{
			if (!bIsMouseDragging_)
			{
			}
		}
		// ---<
	}
}


/********************************************************************************
 * Key Escape pressed.
 ********************************************************************************/
void
StateGame::EscPressed(EventDetails* eventDetails)
{
    // ---> NoesisGUI
	// Note that several views can be managed in the same render thread just by 
	// initializing all of them with the same render device.That way internal 
	// resources like rampsand glyphs are shared across all views.RenderDevice 
	// is a heavyweight class.Extra instances should be avoided whenever possible.
    //spGamePlayGUI_ = *new GamePlayGUI(this);
    // ---< NoesisGUI
	/*
	switch (subMode_)
	{
		case SubMode::BuyEquipment:
		{
			if (tempEntityID_ != -1)
			{
				EntityManager* entityManager = stateManager_->GetContext()->entityManager;
				entityManager->RemoveEntity(tempEntityID_);
				tempEntityID_ = -1;
			}
			ChangeSubMode(SubMode::Normal);
		} break;

		default:
		{
			ChangeSubMode(SubMode::Normal);
		} break;
	}
	*/
}




/******************************* Activate ***************************************
 *
 ********************************************************************************/
void
StateGame::Activate()
{
	LOG_ENGINE_INFO("StateGame Activated!");
	//gameWorld_->Redraw();
	//LoadFloors();
	//gameClock_.restart();
	//elapsedTime_ = gameClock_.getElapsedTime();
}

/******************************* Deactivate *************************************
 *
 ********************************************************************************/
void
StateGame::Deactivate()
{
	LOG_ENGINE_INFO("StateGame Deactivated!");
}
// Redraw GameMap
void
StateGame::RedrawGameMap()
{
	pGameWorld_->Redraw();
}



/*********************************************************************************
 * Player Move.
 *********************************************************************************/
void
StateGame::PlayerMove(EventDetails* eventDetails)
{
	//std::cout << "Player Should Start Moving!" << std::endl;
	Message msg((MessageType)EntityMessageType::StartMoving);
	if (eventDetails->bindingName == "Player_MoveUp") { msg.intValue = (int32)(Direction::Up); }
	else if (eventDetails->bindingName == "Player_MoveDown") { msg.intValue = (int32)(Direction::Down); }
	else if (eventDetails->bindingName == "Player_MoveLeft") { msg.intValue = (int32)(Direction::Left); }
	else if (eventDetails->bindingName == "Player_MoveRight") { msg.intValue = (int32)(Direction::Right); }
	msg.receiver = playerID_;
	stateManager_->GetContext()->systemManager->GetMessageHandler()->Dispatch(msg);
}
/*********************************************************************************
 * On Mouse Moved.
 *********************************************************************************/
void
StateGame::OnMouseMoved(EventDetails* eventDetails)
{
	if (spGamePlayGUI_->IsMouseOverAnyControl())
	{
		if (isMouseLeftDown_)
		{
			OnMouseLeftRelease(nullptr);
			bMouseWasForcedToRelease_ = true;
		}
		return;
	}

	sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();
	// ---> Update Temp Entity
	if (tempEntityID_ != -1)
	{
		//cPosTempEntity_->SetPosition(mouseWorldPos_);
		cPosTempEntity_->SetPosition(mouseTilePos_.x * TILE_SIZE,
									 (mouseTilePos_.y + 1) * TILE_SIZE);
		if (subMode_ != SubMode::AddDoor)
		{
			vec2i tempEntityTilePos = cPosTempEntity_->GetTilePos();
			vec2i gizmoTilePos = tempEntityTilePos + cColTempEntity_->GetUserOffset();
			gizmoFootstep_->setPosition(gizmoTilePos.x * TILE_SIZE,
										gizmoTilePos.y * TILE_SIZE);
		}

		if (subMode_ == SubMode::BuyEntity ||
			subMode_ == SubMode::MoveEntity)
		{
			std::vector<vec2i> collidingTiles = pSCollision_->CollidesWithStructures(tempEntityID_);
			std::vector<vec2i> moreTiles = pSCollision_->CollidesWithEntities(tempEntityID_);
			for (vec2i tile : moreTiles)
			{
				collidingTiles.push_back(tile);
			}
			//collidingTiles += moreTiles;
			if (collidingTiles.size() > 0)
			{
				//countRed_++;
				SetCountRed(countRed_ + 1);
				pEntityManager_->TintEntity(tempEntityID_, sf::Color::Red);
				gizmoFootstep_->setColor(sf::Color::Red);
			}
			else
			{
				//countRed_ = 0;
				SetCountRed(0);
				pEntityManager_->UntintEntity(tempEntityID_);
				gizmoFootstep_->setColor(sf::Color(255, 255, 255, 255));
			}
		}
		//pEntityManager_->TintEntity(tempEntityID_, sf::Color::Red);
		//cPosTempEntity_->SetPosition(100, 100);
	}
	// ---< 
	// ---> Update mouse dragging
	UpdateMouseDragging();
	// ---< Update mouse dragging
	// ---> Update tile under mouse. TileUnderMouse
	UpdateTileUnderMouse();
	// ---< Update tile under mouse. TileUnderMouse
	// ---> Update entity under mouse.(entityUnderMouse_)
	UpdateEntityUnderMouse();
	// ---< Update entity under mouse.(entityUnderMouse_)
	switch (subMode_)
	{
		// ---> SubMode::AssignRoom
		case SubMode::AssignRoom:
		{
			Tile* tile = pGameWorld_->GetTile(mouseTilePos_.x, mouseTilePos_.y, 0);
			string tileInfoStr("Tile:" + std::to_string(tile->pos.x) + "," +
							   std::to_string(tile->pos.y) + "RoomID: " + std::to_string(tile->roomID));
			Room* room = pGameWorld_->GetRoom(tile->roomID);
			sf::Color tempRoomTileColor = sf::Color(255, 0, 0, 100);
			if (room && room->GetType() != RoomType::Outside)
			{
				if (room->IsValidRoomOfType(tempRoomType_))
					tempRoomTileColor = sf::Color(0, 255, 0, 100);
				pTempRoom_ = room;
				tempRoomTileOverlays_.clear();
				for (Tile* t : room->GetTiles())
				{
					sf::RectangleShape overlay;
					overlay.setFillColor(tempRoomTileColor);
					overlay.setSize(vec2f(TILE_SIZE, TILE_SIZE));
					overlay.setPosition(t->pos.x * TILE_SIZE,
										t->pos.y * TILE_SIZE);
					LOG_GAME_INFO("overlay pos:" + std::to_string(t->pos.x * TILE_SIZE) + ","
								  + std::to_string(t->pos.y * TILE_SIZE));
					LOG_GAME_INFO("overlay size: " + std::to_string(TILE_SIZE) + ","
								  + std::to_string(TILE_SIZE));
					tempRoomTileOverlays_.push_back(overlay);
				}
				LOG_GAME_INFO("Num Overlays: " + std::to_string(tempRoomTileOverlays_.size()));
			}
			else if (room && room->GetType() == RoomType::Outside)
			{
				tempRoomTileOverlays_.clear();
			}
			else
			{
				pTempRoom_ = nullptr;
			}
		} break;
		// ---< SubMode::AssignRoom

		// ---> SubMode::Normal
		case SubMode::Normal:
		{
			sf::FloatRect viewSpace = stateManager_->GetContext()->window->GetViewSpace();
			for (TestEntity* character : charMgr_->GetVisibleCharacters(viewSpace))
			{
				if (character->IntersectsMouse(mouseWorldPos_))
				{
					mouseOverCharID_ = character->GetID();
					character->Tint(sf::Color(0, 255, 0, 100));
				}
				else
				{
					if (mouseOverCharID_ == character->GetID())
						character->UnTint();
				}
			}
		} break;
		// ---< SubMode::Normal

		// ---> SubMode::AddDoor
		case SubMode::AddDoor:
		{
			vec2i tilePos = pEntityManager_->GetEntityTilePos(tempEntityID_);
			string logString("TempEntityTilePos:" + STR(tilePos.x) + "," + STR(tilePos.y));
			LOG_ENGINE_ERROR(logString);
			if (tilePos != tempEntityTilePos_)
			{
				tempEntityTilePos_ = tilePos;
				if (pSWall_->OKToAddDoor(tilePos.x, tilePos.y))
				{
					pEntityManager_->TintEntity(tempEntityID_, sf::Color::Green);
				}
				else
				{
					pEntityManager_->TintEntity(tempEntityID_, sf::Color::Red);
				}
			}
		} break;
		// ---< SubMode::AddDoor

		// ---> SubMode::ChangeWallpaper
		case SubMode::ChangeWallpaper:
		{
			// ---> Mouse Dragging
			if (!bIsMouseDragging_)
			{
				wallsToChangeWallpaper_.clear();
				if (entityUnderMouseName_.find("Wall") != std::string::npos)
				{
					pEntityManager_->TintEntity(entityUnderMouse_, sf::Color::Green);
					wallsToChangeWallpaper_.push_back(entityUnderMouse_);
				}
				pEntityManager_->UntintEntity(oldEntityUnderMouse_);
			}
			// ---> Mouse Dragging
			else
			{
				if (entityUnderMouse_ == -1 ||
					entityUnderMouseName_.find("Wall") == std::string::npos)
				{
					for (int32 wallID : wallsToChangeWallpaper_)
					{
						pEntityManager_->UntintEntity(wallID);
					}
					wallsToChangeWallpaper_.clear();
					break;
				}
				switch (mouseDraggingDirection_)
				{
					case MouseDraggingDirection::Right:
					{
						for (int32 x = mouseDraggingXRange_.x;
							 x <= mouseDraggingXRange_.y;
							 x++)
						{
							int32 firstWallTilePosY = pEntityManager_->GetEntityTilePos(entityUnderMouse_).y;
							int32 wallID = pSWall_->GetWallID(x, firstWallTilePosY);
							if (wallID == -1) continue;
							pEntityManager_->TintEntity(wallID, sf::Color::Green);
							wallsToChangeWallpaper_.push_back(wallID);
						}
					} break;
					case MouseDraggingDirection::Down:
					{
						for (int32 y = mouseDraggingYRange_.x;
							 y <= mouseDraggingYRange_.y;
							 y++)
						{
							int32 firstWallTilePosX = pEntityManager_->GetEntityTilePos(entityUnderMouse_).x;
							int32 wallID = pSWall_->GetWallID(firstWallTilePosX, y);
							if (wallID == -1) continue;
							pEntityManager_->TintEntity(wallID, sf::Color::Green);
							wallsToChangeWallpaper_.push_back(wallID);
						}
					} break;
					default:
					{
					}
				}
			}
			// ---< Mouse Dragging
		}
		// ---< SubMode::ChangeWallpaper
		// ---> SubMode::Bulldozing
		case SubMode::Bulldozing:
		{
			// ---> Mouse is not dragging
			if (!bIsMouseDragging_)
			{
				wallsToBulldoze_.clear();
				doorsToBulldoze_.clear();
				if (entityUnderMouseName_.find("Wall") != std::string::npos)
				{
					pEntityManager_->TintEntity(entityUnderMouse_, sf::Color::Green);
					wallsToBulldoze_.push_back(entityUnderMouse_);
				}
				if (entityUnderMouseName_.find("Door") != std::string::npos)
				{
					pEntityManager_->TintEntity(entityUnderMouse_, sf::Color::Green);
					doorsToBulldoze_.push_back(entityUnderMouse_);
				}
				pEntityManager_->UntintEntity(oldEntityUnderMouse_);
			}
			// ---< Mouse is not dragging
			// ---> Mouse is dragging
			else
			{
			}
			// ---< Mouse is dragging
		} break;
		// ---< SubMode::Bulldozing
		// ---> SubMode::MoveEntity
		case SubMode::MoveEntity:
		{
			if (tempEntityID_ == -1)
				UpdateHighlightedEntityGizmo();
			else
				;
		} break;
		// ---< SubMode::MoveEntity
	}
}

/*********************************************************************************
 * On Mouse Right.
 *********************************************************************************/
void
StateGame::OnMouseRight(EventDetails* eventDetails)
{
	std::cout << "Game Right Click!" << std::endl;
}
void
StateGame::OnMouseRightRelease(EventDetails* eventDetails)
{
}

/*********************************************************************************
 * On Mouse Wheel.
 *********************************************************************************/
void
StateGame::OnMouseWheel(EventDetails* eventDetails)
{
	// TODO: what does zoom's value mean
	if(eventDetails->hasBeenProcessed) { return; }
	real32 factor = 0.05f;
	factor *= eventDetails->mouseWheelDelta;
	factor = 1.0f - factor;
	// factor is whether 1.05 or 0.95
	// NOTE: zoom in mouseWheelDelta is -1
	// NOTE: zoom out mouseWheelDelta is 1
	if (zoom_ < 0.15 && factor < 1)
	{
	}
	else if (zoom_ > 0.8 && factor > 1)
	{
	}
	else
	{
		zoom_ *= factor;
		view_.zoom(factor);
		// ---> prevent zooming beyond border
		vec2u mapSize = pGameWorld_->GetMapSize();
		vec2f worldBottomRightPos = vec2f(mapSize.x * TILE_SIZE,
										  mapSize.y * TILE_SIZE);
		vec2f worldBottomLeftPos = vec2f(0.0f, mapSize.y * TILE_SIZE);
		vec2f worldUpperRightPos = vec2f(mapSize.x * TILE_SIZE, 0.0f);
		vec2i worldBRScrPos = WorldToScreenPos(worldBottomRightPos);
		vec2i worldURScrPos = WorldToScreenPos(worldUpperRightPos);
		vec2i worldBLScrPos = WorldToScreenPos(worldBottomLeftPos);
		vec2i worldOriginScrPos = WorldToScreenPos(vec2f(0.0f, 0.0f));
		if (worldOriginScrPos.x > 0)
		{
			// worldOriginScrPos should == 0
			// and that means
			vec2f viewSize = view_.getSize();
			vec2f viewCenter = view_.getCenter();
			view_.setCenter(vec2f(view_.getCenter().x + worldOriginScrPos.x/zoom_,
								  view_.getCenter().y));
		}
		if (worldURScrPos.x < 1920)
		{
			view_.setCenter(vec2f(view_.getCenter().x - (1920-worldURScrPos.x)/zoom_,
								  view_.getCenter().y));
		}
		if (worldOriginScrPos.y > 0)
		{
			// worldOriginScrPos should == 0
			// and that means
			vec2f viewSize = view_.getSize();
			vec2f viewCenter = view_.getCenter();
			view_.setCenter(vec2f(view_.getCenter().x,
								  view_.getCenter().y + worldOriginScrPos.y / zoom_));
		}
		if (worldBLScrPos.y < 1080)
		{
			view_.setCenter(vec2f(view_.getCenter().x,
								  view_.getCenter().y - (1080 - worldBLScrPos.y) / zoom_));
		}
		// ---< prevent zooming beyond border
	}
}

/******************************* UpdateMouse *************************************
 * FIXME: should rename to UpdateMouseInfo() and call it in OnMouseMoved?
 *********************************************************************************/
void
StateGame::UpdateMouse()
{
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	vec2i newMousePos = eventManager->GetMousePos();
	mouseDifference_ = vec2f(newMousePos - mousePos_);
	mouseDifference_ *= zoom_;
	mousePos_ = newMousePos;

	//sf::View view = window_->GetRenderWindow()->getView();
	vec2f viewPos = view_.getCenter() - (view_.getSize() * 0.5f);

	mouseWorldPos_ = viewPos + (vec2f(mousePos_) * zoom_);
	stateManager_->GetContext()->window->SetMouseWorldPos(mouseWorldPos_);

	vec2i newMouseTilePosition = vec2i(floor(mouseWorldPos_.x / TILE_SIZE),
									   floor(mouseWorldPos_.y / TILE_SIZE));
	if (newMouseTilePosition.x < 0)
		newMouseTilePosition.x = 0;
	if (newMouseTilePosition.y < 0)
		newMouseTilePosition.y = 0;
	if (newMouseTilePosition.x >= pGameWorld_->GetMapSize().x)
		newMouseTilePosition.x = pGameWorld_->GetMapSize().x - 1;
	if (newMouseTilePosition.y >= pGameWorld_->GetMapSize().y)
		newMouseTilePosition.y = pGameWorld_->GetMapSize().y - 1;
	// TODO: secondaryAction?????
	/*
	if(mouseTilePos_ != newMouseTilePosition && isInAction_)
	{
		isInSecondaryAction_ = true;
	}
	*/

	mouseTilePos_ = newMouseTilePosition;

	// ---> Cursor
	m_pCursorSprite->setPosition(vec2f(newMousePos));
	// ---< Cursor
	// ---> ViewModel
	// TODO: if(WINDOW_DEBUG_OPEN) then
	spViewModel_->GetMouseInfo()->SetTilePosX(mouseTilePos_.x);
	spViewModel_->GetMouseInfo()->SetTilePosY(mouseTilePos_.y);
	//TileInfo* tileInfo = tile->tileInfo;
	//bool bHasDoor = sysWall->HasDoor(mouseTilePos_.x, mouseTilePos_.y);
	// ---< ViewModel
}


/******************************* Pan ****************************************
 * Holding RMB and drag to move around.
 ****************************************************************************/
void
StateGame::Pan()
{
	if(mouseDifference_ == vec2f(0.0f, 0.0f))
	{
		return;
	}

	vec2i worldOriginScrPos = WorldToScreenPos(vec2f(0.0f, 0.0f));
	vec2f newCenter = view_.getCenter() - vec2f(mouseDifference_);
	//LOG_GAME_INFO("worldOrignScrPos: " + STR(worldOriginScrPos.x) + "," + STR(worldOriginScrPos.y));
	//LOG_GAME_INFO("mouseDifference_:" + STR(mouseDifference_.x) + "," + STR(mouseDifference_.y));
	vec2u mapSize = pGameWorld_->GetMapSize();
	vec2f worldBottomRightPos = vec2f(mapSize.x * TILE_SIZE,
									  mapSize.y * TILE_SIZE);
	vec2f worldBottomLeftPos = vec2f(0.0f, mapSize.y * TILE_SIZE);
	vec2f worldUpperRightPos = vec2f(mapSize.x * TILE_SIZE, 0.0f);
	vec2i worldBRScrPos = WorldToScreenPos(worldBottomRightPos);
	vec2i worldURScrPos = WorldToScreenPos(worldUpperRightPos);
	vec2i worldBLScrPos = WorldToScreenPos(worldBottomLeftPos);
	//LOG_GAME_INFO("worldURScrPos: " + STR(worldURScrPos.x) + "," + STR(worldURScrPos.y));
	//LOG_GAME_INFO("worldBRScrPos: " + STR(worldBRScrPos.x) + "," + STR(worldBRScrPos.y));
	//LOG_GAME_INFO("worldBLScrPos: " + STR(worldBLScrPos.x) + "," + STR(worldBLScrPos.y));
	//LOG_GAME_INFO("viewCenter: " + STR(view_.getCenter().x) + "," +
	//			  STR(view_.getCenter().y));
	//LOG_GAME_INFO("viewCenter + viewSize = " + STR(view_.getCenter().x +
	//											   view_.getSize().x/2));
	// ---> prevent moving beyond left/right border
	if ((worldOriginScrPos.x > -(32/zoom_) && mouseDifference_.x > 0) ||
		(worldURScrPos.x < (1920 + (32/zoom_)) && mouseDifference_.x < 0))
	{
		//LOG_GAME_ERROR("Trying to move beyond the left/right border");

		if ((worldOriginScrPos.y > -(32/zoom_) && mouseDifference_.y > 0) ||
			(worldBRScrPos.y < (1080 + (16/zoom_)) && mouseDifference_.y < 0))
		{
			newCenter = vec2f(view_.getCenter().x,
							  view_.getCenter().y);
		}
		else
		{
			newCenter = vec2f(view_.getCenter().x,
							  view_.getCenter().y - (real32)mouseDifference_.y);
		}
	}
	// ---<

	// ---> try to move beyond top/bottom border
	else if((worldOriginScrPos.y > -(32/zoom_) && mouseDifference_.y > 0) ||
	   (worldBRScrPos.y < (1080 + (32/zoom_)) && mouseDifference_.y < 0))
	{
		//LOG_GAME_ERROR("Trying to move beyond the top/bottom border");
		if ((worldOriginScrPos.x > -(32/zoom_) && mouseDifference_.x > 0) ||
			(worldURScrPos.x < (1920 + (32/zoom_)) && mouseDifference_.x < 0))
		{
			newCenter = vec2f(view_.getCenter().x,
							  view_.getCenter().y);
		}
		else
		{
			newCenter = vec2f(view_.getCenter().x - (real32)mouseDifference_.x,
							  view_.getCenter().y);
		}
	}
	else
	{
	}
	// ---<
	view_.setCenter(newCenter);

}

/*********************************************************************************
 * Toggle Draw Grid.
 *********************************************************************************/
void
StateGame::ToggleDrawGrid()
{
	int32 a = 100;
	m_bDEBUGDrawGrid = !m_bDEBUGDrawGrid;
}
void
StateGame::ToggleDrawCollidable(EventDetails* eventDetails)
{
	//isDrawCollidable_ = !isDrawCollidable_;
	isDrawCollidable_ = false;
}

/*********************************************************************************
 * Draw Debug Stuff.
 *********************************************************************************/
void
StateGame::DrawDebugStuff()
{
	sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	if (tempEntityID_ != -1)
	{
		SystemManager* sysManager = stateManager_->GetContext()->systemManager;
		SCollision* sysCollision = sysManager->GetSystem<SCollision>(SystemType::Collision);
		sysCollision->DEBUGDrawCollidables(renderWindow);
	}

	DEBUGDrawPanel();
	DEBUGDrawCharacterInfo();
	// draw navgraph
	if (m_bDEBUGDrawNavGraph)
	{
		pGameWorld_->DEBUGDrawNavGraph(renderWindow);
	}
	// draw grid
	if (m_bDEBUGDrawGrid)
	{
		pGameWorld_->DEBUGDrawGrid();
	}
}



/*********************************************************************************
 * Is Tile Walkable.
 *********************************************************************************/
// TODO: move to gameMap?
bool
StateGame::IsTileWalkable(vec2i tilePos)
{
	// has wall
	SWall* sysWall = stateManager_->GetContext()->systemManager->GetSystem<SWall>(SystemType::Wall);
	if (sysWall->HasWall(tilePos.x, tilePos.y))
	{
		std::cout << "Tile is not walkable since it has wall" << std::endl;
		return false;
	}
	// return false
	// is solid
	Tile* layer0Tile = pGameWorld_->GetTile(tilePos.x, tilePos.y, 0);
	Tile* layer1Tile = pGameWorld_->GetTile(tilePos.x, tilePos.y, 1);
	if(layer0Tile)
	{
		if (layer0Tile->solid)
		{
			std::cout << "Tile is not walkable since it is solid" << std::endl;
			return false;
		}
	}
	if (layer1Tile)
	{
		if (layer1Tile->solid)
		{
			std::cout << "Tile is not walkable since it is solid" << std::endl;
			return false;
		}
	}
	// return false
	// return true
	return true;
}

/*********************************************************************************
 * Init Debug Stuff.
 *********************************************************************************/
void
StateGame::DEBUGInitDebugStuff()
{
	// ---> Draw Collidable
	isDrawCollidable_ = false;
	// ---<
	isDebugStuffInited_ = true;
}


/*********************************************************************************
 * On Tab Key Pressed.
 *********************************************************************************/
void
StateGame::OnTabKeyPressed(EventDetails* eventDetails)
{
	if (tempEntityID_ != -1)
	{
		EntityManager* entityManager = stateManager_->GetContext()->entityManager;
		entityManager->RotateEntityClockwise(tempEntityID_);
	}
}



/******************************* DrawTempTileLayout ******************************
 * For SubMode::BuldWall/SubMode::LayFloor/SubMode::BuildFoundation/SubMode::Bulldozing
 * NOTE: not used for SubMode::ChangeWallpaper
 *********************************************************************************/
void
StateGame::DrawTempTileLayout(Window* window)
{
	if (subMode_ != SubMode::LayFloor &&
	    subMode_ != SubMode::BuildFoundation &&
		subMode_ != SubMode::BuildWall &&
		subMode_ != SubMode::Bulldozing)
	{ 
		return;  
	}
	// ----------> Case 1: Not Dragging Mouse
	if (!bIsMouseDragging_)
	{
		//countRed_ = 0;
		SetCountRed(0);
		Tile* tile = pGameWorld_->GetTileMap()->GetTile(mouseTilePos_.x,
													   mouseTilePos_.y,
													   0);
		// ---> Foundation
		if (!tile)
			return;
		//sf::Sprite tileSprite;
		if (tileTempSprite_)
		{
			tileTempSprite_->setPosition(mouseTilePos_.x * TILE_SIZE,
										 mouseTilePos_.y * TILE_SIZE);
			//tileSprite.setTexture(*(GetTextureManager()->GetResource("cursor_foundation")));
			if ((subMode_ == SubMode::BuildFoundation && tile->tileType != TileType::Empty) ||
				(subMode_ == SubMode::LayFloor && tile->tileType != TileType::Foundation))
			{
				//countRed_++;
				SetCountRed(countRed_ + 1);
				tileTempSprite_->setColor(sf::Color(255, 0, 0, 100));
			}
			else
			{
				//tempTilePlaceHolders_.push_back();
				// FIXME: very weird bug!!! if you set alpha to 100, 
				// and then you lay single floor, it becomes black!!!
				tileTempSprite_->setColor(sf::Color(255, 255, 255, 255));
			}
			GetRenderWindow()->draw(*tileTempSprite_);
			return;
		}
		// ---> For SubMode::Bulldozing & SubMode::BuildWall
		else
		{
			TilePlaceHolder tilePlaceHolder;
			tilePlaceHolder.shape.setSize(vec2f(TILE_SIZE, TILE_SIZE));
			tilePlaceHolder.shape.setPosition(mouseTilePos_.x * TILE_SIZE, mouseTilePos_.y * TILE_SIZE);
			tilePlaceHolder.shape.setFillColor(sf::Color(0, 255, 0, 100));

			// ---> SubMode::BuildWall
			if (subMode_ == SubMode::BuildWall &&
				!pSWall_->IsOKToPlaceWall(mouseTilePos_.x, mouseTilePos_.y, pGameWorld_))
			{
				//countRed_++;
				SetCountRed(countRed_ + 1);
				tilePlaceHolder.shape.setFillColor(sf::Color(255, 0, 0, 100));
			}
			// ---< SubMode::BuildWall
			// ---> SubMode::Bulldozing
			if (subMode_ == SubMode::Bulldozing &&
				tileUnderMouse_->tileType == TileType::Empty &&
				wallsToBulldoze_.size() == 0 &&
				doorsToBulldoze_.size() == 0)
			{
				//countRed_++;
				SetCountRed(countRed_ + 1);
				tilePlaceHolder.shape.setFillColor(sf::Color(255, 0, 0, 100));
			}
			// ---< SubMode::Bulldozing
			GetRenderWindow()->draw(tilePlaceHolder.shape);
			return;
		}
	}
	// ----------< Case 1: Not Dragging Mouse

	// ----------> Case 2: Dragging Mouse
	tempTilePlaceHolders_.clear();
	countGreen_ = 0;
	//countRed_ = 0;
	SetCountRed(0);
	if (mouseDraggingMode_ == MouseDraggingMode::UnConstrained)
	{
		for (int32 x = mouseDraggingXRange_.x;
			 x <= mouseDraggingXRange_.y; ++x)
		{
			for (int32 y = mouseDraggingYRange_.x;
				 y <= mouseDraggingYRange_.y; ++y)
			{
				TilePlaceHolder floorPlaceHolder;
				floorPlaceHolder.shape.setPosition(x * TILE_SIZE, (y + 1) * TILE_SIZE);
				floorPlaceHolder.shape.setOrigin(vec2f(0.0f, TILE_SIZE));
				floorPlaceHolder.shape.setSize(vec2f(TILE_SIZE, TILE_SIZE));
				floorPlaceHolder.shape.setFillColor(sf::Color(0, 255, 0, 100));
				countGreen_++;
				//SFloor* sysFloor = stateManager_->GetContext()->systemManager->GetSystem<SFloor>(SystemType::Floor);
				floorPlaceHolder.tilepos.x = x;
				floorPlaceHolder.tilepos.y = y;
				Tile* tile = pGameWorld_->GetTileMap()->GetTile(x, y, 0);
				TileInfo* tileInfo = tile->tileInfo;
				if (subMode_ == SubMode::BuildFoundation)
				{
					if (tile->solid ||
						tile->tileType != TileType::Empty)
					{
						floorPlaceHolder.shape.setFillColor(sf::Color(255, 0, 0, 100));
						//countRed_++;
						SetCountRed(countRed_ + 1);
						countGreen_--;
					}
				}
				if (subMode_ == SubMode::LayFloor)
				{
					if (tile->tileType != TileType::Foundation)
					{
						floorPlaceHolder.shape.setFillColor(sf::Color(255, 0, 0, 100));
						//countRed_++;
						SetCountRed(countRed_ + 1);
						countGreen_--;
					}
				}
				tempTilePlaceHolders_.push_back(floorPlaceHolder);
			}
		}
	}
	// MouseDraggingMode::Constrained is only used for SubMode::BuildWall for now.
	else if (mouseDraggingMode_ == MouseDraggingMode::Constrained)
	{
		if (mouseDraggingDirection_ == MouseDraggingDirection::Right)
		{
			for (int32 x = mouseDraggingXRange_.x;
				 x <= mouseDraggingXRange_.y; ++x)
			{
				int32 y = mouseFirstDownTilePos_.y;
				TilePlaceHolder tilePlaceHolder;
				tilePlaceHolder.shape.setPosition(x * TILE_SIZE, (y + 1) * TILE_SIZE);
				tilePlaceHolder.shape.setOrigin(vec2f(0.0f, TILE_SIZE));
				tilePlaceHolder.shape.setSize(vec2f(TILE_SIZE, TILE_SIZE));
				tilePlaceHolder.shape.setFillColor(sf::Color(0, 255, 0, 100));
				countGreen_++;
				//SFloor* sysFloor = stateManager_->GetContext()->systemManager->GetSystem<SFloor>(SystemType::Floor);
				tilePlaceHolder.tilepos.x = x;
				tilePlaceHolder.tilepos.y = y;
				Tile* tile = pGameWorld_->GetTileMap()->GetTile(x, y, 0);
				TileInfo* tileInfo = tile->tileInfo;
				if (subMode_ == SubMode::BuildWall)
				{
					if (tile->tileType != TileType::Foundation &&
						tile->tileType != TileType::Floor)
					{
						tilePlaceHolder.shape.setFillColor(sf::Color(255, 0, 0, 100));
						//countRed_++;
						SetCountRed(countRed_ + 1);
						countGreen_--;
					}
				}
				tempTilePlaceHolders_.push_back(tilePlaceHolder);
			}
		}
		else if (mouseDraggingDirection_ == MouseDraggingDirection::Down)
		{
			for (int32 y = mouseDraggingYRange_.x;
				 y <= mouseDraggingYRange_.y; ++y)
			{
				int32 x = mouseFirstDownTilePos_.x;
				TilePlaceHolder tilePlaceHolder;
				tilePlaceHolder.shape.setPosition(x * TILE_SIZE, (y + 1) * TILE_SIZE);
				tilePlaceHolder.shape.setOrigin(vec2f(0.0f, TILE_SIZE));
				tilePlaceHolder.shape.setSize(vec2f(TILE_SIZE, TILE_SIZE));
				tilePlaceHolder.shape.setFillColor(sf::Color(0, 255, 0, 100));
				countGreen_++;
				//SFloor* sysFloor = stateManager_->GetContext()->systemManager->GetSystem<SFloor>(SystemType::Floor);
				tilePlaceHolder.tilepos.x = x;
				tilePlaceHolder.tilepos.y = y;
				Tile* tile = pGameWorld_->GetTileMap()->GetTile(x, y, 0);
				TileInfo* tileInfo = tile->tileInfo;
				if (subMode_ == SubMode::BuildWall)
				{
					if (tile->tileType != TileType::Foundation &&
						tile->tileType != TileType::Floor)
					{
						tilePlaceHolder.shape.setFillColor(sf::Color(255, 0, 0, 100));
						//countRed_++;
						SetCountRed(countRed_ + 1);
						countGreen_--;
					}
				}
				tempTilePlaceHolders_.push_back(tilePlaceHolder);
			}
		}
		// NOTE: [double walls]https://www.simgendog.com/docs/tlh/keyworld=doublewall
		// ---> Disable building double walls
		int countHasAdjacentWall = 0;
		if (tempTilePlaceHolders_.size() > 2)
		{
			for (auto& itr : tempTilePlaceHolders_)
			{
				if (pSWall_->HasWall(itr.tilepos.x, itr.tilepos.y - 1) ||
					pSWall_->HasWall(itr.tilepos.x, itr.tilepos.y + 1) ||
					pSWall_->HasWall(itr.tilepos.x - 1, itr.tilepos.y) ||
					pSWall_->HasWall(itr.tilepos.x + 1, itr.tilepos.y))
				{
					countHasAdjacentWall++;
					if (countHasAdjacentWall >= 2)
					{
						//countRed_ = 999;
						SetCountRed(999);
						break;
					}
				}
				else
					countHasAdjacentWall = 0;
			}
		}
		if (tempTilePlaceHolders_.size() == 2)
		{
			// horizontal
			if (mouseDraggingDirection_ == MouseDraggingDirection::Right)
			{
				for (auto& itr : tempTilePlaceHolders_)
				{
					if (pSWall_->HasWall(itr.tilepos.x, itr.tilepos.y - 1) ||
						pSWall_->HasWall(itr.tilepos.x, itr.tilepos.y + 1))
					{
						countHasAdjacentWall++;
						if (countHasAdjacentWall >= 2)
						{
							//countRed_ = 999;
							SetCountRed(999);
							break;
						}
					}
					else
						countHasAdjacentWall = 0;
				}
			}
			// vertical
			if (mouseDraggingDirection_ == MouseDraggingDirection::Down)
			{
				for (auto& itr : tempTilePlaceHolders_)
				{
					if (pSWall_->HasWall(itr.tilepos.x - 1, itr.tilepos.y) ||
						pSWall_->HasWall(itr.tilepos.x + 1, itr.tilepos.y))
					{
						countHasAdjacentWall++;
						if (countHasAdjacentWall >= 2)
						{
							//countRed_ = 999;
							SetCountRed(999);
							break;
						}
					}
					else
						countHasAdjacentWall = 0;
				}
			}
		}
		// ---< Disable building double walls.
	}

	// ---> actually draw temp tile placeholders
	if (countRed_ > 0 && tileTempSprite_)
		tileTempSprite_->setColor(sf::Color::Red);
	else if(tileTempSprite_)
		tileTempSprite_->setColor(sf::Color(255, 255, 255, 255));
	for (auto& itr : tempTilePlaceHolders_)
	{
		if (tileTempSprite_)
		{
			tileTempSprite_->setPosition(vec2f(itr.tilepos.x * TILE_SIZE,
											   itr.tilepos.y * TILE_SIZE));
			GetRenderWindow()->draw(*tileTempSprite_);
		}
		else
		{
			if(countRed_ > 0)
				itr.shape.setFillColor(sf::Color(255, 0, 0, 100));
			window->GetRenderWindow()->draw(itr.shape);
		}
		// ---> cost
		if (subMode_ == SubMode::BuildFoundation)
		{
			//tempCost_ = pGameStateData_->GetBasicInfo()->GetPrice("Foundation") * countGreen;
			tempCost_ = 40 * countGreen_;
			std::stringstream ss;
			ss << "$" << std::fixed << std::setprecision(2) << tempCost_;
			tempCostText_->setString(ss.str());
		}
		// ---< cost
	}
	// ---< actually draw temp tile placeholder
	// ----------< Case 2: Mouse is dragging
}

/*********************************************************************************
 * GOAP: Get Entity ID by name
 *********************************************************************************/
int32
StateGame::GetEntityID(const string& name)
{
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	int32 result = -1;
	result = entityManager->GetEntityID(name);
	return result;
}

/*********************************************************************************
 * GOAP: Get Entity Pos
 *********************************************************************************/
vec2f
StateGame::GetEntityPos(const int32& entityID)
{
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	CPosition* cPos = entityManager->GetComponent<CPosition>(entityID, ComponentType::Position);
	return cPos->GetPosition();
}
/******************************* GetEntityTilePos *******************************
 * Get Entity TilePos
 ********************************************************************************/
vec2i
StateGame::GetEntityTilePos(const int32 entityID) const
{
	return pEntityManager_->GetEntityTilePos(entityID);
}

/*********************************************************************************
 * Lay Floor
 *********************************************************************************/
void
StateGame::LayFloor(const int32& x, const int32& y,
					const string& tileSetName,
					const int32& tileID)
{
	//subMode_ = SubMode::LayFloor;
	//ChangeSubMode(SubMode::LayFloor);
	pGameWorld_->GetTileMap()->SetTile(x, y, 0,
									  floorTileSetName_,
									  floorTileID_);
	Tile* tile = pGameWorld_->GetTile(x, y, 0);
	tile->tileType = TileType::Floor;
}
/*********************************************************************************
 * Pause/UnPause.
 *********************************************************************************/
void
StateGame::Pause()
{
	subMode_ = SubMode::Paused;
}

void
StateGame::UnPause()
{
	subMode_ = SubMode::Normal;
}

/*********************************************************************************
 * Back To Main Menu
 *********************************************************************************/
void
StateGame::BackToMainMenu()
{
	// TODO: make sure you destory the state when you switch out of it.
	stateManager_->SwitchTo(GameStateType::MainMenu);
}

// ---> Coordinate Conversion
vec2i 
StateGame::WorldToScreenPos(const vec2f& worldPos)
{
	vec2i result;
	sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();
	result = renderWindow->mapCoordsToPixel(worldPos, view_);
	//Logger::Instance()->EngineLogger()->info("worldPos:" + std::to_string(worldPos.x) + "," + std::to_string(worldPos.y));
	//Logger::Instance()->EngineLogger()->info("screenPos:" + std::to_string(result.x) + "," + std::to_string(result.y));
	return result;
}
// ---<

/*********************************************************************************
 * Save Game.
 *********************************************************************************/
void
StateGame::Save(const string& saveFileName)
{
	SystemManager* sysManager = stateManager_->GetContext()->systemManager;
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	if (saveFileName.empty())
	{
		// overwrite current save
		entityManager->SaveEntitiesToFile();
	}
}

/*********************************************************************************
 * Load Game.
 *********************************************************************************/
void
StateGame::LoadWalls()
{
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	SWall* sysWall = stateManager_->GetContext()->systemManager->GetSystem<SWall>(SystemType::Wall);
	auto savedWalls = Utils::GetFileList(Utils::GetMediaDirectory() + 
										 "SavedEntities/Walls/" ,"*.entity");
	for (auto& itr : savedWalls)
	{
		string wallEntityFileName = itr.first;
		int32 wallpieceID = entityManager->AddEntity(wallEntityFileName,
													 Utils::GetMediaDirectory() + "SavedEntities/Walls/");
		sysWall->UpdateWallGrid(wallpieceID);
	}
	sysWall->CalculateWallShapes();
	std::cout << "Num Walls: " << sysWall->GetNumEntities() << std::endl;
}

void
StateGame::LoadDoors()
{
	auto savedDoors = Utils::GetFileList(Utils::GetMediaDirectory() + 
										 "SavedEntities/Doors/" ,"*.entity");
	SWall* sysWall = stateManager_->GetContext()->systemManager->GetSystem<SWall>(SystemType::Wall);
	for (auto& itr : savedDoors)
	{
		string doorEntityFileName = itr.first;
		// ---> Get Door Tile Pos
		std::ifstream doorEntityFile;
		doorEntityFile.open(Utils::GetMediaDirectory() + "SavedEntities/Doors/" + doorEntityFileName);
		if (!doorEntityFile.is_open())
		{
			std::cout << "Failed to load entity from file: " << doorEntityFileName << std::endl;
			continue;
		}
		string line;
		vec2i tilePos;
		vec2f worldPos;
		while (std::getline(doorEntityFile, line))
		{
			if (line[0] == '|')
			{
				continue;
			}
			std::stringstream keystream(line);
			string type;
			int32 componentType = (int32)ComponentType::Position;
			keystream >> type;
			if (type == "Component")
			{
				keystream >> componentType;
			}
			else
			{
				continue;
			}
			if (componentType != (int32)ComponentType::Position)
			{
				continue;
			}
			else
			{
				keystream >> worldPos.x >> worldPos.y;
				tilePos.x = worldPos.x / TILE_SIZE;
				tilePos.y = worldPos.y / TILE_SIZE;
				break;
			}
		}
		doorEntityFile.close();
		// ---<
		if (sysWall->AddDoor(doorEntityFileName,
							 Utils::GetMediaDirectory() + "SavedEntities/Doors/",
							 tilePos.x, tilePos.y - 1))
		{
			std::cout << "Door Added" << std::endl;
			sysWall->CalculateWallShapes();
		}
	}
}

void
StateGame::LoadFloors()
{
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	SFloor* sysFloor = stateManager_->GetContext()->systemManager->GetSystem<SFloor>(SystemType::Floor);
	auto savedFloors = Utils::GetFileList(Utils::GetMediaDirectory() + 
										 "SavedEntities/Floors/" ,"*.entity");
	for (auto& itr : savedFloors)
	{
		string floorEntityFileName = itr.first;
		int32 floorPieceID = entityManager->AddEntity(floorEntityFileName,
													  Utils::GetMediaDirectory() + "SavedEntities/Floors/");
		sysFloor->UpdateFloorGrid(floorPieceID);
	}
	std::cout << "Num Floors: " << sysFloor->GetNumEntities() << std::endl;
}

/******************************* EntityAdded *************************************
 *
 *********************************************************************************/
void
StateGame::EntityAdded(int32 entityID)
{
	pGameWorld_->EntityAdded(entityID);

	// ---> A locker_00 provides 1 staff capacity.
	if (pEntityManager_->GetEntityName(entityID) == "Locker_00")
	{
		int32 staffCapacity = pGameStateData_->GetBasicInfo()->GetStaffCapacity();
		// TODO: modulate this(e.g. 
		pGameStateData_->GetBasicInfo()->SetStaffCapacity(staffCapacity + 1);
	}
	// ---< A locker_00 provides 1 staff capacity.
}

/******************************* EntityRemoved **********************************
 *
 ********************************************************************************/
void
StateGame::EntityRemoved(int32 entityID)
{
	pGameWorld_->EntityRemoved(entityID);
}

/*********************************************************************************
 * Getters/Setters.
 *********************************************************************************/
vec2u
StateGame::GetWindowSize() const
{
	return stateManager_->GetContext()->window->GetSize();
}

SubMode
StateGame::GetSubMode() const
{
	return subMode_;
}



/*********************************************************************************
 * DEBUG Draw Panel
 *********************************************************************************/
void
StateGame::DEBUGDrawPanel()
{
}

/******************************* DEBUGDrawCharacterInfo *************************
 *
 ********************************************************************************/
void
StateGame::DEBUGDrawCharacterInfo()
{
}


// InitTestEntities
void
StateGame::InitTestEntities()
{
	// Hire a nurse
	// ---> A Tester
	// TODO: why does target(200, 200) will generate a seek force Nan??
	//charMgr_->NewCharacter(Profession::Tester, vec2f(200.0f, 200.0f));
	//charMgr_->NewCharacter(Profession::Tester, vec2f(300.0f, 200.0f));
	// ---< A Tester

	// ---> A Doctor
	// ---< A Doctor


	// ---> A Nurse
	//charMgr_->NewCharacter(Profession::Nurse, vec2f(260.0f, 500.0f));
	// ---< A Nurse


	// ---> A Worker
	// ---< A Worker

	// ---> A Janitor
	// ---< A Janitor
	//pSelectedCharacter_ = charMgr_->GetCharacterFromID(0);
	// 2

	// 3

	// 4
	
	// 5
}

// DEBUGGetFont
sf::Font&
StateGame::DEBUGGetFont()
{
	return *(stateManager_->GetContext()->fontManager->GetResource("Main"));
}

// ---> Modify
void 
StateGame::PopThoughtBubble(int32 testEntityID)
{
}


string
StateGame::GetEntityName(int32 entityID)
{
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	string entityName = entityManager->GetEntityName(entityID);
	return entityName;
}

TextureManager* 
StateGame::GetTextureManager() const
{
	return stateManager_->GetContext()->textureManager;
}

FontManager*
StateGame::GetFontManager() const
{
	return stateManager_->GetContext()->fontManager;
}

SystemManager*
StateGame::GetSystemManager() const
{
	return stateManager_->GetContext()->systemManager;
}
EntityManager* 
StateGame::GetEntityManager() const
{
	return stateManager_->GetContext()->entityManager;
}

CharacterManager*
StateGame::GetCharacterManager() const
{
	return charMgr_;
}

GameWorld*
StateGame::GetGameWorld() const
{
	return pGameWorld_;
}
void 
StateGame::SelectNextCharacter(EventDetails* eventDetails)
{
	selectedCharID_++;
	if (selectedCharID_ >= charMgr_->GetNumCharacters())
		selectedCharID_ = charMgr_->GetNumCharacters() - 1;
	pSelectedCharacter_ = charMgr_->GetCharacterFromID(selectedCharID_);
}

void
StateGame::SelectPrevCharacter(EventDetails* eventDetails)
{
	selectedCharID_--;
	if (selectedCharID_ < 0)
		selectedCharID_ = 0;
	pSelectedCharacter_ = charMgr_->GetCharacterFromID(selectedCharID_);
}


Clock*
StateGame::GetClock() const
{
	return clock_;
}

/******************************* ChangeSubMode **********************************
 * changes submode
 ********************************************************************************/
void
StateGame::ChangeSubMode(const SubMode& subMode, const char* parameter)
{
	subMode_ = subMode;
	TextureManager* textureManager = stateManager_->GetContext()->textureManager;
	switch (subMode_)
	{
		case SubMode::BuildWall:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_pointhand"));
			mouseDraggingMode_ = MouseDraggingMode::Constrained;
			tileTempSprite_ = nullptr;
		} break;

		case SubMode::LayFloor:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_pointhand"));
			string param(parameter);
			string delimiter = "-";
			floorTileSetName_ = param.substr(0, param.find(delimiter));
			floorTileID_ = std::stoi(param.substr(param.find(delimiter) + 1, param.size()));

			tileTempSprite_ = &pGameWorld_->GetTileMap()->GetTileSet(floorTileSetName_)->GetTileInfo(floorTileID_)->GetSprite();
			mouseDraggingMode_ = MouseDraggingMode::UnConstrained;
		} break;

		case SubMode::Normal:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_normal"));
			if (tempEntityID_ != -1)
			{
				GetEntityManager()->RemoveEntity(tempEntityID_);
				OnNewTempEntity(-1);
			}
			mouseDraggingMode_ = MouseDraggingMode::Disabled;
			tileTempSprite_ = nullptr;
		} break;

		case SubMode::AddDoor:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_pointhand"));
			tempEntityFilePath_ = string(parameter);
			EntityManager* entityManager = stateManager_->GetContext()->entityManager;
			OnNewTempEntity(entityManager->AddEntity(tempEntityFilePath_));
			entityManager->TintEntity(tempEntityID_, sf::Color::Red);
			mouseDraggingMode_ = MouseDraggingMode::Disabled;
			tileTempSprite_ = nullptr;
		} break;


		case SubMode::ChangeWallpaper:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_pointhand"));
			tempEntityFilePath_ = string(parameter);
			mouseDraggingMode_ = MouseDraggingMode::Constrained;
			tileTempSprite_ = nullptr;
		} break;

		case SubMode::BuyEntity:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_pointhand"));
			tempEntityFilePath_ = string(parameter);
			EntityManager* entityManager = stateManager_->GetContext()->entityManager;
			OnNewTempEntity(entityManager->AddEntity(tempEntityFilePath_));
			//entityManager->TintEntity(tempEntityID_, sf::Color::Red);
			mouseDraggingMode_ = MouseDraggingMode::Disabled;
			tileTempSprite_ = nullptr;
		} break;

		case SubMode::BuildFoundation:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_pointhand"));
			tileTempSprite_ = new sf::Sprite();
			tileTempSprite_->setTexture(*(GetTextureManager()->GetResource("cursor_foundation")));
			mouseDraggingMode_ = MouseDraggingMode::UnConstrained;
		} break;

		case SubMode::Bulldozing:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_hammer"));
			mouseDraggingMode_ = MouseDraggingMode::UnConstrained;
			tileTempSprite_ = nullptr;
		} break;

		case SubMode::MoveEntity:
		{
			m_pCursorSprite->setTexture(*textureManager->GetResource("cursor_pointhand"));
			tileTempSprite_ = nullptr;
		} break;

		default :
		{

		} break;
	}
}


/******************************* PutItemOnGround ********************************
 *
 ********************************************************************************/
void 
StateGame::PutItemOnGround(const vec2f& pos, const ItemType& itemType)
{
	EntityManager* entityManager = stateManager_->GetContext()->entityManager;
	//stateManager_->GetContext()->entityManager->AddEntity("Doctor");
	// TODO: entityManager->AddEntity(filename, pos);
	int32 litterID = entityManager->AddEntity("/Items/ItemLitterSnack");
	int32 randX = Math::RandInRange(0, 10);
	int32 randY = Math::RandInRange(0, 10);
	entityManager->SetEntityPos(litterID, vec2f(pos.x + randX, pos.y + randY));
	vec2f litterPos = pos;
	//(void*)extraInfo = &litterPos;
	pGameWorld_->EntityAdded(litterID);
	CharMsgDispatcher::Instance()->BroadcastMessage(SENDER_ID_IRRELEVANT,
											        CharMsgType::NewLitter,
										&litterPos);
}
// ---< Modify

/******************************* AssignRoom *************************************
 *
 ********************************************************************************/
void
StateGame::AssignRoom(RoomType roomType)
{
	tempRoomType_ = roomType;
	ChangeSubMode(SubMode::AssignRoom);
}

/******************************* GetClosestRoom *********************************
 *
 ********************************************************************************/
Room*
StateGame::GetClosestRoom(RoomType roomType) const
{
	Room* result = nullptr;

	std::vector<Room*> allRooms = pGameWorld_->GetRooms();
	for (Room* room : allRooms)
	{
		if (room->GetType() == roomType)
			result = room;
	}
	return result;
}


TestEntity*
StateGame::GetTestEntityFromID(int32 testEntityID) const
{
	return charMgr_->GetCharacterFromID(testEntityID);
}


void
StateGame::Key1Pressed(EventDetails* eventDetails)
{
	bKey1Pressed_ = true;
}

/******************************* LoadingFinishedCallback ************************
 *
 ********************************************************************************/
void
StateGame::LoadingFinishedCallback()
{
	// ---> Setup NoesisGUI for StateGame
	Noesis::RegisterComponent<MYGUI::GameTimeUserControl>();
	Noesis::RegisterComponent<MYGUI::GamePlayGUI>();
	Noesis::RegisterComponent<MYGUI::ButtonsBarUserControl>();
	Noesis::RegisterComponent<MYGUI::WindowBuild>();
	Noesis::RegisterComponent<MYGUI::WindowItem>();
	Noesis::RegisterComponent<MYGUI::WindowDebug>();
	Noesis::RegisterComponent<MYGUI::HUDResourcesBar>();
	Noesis::RegisterComponent<MYGUI::MultiTextConverter>();



	LOG_ENGINE_INFO("[StateGame] LoadingFinishedCallback");
	pGameWorld_->InitNavGraph();

	spGamePlayGUI_ = Noesis::MakePtr<MYGUI::GamePlayGUI>();
	spViewModel_ = Noesis::MakePtr<MYGUI::StateGameViewModel>(spGamePlayGUI_.GetPtr());
	spViewModel_->SetStateGame(this);
	spViewModel_->SetGameStateData(pGameStateData_);
	//spViewModel_->SetMouse(new MYGUI::Mouse());
	spGamePlayGUI_->SetDataContext(spViewModel_);

    Window* window = stateManager_->GetContext()->window;
    spNoesisView_ = Noesis::GUI::CreateView(spGamePlayGUI_);
    window->SetNoesisView(spNoesisView_);
	bKey1Pressed_ = true;
	// ---<


	// ---> InGameGUI(VS. NoesisGUI)
	sf::Font* font = stateManager_->GetContext()->fontManager->GetResource("Savior1");
	tempCostText_ = new sf::Text("", *font, 48);
	tempCostText_->setFillColor(sf::Color::Yellow);
	tempCostText_->setOutlineColor(sf::Color::Black);
	tempCostText_->setOutlineThickness(2.0f);
	// ---< InGameGUI(VS. NoesisGUI)
}

/******************************* OnNewTempEntity ********************************
 *
 ********************************************************************************/
void
StateGame::OnNewTempEntity(const int32 entityID)
{
	// ---> Delete Temp Entity
	if (entityID == -1)
	{
		tempEntityID_ = -1;
		cPosTempEntity_ = nullptr;
		cColTempEntity_ = nullptr;
		gizmoFootstep_ = nullptr;
		return;
	}
	// ---< Delete Temp Entity

	tempEntityID_ = entityID;
	//SCollision* sysCollision = stateManager_->GetContext()->systemManager->GetSystem<SCollision>(SystemType::Collision);
	cPosTempEntity_ = pEntityManager_->GetComponent<CPosition>(tempEntityID_, ComponentType::Position);
	cColTempEntity_ = pEntityManager_->GetComponent<CCollidable>(tempEntityID_, ComponentType::Collidable);

	cPosTempEntity_->SetPosition(mouseTilePos_.x * TILE_SIZE,
								 (mouseTilePos_.y + 1) * TILE_SIZE);

	if (gizmoFootstep_ == nullptr &&
		subMode_ != SubMode::AddDoor)
	{
		gizmoFootstep_ = new sf::Sprite;
		pTextureManager_->RequireResource("GizmoFootstep");
		gizmoFootstep_->setTexture(*pTextureManager_->GetResource("GizmoFootstep"));

		vec2i tempEntityTilePos = cPosTempEntity_->GetTilePos();
		vec2i gizmoTilePos = tempEntityTilePos + cColTempEntity_->GetUserOffset();
		gizmoFootstep_->setPosition(gizmoTilePos.x * TILE_SIZE,
									gizmoTilePos.y * TILE_SIZE);
	}
}


/******************************* DrawTempGrid ***********************************
 * Draw a grid around something you are about to build/move
 ********************************************************************************/
void
StateGame::DrawTempGrid()
{
	sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();

	sf::Color lineColor = sf::Color::Green;
	lineColor.a = 155;
	if (countRed_ > 0)
	{
		lineColor = sf::Color::Red;
		lineColor.a = 155;
	}
	if(!bIsMouseDragging_)
	{
		// ---> Horizontal Lines
		for (int32 row = -1; row <= 2; row++)
		{
			sf::Vertex line[] = {
				sf::Vertex(vec2f((mouseTilePos_.x - 3) * TILE_SIZE, (mouseTilePos_.y + row) * TILE_SIZE), sf::Color(lineColor.r, lineColor.g, lineColor.b, 55)),
				sf::Vertex(vec2f((mouseTilePos_.x) * TILE_SIZE, (mouseTilePos_.y + row) * TILE_SIZE), sf::Color(lineColor.r, lineColor.g, lineColor.b, 255)),
				sf::Vertex(vec2f((mouseTilePos_.x + 4) * TILE_SIZE,(mouseTilePos_.y + row) * TILE_SIZE), sf::Color(lineColor.r, lineColor.g, lineColor.b, 55)),
			};
			renderWindow->draw(line, 3, sf::LineStrip);
		}
		// ---< Horizontal Lines

		// ---> Vertical Lines
		for (int32 column = 0; column <= 1; column++)
		{
			sf::Vertex line[] = {
				sf::Vertex(vec2f((mouseTilePos_.x + column) * TILE_SIZE, (mouseTilePos_.y - 3) * TILE_SIZE), sf::Color(lineColor.r, lineColor.g, lineColor.b, 55)),
				sf::Vertex(vec2f((mouseTilePos_.x + column) * TILE_SIZE, mouseTilePos_.y * TILE_SIZE), sf::Color(lineColor.r, lineColor.g, lineColor.b, 255)),
				sf::Vertex(vec2f((mouseTilePos_.x + column) * TILE_SIZE, (mouseTilePos_.y + 4) * TILE_SIZE), sf::Color(lineColor.r, lineColor.g, lineColor.b, 55)),
			};
			renderWindow->draw(line, 3, sf::LineStrip);
		}
		// ---< Vertical Lines
	}
	else 
	{
		if (mouseDraggingMode_ == MouseDraggingMode::UnConstrained)
		{
			if (bIsMouseDragging_ &&
				tempTilePlaceHolders_.size() > 1)
			{
				// ---> Horizontal Lines
				for (int32 row = mouseDraggingYRange_.x;
					 row <= mouseDraggingYRange_.y + 1;
					 row++)
				{
					vec2u xRange = vec2u(mouseDraggingXRange_.x - 1,
										 mouseDraggingXRange_.y + 2);
					DrawHorizontalGridLine(row, xRange, lineColor);
				}
				// ---< Horizontal Lines

				// ---> Veritcal Lines
				for (int32 column = mouseDraggingXRange_.x;
					 column <= mouseDraggingXRange_.y + 1;
					 column++)
				{
					vec2u yRange = vec2u(mouseDraggingYRange_.x - 1,
										 mouseDraggingYRange_.y + 2);
					DrawVerticalGridLine(column, yRange, lineColor);
				}
				// ---< Vertical Lines
			}
		}
		else if (mouseDraggingMode_ == MouseDraggingMode::Constrained)
		{
			// ---> Dragging Horizontally
			if (mouseDraggingDirection_ == MouseDraggingDirection::Right)
			{
				// ---> Vertical lines
				for (int32 column = mouseDraggingXRange_.x;
					 column <= mouseDraggingXRange_.y + 1;
					 column++)
				{
					vec2u yRange = vec2u(mouseFirstDownTilePos_.y - 1,
										 mouseFirstDownTilePos_.y + 2);
					DrawVerticalGridLine(column, yRange, lineColor);
				}
				// ---< Vertical lines
				// ---> Horizontal lines
				DrawHorizontalGridLine(mouseFirstDownTilePos_.y,
									   vec2u(mouseDraggingXRange_.x - 1, mouseDraggingXRange_.y + 2),
									   lineColor);
				DrawHorizontalGridLine(mouseFirstDownTilePos_.y + 1,
									   vec2u(mouseDraggingXRange_.x - 1, mouseDraggingXRange_.y + 2),
									   lineColor);
				// ---< Horizontal lines
			}
			// ---< Dragging Horizontally
			// ---> Dragging Vertically
			else
			{
				// ---> Horizontal lines
				for (int32 row = mouseDraggingYRange_.x;
					 row <= mouseDraggingYRange_.y + 1;
					 row++)
				{
					vec2u xRange = vec2u(mouseFirstDownTilePos_.x - 1,
										 mouseFirstDownTilePos_.x + 2);
					DrawHorizontalGridLine(row, xRange, lineColor);
				}
				// ---< Vertical lines
				// ---> Vertical lines
				DrawVerticalGridLine(mouseFirstDownTilePos_.x,
									 vec2u(mouseDraggingYRange_.x - 1, mouseDraggingYRange_.y + 2),
									 lineColor);
				DrawVerticalGridLine(mouseFirstDownTilePos_.x + 1,
									 vec2u(mouseDraggingYRange_.x - 1, mouseDraggingYRange_.y + 2),
									 lineColor);
				// ---< Vertical lines
			}
			// ---< Dragging Vertically
		}
	}
}

/******************************* DrawHorizontalGridLine *************************
 *
 ********************************************************************************/
void
StateGame::DrawHorizontalGridLine(uint32 row, vec2u xRange, sf::Color color)
{
	sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();
	sf::Vertex line[] = {
		sf::Vertex(vec2f(xRange.x * TILE_SIZE, row * TILE_SIZE), color),
		sf::Vertex(vec2f(xRange.y * TILE_SIZE, row * TILE_SIZE), color)
	};
	renderWindow->draw(line, 2, sf::Lines);
}
/******************************* DrawVerticalGridLine ***************************
 *
 ********************************************************************************/
void
StateGame::DrawVerticalGridLine(uint32 column, vec2u yRange, sf::Color color)
{
	sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();
	sf::Vertex line[] = {
		sf::Vertex(vec2f(column * TILE_SIZE, yRange.x * TILE_SIZE), color),
		sf::Vertex(vec2f(column * TILE_SIZE, yRange.y * TILE_SIZE), color)
	};
	renderWindow->draw(line, 2, sf::Lines);
}

sf::RenderWindow*
StateGame::GetRenderWindow() const
{
	return stateManager_->GetContext()->window->GetRenderWindow();
}

/******************************* Update Mouse Dragging **************************
 *
 ********************************************************************************/
void
StateGame::UpdateMouseDragging()
{
	if (mouseDraggingMode_ == MouseDraggingMode::Disabled)
	{
		bIsMouseDragging_ = false;
		return;
	}
	// ---> check if mouse is dragging
	if (!bIsMouseDragging_ && isMouseLeftDown_)
	{
		real32 mouseDiffLen = Utils::Length(mouseDifference_);
		// ---> check if mouse is really dragging
		if (mouseDiffLen < 1.0f)
		{
			bIsMouseDragging_ = false;
			return;
		}
		else
		{
			bIsMouseDragging_ = true;
		}
		// ---<
	}
	// ---< check if mouse is dragging
	int32 xDiff = mouseFirstDownTilePos_.x - mouseTilePos_.x;
	int32 yDiff = mouseFirstDownTilePos_.y - mouseTilePos_.y;
	// ---> Mouse Dragging Horizontally
	if (mouseDraggingMode_ == MouseDraggingMode::Constrained)
	{
		if (std::abs(xDiff) >= std::abs(yDiff))
		{
			mouseDraggingDirection_ = MouseDraggingDirection::Right;
			int32 startX;
			int32 endX;
			int32 y;
			if (mouseFirstDownTilePos_.x < mouseTilePos_.x)
			{
				startX = mouseFirstDownTilePos_.x;
				endX = mouseTilePos_.x;
			}
			else
			{
				startX = mouseTilePos_.x;
				endX = mouseFirstDownTilePos_.x;
			}
			mouseDraggingXRange_ = vec2u(startX, endX);
			mouseDraggingDirection_ = MouseDraggingDirection::Right;
		}
		// ---> Mouse Dragging Vertically
		else
		{
			mouseDraggingDirection_ = MouseDraggingDirection::Down;
			int32 startY;
			int32 endY;
			if (mouseFirstDownTilePos_.y < mouseTilePos_.y)
			{
				startY = mouseFirstDownTilePos_.y;
				endY = mouseTilePos_.y;
			}
			else
			{
				startY = mouseTilePos_.y;
				endY = mouseFirstDownTilePos_.y;
			}
			//mouseDraggingYRange_ = vec2u(startY, endY);
			mouseDraggingYRange_ = vec2u(startY, endY);
			mouseDraggingDirection_ = MouseDraggingDirection::Down;
		}
	}
	else if (mouseDraggingMode_ == MouseDraggingMode::UnConstrained)
	{
		vec2u xRange;
		vec2u yRange;
		if (mouseFirstDownTilePos_.x > mouseTilePos_.x)
		{
			xRange.x = mouseTilePos_.x;
			xRange.y = mouseFirstDownTilePos_.x;
		}
		else
		{
			xRange.x = mouseFirstDownTilePos_.x;
			xRange.y = mouseTilePos_.x;
		}
		if (mouseFirstDownTilePos_.y > mouseTilePos_.y)
		{
			yRange.x = mouseTilePos_.y;
			yRange.y = mouseFirstDownTilePos_.y;
		}
		else
		{
			yRange.x = mouseFirstDownTilePos_.y;
			yRange.y = mouseTilePos_.y;
		}
		//mouseDraggingXRange_ = xRange;
		//mouseDraggingYRange_ = yRange;
		mouseDraggingXRange_ = xRange;
		mouseDraggingYRange_ = yRange;
		mouseDraggingDirection_ = MouseDraggingDirection::Area;
	}

	// ---> viewmodel
	// TODO: should move all mouse info into StateGameViewModel
	spViewModel_->GetMouseInfo()->SetDraggingXRangeStart(mouseDraggingXRange_.x);
	spViewModel_->GetMouseInfo()->SetDraggingXRangeEnd(mouseDraggingXRange_.y);
	spViewModel_->GetMouseInfo()->SetDraggingYRangeStart(mouseDraggingYRange_.x);
	spViewModel_->GetMouseInfo()->SetDraggingYRangeEnd(mouseDraggingYRange_.y);
	// ---< viewmodel
}

/******************************* UpdateTileUnderMouse ***************************
 * Update Tile Under Mouse
 ********************************************************************************/
void
StateGame::UpdateTileUnderMouse(bool bForceUpdate)
{
	Tile* newTileUnderMouse = pGameWorld_->GetTile(mouseTilePos_);
	if (tileUnderMouse_ == nullptr || // first time
		tileUnderMouse_ != newTileUnderMouse ||// tile under mouse is new
		bForceUpdate)
	{
		oldTileUnderMouse_ = tileUnderMouse_;
		tileUnderMouse_ = newTileUnderMouse;

		if (!bIsMouseDragging_ &&
			(subMode_ == SubMode::BuildWall ||
			 subMode_ == SubMode::BuildFoundation ||
			 subMode_ == SubMode::LayFloor ||
			 subMode_ == SubMode::Bulldozing))
		{
			tempTilePlaceHolders_.clear();
			TilePlaceHolder tilePlaceHolder;
			tilePlaceHolder.shape.setSize(vec2f(TILE_SIZE, TILE_SIZE));
			tilePlaceHolder.shape.setPosition(mouseTilePos_.x * TILE_SIZE,
											  mouseTilePos_.y * TILE_SIZE);
			tilePlaceHolder.shape.setFillColor(sf::Color(0, 255, 0, 100));
			tilePlaceHolder.tilepos = mouseTilePos_;
			tempTilePlaceHolders_.push_back(tilePlaceHolder);
		}
	}
	spViewModel_->SetTileUnderMouse(new MYGUI::Tile(tileUnderMouse_->solid, 
													string(magic_enum::enum_name(tileUnderMouse_->tileType)).c_str(), 
													tileUnderMouse_->roomID,
													false,
													tileUnderMouse_->entityID_));
	if (oldTileUnderMouse_)
	{
		spViewModel_->SetOldTileUnderMouse(new MYGUI::Tile(oldTileUnderMouse_->solid,
														   string(magic_enum::enum_name(oldTileUnderMouse_->tileType)).c_str(),
														   oldTileUnderMouse_->roomID,
														   false,
														   tileUnderMouse_->entityID_));
	}
}

void 
StateGame::UpdateTempEntity()
{
}

/******************************* UpdateEntityUnderMouse *************************
 * Update Entity Under Mouse
 ********************************************************************************/
void
StateGame::UpdateEntityUnderMouse()
{
	int32 newEntityUnderMouse = pSRenderer_->GetEntityUnderMouse(mouseWorldPos_);
	if (newEntityUnderMouse != entityUnderMouse_)
	{
		entityUnderMouseName_ = pEntityManager_->GetEntityName(newEntityUnderMouse);
		oldEntityUnderMouse_ = entityUnderMouse_;
		entityUnderMouse_ = newEntityUnderMouse;
	}
	// ---> viewmodel
	spViewModel_->SetEntityUnderMouse(entityUnderMouse_);
	spViewModel_->SetEntityUnderMouseName(entityUnderMouseName_.c_str());
	// ---< viewmodel
}

/******************************* ResetMouseLeft *********************************
 * Find a better name then ClearMouseLeft
 ********************************************************************************/
void
StateGame::ResetMouseLeft()
{
	isMouseLeftDown_ = false;
	bIsMouseDragging_ = false;
	//countRed_ = 0;

	UpdateEntityUnderMouse();
	UpdateTileUnderMouse(true);
}

/******************************* GetTilesTakenByEntity **************************
 *
 ********************************************************************************/
std::vector<vec2i>
StateGame::GetTilesTakenByEntity(const int32 entityID) const
{
	return pSCollision_->GetTilesTakenByEntity(entityID);
}

/******************************* DrawTempEntityGizmo ****************************
 *
 ********************************************************************************/
void
StateGame::UpdateTempEntityGizmo()
{
	/*
	if (tempEntityID_ == -1) return;
	switch (subMode_)
	{
		case SubMode::BuyEntity:
		case SubMode::MoveEntity:
		{
			vec2i tempEntityTilePos = cPosTempEntity_->GetTilePos();
			vec2i gizmoTilePos = tempEntityTilePos + cColTempEntity_->GetUserOffset();
			gizmoFootstep_->setPosition(gizmoTilePos.x * TILE_SIZE,
									    gizmoTilePos.y * TILE_SIZE);
		} break;


		case SubMode::SellEntity:
		{
		} break;
	}
	*/
}

void
StateGame::UpdateHighlightedEntityGizmo()
{
	if (subMode_ != SubMode::MoveEntity &&
		subMode_ != SubMode::SellEntity &&
		subMode_ != SubMode::InspectEntity)
		return;
	if (entityUnderMouse_ != -1 &&
		entityUnderMouse_ != highlightedEntityID_ &&
		entityUnderMouseName_.find("Wall") == std::string::npos &&
		entityUnderMouseName_.find("Door") == std::string::npos)
	{
		CPosition* cPos = pEntityManager_->GetComponent<CPosition>(entityUnderMouse_,
																   ComponentType::Position);
		CCollidable* cCol = pEntityManager_->GetComponent<CCollidable>(entityUnderMouse_,
																	   ComponentType::Collidable);
		vec2i tileSpan = cCol->GetTileSpan();
		vec2i tilePos = cPos->GetTilePos();
		vec2f gizmoULPos(tilePos.x * TILE_SIZE, (tilePos.y - (tileSpan.y - 1)) * TILE_SIZE);
		vec2f gizmoURPos((tilePos.x + tileSpan.x) * TILE_SIZE, (tilePos.y - (tileSpan.y - 1)) * TILE_SIZE);
		vec2f gizmoBLPos(tilePos.x * TILE_SIZE, (tilePos.y + 1) * TILE_SIZE);
		vec2f gizmoBRPos((tilePos.x + tileSpan.x) * TILE_SIZE, (tilePos.y + 1) * TILE_SIZE);
		gizmoHighlightedEntityUL_->setPosition(gizmoULPos);
		gizmoHighlightedEntityUR_->setPosition(gizmoURPos);
		gizmoHighlightedEntityBL_->setPosition(gizmoBLPos);
		gizmoHighlightedEntityBR_->setPosition(gizmoBRPos);

		pAudioManager_->Play("EntityHighlighted");
		highlightedEntityID_ = entityUnderMouse_;
	}
}


/******************************* Setters/Getters ********************************
 * member variable setters&getters
 ********************************************************************************/
void
StateGame::SetCountRed(int32 countRed)
{
	countRed_ = countRed;
}

/******************************* ConfirmTempEntity ******************************
 * Confirm placing temp entity
 ********************************************************************************/
void
StateGame::ConfirmTempEntity()
{
	if (tempEntityID_ == -1) return;
	if (countRed_ == 0)
	{
		EntityAdded(tempEntityID_);
		//tempEntityID_ = -1;
		OnNewTempEntity(-1);
		pAudioManager_->Play("EntityPlaced");
		ChangeSubMode(SubMode::Normal);
	}
	else
	{
		pAudioManager_->Play("Error");
	}
}

/******************************* MakeEntityTemp *********************************
 *
 ********************************************************************************/
void
StateGame::MakeEntityTemp(int32 entityID)
{
	if (tempEntityID_ == -1 &&
		entityID != -1)
	{
		//pGameWorld_->EntityRemoved(entityID);
		EntityRemoved(entityID);
		//tempEntityID_ = highlightedEntityID_;
		OnNewTempEntity(entityID);
		std::cout << "Moving Entity: " << tempEntityID_;
		pAudioManager_->Play("EntitySelected");
	}
}



