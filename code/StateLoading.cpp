#include "StateLoading.h"
#include "StateManager.h"
#include "Window.h"
#include "FontManager.h"
#include "Utilities.h"


/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
StateLoading::StateLoading(StateManager* stateManager)
	: BaseState(stateManager)
	, originalWork_(0)
	, percentage_(0)
	, manualContinue_(true)
{
	int32 a = 100;
}

StateLoading::~StateLoading()
{
}

void
StateLoading::OnCreate()
{
	FontManager* fontManager = stateManager_->GetContext()->fontManager;
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	TextureManager* textureManager = stateManager_->GetContext()->textureManager;

	vec2u windowSize = stateManager_->GetContext()->window->GetSize();

	// text
	fontManager->RequireResource("Savior1");
	sf::Font* font = fontManager->GetResource("Savior1");
	sfText_.setFont(*font);
	sfText_.setColor(sf::Color(148, 167, 127, 255));
	sfText_.setCharacterSize(48);

	// bar frame
	textureManager->RequireResource("ProgressBarFrame");
	progressBarFrameTexture_ = textureManager->GetResource("ProgressBarFrame");
	progressBarFrame_.setTexture(*progressBarFrameTexture_);
	progressBarFrame_.setOrigin(progressBarFrameTexture_->getSize().x / 2.0f,
								progressBarFrameTexture_->getSize().y / 2.0f);
	progressBarFrame_.setPosition(windowSize.x / 2.0f, windowSize.y / 2.0f);
	// bar pieces
	textureManager->RequireResource("ProgressBarPiece");
	progressBarPieceTexture_ = textureManager->GetResource("ProgressBarPiece");

	// running tester
	runningDoctor_ = new GameSprite(textureManager);
	runningDoctor_->LoadSheet("Characters/Bodies/Tester_00");
	runningDoctor_->SetDirection(Direction::Down);
	runningDoctor_->SetPosition(vec2f(windowSize.x / 2, 500));
	runningDoctor_->SetAnimation("Fiddle", true, true);
	runningDoctor_->SetScale(vec2f(3.0f, 3.0f));

	eventManager->AddCallback(GameStateType::Loading, "Key_Space", &StateLoading::Proceed, this);

	LOG_ENGINE_INFO("StateLoading Created!");
}


void
StateLoading::OnDestroy()
{
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	FontManager* fontManager = stateManager_->GetContext()->fontManager;

	eventManager->RemoveCallback(GameStateType::Loading, "Key_Space");
	fontManager->ReleaseResource("Savior1");
	// running doctor
	// TODO: GameSprite Destrucotr
	delete runningDoctor_;

	LOG_ENGINE_INFO("StateLoading Destroyed!");
}


/********************************************************************************
 * The Magic Update Method.
 ********************************************************************************/
void
StateLoading::Update(const sf::Time& time)
{
	runningDoctor_->Update(time.asSeconds());
	//std::cout << "StateLoading is Updating!!!!!" << std::endl;
	if(fileLoaders_.empty())
	{
		if(!manualContinue_)
		{
			Proceed(nullptr);
		}
		return;
	}
	vec2u windowSize = stateManager_->GetContext()->window->GetSize();
	if(fileLoaders_.back()->IsDone())
	{
		/// @todo where does FileLoader->OnRemove gets called
		fileLoaders_.back()->OnRemove();
		fileLoaders_.pop_back();
		if(fileLoaders_.empty())
		{
			UpdateText("Press Space To Continue", 100.0f);
			return;
		}
	}

	if(!fileLoaders_.back()->HasStarted())
	{
		fileLoaders_.back()->Begin();
	}

	real32 percentage = CalculateProgress();
	UpdateText("", percentage);
}


/********************************************************************************
 * Draw.
 ********************************************************************************/
void
StateLoading::Draw()
{
	Window* window = stateManager_->GetContext()->window;
	window->Draw(progressBarFrame_);
	window->Draw(sfText_);
	DrawProgressBarPieces();
	runningDoctor_->Draw(window);
}

void
StateLoading::UpdateText(const string& text, real32 percentage)
{
	if (text != "")
	{
		sfText_.setString(text);
	}
	else
	{
		std::cout << percentage << std::endl;
		std::cout << (int32)percentage << std::endl;
		std::cout << std::to_string((int32)percentage) << std::endl;
		sfText_.setString("Loading " +
						  std::to_string((int32)percentage) +
						  "%");
	}
	vec2u windowSize = stateManager_->GetContext()->window->GetSize();
	sfText_.setPosition(windowSize.x / 2.0f, windowSize.y / 2.0f + 50.0f);
	Utils::CenterSFMLText(sfText_);
}

real32
StateLoading::CalculateProgress()
{
	real32 absolute = 100.0f;
	if(fileLoaders_.empty()) { return absolute; }
	if(fileLoaders_.back()->GetTotalNumberOfLines())
	{
		/// @todo UNDERSTAND:
		real32 d = (100.f * (originalWork_ - fileLoaders_.size())) / static_cast<real32>(originalWork_);
		real32 current = (100.f * fileLoaders_.back()->GetCurrentLineNumber()) /
					     static_cast<real32>(fileLoaders_.back()->GetTotalNumberOfLines());
		real32 totalCurrent = current / static_cast<real32>(originalWork_);
		absolute = d + totalCurrent;
	}
	numProgressBarPieces_ = absolute / 100 * 72;
	if (absolute > 100.0f || absolute < 0.0f)
		absolute = 0.0f;
	return absolute;
}

void
StateLoading::SetManualContinue(bool manualContinue)
{
	manualContinue_ = manualContinue;
}

void
StateLoading::Proceed(EventDetails* eventDetails)
{
	if(!fileLoaders_.empty()) { return; }
	if(finishedCallback_)
		finishedCallback_();
	stateManager_->SwitchTo(stateManager_->GetNextToLastGameStateType());
}

void
StateLoading::AddFileLoader(FileLoader* fileLoader)
{
	fileLoaders_.emplace_back(fileLoader);
	/// @todo where is FileLoader->OnAdd
	fileLoader->OnAdd();
}

bool
StateLoading::HasWork() const
{
	return !fileLoaders_.empty();
}

void
StateLoading::Activate()
{
	originalWork_ = fileLoaders_.size();

	LOG_ENGINE_INFO("StateLoading Activated!");
}

void
StateLoading::Deactivate()
{
	LOG_ENGINE_INFO("StateLoading Deactivated!");
}

/*********************************************************************************
 * Draw Progress Bar Pieces.
 *********************************************************************************/
void
StateLoading::DrawProgressBarPieces()
{
	//sf::RenderWindow* renderWindow = stateManager_->GetContext()->window->GetRenderWindow();
	Window* window = stateManager_->GetContext()->window;
	vec2u windowSize = stateManager_->GetContext()->window->GetSize();
	uint32 pieceStartPos = progressBarFrame_.getPosition().x - progressBarFrameTexture_->getSize().x / 2 + 20;
	for (int32 i = 0; i < numProgressBarPieces_; i++)
	{
		sf::Sprite progressBarPiece;
		progressBarPiece.setTexture(*progressBarPieceTexture_);
		// TODO: don't hard code it
		progressBarPiece.setOrigin(vec2f(0.0f, 12.0f));
		progressBarPiece.setPosition(pieceStartPos + i * 16.0f, windowSize.y / 2.0f);
		progressBarPieces_.push_back(progressBarPiece);
	}
	for (auto& itr : progressBarPieces_)
	{
		//renderWindow->draw(itr);
		window->Draw(itr);
	}
}


