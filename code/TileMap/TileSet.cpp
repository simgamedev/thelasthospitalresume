#include "TileSet.h"
#include "TileInfo.h"



/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
TileSet::TileSet(const string& name, TextureManager* textureManager)
	: name_(name)
	, textureName_("")
	, textureManager_(textureManager)
{
}

/********************************************************************************
 * Empty Destructor.
 ********************************************************************************/
TileSet::~TileSet()
{
	FreeTexture();
}

void
TileSet::FreeTexture()
{
	if(textureName_.empty()) { return; }
	textureManager_->ReleaseResource(textureName_);
	textureName_ = "";
}


/********************************************************************************
 * Get TileInfo
 ********************************************************************************/
TileInfo*
TileSet::GetTileInfo(const int32& tileID)
{
	auto tileInfosItr = tileInfos_.find(tileID);
	if(tileInfosItr == tileInfos_.end())
	{
		return nullptr;
	}
	return tileInfosItr->second.get();
}
/********************************************************************************
 * Read from file line by line.
 ********************************************************************************/
bool
TileSet::ProcessLine(std::stringstream& stream)
{
	string key;
	stream >> key;
	if(key == "TileSetName")
	{
		return true;
	}
	else if(key == "TextureName")
	{
		if(!textureName_.empty()) { return true; }
		stream >> textureName_;
		textureManager_->RequireResource(textureName_);
		return true;
	}
	else if(key == "Name")
	{
		stream >> name_;
		return true;
	}
	else if(key == "TileInfo")
	{
		uint32 tileID;
		string tileName;
		stream >> tileID;
		stream >> tileName;
		auto tileInfo = std::make_unique<TileInfo>(textureManager_, name_, tileName, tileID);
		//stream >> tileInfo->friction.x >> tileInfo->friction.y >> tileInfo->deadly;
		if(!tileInfos_.emplace(tileID, std::move(tileInfo)).second)
		{
			// Duplicate TileInfo
			std::cout << "Error: Duplicate TileInfo: " << tileInfo->GetID() << std::endl;
			tileInfo = nullptr;
		}
		return true;
	}
	else
	{
		std::cout << "Invalid key in tileset file: " << name_ << std::endl;
		return false;
	}
}

/********************************************************************************
 * Purge all TileInfos.
 ********************************************************************************/
void
TileSet::Purge()
{
	tileInfos_.clear();
	FreeTexture();
}

string
TileSet::GetTextureName() const
{
	return textureName_;
}

string
TileSet::GetName() const
{
	return name_;
}


TextureManager*
TileSet::GetTextureManager() const
{
	return textureManager_;
}



