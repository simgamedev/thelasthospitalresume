#pragma once
#include <unordered_map>
#include "TileSet.h"
#include "TileInfo.h"
#include "Tile.h"

#define MAX_MAP_LAYERS 4
class TileMap
{
public:
	TileMap(const vec2u& mapSize);
	void AddTileSet(TileSet* tileSet);

	Tile* SetTile(uint32 x, uint32 y, uint32 elevation, const string& tileSetName, int32 tileID);
	Tile* SetTile(uint32 x, uint32 y, uint32 elevation, TileSet* tileSet, int32 tileID);
	Tile* SetTile(uint32 x, uint32 y, uint32 elevation, Tile* tileToCopy);
	Tile* GetTile(uint32 x, uint32 y, uint32 elevation);
	void PlotBrush(TileMap& brush, const vec2i& tilePos, uint32 startLayer);
	void RemoveTile(uint32 x, uint32 y, int32 elevation = -1);
	void RemoveTiles(const vec2u& xRange, const vec2u& yRange, const vec2u& elevationRange);
	/// @todo get rid of GetMapSize();
	vec2u GetSize() const;
	uint32 GetTileCount() const;
	TileSet* GetTileSet(const string& tileSetName) const;
	/// @todo make clear these are for Brush.
	uint32 GetLowestElevation() const;
	uint32 GetHighestElevation() const;
	/// @todo get rid of setmapsize
	void SetMapSize(const vec2u& mapSize);
	void SetSize(const vec2u& mapSize);


	void SaveToFile(std::ofstream& stream);
	void ReadInTile(std::stringstream& stream);

	void Purge();

private:
	std::unordered_map<string, TileSet*> tileSets_;
	Tile* CreateTile(uint32 x, uint32 y, uint32 elevation);
	/// @todo make clear it's <1d coord, Tile*>
	std::unordered_map<uint32, std::unique_ptr<Tile>> tiles_;
	/// @todo make clear it's <tileSetName, tileSet>
	vec2u mapSize_;
};
