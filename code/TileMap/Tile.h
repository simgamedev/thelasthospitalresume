#pragma once

#include "TileInfo.h"
/**
   Tile.
 */
struct Tile
{
public:
	Tile()
	{
		tileType = TileType::Empty;
		roomID = 0;
		entityID_ = -1;
	}


	TileInfo* tileInfo;
	vec3i pos;
	bool solid;
	int32 entityID_;

	/// @todo GetTilePos() vec2u tilePos
	int32 roomID;
	TileType tileType;
	
	// Room
	//Room* room_;
};
