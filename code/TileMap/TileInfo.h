#pragma once
#include "../common.h"
#include "../TextureManager.h"
#include <SFML/Graphics/Sprite.hpp> 
/** TileType/
*/
enum class TileType
{
	Empty = 0, // also for underground?
	Foundation,
	Wall,
	Floor,
	FarmLand,
	SewagePipe,
	WaterPipe,
	ElectricityWire,
};


/**
   Tile Info(Sparse/Flyweight Pattern).
 */
#define TILE_SIZE 24
class TileInfo
{
public:
	TileInfo(uint32 tileID = 0);
	/// @todo TileInfo should have TileSet name. And We should have a TileSet Resource Manager.
	TileInfo(TextureManager* textureManager,
			 const string& tileSetName,
			 const string& tileName,
			 uint32 tileID = 0);

	~TileInfo();

	uint32 GetID() const;

	//TileType GetType() const;
	//void SetType(TileType tileType);
	string GetTileSetName() const;
	sf::Sprite& GetSprite();
	vec2f GetFriction();
	void SetFriction(const vec2f& friction);
private:
	sf::Sprite sprite_;
	string tileSetName_;
	uint32 tileID_;
	string tileName_;
	vec2f friction_ = vec2f(0.89f, 0.89f);

	//TileType tileType_;
};
