#pragma once
#include <unordered_map>
#include <memory>
#include "../Threads/FileLoader.h"
#include "../TextureManager.h"

struct TileInfo;
/**
   TileSet.
 */
class TileSet : public FileLoader
{
public:
	TileSet(const string& name, TextureManager* textureManager);
	~TileSet();

	void Purge();
	string GetTextureName() const;
	TextureManager* GetTextureManager() const;
	string GetName() const;
	TileInfo* GetTileInfo(const int32& tileID);
private:
	void FreeTexture();
	bool ProcessLine(std::stringstream& stream);
	/// @attention <tileID, smart_ptr(TileInfo)>
	std::unordered_map<uint32, std::unique_ptr<TileInfo>> tileInfos_;
	TextureManager* textureManager_;
	string name_;
	string textureName_;
};
