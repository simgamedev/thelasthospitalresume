#include "TileMap.h"
#include "MapHelpers.h"


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
TileMap::TileMap(const vec2u& mapSize)
{
	SetMapSize(mapSize);
}


/********************************************************************************
 * Add TileSet.
 ********************************************************************************/
void
TileMap::AddTileSet(TileSet* tileSet)
{
	tileSets_.emplace(tileSet->GetName(), tileSet);
}


/********************************************************************************
 * Set Tile.
 * @param x
 ********************************************************************************/
Tile*
TileMap::SetTile(uint32 x, uint32 y, uint32 elevation, const string& tileSetName, int32 tileID)
{
	// ---> Get TileInfo
	auto tileSetsItr = tileSets_.find(tileSetName);
	if(tileSetsItr == tileSets_.end())
	{
		std::cout << "Error: Tileset " << tileSetName << " doesn't exist!" << std::endl;
		return nullptr;
	}
	auto tileSet = tileSetsItr->second;
	TileInfo* tileInfo = tileSet->GetTileInfo(tileID);
	if(tileInfo == nullptr)
	{
		std::cout << "Error: No such tileID " << tileID << " in tileset " << tileSetName << std::endl;
	}
	// ---> Get Tile
	Tile* tile = GetTile(x, y, elevation);
	if(!tile)
	{
		tile = CreateTile(x, y, elevation);
		if(!tile)
		{
			std::cout << "Failed to create tile " << tileID << "," << tileSetName << std::endl;
			return nullptr;
		}
	}
	tile->tileInfo = tileInfo;
	return tile;
}
/*********************************************************************************
 * Set Tile.
 *********************************************************************************/
Tile*
TileMap::SetTile(uint32 x, uint32 y, uint32 elevation, TileSet* tileSet, int32 tileID)
{
	TileInfo* tileInfo = tileSet->GetTileInfo(tileID);
	if(tileInfo == nullptr)
	{
		std::cout << "Error: No such tileID " << tileID << " in tileset " << tileSet->GetName() << std::endl;
	}
	// ---> Get Tile
	Tile* tile = GetTile(x, y, elevation);
	if(!tile)
	{
		tile = CreateTile(x, y, elevation);
		if(!tile)
		{
			std::cout << "Failed to create tile " << tileID << "," << tileSet->GetName() << std::endl;
			return nullptr;
		}
	}
	tile->tileInfo = tileInfo;
	return tile;
}

Tile*
TileMap::SetTile(uint32 x, uint32 y, uint32 elevation, Tile* tileToCopy)
{
	Tile* tile = GetTile(x, y, elevation);
	if(!tile)
	{
		tile = CreateTile(x, y, elevation);
		if(!tile)
		{
			std::cout << "Failed to create tile!" << std::endl;
			return nullptr;
		}
	}

	*tile = *tileToCopy;
	return tile;
}


/********************************************************************************
 * Get Tile at (x,y,elevation).
 * @param x, y, elevation
 * @return Tile*
 ********************************************************************************/
Tile*
TileMap::GetTile(uint32 x, uint32 y, uint32 elevation)
{
	if(!ValidCoords(x, y, elevation, mapSize_)) { return nullptr; }

	auto tilesItr =  tiles_.find(ConvertCoords(x, y, elevation, mapSize_));
	if(tilesItr == tiles_.end()) { return nullptr; }
	return tilesItr->second.get();
}


/********************************************************************************
 * Plot Brush For Map Editor.
 * @param brush(it's a tilemap)
 * @param startLayer starting from which layer to plot brush, brush could be a multiple layer brush
 * @return void
 * @attention elevation/layer is used interchangeably
 ********************************************************************************/
void
TileMap::PlotBrush(TileMap& brush, const vec2i& tilePos, uint32 startLayer)
{
	for(int32 x = 0; x < brush.GetSize().x; ++x)
	{
		for(int32 y = 0; y < brush.GetSize().y; ++y)
		{
			for(int32 layer = 0; layer < MAX_MAP_LAYERS; ++layer)
			{
				vec2i plottingTilePos = tilePos + vec2i(x, y);
				/// @todo mapSize_ and mapSize duplicate??
				// ---> Bounds Check
				if(plottingTilePos.x >= mapSize_.x ||
				   plottingTilePos.y >= mapSize_.y)
				{
					continue;
				}
				Tile* brushTile = brush.GetTile(x, y, layer);
				if(!brushTile) { continue; } // Bursh empty here.
				/// @todo don't bound check layer + startLayer??
				Tile* newTile = SetTile(plottingTilePos.x,
										plottingTilePos.y,
										layer + startLayer,
										brushTile);
				if(!newTile)
				{
					std::cout << "Failed to plot tile from brush" << std::endl;
					continue;
				}
				/// @todo isn't this just a duplicate?
				*newTile = *brushTile;
			}
		}
	}
}
/********************************************************************************
 * Get Map Size.
 ********************************************************************************/
vec2u
TileMap::GetSize() const
{
	return mapSize_;
}
/********************************************************************************
 * Remove A Single Tile.
 * @param elevation == -1, means remove from all layers.
 ********************************************************************************/
void
TileMap::RemoveTile(uint32 x, uint32 y, int32 elevation)
{
	if(!ValidCoords(x, y, elevation, mapSize_)) { return; }

	for(uint32 layer = (elevation == -1 ? 0 : elevation);
		layer <= (elevation == -1 ? MAX_MAP_LAYERS : elevation);
		++layer)
	{
		auto tilesItr = tiles_.find(ConvertCoords(x, y, layer, mapSize_));
		if(tilesItr == tiles_.end()) { continue; }
		tiles_.erase(tilesItr);
	}
}


/********************************************************************************
 * Remove Multiple Tiles.
 ********************************************************************************/
void
TileMap::RemoveTiles(const vec2u& xRange, const vec2u& yRange,  const vec2u& layerRange)
{
	if(!ValidCoords(xRange.x, yRange.x, layerRange.x, mapSize_)) { return; }
	if(!ValidCoords(xRange.y, yRange.y, layerRange.y, mapSize_)) { return; }

	for(auto x = xRange.x; x <= xRange.y; x++)
	{
		for(auto y = yRange.x; y <= yRange.y; y++)
		{
			for(auto layer = layerRange.x; layer <= layerRange.y; ++layer)
			{
				auto tilesItr = tiles_.find(ConvertCoords(x, y, layer, mapSize_));
				if(tilesItr == tiles_.end())
				{
					continue;
				}
				tiles_.erase(tilesItr);
			}
		}
	}
}

/********************************************************************************
 * Get Tile Count.
 ********************************************************************************/
uint32
TileMap::GetTileCount() const
{
	return tiles_.size();
}


/********************************************************************************
 * Get TileSet.
 ********************************************************************************/
TileSet*
TileMap::GetTileSet(const string& tileSetName) const
{
	auto tileSetsItr = tileSets_.find(tileSetName);
	if(tileSetsItr == tileSets_.end())
	{
		return nullptr;
	}
	TileSet* tileSet = tileSetsItr->second;
	return tileSet;
}



/********************************************************************************
 * Get Lowest Elevation.
 ********************************************************************************/
uint32
TileMap::GetLowestElevation() const
{
	uint32 x = 0, y = 0, layer = 0;
	uint32 lowestLayer = 0;
	for(auto& tilesItr : tiles_)
	{
		uint32 coord1D = tilesItr.first;
		ConvertCoords(coord1D, x, y, layer, mapSize_);
		if(layer == 0) { return 0; }
		if(layer < lowestLayer)
		{
			lowestLayer = layer;
		}
	}
	return lowestLayer;
}

/********************************************************************************
 * Get Highest Elevation.
 ********************************************************************************/
uint32
TileMap::GetHighestElevation() const
{
	uint32 x = 0, y = 0, layer = 0;
	uint32 highestLayer = 0;
	for(auto& tilesItr : tiles_)
	{
		uint32 coord1D = tilesItr.first;
		ConvertCoords(coord1D, x, y, layer, mapSize_);
		if(layer == MAX_MAP_LAYERS - 1)
		{
			return (MAX_MAP_LAYERS - 1);
		}
		if(layer > highestLayer)
		{
			highestLayer = layer;
		}
	}
	return highestLayer;
}



/********************************************************************************
 * Set Map Size.
 ********************************************************************************/
void
TileMap::SetSize(const vec2u& mapSize)
{
	mapSize_ = mapSize;
}
void
TileMap::SetMapSize(const vec2u& mapSize)
{
	mapSize_ = mapSize;
}


/********************************************************************************
 * Save To File.
 ********************************************************************************/
void
TileMap::SaveToFile(std::ofstream& stream)
{
	bool isFirstLine = true;
	for(auto& tilesItr : tiles_)
	{
		uint32 coord1D = tilesItr.first;
		Tile* tile = tilesItr.second.get();
		/// @todo doxygen test
		/// @attention doxygen test
		/// @bug doxygen test
		/// @cite doxygen test
		uint32 x, y, layer;
		ConvertCoords(coord1D, x, y, layer, mapSize_);
		if (layer == 1)
		{
			int32 a = 100;
		}
		int32 tileID = tile->tileInfo->GetID();
		string tileSetName = tile->tileInfo->GetTileSetName();
		if(!isFirstLine) { stream << std::endl; }
		stream << "TILE " << tileSetName << " " << tileID << " " << x << " " << y << " " << layer << " "
			   << (tile->solid ? 1: 0);
		if (tile->solid)
		{
			int32 a = 100;
		}
		if(isFirstLine) { isFirstLine = false; }
	}
}


/********************************************************************************
 * Read In Tile.
 ********************************************************************************/
void
TileMap::ReadInTile(std::stringstream& stream)
{
	string tileSetName = "";
	int32 tileID = 0;

	stream >> tileSetName;
	stream >> tileID;

	// ---> TileSet Not Found.
	auto tileSetsItr = tileSets_.find(tileSetName);
	if(tileSetsItr == tileSets_.end())
	{
		std::cout << "Error: TileSet " << tileSetName << " doesn't exist!" << std::endl;
	}
	TileSet* tileSet = tileSetsItr->second;
	// ---> TileInfo Not Found.
	TileInfo* tileInfo = tileSet->GetTileInfo(tileID);
	if(tileInfo == nullptr)
	{
		std::cout << "Error: TileID " << tileID << " in tileset " << tileSetName << " doesn't exist.";
	}
	// ---> Create Tile.
	vec2u tilePos;
	uint32 tileLayer = 0;
	uint32 tileSolidity = 0;
	stream >> tilePos.x >> tilePos.y >> tileLayer >> tileSolidity;
	if(tilePos.x > mapSize_.x || tilePos.y > mapSize_.y || tileLayer >= MAX_MAP_LAYERS)
	{
		std::cout << "Error: tile is out of range." << std::endl;
		return;
	}
	auto tile = std::make_unique<Tile>();
	tile->tileInfo = tileInfo;
	tile->pos = vec3i(tilePos.x, tilePos.y, tileLayer);
	tile->solid = (tileSolidity != 0);

	// --> Add Tile To Container.
	if(!tiles_.emplace(ConvertCoords(tilePos.x, tilePos.y, tileLayer, mapSize_), std::move(tile)).second)
	{
		std::cout << "Error: Duplicate tile wile reading map line: " << tilePos.x << "," << tilePos.y << std::endl;
		return;
	}
}

/********************************************************************************
 * Purge. Since we are using smart pointers now, just clear the container is enough.
 ********************************************************************************/
void
TileMap::Purge()
{
	tiles_.clear();
}

/********************************************************************************
 * Create Tile.
 ********************************************************************************/
Tile*
TileMap::CreateTile(uint32 x, uint32 y, uint32 elevation)
{
	auto tile = std::make_unique<Tile>();
	Tile* tilePtr = tile.get();
	/// @todo get rid of the mapSize_
	if(!tiles_.emplace(ConvertCoords(x, y, elevation, mapSize_), std::move(tile)).second)
	{
		return nullptr;
	}
	return tilePtr;
}





