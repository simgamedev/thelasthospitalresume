#include "TileInfo.h"
#include "TileSet.h"

TileInfo::TileInfo(uint32 tileID)
	: tileID_(tileID)
{
}

TileInfo::TileInfo(TextureManager* textureManager,
				   const string& tileSetName,
				   const string& tileName,
				   uint32 tileID)
	: tileSetName_(tileSetName)
	, tileID_(tileID)
	, tileName_(tileName)
	//, tileType_(TileType::Empty)
{
	if (tileSetName_ == "FloorPieces")
	{
		int32 a = 100;
	}
	if (tileSetName_ != "")
	{
		sf::Texture* texture = textureManager->GetResource(tileSetName_);
		sprite_.setTexture(*texture);
		vec2u textureSize = texture->getSize();
		sf::IntRect tileSpriteRect(tileID_ % (textureSize.x / TILE_SIZE) * TILE_SIZE,
			tileID_ / (textureSize.x / TILE_SIZE) * TILE_SIZE,
			TILE_SIZE,
			TILE_SIZE);
		sprite_.setTextureRect(tileSpriteRect);

		// ---> TileType based on tile names?
		/*
		if(tileName_.find("Foundation") != std::string::npos)
		{
			tileType_ = TileType::Foundation;
		}
		if(tileName_.find("Floor") != std::string::npos)
		{
			tileType_ = TileType::Floor;
		}
		*/
		// ---<
	}
}


TileInfo::~TileInfo()
{
}

uint32
TileInfo::GetID() const
{
	return tileID_;
}


string
TileInfo::GetTileSetName() const
{
	return tileSetName_;;
}


sf::Sprite&
TileInfo::GetSprite()
{
	return sprite_;
}

vec2f
TileInfo::GetFriction()
{
	return friction_;
}

void 
TileInfo::SetFriction(const vec2f& friction)
{
	friction_ = friction;
}