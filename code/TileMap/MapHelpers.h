#pragma once
#include "../common.h"


inline uint32 ConvertCoords(uint32 x, uint32 y, uint32 layer, const vec2u& mapSize)
{
	return ((layer * mapSize.y  + y) * mapSize.x + x);
}


inline void ConvertCoords(uint32 coord1D, uint32& x, uint32& y, uint32& layer, const vec2u& mapSize)
{
	layer = (coord1D / (mapSize.x * mapSize.y));
	y = (coord1D - (layer * mapSize.x * mapSize.y)) / mapSize.x;
	x = coord1D - (mapSize.x * (y + (mapSize.y * layer)));
}

inline bool ValidCoords(uint32 x, uint32 y, uint32 layer, const vec2u& mapSize)
{
	return !(x < 0 ||
			 y < 0 ||
			 x >= mapSize.x ||
			 y >= mapSize.y ||
			 layer < 0 ||
			 layer >= MAX_MAP_LAYERS);
}
