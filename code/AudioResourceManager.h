#pragma once
#include "ResourceManager.h"
#include <SFML/Audio/SoundBuffer.hpp>

class AudioResourceManager : public ResourceManager<AudioResourceManager, sf::SoundBuffer>
{
public:
	AudioResourceManager() : ResourceManager("audio.cfg")
	{}

	bool Load(sf::SoundBuffer* resource, const string& path)
	{
		return resource->loadFromFile(Utils::GetMediaDirectory() + path);
	}
};
