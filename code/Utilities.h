#pragma once
#define WIN64
//#define APPLE
#ifdef WIN64
#define _WIN32_WINNT 0x0A00
#include <windows.h>
#include <Shlwapi.h>
#endif

#ifdef APPLE
#include <mach-o/dyld.h>
#include <limits.h>
#include <dirent.h>
#endif


#include <iostream>
#include <sstream>
#include <algorithm>
#include "common.h"
#include <cstdlib>
#include <cmath>
#include <SFML/Graphics/Text.hpp>
#include "Math/C2DMatrix.h"


namespace Utils
{
	//#define WIN32_LEAN_AND_MEAN
#ifdef WIN64
	inline string GetMediaDirectory()
	{
		HMODULE hModule = GetModuleHandle(nullptr);
		if(hModule)
		{
			char path[256];
			GetModuleFileNameA(hModule, path, sizeof(path));
			PathRemoveFileSpecA(path);
			strcat_s(path, "\\");
		    string result(path);
			std::size_t pos = result.find("build");
			result = result.substr(0, pos);
			result = result + "media\\";
			return(result);
		}
		return "";
	}
	/// @attention bool means is direcotry or not
	inline std::vector<std::pair<string, bool>>
	GetFileList(const string& directory, const string& fileType = "*.*", bool directories = false)
	{
		std::vector<std::pair<string, bool>> fileList;
		if(fileType.empty()) { return fileList; }
		string path = directory + fileType;
		WIN32_FIND_DATAA findData;
		HANDLE found = FindFirstFileA(path.c_str(), &findData);
		if(found == INVALID_HANDLE_VALUE) { return fileList; }
		do
		{
			if(!(findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) || directories)
			{
				fileList.emplace_back(std::make_pair(string(findData.cFileName),
													 ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)));
			}
		}
		while(FindNextFileA(found, &findData));
		FindClose(found);
		return fileList;
	}
#endif
#ifdef APPLE
	/********************************************************************************
	 * Mac Version GetMediaDirectory
	 ********************************************************************************/
    inline string GetMediaDirectory()
    {
        char buffer[PATH_MAX];
        uint32_t bufferSize = PATH_MAX;
        _NSGetExecutablePath(buffer, &bufferSize);
        std::string result(buffer);
        std::size_t pos = result.find("build_mac");
        result = result.substr(0, pos);
        result = result + "media/";
        return result;
    }
    /********************************************************************************
     *  Mac Version GetFileLIst
     ********************************************************************************/
    inline std::vector<std::pair<string, bool>>
    GetFileList(const string& directory, const string& fileType = "*.*", bool directories = false)
    {
        std::vector<std::pair<string, bool>> fileList;
        DIR* dir;
        string path = directory + fileType;
        struct dirent *ent;
        if((dir = opendir(directory.c_str())) != NULL)
        {
            /* print all the files and directories within the directory */
            while((ent = readdir(dir)) != NULL)
            {
                if(string(ent->d_name) == "." || string(ent->d_name) == "..")
                    continue;
                std::cout << string(ent->d_name) << std::endl;
                fileList.emplace_back(std::make_pair(string(ent->d_name), 1));
            }
        }
        /*
        if(fileType.empty()) { return fileList; }
        string path = directory + fileType;
        WIN32_FIND_DATAA findData;
        HANDLE found = FindFirstFileA(path.c_str(), &findData);
        if(found == INVALID_HANDLE_VALUE) { return fileList; }
        do
        {
            if(!(findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) || directories)
            {
                fileList.emplace_back(std::make_pair(string(findData.cFileName),
                                                     ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)));
            }
        }
        while(FindNextFileA(found, &findData));
        FindClose(found);
         */
        return fileList;
    }
#endif
	/********************************************************************************
	 * std::to_string with precision
	 ********************************************************************************/
	template <typename T>
	std::string ToString(const T aValue, const int32 precision = 2)
	{
		std::ostringstream out;
		out.precision(precision);
		out << std::fixed << aValue;
		return out.str();
	}
	/********************************************************************************
	 * Center SFML Text
	 ********************************************************************************/
	inline real32 GetSFMLTextMaxHeight(const sf::Text& text)
	{
		auto charSize = text.getCharacterSize();
		auto font = text.getFont();
		auto textString = text.getString().toAnsiString();
		bool isBold = (text.getStyle() & sf::Text::Bold);
		real32 max = 0.0f;
		for(size_t i = 0; i < textString.length(); ++i)
		{
			sf::Uint32 character = textString[i];
			auto glyph = font->getGlyph(character, charSize, isBold);
			auto height = glyph.bounds.height;
			if(height <= max) { continue; }
			max = height;
		}
		return max;
	}
	/// @todo CenterSFMLText??
	inline void CenterSFMLText(sf::Text& text)
	{
		sf::FloatRect rect = text.getLocalBounds();
		auto maxHeight = Utils::GetSFMLTextMaxHeight(text);
		if(maxHeight >= rect.height)
		{
			text.setOrigin(rect.left + (rect.width * 0.5f),
						   rect.top + (maxHeight * 0.5f));
		}
		else
		{
			text.setOrigin(rect.left + (rect.width * 0.5f),
						   rect.top + (rect.height * 0.5f));
		}
	}
	/********************************************************************************
	 * Sort file list.
	 ********************************************************************************/
	inline void
	SortFileList(std::vector<std::pair<string, bool>>& fileList)
	{
		std::sort(fileList.begin(),
				  fileList.end(),
				  [](std::pair<string, bool>& entry1, std::pair<string, bool>& entry2)
				  {
					  if(entry1.second && !entry1.second) { return true; }
					  return false;
				  });
	}

	inline void ReadQuotedString(std::stringstream& stream, string& str)
	{
		/// @todo ReadQuotedString
		stream >> str;
		if(str.at(0) == '"')
		{
			while(str.at(str.length() - 1) != '"' || !stream.eof())
			{
				string temp;
				stream >> temp;
				str.append(" " + temp);
			}
		}
		str.erase(std::remove(str.begin(), str.end(), '"'), str.end());
	}

	/*********************************************************************************
	 * Vector Helpers.
	 *********************************************************************************/
	inline void Truncate(vec2f& vec, real32 max)
	{
		// ---> Modify
		/*
		vec.x = std::min(vec.x, max);
		vec.y = std::min(vec.y, max);
		*/
	}
	inline vec2f Normalize(vec2f vec)
	{
		if (vec == vec2f(0.0f, 0.0f)) return vec2f(0.0f, 0.0f);
		return vec / (std::sqrt(vec.x * vec.x + vec.y * vec.y));
	}
	inline real32 LengthSq(vec2f vec)
	{
		return (vec.x * vec.x + vec.y * vec.y);
	}
	inline real32 Length(const vec2f& vec)
	{
		return std::sqrt(vec.x * vec.x + vec.y * vec.y);
	}
	inline vec2f Perpendicular(const vec2f& vec)
	{
		return vec2f(-vec.y, vec.x);
	}
	//returns a random real32 between zero and 1
	inline real32 RandFloat()
	{ 
		return ((rand()) / (RAND_MAX + 1.0));
	}
	// returns a random real32 betwen -1 and 1
	inline real32 RandomClamped()
	{
		srand(time(NULL));
		return (RandFloat() - RandFloat());
	}
	inline vec2f PointToWorldSpace(const vec2f& point,
								   const vec2f& AgentHeading,
								   const vec2f& AgentSide,
								   const vec2f& AgentPosition)
	{
		//make a copy of the point
		vec2f transPoint = point;

		//create a transformation matrix
		C2DMatrix matTransform;

		//rotate
		matTransform.Rotate(AgentHeading, AgentSide);

		//and translate
		matTransform.Translate(AgentPosition.x, AgentPosition.y);

		//now transform the vertices
		matTransform.TransformVector2Ds(transPoint);

		return transPoint;
	}
	inline real32 HeadingToAngle(const vec2f& heading)
	{
		real32 radian = atan2(heading.y, heading.x);
		return radian * 57.29577;
	}
	inline real32 DistanceSq(const vec2f& a, const vec2f& b)
	{
		return ((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
	}
	// vec2f equals
	inline bool Vec2fEquals(const vec2f& a, const vec2f& b)
	{
		if (std::abs(a.x - b.x) < 0.1 &&
			std::abs(a.y - b.y) < 0.1)
			return true;
		return false;
	}



	/*********************************************************************************/
}
