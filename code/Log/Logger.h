#pragma once

#include <spdlog/spdlog.h>

class Logger
{
public:
	static Logger* Instance();
	std::shared_ptr<spdlog::logger> EngineLogger();
	std::shared_ptr<spdlog::logger> GamePlayLogger();
private:
	Logger();
	~Logger();
	Logger(const Logger&)=delete;
	Logger& operator=(const Logger&)=delete;
	static Logger* pInstance_;
	std::shared_ptr<spdlog::logger> engineLogger_;
	std::shared_ptr<spdlog::logger> gameplayLogger_;
};
