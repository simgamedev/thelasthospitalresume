#include "Logger.h"
#include <spdlog/sinks/stdout_color_sinks.h>

Logger* Logger::pInstance_ = nullptr;

Logger*
Logger::Instance()
{
	// NOTE: They say that C++11 inilizaiton is thread-safe by default.
	if(!pInstance_)
	{
		pInstance_ = new Logger();
	}

	return pInstance_;
}


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
Logger::Logger()
{
	spdlog::set_pattern("%^[%n]: %v%$");
	// engine logger
	engineLogger_ = spdlog::stdout_color_mt("ENGINE");
	engineLogger_->set_level(spdlog::level::trace);

	// game play logger
	gameplayLogger_ = spdlog::stdout_color_mt("GAMEPLAY");
	gameplayLogger_->set_level(spdlog::level::trace);
}
/********************************************************************************
 * Destructor
 ********************************************************************************/
Logger::~Logger()
{
}
/********************************************************************************
 * Engine Logger.
 ********************************************************************************/
std::shared_ptr<spdlog::logger>
Logger::EngineLogger()
{
	return engineLogger_;
}

/********************************************************************************
 * Gameplay Logger.
 ********************************************************************************/
std::shared_ptr<spdlog::logger>
Logger::GamePlayLogger()
{
	return gameplayLogger_;
}
