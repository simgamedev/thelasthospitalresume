#pragma once
#include <SFML/Graphics/Font.hpp>
#include "ResourceManager.h"

/**
   Manages fonts.
 */
class FontManager : public ResourceManager<FontManager, sf::Font>
{
public:
	FontManager()
		: ResourceManager("fonts.cfg")
	{
	}

	bool Load(sf::Font* resource, const string& resourcePath)
	{
		return resource->loadFromFile(Utils::GetMediaDirectory() + resourcePath);
	}
};
