#pragma once
#include "EventTypes.h"
#include "GUI/GUIEvent.h"

struct EventInfo
{
	EventInfo()
	: type(EventInfoType::SFML)
	, keycode(0)
	{
	}

	EventInfo(int32 keycode)
	: type(EventInfoType::SFML)
	/// @todo is this ok?
	, keycode(keycode)
	{
	}

	EventInfo(const GUIEvent& guiEvent)
	: type(EventInfoType::GUI)
	, guiEvent(guiEvent)
	{
	}

	EventInfo(const EventInfoType& type)
	{
		if(this->type == EventInfoType::GUI)
		{
			DestroyGUIStrings();
		}
		this->type = type;
		if(this->type == EventInfoType::GUI)
		{
			CreateGUIStrings("", "");
		}
	}

	EventInfo(const EventInfo& rhs)
	{
		Move(rhs);
	}

	EventInfo& operator=(const EventInfo& rhs)
	{
		if(&rhs != this)
		{
			Move(rhs);
		}
		return *this;
	}

	~EventInfo()
	{
		if(type == EventInfoType::GUI)
		{
			DestroyGUIStrings();
		}
	}

	union
	{
		int32 keycode;
		GUIEvent guiEvent;
	};

	EventInfoType type;
private:
	void Move(const EventInfo& rhs)
	{
		if(type == EventInfoType::GUI)
		{
			DestroyGUIStrings();
		}
		type = rhs.type;
		if(type == EventInfoType::SFML)
		{
			keycode = rhs.keycode;
		}
		else
		{
			CreateGUIStrings(rhs.guiEvent.interfaceName, rhs.guiEvent.elementName);
			guiEvent = rhs.guiEvent;
		}
	}

	void DestroyGUIStrings()
	{
		/*
		guiEvent.interfaceName.~basic_string();
		guiEvent.elementName.~basic_string();
		*/
	}

	void CreateGUIStrings(const string& interfaceName, const string& elementName)
	{
		new (&guiEvent.interfaceName) string(interfaceName);
		new (&guiEvent.elementName) string(elementName);
	}
};
