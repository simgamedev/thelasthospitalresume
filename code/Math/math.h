#pragma once
#include "../common.h"
#include "C2DMatrix.h"
#include <math.h>

namespace Math
{
	const real32 Pi = 3.1415926;
	const real32 TwoPi = Pi * 2;
	const real32 HalfPi = Pi / 2;
	const real32 QuarterPi = Pi / 4;
	/******************************* Distance ***************************************
	 ********************************************************************************/
	inline real32 Distance(const vec2f& pos1, const vec2f& pos2)
	{
		return std::sqrt((pos1.x - pos2.x) * (pos1.x - pos2.x) +
						 (pos1.y - pos2.y) * (pos1.y - pos2.y));
	}

	/******************************* DistanceSq ***************************************
	 * returns distance squared
	 ********************************************************************************/
	inline real32 DistanceSq(const vec2f& pos1, const vec2f& pos2)
	{
		return (pos1.x - pos2.x) * (pos1.x - pos2.x) + (pos1.y - pos2.y) * (pos1.y - pos2.y);
	}


	/******************************* RandInRange ************************************
	 ********************************************************************************/
	inline real32 RandInRange(real32 low, real32 high)
	{
		return 1.0f;
	}

	/******************************* RandIntRange ************************************
	 *
	 * Generates a random interger number in range
	 ********************************************************************************/
	inline int32 RandInRange(int32 low, int32 high)
	{
		return (rand() % high + low);
	}

	/******************************* RandFloat ************************************
	 *
	 * returns a random real32 between zero and 1
	 ********************************************************************************/
	inline real32 RandFloat() { return ((rand()) / (RAND_MAX + 1.0)); }

	/******************************* RandomClamped **********************************
	 *
	 * returns a random real32 in the range -1 < n < 1
	 ********************************************************************************/
	inline real32 RandomClamped()
	{
		return RandFloat() - RandFloat();
	}

	/******************************* PointToWorldSpace ******************************
	 *
	 * Transforms a point from the agent's local space into world space
	 ********************************************************************************/
	inline vec2f PointToWorldSpace(const vec2f& point,
								   const vec2f& agentHeading,
								   const vec2f& agentSide,
								   const vec2f& agentPosition)
	{
		// make a copy of the point
		vec2f transPoint = point;

		// create a tranformation matrix
		C2DMatrix matTranform;

		// rotate
		matTranform.Rotate(agentHeading, agentSide);

		// and translate
		matTranform.TransformVector2Ds(transPoint);

		return transPoint;
	}


	/******************************* Cos ********************************************
	 *
	 ********************************************************************************/
	inline real32 Cos(real32 angleInRadians)
	{
		return std::cosf(angleInRadians);
	}
	

	/******************************* Sin ********************************************
	 *
	 ********************************************************************************/
	inline real32 Sin(real32 angleInRadians)
	{
		return std::sinf(angleInRadians);
	}

	/******************************* Zero *******************************************
	 *
	 ********************************************************************************/
	inline void Zero(vec2f& vec2)
	{
		vec2.x = 0;
		vec2.y = 0;
	}

	/******************************* Dot *******************************************
	 *
	 ********************************************************************************/
	inline real32 Dot(const vec2f& a, const vec2f& b)
	{
		return(a.x * b.x + a.y + b.y);
	}

	/******************************* Length *******************************************
	 *
	 ********************************************************************************/
	inline real32 Length(const vec2f& vec2)
	{
		return std::sqrt(vec2.x * vec2.x + vec2.y * vec2.y);
	}

	/******************************* LengthSq *******************************************
	 *
	 ********************************************************************************/
	inline real32 LengthSq(const vec2f& vec2)
	{
		return (vec2.x * vec2.x + vec2.y * vec2.y);
	}

	/******************************* Normalize *******************************************
	 *
	 ********************************************************************************/
	inline vec2f Normalize(const vec2f& vec2)
	{
		//return vec2 / Length(vec2);
		if (Length(vec2) == 0.0f)
			return vec2f(0.0f, 0.0f);
		return vec2 / Length(vec2);
	}
}
