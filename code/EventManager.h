#pragma once
#include "common.h"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <unordered_map>
#include <functional>
#include <fstream>
#include <sstream>
#include <iostream>
#include "EventDetails.h"
#include "StateDependent.h"
#include "EventInfo.h"


/*
struct EventInfo
{
	EventInfo()
	{
		keycode = 0;
	}
	EventInfo(int32 key_code)
	{
		keycode = key_code;
	}
	union
	{
		int32 keycode;
	};
};
*/


/********************************************************************************
 * event detailed info for various different types of event.
 ********************************************************************************/
/*
struct EventDetails
{
	EventDetails(const string& binding_name)
	: bindingName(binding_name)
	{
		Clear();
	}
	string bindingName;

	//vec2i size_; ///< for window resize which we don't need
	sf::Uint32 textEntered;
	vec2i mousePos;
	int32 mouseWheelDelta;
	int32 keycode;

	void Clear()
	{
		//size_ = vec2i(0, 0);
		textEntered = 0;
		mousePos = vec2i(0, 0);
		mouseWheelDelta = 0;
		keycode = -1;
	}
};
*/

using Events = std::vector<std::pair<EventType, EventInfo>>;

struct Binding
{
	Binding(const string& l_name)
	: name(l_name)
	, eventDetails(l_name)
	, numEventsHappening(0)
	{
	}

	void BindEvent(EventType type, EventInfo info = EventInfo())
	{
		eventsShouldHappen.emplace_back(type, info);
	}

	string name;
	Events eventsShouldHappen;
	int32 numEventsHappening;
	EventDetails eventDetails;
};

enum class GameStateType;
using Bindings = std::unordered_map<string, Binding*>;
using CallbackContainer = std::unordered_map<string, std::function<void(EventDetails*)>>;
using Callbacks = std::unordered_map<GameStateType, CallbackContainer>;
/********************************************************************************
 * A Simpile Class that Manages all the input events.
 * We call the events bindings. And Bindings get specified
 * in keys.cfg file.
 ********************************************************************************/
class Window;
class EventManager : public StateDependent
{
public:
	EventManager(Window* window);
	~EventManager();

	bool AddBinding(Binding* binding);
	bool RemoveBinding(const string& name);
	void SetFocus(const bool& focus);
	//void SetCurrentGameState(GameStateType gameState);

	template<class T>
	bool AddCallback(const GameStateType& stateType, const string& bindingName,
					 void(T::*func)(EventDetails*), T* instance)
	{
		auto itr = callbacks_.emplace(stateType, CallbackContainer()).first;
		auto temp = std::bind(func, instance, std::placeholders::_1);
		return itr->second.emplace(bindingName, temp).second;
	}

	template<class T>
	bool AddCallback(const string& bindingName,
					 void(T::*func)(EventDetails*), T* instance)
	{
		return AddCallback(currentGameState_, bindingName, func, instance);
	}

	bool RemoveCallback(const GameStateType& stateType, const string& bindingName)
	{
		auto itr = callbacks_.find(stateType);
		if(itr == callbacks_.end()){ return false; }
		auto itr2 = itr->second.find(bindingName);
		if(itr2 == itr->second.end()){ return false; }
		itr->second.erase(bindingName);
		return true;
	}

	/** Handles sf::Event */
	void HandleEvent(sf::Event& event);
	/** Handles GUIEvent */
	void HandleEvent(GUIEvent& event);
	vec2i GetMousePos();
	void Update();

	// State Dependent
	void ChangeGameState(const GameStateType& gameState);
	void RemoveGameState(const GameStateType& gameState);
		
private:
	void LoadBindings();
	Bindings bindings_;
	Callbacks callbacks_;
	bool hasFocus_;
	//GameStateType currentGameState_;
	Window* window_;
};
