#pragma once
enum class GameStateType
{
	Intro = 1, MainMenu, Game, Paused, GameOver, Credits, Loading, MapEditor
};
