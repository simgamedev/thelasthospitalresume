#pragma once
#include "BaseState.h"
#include "EventManager.h"

class StateIntro : public BaseState
{
public:
	StateIntro(StateManager* stateManager);
	~StateIntro();

	void OnCreate();
	void OnDestroy();

	void Activate();
	void Deactivate();

	void Update(const sf::Time& time);
	void Draw();

	void Continue(EventDetails* eventDetails);
private:
	sf::Texture introTexture_;
	sf::Sprite introSprite_;
	sf::Font font_;
	sf::Text text_;

	real32 timePassed_;
};
