#include "StatePaused.h"
#include "StateManager.h"


/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
StatePaused::StatePaused(StateManager* stateManager)
	: BaseState(stateManager)
{
}

/********************************************************************************
 * Empty Destructor.
 ********************************************************************************/
StatePaused::~StatePaused(){}


void
StatePaused::OnCreate()
{
	SetTransparent(true);
	font_.loadFromFile("Zpix.ttf");
	text_.setFont(font_);
	//text.setString(sf::String("PAUSED"));
	text_.setString(string("PAUSED"));

	vec2u windowSize = stateManager_->GetContext()->window->GetRenderWindow()->getSize();

	sf::FloatRect textRect = text_.getLocalBounds();
	text_.setOrigin(textRect.left + textRect.width / 2.0f,
					textRect.top + textRect.height / 2.0f);
	text_.setPosition(windowSize.x / 2.0f,
					  windowSize.y / 2.0f);

	rectangle_.setSize(vec2f(windowSize));
	rectangle_.setPosition(0, 0);
	rectangle_.setFillColor(sf::Color(0, 0, 0, 150));

	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->AddCallback(GameStateType::Paused, "Key_Pause", &StatePaused::Unpause, this);
}

void
StatePaused::OnDestroy()
{
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->RemoveCallback(GameStateType::Paused, "Key_Pause");
}


void
StatePaused::Draw()
{
	sf::RenderWindow* window = stateManager_->GetContext()->window->GetRenderWindow();
	window->draw(rectangle_);
	window->draw(text_);
}


void
StatePaused::Unpause(EventDetails* eventDetails)
{
	stateManager_->SwitchTo(GameStateType::Game);
}

void StatePaused::Activate(){}
void StatePaused::Deactivate(){}
void StatePaused::Update(const sf::Time& time){}
