#include "GameStateData.h"
#include "Utilities.h"

// ---> Testing Lua
static int32 wrap_CreateLevel(lua_State* L);
static int32 wrap_SetTile(lua_State* L);
// ---< 

/****************************** GameStateData ***********************************
 * ctor
 ********************************************************************************/
GameStateData::GameStateData()
{
	ReadIn("");
}

/******************************* ReadIn ****************************************
 * Read data from lua file
 *******************************************************************************/
bool
GameStateData::ReadIn(const char* filename)
{
	LOG_ENGINE_INFO("Starting to load GameStateData from lua file");
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	lua_register(L, "_CreateLevel", wrap_CreateLevel);
	lua_register(L, "_SetTile", wrap_SetTile);
	
	string scriptPath = Utils::GetMediaDirectory() + "/Lua/test.lua";
	if (CheckLua(L, luaL_dofile(L, scriptPath.c_str())))
	{
		// ---> Read in BasicInfo
		// TODO: implement ReadLuaTable(const char* tableName, const char* keyName)
		// FIXME: where do you delete it?
		basicInfo_ = new BasicInfo();
		lua_getglobal(L, "BasicInfo");
		if (lua_istable(L, -1))
		{
			// ---> Cash
			lua_pushstring(L, "Cash");
			lua_gettable(L, -2);
			basicInfo_->SetCash((real32)lua_tonumber(L, -1));
			lua_pop(L, 1);
			// ---< Cash

			// ---> ResearchPoints
			lua_pushstring(L, "ResearchPoints");
			lua_gettable(L, -2);
			basicInfo_->SetResearchPoints((int32)lua_tonumber(L, -1));
			lua_pop(L, 1);
			// ---< ResearchPoints

			// ---> NumStaff
			lua_pushstring(L, "NumStaff");
			lua_gettable(L, -2);
			basicInfo_->SetNumStaff((int32)lua_tonumber(L, -1));
			lua_pop(L, 1);
			// ---< NumStaff

			// ---> StaffCapacity
			lua_pushstring(L, "StaffCapacity");
			lua_gettable(L, -2);
			basicInfo_->SetStaffCapacity((int32)lua_tonumber(L, -1));
			lua_pop(L, 1);
			// ---< StaffCapacity
		}
		// ---> ResearchPoints
		// ---< ResearchPoints
		// ---<
		/*
		lua_getglobal(L, "LoadLevel");
		if (lua_isfunction(L, -1))
		{
			lua_pushlightuserdata(L, this);
			lua_pushnumber(L, 1);
			if (CheckLua(L, lua_pcall(L, 2, 1, 0)))
			{
				string logString("Successfully executed lua script:" + scriptPath);
				LOG_ENGINE_INFO(logString);
				return true;
			}
		}
		*/
	}
	else
	{
		string logString("Failed to execute lua script: " + scriptPath);
		LOG_ENGINE_ERROR(logString);
	}

	return false;
}


// ---> Testing Lua
void
GameStateData::CreateLevel(int32 w, int32 h)
{
	LOG_ENGINE_INFO("[LUA] cpp fun CreateLevel called from lua!");
	int32 a = 100;
	int32 b = 200;
}

void
GameStateData::SetTile(int32 x, int32 y)
{
}

static int32 wrap_SetTile(lua_State* L)
{
	return 0;
}

static int32 wrap_CreateLevel(lua_State* L)
{
	// ensure lua code provied 3 arguments
	// TODO: Formally handle lua errors
	if (lua_gettop(L) != 3) return -1;
	GameStateData* object = static_cast<GameStateData*>(lua_touserdata(L, 1));
	int32 w = lua_tointeger(L, 2);
	int32 h = lua_tointeger(L, 3);
	object->CreateLevel(w, h);
	return 0;
}
// ---< Testing Lua


/******************************* CheckLua **************************************
 *
 *******************************************************************************/
bool
GameStateData::CheckLua(lua_State* L, int r)
{
	if (r != LUA_OK)
	{
		string errorMsg = lua_tostring(L, -1);
		LOG_ENGINE_ERROR("Lua Error: " + errorMsg);
		return false;
	}
	return true;
}

/****************************** ~GameStateData **********************************
 * destructor
 ********************************************************************************/
GameStateData::~GameStateData()
{
}

/******************************* WriteOut ***************************************
 * Write GameStateData to a lua file
 ********************************************************************************/
bool
GameStateData::WriteOut(const char* filename)
{
	return true;
}


NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(GameStateData)
{
	NsProp("BasicInfo", &GameStateData::GetBasicInfo, &GameStateData::SetBasicInfo);
}
