#pragma once

#include <NsCore/Noesis.h>
#include <NsCore/TypeOf.h>
#include <NsCore/RegisterComponent.h>
#include <NsCore/ReflectionImplement.h>
#include <NsGui/BaseValueConverter.h>


namespace MYGUI
{
class MultiTextConverter final : public Noesis::BaseMultiValueConverter
{
public:
    bool TryConvert(Noesis::ArrayRef<Noesis::BaseComponent*> values, const Noesis::Type*,
					Noesis::BaseComponent*,
					Noesis::Ptr<Noesis::BaseComponent>& result) override
    {
        // ---> LayFloor
        if (values.Size() == 4)
        {
            Noesis::String str1 = Noesis::Boxing::Unbox<Noesis::String>(values[0]);
            Noesis::String str2 = Noesis::Boxing::Unbox<Noesis::String>(values[1]);
            Noesis::String str3 = Noesis::Boxing::Unbox<Noesis::String>(values[2]);
            int32 tileID = Noesis::Boxing::Unbox<int32>(values[3]);
			Noesis::String resultStr = str1 + str2 + str3 + Noesis::String(STR(tileID).c_str());
			result = Noesis::Boxing::Box(resultStr);
        }
        // ---> AddDoor
        if (values.Size() == 2)
        {
            Noesis::String str1 = Noesis::Boxing::Unbox<Noesis::String>(values[0]);
            Noesis::String str2 = Noesis::Boxing::Unbox<Noesis::String>(values[1]);
            Noesis::String resultStr = str1 + str2;
			result = Noesis::Boxing::Box(resultStr);
        }

        //result = Boxing::Box(Color(r, g, b));
        return true;
    }

    NS_IMPLEMENT_INLINE_REFLECTION_(MultiTextConverter, Noesis::BaseMultiValueConverter,
        "MYGUI.MultiTextConverter");
};
}
