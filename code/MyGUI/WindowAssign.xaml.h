#pragma once

#include <NsCore/Noesis.h>
#include <NsGui/UserControl.h>


namespace MYGUI
{
class WindowAssign : public Noesis::UserControl
{
public:
	WindowAssign();
	~WindowAssign();
//void SetStateGame(StateGame* StateGame);
private:
	void InitializeComponent();
	void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	
	NS_DECLARE_REFLECTION(WindowAssign, UserControl);
};
}
