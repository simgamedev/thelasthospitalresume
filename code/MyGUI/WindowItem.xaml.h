#pragma once

#include <NsCore/Noesis.h>
#include <NsGui/UserControl.h>


namespace MYGUI
{
class WindowItem : public Noesis::UserControl
{
public:
	WindowItem();
	~WindowItem();
//void SetStateGame(StateGame* StateGame);
private:
	void InitializeComponent();
	void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	
	NS_DECLARE_REFLECTION(WindowItem, UserControl);
};
}
