#include "WindowDebug.xaml.h"


#include <NsGui/IntegrationAPI.h>
//#include <NsCore/ReflectionImplementEmpty.h>

using namespace MYGUI;
using namespace Noesis;


WindowDebug::WindowDebug()
{
	Initialized() += Noesis::MakeDelegate(this, &WindowDebug::OnInitialized);
	InitializeComponent();
}

void
WindowDebug::InitializeComponent()
{
	GUI::LoadComponent(this, "WindowDebug.xaml");
}

void
WindowDebug::OnInitialized(BaseComponent*, const EventArgs&)
{
	//SetDataContext(MakePtr<StateGameViewModel>());
}


WindowDebug::~WindowDebug()
{
}

NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(WindowDebug, "MYGUI.WindowDebug")
{
}
