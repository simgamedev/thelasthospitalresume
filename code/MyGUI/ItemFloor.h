#pragma once

#include <NoesisPCH.h>
#include "../common.h"
#include "../Utilities.h"
#include <fstream>
#include <iostream>

class ItemFloor final : public Noesis::BaseComponent
{
public:
	ItemFloor(const char* filepath)
	{
		ReadIn(filepath);
	}	

	// ---> Name
	const char* GetName() const { return _name.Str(); }
	void SetName(const char* name) { _name = name; }
	// ---<

	// ---> Price
	int32 GetPrice() const { return _price; }
	void SetPrice(const int32 price) { _price = price; }
	// ---< Price

	// ---> Icon Path
	const char* GetIconPath() const { return _iconPath.Str(); }
	void SetIconPath(const char* iconPath) { _iconPath = iconPath; }
	// ---< Icon Path

	// ---> TileSetName
	const char* GetTileSetName() const { return _tileSetName.Str(); }
	void SetTileSetName(const char* tileSetName) { _tileSetName = tileSetName; }
	// ---< TileSetName

	// ---> TileID
	int32 GetTileID() const { return _tileID; }
	void SetTileID(const int32 tileID) { _tileID = tileID; }
	// ---< TileID
private:
	void ReadIn(const char* filename)
	{
		std::ifstream file;
		file.open(Utils::GetMediaDirectory() + "Entities/Floors/" + filename);
		if(!file.is_open())
		{
			return;
		}
		string line;
		string key;
		while(std::getline(file, line))
		{
			if(line[0] == '|')
				continue;
			std::stringstream linestream(line);
			linestream >> key;
			if (key == "Name")
			{
				string name;
				linestream >> name;
				SetName(name.c_str());
			}
			else if (key == "Price")
				linestream >> _price;
			else if (key == "TileID")
				linestream >> _tileID;
			else if(key == "IconPath")
			{
				string iconPath;
				linestream >> iconPath;
				SetIconPath(iconPath.c_str());
			}
			else if(key == "TileSetName")
			{
				string tileSetName;
				linestream >> tileSetName;
				SetTileSetName(tileSetName.c_str());
			}
		}
	}
private:
	Noesis::String _name;
	Noesis::String _iconPath;
	Noesis::String _tileSetName;
	int32 _tileID;
	int32 _price;
	string _cppString;
private:
	NS_IMPLEMENT_INLINE_REFLECTION(ItemFloor, Noesis::BaseComponent)
	{
		NsProp("Name", &ItemFloor::GetName, &ItemFloor::SetName);
		NsProp("Price", &ItemFloor::GetPrice, &ItemFloor::SetPrice);
		NsProp("IconPath", &ItemFloor::GetIconPath, &ItemFloor::SetIconPath);
		NsProp("TileSetName", &ItemFloor::GetTileSetName, &ItemFloor::SetTileSetName);
		NsProp("TileID", &ItemFloor::GetTileID, &ItemFloor::SetTileID);
	}
};
