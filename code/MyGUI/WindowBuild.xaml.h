#ifndef WINDOW_BUILD_H
#define WINDOW_BUILD_H

#include <NsCore/Noesis.h>
#include <NsGui/UserControl.h>


namespace MYGUI
{
class WindowBuild : public Noesis::UserControl
{
public:
	WindowBuild();
	~WindowBuild();
//void SetStateGame(StateGame* StateGame);
private:
	void InitializeComponent();
	void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	
	NS_DECLARE_REFLECTION(WindowBuild, UserControl);
};
}

#endif
