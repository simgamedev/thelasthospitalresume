#include "ButtonsBarUserControl.h"


#include <NsCore/Noesis.h>
#include <NsCore/ReflectionImplementEmpty.h>
#include <NsCore/RegisterComponent.h>
#include <NsGui/Button.h>
#include <NsGui/IntegrationAPI.h>


using namespace MYGUI;
using namespace Noesis;


ButtonsBarUserControl::ButtonsBarUserControl()
{
	Initialized() += Noesis::MakeDelegate(this, &ButtonsBarUserControl::OnInitialized);
	InitializeComponent();
}

void
ButtonsBarUserControl::InitializeComponent()
{
	GUI::LoadComponent(this, "ButtonsBarUserControl.xaml");
}

void
ButtonsBarUserControl::OnInitialized(BaseComponent*, const EventArgs&)
{
	//SetDataContext(MakePtr<StateGameViewModel>());
}


/******************************* ToggleActiveButton *****************************
 * 
 ********************************************************************************/
void
ButtonsBarUserControl::ToggleActiveButton()
{
}

ButtonsBarUserControl::~ButtonsBarUserControl()
{
}

NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(ButtonsBarUserControl, "MYGUI.ButtonsBarUserControl")
{
}
