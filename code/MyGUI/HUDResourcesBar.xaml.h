#pragma once

#include <NsCore/Noesis.h>
#include <NsGui/UserControl.h>

namespace MYGUI
{
class HUDResourcesBar : public Noesis::UserControl
{
public:
	HUDResourcesBar();
	~HUDResourcesBar();
//void SetStateGame(StateGame* StateGame);
	void ToggleActiveButton();
private:
	void InitializeComponent();
	void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	
	NS_DECLARE_REFLECTION(HUDResourcesBar, UserControl);
};
}
