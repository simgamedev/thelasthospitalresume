#pragma once

#include <NoesisPCH.h>
#include <NsApp/NotifyPropertyChangedBase.h>
#include "../common.h"

namespace MYGUI
{

class Tile : public NotifyPropertyChangedBase
{
public:
	Tile(bool solid,
		 const char* tileType,
		 int32 roomID,
		 bool hasDoor,
		 int32 entityID)
		: _solid(solid)
		, _tileType(tileType)
		, _roomID(roomID)
		, _hasDoor(hasDoor)
		, _entityID(entityID)
	{
	}	

	// ---> Solid
	bool GetSolid() const
	{
		return _solid;
	}
	void SetSolid(const bool value)
	{
		_solid = value;
		OnPropertyChanged("Solid");
	}

	// ---> TileType
	const char* GetTileType() const
	{
		return _tileType.Str();
	}
	void SetTileType(const char* tileType)
	{
		_tileType = tileType;
		OnPropertyChanged("TileType");
	}

	// ---> RoomID
	int32 GetRoomID() const
	{
		return _roomID;
	}
	void SetRoomID(const int32 roomID)
	{
		_roomID = roomID;
	}
	// ---> EntityID
	int32 GetEntityID() const
	{
		return _entityID;
	}
	void SetEntityID(const int32 entityID)
	{
		_entityID = entityID;
	}
	// ---< EntityID

	// ---> HasDoor
	bool GetHasDoor() const { return _hasDoor; }
	void SetHasDoor(const bool hasDoor) { _hasDoor = hasDoor;  OnPropertyChanged("HasDoor"); }
	// ---< HasDoor

private:
	bool _solid;
	bool _hasDoor;
	Noesis::String _tileType;
	int32 _roomID;
	int32 _entityID;
private:
	NS_IMPLEMENT_INLINE_REFLECTION(Tile, NotifyPropertyChangedBase)
	{
		NsProp("Solid", &Tile::GetSolid, &Tile::SetSolid);
		NsProp("TileType", &Tile::GetTileType, &Tile::SetTileType);
		NsProp("RoomID", &Tile::GetRoomID, &Tile::SetRoomID);
		NsProp("HasDoor", &Tile::GetHasDoor, &Tile::SetHasDoor);
		NsProp("EntityID", &Tile::GetEntityID, &Tile::SetEntityID);
	}
};

}
