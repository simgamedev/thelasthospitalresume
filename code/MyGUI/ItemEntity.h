#pragma once

#include <NoesisPCH.h>
#include "../common.h"
#include "../Utilities.h"
#include <fstream>
#include <iostream>

class ItemEntity final : public Noesis::BaseComponent
{
public:
	ItemEntity(const char* filepath)
	{
		ReadIn(filepath);
		int32 a = 100;
	}	

	// ---> Name
	const char* GetName() const { return _name.Str(); }
	void SetName(const char* name) { _name = name; }
	// ---<

	// ---> Price
	int32 GetPrice() const { return _price; }
	void SetPrice(const int32 price) { _price = price; }
	// ---< Price

	// ---> Icon Path
	const char* GetIconPath() const { return _iconPath.Str(); }
	void SetIconPath(const char* iconPath) { _iconPath = iconPath; }
	// ---< Icon Path

	// ---> EntityFilePath
	const char* GetEntityFilePath() const { return _entityFilePath.Str(); }
	void SetEntityFilePath(const char* entityFilePath) { _entityFilePath = entityFilePath; }
	// ---< EntityFilePath

	// ---> TooltipText
	const char* GetTooltipText() const { return _tooltipText.Str(); }
	void SetTooltipText(const char* tooltipText) { _tooltipText = tooltipText; }
	// ---< TooltipText

private:
	Noesis::String _name;
	Noesis::String _iconPath;
	Noesis::String _entityFilePath;
	Noesis::String _tooltipText;
	int32 _price;
private:
	NS_IMPLEMENT_INLINE_REFLECTION(ItemEntity, Noesis::BaseComponent)
	{
		NsProp("Name", &ItemEntity::GetName, &ItemEntity::SetName);
		NsProp("Price", &ItemEntity::GetPrice, &ItemEntity::SetPrice);
		NsProp("IconPath", &ItemEntity::GetIconPath, &ItemEntity::SetIconPath);
		NsProp("EntityFilePath", &ItemEntity::GetEntityFilePath, &ItemEntity::SetEntityFilePath);
		NsProp("TooltipText", &ItemEntity::GetTooltipText, &ItemEntity::SetTooltipText);
	}
private:
	void ReadIn(const char* filename)
	{
		std::ifstream file;
		file.open(Utils::GetMediaDirectory() + "Entities/" + filename);
		if(!file.is_open())
		{
			return;
		}
		string line;
		string key;
		while(std::getline(file, line))
		{
			if(line[0] == '|')
				continue;
			std::stringstream linestream(line);
			linestream >> key;
			if (key == "Name")
			{
				string name;
				linestream >> name;
				SetName(name.c_str());
			}
			else if (key == "TooltipText")
			{
				string tooltipText;
				linestream >> tooltipText;
				SetTooltipText(tooltipText.c_str());
			}
			else if (key == "Price")
				linestream >> _price;
			else if(key == "IconPath")
			{
				string iconPath;
				linestream >> iconPath;
				SetIconPath(iconPath.c_str());
			}
			else if(key == "EntityFilePath")
			{
				string entityFilePath;
				linestream >> entityFilePath;
				SetEntityFilePath(entityFilePath.c_str());
			}
		}
	}
};
