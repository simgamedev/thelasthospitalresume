#include "GamePlayGUI.xaml.h"

#include <NsCore/Noesis.h>
#include <NsGui/IntegrationAPI.h>
#include <NsGui/UIElementCollection.h>
#include <NsDrawing/Thickness.h>
#include <NsCore/Nullable.h>

#include "StateGameViewModel.h"
// ---> SubMenu Windows
#include "WindowBuild.xaml.h"
#include "WindowAssign.xaml.h"
#include "WindowItem.xaml.h"
//#include "WindowHire.xaml.h"
// ---<

#include <iostream>



using namespace Noesis;
using namespace MYGUI;

GamePlayGUI::GamePlayGUI()
	: _activeWindow(nullptr)
	, _uiScale(1.0f)
{
	//GUI::LoadComponent(this, "Settings.xaml");
	Initialized() += MakeDelegate(this, &GamePlayGUI::OnInitialized);
	InitializeComponent();
}

void
GamePlayGUI::InitializeComponent()
{
	GUI::LoadComponent(this, "GamePlayGUI.xaml");
}

void
GamePlayGUI::OnInitialized(BaseComponent*, const EventArgs&)
{
	// DIG: DataContext is set from StateGame.cpp, I think this is a better MVVM structure??
	//SetDataContext(MakePtr<StateGameViewModel>(this));
	_rootGrid = this->FindName<Noesis::Grid>("rootGrid");
	_buttonsBar = this->FindName<ButtonsBarUserControl>("buttonsBar");
}


/******************************* OpenWindow *************************************
 * Open a sub menu window
 ********************************************************************************/
void
GamePlayGUI::OpenWindow(const string& windowName)
{
	// ---> close active window first
	if (_activeWindow != nullptr)
		CloseActiveWindow();
	// ---<

	// ---> windowBuild
	if (windowName == "Build")
	{
		_activeWindow = new WindowBuild();
		_activeWindow->SetName("windowBuild");
		_activeBtnsBarBtn = _buttonsBar->FindName<Noesis::RadioButton>("btnBuild");
	}
	// ---> windowAssign
	else if (windowName == "Assign")
	{
		_activeWindow = new WindowAssign();
		_activeWindow->SetName("windowAssign");
		_activeBtnsBarBtn = _buttonsBar->FindName<Noesis::RadioButton>("btnAssign");
	}
	// ---> windowItem
	else if (windowName == "Item")
	{
		_activeWindow = new WindowItem();
		_activeWindow->SetName("windowItem");
		_activeBtnsBarBtn = _buttonsBar->FindName<Noesis::RadioButton>("btnItem");
	}
	// ---> windowHire
	else if (windowName == "Hire")
	{
		//_activeWindow = new WindowHire();
		//_activeWindow->SetName("windowHire");
	}

	_activeWindow->SetValue<uint32>(RowProperty, (uint32)2);
	_activeWindow->SetVerticalAlignment(VerticalAlignment::VerticalAlignment_Bottom);
	_activeWindow->SetMargin(Thickness(5, 0, 0, 0));
	_activeWindow->SetHeight(200);
	_rootGrid->GetChildren()->Add(_activeWindow);
}


/******************************* CloseActiveWindow ******************************
 * Private function to close the active sub menu window
 ********************************************************************************/
void
GamePlayGUI::CloseActiveWindow()
{
	if (_activeWindow == nullptr)
		return;
	_rootGrid->GetChildren()->Remove(_activeWindow);
	_activeBtnsBarBtn->SetIsChecked(Noesis::Nullable<bool>(false));
	//_activeWindow->Release();
	// FIXME: memory leak?
	_activeWindow = nullptr;
}


/******************************* OnKeyboardKeyDown ******************************
 * Private function to close the active sub menu window
 ********************************************************************************/
void
GamePlayGUI::OnKeyboardKeyDown(Noesis::BaseComponent* pSender, const Noesis::KeyEventArgs& args)
{
	switch (args.key)
	{
		case Noesis::Key::Key_Escape:
		{
			LOG_GAME_INFO("Noesis received Key_Escape!");
			args.handled = true;
		} break;
	}
}

/******************************* ShowDebugWindow ********************************
 *
 ********************************************************************************/
void
GamePlayGUI::ShowDebugWindow()
{
	_windowDebug = Noesis::MakePtr<WindowDebug>();
	_windowDebug->SetVerticalAlignment(VerticalAlignment::VerticalAlignment_Top);
	_windowDebug->SetValue<uint32>(RowProperty, (uint32)0);
	_windowDebug->SetValue<uint32>(RowSpanProperty, (uint32)3);
	_windowDebug->SetValue<uint32>(ColumnProperty, (uint32)2);
	_windowDebug->SetMargin(Thickness(5, 0, 0, 0));
	_rootGrid->GetChildren()->Add(_windowDebug);
}

/******************************* IsMOuseOver ************************************
 *
]********************************************************************************/
bool
GamePlayGUI::IsMouseOverAnyControl() const
{
	Noesis::UIElementCollection* children = _rootGrid->GetChildren();
	int32 size = children->Count();
	for (int32 i = 0; i < size; i++)
	{
		Noesis::UIElement* child = children->Get(i);
		if (child->GetIsMouseOver())
			return true;
	}
	return false;
}

bool
GamePlayGUI::ConnectEvent(BaseComponent* source, const char* event, const char* handler)
{
	NS_CONNECT_EVENT(Noesis::Grid, KeyDown, OnKeyboardKeyDown);
	return false;
}



GamePlayGUI::~GamePlayGUI()
{
}

NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(GamePlayGUI)
{
}

