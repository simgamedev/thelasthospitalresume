
#include <NsCore/Noesis.h>
#include <NsCore/ReflectionImplement.h>
#include <NsGui/Button.h>
#include <NsGui/UIElementData.h>
#include <NsGui/IntegrationAPI.h>
#include <NsGui/FrameworkPropertyMetadata.h>

#include "MainMenuGUI.xaml.h"

#include "../StateMainMenu.h"

using namespace Noesis;
using namespace MYGUI;


MainMenuGUI::MainMenuGUI()
	: pStateMainMenu_(nullptr)
{
	Initialized() += Noesis::MakeDelegate(this, &MainMenuGUI::OnInitialized);
	InitializeComponent();
}

void
MainMenuGUI::SetStateMainMenu(StateMainMenu* stateMainMenu)
{
	pStateMainMenu_ = stateMainMenu;
}

MainMenuGUI::~MainMenuGUI()
{
}

void
MainMenuGUI::InitializeComponent()
{
	GUI::LoadComponent(this, "MainMenu.xaml");
}

void
MainMenuGUI::OnInitialized(BaseComponent*, const EventArgs&)
{
	LOG_GAME_INFO("You should always use the Initialized event");
}

bool
MainMenuGUI::ConnectEvent(BaseComponent* source, const char* event, const char* handler)
{
	NS_CONNECT_EVENT(Button, Click, OnBtnNewGame);
	NS_CONNECT_EVENT(Button, Click, OnBtnLoadGame);
	NS_CONNECT_EVENT(Button, Click, OnBtnCredits);
	NS_CONNECT_EVENT(Button, Click, OnBtnExit);
	return false;
}

void MainMenuGUI::OnBtnNewGame(Noesis::BaseComponent* pSender, const Noesis::RoutedEventArgs& args)
{
	pStateMainMenu_->NewGame();
}

void
MainMenuGUI::OnBtnLoadGame(Noesis::BaseComponent* pSender, const Noesis::RoutedEventArgs& args)
{
}

void
MainMenuGUI::OnBtnCredits(Noesis::BaseComponent* pSender, const Noesis::RoutedEventArgs& args)
{
}

void
MainMenuGUI::OnBtnExit(Noesis::BaseComponent* pSender, const Noesis::RoutedEventArgs& args)
{
	pStateMainMenu_->Exit();
}

void
MainMenuGUI::Toggle()
{
	if(this->GetVisibility() == Visibility::Visibility_Visible)
		this->SetVisibility(Visibility::Visibility_Hidden);
	else
		this->SetVisibility(Visibility::Visibility_Visible);
}



NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(MainMenuGUI)
{
}

