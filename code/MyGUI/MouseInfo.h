#pragma once

#include <NoesisPCH.h>
#include <NsApp/NotifyPropertyChangedBase.h>
#include "../common.h"
#include <iostream>

namespace MYGUI
{

class MouseInfo : public NotifyPropertyChangedBase
{
public:
	MouseInfo()
	{
	}	
	// ---> TilePos
	int32 GetTilePosX() const
	{
		return _tilePosX;
	}
	void SetTilePosX(const int32 tilePosX)
	{
		_tilePosX = tilePosX;
		OnPropertyChanged("TilePosX");
	}
	int32 GetTilePosY() const
	{
		return _tilePosY;
	}
	void SetTilePosY(const int32 tilePosY)
	{
		_tilePosY = tilePosY;
		OnPropertyChanged("TilePosY");
	}
	// ---< TilePos

	// ---> Dragging
	int32 GetDraggingXRangeStart() const
	{
		return _draggingXRangeStart;
	}
	void SetDraggingXRangeStart(const int32 value)
	{
		_draggingXRangeStart = value;
		OnPropertyChanged("DraggingXRangeStart");
	}
	int32 GetDraggingXRangeEnd() const
	{
		return _draggingXRangeEnd;
	}
	void SetDraggingXRangeEnd(const int32 value)
	{
		_draggingXRangeEnd = value;
		OnPropertyChanged("DraggingXRangeEnd");
	}
	int32 GetDraggingYRangeStart() const
	{
		return _draggingYRangeStart;
	}
	void SetDraggingYRangeStart(const int32 value)
	{
		_draggingYRangeStart = value;
		OnPropertyChanged("DraggingYRangeStart");
	}
	int32 GetDraggingYRangeEnd() const
	{
		return _draggingYRangeEnd;
	}
	void SetDraggingYRangeEnd(const int32 value)
	{
		_draggingYRangeEnd = value;
		OnPropertyChanged("DraggingYRangeEnd");
	}
	// ---< Dragging


private:
	int32 _tilePosX;
	int32 _tilePosY;
	int32 _draggingXRangeStart;
	int32 _draggingXRangeEnd;
	int32 _draggingYRangeStart;
	int32 _draggingYRangeEnd;
private:
	NS_IMPLEMENT_INLINE_REFLECTION(MouseInfo, NotifyPropertyChangedBase)
	{
		NsProp("TilePosX", &MouseInfo::GetTilePosX, &MouseInfo::SetTilePosX);
		NsProp("TilePosY", &MouseInfo::GetTilePosY, &MouseInfo::SetTilePosY);
		NsProp("DraggingXRangeStart", &MouseInfo::GetDraggingXRangeStart, &MouseInfo::SetDraggingXRangeStart);
		NsProp("DraggingXRangeEnd", &MouseInfo::GetDraggingXRangeEnd, &MouseInfo::SetDraggingXRangeEnd);
		NsProp("DraggingYRangeStart", &MouseInfo::GetDraggingYRangeStart, &MouseInfo::SetDraggingYRangeStart);
		NsProp("DraggingYRangeEnd", &MouseInfo::GetDraggingYRangeEnd, &MouseInfo::SetDraggingYRangeEnd);
	}
};

}
