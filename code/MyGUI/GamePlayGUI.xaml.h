#pragma once

#include <NsCore/ReflectionDeclare.h>
#include <NsGui/Grid.h>
#include <NsGui/IView.h>
#include <NsGui/ToggleButton.h>
#include <NsGui/RadioButton.h>

#include "ButtonsBarUserControl.h"
#include "WindowDebug.xaml.h"

#include "../common.h"


/**
   A wrapper class for GamePlayGUI.xaml, i.e. code behind
 */
namespace MYGUI
{

class GamePlayGUI : public Noesis::Grid
{
public:
	GamePlayGUI();
	~GamePlayGUI();
	//void SetStateGame(StateGame* StateGame);
	void OpenWindow(const string& windowName);
	void CloseActiveWindow();
	void ShowDebugWindow();

	bool ConnectEvent(BaseComponent* source, const char* event, const char* handler) override;
	void OnKeyboardKeyDown(Noesis::BaseComponent* pSender, const Noesis::KeyEventArgs& args);
	bool IsMouseOverAnyControl() const;
private:
	void InitializeComponent();
	void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	Noesis::UserControl* _activeWindow;
	Noesis::Ptr<WindowDebug> _windowDebug;
	ButtonsBarUserControl* _buttonsBar;
	Noesis::RadioButton* _activeBtnsBarBtn;
	Noesis::Grid* _rootGrid;
	real32 _uiScale;
private:
	NS_DECLARE_REFLECTION(GamePlayGUI, Noesis::Grid)
};

}
