#include "HUDResourcesBar.xaml.h"


#include <NsCore/Noesis.h>
#include <NsCore/ReflectionImplementEmpty.h>
#include <NsCore/RegisterComponent.h>
#include <NsGui/Button.h>
#include <NsGui/IntegrationAPI.h>


using namespace MYGUI;
using namespace Noesis;


HUDResourcesBar::HUDResourcesBar()
{
	Initialized() += Noesis::MakeDelegate(this, &HUDResourcesBar::OnInitialized);
	InitializeComponent();
}

void
HUDResourcesBar::InitializeComponent()
{
	GUI::LoadComponent(this, "HUDResourcesBar.xaml");
}

void
HUDResourcesBar::OnInitialized(BaseComponent*, const EventArgs&)
{
	//SetDataContext(MakePtr<StateGameViewModel>());
}


/******************************* ToggleActiveButton *****************************
 * 
 ********************************************************************************/
void
HUDResourcesBar::ToggleActiveButton()
{
}

HUDResourcesBar::~HUDResourcesBar()
{
}

NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(HUDResourcesBar, "MYGUI.HUDResourcesBar")
{
}
