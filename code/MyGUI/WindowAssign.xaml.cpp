#include "WindowAssign.xaml.h"


#include <NsCore/Noesis.h>
#include <NsGui/Button.h>
#include <NsGui/IntegrationAPI.h>

using namespace MYGUI;
using namespace Noesis;


WindowAssign::WindowAssign()
{
	Initialized() += Noesis::MakeDelegate(this, &WindowAssign::OnInitialized);
	InitializeComponent();
}

void
WindowAssign::InitializeComponent()
{
	GUI::LoadComponent(this, "WindowAssign.xaml");
}

void
WindowAssign::OnInitialized(BaseComponent*, const EventArgs&)
{
	//SetDataContext(MakePtr<StateGameViewModel>());
}


WindowAssign::~WindowAssign()
{
}

NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(WindowAssign, "MYGUI.WindowAssign")
{
}
