#include "StateGameViewModel.h"
#include <iostream>

#include "GamePlayGUI.xaml.h"
#include "../StateGame.h"


using namespace MYGUI;
using namespace Noesis;

StateGameViewModel::StateGameViewModel(GamePlayGUI* gamePlayGUI)
	: _gamePlayGUI(gamePlayGUI)
	, pStateGame_(nullptr)
{
	_debugText = "Debug info here!";
	_openWindowCommand.SetExecuteFunc(MakeDelegate(this, &StateGameViewModel::OpenWindow));
	_closeWindowCommand.SetExecuteFunc(MakeDelegate(this, &StateGameViewModel::CloseWindow));
	_changeGameSubModeCommand.SetExecuteFunc(MakeDelegate(this, &StateGameViewModel::ChangeGameSubMode));
	_escPressedCommand.SetExecuteFunc(MakeDelegate(this, &StateGameViewModel::EscPressed));
	_showDebugWindowCommand.SetExecuteFunc(MakeDelegate(this, &StateGameViewModel::ShowDebugWindow));
	_toggleTileGridCommand.SetExecuteFunc(MakeDelegate(this, &StateGameViewModel::ToggleTileGrid));


	// ---> ItemFloors
	auto itemFloorFiles = Utils::GetFileList(Utils::GetMediaDirectory() + 
											"Entities/Floors/" ,"*.floor");
	_itemFloors = *new Noesis::ObservableCollection<ItemFloor>();
	for (auto& itr : itemFloorFiles)
	{
		LOG_ENGINE_ERROR(itr.first);
		// FIXME: where do you delete them?
		Noesis::Ptr<ItemFloor> itemFloor = *new ItemFloor(string(itr.first).c_str());
		_itemFloors->Add(itemFloor);
	}
	// ---< ItemFloors

	// ---> ItemDoors
	auto itemDoorFiles = Utils::GetFileList(Utils::GetMediaDirectory() + 
											"Entities/Doors/" ,"*.item");
	_itemDoors = *new Noesis::ObservableCollection<ItemDoor>();
	for (auto& itr : itemDoorFiles)
	{
		LOG_ENGINE_ERROR(itr.first);
		// FIXME: where do you delete them?
		Noesis::Ptr<ItemDoor> itemDoor = *new ItemDoor(string(itr.first).c_str());
		_itemDoors->Add(itemDoor);
	}
	// ---< ItemDoors

	// ---> ItemWallpapers
	auto itemWallpaperFiles = Utils::GetFileList(Utils::GetMediaDirectory() + 
											     "Entities/Walls/" ,"*.item");
	_itemWallpapers = *new Noesis::ObservableCollection<ItemWallpaper>();
	for (auto& itr : itemWallpaperFiles)
	{
		LOG_ENGINE_ERROR(itr.first);
		// FIXME: where do you delete them?
		Noesis::Ptr<ItemWallpaper> itemWallpaper = *new ItemWallpaper(string(itr.first).c_str());
		_itemWallpapers->Add(itemWallpaper);
	}
	// ---< ItemWallpapers

	// ---< ItemFurnitures
	// TODO: these need to be loaded in-time when tab is opened, not preloaded.
	auto itemFurnitureFiles = Utils::GetFileList(Utils::GetMediaDirectory() +
												 "Entities/Furnitures/", "*.item");
	_itemFurnitures = *new Noesis::ObservableCollection<ItemEntity>();
	for (auto& itr : itemFurnitureFiles)
	{
		string longerPath("Furnitures/" + string(itr.first));
		Noesis::Ptr<ItemEntity> itemFurniture = *new ItemEntity(longerPath.c_str());
		_itemFurnitures->Add(itemFurniture);
	}
	// ---< ItemFurnitures

	SetMouseInfo(new MouseInfo());
}

StateGameViewModel::~StateGameViewModel()
{
}


/******************************* OpenWindow *************************************
 * Opens a submenu window by loading a usercontrol
 ********************************************************************************/
void
StateGameViewModel::OpenWindow(BaseComponent* _param)
{
	Noesis::String nString = _param->ToString();
	string part1("Should open window ");
	string part2(nString.Str());
	string result = part1 + part2;

	SetDebugText(result.c_str());

	_gamePlayGUI->OpenWindow(part2);
}

/******************************* CloseWindow ************************************
 * Closes a submenu window
 ********************************************************************************/
void
StateGameViewModel::CloseWindow(BaseComponent* _param)
{
	Noesis::String nString = _param->ToString();
	string part1("Should close window ");
	string part2(nString.Str());
	string result = part1 + part2;

	SetDebugText(result.c_str());

	//pStateGame_->ChangeSubMode(SubMode::Normal);
	_gamePlayGUI->CloseActiveWindow();
}

/******************************* ChangeGameSubMode ********************************
 * Changes game's submode(build/buy/etc.)
 ********************************************************************************/
void
StateGameViewModel::ChangeGameSubMode(BaseComponent* param_)
{
	/*
	if (Noesis::Boxing::CanUnbox<Noesis::TextBlock>(param_))
	{
		int32 a = 100;
		//TextBlock textBlock = Boxing::UnBox<TextBlock>(param_);
	}
	*/
	string param(param_->ToString().Str());
	string delimiter = "-";
	string mode = param.substr(0, param.find(delimiter));
	string parameters = param.substr(param.find(delimiter)+1, param.size());
	if (mode == "BuildFoundation")
	{
		pStateGame_->ChangeSubMode(SubMode::BuildFoundation);
	}
	else if (mode == "BuildWall")
	{
		pStateGame_->ChangeSubMode(SubMode::BuildWall);
	}
	else if (mode == "Bulldozing")
	{
		pStateGame_->ChangeSubMode(SubMode::Bulldozing);
	}
	else if (mode == "LayFloor")
	{
		pStateGame_->ChangeSubMode(SubMode::LayFloor, parameters.c_str());
	}
	else if (mode == "AddDoor")
	{
		pStateGame_->ChangeSubMode(SubMode::AddDoor, parameters.c_str());
	}
	else if (mode == "ChangeWallpaper")
	{
		pStateGame_->ChangeSubMode(SubMode::ChangeWallpaper, parameters.c_str());
	}
	else if (mode == "BuyEntity")
	{
		pStateGame_->ChangeSubMode(SubMode::BuyEntity, parameters.c_str());
	}
	else if (mode == "MoveEntity")
	{
		pStateGame_->ChangeSubMode(SubMode::MoveEntity, parameters.c_str());
	}
	_gamePlayGUI->CloseActiveWindow();
}

void
StateGameViewModel::EscPressed(BaseComponent* param_)
{
	LOG_ENGINE_ERROR("HAHAHAHAHAH");
	// todo: implnet LOG_GUI_INFO
	//LOG_GUI_INFO("Esc key pressed!");
	pStateGame_->ChangeSubMode(SubMode::Normal);
	_gamePlayGUI->CloseActiveWindow();
}

/******************************* ShowDebugWindow ********************************
 *
 ********************************************************************************/
void
StateGameViewModel::ShowDebugWindow(BaseComponent* param_)
{
	_gamePlayGUI->ShowDebugWindow();
}
/******************************* ToggleTileGrid *********************************
 *
 ********************************************************************************/
void
StateGameViewModel::ToggleTileGrid(BaseComponent* param_)
{
	pStateGame_->ToggleDrawGrid();
}


NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(StateGameViewModel)
{
	// ---> Commands
	NsProp("DebugText", &StateGameViewModel::GetDebugText, &StateGameViewModel::SetDebugText);
	NsProp("OpenWindowCommand", &StateGameViewModel::GetOpenWindowCommand);
	NsProp("CloseWindowCommand", &StateGameViewModel::GetCloseWindowCommand);
	NsProp("ChangeGameSubModeCommand", &StateGameViewModel::GetChangeGameSubModeCommand);
	NsProp("EscPressedCommand", &StateGameViewModel::GetEscPressedCommand);
	NsProp("ShowDebugWindowCommand", &StateGameViewModel::GetShowDebugWindowCommand);
	NsProp("ToggleTileGridCommand", &StateGameViewModel::GetToggleTileGridCommand);
	// ---< Commands

	NsProp("FPS", &StateGameViewModel::GetFPS, &StateGameViewModel::SetFPS);
	//NsProp("MouseTilePosX", &StateGameViewModel::GetMouseTilePosX, &StateGameViewModel::SetMouseTilePosX);
	//NsProp("MouseTilePosY", &StateGameViewModel::GetMouseTilePosY, &StateGameViewModel::SetMouseTilePosY);
	NsProp("MouseInfo", &StateGameViewModel::GetMouseInfo, &StateGameViewModel::SetMouseInfo);
	NsProp("TileUnderMouse", &StateGameViewModel::GetTileUnderMouse, &StateGameViewModel::SetTileUnderMouse);
	NsProp("OldTileUnderMouse", &StateGameViewModel::GetOldTileUnderMouse, &StateGameViewModel::SetOldTileUnderMouse);
	NsProp("EntityUnderMouse", &StateGameViewModel::GetEntityUnderMouse, &StateGameViewModel::SetEntityUnderMouse);
	NsProp("EntityUnderMouseName", &StateGameViewModel::GetEntityUnderMouseName, &StateGameViewModel::SetEntityUnderMouseName);
	NsProp("GameStateData", &StateGameViewModel::GetGameStateData, &StateGameViewModel::SetGameStateData);
	NsProp("ItemFloors", &StateGameViewModel::GetItemFloors);
	NsProp("ItemDoors", &StateGameViewModel::GetItemDoors);
	NsProp("ItemWallpapers", &StateGameViewModel::GetItemWallpapers);
	NsProp("ItemFurnitures", &StateGameViewModel::GetItemFurnitures);
}
