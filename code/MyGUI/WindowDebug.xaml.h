#pragma once

#include <NsCore/Noesis.h>
#include <NsGui/UserControl.h>


namespace MYGUI
{
class WindowDebug : public Noesis::UserControl
{
public:
	WindowDebug();
	~WindowDebug();
//void SetStateGame(StateGame* StateGame);
private:
	void InitializeComponent();
	void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	
	NS_DECLARE_REFLECTION(WindowDebug, UserControl);
};
}

