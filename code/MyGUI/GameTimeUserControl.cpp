#include "GameTimeUserControl.h"


#include <NsCore/Noesis.h>
#include <NsCore/ReflectionImplementEmpty.h>
#include <NsCore/RegisterComponent.h>
#include <NsGui/Button.h>
#include <NsGui/IntegrationAPI.h>
#include <NsGui/TextBlock.h>
#include <NsGui/TextBox.h>
#include <NsApp/EmbeddedXamlProvider.h>
#include <NsApp/EmbeddedFontProvider.h>

using namespace MYGUI;
using namespace Noesis;


GameTimeUserControl::GameTimeUserControl()
{
	Initialized() += Noesis::MakeDelegate(this, &GameTimeUserControl::OnInitialized);
	InitializeComponent();
}

void
GameTimeUserControl::InitializeComponent()
{
	GUI::LoadComponent(this, "GameTimeUserControl.xaml");
}

void
GameTimeUserControl::OnInitialized(BaseComponent*, const EventArgs&)
{
	//SetDataContext(MakePtr<StateGameViewModel>());
}


GameTimeUserControl::~GameTimeUserControl()
{
}

NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(GameTimeUserControl, "MYGUI.GameTimeUserControl")
{
}
