#include "WindowItem.xaml.h"


#include <NsCore/Noesis.h>
#include <NsCore/ReflectionImplementEmpty.h>
#include <NsCore/RegisterComponent.h>
#include <NsGui/Button.h>
#include <NsGui/IntegrationAPI.h>
#include <NsGui/TextBlock.h>
#include <NsGui/TextBox.h>
#include <NsApp/EmbeddedXamlProvider.h>
#include <NsApp/EmbeddedFontProvider.h>

using namespace MYGUI;
using namespace Noesis;


WindowItem::WindowItem()
{
	Initialized() += Noesis::MakeDelegate(this, &WindowItem::OnInitialized);
	InitializeComponent();
}

void
WindowItem::InitializeComponent()
{
	GUI::LoadComponent(this, "WindowItem.xaml");
}

void
WindowItem::OnInitialized(BaseComponent*, const EventArgs&)
{
	//SetDataContext(MakePtr<StateGameViewModel>());
}


WindowItem::~WindowItem()
{
}

NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(WindowItem, "MYGUI.WindowItem")
{
}
