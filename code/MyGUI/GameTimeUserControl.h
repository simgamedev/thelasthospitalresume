#pragma once

#include <NsCore/Noesis.h>
#include <NsGui/UserControl.h>


namespace MYGUI
{
class GameTimeUserControl : public Noesis::UserControl
{
public:
	GameTimeUserControl();
	~GameTimeUserControl();
//void SetStateGame(StateGame* StateGame);
private:
	void InitializeComponent();
	void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	
	NS_DECLARE_REFLECTION(GameTimeUserControl, UserControl);
};
}
