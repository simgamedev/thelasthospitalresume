#include <NsCore/Noesis.h>
#include <NsApp/DelegateCommand.h>
#include <NsApp/NotifyPropertyChangedBase.h>
#include <NsApp/DelegateCommand.h>
#include <NsCore/ReflectionImplement.h>


#include "../common.h"
using namespace NoesisApp;

#include "Tile.h" // in namespace MYGUI
#include "MouseInfo.h"
#include "../GameStateData.h"
#include "ItemFloor.h"
#include "ItemDoor.h"
#include "ItemWallpaper.h"
#include "ItemEntity.h"

class StateGame;
namespace MYGUI
{

class GamePlayGUI;
class StateGameViewModel : public NoesisApp::NotifyPropertyChangedBase
{
public:
	StateGameViewModel(GamePlayGUI* gamePlayGUI);
	~StateGameViewModel();

	//************************************************************
	//* Properties
	//************************************************************
	// ---> DebugText
	const char* GetDebugText() const
	{
		return _debugText.Str();
	}
	void SetDebugText(const char* debugText)
	{
		if (_debugText != debugText)
		{
			_debugText = debugText;
			OnPropertyChanged("DebugText");
		}
	}
	// ---< DebugText
	// ---> FPS
	real32 GetFPS() const
	{
		return _fps;
	}
	void SetFPS(const real32 fps)
	{
		_fps = fps;
		OnPropertyChanged("FPS");
	}
	// ---> Tile
	MouseInfo* GetMouseInfo() const
	{
		return _mouseInfo;
	}
	void SetMouseInfo(MouseInfo* mouseInfo)
	{
		_mouseInfo = mouseInfo;
		OnPropertyChanged("MouseInfo");
	}
	// ---< Tile
	// ---> Tile
	Tile* GetTileUnderMouse() const
	{
		return _tileUnderMouse;
	}
	void SetTileUnderMouse(Tile* tile)
	{
		_tileUnderMouse = tile;
		OnPropertyChanged("TileUnderMouse");
	}
	Tile* GetOldTileUnderMouse() const
	{
		return _oldTileUnderMouse;
	}
	void SetOldTileUnderMouse(Tile* tile)
	{
		_oldTileUnderMouse = tile;
		OnPropertyChanged("OldTileUnderMouse");
	}
	// ---< Tile
	// ---> entity under mouse
	int32 GetEntityUnderMouse() const
	{
		return _entityUnderMouse;
	}
	void SetEntityUnderMouse(const int32 value)
	{
		_entityUnderMouse = value;
		OnPropertyChanged("EntityUnderMouse");
	}
	// ---< entity under mouse
	// ---> Entity under mouse name.
	const char* GetEntityUnderMouseName() const
	{
		return _entityUnderMouseName.Str();
	}
	void SetEntityUnderMouseName(const char* entityUnderMouseName)
	{
		if (_entityUnderMouseName != entityUnderMouseName)
		{
			_entityUnderMouseName = entityUnderMouseName;
			OnPropertyChanged("EntityUnderMouseName");
		}
	}
	// ---< Entity under mouse name.
	// ---< DebugText
	// ---> GameStateData
	GameStateData* GetGameStateData() const
	{
		return _gameStateData;
	}
	void SetGameStateData(GameStateData* gameStateData)
	{
		_gameStateData = gameStateData;
		OnPropertyChanged("GameStateData");
	}
	// ---< GameStateData

	// ---> ItemFloors
	Noesis::ObservableCollection<ItemFloor>* GetItemFloors() const
	{
		return _itemFloors;
	}
	// ---< ItemFloors
	// ---> ItemDoors
	Noesis::ObservableCollection<ItemDoor>* GetItemDoors() const
	{
		return _itemDoors;
	}
	// ---< ItemDoors
	// ---> ItemWallpapers
	Noesis::ObservableCollection<ItemWallpaper>* GetItemWallpapers() const
	{
		return _itemWallpapers;
	}
	// ---< ItemWallpapers
	// ---> ItemFurnitures
	Noesis::ObservableCollection<ItemEntity>* GetItemFurnitures() const
	{
		return _itemFurnitures;
	}
	// ---< ItemFurnitures



	//************************************************************
	//* Commands
	//************************************************************
	const DelegateCommand* GetOpenWindowCommand() const
	{
		return &_openWindowCommand;
	}
	const DelegateCommand* GetCloseWindowCommand() const
	{
		return &_closeWindowCommand;
	}
	const DelegateCommand* GetChangeGameSubModeCommand() const
	{
		return &_changeGameSubModeCommand;
	}
	const DelegateCommand* GetEscPressedCommand() const
	{
		return &_escPressedCommand;
	}
	const DelegateCommand* GetShowDebugWindowCommand() const
	{
		return &_showDebugWindowCommand;
	}
	const DelegateCommand* GetToggleTileGridCommand() const
	{
		return &_toggleTileGridCommand;
	}
	const DelegateCommand* GetLayFloorCommand() const
	{
		return &_layFloorCommand;
	}
	//************************************************************
	//* SetStateGame
	//************************************************************
	void SetStateGame(StateGame* stateGame)
	{
		pStateGame_ = stateGame;
	}
private:
	StateGame* pStateGame_;
	void OpenWindow(BaseComponent* _param);
	void CloseWindow(BaseComponent* _param);
	void ChangeGameSubMode(BaseComponent* param_);
	void EscPressed(BaseComponent* param_);
	void ShowDebugWindow(BaseComponent* param_);
	void ToggleTileGrid(BaseComponent* param_);

private:
	GamePlayGUI* _gamePlayGUI;
	DelegateCommand _openWindowCommand;
	DelegateCommand _closeWindowCommand;
	DelegateCommand _changeGameSubModeCommand;
	DelegateCommand _escPressedCommand;
	DelegateCommand _showDebugWindowCommand;
	DelegateCommand _toggleTileGridCommand;
	DelegateCommand _layFloorCommand;
	Noesis::String _debugText;
	Noesis::String _entityUnderMouseName;
	real32 _fps;
	Tile* _tileUnderMouse;
	Tile* _oldTileUnderMouse;
	int32 _entityUnderMouse;
	MouseInfo* _mouseInfo;
	GameStateData* _gameStateData;
	Noesis::Ptr<Noesis::ObservableCollection<ItemFloor>> _itemFloors;
	Noesis::Ptr<Noesis::ObservableCollection<ItemDoor>> _itemDoors;
	Noesis::Ptr<Noesis::ObservableCollection<ItemWallpaper>> _itemWallpapers;
	Noesis::Ptr<Noesis::ObservableCollection<ItemEntity>> _itemFurnitures;
	Noesis::Ptr<Noesis::ObservableCollection<ItemEntity>> _itemEquipments;
	Noesis::Ptr<Noesis::ObservableCollection<ItemEntity>> _itemDecorations;
	NS_DECLARE_REFLECTION(StateGameViewModel, NotifyPropertyChangedBase);
};
}
