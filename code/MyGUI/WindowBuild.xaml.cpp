#include "WindowBuild.xaml.h"


#include <NsCore/Noesis.h>
#include <NsCore/ReflectionImplementEmpty.h>
#include <NsCore/RegisterComponent.h>
#include <NsGui/Button.h>
#include <NsGui/IntegrationAPI.h>
#include <NsGui/TextBlock.h>
#include <NsGui/TextBox.h>
#include <NsApp/EmbeddedXamlProvider.h>
#include <NsApp/EmbeddedFontProvider.h>

using namespace MYGUI;
using namespace Noesis;


WindowBuild::WindowBuild()
{
	Initialized() += Noesis::MakeDelegate(this, &WindowBuild::OnInitialized);
	InitializeComponent();
}

void
WindowBuild::InitializeComponent()
{
	GUI::LoadComponent(this, "WindowBuild.xaml");
}

void
WindowBuild::OnInitialized(BaseComponent*, const EventArgs&)
{
	//SetDataContext(MakePtr<StateGameViewModel>());
}


WindowBuild::~WindowBuild()
{
}

NS_BEGIN_COLD_REGION
NS_IMPLEMENT_REFLECTION(WindowBuild, "MYGUI.WindowBuild")
{
}
