#pragma once

//#include "NsGui/UserControl.h"
#include <NsCore/Noesis.h>
#include <NsGui/Grid.h>
#include <NsCore/ReflectionDeclare.h>


class StateMainMenu;
namespace MYGUI
{
	/**
		Code-behind for MainMenu.xaml
		*/
	class MainMenuGUI : public Noesis::Grid
	{
	public:
		MainMenuGUI();
		~MainMenuGUI();

		bool ConnectEvent(BaseComponent* source, const char* event, const char* handler) override;

		void Toggle();

		void OnBtnNewGame(Noesis::BaseComponent* pSender, const Noesis::RoutedEventArgs& args);
		void OnBtnLoadGame(Noesis::BaseComponent* pSender, const Noesis::RoutedEventArgs& args);
		void OnBtnCredits(Noesis::BaseComponent* pSender, const Noesis::RoutedEventArgs& args);
		void OnBtnExit(Noesis::BaseComponent* pSender, const Noesis::RoutedEventArgs& args);

		void SetStateMainMenu(StateMainMenu* StateMainMenu);
	private:
		StateMainMenu* pStateMainMenu_;

		void InitializeComponent();
		void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	private:
		NS_DECLARE_REFLECTION(MainMenuGUI, Noesis::Grid)
	};
}
