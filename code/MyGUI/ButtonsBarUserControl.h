#ifndef BUTTONS_BAR_USER_CONTROL_H
#define BUTTONS_BAR_USER_CONTROL_H

#include <NsCore/Noesis.h>
#include <NsGui/UserControl.h>

namespace MYGUI
{
class ButtonsBarUserControl : public Noesis::UserControl
{
public:
	ButtonsBarUserControl();
	~ButtonsBarUserControl();
//void SetStateGame(StateGame* StateGame);
	void ToggleActiveButton();
private:
	void InitializeComponent();
	void OnInitialized(BaseComponent*, const Noesis::EventArgs&);
	
	NS_DECLARE_REFLECTION(ButtonsBarUserControl, UserControl);
};
}


#endif
