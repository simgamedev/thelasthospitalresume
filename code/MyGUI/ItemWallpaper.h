#pragma once

#include <NoesisPCH.h>
#include "../common.h"
#include "../Utilities.h"
#include <fstream>
#include <iostream>

class ItemWallpaper final : public Noesis::BaseComponent
{
public:
	ItemWallpaper(const char* filepath)
	{
		ReadIn(filepath);
		int32 a = 100;
	}	

	// ---> Name
	const char* GetName() const { return _name.Str(); }
	void SetName(const char* name) { _name = name; }
	// ---<

	// ---> Price
	int32 GetPrice() const { return _price; }
	void SetPrice(const int32 price) { _price = price; }
	// ---< Price

	// ---> Icon Path
	const char* GetIconPath() const { return _iconPath.Str(); }
	void SetIconPath(const char* iconPath) { _iconPath = iconPath; }
	// ---< Icon Path

	// ---> EntityFilePath
	const char* GetEntityFilePath() const { return _entityFilePath.Str(); }
	void SetEntityFilePath(const char* tileSetName) { _entityFilePath = tileSetName; }
	// ---< EntityFilePath

private:
	Noesis::String _name;
	Noesis::String _iconPath;
	Noesis::String _entityFilePath;
	int32 _price;
private:
	NS_IMPLEMENT_INLINE_REFLECTION(ItemWallpaper, Noesis::BaseComponent)
	{
		NsProp("Name", &ItemWallpaper::GetName, &ItemWallpaper::SetName);
		NsProp("Price", &ItemWallpaper::GetPrice, &ItemWallpaper::SetPrice);
		NsProp("IconPath", &ItemWallpaper::GetIconPath, &ItemWallpaper::SetIconPath);
		NsProp("EntityFilePath", &ItemWallpaper::GetEntityFilePath, &ItemWallpaper::SetEntityFilePath);
	}
private:
	void ReadIn(const char* filename)
	{
		std::ifstream file;
		file.open(Utils::GetMediaDirectory() + "Entities/Walls/" + filename);
		if(!file.is_open())
		{
			return;
		}
		string line;
		string key;
		while(std::getline(file, line))
		{
			if(line[0] == '|')
				continue;
			std::stringstream linestream(line);
			linestream >> key;
			if (key == "Name")
			{
				string name;
				linestream >> name;
				SetName(name.c_str());
			}
			else if (key == "Price")
				linestream >> _price;
			else if(key == "IconPath")
			{
				string iconPath;
				linestream >> iconPath;
				SetIconPath(iconPath.c_str());
			}
			else if(key == "EntityFilePath")
			{
				string tileSetName;
				linestream >> tileSetName;
				SetEntityFilePath(tileSetName.c_str());
			}
		}
	}
};
