#pragma once

#include "Window.h"
#include "EventManager.h"
#include "TextureManager.h"
#include "ECS/SystemManager.h"
#include "ECS/EntityManager.h"
#include "FontManager.h"
#include "GUI/GUIManager.h"
// TODO: move this&related to "Resources" folder
#include "AudioResourceManager.h"
#include "Audio/AudioManager.h"
/**
	A simple Service Locator called SharedContext.
 */
class GameWorld;

struct SharedContext
{
	SharedContext()
	: window(nullptr)
	  /// @todo maybe remove event manager from here?
	, eventManager(nullptr)
	, textureManager(nullptr)
	, fontManager(nullptr)
	, systemManager(nullptr)
	, entityManager(nullptr)
	, guiManager(nullptr)
	, gameMap(nullptr)
	, audioResourceManager(nullptr)
	, audioManager(nullptr)
	{
	}

	Window* window;
	EventManager* eventManager;
	TextureManager* textureManager;
	FontManager* fontManager;
	SystemManager* systemManager;
	EntityManager* entityManager;
	GUIManager* guiManager;
	GameWorld* gameMap;
	AudioResourceManager* audioResourceManager;
	AudioManager* audioManager;
};
