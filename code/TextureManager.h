#pragma once
#include "ResourceManager.h"
#include <SFML/Graphics/Texture.hpp>

/**
 * A simpile texture manager.
 */
class TextureManager : public ResourceManager<TextureManager, sf::Texture>
{
public:
	TextureManager()
		: ResourceManager("textures.cfg")
	{
	}

	bool Load(sf::Texture* resource, const string& path)
	{
		return resource->loadFromFile(Utils::GetMediaDirectory() + path);
	}
};
												  
