#include "Patient.h"
#include "../AI/GOAP/ActionGotoPos.h"
#include "../StateGame.h"
#include "../AI/GOAP/ActionGoHome.h"


/****************************** Ctor ****************************************
 *
 ****************************************************************************/
Patient::Patient(const vec2f& pos, StateGame* stateGame)
	: TestEntity(Profession::Patient, pos, stateGame)
	, bFreeWill_(false)
{
	money_ = 20000.0;
	/*
	Room* waitingRoom = stateGame_->GetClosestRoom(RoomType::WaitingRoom);
	if (waitingRoom)
	{
		std::vector<Tile*> tiles = waitingRoom->GetTiles();
		int32 randTileIdx = Math::RandInRange(0, tiles.size() - 1);
		Tile* randTile = tiles.at(randTileIdx);
		while (randTile->tileType != TileType::Foundation &&
			   randTile->tileType != TileType::Floor)
		{
			randTileIdx = Math::RandInRange(0, tiles.size() - 1);
			randTile = tiles.at(randTileIdx);
		}
		vec2f posInRoom = vec2f(randTile->pos.x * TILE_SIZE, randTile->pos.y * TILE_SIZE);


		GOAP::Goal* goalGotoWaitingRoom = new GOAP::Goal("GotoWaitingRoom");
		GOAP::ActionGotoPos* actGotoPos = new GOAP::ActionGotoPos(this, posInRoom);
		currentPlan_.push_back(actGotoPos);
		currentGoal_ = goalGotoWaitingRoom;
		stateMachine_->PushState(AgentStateType::Perform);
	}
	// ---<
	*/

	// ---> Innate Actions
	GOAP::ActionGoHome goHome(this, vec2f(10, 300));
	innateActions_.push_back(goHome);
	// ---<

	// ---> Init Beliefs
	agentBeliefs_.SetVariable("kIsRegistered", 0);
	// ---<

	// ---> Goal goto WaitingRoom.
	GOAP::WorldState desiredState;
	desiredState.SetVariable("kIsInRoomWaitingRoom", 1);
	GOAP::Goal* goalGotoWaitingRoom = new GOAP::Goal("GotoWaitingRoom", desiredState, 3.0f);
	goalArbitrator_->AddGoal(goalGotoWaitingRoom);
	// ---< Goal goto WaitingRoom.
}

/****************************** Dtor ****************************************
 *
 ****************************************************************************/
Patient::~Patient()
{
}

/******************************* OnPlanFinished ****************************************
 ********************************************************************************/
void 
Patient::OnPlanFinished()
{
	// ---> Finished Goal: GoalGotoPCRTestRoom
	if (currentGoal_->GetName() == "GotoPCRTestRoom")
	{
		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		// ---<

		if(gameWorld_->AddAction("SwabMe", this))
		{
		}
		else
		{ 
			LOG_GAME_INFO("Pateint faield to add to world ActionSwabMe");
		}

		return;
	}
	// ---< Finished Goal: GoalGotoPCRTestRoom

	// ---> Finished Goal: GoalGotoWaitingRoom
	if (currentGoal_->GetName() == "GotoWaitingRoom")
	{
		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		// ---<

		if (agentBeliefs_.GetVariable("kIsRegistered") == 0)
		{
			// ---> New Goal: GoalGetRegistered
			GOAP::WorldState desiredState;
			desiredState.SetVariable("kIsRegistered", 1);
			GOAP::Goal* goalGetRegistered = new GOAP::Goal("GetRegistered", desiredState, 3.0f);
			goalArbitrator_->AddGoal(goalGetRegistered);
			currentGoal_ = goalGetRegistered;
			// ---< New Goal: GoalGetRegistered
		}

		return;
	}
	// ---< Finished Goal: GoalGotoWaitingRoom

	// ---> Finished Goal: GoalGetRegistered
	if (currentGoal_->GetName() == "GetRegistered")
	{
		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		// ---<

		// ---> New Goal: GoalGotoPCRTestRoom
		GOAP::WorldState desiredState;
		desiredState.SetVariable("kIsInRoomPCRTestRoom", 1);
		GOAP::Goal* gotoPCRTestRoom = new GOAP::Goal("GotoPCRTestRoom", desiredState, 3.0f);
		goalArbitrator_->AddGoal(gotoPCRTestRoom);
		currentGoal_ = gotoPCRTestRoom;
		// ---< New Goal: GoalGotoPCRTestRoom
		return;
	}
	// ---< Finished Goal: GoalGetRegistered

	// ---> Finished Goal: GoalGotoXRayRoom
	if (currentGoal_->GetName() == "GotoXRayRoom")
	{
		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		// ---<

		// ---> New Goal: GoalGotoEntityXRayMachine
		GOAP::WorldState desiredState;
		desiredState.SetVariable("kIsAtEntityXRayMachine", 1);
		GOAP::Goal* gotoXRayMachine = new GOAP::Goal("GotoEntityXRayMachine", desiredState, 3.0f);
		goalArbitrator_->AddGoal(gotoXRayMachine);
		currentGoal_ = gotoXRayMachine;
		// ---< New Goal: GoalGotoEntityXRayMachine
		return;
	}
	// ---< Finished Goal: GoalGotoXRayRoom

	// ---> Finished Goal: GoalGotoEntityXRayMachine
	if (currentGoal_->GetName() == "GotoEntityXRayMachine")
	{
		gameWorld_->AddAction("XRayScan", this);

		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		return;
		// ---<
	}
	// ---< Finished Goal

	// ---> Finished Goal: GoalGotoPharmacy
	if (currentGoal_->GetName() == "GotoRoomPharmacy")
	{
		// Provide actions so nurse can give you drugs
		gameWorld_->AddAction("GiveMeDrug", this);

		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		return;
		// ---<
	}
	// ---< Finished Goal: GoalGotoPharmacy

	// ---> Finished Goal: GoalGotoWard
	if (currentGoal_->GetName() == "GotoRoomWard")
	{
		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		// ---<

		// ---> New Goal: GoalLieInBed
		GOAP::WorldState desiredState;
		desiredState.SetVariable("kIsInBed", 1);
		GOAP::Goal* lieInBed = new GOAP::Goal("LieInBed", desiredState, 3.0f);
		goalArbitrator_->AddGoal(lieInBed);
		currentGoal_ = lieInBed;
		// ---< New Goal: GoalLieInBed

		// ---> Send Broadcastst that you'are gonna need IV&Ventilator
		// ActionHookMeToVentilator
		gameWorld_->AddAction("HookMeToVent", this);
		// ActionHookMeToPatient
		// ---<
		return;
	}
	// ---< Finished Goal: GoalGotoWard

	// ---> Finished Goal: GoalLieInBed
	if (currentGoal_->GetName() == "LieInBed")
	{
		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		// ---<

		GOAP::WorldState desiredState;
		desiredState.SetVariable("kIsAtHome", 1);
		GOAP::Goal* goal = new GOAP::Goal("GoHome", desiredState, 3.0f);
		//goal->SetTargetTestEntity(broadcast.senderTestEntity);
		goalArbitrator_->AddGoal(goal);

		return;
	}
	// ---< Finished Goal: GoalLieInBed
}

/******************************* OnMessage **************************************
 *
 ********************************************************************************/
void
Patient::OnMessage(Telegram telegram)
{
	// ---> Test Result Ready
	if (telegram.type == CharMsgType::TestResultReady)
	{
		bool testResult = (*static_cast<bool*>(telegram.extraInfo));
		// COVID-90 Positive!!
		if (testResult == true)
		{
			int32 a = 100;
			//--------------> Should cry now!!!
			// ---> Add a Goal: GoalGotoXRayRoom For Further Testing
			GOAP::WorldState desiredState;
			desiredState.SetVariable("kIsInRoomXRayRoom", 1);
			GOAP::Goal* goal = new GOAP::Goal("GotoXRayRoom", desiredState, 3.0f);
			goalArbitrator_->AddGoal(goal);
			// ---<
		}
		// COVID-90 Negative!!
		else
		{
			GOAP::WorldState desiredState;
			desiredState.SetVariable("kIsAtHome", 1);
			GOAP::Goal* goal = new GOAP::Goal("GoHome", desiredState, 3.0f);
			//goal->SetTargetTestEntity(broadcast.senderTestEntity);
			goalArbitrator_->AddGoal(goal);
		}
	}
	// ---< Test Result Ready

	// ---> XRay Result Ready
	if (telegram.type == CharMsgType::XRayResultReady)
	{
		bool xRayResult = (*static_cast<bool*>(telegram.extraInfo));
		// Lung damaged
		if (xRayResult)
		{
			GOAP::WorldState desiredState;
			desiredState.SetVariable("kIsInRoomWard", 1);
			GOAP::Goal* goal = new GOAP::Goal("GotoRoomWard", desiredState, 3.0f);
			goalArbitrator_->AddGoal(goal);
		}
		// Lung Not Damaged
		else
		{
			GOAP::WorldState desiredState;
			desiredState.SetVariable("kIsInRoomPharmacy", 1);
			GOAP::Goal* goal = new GOAP::Goal("GotoRoomPharmacy", desiredState, 3.0f);
			goalArbitrator_->AddGoal(goal);
		}
	}
	
	// ---> ReceivedDrug
	if (telegram.type == CharMsgType::ReceivedDrug)
	{
		//SetHoldingItem(ItemType::MedPill);
		SetHoldingItem(ItemType::SnackSandwich);
		GOAP::WorldState desiredState;
		desiredState.SetVariable("kIsAtHome", 1);
		GOAP::Goal* goal = new GOAP::Goal("GoHome", desiredState, 3.0f);
		//goal->SetTargetTestEntity(broadcast.senderTestEntity);
		goalArbitrator_->AddGoal(goal);
	}
}

