#include "Tester.h"
#include "../AI/GOAP/ActionGotoPos.h"
#include "../StateGame.h"

#include "../Room/Room.h"


/****************************** Ctor ****************************************
 *
 ****************************************************************************/
Tester::Tester(const vec2f& pos, StateGame* stateGame)
	: TestEntity(Profession::Tester, pos, stateGame)
	, bFreeWill_(true)
{
	// ---> Alaywas has a Goal of GivePCRTest
	GOAP::WorldState desiredState;
	desiredState.SetVariable("kIsPCRTested", 1);
	GOAP::Goal* goalGivePCRTest = new GOAP::Goal("GivePCRTest", desiredState, 300.0f);
	goalArbitrator_->AddGoal(goalGivePCRTest);
	// ---<

	// ---> Temporarily always has a Goal of GiveXRayScan
	GOAP::WorldState desiredState2;
	desiredState2.SetVariable("kIsXRayScanned", 1);
	GOAP::Goal* goalGiveXRay = new GOAP::Goal("GiveXRayScan", desiredState2, 300.0f);
	goalArbitrator_->AddGoal(goalGiveXRay);
	// ---< 
}

/****************************** Dtor ****************************************
 *
 ****************************************************************************/
Tester::~Tester()
{
}

/******************************* OnPlanFinished ****************************************
 ********************************************************************************/
void 
Tester::OnPlanFinished()
{
	// ---> Finished Goal: GivePCRTest
	if (currentGoal_->GetName() == "GivePCRTest")
	{
		int32 a = 100;
		pOwnedRoom_->SetOccupied(false);
		// --->
		/*
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		*/
		return;
		// ---<
	}
	// ---< Finished Goal: GivePCRTest

	// ---> Finished Goal: GiveXRayScan
	if (currentGoal_->GetName() == "GiveXRayScan")
	{
		// --->
		return;
		// ---<
	}
	// ---< Finished Goal: GiveXRayScan
}

/******************************* OnMessage **************************************
 *
 ********************************************************************************/
void
Tester::OnMessage(Telegram telegram)
{
	if (telegram.type == CharMsgType::RoomReady)
	{
		// abrupt every thing that you are currently doing
		//ClearCurrentPlan();
		//Room* pcrTestRoom = stateGame_->GetClosestRoom(RoomType::PCRTestRoom);
		//int32 roomID = *static_cast<int32*>(telegram.extraInfo);
		int32 roomID = telegram.sender;
		Room* pcrTestRoom = gameWorld_->GetRoom(roomID);
		pcrTestRoom->GetRandomEmptyPos();
		vec2f randPosInRoom = pcrTestRoom->GetRandomEmptyPos();

		// --> GoToPosGoal OR!! Should call Task(which is mandatoy)
		GOAP::Goal* goalGotoPCRTestRoom = new GOAP::Goal("GotoPCRTestRoom");
		currentGoal_ = goalGotoPCRTestRoom;
		GOAP::ActionGotoPos* actGotoPos = new GOAP::ActionGotoPos(this, randPosInRoom);
		currentPlan_.push_back(actGotoPos);
		stateMachine_->PushState(AgentStateType::Perform);
	}
}

