#pragma once

#include "../TestEntity.h"

/**
   Patient Character Class
 */
class Patient : public TestEntity
{
public:
	Patient(const vec2f& pos, StateGame* stateGame);
	~Patient();

	void OnPlanFinished();
	void OnMessage(Telegram telegram);
private:
	bool bFreeWill_;
	real32 money_;
};
