#pragma once

#include "../TestEntity.h"

class Room;
/**
   Tester Character Class
 */
class Tester : public TestEntity
{
public:
	Tester(const vec2f& pos, StateGame* stateGame);
	~Tester();

	void OnPlanFinished();
	void OnMessage(Telegram telegram);
public:
	Room* pOwnedRoom_;
	bool bFreeWill_;
};
