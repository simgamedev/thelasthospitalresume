#include "Nurse.h"
#include "../AI/GOAP/ActionGotoPos.h"
#include "../StateGame.h"


/****************************** Ctor ****************************************
 *
 ****************************************************************************/
Nurse::Nurse(const vec2f& pos, StateGame* stateGame)
	: TestEntity(Profession::Nurse, pos, stateGame)
	, bFreeWill_(true)
{
	// TODO: Below
	/*
	if(pRoomOwned_->GetType() == RoomType::Pharmacy)
	{
	}
	*/
	// ---> Alaywas has a Goal of GiveDrug
	GOAP::WorldState desiredState;
	desiredState.SetVariable("kPatientHasDrug", 1);
	GOAP::Goal* goalGiveDrug = new GOAP::Goal("GiveDrug", desiredState, 300.0f);
	goalArbitrator_->AddGoal(goalGiveDrug);
	// ---<

	// ---> Always has a Goal of HookPatientToVent
	GOAP::WorldState desiredState2;
	desiredState2.SetVariable("kIsPatientOnVent", 1);
	GOAP::Goal* goalHookPatientToVent = new GOAP::Goal("HookPatientToVent", desiredState2, 300.0f);
	goalArbitrator_->AddGoal(goalHookPatientToVent);
	// ---< 
}

/****************************** Dtor ****************************************
 *
 ****************************************************************************/
Nurse::~Nurse()
{
}

/******************************* OnPlanFinished ****************************************
 ********************************************************************************/
void 
Nurse::OnPlanFinished()
{

	// ---> Finished Goal: GiveXRayScan
	if (currentGoal_->GetName() == "GiveDrug")
	{
		// --->
		goalArbitrator_->RemoveGoal(currentGoal_);
		currentGoal_ = nullptr;
		return;
		// ---<
	}
	// ---< Finished Goal: GiveXRayScan
}

/******************************* OnMessage **************************************
 *
 ********************************************************************************/
void
Nurse::OnMessage(Telegram telegram)
{
	if (telegram.type == CharMsgType::RoomReady)
	{
		// abrupt every thing that you are currently doing
		//ClearCurrentPlan();
		//Room* pcrTestRoom = stateGame_->GetClosestRoom(RoomType::PCRTestRoom);
		//int32 roomID = *static_cast<int32*>(telegram.extraInfo);
		int32 roomID = telegram.sender;
		Room* pcrTestRoom = gameWorld_->GetRoom(roomID);
		pcrTestRoom->GetRandomEmptyPos();
		vec2f randPosInRoom = pcrTestRoom->GetRandomEmptyPos();

		// --> GoToPosGoal OR!! Should call Task(which is mandatoy)
		GOAP::Goal* goalGotoPCRTestRoom = new GOAP::Goal("GotoPCRTestRoom");
		currentGoal_ = goalGotoPCRTestRoom;
		GOAP::ActionGotoPos* actGotoPos = new GOAP::ActionGotoPos(this, randPosInRoom);
		currentPlan_.push_back(actGotoPos);
		stateMachine_->PushState(AgentStateType::Perform);
	}
}
