#pragma once

#include "../TestEntity.h"

/**
   Nurse Character Class
 */
class Nurse : public TestEntity
{
public:
	Nurse(const vec2f& pos, StateGame* stateGame);
	~Nurse();

	void OnPlanFinished();
	void OnMessage(Telegram telegram);
private:
	bool bFreeWill_;
};
