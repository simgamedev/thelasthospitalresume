#pragma once

#include "common.h"
//#include "AI/SteeringBehaviors.h"
#include "AI/AgentSteeringBehaviors.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "TextureManager.h"

#include "Directions.h"
//#include "AI/GOAP/Action.h"
#include "AI/GOAP/Planner.h"
#include <queue>
#include <deque>
#include "GameSprite.h"

//---> Real Path Finding
#include "Graph/PathPlanner.h"
#include "Messaging/Telegram.h"
#include "Graph/PathEdge.h"
//---<
//#include "Map/GameWorld.h"
// ---> Modify
#include "GameWorld/GameWorld.h"
#include "AI/FSM.h"
#include "AI/FSMState.h"
#include "AI/GOAP/Arbitrator.h"
#include "ECS/ItemTypes.h"
#include "ECS/SystemManager.h"
#include "ECS/MessageHandler.h"
// ---< Modify

// ---> NoesisGUI
// ---< NoesisGUI
enum class Profession
{
	Patient,
	Tester,
	Doctor,
	Nurse,
	Janitor,
	Security,
	Chef,
	Worker,
	Helper,
};

enum class BloodType
{
	A,
	B,
	O,
	NegativeO
};

enum class Sex
{
	Male,
	Female,
};

class StateGame;
class TestEntity : public Observer
{
public:
	TestEntity(Profession profession, const vec2f& pos, StateGame* stateGame);
	void Init();
	~TestEntity();
	void Update(const sf::Time& time);
	void UpdateAI(const sf::Time& time); // GOAP
	void Draw(Window* window);
	//void DEBUGDraw(sf::RenderWindow* renderWindow);
	void PlayAnimation(const string& name, bool play, bool loop);
	StateGame* GetGame() { return stateGame_; }

	// ---> GOAP
	int32 GetAvailableEntity(const string& name);
	vec2f GetTargetEntityPos(const int32& entityID);
	// ---< 

	bool CanWalkTo(const vec2f& pos);
	bool CanWalkBetween(const vec2f& start, const vec2f& end);


	// ---> Getters&Setters
	string GetName() const { return name_; }
	bool HasGoal(const string& name) const;
	//vec2f GetMouseClickWorldPos();
	void SetPosition(const vec2f& position);
	//void SetTargetPos(const vec2f& targetPos);
	//void SetSeekOn(const bool& seekON);
	//void SetOrigin(Origin origin);
	vec2f GetPosition() const;
	real32 GetMaxSpeed() const;
	vec2f GetVelocity() const;
	vec2f GetTargetPos() const;
	real32 GetTimeElapsed() const;
	vec2f GetHeading() const;
	real32 GetMaxForce() const;
	// side is  perpendicular to heading
	vec2f GetSide() const;
	vec2i GetTilepos() const;
	int32 GetID() const;
//private:
	// ---> Modify
	//private
	void UpdateMovement(const real32& dt);
	void UpdateWander(const real32& dt);
	void SetID(int32 id);
	Sex sex_;
	void OnBroadcast(Telegram telegram);
	void OnMessage(Telegram telegram);
	void OnNotify(const Message& msg);
	TestEntity* currentPatient_;
	std::deque<GOAP::Action*> currentPlan_;
	Profession GetProfession() const;
	int32 GetToiletSatisfaction() const;
	void DecreaseHunger(int32 amount);
	void IncreaseToiletSatisfaction(int32 amount);
	void DecreaseToiletSatisfaction(int32 amount);
	GameWorld* gameWorld_;
	//bool Perform(GOAP::Action* action);
	// ---< Modify
public:
	StateGame* stateGame_;
	GameSprite* gameSprite_;
	//vec2f targetPos_;
	// ---> Messaging/Events
	// ---< Messaging/Events
	// ---> Movement
	vec2f pos_;
	vec2i tilepos_;
	vec2f velocity_;
	vec2f heading_;
	vec2f side_;
	real32 mass_;
	real32 maxSpeed_;
	real32 maxForce_;
	real32 maxTurnRate_;
	Steering* pSteering_;
	// ---< Movement
	// ---> Character Info/States
	string name_;
	Profession profession_;
	BloodType bloodType_;
	int32 numDrugsTook_;
	int32 numIVsTook_;
	int32 id_;
	bool pcrTested_;
	int32 infectionStage_;

	int32 hunger_;
	int32 moral_;
	int32 energy_;
	int32 toilet_;

	bool infected_;
	real32 temperature_;
	real32 lungCondition_;
	// ---< Character Info
	// ---> Update
	real32 timeElapsed_;
	GameTime lastGameTime_;
	// ---< Update
	// ---> GOAP
	FSM* stateMachine_;
	GOAP::Arbitrator* goalArbitrator_;
	GOAP::Planner* goapPlanner_;
	std::vector<GOAP::Action> innateActions_;;
	GOAP::Action* currentAction_;
	GOAP::Goal* currentGoal_;
	GOAP::WorldState agentBeliefs_;

	GOAP::Action* GetCurrentAction() const { return currentAction_; }
	GOAP::Goal* GetCurrentGoal() const { return currentGoal_; }
	GOAP::WorldState* GetAgentBeliefs();
	void OnPlanFinished();
	void OnPlanAborted(GOAP::Action& failedAction);
	bool HasActivePlan();
	AgentStateType GetCurrentState() const;
	void CopyPlan(std::vector<GOAP::Action> plan);
	void ClearCurrentPlan();
	std::deque<GOAP::Action*> GetCurrentPlan() const;
	// ---< GOAP
	// ---> Steering
	PathPlanner* pathPlanner_;
	std::list<PathEdge> m_PathAsEdges;
	bool m_bLastEdgeInPath;
	PathEdge* currentEdge_;
	void FollowPath();
	bool DEBUGDrawPath_;
	std::vector<sf::Vertex> DEBUGPathDrawable_;
	void DEBUGUpdatePathDrawable();
	void ArriveAt(vec2f pos);
	void SeekTo(vec2f pos);
	void Flee(const vec2f& target);
	void GotoPosition(vec2f pos);
	// ---< Steering
	// ---> Inventory/ECS
	void SetHoldingItem(const EntityID& entityID);
	void SetHoldingItem(const ItemType& itemType);
	void ConsumeHoldingItem();
	int32 holdingItemID_;
	SStorage* GetSysStorage();
	// ---< Inventory/ECS
	// ---> Temp
	StateGame* GetStateGame() const;
	// ---< Temp
	// ---> Getters&Setters
	void SetHunger(const real32& hunger) { hunger_ = hunger; }
	real32 GetHunger() const { return hunger_; }
	GameWorld* GetGameWorld();
	// ---<
public:
	// ---> Characters
	bool IntersectsMouse(const vec2f& mouseWorldPos);
	void Tint(sf::Color& tintColor);
	void UnTint();
	sf::FloatRect GetGlobalBounds();
	// ---< Characters
};
