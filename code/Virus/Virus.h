#pragma once
#include "../Time/time.h"


/** Covid-90 virus */

struct Virus
{
	Virus(GameTime bornTime, vec2u tilepos)
		: timeBorn(bornTime)
		, tilePos(tilepos)
	{
	}

	GameTime timeBorn; // a virus would live 3 hours in air
	vec2u tilePos;
};
