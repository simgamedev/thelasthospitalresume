#include "Game.h"
/**
   Default Constructor.
 */
Game::Game()
{
	Init();
}

/********************************************************************************
 * Set up managers ect.
 ********************************************************************************/
void
Game::Init()
{
	// ---> Orders are important.
	clock_.restart();
	srand(time(nullptr));
	context_.window = &window_;
	context_.eventManager = window_.GetEventManager();
	context_.textureManager = &textureManager_;

	fontManager_ = new FontManager();
	context_.fontManager = fontManager_;

	guiManager_ = new GUIManager(&context_);
	context_.guiManager = guiManager_;


	stateManager_ = new StateManager(&context_);
	stateManager_->AddDependent(guiManager_);
	// TODO move this to a proper place.
	stateManager_->AddDependent(context_.eventManager);
	//stateManager_.SwitchTo(GameStateType::Game);

	// ---> SoundManager
	audioResourceManager_ = new AudioResourceManager();
	audioManager_ = new AudioManager(audioResourceManager_);
	stateManager_->AddDependent(audioManager_);
	context_.audioResourceManager = audioResourceManager_;
	context_.audioManager = audioManager_;
	// ---< SoundManager

	// ---> ECS
	systemManager_ = new SystemManager();
	entityManager_ = new EntityManager(systemManager_, &textureManager_);
	systemManager_->SetEntityManager(entityManager_);
	context_.systemManager = systemManager_;
	context_.entityManager = entityManager_;
	// ---< ECS
	/// @todo Make use of StateIntro
	stateManager_->SwitchTo(GameStateType::MainMenu);
}

/**
   Empty Destructor.
 */
Game::~Game()
{
	/// @todo this way of doing things?
	delete guiManager_;
	delete fontManager_;
}


/**
   Update method.
 */
void
Game::Update()
{
	window_.Update();
	stateManager_->Update(elapsedTime_);

	// ---> GUI
	guiManager_->Update(elapsedTime_.asSeconds());
	GUIEvent guiEvent;
	while(context_.guiManager->PollEvent(guiEvent))
	{
		window_.GetEventManager()->HandleEvent(guiEvent);
	}

	window_.ups_ = ups_;
}



/**
   Simply Render
 */
void
Game::Render()
{
	window_.BeginActualDraw();
	stateManager_->Draw();
	//guiManager_->Draw(window_.GetRenderWindow());
	window_.ActualDraw();
	window_.EndActualDraw();
}

/**
   Get Window
 */
Window*
Game::GetWindow()
{
	return &window_;
}

/**
   Restart the sf::Clock
 */
void
Game::RestartClock()
{
	elapsedTime_ = clock_.restart();
}

/**
   Get elapsed time
 */
sf::Time
Game::GetElapsedTime()
{
	return elapsedTime_;
}


void
Game::LateUpdate()
{
	stateManager_->ProcessRemovingRequests();
	RestartClock();
	window_.fps_ = fps_;
}

