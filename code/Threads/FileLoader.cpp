#include <fstream>
#include "FileLoader.h"
#include "../Utilities.h"
#include <SFML/Graphics.hpp>
#include <sstream>

/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
FileLoader::FileLoader()
	: totalNumberOfLines_(0)
	, currentLineNumber_(0)
{
}

void
FileLoader::AddFile(const string& filepath)
{
	files_.emplace_back(filepath, 0);
}

size_t
FileLoader::GetTotalNumberOfLines()
{
	//sf::Lock lock(mutex_);
	//mutex_.lock();
	std::lock_guard<std::mutex> lg(mutex_);
	return totalNumberOfLines_;
}

size_t
FileLoader::GetCurrentLineNumber()
{
	//sf::Lock lock(mutex_);
	//mutex_.lock();
	//std::lock_guard<std::mutex> lg(mutex_);
	return currentLineNumber_;
}


void
FileLoader::SaveToFile(const string& filepath)
{

}

void
FileLoader::ResetForNextFile()
{
}

void
FileLoader::Work()
{
	CountTotalNumberOfLines();
	if(!totalNumberOfLines_)
	{
		std::cout << "Error: No lines to process." << std::endl;
		Done();
		return;
	}
	// ---> Process Each File
	for(auto& filesItr : files_)
	{
		ResetForNextFile();
		std::ifstream file(filesItr.first);
		string line;
		auto& numLinesLeft = filesItr.second;
		while(std::getline(file, line))
		{
			//sf::Lock lock(mutex_);
			//mutex_.lock();
			++currentLineNumber_;
			--numLinesLeft;
			if(line[0] == '|') { continue; }
			/// @todo should we call keystream keyValueStream?
			std::stringstream keystream(line);
			if(!ProcessLine(keystream))
			{
				std::cout << "Error: File loader teminated due to an interanl error." << std::endl;
				{
					//sf::Lock lock(mutex_);
					//mutex_.lock();
					/// @todo what's this
					currentLineNumber_ += numLinesLeft;
					break;
				}
			}
		}
		file.close();
	}
	files_.clear();
	Done();
}

void
FileLoader::CountTotalNumberOfLines()
{
	totalNumberOfLines_ = 0;
	currentLineNumber_ = 0;
	for(auto filesItr = files_.begin();
		filesItr != files_.end();)
	{
		string filepath = filesItr->first;
		size_t& numberOfLines = filesItr->second;
		if(filepath.empty())
		{
			filesItr = files_.erase(filesItr);
			continue;
		}
		// ---> Open File
		std::ifstream file(filepath);
		if(!file.is_open())
		{
			std::cout << "Failed to open file: " << filepath << std::endl;
			filesItr = files_.erase(filesItr);
			continue;
		}
		// ---> Count number of lines in this file.
		file.unsetf(std::ios_base::skipws);
		{
			//sf::Lock lock(mutex_);
			//mutex_.lock();
			numberOfLines = static_cast<size_t>(std::count(std::istreambuf_iterator<char>(file),
														   std::istreambuf_iterator<char>(),
														   '\n'));
			totalNumberOfLines_ += numberOfLines;
			std::cout << "Number of lines in file: " << filepath << "," << numberOfLines << std::endl;
		}
		++filesItr;
		file.close();
	}
}
