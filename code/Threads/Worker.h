#pragma once
#include <SFML/System/Thread.hpp>
#include <thread>
#include <mutex>

/**
   For loading files multithreadedly.
 */

class Worker
{
public:
	Worker()
		: thread_(&Worker::Work, this)
		, done_(false)
		, started_(false)
	{
	}
	virtual void OnAdd() {}
	virtual void OnRemove() {}
	void Begin()
	{

		//sf::Lock lock(mutex_);
		std::lock_guard<std::mutex> guard(mutex_);
		if(done_ || started_) { return; }
		started_ = true;
		//thread_.launch();
		thread_.join();
	}

	bool IsDone()
	{
		//sf::Lock lock(mutex_);
		std::lock_guard<std::mutex> guard(mutex_);
		return done_;
	}

	bool HasStarted()
	{
		//sf::Lock lock(mutex_);
		std::lock_guard<std::mutex> guard(mutex_);
		return started_;
	}

	void ResetWorker()
	{
		//sf::Lock lock(mutex_);
		std::lock_guard<std::mutex> guard(mutex_);
		done_ = false;
		started_ = false;
	}
protected:
	void Done()
	{
		done_ = true;
	}
	virtual void Work() = 0;
	//sf::Thread thread_;
	std::thread thread_;
	bool done_;
	bool started_;
	//sf::Mutex mutex_;
	std::mutex mutex_;
};
