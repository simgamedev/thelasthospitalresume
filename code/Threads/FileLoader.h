#pragma once
#include "../Common.h"
#include <vector>
//#include <SFML/System/Lock.hpp>
//#include <SFML/System/Mutex.hpp>
#include "Worker.h"

/// NOTE: <filepath, number of lines>
using LoadeePaths = std::vector<std::pair<string, size_t>>;

/**
   A super class for classes that need to load files.
 */
class FileLoader : public Worker
{
public:
	FileLoader();
	void AddFile(const string& filepath);
	virtual void SaveToFile(const string& filepath);

	size_t GetTotalNumberOfLines();
	size_t GetCurrentLineNumber();
private:
	virtual bool ProcessLine(std::stringstream& stream) = 0;
	virtual void ResetForNextFile();
	void Work();
	void CountTotalNumberOfLines();

	LoadeePaths files_;
	size_t totalNumberOfLines_;
	size_t currentLineNumber_;
};

