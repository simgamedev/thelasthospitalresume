#pragma once
#include "common.h"
#include <SFML/System/String.hpp>
#include "GUI/GUIEvent.h"


struct EventDetails
{
	EventDetails(const string& bindingName)
	: bindingName(bindingName)
	, hasBeenProcessed(false)
	, controlPressed(false)
	, altPressed(false)
	, shiftPressed(false)
	{
		Clear();
	}

	string bindingName;
	//vec2i size;
	uint32 characterEntered;
	vec2i mousePos;
	int32 mouseWheelDelta;
	int32 keycode;

	string interfaceName;
	string elementName;
	GUIEventType guiEventType;
	
	bool hasBeenProcessed;

	bool controlPressed;
	bool altPressed;
	bool shiftPressed;
	bool systemPressed;

	void Clear()
	{
		characterEntered = 0;
		mousePos = vec2i(0, 0);
		mouseWheelDelta = 0;
		keycode = -1;
		interfaceName.clear();
		elementName.clear();
		guiEventType = GUIEventType::None;
		hasBeenProcessed = false;
		controlPressed = false;
		altPressed = false;
		altPressed = false;
		shiftPressed = false;
	}
};
