#pragma once
#include "../common.h"
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>

enum class GUIElementState{ Neutral, Focused, Clicked };

/**
   GUI Element Style.
 */
struct GUIStyle
{
	GUIStyle()
	: size(0.0f, 0.0f)
	, styleFileName("")
	, bgColor(sf::Color(0, 0, 0, 0))
	, elementColor(sf::Color(0, 0, 0, 0))
	, bgImageName("")
	, outlineThickness(0.0f)
    , textSize(12)
	, textCenterOrigin(false)
	, stretchToFit(false)
	, bgImageColor(255, 255, 255, 255)
	{
		sf::Color noneColor = sf::Color(0, 0, 0, 0);
		bgColor = noneColor;
		elementColor = noneColor;
		textColor = sf::Color(255, 255, 255, 255);
	}

	vec2f size;
	/// @todo why dou you need a stylefilename
	string styleFileName;
	sf::Color bgColor;
	sf::Color elementColor;
	string bgImageName;
	sf::Color bgImageColor;
	sf::Color textColor;
	string fontName;
	vec2f textPadding;
	uint32 textSize;
	string glyphImageName;
	vec2u glyphImageSize;
	vec2f glyphPadding;
	vec2f glyphImageScale;
	real32 outlineThickness;
	sf::Color outlineColor;
	bool textCenterOrigin;
	bool stretchToFit;
};


/**
   Drawbale part of GUIElement.
 */
struct GUIDrawable
{
	sf::RectangleShape bgSolid;
	sf::Sprite bgImage;
	sf::Sprite glyphImage;
	sf::Text text;
};
