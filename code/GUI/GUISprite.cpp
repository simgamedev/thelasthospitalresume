#include "GUISprite.h"
#include "GUIManager.h"
#include "../SharedContext.h"
#include "../TextureManager.h"


/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
GUISprite::GUISprite(const string& name, GUIInterface* owner)
	: GUIElement(name, GUIElementType::Sprite, owner)
	, textureManager_(owner_->GetManager()->GetContext()->textureManager)
{
}

GUISprite::~GUISprite()
{
	if(!textureName_.empty())
	{
		textureManager_->ReleaseResource(textureName_);
	}
}


void
GUISprite::SetTexture(const string& textureName)
{
	if(!textureName_.empty())
	{
		textureManager_->ReleaseResource(textureName_);
		textureName_ = "";
	}
	if(!textureManager_->RequireResource(textureName))
	{
		return;
	}
	sf::Texture* texture = textureManager_->GetResource(textureName);
	sprite_.setTexture(*texture);
	sprite_.setTextureRect(sf::IntRect(0, 0, texture->getSize().x, texture->getSize().y));
	textureName_ = textureName;
	needsRedraw_ = true;
	/// @todo only redraw owner's bg layer???????
	owner_->SetRedraw(true);

}

void
GUISprite::SetTexture(sf::RenderTexture& renderTexture)
{
	if(!textureName_.empty())
	{
		textureManager_->ReleaseResource(textureName_);
		textureName_ = "";
	}
	sprite_.setTexture(renderTexture.getTexture());
	auto textureSize = renderTexture.getTexture().getSize();
	sprite_.setTextureRect(sf::IntRect(0, 0, textureSize.x, textureSize.y));
	// ---> TEMP
	//sprite_.setScale(vec2f(2.0f, 2.0f));
	// ---< TEMP
	ApplyStyle();
	needsRedraw_ = true;
	/// @todo only redraw owner's bg layer???????
	owner_->SetRedraw(true);
}

void
GUISprite::ReadIn(std::stringstream& stream)
{
	string textureName;
	if(!(stream >> textureName)) { return; }
	SetTexture(textureName);
}

void
GUISprite::OnClick(const vec2f& mousePos)
{
	SetState(GUIElementState::Clicked);
}

void
GUISprite::OnRelease()
{
	SetState(GUIElementState::Neutral);
}

void
GUISprite::OnHover(const vec2f& mousePos)
{
	SetState(GUIElementState::Focused);
}

void
GUISprite::OnLeave()
{
	SetState(GUIElementState::Neutral);
}

void
GUISprite::ApplyStyle()
{
	GUIElement::ApplyStyle();

	auto texture = sprite_.getTexture();
	/// @todo when does it not have a texuter??
	if(texture)
	{
		//styles_[state_].size = vec2f(texture->getSize());
		for (auto& itr : styles_)
		{
			GUIStyle& style = itr.second;
			style.size.x = scale_.x * texture->getSize().x;
			style.size.y = scale_.y * texture->getSize().y;
		}

	}

	/// @todo should enable scaling
	sprite_.setPosition(GetPosition());
}

void
GUISprite::Update(real32 dt)
{
}

void
GUISprite::Draw(sf::RenderTarget* renderTarget)
{
	if (this->GetName() == "TileSetSprite")
	{
		int32 a = 100;
	}
	renderTarget->draw(sprite_);
	renderTarget->draw(drawable_.text);
}

/*********************************************************************************
* Set Scale.
 *********************************************************************************/
// ---> TEMP
void
GUISprite::SetScale(vec2f scale)
{
	scale_ = scale;
	auto textureSize = sprite_.getTexture()->getSize();
	for (auto& itr : styles_)
	{
		GUIStyle& style = itr.second;
	    style.size.x = scale_.x * textureSize.x;
	    style.size.y = scale_.y * textureSize.y;
	}
	sprite_.setScale(scale_);
	/// @todo what's the difference needsRedraw_/SetRedraw
	needsRedraw_ = true;
	SetRedraw(true);
}
// ---< TEMP



