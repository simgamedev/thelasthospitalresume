#pragma once

#include "GUIManager.h"
#include "../EventManager.h"
#include "GUIToggleButton.h"
class GUITabMenu
{
public:
	GUITabMenu(GUIManager* guiManager, const string& interfaceFileName,
			   EventManager* eventManager);
	GUITabMenu(GUIManager* guiManager,
			   EventManager* eventManager,
			   const string& name,
			   const vec2f& pos,
			   const vec2f& size);
	void AddTab(const string& name,
				const vec2f& pos,
				const vec2f& size);
	~GUITabMenu();
	void TabChanged(GUIToggleButton* currentButton);
	void TurnOffAll();
	template<class T>
	void SetTabChangedCallback(void(T::*func)(GUIToggleButton*), T* instance)
	{
		auto temp = std::bind(func, instance, std::placeholders::_1);
		tabChangedCallback_ = temp;
	}

	void Show();
	void Hide();
	bool IsActive() const;
	void SetPosition(const vec2f pos);
	// ---> Getters/Setters.
	vec2f GetSize() const;
	GUIToggleButton* GetCurrentOnButton();
public:
	GUIInterface* interface_;
	std::function<void(GUIToggleButton*)> tabChangedCallback_;



	GUIManager* guiManager_;
	EventManager* eventManager_;
};
