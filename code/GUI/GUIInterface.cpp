#include "GUIInterface.h"
#include "GUIManager.h"
#include "GUITextfield.h"
#include "../SharedContext.h"
#include "../Window.h"


/********************************************************************************
 * Constructor.
 ********************************************************************************/
GUIInterface::GUIInterface(const string& name, GUIManager* guiManager, GUIInterface* owner)
	: GUIElement(name, GUIElementType::Interface, (owner ? owner : this))
	, parent_(nullptr)
	, guiManager_(guiManager)
	, movable_(false)
	, beingMoved_(false)
	, showTitleBar_(false)
	, focused_(false)
	, scrollHorizontal_(0)
	, scrollVertical_(0)
	, contentLayerRedraw_(true)
	, controlLayerRedraw_(true)
{
	bgLayerRenderTexture_ = std::make_unique<sf::RenderTexture>();
	contentLayerRenderTexture_ = std::make_unique<sf::RenderTexture>();
	controlLayerRenderTexture_ = std::make_unique<sf::RenderTexture>();
}

GUIInterface::~GUIInterface() {}

void
GUIInterface::SetPosition(const vec2f& pos)
{
	GUIElement::SetPosition(pos);
	bgLayerSprite_.setPosition(pos);
	contentLayerSprite_.setPosition(pos);
	controlLayerSprite_.setPosition(pos);
	// exactly just above window
	titleBar_.setPosition(position_.x, position_.y - titleBar_.getSize().y);
	drawable_.text.setPosition(titleBar_.getPosition() + styles_[state_].textPadding);
}

void
GUIInterface::PlaceAtCenterWindow()
{
	vec2f size = styles_[state_].size;
	vec2u windowSize = guiManager_->GetContext()->window->GetWindowSize();
	SetPosition({(windowSize.x / 2) - size.x / 2, (windowSize.y / 2) - size.y / 2 });
}


/********************************************************************************
 * Add Element to gui window.
 ********************************************************************************/
bool
GUIInterface::AddElement(const GUIElementType& type, const string& name)
{
	if(elements_.find(name) != elements_.end()) { return false; }
	std::unique_ptr<GUIElement> element(guiManager_->CreateElement(type, this));
	if(!element) { return false; }
	element->SetName(name);
	element->SetOwner(this);
	elements_.emplace(name, std::move(element));
	contentLayerRedraw_ = true;
	controlLayerRedraw_ = true;
	return true;
}

/********************************************************************************
 * Add Element to gui window.
 ********************************************************************************/
bool
GUIInterface::AddElement(GUIElement* guiElement)
{
	//elements_.emplace(guiElement->GetName(), std::move(guiElement));
	elements_.emplace(guiElement->GetName(), guiElement);
	return true;
}

GUIElement*
GUIInterface::GetElement(const string& name) const
{
	auto elementsItr = elements_.find(name);
	if(elementsItr == elements_.end())
	{
		return nullptr;
	}
	else
	{
		//auto element = elementsItr->second;
		//return element.get();
		return elementsItr->second.get();
	}
}

/*********************************************************************************
 * Get Elements.
 *********************************************************************************/
std::unordered_map<string, std::unique_ptr<GUIElement>>&
GUIInterface::GetElements()
{
	return elements_;
}


/********************************************************************************
 * Remove an element from interface.
 ********************************************************************************/
bool
GUIInterface::RemoveElement(const string& name)
{
	auto elementsItr = elements_.find(name);
	if(elementsItr == elements_.end()) { return false; }
	/// @NOTE: it's a smart pointer, so we don't need delete anymore.
	elements_.erase(elementsItr);
	contentLayerRedraw_ = true;
	controlLayerRedraw_ = true;
	AdjustContentSize();
	return true;
}


/********************************************************************************
 * Remove elements with names containing xxx.
 ********************************************************************************/
bool
GUIInterface::RemoveElementsWithNamesContaining(const string& nameSnippet)
{
	bool success = false;
	for(auto elementsItr = elements_.begin();
		elementsItr != elements_.end();)
	{
		string elementName = elementsItr->first;
		if(elementName.find(nameSnippet) == std::string::npos)
		{
			++elementsItr;
			continue;
		}
		elementsItr = elements_.erase(elementsItr);
		success = true;
	}
	if(!success) { return success; }
	contentLayerRedraw_ = true;
	controlLayerRedraw_ = true;
	AdjustContentSize();
	return success;
}

bool
GUIInterface::HasParent() const
{
	return parent_ != nullptr;
}


GUIManager*
GUIInterface::GetManager() const
{
	return guiManager_;
}

bool
GUIInterface::IsInside(const vec2f& point) const
{
	/// @todo make sure this is how we call parent class method???
	if(GUIElement::IsInside(point))
	{
		return true;
	}

	if(showTitleBar_ &&
	   titleBar_.getGlobalBounds().contains(point))
	{
		return true;
	}

	return false;
}

void
GUIInterface::Focus()
{
	OnFocus();
}

void
GUIInterface::Defocus()
{
	OnDefocus();
}

bool
GUIInterface::IsFocused() const
{
	return focused_;
}


/********************************************************************************
 * Read from file.
 ********************************************************************************/
void
GUIInterface::ReadIn(std::stringstream& stream)
{
	string movable;
	string showTitleBar;
	string titleString;
	stream >> elementsPadding_.x >> elementsPadding_.y >> movable >> showTitleBar;
	if(movable == "Movable") { movable_ = true; }
	if (showTitleBar == "ShowTitleBar")
	{
		showTitleBar_ = true;
		Utils::ReadQuotedString(stream, titleString);
		drawable_.text.setString(titleString);
	}
}

/********************************************************************************
 * 
 ********************************************************************************/
bool
GUIInterface::ContentLayerContainsPoint(const vec2f& point)
{
	vec2f globalPos = GetGlobalPosition();
	vec2i size;
	if(contentRectSize_ == vec2i(0, 0))
	{
		size = vec2i(styles_[state_].size);
	}
	else
	{
		size = contentRectSize_;
	}
	globalPos += contentPositionOffset_;
	sf::FloatRect rect;
	rect.left = globalPos.x;
	rect.top = globalPos.y;
	rect.width = size.x;
	rect.height = size.y;

	return rect.contains(point);
}


/********************************************************************************
 * On Click.
 ********************************************************************************/
void
GUIInterface::OnClick(const vec2f& mousePos)
{
	// ---> TEMP
	if (this->GetName() == "TileSetSubInterface")
	{
		int32 a = 100;
	}
	// ---< TEMP
	DefocusTextfields();
	// ---> Clicking on titleBar
	if(showTitleBar_ &&
	   titleBar_.getGlobalBounds().contains(mousePos))
	{
		if(movable_) { beingMoved_ = true; }
		for(auto& itr : elements_)
		{
			auto& element = itr.second;
			if(!element->IsActive()) { continue; }
			element->OnInterfaceClick(mousePos - GetPosition());
		}
	}

	// ---> Not on titleBar
	GUIEvent guiEvent;
	guiEvent.type = GUIEventType::Click;
	guiEvent.interfaceName = name_;
	guiEvent.elementName.clear();
	vec2f interfacePos = GetPosition();
	// ---> TEMP
	if (this->owner_ != this)
	{
		interfacePos += owner_->GetPosition();
	}
	// ---< TEMP
	guiEvent.clickCoords.x = mousePos.x - interfacePos.x;
	guiEvent.clickCoords.y = mousePos.y - interfacePos.y;
	guiManager_->AddEvent(guiEvent);

	bool contentLayerHasMouse = ContentLayerContainsPoint(mousePos);
	for(auto& itr : elements_)
	{
		auto& element = itr.second;
		if(!element->IsActive()) { continue; }
		vec2f elementPos = element->GetPosition();
		vec2f scrollOffset = vec2f(0.0f, 0.0f);
		// ---> Click is on interface
		if(!element->IsInside(mousePos))
		{
			/// @todo what does OnInterfaceClick take as parameters??
			element->OnInterfaceClick(mousePos - interfacePos - elementPos + scrollOffset);
			continue;
		}

		// ---> Click is on elements.
		if(!contentLayerHasMouse && !element->IsControl()) { continue; }
		guiEvent.elementName = element->name_;
		if(!element->IsControl())
		{
			elementPos += contentPositionOffset_;
			scrollOffset = vec2f(scrollHorizontal_, scrollVertical_);
		}
		guiEvent.clickCoords.x = mousePos.x - interfacePos.x - elementPos.x + scrollOffset.x;
		guiEvent.clickCoords.y = mousePos.y - interfacePos.y - elementPos.y + scrollOffset.y;

		// ---> TEMP
		if (element->GetType() == GUIElementType::Interface)
		{
			element->OnClick(mousePos);
			// ---> TEMP
			guiEvent.interfaceName = element->GetName();
			guiEvent.elementName.clear();
			// ---< TEMP
		}
		else
		{
			element->OnClick({ guiEvent.clickCoords.x, guiEvent.clickCoords.y });
			//element->OnClick(mousePos);
		}
		// ---< TEMP
		guiManager_->AddEvent(guiEvent);
	}
	SetState(GUIElementState::Clicked);
}

void
GUIInterface::OnRelease()
{
	GUIEvent guiEvent;
	guiEvent.type = GUIEventType::Release;
	guiEvent.interfaceName = name_;
	guiEvent.elementName.clear();
	vec2f interfacePos = GetPosition();
	// ---> TEMP make clear of parent_/owner_
	if (this->parent_)
	{
		interfacePos = GetGlobalPosition();
	}
	// ---< TEMP
	vec2i mousePos = guiManager_->GetContext()->eventManager->GetMousePos();
	guiEvent.clickCoords.x = mousePos.x - interfacePos.x;
	guiEvent.clickCoords.y = mousePos.y - interfacePos.y;
	guiManager_->AddEvent(guiEvent);

	for(auto& itr : elements_)
	{
		auto& element = itr.second;
		if(element->GetState() != GUIElementState::Clicked)
		{
			continue;
		}
		guiEvent.elementName = element->name_;

		vec2f elementPos = element->GetPosition();
		vec2f scrollOffset = vec2f(0.0f, 0.0f);
		if(!element->IsControl())
		{
			elementPos += contentPositionOffset_;
			scrollOffset = vec2f(scrollHorizontal_, scrollVertical_);
		}
		guiEvent.clickCoords.x = mousePos.x - interfacePos.x - elementPos.x + scrollOffset.x;
		guiEvent.clickCoords.y = mousePos.y - interfacePos.y - elementPos.y + scrollOffset.y;

		element->OnRelease();
		if (guiEvent.elementName == "TileSetSprite")
		{
			int32 a = 100;
		}
		guiManager_->AddEvent(guiEvent);
	}

	SetState(GUIElementState::Focused);
}


void
GUIInterface::OnHover(const vec2f& mousePos)
{
	GUIEvent guiEvent;
	guiEvent.type = GUIEventType::Hover;
	guiEvent.interfaceName = name_;
	guiEvent.elementName.clear();
	guiEvent.clickCoords.x = mousePos.x;
	guiEvent.clickCoords.y = mousePos.y;
	guiManager_->AddEvent(guiEvent);

	/// @todo why don't we iterarte all elements in OnHover() method???
	SetState(GUIElementState::Focused);
}

void
GUIInterface::OnLeave()
{
	GUIEvent guiEvent;
	guiEvent.type = GUIEventType::Leave;
	guiEvent.interfaceName = name_;
	guiEvent.elementName.clear();
	guiManager_->AddEvent(guiEvent);

	/// @todo why don't we iterarte all elements in OnHover() method???
	SetState(GUIElementState::Neutral);
}


void
GUIInterface::OnTextEntered(const char& characterEntered)
{
	// TODO: Should be no ontextentered when no interafe is active.
	//std::cout << "GUIInterface::OnTextEntered!" << std::endl;
	/// @todo make sure only one textfiled get the input.
	for(auto& itr : elements_)
	{
		auto& element = itr.second;
		if(!element->IsActive()) { continue; }
		if(element->GetType() != GUIElementType::Textfield) { continue; }
		if(element->GetState() != GUIElementState::Clicked) { continue; }
		auto textfield = static_cast<GUITextfield*>(element.get());
		textfield->HandleCharacterEntered(characterEntered);
		return;
	}
}


void
GUIInterface::OnFocus()
{
	guiManager_->BringToFront(this);
	if(focused_) { return; }
	focused_ = true;
	GUIEvent guiEvent;
	guiEvent.type = GUIEventType::Focus;
	guiEvent.interfaceName = name_;
	guiEvent.elementName.clear();
	guiManager_->AddEvent(guiEvent);

	for(auto& itr : elements_)
	{
		auto& element = itr.second;
		if(!element->IsActive()) { continue; }
		// @todo maby element->OnFocus should be called OnPanelFocus()??
		element->OnFocus();
		guiEvent.elementName = element->name_;
		guiManager_->AddEvent(guiEvent);
	}
}


void
GUIInterface::OnDefocus()
{
	if(!focused_) { return; }
	focused_ = false;
	GUIEvent guiEvent;
	guiEvent.type = GUIEventType::Defocus;
	guiEvent.interfaceName = name_;
	guiEvent.elementName.clear();

	for(auto& itr : elements_)
	{
		auto& element = itr.second;
		if(!element->IsActive()) { continue; }
		element->OnDefocus();
		guiEvent.elementName = element->name_;
		guiManager_->AddEvent(guiEvent);
	}
}

void
GUIInterface::OnArrowKey(const string& eventName)
{
}

vec2f
GUIInterface::GetPadding() const
{
	return elementsPadding_;
}

void
GUIInterface::SetPadding(const vec2f& padding)
{
	elementsPadding_ = padding;
}

void
GUIInterface::Update(real32 dt)
{
	vec2f mousePos = vec2f(guiManager_->GetContext()->eventManager->GetMousePos());

	// ---> Drag Panel Around
	if(beingMoved_ && mouseLastPos_ != mousePos)
	{
		vec2f diff = mousePos - mouseLastPos_;
		mouseLastPos_ = mousePos;
		vec2f newPos = position_ + diff;
		SetPosition(newPos);
	}
	// ---<

	bool contentLayerHasMouse = ContentLayerContainsPoint(mousePos);
	for(auto& itr : elements_)
	{
		// ---> Redraw elements if needed.
		auto& element = itr.second;
		// ---> TEMP
		// ---< TEMP
		if(element->NeedsRedraw())
		{
			if(element->IsControl())
			{
				controlLayerRedraw_ = true;
			}
			else
			{
				contentLayerRedraw_ = true;
			}
		}
		// ---> TEMP
		if (element->GetType() == GUIElementType::Interface)
		{
			contentLayerRedraw_ = true;
		}
		// ---< TEMP

		if(!element->IsActive())
		{
			continue;
		}

		element->Update(dt);

		if(beingMoved_) { continue; }

		GUIEvent guiEvent;
		guiEvent.interfaceName = name_;
		guiEvent.elementName = element->name_;
		guiEvent.clickCoords.x = mousePos.x;
		guiEvent.clickCoords.y = mousePos.y;
		if (element->name_ == "HScrollBar" &&
			mousePos.x <= 260)
		{
			int32 a = 100;
		}
		if(IsInside(mousePos) &&
		   element->IsInside(mousePos) &&
		   !titleBar_.getGlobalBounds().contains(mousePos) &&
		   !(!contentLayerHasMouse && !element->IsControl()) &&
		   state_ != GUIElementState::Neutral)
		{
			if(element->GetState() != GUIElementState::Neutral) { continue; }
			element->OnHover(mousePos);
			guiEvent.type = GUIEventType::Hover;
			guiManager_->AddEvent(guiEvent);
		}
		else if(element->GetState() == GUIElementState::Focused)
		{
			element->OnLeave();
			guiEvent.type = GUIEventType::Leave;
			guiManager_->AddEvent(guiEvent);
		}
	}
}

void
GUIInterface::Draw(sf::RenderTarget* renderTarget)
{
	// ---> draw the 3 layers
	renderTarget->draw(bgLayerSprite_);
	renderTarget->draw(contentLayerSprite_);
	renderTarget->draw(controlLayerSprite_);

	// ---> draw titleBar
	if(!showTitleBar_) { return; }
	renderTarget->draw(titleBar_);
	renderTarget->draw(drawable_.text);
}

bool
GUIInterface::IsBeingMoved() const { return beingMoved_; }
bool
GUIInterface::IsMovable() const { return movable_; }

void
GUIInterface::BeginMoving()
{
	if(!showTitleBar_ || !movable_) { return; }
	beingMoved_ = true;
	vec2i mousePos = guiManager_->GetContext()->eventManager->GetMousePos();
	mouseLastPos_ = vec2f(mousePos);
}

void
GUIInterface::StopMoving()
{
	beingMoved_ = false;
}

vec2f
GUIInterface::GetGlobalPosition() const
{
	vec2f pos = position_;
	GUIInterface* parent = parent_;
	while(parent)
	{
		pos += parent->GetPosition();
		parent = parent->parent_;
	}
	return pos;
}


/********************************************************************************
 * Apply Style.
 ********************************************************************************/
#define TITLEBAR_HEIGHT 32.0f
void
GUIInterface::ApplyStyle()
{
	GUIElement::ApplyStyle();
	drawable_.bgSolid.setPosition(0.0f, 0.0f);
	drawable_.bgImage.setPosition(0.0f, 0.0f);
	/// @todo should we move titleBar_ to struct drawable?
	titleBar_.setSize(vec2f(styles_[state_].size.x, TITLEBAR_HEIGHT));
	titleBar_.setPosition(position_.x, position_.y - titleBar_.getSize().y);
	if (styles_[state_].outlineThickness != 0.0f)
	{
		//titleBar_.setOutlineThickness(styles_[state_].outlineThickness);
		titleBar_.setOutlineThickness(-1.0f);
		titleBar_.setOutlineColor(sf::Color(26, 17, 10, 255));
	}
	/// @todo come up with a better name instead of "elementColor"
	titleBar_.setFillColor(styles_[state_].elementColor);
	drawable_.text.setPosition(titleBar_.getPosition() + styles_[state_].textPadding);
	/// @todo panel drawable's text&glyph is on titleBar, refelct that on your naming style.
	drawable_.glyphImage.setPosition(titleBar_.getPosition() + styles_[state_].glyphPadding);
}


/********************************************************************************
 * Redraws Bg Layer.
 ********************************************************************************/
void
GUIInterface::RedrawBgLayer()
{
	// ---> create/recreate bgLayerRenderTexture
	if(bgLayerRenderTexture_->getSize().x != styles_[state_].size.x ||
	   bgLayerRenderTexture_->getSize().y != styles_[state_].size.y)
	{
		bgLayerRenderTexture_->create(static_cast<uint32>(styles_[state_].size.x),
									  static_cast<uint32>(styles_[state_].size.y));
	}
	bgLayerRenderTexture_->clear(sf::Color(0, 0, 0, 0));
	/// @todo why do we call ApplyStyle here???
	ApplyStyle();
	bgLayerRenderTexture_->draw(drawable_.bgSolid);
	if(!styles_[state_].bgImageName.empty())
	{
		bgLayerRenderTexture_->draw(drawable_.bgImage);
	}

	bgLayerRenderTexture_->display();
	bgLayerSprite_.setTexture(bgLayerRenderTexture_->getTexture());
	/// @todo is the even neccesary since we need the full renderTexture drawn?
	bgLayerSprite_.setTextureRect(sf::IntRect(0, 0,
											 static_cast<int32>(styles_[state_].size.x),
											 static_cast<int32>(styles_[state_].size.y)));
	SetRedraw(false);
}

bool
GUIInterface::NeedsContentLayerRedraw() const { return contentLayerRedraw_; }
/********************************************************************************
 * Redraw Content Layer.
 ********************************************************************************/
void
GUIInterface::RedrawContentLayer()
{
	// ---> Prepare texture for drawing.
	if(contentLayerRenderTexture_->getSize().x != contentSize_.x ||
	   contentLayerRenderTexture_->getSize().y != contentSize_.y)
	{
		contentLayerRenderTexture_->create(static_cast<uint32>(contentSize_.x),
										   static_cast<uint32>(contentSize_.y));
	}
	contentLayerRenderTexture_->clear(sf::Color(0, 0, 0, 0));

	if (this->GetName() == "TestInterface")
	{
		int32 a = 100;
	}
	// ---> Draw.
	for(auto& itr : elements_)
	{
		auto& element = itr.second;
		// ---> TEMP
		// ---< TEMP
		if(!element->IsActive() || element->IsControl()) { continue; }
		element->ApplyStyle();
		// ---> TEMP
		if (element->GetType() == GUIElementType::Interface)
		{
			GUIInterface* gI = (GUIInterface*)element.get();
			gI->RedrawBgLayer();
			gI->RedrawContentLayer();
			gI->RedrawControlLayer();
		}
		// ---<
		element->Draw(contentLayerRenderTexture_.get());
		element->SetRedraw(false);
	}
	
	// ---> Display.
	contentLayerRenderTexture_->display();
	contentLayerSprite_.setTexture(contentLayerRenderTexture_->getTexture());

	vec2i visibleSize;
	if(contentRectSize_ == vec2i(0, 0)) // No ContentRect enabled
	{
		visibleSize = vec2i(styles_[state_].size.x, styles_[state_].size.y);
	}
	else
	{
		visibleSize = contentRectSize_;
	}

	contentLayerSprite_.setTextureRect(sf::IntRect(scrollHorizontal_,
												   scrollVertical_,
												   visibleSize.x,
												   visibleSize.y));
	contentLayerRedraw_ = false;
}




bool
GUIInterface::NeedsControlLayerRedraw() const { return controlLayerRedraw_; }
/********************************************************************************
 * Redraw Control Layer.
 ********************************************************************************/
void
GUIInterface::RedrawControlLayer()
{
	// ---> prepares texture for drawing.
	if(controlLayerRenderTexture_->getSize().x != styles_[state_].size.x ||
	   controlLayerRenderTexture_->getSize().y != styles_[state_].size.y)
	{
		controlLayerRenderTexture_->create(static_cast<uint32>(styles_[state_].size.x),
									 static_cast<uint32>(styles_[state_].size.y));
	}
	controlLayerRenderTexture_->clear(sf::Color(0, 0, 0, 0));

	// ---> draw
	for(auto& itr : elements_)
	{
		auto& element = itr.second;
		// --> looking for active control element
		if(!element->IsActive() || !element->IsControl())
		{
			continue;
		}
		element->ApplyStyle();
		element->Draw(controlLayerRenderTexture_.get());
		element->SetRedraw(false);
	}

	// ---> display
	controlLayerRenderTexture_->display();;
	controlLayerSprite_.setTexture(controlLayerRenderTexture_->getTexture());
	controlLayerSprite_.setTextureRect(sf::IntRect(0, 0,
												  static_cast<int32>(styles_[state_].size.x),
												  static_cast<int32>(styles_[state_].size.y)));
	controlLayerRedraw_ = false;
}

	

/********************************************************************************
 * Adjust Content Size.
 ********************************************************************************/
void
GUIInterface::AdjustContentSize(const GUIElement* refElement)
{
	if(refElement)
	{
		// ---> TEMP
		vec2f bottomRight = refElement->GetPosition() + refElement->GetSize();
		if (bottomRight.x > contentSize_.x)
		{
			contentSize_.x = bottomRight.x;
			controlLayerRedraw_ = true;
		}
		if (bottomRight.y > contentSize_.y)
		{
			contentSize_.y = bottomRight.y;
			controlLayerRedraw_ = true;
		}
		return;
		// ---< TEMP
	}

	vec2f farthest = GetSize();
	for(auto& elementsItr: elements_)
	{
		auto& element = elementsItr.second;
		if(!element->IsActive() || element->IsControl()) { continue; }
		vec2f bottomRight = element->GetPosition() + element->GetSize();
		if(bottomRight.x > farthest.x)
		{
			farthest.x = bottomRight.x;
			/// @todo doesn't we need contentRedraw here?
			controlLayerRedraw_ = true;
		}
		if(bottomRight.y > farthest.y)
		{
			farthest.y = bottomRight.y;
			controlLayerRedraw_ = true;
		}
	}
	SetContentSize(farthest);
}

void
GUIInterface::SetContentSize(const vec2f& size)
{
	contentSize_ = size;
}

void
GUIInterface::UpdateScrollHorizontal(uint32 percent)
{
	if(percent > 100) { return; }
	real32 size;
	if(contentRectSize_.x == 0)
	{
		size = GetSize().x;
	}
	else
	{
		size = contentRectSize_.x;
	}
	/// @todo what exactly???
	scrollHorizontal_ = static_cast<int32>(((contentSize_.x - size) / 100) * percent);
	sf::IntRect rect = contentLayerSprite_.getTextureRect();
	contentLayerSprite_.setTextureRect(sf::IntRect(scrollHorizontal_, scrollVertical_, rect.width, rect.height));
}

void
GUIInterface::UpdateScrollVertical(uint32 percent)
{
	if(percent > 100) { return; }
	real32 size;
	if(contentRectSize_.y == 0)
	{
		size = GetSize().y;
	}
	else
	{
		size = contentRectSize_.y;
	}
	scrollVertical_ = static_cast<int>(((contentSize_.y - size) / 100) * percent);
	sf::IntRect rect = contentLayerSprite_.getTextureRect();
	contentLayerSprite_.setTextureRect(sf::IntRect(scrollHorizontal_, scrollVertical_, rect.width, rect.height));
}

vec2f
GUIInterface::GetContentSize() const
{
	return contentSize_;
}

vec2f
GUIInterface::GetContentOffset() const
{
	return contentPositionOffset_;
}

/// @todo attach a pictures showing what is what in GUI documentation.
void
GUIInterface::SetContentRectSize(const vec2i& size)
{
	contentRectSize_ = size;
}

void
GUIInterface::SetContentOffset(const vec2f& offset)
{
	contentPositionOffset_ = offset;
}

/********************************************************************************
 * Defocus all textfields.
 ********************************************************************************/
void
GUIInterface::DefocusTextfields()
{
	GUIEvent guiEvent;
	guiEvent.type = GUIEventType::Defocus;
	guiEvent.interfaceName = name_;
	guiEvent.elementName.clear();
	for(auto& itr : elements_)
	{
		auto& element = itr.second;
		if(element->GetType() != GUIElementType::Textfield) { continue; }
		element->OnDefocus();
		if(!element->IsActive() ||
		   !IsActive())
		{
			continue;
		}
		guiEvent.elementName = element->name_;
		guiManager_->AddEvent(guiEvent);
	}
}
	
/********************************************************************************
 * Request Content Redraw.
 ********************************************************************************/
void
GUIInterface::RequestContentRedraw()
{
	contentLayerRedraw_ = true;
	// ---> TEMP
	// ---< TEMP
}


// ---> TEMP
void
GUIInterface::SetShowTitleBar(bool showTitleBar)
{
	showTitleBar_ = showTitleBar;
}

void
GUIInterface::SetMovable(bool movable)
{
	movable_ = movable;
}
void
GUIInterface::SetTitle(const string& title)
{
	drawable_.text.setString(title);
}
// ---< TEMP
// ---> TEMP
bool
GUIInterface::HasSubInterface()
{
	return (subInterface_ != nullptr);
}
GUIInterface*
GUIInterface::GetSubInterface()
{
	return subInterface_;
}
