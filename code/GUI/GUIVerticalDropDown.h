#pragma once
#include "../common.h"
#include <SFML/System/Vector2.hpp>
#include "../GameStateTypes.h"
#include <vector>
#include <functional>

class EventManager;
class GUIElement;
class GUIInterface;
class GUIManager;
struct EventDetails;

class GUIVerticalDropDown
{
public:
	GUIVerticalDropDown(EventManager* eventManager, GUIManager* guiManager, GUIElement* element = nullptr);
	~GUIVerticalDropDown();

	void Setup(const GameStateType& gameState, const string& name, const string& interfaceFileName);
	// ---> TEMP
	void SetupNew(const GameStateType& gameState, const string& name, const string& interfaceFileName);
	template<class T>
	void SetEntryClickCallback(void(T::*func)(const string&), T* instance)
	{
		auto temp = std::bind(func, instance, std::placeholders::_1);
		entryClickCallback_ = temp;
	}
	// ---< TEMP
	void SetPosition(const vec2f& position);
	void Show();
	void BringToFront();
	void Hide();

	string GetSelected() const;
	void ResetSelected();

	void AddEntry(const string& entry);
	void RemoveEntry(const string& entry);
	void PurgeEntries();

	void OnClick(EventDetails* eventDetails);
	void Redraw();
private:
	GUIInterface* interface_;
	/// @attention element_ is the btn to click to get the dropdown
	GUIElement* element_;
	string interfaceName_;
	string entryStyleFileName_;

	std::vector<string> entries_;

	string entryCallbackNamePrefix_;
	string selection_;
	GameStateType gameState_;

	GUIManager* guiManager_;
	EventManager* eventManager_;
	std::function<void(const string&)> entryClickCallback_;
};
