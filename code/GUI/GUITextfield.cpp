#include "GUITextfield.h"
#include <iostream>
#include <algorithm>
#include "../Utilities.h"


/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
GUITextfield::GUITextfield(const string& name, GUIInterface* owner)
	: GUIElement(name, GUIElementType::Textfield, owner)
	, cursorPosition_(0)
	, cursorSize_(0)
	, elapsedTime_(0.0f)
	, showCursor_(false)
{
}


GUITextfield::~GUITextfield()
{
}


void
GUITextfield::HandleCharacterEntered(const char& character)
{
	if(GetState() != GUIElementState::Clicked) { return; }
	// Backspace
	std::cout << "should handle character entered!" << std::endl;
	if(character == 8)
	{
		if(!cursorPosition_) { return; }
		string text = GetText();
		text.erase(cursorPosition_ - 1, 1);
		SetText(text);
		MoveCursor(-1);
		return;
	}
	if(character < 32 || character > 126) { return; }
	string text = GetText();
	text.insert(cursorPosition_, 1, character);
	SetText(text);
	MoveCursor(+1);
	return;
}


void
GUITextfield::HandleArrowKey(const string& eventName)
{
}

void
GUITextfield::ReadIn(std::stringstream& stream)
{
	std::string content;
	Utils::ReadQuotedString(stream, content);
	drawable_.text.setString(content);
}

void
GUITextfield::OnClick(const vec2f& mousePos)
{
	SetState(GUIElementState::Clicked);
	cursorPosition_ = GetText().size();
	if(cursorPosition_ < 0)
	{
		cursorPosition_ = 0;
	}
}

void
GUITextfield::OnRelease()
{
}

void
GUITextfield::OnHover(const vec2f& mousePos)
{
	if(state_ == GUIElementState::Clicked) { return; }
	SetState(GUIElementState::Focused);
}

void
GUITextfield::OnLeave()
{
	if(state_ == GUIElementState::Clicked) { return; }
	SetState(GUIElementState::Neutral);
}

void
GUITextfield::OnDefocus()
{
	SetState(GUIElementState::Neutral);
}

void
GUITextfield::Update(real32 dt)
{
	if(state_ != GUIElementState::Clicked) { return; }
	elapsedTime_ += dt;
	if(elapsedTime_ >= 0.5f)
	{
		showCursor_ = !showCursor_;
		elapsedTime_ = 0.0f;
		needsRedraw_ = true;
	}
	cursorSize_ = drawable_.text.getCharacterSize();
	vec2f cursorPixelPosition = GetPosition();
	cursorPixelPosition.x += GetCursorPixelPosX();
	/// @todo i think +1 is padding for 1?
	cursorPixelPosition.x += 1;
	cursorPixelPosition.y += 1;
	cursor_[0].position = cursorPixelPosition;
	cursor_[1].position = cursorPixelPosition;
	cursor_[1].position.y += cursorSize_;
}

void
GUITextfield::Draw(sf::RenderTarget* renderTarget)
{
	renderTarget->draw(drawable_.bgSolid);
	if(!styles_[state_].glyphImageName.empty())
	{
		renderTarget->draw(drawable_.glyphImage);
	}
	renderTarget->draw(drawable_.text);
	// ---> draw cursor
	if(state_ != GUIElementState::Clicked) { return; }
	if(!showCursor_) { return; }
	renderTarget->draw(cursor_, 2, sf::Lines);
}

void
GUITextfield::MoveCursor(int32 amount)
{
	cursorPosition_ += amount;
	// ---> Cap cursor position.
	if(cursorPosition_ < 0)
	{
		cursorPosition_ = 0;
	}
	else if(cursorPosition_ > drawable_.text.getString().getSize())
	{
		cursorPosition_ += drawable_.text.getString().getSize();
	}
			
}
	  

real32
GUITextfield::GetCursorPixelPosX()
{
	auto cStr = drawable_.text.getString().toAnsiString();
	auto font = drawable_.text.getFont();
	real32 result = 0.0f;
	for(size_t i = 0; i < cursorPosition_; ++i)
	{
		auto fontGlyph = font->getGlyph(cStr[i], drawable_.text.getCharacterSize(), false);
		result += fontGlyph.advance;
	}
	return result;
}
