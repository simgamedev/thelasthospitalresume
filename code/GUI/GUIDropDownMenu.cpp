#include "GUIDropDownMenu.h"
#include "GUIManager.h"
#include "../SharedContext.h"



/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
GUIDropDownMenu::GUIDropDownMenu(const string& name, GUIInterface* owner)
	: GUIElement(name, GUIElementType::DropDownMenu, owner)
	, dropDown_(owner_->GetManager()->GetContext()->eventManager,
				owner_->GetManager(),
				this)
{
	dropDown_.Hide();
	SetText("Select...");
}


/********************************************************************************
 * Destructor.
 ********************************************************************************/
GUIDropDownMenu::~GUIDropDownMenu() {}


/********************************************************************************
 * Get DropDown.
 ********************************************************************************/
GUIVerticalDropDown*
GUIDropDownMenu::GetDropDown()
{
	return &dropDown_;
}

/********************************************************************************
 * Read In.
 ********************************************************************************/
void
GUIDropDownMenu::ReadIn(std::stringstream& stream)
{
	if(!(stream >> interfaceFileName_)) { return; }
}


/********************************************************************************
 * On Click.
 ********************************************************************************/
void
GUIDropDownMenu::OnClick(const vec2f& mousePos)
{
	if(state_ == GUIElementState::Clicked)
	{
		SetState(GUIElementState::Neutral);
		dropDown_.Hide();
		return;
	}
	SetState(GUIElementState::Clicked);
	dropDown_.Show();
	dropDown_.SetPosition(GetGlobalPosition() + vec2f(0.0f, GetSize().y));
}

/********************************************************************************
 * When click lands on owner interafce.
 ********************************************************************************/
void
GUIDropDownMenu::OnInterfaceClick(const vec2f& mousePos)
{
	SetState(GUIElementState::Neutral);
	dropDown_.Hide();
}


void
GUIDropDownMenu::OnRelease()
{
}


void
GUIDropDownMenu::OnHover(const vec2f& mousePos)
{
	if(state_ == GUIElementState::Clicked) { return; }
	SetState(GUIElementState::Focused);
}

void
GUIDropDownMenu::OnLeave()
{
	if(state_ == GUIElementState::Clicked) { return; }
	SetState(GUIElementState::Neutral);
}

void
GUIDropDownMenu::OnFocus()
{
}

void
GUIDropDownMenu::OnDefocus()
{
	if(state_ != GUIElementState::Clicked) { return; }
	dropDown_.BringToFront();
}

void
GUIDropDownMenu::Setup()
{
	// ---> TEMP
	/*
	if(this->GetName() == "TileSetDropDown")
	{
		dropDown_.SetupNew(owner_->GetManager()->GetCurrentGameState(),
						   "_dropdown_" + name_,
						   interfaceFileName_);
	}
	// ---< TEMP
	else
	{
		dropDown_.Setup(owner_->GetManager()->GetCurrentGameState(),
						"_dropdown_" + name_,
						interfaceFileName_);
	}*/
	dropDown_.SetupNew(owner_->GetManager()->GetCurrentGameState(),
					   "_dropdown_" + name_,
					   interfaceFileName_);
}

/********************************************************************************
 * Empty Update Method.
 ********************************************************************************/
void
GUIDropDownMenu::Update(real32 dt)
{
}

void
GUIDropDownMenu::Draw(sf::RenderTarget* renderTarget)
{
	// draw bg solid
	renderTarget->draw(drawable_.bgSolid);
	// draw glyph image
	if(!styles_[state_].glyphImageName.empty())
	{
		renderTarget->draw(drawable_.glyphImage);
	}
	// draw text
	renderTarget->draw(drawable_.text);
}
