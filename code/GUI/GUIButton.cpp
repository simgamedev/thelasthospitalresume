#include "GUIButton.h"
#include "../Utilities.h"


/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
GUIButton::GUIButton(const string& name, GUIInterface* owner)
	: GUIElement(name, GUIElementType::Button, owner)
{
}


GUIButton::~GUIButton() {}

void
GUIButton::ReadIn(std::stringstream& stream)
{
	string content;
	Utils::ReadQuotedString(stream, content);
	/// @todo should be widesstring to support Chinese
	drawable_.text.setString(content);
}


void
GUIButton::OnClick(const vec2f& mousePos)
{
	std::cout << "On Click" << std::endl;
	SetState(GUIElementState::Clicked);
	//pSoundManager_->PlaySound(styles_[GUIElementState::Clicked].soundName);
}


void
GUIButton::OnRelease()
{
	std::cout << "On Release" << std::endl;
	SetState(GUIElementState::Neutral);
	//pSoundManager_->PlaySound(styles_[GUIElementState::Neutral].soundName);
}

void
GUIButton::OnHover(const vec2f& mousePos)
{
	std::cout << "On Hover" << std::endl;
	SetState(GUIElementState::Focused);
	//pSoundManager_->PlaySound(styles_[GUIElementState::Focused].soundName);
}

void
GUIButton::OnLeave()
{
	std::cout << "On Leave" << std::endl;
	SetState(GUIElementState::Neutral);
}

void
GUIButton::Update(real32 dt)
{
}

void
GUIButton::Draw(sf::RenderTarget* renderTarget)
{
	/// @todo should be able to set no bg for labels.
	// draw bg image
	if (!styles_[state_].bgImageName.empty())
	{
		renderTarget->draw(drawable_.bgImage);
	}
	else
	{
		// draw bg solid
		renderTarget->draw(drawable_.bgSolid);
	}
	// draw glyph
	if(!styles_[state_].glyphImageName.empty())
	{
		LOG_ENGINE_ERROR(styles_[state_].glyphImageName);
		renderTarget->draw(drawable_.glyphImage);
	}
	// draw text
	renderTarget->draw(drawable_.text);
}


/*********************************************************************************
 * Set Text Padding.
 *********************************************************************************/
void
GUIButton::SetTextPadding(vec2f textPadding)
{
	for (auto& itr : styles_)
	{
		GUIStyle& style = itr.second;
		//style.textCenterOrigin = true;
		style.textPadding = textPadding;
	}
}


/*********************************************************************************
 * Set Glyph Image Name.
 *********************************************************************************/
void
GUIButton::SetGlyphImageName(const string& glyphImageName)
{
	for (auto& itr : styles_)
	{
		// NOTE: don't use a reference.
		GUIStyle style = itr.second;
		style.glyphImageName = glyphImageName;
		UpdateStyle(itr.first, style);
	}
}
