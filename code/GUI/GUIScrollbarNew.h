#pragma once
#include "GUIElement.h"


/**
   A simple scrollbar.
 */
class GUIScrollbarNew : public GUIElement
{
public:
	GUIScrollbarNew(const string& name, GUIInterface* owner);
	~GUIScrollbarNew();

	void SetPosition(const vec2f& pos);

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mosuePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();

	void ApplyStyle();
	void UpdateStyle(const GUIElementState& state, const GUIStyle& style);

	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);
private:
	SliderDirection direction_;
	/// @todo slider should also be a sprite
	/// @todo slider should responds to mouse wheel too.
	sf::RectangleShape sliderShape_;
	vec2f mouseLastPos_;
	int32 percentage_;
};
