#include "GUIToggleButton.h"
#include "../Utilities.h"
#include "GUITabMenu.h"

/*********************************************************************************
 * Default Constructorl.
 *********************************************************************************/
GUIToggleButton::GUIToggleButton(const string& name, GUIInterface* owner)
    : GUIElement(name, GUIElementType::ToggleButton, owner)
    , toggleButtonState_(ToggleButtonState::Off)
    , menu_(nullptr)
{
}

GUIToggleButton::~GUIToggleButton()
{
}

void
GUIToggleButton::ReadIn(std::stringstream& stream)
{
	string content;
	Utils::ReadQuotedString(stream, content);
	/// @todo should be widesstring to support Chinese
	drawable_.text.setString(content);
}

void
GUIToggleButton::OnClick(const vec2f& mousePos)
{
    LOG_ENGINE_ERROR("CheckBox On Click");
	SetState(GUIElementState::Clicked);
}
/*********************************************************************************
 * Toggle
 *********************************************************************************/
void
GUIToggleButton::Toggle()
{
    if (toggleButtonState_ == ToggleButtonState::Off)
    {
        toggleButtonState_ = ToggleButtonState::On;
    }
    else
    {
        toggleButtonState_ = ToggleButtonState::Off;
    }
}

void
GUIToggleButton::OnRelease()
{
    LOG_ENGINE_ERROR("CheckBox On Release");
	SetState(GUIElementState::Neutral);
    if (toggleButtonState_ == ToggleButtonState::On)
    {
        UnCheck();
    }
    else
    {
        Check();
    }
}

void
GUIToggleButton::OnHover(const vec2f& mousePos)
{
    LOG_ENGINE_ERROR("CheckBox On Hover");
	SetState(GUIElementState::Focused);
}


void
GUIToggleButton::OnLeave()
{
    LOG_ENGINE_ERROR("CheckBox On Leave");
	SetState(GUIElementState::Neutral);
}

void
GUIToggleButton::Update(real32 dt)
{
}

void
GUIToggleButton::Draw(sf::RenderTarget* renderTarget)
{
    if (toggleButtonState_ == ToggleButtonState::On)
    {
        drawable_.bgSolid.setFillColor(styles_.at(GUIElementState::Clicked).bgColor);
        renderTarget->draw(drawable_.glyphImage);
	}
    renderTarget->draw(drawable_.bgSolid);
	renderTarget->draw(drawable_.text);
}

// ---> TEMP
void
GUIToggleButton::Check()
{
    LOG_ENGINE_ERROR("Checked");
    toggleButtonState_ = ToggleButtonState::On;
    SetRedraw(true);
    if (menu_)
    {
        menu_->TabChanged(this);
    }
}
// ---< TEMP
void
GUIToggleButton::UnCheck()
{
    toggleButtonState_ = ToggleButtonState::Off;
    SetRedraw(true);
}

bool
GUIToggleButton::IsOn() const
{
	if(toggleButtonState_ == ToggleButtonState::On)
		return true;
	else
		return false;
}
