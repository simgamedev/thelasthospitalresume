#include "GUIVerticalDropDown.h"
#include "GUIManager.h"


/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
GUIVerticalDropDown::GUIVerticalDropDown(EventManager* eventManager,
										 GUIManager* guiManager,
										 GUIElement* element)
	: eventManager_(eventManager)
	, guiManager_(guiManager)
	, interface_(nullptr)
	, element_(element)
{
}



/********************************************************************************
 * Destructor.
 ********************************************************************************/
GUIVerticalDropDown::~GUIVerticalDropDown()
{
	eventManager_->RemoveCallback(gameState_, entryCallbackNamePrefix_ + "Click");
	eventManager_->RemoveCallback(gameState_, entryCallbackNamePrefix_ + "Leave");
	if(interface_)
	{
		guiManager_->RemoveInterface(gameState_, interfaceName_);
	}
}
/********************************************************************************
 * SetupNew
 ********************************************************************************/
void
GUIVerticalDropDown::SetupNew(const GameStateType& gameState,
						      const string& interfaceName,
						      const string& interfaceFileName)
{
	if(interface_) { return; }
	gameState_ = gameState;
	// Load interface from file
	guiManager_->AddInterfaceDefault(gameState,
									 interfaceName,
									 "",
									 0, 32, (int32)element_->GetSize().x, 600,
									 false,
									 false);
	interfaceName_ = interfaceName;
	interface_ = guiManager_->GetInterface(gameState_, interfaceName);
	/// @todo why do we keep a currentGameState_ in VerticalDropDown?
	interface_->SetActive(false);
	// @todo should we call it DummyEntry?
	//entryStyleFileName_ = interface_->GetElement("Entry_")->GetStyleFileName();
	entryStyleFileName_ = "DefaultDropDownEntry.style";

	entryCallbackNamePrefix_ = "_callback" + interfaceName_;
	//std::unique_ptr<Binding> binding;
	GUIEvent guiEvent;
	guiEvent.interfaceName = interfaceName_;
	guiEvent.elementName = "*";
	//binding = std::make_unique<Binding>(entryCallbackNamePrefix_ + "Click");
	/// @todo change to smart ptr or figure out where to delete/new
	Binding* binding = new Binding(entryCallbackNamePrefix_ + "Click");
	guiEvent.type = GUIEventType::Click;
	EventInfo eventInfo(guiEvent);
	binding->BindEvent(EventType::GUIClick, eventInfo);
	eventManager_->AddBinding(binding);
	eventManager_->AddCallback(gameState_,
							   entryCallbackNamePrefix_ + "Click",
							   &GUIVerticalDropDown::OnClick,
							   this);
}


/********************************************************************************
 * Setup.
 ********************************************************************************/
void
GUIVerticalDropDown::Setup(const GameStateType& gameState,
						   const string& interfaceName,
						   const string& interfaceFileName)
{
	if(interface_) { return; }
	gameState_ = gameState;
	interfaceName_ = interfaceName;
	// Load interface from file
	if(!guiManager_->LoadInterface(gameState_, interfaceFileName, interfaceName_))
	{
		return;
	}
	/// @todo why do we keep a currentGameState_ in VerticalDropDown?
	interface_ = guiManager_->GetInterface(gameState_, interfaceName_);
	interface_->SetActive(false);
	// @todo should we call it DummyEntry?
	entryStyleFileName_ = interface_->GetElement("Entry_")->GetStyleFileName();
	//entryStyleFileName_ = "DefaultDropDownEntry.style";

	entryCallbackNamePrefix_ = "_callback" + interfaceName_;
	//std::unique_ptr<Binding> binding;
	GUIEvent guiEvent;
	guiEvent.interfaceName = interfaceName_;
	guiEvent.elementName = "*";
	//binding = std::make_unique<Binding>(entryCallbackNamePrefix_ + "Click");
	/// @todo change to smart ptr or figure out where to delete/new
	Binding* binding = new Binding(entryCallbackNamePrefix_ + "Click");
	guiEvent.type = GUIEventType::Click;
	EventInfo eventInfo(guiEvent);
	binding->BindEvent(EventType::GUIClick, eventInfo);
	eventManager_->AddBinding(binding);
	eventManager_->AddCallback(gameState_,
							   entryCallbackNamePrefix_ + "Click",
							   &GUIVerticalDropDown::OnClick,
							   this);
}

/********************************************************************************
 * Set Position.
 ********************************************************************************/
void
GUIVerticalDropDown::SetPosition(const vec2f& position)
{
	if(!interface_) { return; }
	interface_->SetPosition(position);
}

void
GUIVerticalDropDown::Show()
{
	if(!interface_) { return; }
	interface_->SetActive(true);
	interface_->Focus();
}

void
GUIVerticalDropDown::BringToFront()
{
	if(!interface_) { return; }
	guiManager_->BringToFront(interface_);
}

void
GUIVerticalDropDown::Hide()
{
	if(!interface_) { return; }
	interface_->SetActive(false);
	if(!element_) { return; }
	element_->SetState(GUIElementState::Neutral);
}

string
GUIVerticalDropDown::GetSelected() const
{
	return selection_;
}

/********************************************************************************
 * Reset Selected.
 ********************************************************************************/
void
GUIVerticalDropDown::ResetSelected()
{
	selection_ = "";
	if(!element_) { return; }
	element_->SetText("Select...");
}


void
GUIVerticalDropDown::AddEntry(const string& entry)
{
	entries_.emplace_back(entry);
}

void
GUIVerticalDropDown::RemoveEntry(const string& entry)
{
	entries_.erase(std::find_if(entries_.begin(),
								entries_.end(),
								[&entry](const string& str)
								{
									return str == entry;
								}));
}

/********************************************************************************
 * Purge Entries.
 ********************************************************************************/
void
GUIVerticalDropDown::PurgeEntries()
{
	entries_.clear();
}
	

/********************************************************************************
 * On Click.
 ********************************************************************************/
void
GUIVerticalDropDown::OnClick(EventDetails* eventDetails)
{
	std::cout << "GUIVerticalDropDown::OnClick()" << std::endl;
	if(eventDetails->elementName == "") { return; }
	selection_ = interface_->GetElement(eventDetails->elementName)->GetText();
	Hide();
	if(!element_) { return; }
	element_->SetText(selection_);
	element_->SetRedraw(true);

	// ---> TEMP
	entryClickCallback_(selection_);
	// ---< TEMP
}


/********************************************************************************
 * Redraw.
 ********************************************************************************/
void
GUIVerticalDropDown::Redraw()
{
	if(!interface_) { return; }
	interface_->RemoveElementsWithNamesContaining("Entry_");
	size_t index = 0;
	vec2f position = vec2f(0.0f, 0.0f);
	for(auto entry : entries_)
	{
		interface_->AddElement(GUIElementType::Button, "Entry_" + std::to_string(index));
		GUIElement* entryElement = interface_->GetElement("Entry_" + std::to_string(index));
		entryElement->SetText(entry);
		entryElement->SetPosition(position);
		guiManager_->LoadStyleNew(entryStyleFileName_, entryElement, vec2f(element_->GetSize().x, 32.0f));
		//guiManager_->LoadStyle(entryStyleFileName_, entryElement);
		position.y += entryElement->GetSize().y + 1.0f;
		++index;
	}
}

	
