#include "GUITabMenu.h"
#include "../GameStateTypes.h"
#include "../Utilities.h"

/********************************************************************************
 * Constructor.
 ********************************************************************************/
GUITabMenu::GUITabMenu(GUIManager* guiManager, const string& interfaceFileName,
                       EventManager* eventManager)
    : guiManager_(guiManager)
    , eventManager_(eventManager)
{
    guiManager->LoadInterfaceNew(interfaceFileName, "GamePlayBottomTabs");
    interface_ = guiManager->GetInterface("GamePlayBottomTabs");
    interface_->SetPosition(vec2f(0, 0));

    for (auto& itr : interface_->GetElements())
    {
        GUIToggleButton* toggleButton = (GUIToggleButton*)itr.second.get();
        //toggleButton->UnCheck();
        toggleButton->menu_ = this;
    }
}

/*********************************************************************************
 * Destrucotr.
 *********************************************************************************/
GUITabMenu::~GUITabMenu()
{
    if (interface_)
    {
        string name = interface_->GetName();
		guiManager_->RemoveInterface(name);
    }
}

/********************************************************************************
 * Constructor.
 ********************************************************************************/
GUITabMenu::GUITabMenu(GUIManager* guiManager,
		               EventManager* eventManager,
		               const string& name,
		               const vec2f& pos,
		               const vec2f& size)
    : guiManager_(guiManager)
    , eventManager_(eventManager)
    , interface_(nullptr)
{
    guiManager_->AddInterfaceDefault(GameStateType::Game,
                                     name,
                                     name,
                                     pos.x, pos.y, size.x, size.y,
                                     false,
                                     false);
    interface_ = guiManager_->GetInterface(name);
}
/*********************************************************************************
 * Tab Changed.
 *********************************************************************************/
void
GUITabMenu::TabChanged(GUIToggleButton* currentButton)
{
    // ---> Toggle Other Buttons First.
    for (auto& itr : interface_->GetElements())
    {
        GUIToggleButton* toggleButton = (GUIToggleButton*)itr.second.get();
        if (toggleButton != currentButton)
        {
            toggleButton->UnCheck();
        }
    }
    // ---<
    tabChangedCallback_(currentButton);
}

/*********************************************************************************
 * Turn Off All.
 *********************************************************************************/
void
GUITabMenu::TurnOffAll()
{
    // ---> Toggle Other Buttons First.
    for (auto& itr : interface_->GetElements())
    {
        GUIToggleButton* toggleButton = (GUIToggleButton*)itr.second.get();
		toggleButton->UnCheck();
    }
    // ---<
}
/********************************************************************************
 * Add Tab.
 ********************************************************************************/
void
GUITabMenu::AddTab(const string& name,
                   const vec2f& pos,
                   const vec2f& size)
{
    GUIToggleButton* tab = guiManager_->AddToggleButtonDefault(interface_,
                                                               name,
                                                               pos,
                                                               size,
                                                               name,
                                                               "Content");
    tab->menu_ = this;
}
/*********************************************************************************
 * Show/Hide.
 *********************************************************************************/
void
GUITabMenu::Show()
{
	interface_->SetActive(true);
}


void
GUITabMenu::Hide()
{
	interface_->SetActive(false);
}

bool
GUITabMenu::IsActive() const
{
    return interface_->IsActive();
}

/********************************************************************************
 * Getters/Setters
 ********************************************************************************/
void
GUITabMenu::SetPosition(const vec2f pos)
{
	interface_->SetPosition(pos);
}	

vec2f
GUITabMenu::GetSize() const
{
	return interface_->GetSize();
}

GUIToggleButton*
GUITabMenu::GetCurrentOnButton()
{
    for (auto& itr : interface_->GetElements())
    {
        GUIToggleButton* toggleButton = (GUIToggleButton*)itr.second.get();
		if(toggleButton->IsOn())
		{
			return toggleButton;
		}
    }
}
