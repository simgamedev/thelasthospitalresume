#include "GUIElement.h"
#include "GUIInterface.h"
#include "GUIManager.h"
#include "../SharedContext.h"
#include "../TextureManager.h"
#include "../FontManager.h"
#include "../Utilities.h"


/********************************************************************************
 * Constructor.
 ********************************************************************************/
GUIElement::GUIElement(const string& name,
					   const GUIElementType& type,
					   GUIInterface* owner)
	: name_(name)
	, type_(type)
	, owner_(owner)
	, state_(GUIElementState::Neutral)
	, needsRedraw_(false)
	, active_(true)
	, isControl_(false)
{
}


/********************************************************************************
 * Destructor.
 ********************************************************************************/
GUIElement::~GUIElement()
{
	ReleaseResources();
}


/********************************************************************************
 Release Resources.
 ********************************************************************************/
void
GUIElement::ReleaseResources()
{
	for(auto& stylesItr : styles_)
	{
		GUIStyle& style = stylesItr.second;
		ReleaseTexture(style.bgImageName);
		ReleaseTexture(style.glyphImageName);
		ReleaseFont(style.fontName);
	}
}

void
GUIElement::UpdateStyle(const GUIElementState& state, const GUIStyle& style)
{
	if(style.bgImageName != styles_[state].bgImageName)
	{
		ReleaseTexture(styles_[state].bgImageName);
		RequireTexture(style.bgImageName);
	}

	if(style.glyphImageName != styles_[state].glyphImageName)
	{
		ReleaseTexture(styles_[state].glyphImageName);
		RequireTexture(style.glyphImageName);
	}

	if(style.fontName != styles_[state].fontName)
	{
		ReleaseFont(styles_[state].fontName);
		RequireFont(style.fontName);
	}

	styles_[state] = style;
	/// @todo why?
	if(state == state_)
	{
		SetRedraw(true);
		ApplyStyle();
	}
}


/********************************************************************************
 * Apply Style.
 ********************************************************************************/
void
GUIElement::ApplyStyle()
{
	//Logger::Instance()->EngineLogger()->info("<GUIElement>" + this->GetName() + "::ApplyStyle() called!");
	ApplyBgStyle();
	ApplyTextStyle();
	ApplyGlyphStyle();
	if(styles_[state_].stretchToFit)
	{
		styles_[state_].size = GetContentSize();
	}
	if(owner_ != this && !IsControl())
	{
		// ---> TEMP
		owner_->AdjustContentSize(this);
		//std::cout << "AdjustContentSize Called From: " << this->name_ << std::endl;
		// ---< TEMP
	}
	// ---> FIXME
	//std::cout << "ApplyStyle called from " << this->name_ << std::endl;
}

void
GUIElement::ApplyTextStyle()
{
	FontManager* fontManager = owner_->GetManager()->GetContext()->fontManager;
	const GUIStyle& currentStyle = styles_[state_];
	if(!currentStyle.fontName.empty())
	{
		drawable_.text.setFont(*fontManager->GetResource(currentStyle.fontName));
		drawable_.text.setFillColor(currentStyle.textColor);
		drawable_.text.setCharacterSize(currentStyle.textSize);
		if(currentStyle.textCenterOrigin)
		{
			Utils::CenterSFMLText(drawable_.text);
		}
		else
		{
			drawable_.text.setOrigin(0.0f, 0.0f);
		}
	}

	if(currentStyle.textCenterOrigin && currentStyle.stretchToFit)
	{
		/// @todo I don't thikn stretchToFit is used here.
		vec2f contentSize = GetContentSize();
		vec2f position = position_ + (contentSize / 2.0f);
		drawable_.text.setPosition(position + currentStyle.textPadding);
	}
	else if (currentStyle.textCenterOrigin && !currentStyle.stretchToFit)
	{
		vec2f contentSize = GetContentSize();
		vec2f position = position_ + (contentSize / 2.0f);
		drawable_.text.setPosition(position + currentStyle.textPadding);
	}
	else
	{
		if (this->GetName() == "Pan")
		{
			int32 a = 100;
		}
		drawable_.text.setPosition(position_ + currentStyle.textPadding);
	}
}

void
GUIElement::ApplyBgStyle()
{
	TextureManager* textureManager = owner_->GetManager()->GetContext()->textureManager;
	const GUIStyle& currentStyle = styles_[state_];
	// Bg is image
	if(!currentStyle.bgImageName.empty())
	{
		sf::Texture* bgTexture = textureManager->GetResource(currentStyle.bgImageName);
		vec2f textureSize = vec2f(bgTexture->getSize());
		drawable_.bgImage.setScale(currentStyle.size.x / textureSize.x, currentStyle.size.y / textureSize.y);
		drawable_.bgImage.setTexture(*bgTexture);
		drawable_.bgImage.setColor(currentStyle.bgImageColor);
		drawable_.bgImage.setPosition(position_);
	}
	// Bg is solid color
	else
	{
		/// @todo you either have bg image, or bg solid.
		drawable_.bgSolid.setSize(currentStyle.size);
		drawable_.bgSolid.setFillColor(currentStyle.bgColor);
		drawable_.bgSolid.setPosition(position_);
		//  element has outline
		if (currentStyle.outlineThickness != 0.0f)
		{
			drawable_.bgSolid.setOutlineThickness(-2.0f);
			drawable_.bgSolid.setOutlineColor(sf::Color(26, 17, 10, 255));
		}
	}
}

void
GUIElement::ApplyGlyphStyle()
{
	TextureManager* textureManager = owner_->GetManager()->GetContext()->textureManager;
	const GUIStyle& currentStyle = styles_[state_];
	if(!currentStyle.glyphImageName.empty())
	{
		sf::Texture* glyphTexture = textureManager->GetResource(currentStyle.glyphImageName);
		drawable_.glyphImage.setTexture(*glyphTexture);
		vec2f glyphTextureSize = vec2f(glyphTexture->getSize());
		vec2u glyphImageSize = currentStyle.glyphImageSize;
		vec2f glyphImageScale;
		if(currentStyle.glyphImageScale.x > 0.0f)
		{
			glyphImageScale = currentStyle.glyphImageScale;
		}
		else
		{
			glyphImageScale = vec2f(glyphImageSize.x / glyphTextureSize.x,
									glyphImageSize.y / glyphTextureSize.y);
		}
		drawable_.glyphImage.setScale(glyphImageScale);
	}

	drawable_.glyphImage.setPosition(position_ + currentStyle.glyphPadding);
}

/********************************************************************************
 * Get Content Size.
 ********************************************************************************/
vec2f
GUIElement::GetContentSize() const
{
	const GUIStyle& currentStyle = styles_.at(state_);
	sf::FloatRect textBounds = drawable_.text.getGlobalBounds();
	vec2f textSize = vec2f(textBounds.width, textBounds.height);
	vec2f bgSolidSize = drawable_.bgSolid.getSize();
	sf::FloatRect bgImageBounds = drawable_.bgImage.getGlobalBounds();
	vec2f bgImageSize = vec2f(bgImageBounds.width, bgImageBounds.height);
	sf::FloatRect glyphImageBounds = drawable_.glyphImage.getGlobalBounds();
	vec2f glyphImageSize = vec2f(glyphImageBounds.width, glyphImageBounds.height);

	vec2f maxSize;
	maxSize.x = std::max(textSize.x, bgSolidSize.x);
	maxSize.x = std::max(maxSize.x, bgImageSize.x);
	maxSize.x = std::max(maxSize.x, glyphImageSize.x);
	maxSize.x = std::max(maxSize.x, currentStyle.size.x);

	maxSize.y = std::max(textSize.y, bgSolidSize.y);
	maxSize.y = std::max(maxSize.y, bgImageSize.y);
	maxSize.y = std::max(maxSize.y, glyphImageSize.y);
	maxSize.y = std::max(maxSize.y, currentStyle.size.y);

	return maxSize;
}


GUIElementType
GUIElement::GetType() const
{

	return type_;
}

void
GUIElement::SetState(const GUIElementState& state)
{
	if(state_ == state) { return; }
	state_ = state;
	SetRedraw(true);
}

string
GUIElement::GetName() const { return name_; }

void
GUIElement::SetName(const string& name) { name_ = name; }

const vec2f&
GUIElement::GetPosition() const { return position_; }

void
GUIElement::SetPosition(const vec2f& position)
{
	position_ = position;
	if(owner_ == nullptr || owner_ == this) { return; }
	const vec2f& padding = owner_->GetPadding();
	if(position_.x < padding.x) { position_.x = padding.x; }
	if(position_.y < padding.y) { position_.y = padding.y; }
}

vec2f
GUIElement::GetSize() const { return styles_.at(state_).size; }

GUIElementState
GUIElement::GetState() const { return state_; }

void
GUIElement::SetRedraw(const bool& redraw)
{
	needsRedraw_ = redraw;
}

bool
GUIElement::NeedsRedraw() const { return needsRedraw_; }

void
GUIElement::SetOwner(GUIInterface* owner) { owner_ = owner; }

GUIInterface*
GUIElement::GetOwner() const
{
	return owner_;
}

bool
GUIElement::HasOwner() const
{
	return owner_ != nullptr;
}

bool
GUIElement::IsActive() const
{
	return active_;
}

void
GUIElement::SetActive(const bool& active)
{
	if(active != active_)
	{
		active_ = active;
		SetRedraw(true);
	}
}

bool
GUIElement::IsControl() const
{
	return isControl_;
}

void
GUIElement::SetIsControl(const bool& control)
{
	isControl_ = control;
}

string
GUIElement::GetText() const
{
	return drawable_.text.getString();
}

void
GUIElement::SetText(const string& text)
{
	drawable_.text.setString(text);
	// TODO: is this the proper place to set redraw?
	SetRedraw(true);
}


/********************************************************************************
 * Is Mouse Inside Element?.
 ********************************************************************************/
bool
GUIElement::IsInside(const vec2f& point) const
{
	vec2f globalPosition = GetGlobalPosition();
	return(point.x >= globalPosition.x &&
		   point.y >= globalPosition.y &&
		   point.x <= globalPosition.x + styles_.at(state_).size.x &&
		   point.y <= globalPosition.y + styles_.at(state_).size.y);
}


/********************************************************************************
 * Get Global Position.
 ********************************************************************************/
vec2f
GUIElement::GetGlobalPosition() const
{
	vec2f position = GetPosition();
	if(owner_ == nullptr || owner_ == this) { return position; }
	position += owner_->GetGlobalPosition();
	if(IsControl()) { return position; }
	/// @todo is GetContentOffset() padding?
	position += owner_->GetContentOffset();
	position.x -= owner_->scrollHorizontal_;
	position.y -= owner_->scrollVertical_;

	return position;
}


void
GUIElement::RequireTexture(const string& name)
{
	/// @todo use Assert(!name.empty()) instead of if(name.empty()) { return; }
	if(name.empty()) { return; }
	owner_->GetManager()->GetContext()->textureManager->RequireResource(name);
}

void
GUIElement::RequireFont(const string& name)
{
	//assert(!name.empty());
	owner_->GetManager()->GetContext()->fontManager->RequireResource(name);
}

void
GUIElement::ReleaseTexture(const string& name)
{
	//assert(!name.empty());
	owner_->GetManager()->GetContext()->textureManager->ReleaseResource(name);
}

void
GUIElement::ReleaseFont(const string& name)
{
	//assert(!name.empty());
	owner_->GetManager()->GetContext()->fontManager->ReleaseResource(name);
}


string
GUIElement::GetStyleFileName() const
{
	return styles_.at(GUIElementState::Neutral).styleFileName;
}

/********************************************************************************
 * Get Text Local Bounds.
 ********************************************************************************/
sf::FloatRect
GUIElement::GetTextLocalBounds()
{
	return drawable_.text.getLocalBounds();
}




	


