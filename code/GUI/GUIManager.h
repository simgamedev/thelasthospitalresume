#pragma once
#include <unordered_map>
#include <functional>
#include <fstream>
#include <memory>
#include <SFML/Graphics.hpp>
#include "GUIInterface.h"
#include "GUIEvent.h"
#include "../StateDependent.h"
#include "../EventManager.h"

class GUISprite;
class GUIScrollbar;
class GUIButton;
class GUIToggleButton;
class GUILabel;
class GUIDropDownMenu;


struct SharedContext;
using GUIInterfaceEntry = std::pair<string, std::unique_ptr<GUIInterface>>;
using GUIInterfaceContainer = std::vector<GUIInterfaceEntry>;
/**
   Manages GUI for each GameStateType
 */
class GUIManager : public StateDependent
{
	friend class GUIInterface;
	friend class GUIElement;
public:
	/// @todo maybe change sharedContext to a Global Singleton.
	GUIManager(SharedContext* sharedContext);
	~GUIManager();

	bool AddInterface(const GameStateType& gameState, const string& name);
	// ---> TEMP
    bool AddInterfaceDefault(const GameStateType& gameState,
						  	 const string& name,
							 const string& title,
						     int32 x, int32 y, int32 w, int32 h,
							 bool showTitleBar,
							 bool movable);

	//HACK: NMSL
	GUILabel* AddLabelDefault(GUIInterface* guiInterface,
							  const string& name,
							  const vec2f& pos,
							  const vec2f& size,
							  const string& text);
	GUIButton* AddButtonDefault(GUIInterface* guiInterface,
								const string& name,
								const vec2f& pos,
								const vec2f& size,
								const string& text,
								const string& layer);
	GUIToggleButton* AddToggleButtonDefault(GUIInterface* guiInterface,
								      const string& name,
								      const vec2f& pos,
								      const vec2f& size,
								      const string& text,
								      const string& layer);
	GUIButton* AddButtonDefault(GUIInterface* guiInterface,
						        const string& name,
						        const vec2f& pos,
							    const vec2f& size,
						        const string& text,
								const string& glyphImageName,
						        const string& layer);
	GUIButton* AddButton(GUIInterface* guiInterface,
						 const string& name,
						 const vec2f& pos,
						 const vec2f& size,
						 const string& text,
						 const string& glyphImageName,
						 const string& styleFileName,
						 const string& layer);
	GUIToggleButton* AddCheckBoxDefault(GUIInterface* guiInterface,
									const string& name,
									const vec2f& pos,
									const vec2f& size);
	GUIInterface* ProduceInterface(const string& name,
								   const vec2f& size,
								   bool showTitleBar,
								   bool movable);
	GUISprite* AddSpriteDefault(GUIInterface* guiInterface,
								const string& name,
								const vec2f& pos,
								const vec2f& size,
								const string& textureName);
	GUIScrollbar* AddScrollBarDefault(GUIInterface* guiInterface,
							  		  const string& name,
							          const vec2f& pos,
							          const vec2f& size,
									  SliderDirection direction);
	void AddSubInterfaceDefault(GUIInterface* parent,
								GUIInterface* child);
	GUIDropDownMenu* AddDropDownMenuDefault(GUIInterface* guiInterface,
											const string& name,
											const vec2f& pos,
											const vec2f& size,
											const string& text);
											
	// ---< TEMP
	/** add panel for current game state */
	bool AddInterface(const string& name);
	GUIInterface* GetInterface(const GameStateType& gameState, const string& name);
	GUIInterface* GetInterface(const string& name);

	bool BringToFront(const GUIInterface* guiInterface);
	bool BringToFront(const GameStateType& gameState, const string& name);

	bool LoadInterface(const GameStateType& gameState, const string& interfaceFilename, const string& interfaceName);
	bool LoadInterface(const string& interfaceFilename, const string& interfaceName);
	bool LoadInterface(GUIInterface* guiInterface, const string& interfaceFilename);
	bool LoadInterfaceNew(const GameStateType& gameState, const string& interfaceFilename, const string& interfaceName);
	bool LoadInterfaceNew(const string& interfaceFilename, const string& interfaceName);
	bool LoadInterfaceNew(GUIInterface* guiInterface, const string& interfaceFilename);
	bool RemoveInterface(const GameStateType& gameState, const string& name);
	bool RemoveInterface(const string& name);

	void ChangeGameState(const GameStateType& gameState);
	void RemoveGameState(const GameStateType& gameState);

	SharedContext* GetContext() const;

	void DefocusAllInterfaces();

	void HandleClick(EventDetails* eventDetails);
	void HandleRelease(EventDetails* eventDetails);
	void HandleTextEntered(EventDetails* eventDetails);
	void HandleArrowKeys(EventDetails* eventDetails);
	void HandleMouseMoved(EventDetails* eventDetails);

	void AddEvent(GUIEvent guiEvent);
	bool PollEvent(GUIEvent& guiEvent);

	void Update(real32 dt);
	void Draw(sf::RenderWindow* renderWindow);

	template<class T>
	void RegisterElement(const GUIElementType& guiElementType)
	{
		factories_[guiElementType] = [](GUIInterface* owner) -> GUIElement*
		{
			return new T("", owner);
		};
	}

	bool LoadStyle(const string& styleFileName, GUIElement* guiElement);
	bool LoadStyleNew(const string& styleFileName, GUIElement* guiElement, vec2f elementSize);
private:
	void HandleBringToFront();	
	GUIElement* CreateElement(const GUIElementType& guiElementType, GUIInterface* owner);
	GUIElementType StringToType(const string& typeString) const;

	std::unordered_map<GameStateType, GUIInterfaceContainer> interfaces_;
	std::vector<std::pair<GameStateType, GUIInterface*>> toBringToFront_;
	std::unordered_map<GameStateType, std::vector<GUIEvent>> guiEvents_;
	SharedContext* sharedContext_;
	std::unordered_map<GUIElementType, std::function<GUIElement*(GUIInterface*)>> factories_;
	std::unordered_map<string, GUIElementType> elementTypes_;
};
