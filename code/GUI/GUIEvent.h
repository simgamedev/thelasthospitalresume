#pragma once
#include "../common.h"

enum class GUIEventType { None, Click, Release, Hover, Leave, Focus, Defocus };

struct ClickCoordinates
{
	ClickCoordinates& operator=(const ClickCoordinates& rhs)
	{
		if(&rhs != this)
		{
			x = rhs.x;
			y = rhs.y;
		}
		return *this;
	}

	real32 x,y;
};

struct GUIEvent
{
	GUIEvent& operator=(const GUIEvent& rhs)
	{
		if(&rhs != this)
		{
			type = rhs.type;
			elementName = rhs.elementName;
			interfaceName = rhs.interfaceName;
			clickCoords = rhs.clickCoords;
		}
		return *this;
	}

	GUIEventType type;
	string elementName;
	string interfaceName;
	union
	{
		ClickCoordinates clickCoords;
	};
};
