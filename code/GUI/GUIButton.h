#pragma once
#include "GUIElement.h"


class GUIButton : public GUIElement
{
public:
	GUIButton(const string& name, GUIInterface* owner);
	~GUIButton();

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mousePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();
	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);

	// ---> TEMP
	void SetTextPadding(vec2f textPadding);
	void SetGlyphImageName(const string& glyphImageName);
	// ---< TEMP
};
