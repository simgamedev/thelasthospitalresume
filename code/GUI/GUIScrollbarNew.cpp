#include "GUIScrollbarNew.h"
#include "GUIInterface.h"
#include "GUIManager.h"
#include "../Window.h"
#include "../SharedContext.h"


/********************************************************************************
 * Constructor.
 ********************************************************************************/
GUIScrollbarNew::GUIScrollbarNew(const string& name, GUIInterface* owner)
	: GUIElement(name, GUIElementType::ScrollbarNew, owner)
{
	isControl_ = true;
}
GUIScrollbarNew::~GUIScrollbarNew() {}


void
GUIScrollbarNew::SetPosition(const vec2f& pos)
{
	GUIElement::SetPosition(pos);
	//// @todo try comment it out.
	if(direction_ == SliderDirection::Horizontal)
	{
		position_.x = 0;
	}
	else
	{
		position_.y = 0;
	}
}

void
GUIScrollbarNew::ReadIn(std::stringstream& stream)
{
	std::string direction;
	stream >> direction;
	if(direction == "Horizontal")
	{
		direction_ = SliderDirection::Horizontal;
	}
	else
	{
		direction_ = SliderDirection::Vertical;
	}

	if(direction_ == SliderDirection::Horizontal)
	{
		sliderShape_.setPosition(0, GetPosition().y);
	}
	else
	{
		sliderShape_.setPosition(GetPosition().x, 0);
	}
}

/// @todo OnMouseWheel
/*
void
GUIScrollbarNew::OnMouseWheel()
{
	std::cout << "Deal with mouse wheel!!!!" << std::endl;
}
*/


void
GUIScrollbarNew::OnClick(const vec2f& mousePos)
{
	/// @todo what the is this??
	if(sliderShape_.getGlobalBounds().contains(mousePos + GetPosition())) { return; }
	SetState(GUIElementState::Clicked);
	/// @todo make sure
	//mouseLastPos_ = mousePos;
	auto context = owner_->GetManager()->GetContext();
	mouseLastPos_ = vec2f(context->eventManager->GetMousePos());
}

void
GUIScrollbarNew::OnRelease()
{
	/// @todo make sure's GUIScrollbarNew's OnRelease method works even when mouse releases outside of scrollbar
	SetState(GUIElementState::Neutral);
}

void
GUIScrollbarNew::OnHover(const vec2f& mousePos)
{
	SetState(GUIElementState::Focused);
}

void
GUIScrollbarNew::OnLeave()
{
	SetState(GUIElementState::Neutral);
}

void
GUIScrollbarNew::UpdateStyle(const GUIElementState& state, const GUIStyle& style)
{
	/// @todo make sure this is how you call a super class's method
	GUIElement::UpdateStyle(state, style);
	/// @todo so we only have thickness defined in .style/interface file?
	if(direction_ == SliderDirection::Horizontal)
	{
		styles_[state_].size.x = owner_->GetSize().x;
	}
	else
	{
		styles_[state_].size.y = owner_->GetSize().y;
	}
}

void
GUIScrollbarNew::ApplyStyle()
{
	GUIElement::ApplyStyle();
	sliderShape_.setFillColor(styles_[state_].elementColor);
	// ---> Set Size && Position
	sf::RectangleShape& bgSolid = drawable_.bgSolid;
	if(direction_ == SliderDirection::Horizontal)
	{
		// put it at bottom of panel
		SetPosition(vec2f(0, owner_->GetSize().y - bgSolid.getSize().y));
		// set scrollbar area's size
		bgSolid.setSize(vec2f(owner_->GetSize().x, styles_[state_].size.y));
		sliderShape_.setPosition(sliderShape_.getPosition().x, GetPosition().y);
	}
	else
	{
		// put it at rightmost side of panel
		SetPosition(vec2f(owner_->GetSize().x - bgSolid.getSize().x, 0));
		// set scrollbar area's size
		bgSolid.setSize(vec2f(styles_[state_].size.x, owner_->GetSize().y));
		sliderShape_.setPosition(GetPosition().x, sliderShape_.getPosition().y);
	}

	// ---> Slider size according to content ratio
	/// @todo comeup with a better name?
	real32 contentRatio;
	real32 sliderSize;
	if(direction_ == SliderDirection::Horizontal)
	{
		contentRatio = owner_->GetContentSize().x / owner_->GetSize().x;
		if(contentRatio < 1.0f)
		{
			contentRatio = 1.0f;
		}
		sliderSize = owner_->GetSize().x / contentRatio;
		sliderShape_.setSize(vec2f(sliderSize, bgSolid.getSize().y));
	}
	else
	{
		contentRatio = owner_->GetContentSize().y / owner_->GetSize().y;
		if(contentRatio < 1.0f)
		{
			contentRatio = 1.0f;
		}
		sliderSize = owner_->GetSize().y / contentRatio;
		sliderShape_.setSize(vec2f(bgSolid.getSize().x, sliderSize));
	}
	/// @todo why??
	bgSolid.setPosition(GetPosition());
}


/********************************************************************************
 * Update(real fun staff here!).
 ********************************************************************************/
/// @todo make a flowchat of how the GUI system works
void
GUIScrollbarNew::Update(real32 dt)
{
	if(GetState() != GUIElementState::Clicked) { return; }
	std::cout << "Deal with mouse wheel!!!!" << std::endl;
	SharedContext* context = owner_->GetManager()->GetContext();
	// ---> Move Slider by dragging mouse
	vec2f mousePos = vec2f(context->eventManager->GetMousePos());
	if(mouseLastPos_ == mousePos) { return; } // mouse hasn't really moved.
	vec2f diff = mousePos - mouseLastPos_;
	mouseLastPos_ = mousePos;

	if(direction_ == SliderDirection::Horizontal)
	{
		sliderShape_.move(diff.x, 0);
	}
	else
	{
		sliderShape_.move(0, diff.y);
	}

	// ---> Cap slider position
	if(direction_ == SliderDirection::Horizontal)
	{
		if(sliderShape_.getPosition().x < 0)
		{
			sliderShape_.setPosition(0, sliderShape_.getPosition().y);
		}
		if(sliderShape_.getPosition().x + sliderShape_.getSize().x > owner_->GetSize().x)
		{
			sliderShape_.setPosition(owner_->GetSize().x - sliderShape_.getSize().x, sliderShape_.getPosition().y);
		}
	}
	else
	{
		if(sliderShape_.getPosition().y < 0)
		{
			sliderShape_.setPosition(sliderShape_.getPosition().x, 0);
		}
		if(sliderShape_.getPosition().y + sliderShape_.getSize().y > owner_->GetSize().y)
		{
			sliderShape_.setPosition(sliderShape_.getPosition().x, owner_->GetSize().y - sliderShape_.getSize().y);
		}
	}

	// ---> scroll owner panel content
	real32 scrollableDistance;
	int32 percentage;
	if(direction_ == SliderDirection::Horizontal)
	{
		scrollableDistance = owner_->GetSize().x - sliderShape_.getSize().x;
		percentage = static_cast<int32>(sliderShape_.getPosition().x / scrollableDistance);
		owner_->UpdateScrollHorizontal(percentage);
	}
	else
	{
		scrollableDistance = owner_->GetSize().y - sliderShape_.getSize().y;
		percentage = static_cast<int32>(sliderShape_.getPosition().y / scrollableDistance);
		owner_->UpdateScrollVertical(percentage);
	}
}

void
GUIScrollbarNew::Draw(sf::RenderTarget* renderTarget)
{
	/// @todo drawable_.bgImage
	renderTarget->draw(drawable_.bgSolid);
	/// @todo drawable_.sliderImage
	renderTarget->draw(sliderShape_);
}



