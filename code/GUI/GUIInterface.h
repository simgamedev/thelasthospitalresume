#pragma once
#include <memory>
#include "GUIElement.h"

class GUIManager;

class GUIInterface : public GUIElement
{
	friend class GUIElement;
	friend class GUIManager;
public:
	GUIInterface(const string& name, GUIManager* guiManager, GUIInterface* owner = nullptr);
	~GUIInterface();

	void SetPosition(const vec2f& pos);
	void PlaceAtCenterWindow();

	bool AddElement(const GUIElementType& type, const string& name);
	// ---> TEMP
	bool AddElement(GUIElement* guiElement);
	bool HasSubInterface();
	GUIInterface* GetSubInterface();
	// ---<
	GUIElement* GetElement(const string& name) const;
	bool RemoveElement(const string& name);
	bool RemoveElementsWithNamesContaining(const string& nameSnippet);

	bool HasParent() const;
	GUIManager* GetManager() const;

	bool IsInside(const vec2f& point) const;

	void Focus();
	void Defocus();
	bool IsFocused() const;

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mousePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();
	void OnTextEntered(const char& characterEntered);
	void OnFocus();
	void OnDefocus();
	void OnArrowKey(const string& eventName);

	bool IsBeingMoved() const;
	bool IsMovable() const;
	void BeginMoving();
	void StopMoving();

	void ApplyStyle();
	vec2f GetPadding() const;
	void SetPadding(const vec2f& padding);
	vec2f GetGlobalPosition() const;
	vec2f GetContentSize() const;
	vec2i GetContentRectSize() const;
	vec2f GetContentOffset() const;
	void SetContentRectSize(const vec2i& size);
	void SetContentOffset(const vec2f& offset);

	void RedrawBgLayer();
	bool NeedsContentLayerRedraw() const;
	void RedrawContentLayer();
	bool NeedsControlLayerRedraw() const;
	void RedrawControlLayer();

	void RequestContentRedraw();
	void ToggelTitleBar();

	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);

	void UpdateScrollHorizontal(uint32 percent);
	void UpdateScrollVertical(uint32 percent);

	// ---> TMEP
	void SetShowTitleBar(bool showTitleBar);
	void SetMovable(bool movable);
	void SetTitle(const string& title);
	// ---<
	std::unordered_map<string, std::unique_ptr<GUIElement>>& GetElements();
private:
	/// @todo what does ContentLayerContainsPoint Do?
	bool ContentLayerContainsPoint(const vec2f& point);
	void DefocusTextfields();
	std::unordered_map<string, std::unique_ptr<GUIElement>> elements_;
	vec2f elementsPadding_;

	GUIInterface* parent_;
	GUIManager* guiManager_;

	// Render Textures
	std::unique_ptr<sf::RenderTexture> bgLayerRenderTexture_;
	sf::Sprite bgLayerSprite_;
	std::unique_ptr<sf::RenderTexture> controlLayerRenderTexture_;
	sf::Sprite controlLayerSprite_;
	std::unique_ptr<sf::RenderTexture> contentLayerRenderTexture_;
	sf::Sprite contentLayerSprite_;

	/// @todo tileBar_ should not only be a sf::RectangleShape
	sf::RectangleShape titleBar_;
	vec2f mouseLastPos_;
	bool showTitleBar_;
	bool movable_;
	bool beingMoved_;
	bool focused_;

	// @todo figure out exactly what does AdjustContentSize do
public:
	void AdjustContentSize(const GUIElement* refElement = nullptr);
private:
	void SetContentSize(const vec2f& size);
	vec2f contentSize_;
	vec2i contentRectSize_;
	/// @todo change to contentOffset_;
	vec2f contentPositionOffset_;
	/** scroll is in pixels. */
	/// scroll is in pixels. 
	int32 scrollHorizontal_;
	int32 scrollVertical_;
	bool contentLayerRedraw_;	// 
	bool controlLayerRedraw_;
	public:
	GUIInterface* subInterface_ = nullptr;
};
