#pragma once
#include "GUIElement.h"


enum ToggleButtonState { On, Off};
class GUITabMenu;
/**
 A Simpile ChekcBox
	*/
class GUIToggleButton : public GUIElement
{
public:
	GUIToggleButton(const string& name, GUIInterface* owner);
	~GUIToggleButton();

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mousePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();
	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);
	// ---> TEMP
	void Check();
	void UnCheck();
	void Toggle();
	bool IsOn() const;
	// ---< TEMP
public:
	ToggleButtonState toggleButtonState_;
	GUITabMenu* menu_; // parent tabmenu
};
