#pragma once
#include "../common.h"
#include <functional>


class GUIManager;
class GUIInterface;
class StateManager;
struct EventDetails;
enum class GameStateType;



/**
   Used to browse files.
 */
class GIFileExplorer
{
public:
	GIFileExplorer(std::string name, GUIManager* guiManager, StateManager* stateManager);
	~GIFileExplorer();
	
	void SetDirectory(string path);

	void BackToParentDirectory(EventDetails* eventDetails);
	void HandleEntryClick(EventDetails* eventDetails);
	void ActionButton(EventDetails* eventDetails);
	void CloseButton(EventDetails* eventDetails);

	void Hide();
	void Show();
	void SetLoadMode();
	void SetSaveMode();

	bool IsSaveMode() const;

	template<class T>
	void SetActionButtonCallback(void(T::*method)(const string&), T* instance)
	{
		actionButtonCallback_ = [instance, method](const string& aStr) -> void
		{
			(instance->*method)(aStr);
		};
	}
private:
	void ListFiles();
	GUIInterface* interface_;
	string name_;
	string currentDirectory_;

	string folderEntryStyleFileName_;
	string fileEntryStyleFileName_;

	GUIManager* guiManager_;
	StateManager* stateManager_;
	GameStateType currentGameStateType_;
	std::function<void(string)> actionButtonCallback_;
	bool isSaveMode_;
};


