#include "GIFileExplorer.h"

#include "GUIManager.h"
#include "../StateManager.h"
#include "../EventManager.h"



/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
GIFileExplorer::GIFileExplorer(string name, GUIManager* guiManager, StateManager* stateManager)
	: guiManager_(guiManager)
	, stateManager_(stateManager)
	, name_(name)
	, isSaveMode_(false)
{
	/// @todo interface_ = guiManager_->LoadInterface("FileExplorer.interface", name);
	guiManager_->LoadInterfaceNew("FileExplorer.interface", name);
	interface_ = guiManager_->GetInterface(name);
	/// @todo so the StateDependent is a bad design?
	currentGameStateType_ = stateManager_->GetCurrentGameStateType();
	folderEntryStyleFileName_ = interface_->GetElement("FolderEntry")->GetStyleFileName();
	fileEntryStyleFileName_ = interface_->GetElement("FileEntry")->GetStyleFileName();

	interface_->RemoveElement("FolderEntry");
	interface_->RemoveElement("FileEntry");
	interface_->SetContentRectSize({500, 260});
	interface_->SetContentOffset({0.0f, 0.0f});
	interface_->PlaceAtCenterWindow();

	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->AddCallback<GIFileExplorer>("FileExplorer_GoToParentDirectory",
											  &GIFileExplorer::BackToParentDirectory,
											  this);
	eventManager->AddCallback<GIFileExplorer>("FileExplorer_EntryClick",
											  &GIFileExplorer::HandleEntryClick,
											  this);
	eventManager->AddCallback<GIFileExplorer>("FileExplorer_ActionButtonClick",
											  &GIFileExplorer::ActionButton,
											  this);
	eventManager->AddCallback<GIFileExplorer>("FileExplorer_CloseButtonClick",
											  &GIFileExplorer::CloseButton,
											  this);
}


/********************************************************************************
 * Default Destructor.
 ********************************************************************************/
GIFileExplorer::~GIFileExplorer()
{
	guiManager_->RemoveInterface(currentGameStateType_, name_);

	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->RemoveCallback(currentGameStateType_, "FileExplorer_BackToParentDirectory");
	eventManager->RemoveCallback(currentGameStateType_, "FileExplorer_EntryClick");
	eventManager->RemoveCallback(currentGameStateType_, "FileExplorer_SaveOrLoadButton");
	eventManager->RemoveCallback(currentGameStateType_, "FileExplorer_CloseButton");
}

void
GIFileExplorer::HandleEntryClick(EventDetails* eventDetails)
{
	std::cout << "Should handle entry click" << std::endl;
	// ---> Click on Folder Entry.
	if(eventDetails->elementName.find("FolderEntry_") != std::string::npos)
	{
		string path = (currentDirectory_ +
					   interface_->GetElement(eventDetails->elementName)->GetText()
					   + "/");
		SetDirectory(path);
		interface_->UpdateScrollVertical(0);
	}
	// ---> Click on File Entry.
	else if(eventDetails->elementName.find("FileEntry_") != std::string::npos)
	{
		interface_->GetElement("FileName")->SetText(interface_->GetElement(eventDetails->elementName)->GetText());
	}
}

void
GIFileExplorer::ActionButton(EventDetails* eventDetails)
{
	if(actionButtonCallback_ == nullptr)
	{
		std::cout << "Save Or Load Callback Not Set Yet." << std::endl;
	}
	string filename = interface_->GetElement("FileName")->GetText();
	actionButtonCallback_(currentDirectory_ + filename);
}

void
GIFileExplorer::CloseButton(EventDetails* eventDetails)
{
	Hide();
}

void
GIFileExplorer::SetDirectory(string path)
{
	currentDirectory_ = path;
	std::replace(currentDirectory_.begin(),
				 currentDirectory_.end(),
				 '\\',
				 '/');
	//interface_->RemoveElementsWithNamesContaining("FileEntry_");
	ListFiles();
}


void
GIFileExplorer::ListFiles()
{
	interface_->RemoveElementsWithNamesContaining("FileEntry_");
	interface_->RemoveElementsWithNamesContaining("FolderEntry_");
	Logger::Instance()->EngineLogger()->warn("ListFiles Called");
	interface_->GetElement("Directory")->SetText(currentDirectory_);
	auto fileList = Utils::GetFileList(currentDirectory_, "*.*", true);
	Utils::SortFileList(fileList);
	GUIElement* backToParentButton = interface_->GetElement("ParentDir");
	real32 x = backToParentButton->GetPosition().x;
	//real32 y = backToParentButton->GetPosition().y + backToParentButton->GetSize().y + 1.0f;
	real32 y = backToParentButton->GetPosition().y + 41.0f;
	size_t i = 0;
	for(auto& filesItr : fileList)
	{
		string filename = filesItr.first;
		bool isDirectory = filesItr.second;
		if(filename == "." || filename == "..") { continue; }
		string entryPrefix = (isDirectory ? "FolderEntry_" : "FileEntry_");
		interface_->AddElement(GUIElementType::Button,
							   entryPrefix + std::to_string(i));
		GUIElement* element = interface_->GetElement(entryPrefix + std::to_string(i));
		element->SetText(filename);
		element->SetPosition({x, y});
		guiManager_->LoadStyleNew(isDirectory ? folderEntryStyleFileName_ : fileEntryStyleFileName_,
								  element,
								  vec2f(300.0f, 40.0f));
		//y += backToParentButton->GetSize().y + 4.0f;
		std::cout << "my y pos is :" << y << "," << filename << std::endl;
		y += 41.0f;
		++i;
	}
}


void
GIFileExplorer::SetSaveMode()
{
	isSaveMode_ = true;
	interface_->GetElement("ActionButton")->SetText("Save");
}

void
GIFileExplorer::SetLoadMode()
{
	isSaveMode_ = false;
	interface_->GetElement("ActionButton")->SetText("Load");
}

bool
GIFileExplorer::IsSaveMode() const
{
	return isSaveMode_;
}

void
GIFileExplorer::BackToParentDirectory(EventDetails* eventDetails)
{
	int32 lastForwardSlashPos = currentDirectory_.find_last_of("/", currentDirectory_.length() - 2);
	if(lastForwardSlashPos != std::string::npos)
	{
		string parentDirectory = currentDirectory_.substr(0U, lastForwardSlashPos + 1);
		SetDirectory(parentDirectory);
	}
}


void
GIFileExplorer::Show()
{
	interface_->SetActive(true);
	interface_->PlaceAtCenterWindow();
	ListFiles();
	interface_->Focus();
}


void
GIFileExplorer::Hide()
{
	interface_->SetActive(false);
}
