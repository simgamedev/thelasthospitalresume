#pragma once
#include "GUIElement.h"


class GUILabel : public GUIElement
{
public:
	GUILabel(const string& name, GUIInterface* owner);
	~GUILabel();

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mousePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();
	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);

	// ---> TEMP
	void SetTextSize(uint32 textSize);
	virtual void SetText(const string& text);
	// ---< TEMP
};
