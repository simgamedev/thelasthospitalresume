#pragma once
#include "GUIElement.h"

class GUITextfield : public GUIElement
{
public:
	GUITextfield(const string& name, GUIInterface* owner);
	~GUITextfield();

	void HandleCharacterEntered(const char& character);
	void HandleArrowKey(const string& eventName);

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mousePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();
	void OnDefocus();
	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);

private:
	/// @todo comeup with better names
	void MoveCursor(int32 pos);
	real32 GetCursorPixelPosX();
	sf::Vertex cursor_[2];
	int32 cursorPosition_;
	size_t cursorSize_;

	real32 elapsedTime_;
	bool showCursor_;
};
