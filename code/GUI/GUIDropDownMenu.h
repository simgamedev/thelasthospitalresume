#pragma once
#include "GUIElement.h"
#include "GUIVerticalDropDown.h"

/**
   A  DropDownMenu Button.
 */
class GUIDropDownMenu : public GUIElement
{
public:
	GUIDropDownMenu(const string& name, GUIInterface* owner);
	~GUIDropDownMenu();

	GUIVerticalDropDown* GetDropDown();

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mousePos);
	void OnInterfaceClick(const vec2f& mousePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();
	void OnFocus();
	void OnDefocus();
	void Setup();
	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);
private:
	GUIVerticalDropDown dropDown_;
	string interfaceFileName_;
};
