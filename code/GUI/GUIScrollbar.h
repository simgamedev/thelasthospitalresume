#pragma once
#include "GUIElement.h"


/**
   A simple scrollbar which can only be attached to an GUIInterface.
 */
class GUIScrollbar : public GUIElement
{
public:
	GUIScrollbar(const string& name, GUIInterface* owner);
	~GUIScrollbar();

	void SetPosition(const vec2f& pos);

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mosuePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();
	void SetDirection(SliderDirection direction);
	// ---> TEMP
	void Reset();
	// ---< TEMP

	void ApplyStyle();
	void UpdateStyle(const GUIElementState& state, const GUIStyle& style);

	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);
private:
	SliderDirection direction_;
	/// @todo slider should also be a sprite
	/// @todo slider should responds to mouse wheel too.
	sf::RectangleShape sliderShape_;
	vec2f mouseLastPos_;
	int32 percentage_;
};
