#include "GUILabel.h"
#include "../Utilities.h"


/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
GUILabel::GUILabel(const string& name, GUIInterface* owner)
	: GUIElement(name, GUIElementType::Label, owner)
{
}


GUILabel::~GUILabel() {}

void
GUILabel::ReadIn(std::stringstream& stream)
{
	string content;
	Utils::ReadQuotedString(stream, content);
	/// @todo should be widesstring to support Chinese
	//drawable_.text.setString(content);
	SetText(content);
}

/*********************************************************************************
 * Set Text
 *********************************************************************************/
void
GUILabel::SetText(const string& text)
{
	drawable_.text.setString(text);
	sf::Rect textRect = drawable_.text.getGlobalBounds();
	for (auto& itr : styles_)
	{
		GUIStyle& style = itr.second;
		style.size = vec2f(textRect.width, textRect.height);
	}
	SetRedraw(true);
}


void
GUILabel::OnClick(const vec2f& mousePos)
{
	std::cout << "On Click" << std::endl;
	SetState(GUIElementState::Clicked);
}


void
GUILabel::OnRelease()
{
	std::cout << "On Release" << std::endl;
	SetState(GUIElementState::Neutral);
}

void
GUILabel::OnHover(const vec2f& mousePos)
{
	std::cout << "On Hover" << std::endl;
	SetState(GUIElementState::Focused);
}

void
GUILabel::OnLeave()
{
	std::cout << "On Leave" << std::endl;
	SetState(GUIElementState::Neutral);
}

void
GUILabel::Update(real32 dt)
{
}

void
GUILabel::Draw(sf::RenderTarget* renderTarget)
{
	// >>>>> Set text position with origin at top-left corner.
	sf::Rect textRect = drawable_.text.getLocalBounds();
	drawable_.text.setOrigin(textRect.left, textRect.top);
	drawable_.text.setPosition(position_);
	// <<<<<
	/// @todo should be able to set no bg for labels.
	// draw bg image
	if (!styles_[state_].bgImageName.empty())
	{
		renderTarget->draw(drawable_.bgImage);
	}
	else
	{
		// draw bg solid
		renderTarget->draw(drawable_.bgSolid);
	}
	// draw glyph
	if(!styles_[state_].glyphImageName.empty())
	{
		renderTarget->draw(drawable_.glyphImage);
	}
	// draw text
	renderTarget->draw(drawable_.text);
}


/*********************************************************************************
 * Set Text Size
 *********************************************************************************/
void
GUILabel::SetTextSize(uint32 textSize)
{
	/*
	LOG_ENGINE_ERROR("SetTextSize");
	for (auto& itr : styles_)
	{
		GUIStyle& style = itr.second;
		style.textSize = textSize;
	}
	*/
}

