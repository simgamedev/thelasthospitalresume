#include "GUIManager.h"
#include "GUILabel.h"
#include "GUISprite.h"
#include "GUITextfield.h"
#include "GUIScrollbar.h"
#include "GUIScrollbarNew.h"
#include "GUIButton.h"
#include "GUIToggleButton.h"
#include "GUIDropDownMenu.h"
#include "../SharedContext.h"
#include "../Window.h"


/********************************************************************************
 * Register GUIElements and Add callbacks to key bindings.
 ********************************************************************************/
GUIManager::GUIManager(SharedContext* sharedContext)
	: sharedContext_(sharedContext)
{
	RegisterElement<GUILabel>(GUIElementType::Label);
	RegisterElement<GUIScrollbar>(GUIElementType::Scrollbar);
	RegisterElement<GUIScrollbarNew>(GUIElementType::ScrollbarNew);
	RegisterElement<GUITextfield>(GUIElementType::Textfield);
	RegisterElement<GUISprite>(GUIElementType::Sprite);
	RegisterElement<GUIButton>(GUIElementType::Button);
	RegisterElement<GUIDropDownMenu>(GUIElementType::DropDownMenu);
	RegisterElement<GUIToggleButton>(GUIElementType::ToggleButton);
	elementTypes_.emplace("Label", GUIElementType::Label);
	elementTypes_.emplace("Scrollbar", GUIElementType::Scrollbar);
	elementTypes_.emplace("ScrollbarNew", GUIElementType::ScrollbarNew);
	elementTypes_.emplace("Textfield", GUIElementType::Textfield);
	elementTypes_.emplace("Sprite", GUIElementType::Sprite);
	elementTypes_.emplace("Button", GUIElementType::Button);
	elementTypes_.emplace("DropDownMenu", GUIElementType::DropDownMenu);
	elementTypes_.emplace("ToggleButton", GUIElementType::ToggleButton);

	EventManager* eventManager = sharedContext->eventManager;
	eventManager->AddCallback(GameStateType(0), "Mouse_Left", &GUIManager::HandleClick, this);
	eventManager->AddCallback(GameStateType(0), "Mouse_Left_Release", &GUIManager::HandleRelease, this);
	eventManager->AddCallback(GameStateType(0), "Mouse_Right", &GUIManager::HandleClick, this);
	eventManager->AddCallback(GameStateType(0), "Mouse_Right_Release", &GUIManager::HandleRelease, this);
	eventManager->AddCallback(GameStateType(0), "Mouse_Moved", &GUIManager::HandleMouseMoved, this);
	eventManager->AddCallback(GameStateType(0), "Text_Entered", &GUIManager::HandleTextEntered, this);
	eventManager->AddCallback(GameStateType(0), "Key_LeftArrow", &GUIManager::HandleArrowKeys, this);
	eventManager->AddCallback(GameStateType(0), "Key_RightArrow", &GUIManager::HandleArrowKeys, this);
}

GUIManager::~GUIManager()
{
	EventManager* eventManager = sharedContext_->eventManager;
	eventManager->RemoveCallback(GameStateType(0), "Mouse_Left");
	eventManager->RemoveCallback(GameStateType(0), "Mouse_Left_Release");
	eventManager->RemoveCallback(GameStateType(0), "Mouse_Right");
	eventManager->RemoveCallback(GameStateType(0), "Mouse_Right_Release");
	eventManager->RemoveCallback(GameStateType(0), "Key_LeftArrow");
	eventManager->RemoveCallback(GameStateType(0), "Key_RightArrow");
}


/********************************************************************************
 * Get Interface.
 ********************************************************************************/
GUIInterface*
GUIManager::GetInterface(const GameStateType& gameState, const string& name)
{
	auto interfaceContainersItr = interfaces_.find(gameState);
	if(interfaceContainersItr == interfaces_.end()) { return nullptr; }
	auto& interfacesOfState = interfaceContainersItr->second;
	auto interfacesItr = std::find_if(interfacesOfState.begin(), interfacesOfState.end(),
							 [&name](GUIInterfaceEntry& entry)
							 {
								 string interfaceName = entry.first;
								 return interfaceName == name;
							 });
	if(interfacesItr == interfacesOfState.end())
	{
		return nullptr;
	}
	else
	{
		GUIInterface* guiInterface = interfacesItr->second.get();
		return guiInterface;
	}
	return nullptr;
}

GUIInterface*
GUIManager::GetInterface(const string& name)
{
	return GetInterface(currentGameState_, name);
}

bool
GUIManager::RemoveInterface(const GameStateType& gameState, const string& name)
{
	auto interfaceContainersItr = interfaces_.find(gameState);
	if(interfaceContainersItr == interfaces_.end()) { return false; }
	auto& interfacesOfState = interfaceContainersItr->second;
	auto interfacesItr = std::remove_if(interfacesOfState.begin(), interfacesOfState.end(),
										[&name](GUIInterfaceEntry& entry)
										{
											string interfaceName = entry.first;
											return interfaceName == name;
										});
	if(interfacesItr == interfacesOfState.end())
	{
		return false;
	}
	interfacesOfState.erase(interfacesItr);
	return true;
}

bool
GUIManager::RemoveInterface(const string& name)
{
	return RemoveInterface(currentGameState_, name);
}

bool
GUIManager::BringToFront(const GUIInterface* guiInterface)
{
	return BringToFront(currentGameState_, guiInterface->GetName());
}

bool
GUIManager::BringToFront(const GameStateType& gameState, const string& name)
{
	auto interfaceContainersItr = interfaces_.find(gameState);
	if(interfaceContainersItr == interfaces_.end()) { return false; }
	auto& interfacesOfState = interfaceContainersItr->second;
	auto interfacesItr = std::find_if(interfacesOfState.begin(), interfacesOfState.end(),
									  [&name](GUIInterfaceEntry& entry)
									  {
										  string interfaceName = entry.first;
										  return interfaceName == name;
									  });
	if(interfacesItr == interfacesOfState.end()) { return false; }
	GUIInterface* guiInterface = interfacesItr->second.get();
	toBringToFront_.emplace_back(gameState, guiInterface);
	return true;
}
/*********************************************************************************
 * Handle Mouse Moved.
 *********************************************************************************/
void
GUIManager::HandleMouseMoved(EventDetails* eventDetails)
{
	/*
	std::cout << "Mouse Pos: "
			  << eventDetails->mousePos.x
			  << ","
			  << eventDetails->mousePos.y
			  << std::endl;
			  */
}


/*********************************************************************************
 * Handle Bring To Front.
 *********************************************************************************/
void
GUIManager::HandleBringToFront()
{
	for(auto& itr : toBringToFront_)
	{
		auto interfaceContainersItr = interfaces_.find(currentGameState_);
		if(interfaceContainersItr == interfaces_.end()) { continue; }
		auto& interfacesOfState = interfaceContainersItr->second;
		auto interfacesItr = std::find_if(interfacesOfState.begin(), interfacesOfState.end(),
										  [&itr](const GUIInterfaceEntry& entry)
										  {
											  return itr.second == entry.second.get();
										  });
		if(interfacesItr == interfacesOfState.end()) { continue; }
		string interfaceName = interfacesItr->first;
		auto interfacePtr = std::move(interfacesItr->second);
		interfacesOfState.erase(interfacesItr);
		/// @todo make clear that the top interface is at the back end of it's container.
		interfacesOfState.emplace_back(interfaceName, std::move(interfacePtr));
	}
}
		

void
GUIManager::ChangeGameState(const GameStateType& gameState)
{
	if(currentGameState_ == gameState) { return; }
	HandleRelease(nullptr);
	SetState(gameState);
}

void
GUIManager::RemoveGameState(const GameStateType& gameState)
{
	guiEvents_.erase(gameState);
	interfaces_.erase(gameState);
}

SharedContext*
GUIManager::GetContext() const
{
	return sharedContext_;
}

void
GUIManager::DefocusAllInterfaces()
{
	auto interfaceContainersItr = interfaces_.find(currentGameState_);
	if(interfaceContainersItr == interfaces_.end())
	{
		return;
	}
	auto& interfacesOfState = interfaceContainersItr->second;
	for(auto& interfaceEntry : interfacesOfState)
	{
		GUIInterface* guiInterface = interfaceEntry.second.get();
		// ---> TEMP
		if (guiInterface->HasSubInterface())
		{
			guiInterface->GetSubInterface()->Defocus();
		}
		// ---< TEMP
		guiInterface->Defocus();
	}
}


/********************************************************************************
 * Handle Mouse Click.
 ********************************************************************************/
void
GUIManager::HandleClick(EventDetails* eventDetails)
{
	// ---> LOG 
	/*
	std::cout << "Mouse Click: "
			  << eventDetails->mousePos.x
			  << ","
			  << eventDetails->mousePos.y
			  << std::endl;
    */
	// ---<
	auto interfaceContainersItr = interfaces_.find(currentGameState_);
	if(interfaceContainersItr == interfaces_.end()) { return; }
	/// @todo fixme mousepos inconsistent(eventDetails/eventManager->GetMousePos)
	//vec2i mousePos = sharedContext_->eventManager->GetMousePos();
	vec2i mousePos = eventDetails->mousePos;
	auto& interfacesOfState = interfaceContainersItr->second;
	for(auto interfacesItr = interfacesOfState.rbegin();
		interfacesItr != interfacesOfState.rend();
		++interfacesItr)
	{
		GUIInterface* guiInterface = interfacesItr->second.get();
		if(!guiInterface->IsInside(vec2f(mousePos))) { continue; }
		if(!guiInterface->IsActive()) { continue; }
		if(eventDetails->keycode == static_cast<int32>(sf::Mouse::Left))
		{
			DefocusAllInterfaces();
			guiInterface->Focus();
			guiInterface->OnClick(vec2f(mousePos));
			// ---< TEMP
			if(guiInterface->IsBeingMoved())
			{
				guiInterface->BeginMoving();
			}
		}
		eventDetails->hasBeenProcessed = true;
		return;
	}
	interfacesOfState.back().second->Defocus();
}



/********************************************************************************
 * Hanles mouse button release.
 ********************************************************************************/
void
GUIManager::HandleRelease(EventDetails* eventDetails)
{
	/// @todo so what about right mouse button?
	if(eventDetails &&
	   eventDetails->keycode != static_cast<int32>(sf::Mouse::Left))
	{
		return;
	}
	auto interfaceContainersItr = interfaces_.find(currentGameState_);
	if(interfaceContainersItr == interfaces_.end()) { return; }
	auto& interfacesOfState = interfaceContainersItr->second;
	for(auto& itr : interfacesOfState)
	{
		GUIInterface* guiInterface = itr.second.get();
		if(!guiInterface->IsActive()) { continue; }
		if(guiInterface->GetState() == GUIElementState::Clicked)
		{
			guiInterface->OnRelease();
		}
		if(guiInterface->IsBeingMoved())
		{
			guiInterface->StopMoving();
		}
	}
}


/********************************************************************************
 * Handle Text Entered.
 ********************************************************************************/
void
GUIManager::HandleTextEntered(EventDetails* eventDetails)
{
	// TODO: make should when no active interface exists, this should be doing anything
	//std::cout << "GUIManager::HandleTextEntered" << std::endl;
	auto interfaceContainersItr = interfaces_.find(currentGameState_);
	if(interfaceContainersItr == interfaces_.end()) { return; }
	auto& interfacesOfState = interfaceContainersItr->second;
	for(auto& itr : interfacesOfState)
	{
		GUIInterface* guiInterface = itr.second.get();
		if(!guiInterface->IsActive()) { continue; }
		if(!guiInterface->IsFocused()) { continue; }
		guiInterface->OnTextEntered(eventDetails->characterEntered);
		return;
	}
}


void
GUIManager::HandleArrowKeys(EventDetails* eventDetails)
{
	auto interfaceContainersItr = interfaces_.find(currentGameState_);
	if(interfaceContainersItr == interfaces_.end()) { return; }
	auto& interfacesOfState = interfaceContainersItr->second;
	for(auto& itr : interfacesOfState)
	{
		GUIInterface* guiInterface = itr.second.get();
		if(!guiInterface->IsActive()) { continue; }
		if(!guiInterface->IsFocused()) { continue; }
		guiInterface->OnArrowKey(eventDetails->bindingName);
		return;
	}
}

void
GUIManager::AddEvent(GUIEvent guiEvent)
{
	guiEvents_[currentGameState_].push_back(guiEvent);
}

bool
GUIManager::PollEvent(GUIEvent& guiEvent)
{
	if(guiEvents_[currentGameState_].empty()) { return false; }
	guiEvent = guiEvents_[currentGameState_].back();
	guiEvents_[currentGameState_].pop_back();
	return true;
}


/********************************************************************************
 * Update.
 ********************************************************************************/
void
GUIManager::Update(real32 dt)
{
	vec2i mousePos = sharedContext_->eventManager->GetMousePos();
	HandleBringToFront();
	auto interfaceContainersItr = interfaces_.find(currentGameState_);
	if(interfaceContainersItr == interfaces_.end()) { return; }
	auto& interfacesOfState = interfaceContainersItr->second;
	bool mouseHoverCaptured = false;
	for(auto itr = interfacesOfState.rbegin();
		itr != interfacesOfState.rend();
		++itr)
	{
		GUIInterface* guiInterface = itr->second.get();
		if(!guiInterface->IsActive()) { continue; }
		guiInterface->Update(dt);
		if(guiInterface->IsBeingMoved()) { continue; }
		if(guiInterface->IsInside(vec2f(mousePos)) && !mouseHoverCaptured)
		{
			if(guiInterface->GetState() == GUIElementState::Neutral)
			{
				guiInterface->OnHover(vec2f(mousePos));
			}
			mouseHoverCaptured = true;
			continue;
		}
		else if(guiInterface->GetState() == GUIElementState::Focused)
		{
			guiInterface->OnLeave();
		}
	}
}


void
GUIManager::Draw(sf::RenderWindow* renderWindow)
{
	auto interfaceContainersItr = interfaces_.find(currentGameState_);
	if(interfaceContainersItr == interfaces_.end()) { return; }
	auto& interfacesOfState = interfaceContainersItr->second;
	sf::View currentView = renderWindow->getView();
	/// @todo NOTE: since GUI doens't need sf::view moved
	renderWindow->setView(renderWindow->getDefaultView());
	for(auto& itr : interfacesOfState)
	{
		GUIInterface* guiInterface = itr.second.get();
		// ---> TEMP
		// ---< TEMP
		if(!guiInterface->IsActive()) { continue; }
		/// @todo is guiinterface's needs redraws just needs bg layer redraw?
		if (guiInterface->GetName() == "TestInterface")
		{
			int32 a = 100;
		}
		if(guiInterface->NeedsRedraw())
		{
			guiInterface->RedrawBgLayer();
		}
		if(guiInterface->NeedsContentLayerRedraw())
		{
			guiInterface->RedrawContentLayer();
		}
		if(guiInterface->NeedsControlLayerRedraw())
		{
			guiInterface->RedrawControlLayer();
		}
		guiInterface->Draw(renderWindow);
	}
}


GUIElement*
GUIManager::CreateElement(const GUIElementType& elementType, GUIInterface* owner)
{
	if(elementType == GUIElementType::Interface)
	{
		return new GUIInterface("", this, owner);
	}
	auto factoriesItr = factories_.find(elementType);
	if(factoriesItr ==  factories_.end())
	{
		return nullptr;
	}
	auto factory = factoriesItr->second;
	GUIElement* newElement = factory(owner);
	return newElement;
}

bool
GUIManager::AddInterface(const GameStateType& gameState, const string& name)
{
	/// @todo does this mean that you'll never get an end() itr ?
	auto interfaceContainersItr = interfaces_.emplace(gameState, GUIInterfaceContainer()).first;
	auto& interfacesOfState = interfaceContainersItr->second;
	auto interfacesItr = std::find_if(interfacesOfState.begin(), interfacesOfState.end(),
									  [&name](GUIInterfaceEntry& entry)
									  {
										  string interfaceName = entry.first;
										  return interfaceName == name;
									  });
	if(interfacesItr != interfacesOfState.end())
	{
		std::cout << "Error: Interface with the name " << name << " alreday exist!" << std::endl;
		return false;
	}

	std::unique_ptr<GUIInterface> temp = std::make_unique<GUIInterface>(name, this);
	interfacesOfState.emplace_back(name, std::move(temp));
	return true;
}

bool
GUIManager::AddInterface(const string& name)
{
	return AddInterface(currentGameState_, name);
}
/********************************************************************************
 * Produce Interface.
 ********************************************************************************/
GUIInterface* 
GUIManager::ProduceInterface(const string& name,
							 const vec2f& size,
							 bool showTitleBar,
							 bool movable)
{
	/// @todo where do you delete.
	GUIInterface* guiInterface = new GUIInterface(name, this);
	/*
	if(!AddInterface(currentGameState_, name))
	{
		std::cout << "Failed to add interfaceDefault: " << name << std::endl;
		return nullptr;
	}
	GUIInterface* guiInterface = GetInterface(currentGameState_, name);
	*/
	LoadStyleNew("DefaultInterface2.style", guiInterface, size);
	guiInterface->SetMovable(movable);
	guiInterface->SetShowTitleBar(showTitleBar);
	/// @todo find a proper place to setcontentsize()
	guiInterface->SetContentSize(guiInterface->GetSize());
	guiInterface->Setup();
	guiInterface->SetPosition(vec2f(0, 0));
	return guiInterface;
}
/********************************************************************************
 * Add SubInterface Default.
 ********************************************************************************/
void
GUIManager::AddSubInterfaceDefault(GUIInterface* parent, GUIInterface* child)
{
	child->parent_ = parent;
	parent->subInterface_ = child;
	child->owner_ = parent;
	parent->AddElement(child);
	/*
	std::unique_ptr<GUIInterface> smartPtr(child);
	interfaces_.at(currentGameState_).emplace_back(child->name_, std::move(smartPtr));
	*/
}
/********************************************************************************
 * Load Interface Which Returns a Pointer to the interface.
 ********************************************************************************/

/********************************************************************************
 * Load Interface.
 ********************************************************************************/
bool
GUIManager::LoadInterface(const GameStateType& gameState, const string& interfaceFilename, const string& name)
{
	std::ifstream interfaceFile;
	interfaceFile.open(Utils::GetMediaDirectory() + "GUI/" + interfaceFilename);

	if(!interfaceFile.is_open())
	{
		std::cout << "Faild to load interface file: " << interfaceFilename << std::endl;
		return false;
	}
	string line;
	/// @todo get rid of this 
	string interfaceName;
	while(std::getline(interfaceFile, line))
	{
		if(line[0] == '|') { continue; }
		std::stringstream keystream(line);
		string key;
		keystream >> key;
		/// @todo change other "types" to keys
		if(key == "Interface")
		{
			keystream >> interfaceName;
			string styleFileName;
			keystream >> styleFileName;
			if(!AddInterface(gameState, name))
			{
				std::cout << "Failed to add interface: " << name << std::endl;
				return false;
			}
			GUIInterface* guiInterface = GetInterface(gameState, name);
			//guiInterface->SetFileName();
			keystream >> *guiInterface;
			if(!LoadStyle(styleFileName, guiInterface))
			{
				std::cout << "Failed to load style file: " << styleFileName << std::endl;
			}
			/// @todo find a better place to set content size
			guiInterface->SetContentSize(guiInterface->GetSize());
			guiInterface->Setup();
		}
		else if(key == "Element")
		{
			if(interfaceName.empty())
			{
				std::cout << "Error: 'Element' outside or before declaration of 'Interface'" << std::endl;
				continue;
			}
			/// @todo we should move "size" outof GUIStyle, read it in directly from interface file.
			string elementTypeString;
			string elementName;
			vec2f elementPos;
			string elementStyleFileName;
			string layer;
			keystream >> elementTypeString >> elementName >> elementPos.x >> elementPos.y >> elementStyleFileName >> layer;
			GUIElementType elementType = StringToType(elementTypeString);
			if(elementType == GUIElementType::None)
			{
				std::cout << "Unknown element('" << elementName << "') type: '" << elementTypeString << std::endl;
				continue;
			}

			GUIInterface* guiInterface = GetInterface(gameState, name);
			if(!guiInterface) { continue; }
			if(!guiInterface->AddElement(elementType, elementName)) { continue; }
			GUIElement* guiElement = guiInterface->GetElement(elementName);
			keystream >> *guiElement;
			guiElement->SetPosition(elementPos);
			guiElement->SetIsControl((layer == "Control"));
			if(!LoadStyle(elementStyleFileName, guiElement))
			{
				std::cout << "Failed to load style file: " << elementStyleFileName << std::endl;
			}
			guiElement->Setup();
		}
	}
	interfaceFile.close();
	return true;
}
/********************************************************************************
 * Add Interface Programatically.
 ********************************************************************************/
bool
GUIManager::AddInterfaceDefault(const GameStateType& gameState,
						  	    const string& name,
								const string& title,
						        int32 x, int32 y, int32 w, int32 h,
								bool showTitleBar,
								bool movable)
{
	if(!AddInterface(gameState, name))
	{
		std::cout << "Failed to add interfaceDefault: " << name << std::endl;
		return false;
	}
	GUIInterface* guiInterface = GetInterface(gameState, name);
	guiInterface->SetTitle(title);
	guiInterface->SetMovable(movable);
	guiInterface->SetShowTitleBar(showTitleBar);
	//guiInterface->SetElementsPadding(vec2f(10.f, 10.f));
	vec2f interfaceSize = vec2f(w, h);
	if(!LoadStyleNew("DefaultInterface.style", guiInterface, interfaceSize))
	{
		std::cout << "Failed to load default interface style file!" << std::endl;
	}
	/// @todo find a proper place to setcontentsize()
	guiInterface->SetContentSize(guiInterface->GetSize());
	guiInterface->Setup();
	guiInterface->SetPosition(vec2f(x, y));
}
/********************************************************************************
 * Add Label Programatically.
 ********************************************************************************/
GUILabel* 
GUIManager::AddLabelDefault(GUIInterface* guiInterface,
						  const string& name,
						  const vec2f& pos,
						  const vec2f& size,
						  const string& text)
{
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::Label, name)) { return nullptr; }
	GUIElement* guiElement = guiInterface->GetElement(name);
	//keystream >> *guiElement;
	guiElement->SetText(text);
	guiElement->SetPosition(pos);
	guiElement->SetIsControl(false);
	if(!LoadStyleNew("DefaultLabel.style", guiElement, size))
	{
		std::cout << "Failed to load style file: " << "default button" << std::endl;
	}
	guiElement->Setup();
	return (GUILabel*)guiElement;
}


/********************************************************************************
 * Add Element Programatically.
 ********************************************************************************/
GUIButton*
GUIManager::AddButton(GUIInterface* guiInterface,
					  const string& name,
					  const vec2f& pos,
					  const vec2f& size,
					  const string& text,
					  const string& glyphImageName,
					  const string& styleFileName,
					  const string& layer)
{
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::Button, name)) { return nullptr; }
	GUIButton* guiButton = (GUIButton*)guiInterface->GetElement(name);
	guiButton->SetText(text);
	guiButton->SetPosition(pos);
	guiButton->SetIsControl((layer == "Control"));
	if(!LoadStyleNew(styleFileName, guiButton, size))
	{
		std::cout << "Failed to load style file: " << "default button" << std::endl;
	}
	guiButton->SetGlyphImageName(glyphImageName);
	guiButton->Setup();
	//return (GUIButton*)guiElement;
	return guiButton;
}
GUIButton*
GUIManager::AddButtonDefault(GUIInterface* guiInterface,
							 const string& name,
							 const vec2f& pos,
							 const vec2f& size,
							 const string& text,
							 const string& layer)
{
	//GUIInterface* guiInterface = GetInterface(gameState, name);
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::Button, name)) { return nullptr; }
	GUIElement* guiElement = guiInterface->GetElement(name);
	//keystream >> *guiElement;
	guiElement->SetText(text);
	guiElement->SetPosition(pos);
	guiElement->SetIsControl((layer == "Control"));
	if(!LoadStyleNew("DefaultButton.style", guiElement, size))
	{
		std::cout << "Failed to load style file: " << "default button" << std::endl;
	}
	guiElement->Setup();
	return (GUIButton*)guiElement;
}
/*********************************************************************************
 * Add Toggle Button Default.
 *********************************************************************************/
GUIToggleButton* 
GUIManager::AddToggleButtonDefault(GUIInterface* guiInterface,
								   const string& name,
								   const vec2f& pos,
								   const vec2f& size,
								   const string& text,
								   const string& layer)
{
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::ToggleButton, name)) { return nullptr; }
	GUIElement* guiElement = guiInterface->GetElement(name);
	guiElement->SetText(text);
	guiElement->SetPosition(pos);
	guiElement->SetIsControl((layer == "Control"));
	if(!LoadStyleNew("DefaultToggleButton.style", guiElement, size))
	{
		std::cout << "Failed to load style file: " << "default button" << std::endl;
	}
	guiElement->Setup();
	return (GUIToggleButton*)guiElement;
}

GUIButton*
GUIManager::AddButtonDefault(GUIInterface* guiInterface,
							 const string& name,
							 const vec2f& pos,
							 const vec2f& size,
							 const string& text,
							 const string& glyphImageName,
							 const string& layer)
{
	//GUIInterface* guiInterface = GetInterface(gameState, name);
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::Button, name)) { return nullptr; }
	GUIButton* guiButton = (GUIButton*)guiInterface->GetElement(name);
	guiButton->SetText(text);
	guiButton->SetPosition(pos);
	guiButton->SetIsControl((layer == "Control"));
	if (name == "Wall_00")
	{
		int32 a = 100;
	}
	if(!LoadStyleNew("DefaultButton.style", guiButton, size))
	{
		std::cout << "Failed to load style file: " << "default button" << std::endl;
	}
	guiButton->SetGlyphImageName(glyphImageName);
	guiButton->Setup();
	//return (GUIButton*)guiElement;
	return guiButton;
}
/*********************************************************************************
 * Add CheckBox Default.
 *********************************************************************************/
GUIToggleButton*
GUIManager::AddCheckBoxDefault(GUIInterface* guiInterface,
							   const string& name,
							   const vec2f& pos,
							   const vec2f& size)
{
	//GUIInterface* guiInterface = GetInterface(gameState, name);
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::ToggleButton, name)) { return nullptr; }
	GUIElement* guiElement = guiInterface->GetElement(name);
	//keystream >> *guiElement;
	guiElement->SetPosition(pos);
	if(!LoadStyleNew("DefaultToggleButton.style", guiElement, size))
	{
		std::cout << "Failed to load style file: " << "default button" << std::endl;
	}
	guiElement->Setup();
	return (GUIToggleButton*)guiElement;
}
/********************************************************************************
 * Add GUIDropDownMenu.
 ********************************************************************************/
GUIDropDownMenu*
GUIManager::AddDropDownMenuDefault(GUIInterface* guiInterface,
								   const string& name,
								   const vec2f& pos,
								   const vec2f& size,
								   const string& text)
{
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::DropDownMenu, name)) { return nullptr; }
	GUIDropDownMenu* guiDropDownMenu = (GUIDropDownMenu*)guiInterface->GetElement(name);
	if(!LoadStyleNew("DefaultDropDownMenu.style", guiDropDownMenu, size))
	{
		std::cout << "Failed to load DefaultDropDownMenu.style" << std::endl;
	}
	guiDropDownMenu->SetPosition(pos);
	return guiDropDownMenu;
}



						  
/********************************************************************************
 * Add Sprite Default.
 ********************************************************************************/
GUISprite*
GUIManager::AddSpriteDefault(GUIInterface* guiInterface,
							 const string& name,
							 const vec2f& pos,
							 const vec2f& size,
							 const string& textureName)
{
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::Sprite, name)) { return nullptr; }
	GUISprite* guiSprite = (GUISprite*)guiInterface->GetElement(name);
	//guiSprite->SetTexture(textureName);
	if(!LoadStyleNew("DefaultSprite.style", guiSprite, size))
	{
		std::cout << "Failed to load DefaultSprite.style" << std::endl;
	}
	return guiSprite;
}
/********************************************************************************
 * Add ScrollBar Default.
 ********************************************************************************/
GUIScrollbar*
GUIManager::AddScrollBarDefault(GUIInterface* guiInterface,
								const string& name,
								const vec2f& pos,
								const vec2f& size,
								SliderDirection direction)
{
	if(!guiInterface) { return nullptr; }
	if(!guiInterface->AddElement(GUIElementType::Scrollbar, name)) { return nullptr; }
	GUIScrollbar* scrollbar = (GUIScrollbar*)guiInterface->GetElement(name);
	scrollbar->SetOwner(guiInterface);
	scrollbar->SetIsControl(true);
	scrollbar->SetDirection(direction);
	if(!LoadStyleNew("DefaultScrollBar.style", (GUIElement*)scrollbar, size))
	{
		std::cout << "Failed to load DefaultScrollBar.style" << std::endl;
	}
	return scrollbar;
}

						  
/********************************************************************************
 * Load Interface New.
 ********************************************************************************/
bool
GUIManager::LoadInterfaceNew(const GameStateType& gameState,
							 const string& interfaceFilename,
							 const string& name)
{
	std::ifstream interfaceFile;
	interfaceFile.open(Utils::GetMediaDirectory() + "GUI/" + interfaceFilename);

	if(!interfaceFile.is_open())
	{
		std::cout << "Faild to load interface file: " << interfaceFilename << std::endl;
		return false;
	}
	string line;
	/// @todo get rid of this 
	string interfaceName;
	while(std::getline(interfaceFile, line))
	{
		if(line[0] == '|') { continue; }
		std::stringstream keystream(line);
		string key;
		keystream >> key;
		/// @todo change other "types" to keys
		if(key == "Interface")
		{
			keystream >> interfaceName;
			vec2f interfaceSize;
			keystream >> interfaceSize.x >> interfaceSize.y;
			string styleFileName;
			keystream >> styleFileName;
			if(!AddInterface(gameState, name))
			{
				std::cout << "Failed to add interface: " << name << std::endl;
				return false;
			}
			GUIInterface* guiInterface = GetInterface(gameState, name);
			//guiInterface->SetFileName();
			keystream >> *guiInterface;
			if(!LoadStyleNew(styleFileName, guiInterface, interfaceSize))
			{
				std::cout << "Failed to load style file: " << styleFileName << std::endl;
			}
			/// @todo find a better place to set content size
			guiInterface->SetContentSize(guiInterface->GetSize());
			guiInterface->Setup();
		}
		else if(key == "Element")
		{
			if(interfaceName.empty())
			{
				std::cout << "Error: 'Element' outside or before declaration of 'Interface'" << std::endl;
				continue;
			}
			/// @todo we should move "size" outof GUIStyle, read it in directly from interface file.
			string elementTypeString;
			string elementName;
			vec2f elementPos;
			vec2f elementSize;
			string elementStyleFileName;
			string layer;
			keystream >> elementTypeString >> elementName >> elementPos.x >> elementPos.y
					  >> elementSize.x >> elementSize.y
					  >> elementStyleFileName >> layer;
			GUIElementType elementType = StringToType(elementTypeString);
			if(elementType == GUIElementType::None)
			{
				std::cout << "Unknown element('" << elementName << "') type: '" << elementTypeString << std::endl;
				continue;
			}

			GUIInterface* guiInterface = GetInterface(gameState, name);
			if(!guiInterface) { continue; }
			if(!guiInterface->AddElement(elementType, elementName)) { continue; }
			GUIElement* guiElement = guiInterface->GetElement(elementName);
			if (guiElement->GetName() == "TileSetDropDown")
			{
				int32 a = 100;
			}
			keystream >> *guiElement;
			guiElement->SetPosition(elementPos);
			guiElement->SetIsControl((layer == "Control"));
			if(!LoadStyleNew(elementStyleFileName, guiElement, elementSize))
			{
				std::cout << "Failed to load style file: " << elementStyleFileName << std::endl;
			}
			guiElement->Setup();
		}
	}
	interfaceFile.close();
	return true;
}

bool
GUIManager::LoadInterfaceNew(const string& interfaceFilename, const string& name)
{
	return LoadInterfaceNew(currentGameState_, interfaceFilename, name);
}

bool
GUIManager::LoadInterface(const string& interfaceFilename, const string& name)
{
	return LoadInterface(currentGameState_, interfaceFilename, name);
}
/********************************************************************************
 * Load Style New.
 ********************************************************************************/
bool
GUIManager::LoadStyleNew(const std::string& styleFileName, GUIElement* guiElement, vec2f elementSize)
{
	std::ifstream styleFile;
	styleFile.open(Utils::GetMediaDirectory() + "GUI/" + styleFileName);


	string currentlyReadingStateString;
	GUIStyle ParentStyle;
	GUIStyle TemporaryStyle;
	TemporaryStyle.size = elementSize;
	// ---> Special Treatment For Label.
	if(guiElement->GetType() == GUIElementType::Label)
	{
		TemporaryStyle.textSize = (uint32)elementSize.x;
		TemporaryStyle.size = vec2f(guiElement->GetTextLocalBounds().width,
									guiElement->GetTextLocalBounds().height);
	}
	// ---<

	// ---> Special Treatment For Button.
	if(guiElement->GetType() == GUIElementType::Button ||
	   guiElement->GetType() == GUIElementType::ToggleButton)
	{
		// it button was 96x32, then text size should be 14, keep this ratio.
		// TODO: so resize it proportionally
		TemporaryStyle.textSize = 32;
	}
	// ---<

	// ---> Temp Special Treatment For GUIDropDownMenu.
	if(guiElement->GetType() == GUIElementType::DropDownMenu)
	{
		real32 textSizeRatio = elementSize.y / 32.0f;
		TemporaryStyle.textSize = 18 * textSizeRatio;
	}
	 

	if(!styleFile.is_open())
	{
		std::cout << "Failed to open: " << styleFileName << std::endl;
		return false;
	}

	ParentStyle.styleFileName = styleFileName;
	TemporaryStyle.styleFileName = styleFileName;
	string line;
    while(std::getline(styleFile, line))
	{
		if(line[0] == '|') { continue; }
		std::stringstream keystream(line);
		string key;
		keystream >> key;
		/// @todo what the is this??
		if(key.empty()) { continue; }
		if(key == "State")
		{
			if(!currentlyReadingStateString.empty())
			{
				std::cout << "Error: 'State' keyword found inside another state!" << std::endl;
				continue;
			}
			keystream >> currentlyReadingStateString;
		}
		else if(key == "/State")
		{
			if(currentlyReadingStateString.empty())
			{
				std::cout << "Erro: '/State' keyword found piror to 'State'!" << std::endl;
				continue;
			}
			GUIElementState currentlyReadingState = GUIElementState::Neutral;
			if(currentlyReadingStateString == "Hover")
			{
				currentlyReadingState = GUIElementState::Focused;
			}
			if(currentlyReadingStateString == "Clicked")
			{
				currentlyReadingState = GUIElementState::Clicked;
			}

			if(currentlyReadingState == GUIElementState::Neutral)
			{
				ParentStyle = TemporaryStyle;
				/// @todo NOTE: becase neutral state is a base for other states.
				guiElement->UpdateStyle(GUIElementState::Neutral, TemporaryStyle);
				guiElement->UpdateStyle(GUIElementState::Focused, TemporaryStyle);
				guiElement->UpdateStyle(GUIElementState::Clicked, TemporaryStyle);
			}
			else
			{
				guiElement->UpdateStyle(currentlyReadingState, TemporaryStyle);
			}
			TemporaryStyle = ParentStyle;
			currentlyReadingStateString.clear();
		}
		else
		{
			if(currentlyReadingStateString.empty())
			{
				std::cout << "Error: '" << key << "' keyword found outside of a state keyword." << std::endl;
				continue;
			}
			/*
			if(key == "Size")
			{
				/// @todo size should be read from interface file, not sytle file.
				keystream >> TemporaryStyle.size.x >> TemporaryStyle.size.y;
			}
			*/
			if(key == "BgColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.bgColor = sf::Color(r, g, b, a);
			}
			else if(key == "BgImageName")
			{
				keystream >> TemporaryStyle.bgImageName;
			}
			else if(key == "BgImageColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.bgImageColor = sf::Color(r, g, b, a);
			}
			else if(key == "TextColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.textColor = sf::Color(r, g, b, a);
			}
			else if(key == "TextSize")
			{
				keystream >> TemporaryStyle.textSize;
			}
			else if(key == "TextOriginCenter")
			{
				TemporaryStyle.textCenterOrigin = true;
			}
			else if(key == "StretchToFit")
			{
				TemporaryStyle.stretchToFit = true;
			}
			else if(key == "Font")
			{
				keystream >> TemporaryStyle.fontName;
			}
			else if(key == "TextPadding")
			{
				keystream >> TemporaryStyle.textPadding.x >> TemporaryStyle.textPadding.y;
			}
			else if(key == "ElementColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.elementColor = sf::Color(r, g, b, a);
			}
			else if(key == "GlyphImageName")
			{
				keystream >> TemporaryStyle.glyphImageName;
			}
			else if(key == "GlyphImageSize")
			{
				keystream >> TemporaryStyle.glyphImageSize.x >> TemporaryStyle.glyphImageSize.y;
			}
			else if(key == "GlyphImagePadding")
			{
				keystream >> TemporaryStyle.glyphPadding.x >> TemporaryStyle.glyphPadding.y;
			}
			else if(key == "GlyphImageScale")
			{
				keystream >> TemporaryStyle.glyphImageScale.x >> TemporaryStyle.glyphImageScale.y;
			}
			else if (key == "OutlineThickness")
			{
				keystream >> TemporaryStyle.outlineThickness;
			}
			else if (key == "OutlineColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.outlineColor = sf::Color(r, g, b, a);
			}
			else
			{
				std::cout <<"Error: style key '" << key << "' is unknown.!" << std::endl;
			}
		}
	}
	styleFile.close();
	return true;
}

/********************************************************************************
 * Load Style.
 ********************************************************************************/
bool
GUIManager::LoadStyle(const std::string& styleFileName, GUIElement* guiElement)
{
	std::ifstream styleFile;
	styleFile.open(Utils::GetMediaDirectory() + "GUI/" + styleFileName);

	string currentlyReadingStateString;
	GUIStyle ParentStyle;
	GUIStyle TemporaryStyle;
	if(!styleFile.is_open())
	{
		std::cout << "Failed to open: " << styleFileName << std::endl;
		return false;
	}

	ParentStyle.styleFileName = styleFileName;
	TemporaryStyle.styleFileName = styleFileName;
	string line;
    while(std::getline(styleFile, line))
	{
		if(line[0] == '|') { continue; }
		std::stringstream keystream(line);
		string key;
		keystream >> key;
		/// @todo what the is this??
		if(key.empty()) { continue; }
		if(key == "State")
		{
			if(!currentlyReadingStateString.empty())
			{
				std::cout << "Error: 'State' keyword found inside another state!" << std::endl;
				continue;
			}
			keystream >> currentlyReadingStateString;
		}
		else if(key == "/State")
		{
			if(currentlyReadingStateString.empty())
			{
				std::cout << "Erro: '/State' keyword found piror to 'State'!" << std::endl;
				continue;
			}
			GUIElementState currentlyReadingState = GUIElementState::Neutral;
			if(currentlyReadingStateString == "Hover")
			{
				currentlyReadingState = GUIElementState::Focused;
			}
			if(currentlyReadingStateString == "Clicked")
			{
				currentlyReadingState = GUIElementState::Clicked;
			}

			if(currentlyReadingState == GUIElementState::Neutral)
			{
				ParentStyle = TemporaryStyle;
				/// @todo NOTE: becase neutral state is a base for other states.
				guiElement->UpdateStyle(GUIElementState::Neutral, TemporaryStyle);
				guiElement->UpdateStyle(GUIElementState::Focused, TemporaryStyle);
				guiElement->UpdateStyle(GUIElementState::Clicked, TemporaryStyle);
			}
			else
			{
				guiElement->UpdateStyle(currentlyReadingState, TemporaryStyle);
			}
			TemporaryStyle = ParentStyle;
			currentlyReadingStateString.clear();
		}
		else
		{
			if(currentlyReadingStateString.empty())
			{
				std::cout << "Error: '" << key << "' keyword found outside of a state keyword." << std::endl;
				continue;
			}
			if(key == "Size")
			{
				/// @todo size should be read from interface file, not sytle file.
				keystream >> TemporaryStyle.size.x >> TemporaryStyle.size.y;
			}
			else if(key == "BgColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.bgColor = sf::Color(r, g, b, a);
			}
			else if(key == "BgImageName")
			{
				keystream >> TemporaryStyle.bgImageName;
			}
			else if(key == "BgImageColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.bgImageColor = sf::Color(r, g, b, a);
			}
			else if(key == "TextColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.textColor = sf::Color(r, g, b, a);
			}
			else if(key == "TextSize")
			{
				keystream >> TemporaryStyle.textSize;
			}
			else if(key == "TextOriginCenter")
			{
				TemporaryStyle.textCenterOrigin = true;
			}
			else if(key == "StretchToFit")
			{
				TemporaryStyle.stretchToFit = true;
			}
			else if(key == "Font")
			{
				keystream >> TemporaryStyle.fontName;
			}
			else if(key == "TextPadding")
			{
				keystream >> TemporaryStyle.textPadding.x >> TemporaryStyle.textPadding.y;
			}
			else if(key == "ElementColor")
			{
				int32 r, g, b, a = 0;
				keystream >> r >> g >> b >> a;
				TemporaryStyle.elementColor = sf::Color(r, g, b, a);
			}
			else if(key == "GlyphImageName")
			{
				keystream >> TemporaryStyle.glyphImageName;
			}
			else if(key == "GlyphImageSize")
			{
				keystream >> TemporaryStyle.glyphImageSize.x >> TemporaryStyle.glyphImageSize.y;
			}
			else if(key == "GlyphPadding")
			{
				keystream >> TemporaryStyle.glyphPadding.x >> TemporaryStyle.glyphPadding.y;
			}
			else
			{
				std::cout <<"Error: style key '" << key << "' is unknown.!" << std::endl;
			}
		}
	}
	styleFile.close();
	return true;
}


/********************************************************************************
 * Convert string to element type.
 *******************************************************************************/
GUIElementType
GUIManager::StringToType(const string& typeString) const
{
	auto itr = elementTypes_.find(typeString);
	if(itr == elementTypes_.end())
	{
		return GUIElementType::None;
	}
	else
	{
		return itr->second;
	}
}
