#pragma once
//#include "..\common.h"
#include "../common.h"
#include <sstream>
#include <unordered_map>
#include <SFML/Graphics/RenderTarget.hpp>
#include "GUIStyle.h"


enum class GUIElementType{ None = -1, Interface, Label, Button, Scrollbar, Textfield, ToggleButton, Sprite, DropDownMenu, ScrollbarNew };
enum class SliderDirection { Horizontal, Vertical };


class GUIInterface;

/**
 * The building block of our GUI system.
 */
class GUIElement
{
	friend class GUIInterface;
public:
	GUIElement(const string& name, const GUIElementType& type, GUIInterface* owner);
	virtual ~GUIElement();

	virtual void ReadIn(std::stringstream& stream) {}
	/********************************************************************************
	 * On Click.
	 * @param mousePos is relative to GUIElement.
	 ********************************************************************************/
	virtual void OnClick(const vec2f& mosuePos) {}
	virtual void OnInterfaceClick(const vec2f& mousePos) {}
	virtual void OnRelease() {}
	virtual void OnHover(const vec2f& mousePos) {}
	virtual void OnLeave() {}
	virtual void OnFocus() {}
	virtual void OnDefocus() {}
	virtual void Setup() {}
	virtual void Update(real32 dt) = 0;
	virtual void Draw(sf::RenderTarget* renderTarget) = 0;

	virtual void UpdateStyle(const GUIElementState& state, const GUIStyle& style);
	virtual void ApplyStyle();
	virtual vec2f GetContentSize() const;

	GUIElementType GetType() const;

	string GetName() const;
	//string GetFileName() const;
	void SetName(const string& name);
	const vec2f& GetPosition() const;
	void SetPosition(const vec2f& pos);
	vec2f GetSize() const;
	GUIElementState GetState() const;
	void SetState(const GUIElementState& state);
	void SetRedraw(const bool& redraw);
	bool NeedsRedraw() const;
	void SetOwner(GUIInterface* owner);
	GUIInterface* GetOwner() const;
	bool HasOwner() const;
	bool IsActive() const;
	void SetActive(const bool& active);
	bool IsInside(const vec2f& point) const;
	vec2f GetGlobalPosition() const;
	bool IsControl() const;
	void SetIsControl(const bool& control);
	string GetText() const;
	virtual void SetText(const string& text);
	string GetStyleFileName() const;
	sf::FloatRect GetTextLocalBounds();

	friend std::stringstream& operator>>(std::stringstream& stream, GUIElement& guiElement)
	{
		guiElement.ReadIn(stream);
		return stream;
	}

protected:
	void ApplyTextStyle();
	void ApplyBgStyle();
	void ApplyGlyphStyle();

	void RequireTexture(const string& textureName);
	void RequireFont(const string& fontName);
	void ReleaseTexture(const string& textureName);
	void ReleaseFont(const string& fontName);
	void ReleaseResources();
	string name_;
	//string filename_;
	vec2f position_;
	std::unordered_map<GUIElementState, GUIStyle> styles_;
	GUIDrawable drawable_;
	GUIElementType type_;
	GUIElementState state_;
	GUIInterface* owner_;

	bool needsRedraw_;
	bool active_;
	bool isControl_;
};
