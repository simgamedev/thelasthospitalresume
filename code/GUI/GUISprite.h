#pragma once
#include <SFML/Graphics/Sprite.hpp>
#include "GUIElement.h"


class TextureManager;
class GUISprite : public GUIElement
{
public:
	GUISprite(const string& name, GUIInterface* owner);
	~GUISprite();

	void SetTexture(const string& textureName);
	void SetTexture(sf::RenderTexture& renderTexture);

	void ReadIn(std::stringstream& stream);
	void OnClick(const vec2f& mousePos);
	void OnRelease();
	void OnHover(const vec2f& mousePos);
	void OnLeave();
	void ApplyStyle();
	void Update(real32 dt);
	void Draw(sf::RenderTarget* renderTarget);
	// ---> TEMP
	void SetScale(vec2f scale);
	// ---< TEMP

private:
	TextureManager* textureManager_;
	string textureName_;
	sf::Sprite sprite_;
	vec2f scale_ = vec2f(1.0f, 1.0f);
};
