#pragma once
#include "common.h"
#define GLEW_STATIC
#include <GL/glew.h>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "EventManager.h"

// ---> NoesisGUI
#include <NoesisPCH.h>
// ---< NoesisGUI


class Game;
/**
	A simpile wrapper of sf::RenderWindow.
 */
class Window
{
	friend Game;
public:
	Window();
	Window(const string& title, const vec2u& size);
	~Window();

	void NoesisGUIDraw();

	void Update();

	bool IsDone();
	bool IsFullscreen();
	vec2u GetWindowSize();
	vec2i GetMousePos();
	// FIXME: should we put it here?
	vec2f GetMouseWorldPos() const;
	void SetMouseWorldPos(const vec2f& mouseWorldPos);

	void ToggleFullscreen(EventDetails* eventDetails);
	void Close(EventDetails* eventDetails = nullptr);
	void Draw(sf::Drawable& drawable, int32 order = -1);
	void DrawCursor(sf::Drawable* drawable);
	void DrawGizmo(sf::Drawable* gizmo);
	EventManager* GetEventManager();
	// FIXME: remove this, you don't want to expose renderWindow, it should be private
	sf::RenderWindow* GetRenderWindow();
	sf::FloatRect GetViewSpace();
	vec2u GetSize() const;
	real32 fps_;
	real32 ups_;
	real32 GetFPS() const;
	real32 GetUPS() const;
	void TestDelegate();
private:
	void Setup(const string& title, const vec2u& size);
	void Destroy();
	void Create();

	sf::RenderWindow renderWindow_; ///< The sf::RenderWindow underlying our Window Class
	vec2u windowSize_;
	string windowTitle_;
	bool isDone_;
	bool isFullscreen_;
	bool isFocused_;
	EventManager eventManager_;


	// ---> NoesisGUI
	void OpenGLInit();
	void NoesisGUIInit();
	Noesis::Ptr<Noesis::IView> spNoesisView_;
	unsigned int vaoID_;
	bool bNoesisGUIInited_;
public:
	bool SetNoesisView(Noesis::Ptr<Noesis::IView> view);
	void PauseNoesisGUI();
	// ---< NoesisGUI

private:
	Noesis::Ptr<Noesis::RenderDevice> noesisRenderDevice_;
	void BeginActualDraw();
	void EndActualDraw();
	void ActualDraw();
	std::multimap<int32, sf::Drawable&> renderQueue_;
	std::vector<sf::Drawable*> cursorQueue_;
	std::vector<sf::Drawable*> gizmoQueue_;

	bool bEventHandledByNoesis_;
	Noesis::Key _sfmlToNoesisKeyMapping[256];
	void InitSFMLToNoesisKeyMapping();

	vec2f mouseWorldPos_;
};
