#include "CharMsgDispatcher.h"
#include "../TestEntity.h"
#include "../StateGame.h"


CharMsgDispatcher* CharMsgDispatcher::instance_ = nullptr;

// TODO: formal singleton
CharMsgDispatcher*
CharMsgDispatcher::Instance()
{
	if (instance_ == nullptr)
		instance_ = new CharMsgDispatcher();
	return instance_;
}

CharMsgDispatcher::CharMsgDispatcher()
{
}

void
CharMsgDispatcher::DispatchMessage(GameTime delay,
								   int32 sender,
								   int32 receiver,
								   CharMsgType type,
								   void* extraInfo)
{
	//TestEntity* pReceiver = CharacterManager->GetCharacterFromID(receiver);
	TestEntity* pReceiver = stateGame_->GetCharacterManager()->GetCharacterFromID(receiver);
	Telegram telegram(delay, sender, receiver, type, extraInfo);

	// if there is no delay, route the telegram immediately
	GameTime zeroGameTime(0, 0, 0);
	if(delay == zeroGameTime)
	{
		// send the telegram to the recipient
		Discharge(pReceiver, telegram);
	}
	else
	{
		// delay
	}	
}

void
CharMsgDispatcher::Discharge(TestEntity* pReceiver, const Telegram telegram)
{
	pReceiver->OnMessage(telegram);
}

void
CharMsgDispatcher::DispatchDelayedMessages()
{
	//
	//real32 currentTime = GameClock->GetCurrentRealTime();
	//real32 currentTime = GameClock->GetCurrentGameTime();

}

void
CharMsgDispatcher::BroadcastMessage(Telegram telegram)
{
	// brocast to specific characters
	//Telegram telegram(0.0f, sender, -1, type, extraInfo);
	CharMsgType type = telegram.type;
	if(type == CharMsgType::NewPatient)
	{
		for(TestEntity* testEntity : stateGame_->GetCharacterManager()->GetAllCharactersOfProfession(Profession::Tester))
		{
			testEntity->OnBroadcast(telegram);
		}
	}

	if (type == CharMsgType::NeedDrug)
	{
		for(TestEntity* testEntity : stateGame_->GetCharacterManager()->GetAllCharactersOfProfession(Profession::Nurse))
		{
			testEntity->OnBroadcast(telegram);
		}
	}

	// broadcast to relative systems(not ecs yet)
	auto communicatorsItr = communicators_.find(type);
	if (communicatorsItr == communicators_.end()) { return; }
	MsgCommunicator communicator = communicatorsItr->second;
	communicator.Broadcast(telegram);
}

void
CharMsgDispatcher::BroadcastMessage(int32 sender,
								    CharMsgType type,
							        void* extraInfo)
{
	// brocast to specific characters
	GameTime zeroGameTime(0, 0, 0);
	Telegram telegram(zeroGameTime, sender, -1, type, extraInfo);
	BroadcastMessage(telegram);
}

void
CharMsgDispatcher::SetStateGame(StateGame* stateGame)
{
	stateGame_ = stateGame;
}


void
CharMsgDispatcher::BroadcastDelayedMessage(GameTime delay,
										   int32 sender,
										   CharMsgType type,
										   void* extraInfo)
{
	Telegram telegram(delay, sender, -1, type, extraInfo);
	priorityQ_.emplace(telegram);
}


void
CharMsgDispatcher::DispatchDelayedBroadcasts()
{
	GameTime curGameTime = stateGame_->GetClock()->GetGameTime();
	GameTime zeroGameTime(0, 0, 0);
	while(!priorityQ_.empty() &&
		  (priorityQ_.begin()->dispatchTime < curGameTime))
	{
		LOG_GAME_ERROR("A Delayed Broadcast Just Dispatched!!!!!!!!!!");
		Telegram telegram = *priorityQ_.begin();
		BroadcastMessage(telegram);
		priorityQ_.erase(priorityQ_.begin());
	}
}



void
CharMsgDispatcher::Update(const sf::Time& time)
{
	GameTime curGameTime = Clock::Instance()->GetGameTime();
	if (curGameTime.minutes_ - lastGameTime_.minutes_ < 1)
		return;
	DispatchDelayedBroadcasts();
	lastGameTime_ = curGameTime;
}
