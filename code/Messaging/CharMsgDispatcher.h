#pragma once
#include "../common.h"

#include <set>
#include <map>
#include "Telegram.h"
#include "CharMsgTypes.h"
#include "../Time/Clock.h"
//#include <SFML/System/Time.hpp>
class TestEntity;
class StateGame;

const real32 SEND_MSG_IMMEDIATELY = 0.0f;
const int32 SENDER_ID_IRRELEVANT = -1;

//#include "IMsgObserver.h"
#include "MsgCommunicator.h"
#include "../Time/Time.h"

struct TelegramCompare
{
	bool operator()(const Telegram& lhs, const Telegram& rhs) const
	{
		uint32 t1 = lhs.dispatchTime.minutes_ + lhs.dispatchTime.hours_ * 60 + lhs.dispatchTime.days_ * 60 * 24;
		uint32 t2 = rhs.dispatchTime.minutes_ + rhs.dispatchTime.hours_ * 60 + rhs.dispatchTime.days_ * 60 * 24;
		return t1 < t2;
	}
};
// TODO: Rename this to MessageManager
class CharMsgDispatcher
{
private:
	StateGame* stateGame_;
	static CharMsgDispatcher* instance_;
	std::set<Telegram, TelegramCompare> priorityQ_;
	void Discharge(TestEntity* pReceiver, const Telegram telegram);
	CharMsgDispatcher();
	std::map<CharMsgType, MsgCommunicator> communicators_;
public:
	// this class is a singleton
	static CharMsgDispatcher* Instance();

	void Update(const sf::Time& time);
	void SetStateGame(StateGame* stateGame);
	// send a message to another agent
	void DispatchMessage(GameTime delay,
						 int32 sender,
						 int32 receiver,
						 CharMsgType type,
						 void* extraInfo);

	void BroadcastMessage(Telegram telegram);
	void BroadcastMessage(int32 sender,
						  CharMsgType type,
						  void* extraInfo);

	void BroadcastDelayedMessage(GameTime delay,
						         int32 sender,
						         CharMsgType type,
						         void* extraInfo);
						  
	// TODO: Rename to RegisterForBroadcast(becasue message doesn't need to be registered to receive)
	bool Register(CharMsgType msgType, IMsgObserver* observer)
	{
		return communicators_[msgType].AddObserver(observer);
	}

	bool UnRegister(CharMsgType msgType, IMsgObserver* observer)
	{
		return communicators_[msgType].RemoveObserver(observer);
	}

	void DispatchDelayedMessages();
	void DispatchDelayedBroadcasts();
private:
	GameTime lastGameTime_;
};
