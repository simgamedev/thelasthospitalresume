#pragma once

enum class CharMsgType
{
	NewPatient,
		CharDied,
		NeedDrug,
		NeedChangeIV,
		LungDamaged,
		GarbageDropped,
		ShippingArrived, // or just arrived shiptment provides ActionPickMe?
		TestResultReady,
		GotHome,
		Hospitalized,

		PathReady,
		NoPathAvailable,
		NoFoodForYou,

		NewLitter,
		RoomReady,
		PatientRegistered,
		PCRTestRoomBuilt,
		XRayResultReady,
		ReceivedDrug,
};

