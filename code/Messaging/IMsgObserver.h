#pragma once

#include "Telegram.h"
#include "CharMsgTypes.h"

// TODO: reanme to IBroadcastListener
class IMsgObserver
{
public:
	//virtual void OnMessage(Telegram telegram) = 0;
	virtual void OnBroadcast(Telegram telegram) = 0;
	virtual void RegisterForMsgType(CharMsgType msgType) = 0;
};
