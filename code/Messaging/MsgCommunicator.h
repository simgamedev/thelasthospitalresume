#pragma once

#include <vector>
#include "IMsgObserver.h"

/* Each CharMsgType has exactly 1 Communicator
 */
class MsgCommunicator
{
public:
	~MsgCommunicator()
	{
		observers_.clear();
	}	

	bool AddObserver(IMsgObserver* observer)
	{
		if(HasObserver(observer))
		{
			return false;
		}
		observers_.emplace_back(observer);
	}

	bool RemoveObserver(IMsgObserver* observer)
	{
		auto observersItr = std::find_if(observers_.begin(), observers_.end(),
										 [observer](IMsgObserver* element)
										 {
											 return element ==  observer;
										 });
		if(observersItr == observers_.end())
		{
			return false;
		}
		observers_.erase(observersItr);
		return true;
	}

	bool HasObserver(const IMsgObserver* observer)
	{
		return(std::find_if(observers_.begin(),
							observers_.end(),
							[&observer](IMsgObserver* element)
							{
								return element == observer;
							}) != observers_.end());
	}

	void Broadcast(Telegram telegram)
	{
		for (auto observer : observers_)
		{
			observer->OnBroadcast(telegram);
		}
	}
private:
	std::vector<IMsgObserver*> observers_;
};


