#ifndef TELEGRAM_H
#define TELEGRAM_H

#include "../common.h"
#include "../Time/Time.h"


#include "CharMsgTypes.h"
#include <iostream>


struct Telegram
{
	int32 sender;

	int32 receiver;

	CharMsgType type;

	//real32 dispatchTime;
	GameTime dispatchTime;

	void* extraInfo;

	Telegram(GameTime delay,
			 int32 sender,
			 int32 receiver,
			 CharMsgType type,
			 void* info = nullptr)
		: dispatchTime(delay)
		, sender(sender)
		, receiver(receiver)
		, type(type)
		, extraInfo(info)
	{
	}
	bool operator<(const Telegram& t2)
	{
		if (dispatchTime.days_ < t2.dispatchTime.days_)
			return true;
		if (dispatchTime.hours_ < t2.dispatchTime.hours_)
			return true;
		if (dispatchTime.minutes_ < t2.dispatchTime.minutes_)
			return true;
		return false;
	}
};

/*
inline bool operator<(const Telegram& t1, const Telegram& t2)
{
	if (t1.dispatchTime.days_ < t2.dispatchTime.days_)
		return true;
	if (t1.dispatchTime.hours_ < t2.dispatchTime.hours_)
		return true;
	if (t1.dispatchTime.minutes_ < t2.dispatchTime.minutes_)
		return true;
	return false;
}
*/

#endif
