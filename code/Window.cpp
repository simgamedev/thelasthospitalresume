#include "Window.h"


// ---> NoesisGUI
/*
#include <NsGui/FontProperties.h>
#include <NsGui/IntegrationAPI.h>
#include <NsGui/Grid.h>
#include <NsCore/HighResTimer.h>
#include <NsGui/Button.h>
*/
#include <NsApp/ThemeProviders.h>
#include <NsApp/LocalFontProvider.h>
#include <NsApp/LocalTextureProvider.h>
#include <NsApp/LocalXamlProvider.h>
#include <NsGui/IRenderer.h>
#include <NsRender/GLFactory.h>
// ---< NoesisGUI
// ---> TEMP
#include <NsApp/Interaction.h>
#include <NsApp/StyleInteraction.h>
#include <NsApp/TriggerCollection.h>
#include <NsApp/Launcher.h>
// ---< TEMP
using namespace Noesis;
using namespace NoesisApp;

/********************************************************************************
 * Default Constructor.
 ********************************************************************************/
Window::Window()
	: eventManager_(this)
	, bNoesisGUIInited_(false)
{
	// ---> Fullscreen Mode.
	isFullscreen_ = true;
	sf::VideoMode desktopMode = sf::VideoMode::getDesktopMode();
	Setup("Window", vec2u(desktopMode.width, desktopMode.height));
	// ---<

	// ---> Windowed Mode.
	//isFullscreen_ = false;
	//Setup("Window", vec2u(1920, 1080));
	// ---<
}


/********************************************************************************
 * Constructor with 2 parameters.
 * @param title title text
 * @param size window size in pixels 
 ********************************************************************************/
// FIXME: this seems to be redundant
Window::Window(const string& title, const vec2u& size)
	: eventManager_(this)
{
	Setup(title, size);
}


/********************************************************************************
 * Deconstructor.
 ********************************************************************************/
Window::~Window()
{
	Destroy();
}


/********************************************************************************
 *
 ********************************************************************************/
void
Window::Setup(const string& title, const vec2u& size)
{
	windowTitle_ = title;
	windowSize_ = size;
	isDone_ = false;
	isFocused_ = true;

	eventManager_.AddCallback(GameStateType(0), "Fullscreen_Toggle", &Window::ToggleFullscreen, this);
	eventManager_.AddCallback(GameStateType(0), "Window_Close", &Window::Close, this);
	Create();
}

// ---> Deleate This Crap
/******************************* OpenGLInit *************************************
 *
 ********************************************************************************/
static float vertices[] = {
		-0.5f, 0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,

		0.5f, -0.5f, 0.0f,
		0.5f, 0.5f, 0.0f,
		-0.5f, 0.5f, 0.0f,
};
void
Window::OpenGLInit()
{
	unsigned int vboID;
	glGenVertexArrays(1, &vaoID_);
	glGenBuffers(1, &vboID);

	glBindVertexArray(vaoID_);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}
// ---< Delete this crap

/******************************* NoesisGUIDraw **********************************
 *
 ********************************************************************************/
void
Window::NoesisGUIDraw()
{
	if (spNoesisView_ != nullptr &&
		bNoesisGUIInited_)
	{
		renderWindow_.setActive(true);

		static uint64_t startTime = Noesis::HighResTimer::Ticks();

		// Update (layout, animations). Note that global time is used, not a delta
		uint64_t time = Noesis::HighResTimer::Ticks();
		spNoesisView_->Update(Noesis::HighResTimer::Seconds(time - startTime));

		// Offscreen rendering phase populates textures needed by the on-screen rendering
		spNoesisView_->GetRenderer()->UpdateRenderTree();
		spNoesisView_->GetRenderer()->RenderOffscreen();

		// If you are going to render here with your own engine you need to restore the GPU state
		// because noesis changes it. In this case only framebuffer and viewport need to be restored
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		//glViewport(0, 0, 1920, 1080);
		//glClearStencil(0);
		//glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		// TODO:
		//Draw

		// Rendering is done in the active framebuffer
		spNoesisView_->GetRenderer()->Render();

		renderWindow_.resetGLStates();
		renderWindow_.setActive(false);
	}
}

/******************************* NoesisGUIInit **********************************
 *
 ********************************************************************************/
void
Window::NoesisGUIInit()
{
	Noesis::SetLogHandler([](const char*, uint32_t, uint32_t level, const char*, const char* msg)
						  {
							  // [TRACE] [DEBUG] [INFO] [WARNING] [ERROR]
							  const char* prefixes[] = { "T", "D", "I", "W", "E" };
							  printf("[NOESIS/%s] %s\n", prefixes[level], msg);
						  });

	// Noesis initialization. This must be the first step before using any NoesisGUI functionality
	Noesis::GUI::Init(NS_LICENSE_NAME, NS_LICENSE_KEY);

	// ---> Resource Providers
	Ptr<LocalXamlProvider> myXamlProvider = MakePtr<LocalXamlProvider>("../Media/NoesisGUI/Xaml");
	Ptr<LocalFontProvider> myFontProvider = MakePtr<LocalFontProvider>("../Media/NoesisGUI/Fonts");
	Ptr<LocalTextureProvider> myTextureProvider = MakePtr<LocalTextureProvider>("../Media/NoesisGUI/Textures");

	NoesisApp::SetThemeProviders(myXamlProvider, myFontProvider, myTextureProvider);

	Noesis::GUI::LoadApplicationResources("Theme/NoesisTheme.DarkBlue.xaml");
	// ---< Resource Providers

	//Ptr<FrameworkElement> xaml = Noesis::GUI::LoadXaml<FrameworkElement>("Settings.xaml");
	// View creation to render and interact with the user interface
	// We transfer the ownership to a global pointer instead of a Ptr<> because there is no way
	// in GLUT to do shutdown and we don't want the Ptr<> to be released at global time
	//_view = Noesis::GUI::CreateView(xaml).GiveOwnership();
	//_view->SetFlags(Noesis::RenderFlags_PPAA | Noesis::RenderFlags_LCD);
	//_view->SetSize(1920, 1080);


	// Renderer initialization with an OpenGL device
	// TODO: try do this
    noesisRenderDevice_ = NoesisApp::GLFactory::CreateDevice(false);
	// Noesis::RenderDevice* _noesisRenderDevice = NoesisApp::GLFactory::CreateDevice(false))
	//_view->GetRenderer()->Init(NoesisApp::GLFactory::CreateDevice(false));

	// ---> TEMP
	// NOTE: this is used to register interactivity(i:,ei: in .xaml) stuff
	NoesisApp::Launcher::RegisterAppComponents();
	// ---< TEMP

	InitSFMLToNoesisKeyMapping();

}

/********************************************************************************
 * calls the sf::RenderWindow's create() with sf::Style enum.
 ********************************************************************************/
void
Window::Create()
{
	// ---> OpenGL Context Settings
	sf::ContextSettings contextSettings;
	contextSettings.depthBits = 24;
	contextSettings.stencilBits = 8;
	contextSettings.sRgbCapable = false;
	contextSettings.antialiasingLevel = 4;
	contextSettings.majorVersion = 3;
	contextSettings.minorVersion = 2;
	// Try to set core or compatibility profile
	// ---< OpenGL Context Settings
	auto style = (isFullscreen_ ? sf::Style::Fullscreen : sf::Style::Default);
	renderWindow_.create({ windowSize_.x, windowSize_.y, 32 }, windowTitle_, style, contextSettings);
	renderWindow_.setVerticalSyncEnabled(true);
	// hide default cursor
	renderWindow_.setMouseCursorVisible(false);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		LOG_ENGINE_CRITICAL("[GLEW] Failed to init!");
	}
	else
	{
		LOG_ENGINE_INFO("[GLEW] Inited!");
	}

	// ---> NoesisGUI
	renderWindow_.setActive(true);
	NoesisGUIInit();
	// FIXME: the reason you have to somehow use opengl first
	// maybe caused by the fact that resetGLStates doesn't work and you have to
	// reset objects manually like below
	// -------------------------------------
    //Application loop (while window.isOpen())
	//If you start drawing your OpenGL (in my case 3d scene) before SFML, use glClear instead of window.clear
	//glClear(GL_COLOR_BUFFER_BIT);
	//glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //or your favourite color

	//Drawing your stuff
	//glDrawArrays(...)
	//glDrawElements(....);

	//Switching to sfml

	//Clearing objects
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindVertexArray(0);
	//glUseProgram(0);
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, 0);

	//SFML drawing
	//window.pushGLStates();
	//window.draw(...);
	//window.popGLStates();
	// ----------------------------------------------
	OpenGLInit();
	renderWindow_.setActive(false);
	// ---< NoesisGUI


	// ---> OpenGL Context Info
	sf::ContextSettings settings = renderWindow_.getSettings();

	std::cout << "depth bits:" << settings.depthBits << std::endl;
	std::cout << "stencil bits:" << settings.stencilBits << std::endl;
	std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
	std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;
	// ---< OpenGL Context Info

	// ---> Init IMGUI
	// ---<

	// ---> NoesisGUIInit
	// ---<
}


/********************************************************************************
 * Calls the unerlying sf::RenderWindow's close method.
 ********************************************************************************/
void
Window::Destroy()
{
	LOG_ENGINE_CRITICAL("You didn't properly close the game!!");
	renderWindow_.close();
}


/********************************************************************************
 * Get Window Size.
 ********************************************************************************/
vec2u
Window::GetSize() const
{
	return renderWindow_.getSize();
}


/********************************************************************************
 * Processes sf::Events, If F5 is pressed fullscreen is toggled.
 ********************************************************************************/
void
Window::Update()
{
	sf::Event event;
	while(renderWindow_.pollEvent(event))
	{
		bEventHandledByNoesis_ = false;

		// ---> Lost focus
		if(event.type == sf::Event::LostFocus)
		{
			isFocused_ = false;
			eventManager_.SetFocus(false);
		}
		// ---> Gained focus
		else if(event.type == sf::Event::GainedFocus)
		{
			isFocused_ = true;
			eventManager_.SetFocus(true);
		}
		// ---> Mouse moved
		else if(event.type == sf::Event::MouseMoved)
		{
			if (spNoesisView_)
			{
				//bEventHandledByNoesis_ = spNoesisView_->MouseMove(event.mouseMove.x, event.mouseMove.y);
				spNoesisView_->MouseMove(event.mouseMove.x, event.mouseMove.y);
			}
		}
		// ---> Mouse button pressed
		else if (event.type == sf::Event::MouseButtonPressed)
		{
			// left mouse button
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				if (spNoesisView_)
				{

					/*
					bEventHandledByNoesis_ = spNoesisView_->MouseButtonDown(event.mouseButton.x,
																			event.mouseButton.y,
																			Noesis::MouseButton_Left);
																			*/
					spNoesisView_->MouseButtonDown(event.mouseButton.x,
												   event.mouseButton.y,
												   Noesis::MouseButton_Left);
				}
			}
		}
		// ---> Mouse button released
		else if (event.type == sf::Event::MouseButtonReleased)
		{
			// left mouse button
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				if (spNoesisView_)
				{
					/*
					bEventHandledByNoesis_ = spNoesisView_->MouseButtonUp(event.mouseButton.x,
																		  event.mouseButton.y,
																		  Noesis::MouseButton_Left);
																		  */
					spNoesisView_->MouseButtonUp(event.mouseButton.x,
												 event.mouseButton.y,
											     Noesis::MouseButton_Left);
				}
			}
		}
		// ---> Mouse wheel moved
		else if (event.type == sf::Event::MouseWheelMoved)
		{
			if (spNoesisView_)
			{
				bEventHandledByNoesis_ = spNoesisView_->MouseWheel(event.mouseWheel.x,
																   event.mouseWheel.y,
																   event.mouseWheel.delta * 120);
			}
		}
		// ---< Keyboard key pressed
		else if (event.type == sf::Event::KeyPressed)
		{
			if (spNoesisView_)
			{
				// TODO: implement proper keyboard handling for NoesisGUI
				//bEventHandledByNoesis_ = spNoesisView_->Char(Noesis::Key::Key_Enter);
				//bEventHandledByNoesis_ = spNoesisView_->KeyDown(Noesis::Key::Key_A);
				if (_sfmlToNoesisKeyMapping[event.key.code] < 0)
					;
				else
				{
					int32 sfmlKeyCode = event.key.code;
					Noesis::Key noesisKey= _sfmlToNoesisKeyMapping[sfmlKeyCode];
					bEventHandledByNoesis_ = spNoesisView_->KeyDown(noesisKey);
				}
			}
		}
		// ---< Passing SFML events to NoesisGUI

		// NOTE: if input event was handled by NoesisGUI, then don't pass it to gameplay
		if(!bEventHandledByNoesis_)
			eventManager_.HandleEvent(event);
	}

	eventManager_.Update();
}


/********************************************************************************
 * Toggle Fullscreen.
 ********************************************************************************/
void
Window::ToggleFullscreen(EventDetails* eventDetails)
{
	isFullscreen_ = !isFullscreen_;
	Destroy();
	Create();
}






/**
   Draw.
 */
// TODO: you are not properly using this.
// TODO: Window Class should have an only Renderer on a seperate rendering thread.
// TODO: Here is how every thing(expcet gui) should be drawn:
// TODO: window->PushToDraw(sf::Drawable& drawable, int index=tileY(higher means draw later)
// TODO: and the Window::Draw method should be private.
void 
Window::Draw(sf::Drawable& drawable, int32 order)
{
	renderQueue_.emplace(order, drawable);
	// TODO:
	/*
	for (sf::Drawable& drawable : renderQ_)
	{
		renderWindow.draw(drawable);
	}
	*/
	//renderWindow_.draw(drawable);
}



/**
   Is Fullscreen?
 */
bool
Window::IsFullscreen()
{
	return isFullscreen_;
}
/**
   Is Window Closed?
 */
bool
Window::IsDone()
{
	return isDone_;
}

/**
   Get Window Size In Pixels
 */
vec2u
Window::GetWindowSize()
{
	return windowSize_;
}


/********************************************************************************
 * Get mouse position relative to game window up-left corner
 ********************************************************************************/
vec2i
Window::GetMousePos()
{
	vec2i result = sf::Mouse::getPosition(renderWindow_);
	return result;
}


/********************************************************************************
 * Get Event Manager
 ********************************************************************************/
EventManager*
Window::GetEventManager()
{
	return &eventManager_;
}


/********************************************************************************
 * Close the window
 ********************************************************************************/
void
Window::Close(EventDetails* eventDetails)
{
	isDone_ = true;
}

/********************************************************************************
 * Get the underlying sf::RenderWindow
 ********************************************************************************/
sf::RenderWindow*
Window::GetRenderWindow()
{
	return &renderWindow_;
}

/********************************************************************************
 * Get the window's view space as a sf::FloatRect.
 ********************************************************************************/
sf::FloatRect
Window::GetViewSpace()
{
	vec2f viewCenter = renderWindow_.getView().getCenter();
	vec2f viewSize = renderWindow_.getView().getSize();
	vec2f viewSizeHalf(viewSize.x / 2.0f, viewSize.y / 2.0f);
	sf::FloatRect viewSpace(viewCenter - viewSizeHalf, viewSize);
	return viewSpace;
}

real32
Window::GetFPS() const
{
	return fps_;
}

real32
Window::GetUPS() const
{
	return ups_;
}

void
Window::TestDelegate()
{
}

/******************************* SetNoesisView **********************************
 *
 ********************************************************************************/
bool
Window::SetNoesisView(Noesis::Ptr<Noesis::IView> view)
{
	renderWindow_.setActive(true);

	spNoesisView_.Reset();
	spNoesisView_ = view;
	if (spNoesisView_ != nullptr)
	{
		spNoesisView_->SetFlags(Noesis::RenderFlags_PPAA | Noesis::RenderFlags_LCD);
		spNoesisView_->SetSize(1920, 1080);
		// Note that several views can be managed in the same render thread just 
		// by initializing all of them with the same render device. That way 
		// internal resources like ramps and glyphs are shared across all views. 
		// RenderDevice is a heavyweight class. Extra instances should be avoided whenever possible.
		spNoesisView_->GetRenderer()->Init(noesisRenderDevice_);
		//spNoesisView_->GetRenderer()->Init(NoesisApp::GLFactory::CreateDevice(false));
		bNoesisGUIInited_ = true;
	}

	renderWindow_.setActive(false);
	return true;
}

/******************************* PauseNoesisGUI *********************************
 *
 ********************************************************************************/
// FIXME: it's not being used
void
Window::PauseNoesisGUI()
{
	spNoesisView_.Reset();
	bNoesisGUIInited_ = false;
}



/******************************* Actual Draw ************************************
 * draw everything in the render quque
 ********************************************************************************/
void
Window::BeginActualDraw()
{
	renderWindow_.resetGLStates();
	renderWindow_.clear(sf::Color::Black);
}
void
Window::ActualDraw()
{
	for (auto& itr : renderQueue_)
	{
		sf::Drawable& drawable = itr.second;
		renderWindow_.draw(drawable);
	}

	for (sf::Drawable* drawable : gizmoQueue_)
	{
		renderWindow_.draw(*drawable);
	}
}

void
Window::DrawGizmo(sf::Drawable* gizmo)
{
	gizmoQueue_.push_back(gizmo);
}

void
Window::DrawCursor(sf::Drawable* drawable)
{
	cursorQueue_.push_back(drawable);
}

void
Window::EndActualDraw()
{
	NoesisGUIDraw();
	// ---> Draw Cursor
	for (sf::Drawable* drawable : cursorQueue_)
	{
		renderWindow_.setView(renderWindow_.getDefaultView());
		renderWindow_.draw(*drawable);
	}
	// ---< Draw Cursor
	renderWindow_.display();

	renderQueue_.clear();
	gizmoQueue_.clear();
	cursorQueue_.clear();
}

/******************************* InitSFMLToNoesisKeyMapping *********************
 * draw everything in the render quque
 ********************************************************************************/
void
Window::InitSFMLToNoesisKeyMapping()
{
	for (int32 i = 0; i < 256; i++)
		_sfmlToNoesisKeyMapping[i] = Noesis::Key(-1);
	_sfmlToNoesisKeyMapping[sf::Keyboard::F1] = Noesis::Key::Key_F1;
	_sfmlToNoesisKeyMapping[sf::Keyboard::Return] = Noesis::Key::Key_Return;
	_sfmlToNoesisKeyMapping[sf::Keyboard::A] = Noesis::Key::Key_A;
	_sfmlToNoesisKeyMapping[sf::Keyboard::Escape] = Noesis::Key::Key_Escape;
	_sfmlToNoesisKeyMapping[sf::Keyboard::Tilde] = Noesis::Key::Key_OemTilde;
}

// FIXME: find a proper place for this function
/******************************* MouseWorldPos **********************************
 *
 ********************************************************************************/
vec2f
Window::GetMouseWorldPos() const
{
	return mouseWorldPos_;
}
void
Window::SetMouseWorldPos(const vec2f& mouseWorldPos)
{
	mouseWorldPos_ = mouseWorldPos;
}


