#include "AnimDirectional.h"
#include "GameSprite.h"

void
AnimDirectional::CropSprite()
{
	/*
	vec2f padding = Sprite_->GetSheetPadding();
	vec2f spacing = Sprite_->GetSpriteSpacing();
	sf::IntRect rect((Sprite_->GetSpriteSize().x * frameCurrent_) + padding.x + (spacing.x * frameCurrent_),
					 (Sprite_->GetSpriteSize().y * (frameRow_ + (short)Sprite_->GetDirection())) + padding.y + ((frameRow_ + (short)Sprite_->GetDirection()) * spacing.y),
					 Sprite_->GetSpriteSize().x,
					 Sprite_->GetSpriteSize().y);
	*/
	sf::IntRect rect = sprite_->GetSpriteRect(name_ + "_" + std::to_string(frameCurrent_), sprite_->GetDirection());
	sprite_->CropSprite(rect);
}


void
AnimDirectional::FrameStep()
{
	bool success = SetCurrentFrame(frameCurrent_ + 1);
	if(success){ return; }
	if(looping_)
	{
		SetCurrentFrame(frameStart_);
	}
	else
	{
		SetCurrentFrame(frameEnd_);
		Pause();
	}
}


void
AnimDirectional::ReadIn(std::stringstream& stream)
{
	stream >> frameStart_ >> frameEnd_ >> frameRow_
		   >> frameTime_;
}
