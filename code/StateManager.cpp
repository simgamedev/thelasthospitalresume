#include "StateManager.h"

/********************************************************************************
 * Constructor.
 ********************************************************************************/
StateManager::StateManager(SharedContext* sharedContext)
	: sharedContext_(sharedContext)
{
	RegisterState<StateIntro>(GameStateType::Intro);
	RegisterState<StateMainMenu>(GameStateType::MainMenu);
	RegisterState<StateGame>(GameStateType::Game);
	RegisterState<StatePaused>(GameStateType::Paused);
	RegisterState<StateLoading>(GameStateType::Loading);
	RegisterState<StateMapEditor>(GameStateType::MapEditor);
	// ---> Create a stateloading beforehand
	CreateState(GameStateType::Loading);
	stateLoading_ = static_cast<StateLoading*>(states_.back().second);
}


/********************************************************************************
 * Destructor.
 ********************************************************************************/
StateManager::~StateManager()
{
	for(auto &itr : states_)
	{
		BaseState* state = itr.second;
		state->OnDestroy();
		delete state;
	}
}
/********************************************************************************
 * Get Currnet game state type.
 ********************************************************************************/
GameStateType
StateManager::GetCurrentGameStateType() const
{
	if(states_.empty()) { return static_cast<GameStateType>(0); } // GlobalStateType
	return states_.back().first;
}

/********************************************************************************
 * Get Next To Last State Type.
 ********************************************************************************/
GameStateType
StateManager::GetNextToLastGameStateType() const
{
	if(states_.empty()) { return static_cast<GameStateType>(0); }
	if(states_.size() == 1) { return states_.back().first; }
	return (states_.end() - 2)->first;
}

/********************************************************************************
 * Update.
 ********************************************************************************/
void
StateManager::Update(const sf::Time& time)
{
	if(states_.empty()){ return; }
	// The top state is transcendent, so we have to update state below it too, so on and so forth.
	if(states_.back().second->IsTranscendent() && states_.size() > 1)
	{
		// traverse back to the downmost transcendent state, and starts updating up there
		auto itr = states_.end();
		while(itr != states_.begin())
		{
			if(itr != states_.end())
			{
				if(!itr->second->IsTranscendent())
				{
					break;
				}
			}
			--itr;
		}
		for(;
			itr != states_.end();
			++itr)
		{
			itr->second->Update(time);
		}
	}
	// Top state is not transcendent, so just update the top state only.
	else
	{
		states_.back().second->Update(time);
	}
}


/********************************************************************************
 * Draw.
 ********************************************************************************/
void
StateManager::Draw()
{
	if(states_.empty()){ return; }
	// Top state on stack is transparent, so we have to draw the state below it too, so on and so forth.
	if(states_.back().second->IsTransparent() && states_.size() > 1)
	{
		// traverse back to the downmost transparent state, and starts drawing up there
		auto itr = states_.end();
		while(itr != states_.begin())
		{
			if(itr != states_.end())
			{
				if(!itr->second->IsTransparent())
				{
					break;
				}
			}
			--itr;
		}
		for(;
			itr != states_.end();
			++itr)
		{
			sharedContext_->window->GetRenderWindow()->setView(itr->second->GetView());
			itr->second->Draw();
		}
	}
	// Top state on stack in not transparent, so just draw the top state only.
	else
	{
		sharedContext_->window->GetRenderWindow()->setView(states_.back().second->GetView());
		states_.back().second->Draw();
	}
}

SharedContext* StateManager::GetContext(){ return sharedContext_; }

/********************************************************************************
 * Query if state exists
 ********************************************************************************/
bool StateManager::HasState(const GameStateType& type)
{
	for(auto itr = states_.begin();
		itr != states_.end();
		++itr)
	{
		if(itr->first == type)
		{
			auto toRemoveItr = std::find(toRemove_.begin(), toRemove_.end(), type);
			if(toRemoveItr == toRemove_.end()) { return true; }
			return false;
		}
	}
	return false;
}


/********************************************************************************
 * Process Removing Requests
 ********************************************************************************/
void
StateManager::ProcessRemovingRequests()
{
	while(toRemove_.begin() != toRemove_.end())
	{
		RemoveState(*toRemove_.begin());
		toRemove_.erase(toRemove_.begin());
	}
}



/********************************************************************************
 * Switch to a state.
 ********************************************************************************/
void
StateManager::AddDependent(StateDependent* dependent)
{
	stateDependents_.push_back(dependent);
}
void
StateManager::SetStateDependents(const GameStateType& type)
{
	for(auto& dependent : stateDependents_)
	{
		dependent->ChangeGameState(type);
	}
}
void
StateManager::SwitchTo(const GameStateType& type)
{
	//sharedContext_->eventManager->SetCurrentGameState(type);
	SetStateDependents(type);
	// state alread exists, 
	for(auto itr = states_.begin();
		itr != states_.end();
		++itr)
	{
		GameStateType stateType = itr->first;
		BaseState* state = itr->second;
		if(stateType == type)
		{
			// ---> Put the state you want to switch to on top of states stack.
			states_.back().second->Deactivate();
			states_.erase(itr);
			states_.emplace_back(stateType, state);
			state->Activate();
			sharedContext_->window->GetRenderWindow()->setView(state->GetView());
			// ---> If there are files to load switch to state loading tehn.
			if(type == GameStateType::Loading) { return; }
			if(stateLoading_->HasWork())
			{
				SwitchTo(GameStateType::Loading);
			}
			return;
		}
	}

	// state doesn't exist, create one
	if(!states_.empty()){ states_.back().second->Deactivate(); }
	CreateState(type);
	BaseState* newState = states_.back().second;
	newState->Activate();
	sharedContext_->window->GetRenderWindow()->setView(newState->GetView());
	// ---> If there are files to load switch to state loading tehn.
	if(type == GameStateType::Loading) { return; }
	if(stateLoading_->HasWork())
	{
		SwitchTo(GameStateType::Loading);
	}
}


/********************************************************************************
 * Request Remove.
 ********************************************************************************/
void
StateManager::RequestRemove(const GameStateType& type)
{
	toRemove_.push_back(type);
}

/********************************************************************************
 * Create a state
 ********************************************************************************/
void
StateManager::CreateState(const GameStateType& type)
{
	auto statesFactoriesItr = statesFactories_.find(type);
	if(statesFactoriesItr == statesFactories_.end()){ return; }
	auto stateFactory = statesFactoriesItr->second;
	BaseState* newState = stateFactory();
	newState->view_ = sharedContext_->window->GetRenderWindow()->getDefaultView();
	states_.emplace_back(type, newState);
	newState->OnCreate();
}


/********************************************************************************
 * Really Remove A State.
 ********************************************************************************/
void
StateManager::RemoveState(const GameStateType& type)
{
	for(auto itr = states_.begin();
		itr != states_.end();
		++itr)
	{
		GameStateType stateType = itr->first;
		BaseState* state = itr->second;
		if(stateType == type)
		{
			state->OnDestroy();
			delete state;
			states_.erase(itr);
			return;
		}
	}
}
