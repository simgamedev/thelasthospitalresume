#include "StateIntro.h"
#include "StateManager.h"


/********************************************************************************
 * Empty Constructor.
 ********************************************************************************/
StateIntro::StateIntro(StateManager* stateManager)
	: BaseState(stateManager)
{
}

/********************************************************************************
 * Empty Destructor.
 ********************************************************************************/
StateIntro::~StateIntro(){}


/********************************************************************************
 * Called right after the state gets created.
 ********************************************************************************/
void
StateIntro::OnCreate()
{
	timePassed_ = 0.0f;

	vec2u windowSize = stateManager_->GetContext()->window->GetRenderWindow()->getSize();

	introTexture_.loadFromFile("intro.png");
	introSprite_.setTexture(introTexture_);
	introSprite_.setOrigin(introTexture_.getSize().x / 2.0f,
						  introTexture_.getSize().y / 2.0f);
	introSprite_.setPosition(windowSize.x / 2.0f, 0);

	font_.loadFromFile("Zpix.ttf");
	text_.setFont(font_);
	text_.setString({"Press SPACE to continue"});
	text_.setCharacterSize(30);
	sf::FloatRect textRect = text_.getLocalBounds();
	text_.setOrigin(textRect.left + textRect.width / 2.0f,
				   textRect.top + textRect.height / 2.0f);
	text_.setPosition(windowSize.x / 2.0f, windowSize.y / 2.0f);

	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->AddCallback(GameStateType::Intro, "Intro_Continue", &StateIntro::Continue, this);
}

void
StateIntro::OnDestroy()
{
	EventManager* eventManager = stateManager_->GetContext()->eventManager;
	eventManager->RemoveCallback(GameStateType::Intro, "Intro_Continue");
}


/********************************************************************************
 * Update
 ********************************************************************************/
void
StateIntro::Update(const sf::Time& time)
{
	if(timePassed_ < 5.0f)
	{
		timePassed_ += time.asSeconds();
		introSprite_.setPosition(introSprite_.getPosition().x,
								 introSprite_.getPosition().y + (48 * time.asSeconds()));
	}
}

/********************************************************************************
 * StateIntro
 ********************************************************************************/
void
StateIntro::Draw()
{
	sf::RenderWindow* window = stateManager_->GetContext()->window->GetRenderWindow();

	window->draw(introSprite_);
	if(timePassed_ >= 5.0f)
	{
		window->draw(text_);
	}
}

/********************************************************************************
 * Gets Called when Binding"Intro_Continue" is triggered.
 ********************************************************************************/
void
StateIntro::Continue(EventDetails* eventDetails)
{
	std::cout << "StateIntro::Continue called!" << std::endl;
	if(timePassed_ >= 5.0f)
	{
		stateManager_->SwitchTo(GameStateType::MainMenu);
		stateManager_->RequestRemove(GameStateType::Intro);
	}
}


void StateIntro::Activate(){}
void StateIntro::Deactivate(){}
