#include "Clock.h"

Clock* Clock::instance_ = nullptr;


Clock::Clock()
	: timeElapsed_(0.0f)
	, allTimeElapsed_(0.0f)
	, speed_(ClockSpeed::Normal)
{
	Start();
}

Clock*
Clock::Instance()
{
	if(instance_ == nullptr)
		instance_ = new Clock();
	return instance_;
}

void
Clock::Start()
{
	started_ = true;
}

void
Clock::Pause()
{
}

void
Clock::Stop()
{
}


void
Clock::Update(const sf::Time& time)
{
	timeElapsed_ += time.asSeconds();
	allTimeElapsed_ += time.asSeconds();
	if(timeElapsed_ >= 1.0f)
	{
		switch(speed_)
		{
			case ClockSpeed::Normal:
			{
				gameTime_.minutes_++;
				if(gameTime_.minutes_ >= 59)
				{
					gameTime_.minutes_ = 0;
					gameTime_.hours_++;
				}
				if(gameTime_.hours_ >= 24)
				{
					gameTime_.hours_ = 0;
					gameTime_.days_++;
				}
			} break;

			case ClockSpeed::Fast:
			{
				gameTime_.minutes_ += 6;
			} break;
		}
		timeElapsed_ = 0.0f;
	}
}

real32
Clock::GetWallTime() const
{
	return 100;
}

GameTime
Clock::GetGameTime() const
{
	return gameTime_;
}


