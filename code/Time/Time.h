#pragma once

#include "../common.h"

class Clock;
class GameTime
{
	friend Clock;
public:
	GameTime()
		: days_(0)
		, hours_(0)
		, minutes_(0)
	{
	}

	GameTime(uint32 days, uint32 hours, uint32 minutes)
		: days_(days)
		, hours_(hours)
		, minutes_(minutes)
	{
	}

	~GameTime()
	{
	}

	// overloaded operators
	bool operator==(GameTime& t2)
	{
		if (days_ != t2.days_)
			return false;
		if (hours_ != t2.hours_)
			return false;
		if (minutes_ != t2.minutes_)
			return false;
		return true;
	}
	/**
	bool operator<(GameTime& t2)
	{
		uint32 total1 = minutes_ + hours_ * 60 + days_ * 60 * 24;
		uint32 total2 = t2.minutes_ + t2.hours_ * 60 + t2.days_ * 60 * 24;
		return (total1 < total2);
	}
	*/

	bool operator>(GameTime& t2)
	{
		uint32 total1 = minutes_ + hours_ * 60 + days_ * 60 * 24;
		uint32 total2 = t2.minutes_ + t2.hours_ * 60 + t2.days_ * 60 * 24;
		return (total1 > total2);
	}
public:
	uint32 days_;
	uint32 hours_;
	uint32 minutes_;
};

inline bool operator<(const GameTime& t1, const GameTime& t2)
{
	uint32 total1 = t1.minutes_ + t1.hours_ * 60 + t1.days_ * 60 * 24;
	uint32 total2 = t2.minutes_ + t2.hours_ * 60 + t2.days_ * 60 * 24;
	return (total1 < total2);
}

inline bool operator>=(const GameTime& t1, const GameTime& t2)
{
	uint32 total1 = t1.minutes_ + t1.hours_ * 60 + t1.days_ * 60 * 24;
	uint32 total2 = t2.minutes_ + t2.hours_ * 60 + t2.days_ * 60 * 24;
	return (total1 >= total2);
}

inline GameTime operator+(const GameTime& t1, const GameTime& t2)
{
	uint32 total1 = t1.minutes_ + t1.hours_ * 60 + t1.days_ * 60 * 24;
	uint32 total2 = t2.minutes_ + t2.hours_ * 60 + t2.days_ * 60 * 24;
	uint32 totalMinutes = total1 + total2;

	uint32 newDays = totalMinutes / (24 * 60);
	uint32 newHours = (totalMinutes - newDays * 24 * 60) / 60;
	uint32 newMinutes = (totalMinutes - newDays * 24 * 60) % 60;
	return GameTime(newDays,
					newHours,
					newMinutes);
}

inline GameTime operator-(const GameTime& t1, const GameTime& t2)
{
	uint32 total1 = t1.minutes_ + t1.hours_ * 60 + t1.days_ * 60 * 24;
	uint32 total2 = t2.minutes_ + t2.hours_ * 60 + t2.days_ * 60 * 24;
	uint32 totalMinutes = total1 - total2;
	if(totalMinutes > 0)
	{
		uint32 newDays = totalMinutes / (24 * 60);
		uint32 newHours = (totalMinutes - newDays * 24 * 60) / 60;
		uint32 newMinutes = (totalMinutes - newDays * 24 * 60) % 60;
		return GameTime(newDays,
						newHours,
						newMinutes);
	}
	return GameTime(0, 0, 0);
}





class GameTimeDuration
{
public:
	GameTimeDuration(uint32 days, uint32 hours, uint32 minutes)
		: days_(days)
		, hours_(hours)
		, minutes_(minutes)
	{
	}
private:
	uint32 days_;
	uint32 hours_;
	uint32 minutes_;
};
