#pragma once
#include "../common.h"
#include "Time.h"
#include <SFML/System.hpp>

enum class ClockSpeed
{
	Normal,
	Fast,
	Fastest,
};
class Clock
{
private:
	Clock();
	static Clock* instance_;
private:
	void Start();
	void Stop();
	void Pause();
	bool started_;
	bool paused_;
	bool stopped_;
	real32 timeElapsed_;
	real32 allTimeElapsed_;
	ClockSpeed speed_;
	GameTime gameTime_;
public:
	Clock(Clock& other) = delete;
	void operator=(const Clock&) = delete;

	static Clock* Instance();
	void Update(const sf::Time& time);
public:
	// seconds since start
	real32 GetWallTime() const;
	//
	GameTime GetGameTime() const;
};
