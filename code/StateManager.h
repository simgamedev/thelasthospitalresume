#pragma once
#include <vector>
#include <unordered_map>
#include "StateIntro.h"
#include "StateMainMenu.h"
#include "StateGame.h"
#include "StatePaused.h"
#include "StateLoading.h"
#include "StateMapEditor.h"
#include "SharedContext.h"
#include "GameStateTypes.h"


using StateContainer = std::vector<std::pair<GameStateType, BaseState*>>;
//using StateTypeContainer = std::vector<StateType>;
using StatesFactories = std::unordered_map<GameStateType, std::function<BaseState*(void)>>;

/**
   A Managaer that manages all the game states.
   Each state has to be registered in StateManager's constructor.
 */
class StateManager
{
public:
	StateManager(SharedContext* sharedContext);
	~StateManager();

	void Update(const sf::Time& time);
	void Draw();

	SharedContext* GetContext();
	bool HasState(const GameStateType& type);
	void SwitchTo(const GameStateType& type);
	void RequestRemove(const GameStateType& type);
	void ProcessRemovingRequests();
	template<class T>
	T* GetState(const GameStateType& type)
	{
		for(auto itr = states_.begin(); itr != states_.end(); ++itr)
		{
			GameStateType stateType = itr->first;
			if(stateType != type) { continue; }
			BaseState* state = itr->second;
			return static_cast<T*>(state);
		}
		return nullptr;
	}
	GameStateType GetNextToLastGameStateType() const;
	GameStateType GetCurrentGameStateType() const;
	/**
	   Things that behaveds differently based on currentGameState_
	 */
	void AddDependent(StateDependent* dependent);
	void RemoveDependent(StateDependent* dependent);
private:
	void CreateState(const GameStateType& type);
	void RemoveState(const GameStateType& type);
	void SetStateDependents(const GameStateType& type);

	template<class T>
	void RegisterState(const GameStateType& type)
	{
		statesFactories_[type] = [this]() -> BaseState*
		{
			return new T(this);
		};
	}

	SharedContext* sharedContext_;
	StateContainer states_;
	std::vector<GameStateType> toRemove_;
	StatesFactories statesFactories_;
	std::vector<StateDependent*> stateDependents_;
	StateLoading* stateLoading_;
};

