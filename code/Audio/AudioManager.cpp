#include "AudioManager.h"
#include "../StateManager.h"


AudioManager::AudioManager(AudioResourceManager* audioManager)
	: nextID_(0)
	, pAudioManager_(audioManager)
	, timeElapsed_(0.0f)
	, numAudios_(0)
{
}

AudioManager::~AudioManager()
{
	Cleanup();
}

void
AudioManager::ChangeGameState(const GameStateType& state)
{
	PauseAll(currentGameState_);
	UnpauseAll(state);
	currentGameState_ = state;

	if(musics_.find(currentGameState_) != musics_.end()) { return; }
	AudioInfo info("");
	sf::Music* music = nullptr;
	musics_.emplace(currentGameState_, std::make_pair(info, music));
}

void
AudioManager::RemoveGameState(const GameStateType& state)
{
	auto& stateSounds = sounds_.find(state)->second;

	for(auto& itr : stateSounds)
	{
		RecycleSound(itr.first, itr.second.second, itr.second.first.name);
	}

	sounds_.erase(state);

	auto musicsItr = musics_.find(state);
	if(musicsItr == musics_.end()) { return; }
	if(musicsItr->second.second)
	{
		delete musicsItr->second.second;
		--numAudios_;
	}
	musics_.erase(state);
}	

void
AudioManager::Cleanup()
{
	// ---> sounds
	for(auto& soundsItr : sounds_)
	{
		for(auto& sound : soundsItr.second)
		{
			// TODO: chekc didn't we delete pointer in ReleaseResource??
			pAudioManager_->ReleaseResource(sound.second.first.name);
			delete sound.second.second;
		}
	}
	sounds_.clear();
	// ---< sounds


	// ---> musics
	for(auto& musicsItr : musics_)
	{
		if(musicsItr.second.second)
		{
			delete musicsItr.second.second;
		}
	}
	musics_.clear();
	// ---< musics

	soundProperties_.clear();
	numAudios_ = 0;
	nextID_ = 0;
}

void
AudioManager::Update(real32 dt)
{
	timeElapsed_ += dt;
	if(timeElapsed_ < 0.33f) { return; } // Run once every third of a second.

	// ---> sounds
	auto& soundsContainer = sounds_[currentGameState_];
	for(auto itr = soundsContainer.begin();
		itr != soundsContainer.end();)
	{
		if(!itr->second.second->getStatus())
		{
			RecycleSound(itr->first, itr->second.second, itr->second.first.name);
			itr = soundsContainer.erase(itr);
			continue;
		}
		++itr;
	}
	// ---< sounds

	// ---> musics
	auto& musicsItr = musics_.find(currentGameState_);
	if(musicsItr == musics_.end()) { return; }
	if(!musicsItr->second.second) { return; }
	if(musicsItr->second.second->getStatus()){ return; }
	delete musicsItr->second.second;
	musicsItr->second.second = nullptr;
	--numAudios_;
	// ---< musics
}

/****************************** Play ********************************************
 *
 ********************************************************************************/
SoundID
AudioManager::Play(const string& soundName,
						const vec3f pos,
						bool loop,
						bool relative)
{
	SoundProps* props = GetSoundProperties(soundName);
	if(!props) { return -1; } // Failed to load sound properties.
	SoundID soundID;
	sf::Sound* sound = CreateSound(soundID, props->name);
	if(!sound) { return -1; }
	// Sound created successfully.
	SetupSound(sound, props, loop, relative);
	sound->setPosition(pos);
	AudioInfo info(props->name);
	sounds_[currentGameState_].emplace(soundID, std::make_pair(info, sound));
	sound->play();
	return soundID;
}

bool
AudioManager::Play(const SoundID& soundID)
{
	auto& soundsContainer = sounds_[currentGameState_];
	auto soundsItr = soundsContainer.find(soundID);
	if(soundsItr == soundsContainer.end()) { return false; }
	soundsItr->second.second->play();
	soundsItr->second.first.bManualPaused = false;
	return true;
}

/****************************** Stop ****************************************
 * 
 ****************************************************************************/
bool
AudioManager::Stop(const SoundID& soundID)
{
	auto& soundsContainer = sounds_[currentGameState_];
	auto soundsItr = soundsContainer.find(soundID);
	if(soundsItr == soundsContainer.end()) { return false; }
	soundsItr->second.second->stop();
	soundsItr->second.first.bManualPaused = true;
	return true;
}

/****************************** Pause ***************************************
 * 
 ****************************************************************************/
bool
AudioManager::Pause(const SoundID& soundID)
{
	auto& soundsContainer = sounds_[currentGameState_];
	auto soundsItr = soundsContainer.find(soundID);
	if(soundsItr == soundsContainer.end()) { return false; }
	soundsItr->second.second->pause();
	soundsItr->second.first.bManualPaused = true;
	return true;
}

/****************************** PlayMusic ***************************************
 *
 ********************************************************************************/
bool
AudioManager::PlayMusic(const string& musicName, real32 volume, bool loop)
{
	auto s = musics_.find(currentGameState_);
	if(s == musics_.end()) { return false; }
	string musicFilePath = pAudioManager_->GetPath(musicName);
	if(musicFilePath == "") { return false; }
	if(!s->second.second)
	{
		s->second.second = new sf::Music();
		++numAudios_;
	}
	sf::Music* music = s->second.second;
	if(!music->openFromFile(Utils::GetMediaDirectory() + musicFilePath))
	{
		delete music;
		--numAudios_;
		s->second.second = nullptr;
		LOG_ENGINE_ERROR("[SoundManager] Failed to load music from file: " + musicName);
		return false;
	}
	music->setLoop(loop);
	music->setVolume(volume);
	music->setRelativeToListener(true);
	music->play();
	s->second.first.name = musicName;
	return true;
}

/****************************** PlayMusic ***************************************
 *
 ********************************************************************************/
bool
AudioManager::PlayMusic(const GameStateType& state)
{
	auto musicsItr = musics_.find(currentGameState_);
	if(musicsItr == musics_.end()) { return false; }
	if(!musicsItr->second.second) { return false; }
	musicsItr->second.second->play();
	musicsItr->second.first.bManualPaused = false;
	return true;
}

/****************************** StopMusic ***************************************
 *
 ********************************************************************************/
bool
AudioManager::StopMusic(const GameStateType& state)
{
	auto musicsItr = musics_.find(currentGameState_);
	if(musicsItr == musics_.end()) { return false; }
	if(!musicsItr->second.second) { return false; }
	musicsItr->second.second->stop();
	delete musicsItr->second.second;
	musicsItr->second.second = nullptr;
	--numAudios_;
	return true;
}

/****************************** PauseMusic **************************************
 *
 ********************************************************************************/
bool
AudioManager::PauseMusic(const GameStateType& state)
{
	auto musicsItr = musics_.find(currentGameState_);
	if(musicsItr == musics_.end()) { return false; }
	if(!musicsItr->second.second) { return false; }
	musicsItr->second.second->pause();
	musicsItr->second.first.bManualPaused = true;
	return true;
}

/****************************** SetPosition *************************************
 *    
 ********************************************************************************/
bool
AudioManager::SetPosition(const SoundID& soundID, const vec3f& pos)
{
	auto& soundsContainer = sounds_[currentGameState_];
	auto soundsItr = soundsContainer.find(soundID);
	if(soundsItr == soundsContainer.end()) { return false; }
	soundsItr->second.second->setPosition(pos);
	return true;
}

/****************************** IsPlaying ***************************************
 *
 ********************************************************************************/
bool
AudioManager::IsPlaying(const SoundID& soundID)
{
	auto& soundsContainer = sounds_[currentGameState_];
	auto soundsItr = soundsContainer.find(soundID);
	return (soundsItr != soundsContainer.end() ? soundsItr->second.second->getStatus() : false);
}

/****************************** GetSoundProperty ********************************
 *
 ********************************************************************************/
SoundProps*
AudioManager::GetSoundProperties(const string& soundName)
{
	auto& propsItr = soundProperties_.find(soundName);
	if(propsItr == soundProperties_.end())
	{
		if(!LoadSoundProperties(soundName)) { return nullptr; }
		propsItr = soundProperties_.find(soundName);
	}
	return &propsItr->second;
}

/****************************** LoadProperties **********************************
 *
 ********************************************************************************/
bool
AudioManager::LoadSoundProperties(const string& soundName)
{
	std::ifstream file;
	file.open(Utils::GetMediaDirectory() + "Audio/Sounds/Properties/" + soundName + ".sound");
	if(!file.is_open())
	{
		LOG_ENGINE_ERROR("[FILE]Failed to load sound: " + soundName);
		return false;
	}
	SoundProps props("");
	string line;
	while(std::getline(file, line)){
		if(line[0] == '|') { continue; } // this is a comment line
		std::stringstream keystream(line);
		string type;
		keystream >> type;
		if(type == "Name")
		{
			keystream >> props.name;
		} else if(type == "Volume")
		{
			keystream >> props.volume;
		}
		else if(type == "Pitch")
		{
			keystream >> props.pitch;
		}
		else if(type == "Distance")
		{
			keystream >> props.minDistance;
		}
		else if(type == "Attenuation")
		{
			keystream >> props.attenuation;
		}
		else
		{
			LOG_ENGINE_WARN("[FILE] Unknown key!");
		}
		file.close();
		if(props.name == "") { return false; }
		soundProperties_.emplace(soundName, props);
		return true;
	}
}


/****************************** PauseAll ****************************************
 *
 ********************************************************************************/
void
AudioManager::PauseAll(const GameStateType& state)
{
	// ---> sounds
	auto& container = sounds_[state];
	for(auto itr = container.begin();
		itr != container.end();)
	{
		if(!itr->second.second->getStatus())
		{
			RecycleSound(itr->first, itr->second.second, itr->second.first.name);
			itr = container.erase(itr);
			continue;
		}
		itr->second.second->pause();
		++itr;
	}
	// ---< sounds

	// ---> musics
	auto& musicsItr = musics_.find(state);
	if(musicsItr == musics_.end()) { return; }
	if(!musicsItr->second.second) { return; }
	musicsItr->second.second->pause();
	// ---< musics
}

/****************************** UnpauseAll **************************************
 *    
 ********************************************************************************/
void
AudioManager::UnpauseAll(const GameStateType& state)
{
	// ---> sounds
	auto& container = sounds_[state];
	for(auto& itr : container)
	{
		if(itr.second.first.bManualPaused) { continue; }
		itr.second.second->play();
	}
	// ---< sounds

	// ---> music
	auto& musicsItr = musics_.find(state);
	if(musicsItr == musics_.end()) { return; }
	// TODO: There should be just one single musice right?
	if(!musicsItr->second.second || musicsItr->second.first.bManualPaused)  { return; }
	musicsItr->second.second->play();
	// ---< music
}

/****************************** CreateSound ************************************
 *    
 ********************************************************************************/
sf::Sound*
AudioManager::CreateSound(SoundID& soundID, const string& soundName)
{
	sf::Sound* sound = nullptr;
	// ---> Case: AudioCache Full
	if(!recycled_.empty() && (numAudios_ >= MaxNumAudios || recycled_.size() >= AudioCache))
	{
		auto itr = recycled_.begin();
		while(itr != recycled_.end())
		{
			if(itr->first.second == soundName) { break; }
			++itr;
		}
		if(itr == recycled_.end())
		{
			// If a sound with the same name hasn't been found!
			auto element = recycled_.begin();
			soundID = element->first.first;
			pAudioManager_->ReleaseResource(element->first.second);
			pAudioManager_->RequireResource(soundName);
			sound = element->second;
			sound->setBuffer(*pAudioManager_->GetResource(soundName));
			recycled_.erase(element);
		}
		else
		{
			soundID = itr->first.first;
			sound = itr->second;
			recycled_.erase(itr);
		}
		return sound;
	}
	// ---<

	// ---> Case: AudioCache not full yet
	if(numAudios_ < MaxNumAudios)
	{
		if(pAudioManager_->RequireResource(soundName))
		{
			sound = new sf::Sound();
			soundID = nextID_;
			++nextID_;
			++numAudios_;
			sound->setBuffer(*pAudioManager_->GetResource(soundName));
			return sound;
		}
	}
	// ---<
	LOG_ENGINE_ERROR("[SoundManager] Failed to create sound.");
	return nullptr;
}

/****************************** SetupSound ************************************
 *
 ********************************************************************************/
void
AudioManager::SetupSound(sf::Sound* sound, const SoundProps* props, bool loop, bool relative)
{
	sound->setVolume(props->volume);
	sound->setPitch(props->pitch);
	sound->setMinDistance(props->minDistance);
	sound->setAttenuation(props->attenuation);
	sound->setLoop(loop);
	sound->setRelativeToListener(relative);
}

/****************************** RecycleSound ************************************
 *
 ********************************************************************************/
void
AudioManager::RecycleSound(const SoundID& soundID,
						   sf::Sound* sound,
						   const string& soundName)
{
	recycled_.emplace_back(std::make_pair(soundID, soundName), sound);
}
