#pragma once

#include <unordered_map>
#include <vector>
#include <SFML/Audio.hpp>
#include "../AudioResourceManager.h"
#include "SoundProps.h"
#include "../Utilities.h"
#include "../StateDependent.h"


using SoundID = int32;

enum class GameStateType;
// TODO: SoundInfo should have a more meaningful name
struct AudioInfo
{
	AudioInfo(const string& soundName)
		: name(soundName)
		, bManualPaused(false)
	{
	}
	string name;
	bool bManualPaused; // only use b/p prefix
};

using SoundProperties = std::unordered_map<string, SoundProps>;
using SoundContainer = std::unordered_map<SoundID, std::pair<AudioInfo, sf::Sound*>>;
using Sounds = std::unordered_map<GameStateType, SoundContainer>;
using RecycledSounds = std::vector<std::pair<std::pair<SoundID, string>, sf::Sound*>>;
using MusicContainer = std::unordered_map<GameStateType, std::pair<AudioInfo, sf::Music*>>;
// TODO: we should have more than one music

class AudioManager : public StateDependent
{
public:
	AudioManager(AudioResourceManager* audioManager);
	~AudioManager();

	void ChangeGameState(const GameStateType& state);
	void RemoveGameState(const GameStateType& state);

	void Update(real32 dt);

	SoundID Play(const string& soundName, const vec3f pos = vec3f(0.0f, 0.0f, 0.0f), bool loop = false, bool relative = false);
	bool Play(const SoundID& soundID);
	bool Stop(const SoundID& soundID);
	bool Pause(const SoundID& soundID);

	bool PlayMusic(const string& musicName, real32 volume = 100.0f, bool loop = false);
	bool PlayMusic(const GameStateType& state);
	bool StopMusic(const GameStateType& state);
	bool PauseMusic(const GameStateType& state);

	bool SetPosition(const SoundID& soundID, const vec3f& pos);
	bool IsPlaying(const SoundID& soundID);
	SoundProps* GetSoundProperties(const string& soundName);

	static const int32 MaxNumAudios = 256;
	static const int32 AudioCache = 128;

private:
	bool LoadSoundProperties(const string& filepath);
	void PauseAll(const GameStateType& state);
	void UnpauseAll(const GameStateType& state);

	sf::Sound* CreateSound(SoundID& soundID, const string& soundName);
	void SetupSound(sf::Sound* sound, const SoundProps* props, bool loop = false, bool relative = false);
	void RecycleSound(const SoundID& soundID, sf::Sound* sound, const string& soudnName);

	void Cleanup();

	Sounds sounds_;
	MusicContainer musics_;
	RecycledSounds recycled_;
	SoundProperties soundProperties_;
	GameStateType currentGameState_;

	SoundID nextID_;
	uint32 numAudios_;
	real32 timeElapsed_;

	AudioResourceManager* pAudioManager_;
};
