#pragma once

#include "../common.h"

/** SoundProps */
/****************************** SoundProps ****************************************
 * SFML Sound Properties
 * and this should be the way all structs are code-styled.
 ********************************************************************************/
struct SoundProps
{
	SoundProps(const string& soundName)
		: name(soundName)
		, volume(100)
		, pitch(1.0f)
		, minDistance(10.0f)
		, attenuation(10.0f)
	{
	}

	string name;
	real32 volume;
	real32 pitch;
	real32 minDistance;
	real32 attenuation;
};
