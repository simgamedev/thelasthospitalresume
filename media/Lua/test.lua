-- Basic Info
BasicInfo = {}
BasicInfo["Cash"] = 731.0
BasicInfo["ResearchPoints"] = 6
BasicInfo["NumStaff"] = 0
BasicInfo["StaffCapacity"] = 0
-- Basic Prices
BasicPrices = {}
BasicPrices["Foundation"] = 50
BasicPrices["Wall"] = 200
BasicPrices["BulldozeWall"] = 50
BasicPrices["BulldozeFoundation"] = 200


--------------------------------------------------------------------------
--- Testing Only
--------------------------------------------------------------------------
function LoadLevel(host, level)
   map = ""
   size = {w=16, h=15}

   if level == 1 then
      map =
	 "................"..
	 "................"..
	 "........######.."..
	 ".......#######.."..
	 "........######.."..
	 "................"
   end

   _CreateLevel(host, size.w, size.h)

   -- NOTE: lua counts from 1, not 0
   for y=1, size.h do
      for x=1, size.w do
	 c = string.sub(map, ((y-1) * size.w + x), ((y-1) * size.w + x))

	 if c == '.' then _SetTile(host, x-1, y-1, TILE_EMPTY)
	 elseif c=='#' then _SetTile(host, x-1, y-1, TILE_BLOCK)
	 end
      end
   end
end

